// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  initialize,
} from 'redux-form';
// config
import {
  FORM_ANNOUNCEMENT_SEARCH_FORM,
} from '../../shared/form.js';
import * as SearchActions from '../../actions/Search.js';
// components
import Input from '../Form/Input.jsx';
import SubmitButton from '../Form/SubmitButton.jsx';

const styles = {
  placement: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: 'auto',
    height: 'auto',
    padding: 6,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '4px 36px',
  },
};

type Props = {
  handleSubmit: Function,
  cacheSearchOptions: Function,
  initializeForm: Function,
  announcementOptions: {
    keyword?: string,
    startDate?: string,
    endDate?: string,
  },
};

class AnnouncementSearchForm extends PureComponent<Props> {
  componentDidMount() {
    const {
      announcementOptions,
    } = this.props;

    this.initializing(announcementOptions);
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    initializeForm({
      keyword: (obj && obj.keyword) || '',
      startDate: (obj && obj.startDate) || null,
      endDate: (obj && obj.endDate) || null,
    });
  }

  submit(d) {
    const {
      cacheSearchOptions,
    } = this.props;

    const {
      CACHE_ADMIN_ANNOUNCEMENT_SEARCH,
    } = SearchActions;

    const {
      keyword,
      startDate,
      endDate,
    } = d;

    cacheSearchOptions(CACHE_ADMIN_ANNOUNCEMENT_SEARCH, {
      keyword: keyword || '',
      startDate: startDate || null,
      endDate: endDate || null,
    });
  }

  render() {
    const {
      handleSubmit,
    } = this.props;

    return (
      <div style={styles.placement}>
        <form style={styles.searchWrapper} onSubmit={handleSubmit(d => this.submit(d))}>
          <div style={styles.fieldWrapper}>
            <Field
              type="date"
              name="startDate"
              label="開始時間:"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              type="date"
              name="endDate"
              label="結束時間:"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              name="keyword"
              label="關鍵字："
              placeholder="標題、內容"
              component={Input} />
          </div>
          <div style={styles.functionWrapper}>
            <SubmitButton label="查詢" />
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_ANNOUNCEMENT_SEARCH_FORM,
});

const reduxHook = connect(
  state => ({
    announcementOptions: state.Search.adminAnnouncements,
  }),
  dispatch => bindActionCreators({
    ...SearchActions,
    initializeForm: v => initialize(FORM_ANNOUNCEMENT_SEARCH_FORM, v),
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      AnnouncementSearchForm
    )
  )
);
