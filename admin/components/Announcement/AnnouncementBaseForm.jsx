// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  initialize,
  SubmissionError,
} from 'redux-form';

// config
import {
  FORM_ANNOUNCEMENT_CREATE_EDIT_FORM,
} from '../../shared/form.js';
// components
import FormTitle from '../Global/FormTitle.jsx';
import SubmitButton from '../Form/SubmitButton.jsx';
import Button from '../Global/Button.jsx';
import Input from '../Form/Input.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 12,
  },
  formWrapper: {
    width: 650,
    height: 'auto',
    padding: '16px 32px',
    backgroundColor: '#fff',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '24px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  buttonWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 6px',
  },
  textarea: {
    width: 586,
  },
};

type Props = {
  handleSubmit: Function,
  initializeForm: Function,
  history: {
    push: Function,
  },
  data: {
    id: number,
    title: string,
    content: string,
    createdAt: string,
  },
  mutate: Function,
};

class AnnouncementBaseForm extends PureComponent<Props> {
  componentDidMount() {
    const {
      data,
    } = this.props;

    this.initializing(data);
  }

  componentDidUpdate(prevProps) {
    const {
      data,
    } = this.props;

    if (data !== prevProps.data && data) {
      this.initializing(data);
    }
  }

  getFields(d) {
    const {
      title,
      content,
    } = d;

    if (!title) {
      throw new SubmissionError({
        title: 'required',
      });
    }

    if (!content) {
      throw new SubmissionError({
        content: 'required',
      });
    }

    return ({
      title,
      content,
    });
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    initializeForm({
      title: (obj && obj.title) || '',
      content: (obj && obj.content) || '',
    });
  }

  render() {
    const {
      handleSubmit,
      history,
      data,
      mutate,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <FormTitle
          title={data ? '修改公告' : '新增公告'} />
        <form
          style={styles.formWrapper}
          onSubmit={handleSubmit(async (d) => {
            const {
              title,
              content,
            } = this.getFields(d);

            if (!data) {
              await mutate({
                variables: {
                  title,
                  content,
                },
              });
            } else {
              await mutate({
                variables: {
                  id: data && data.id ? parseInt(data.id, 10) : null,
                  title,
                  content,
                },
              });
            }

            return history.push('/announcement');
          })}>
          <div style={styles.fieldWrapper}>
            <Field
              name="title"
              label="標題："
              placeholder="公告標題"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              type="textarea"
              name="content"
              label="內容："
              placeholder="公告內容"
              component={Input}
              style={styles.textarea} />
          </div>
          <div style={styles.functionWrapper}>
            <div style={styles.buttonWrapper}>
              <SubmitButton label="送出" />
            </div>
            <div style={styles.buttonWrapper}>
              <Button
                label="返回"
                onClick={() => history.push('/announcement')} />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_ANNOUNCEMENT_CREATE_EDIT_FORM,
});

const reduxHook = connect(
  () => ({}),
  dispatch => bindActionCreators({
    initializeForm: v => initialize(FORM_ANNOUNCEMENT_CREATE_EDIT_FORM, v),
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      AnnouncementBaseForm
    )
  )
);
