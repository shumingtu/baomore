// @flow

import React, { PureComponent } from 'react';

import Theme from '../../styles/Theme.js';

const styles = {
  checkBox: {
    width: 20,
    height: 20,
    padding: 0,
    backgroundColor: 'transparent',
    border: `1px solid ${Theme.BLACK_COLOR}`,
    borderRadius: 4,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
    outline: 0,
    ':hover': {
      opacity: 0.88,
    },
  },
  checkedInner: {
    width: 14,
    height: 14,
    borderRadius: 4,
    backgroundColor: Theme.ACTIVE_COLOR,
  },
};

type Props = {
  value: string | number,
  onChange: Function,
  isChecked: boolean,
};

class Checkbox extends PureComponent<Props> {
  render() {
    const {
      value,
      onChange,
      isChecked,
    } = this.props;

    return (
      <button
        key="checkbox-element"
        type="button"
        onClick={() => onChange(value)}
        style={styles.checkBox}>
        {isChecked ? (
          <div style={styles.checkedInner} />
        ) : null}
      </button>
    );
  }
}

export default Checkbox;
