// @flow
import React, { memo } from 'react';
import radium from 'radium';
import { withRouter } from 'react-router-dom';

import editIcon from '../../static/images/edit-icon.png';

const styles = {
  wrapper: {
    width: 24,
    height: 24,
    padding: 0,
    margin: 0,
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    backgroundImage: `url(${editIcon})`,
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    cursor: 'pointer',
    ':hover': {
      opacity: 0.9,
    },
  },
  disabled: {
    opacity: 0.3,
  },
};

function EditButton({
  isPopUp,
  action,
  path,
  disabled = false,
  history,
}: {
  path: string,
  disabled: boolean,
  history: {
    push: Function,
  },
  isPopUp?: boolean,
  action?: Function,
}) {
  return (
    <button
      type="button"
      disabled={disabled}
      onClick={() => {
        if (isPopUp) {
          action();
          return;
        }
        history.push(path);
      }}
      style={[
        styles.wrapper,
        disabled && styles.disabled,
      ]} />
  );
}

EditButton.defaultProps = {
  isPopUp: false,
  action: null,
};

export default memo(
  withRouter(
    radium(EditButton)
  )
);
