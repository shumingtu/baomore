// @flow

import React from 'react';
import radium from 'radium';

import leftArrow from '../../static/images/arrow-left-icon.png';

const styles = {
  naviBtn: {
    width: 12,
    height: 12,
    cursor: 'pointer',
    outline: 'none',
    border: 'none',
    backgroundImage: `url(${leftArrow})`,
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundColor: 'transparent',
  },
};

function PrevButton({
  onClick,
  style = {},
}: {
  onClick: Function,
  style: {},
}) {
  return (
    <button
      type="button"
      style={[
        styles.naviBtn,
        style,
      ]}
      onClick={onClick} />
  );
}

export default radium(PrevButton);
