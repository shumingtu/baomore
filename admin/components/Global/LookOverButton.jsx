// @flow
import React from 'react';
import radium from 'radium';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as PNModelActions from '../../actions/PNModel.js';

import lookOverIcon from '../../static/images/loupe.png';

const styles = {
  wrapper: {
    width: 24,
    height: 24,
    padding: 0,
    margin: 0,
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    backgroundImage: `url(${lookOverIcon})`,
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    cursor: 'pointer',
    ':hover': {
      opacity: 0.9,
    },
  },
  disabled: {
    opacity: 0.3,
  },
};

function LookOverButton({
  disabled = false,
  togglePNModelPopUp = null,
  data = {},
}: {
  data: Object,
  togglePNModelPopUp: Function,
  disabled: boolean,
}) {
  return (
    <button
      type="button"
      disabled={disabled}
      onClick={() => togglePNModelPopUp(data)}
      style={[
        styles.wrapper,
        disabled && styles.disabled,
      ]} />
  );
}

const reduxHook = connect(
  () => ({
  }),
  dispatch => bindActionCreators({
    ...PNModelActions,
  }, dispatch),
);

export default reduxHook(
  withRouter(
    radium(LookOverButton)
  )
);
