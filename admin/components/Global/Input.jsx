// @flow
/* eslint jsx-a11y/label-has-for: 0 */
import React, { PureComponent } from 'react';
import radium from 'radium';
// config
import Theme from '../../styles/Theme.js';

const styles = {
  input: {
    flex: 1,
    fontSize: 14,
    padding: '8px 8px 10px 10px',
    border: `1px solid ${Theme.BLACK_COLOR}`,
    borderRadius: 2,
    backgroundColor: 'transparent',
    outline: 0,
    '::-webkit-input-placeholder': {
      color: 'rgba(0, 0, 0, 0.28)',
    },
  },
  disable: {
    backgroundColor: 'transparent',
    border: 'none',
  },
  disableText: {
    color: 'rgba(0, 0, 0, 0.38)',
    cursor: 'default',
  },
};

type Props = {
  defaultValue: string,
  onSubmit: Function,
  placeholder?: string,
  type?: string,
  style?: Object,
  disabled?: boolean,
};

type State = {
  value: string,
};

class Input extends PureComponent<Props, State> {
  static defaultProps = {
    type: 'text',
    style: [],
    placeholder: '',
    disabled: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      value: props.defaultValue || '',
    };
  }

  render() {
    const {
      onSubmit,
      placeholder,
      type,
      style,
      disabled,
    } = this.props;

    const {
      value,
    } = this.state;

    const customStyles = Array.isArray(style) ? style : [style];

    return (
      <input
        key="global-input"
        type={type}
        value={value}
        onChange={e => this.setState({ value: e.target.value })}
        onKeyPress={(e) => {
          if (e.key === 'Enter') {
            e.preventDefault();

            if (onSubmit) onSubmit(value);
          }
        }}
        placeholder={placeholder}
        disabled={disabled}
        style={[
          styles.input,
          disabled && styles.disableText,
          ...customStyles,
        ]} />
    );
  }
}

export default radium(Input);
