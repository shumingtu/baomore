// @flow
import React from 'react';
import radium from 'radium';

import Theme from '../../styles/Theme.js';

const styles = {
  text: {
    width: 'auto',
    height: 'auto',
    fontSize: 18,
    color: Theme.BLACK_COLOR,
    fontWeight: 500,
    padding: 24,
    margin: 0,
    textAlign: 'center',
  },
};

function FormTitle({
  title,
}: {
  title: string,
}) {
  if (!title) return null;

  return (
    <span style={styles.text}>
      {title}
    </span>
  );
}

export default radium(FormTitle);
