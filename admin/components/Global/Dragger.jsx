// @flow
import React from 'react';
import radium from 'radium';

import dragIcon from '../../static/images/drag-icon.png';
import dragIconActive from '../../static/images/drag-active-icon.png';
import dropAreaIcon from '../../static/images/drop-area-icon.png';

const styles = {
  wrapper: {
    width: 24,
    height: 24,
    border: 0,
    outline: 0,
    padding: 0,
    backgroundColor: 'transparent',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    backgroundImage: `url(${dragIcon})`,
    textAlign: 'center',
    cursor: 'pointer',
    ':hover': {
      opacity: 0.88,
    },
  },
  actived: {
    backgroundImage: `url(${dragIconActive})`,
  },
  dropArea: {
    backgroundImage: `url(${dropAreaIcon})`,
  },
  disable: {
    color: 'rgba(0, 0, 0, 0.3)',
    cursor: 'not-allowed',
  },
};

type Props = {
  isSwapMode: Boolean,
  isActived: Boolean,
  onClick: Function,
  disabled?: Boolean,
};

function Dragger({
  disabled,
  isSwapMode,
  isActived,
  onClick,
}: Props) {
  return (
    <button
      key="dragger"
      type="button"
      onClick={onClick}
      style={[
        styles.wrapper,
        isSwapMode && styles.dropArea,
        isActived && styles.actived,
        disabled && styles.disable,
      ]} />
  );
}

Dragger.defaultProps = {
  disabled: false,
};

export default radium(Dragger);
