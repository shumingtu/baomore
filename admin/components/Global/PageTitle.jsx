// @flow
import React from 'react';

import Theme from '../../styles/Theme.js';

const styles = {
  titleWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '24px 0',
  },
  title: {
    fontSize: 24,
    fontWeight: 700,
    color: Theme.BLACK_COLOR,
  },
};

function PageTitle({
  title,
}: {
  title: string,
}) {
  return (
    <div style={styles.titleWrapper}>
      <span style={styles.title}>
        {title || null}
      </span>
    </div>
  );
}

export default PageTitle;
