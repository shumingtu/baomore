// @flow
import React, { memo } from 'react';
import radium from 'radium';
// config
import ButtonStyles from '../../styles/Button.js';

const styles = {
  ...ButtonStyles,
};

type Props = {
  inactiveLabel: String,
  activeLabel: String,
  isSelected: Boolean,
  onClick: Function,
};

function SwitchButton({
  inactiveLabel,
  activeLabel,
  isSelected,
  onClick,
}: Props) {
  return (
    <button
      type="button"
      onClick={onClick}
      style={[
        styles.button,
        !isSelected && styles.inactive,
      ]}>
      {isSelected ? activeLabel : inactiveLabel}
    </button>
  );
}

export default memo(
  radium(
    SwitchButton
  )
);
