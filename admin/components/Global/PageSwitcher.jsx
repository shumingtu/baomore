// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
// config
import Theme from '../../styles/Theme.js';

const styles = {
  placement: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  wrapper: {
    width: 'auto',
    height: 'auto',
    minWidth: 500,
    padding: '6px 12px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderBottom: `1px solid ${Theme.BLACK_COLOR}`,
  },
  pageBtn: {
    width: 'auto',
    height: 'auto',
    border: 0,
    padding: '0 6px',
    margin: '0 4px',
    fontSize: 16,
    fontWeight: 500,
    letterSpacing: 1.3,
    color: Theme.BLACK_COLOR,
    backgroundColor: 'transparent',
    textAlign: 'center',
    outline: 0,
    cursor: 'pointer',
  },
  activePage: {
    color: Theme.ACTIVE_COLOR,
  },
};

type Props = {
  pages: Array<string>,
  currentPageIdx: number,
  onChange: Function,
};

class PageSwitcher extends PureComponent<Props> {
  render() {
    const {
      pages = [],
      currentPageIdx,
      onChange,
    } = this.props;

    return (
      <div style={styles.placement}>
        <div style={styles.wrapper}>
          {pages.map((p, idx) => (
            <button
              key={`${p}-button`}
              type="button"
              onClick={() => onChange(idx)}
              style={[
                styles.pageBtn,
                currentPageIdx === idx && styles.activePage,
              ]}>
              {p || null}
            </button>
          ))}
        </div>
      </div>
    );
  }
}

export default radium(PageSwitcher);
