// @flow
import React from 'react';
import radium from 'radium';
// config
import ButtonStyles from '../../styles/Button.js';

const styles = {
  ...ButtonStyles,
};

type Props = {
  label: string,
  onClick: Function,
  style: Object,
  disabled: boolean,
  isWhiteBg?: boolean,
};

function Button({
  label = '',
  onClick,
  style = [],
  disabled = false,
  isWhiteBg,
}: Props) {
  const customStyles = Array.isArray(style) ? style : [style];

  return (
    <button
      type="button"
      onClick={onClick}
      disabled={disabled}
      style={[
        styles.button,
        isWhiteBg && styles.subBtn,
        disabled && styles.btnDisabled,
        ...customStyles,
      ]}>
      {label}
    </button>
  );
}

Button.defaultProps = {
  isWhiteBg: false,
};

export default radium(
  Button
);
