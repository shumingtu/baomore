// @flow
import React from 'react';
import radium from 'radium';

import trashIcon from '../../static/images/trashcan-icon.png';

const styles = {
  wrapper: {
    width: 24,
    height: 24,
    padding: 0,
    margin: 0,
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    backgroundImage: `url(${trashIcon})`,
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    cursor: 'pointer',
    ':hover': {
      opacity: 0.9,
    },
  },
  disabled: {
    opacity: 0.3,
  },
};

function DeleteButton({
  onDelete,
  disabled = false,
}: {
  onDelete: Function,
  disabled: boolean,
}) {
  return (
    <button
      type="button"
      disabled={disabled}
      onClick={() => {
        if (confirm('確認刪除嗎？')) {
          onDelete();
        }
      }}
      style={[
        styles.wrapper,
        disabled && styles.disabled,
      ]} />
  );
}

export default radium(DeleteButton);
