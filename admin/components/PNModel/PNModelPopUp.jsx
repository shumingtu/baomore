// @flow
import React, { Component } from 'react';
import radium from 'radium';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import TextStyles from '../../styles/Text.js';

import * as PNModelActions from '../../actions/PNModel.js';
import Button from '../Global/Button.jsx';

import { PNTableDevice } from '../../shared/PNTable.js';

const styles = {
  wrapper: {
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 999,
  },
  box: {
    width: 600,
    height: 600,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    border: '1px solid #979797',
    backgroundColor: 'white',
    position: 'relative',
    padding: 40,
  },
  contentWrapper: {
    width: 520,
    height: 520,
    overflow: 'auto',
    display: 'inline-grid',
  },
  innerContentWrap: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  content: {
    margin: '10px 0',
    ...TextStyles.text,
    fontSize: 14,
  },
  descriptionWrap: {
    margin: '10px 0',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  descriptionBox: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    ...TextStyles.text,
    fontSize: 14,
    width: 400,
  },
  closeBtn: {
    position: 'relative',
    cursor: 'pointer',
    border: 'none',
    outline: 'none',
    fontSize: 20,
    backgroundColor: 'transparent',
  },
  circle: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: 30,
    height: 30,
    borderRadius: '100%',
    border: '2px solid black',
    position: 'absolute',
    top: 20,
    right: 20,
  },
  btnWrap: {
    margin: '10px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  imgWrap: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgInnerWrap: {
    width: 129,
    height: 300,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  },
  goEditBtn: {
    backgroundColor: 'rgb(182, 184, 204)',
  },
};

type Props = {
  data: Object,
  togglePNModelPopUp: Function,
  history: Object,
};

class PNModelPopUp extends Component<Props> {
  close() {
    const {
      togglePNModelPopUp,
    } = this.props;

    togglePNModelPopUp(null);
  }

  render() {
    const {
      data,
      history,
    } = this.props;

    if (!data) return null;

    const {
      id,
      code,
      pnCategory,
      bigSubCategory,
      smallSubCategory,
      name,
      price,
      vendor,
      suitableDevice,
      description,
      picture,
    } = data;

    const device = PNTableDevice.find(x => x.id === suitableDevice);

    return (
      <div style={styles.wrapper}>
        <div
          style={styles.box}>
          <div style={styles.circle}>
            <button
              onClick={() => this.close()}
              style={styles.closeBtn}
              type="button">
              X
            </button>
          </div>
          <div className="hideScrollBar" style={styles.contentWrapper}>
            <div style={styles.innerContentWrap}>
              {picture ? (
                <div style={styles.imgWrap}>
                  <div
                    style={[
                      styles.imgInnerWrap,
                      { backgroundImage: picture ? `url(${picture})` : null },
                    ]} />
                </div>
              ) : null}
              <span style={styles.content}>
                料號編號：
                {code}
              </span>
              <span style={styles.content}>
                料號類別：
                {pnCategory}
              </span>
              <span style={styles.content}>
                大類：
                {bigSubCategory}
              </span>
              <span style={styles.content}>
                小類：
                {smallSubCategory}
              </span>
              <span style={styles.content}>
                品名：
                {name}
              </span>
              <span style={styles.content}>
                價格：
                {price}
              </span>
              <span style={styles.content}>
                廠商：
                {vendor}
              </span>
              <span style={styles.content}>
                裝置類型：
                {device ? device.name : null }
              </span>
              <div style={styles.descriptionWrap}>
                <span style={[styles.content, { margin: 0 }]}>描述：</span>
                <div style={styles.descriptionBox}>
                  {description}
                </div>
              </div>
              <div style={styles.btnWrap}>
                <Button onClick={() => history.push(`/pnModel/${id}/edit`)} style={styles.goEditBtn} label="前往修改" />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  () => ({
  }),
  dispatch => bindActionCreators({
    ...PNModelActions,
  }, dispatch),
);

export default reduxHook(
  withRouter(
    radium(PNModelPopUp)
  )
);
