// @flow
import React, { Component } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  initialize,
  SubmissionError,
  formValueSelector,
} from 'redux-form';
import {
  graphql,
} from 'react-apollo';

import {
  CREATE_PNSUBCATEGORY,
  EDIT_PNSUBCATEGORY,
} from '../../mutations/PNSubCategory.js';
import {
  FETCH_BIGSUBCATEGORIES,
  FETCH_SINGLE_PNSUBCATEGORY,
  FETCH_SMALLSUBCATEGORIES,
} from '../../queries/PNTable.js';

import {
  FORM_PNMODEL_BIGSUBCATEGORY_CREATE_EDIT_FORM,
  FORM_PNMODEL_BIGSUBCATEGORY_LIST_SEARCHER,
  FORM_PNMODEL_SMALLSUBCATEGORY_LIST_SEARCHER,
} from '../../shared/form.js';
import { validatePNSubCategoryCreateEdit } from '../../helper/validator/PNSubCategoryCreateEdit.js';

import TextStyles from '../../styles/Text.js';

import {
  togglePNSubcategoryCreateBox,
} from '../../actions/PNModel.js';

import SubmitButton from '../Form/SubmitButton.jsx';
import FileInput from '../Form/FileInput.jsx';
import CheckboxYesNoGroup from '../Form/CheckboxYesNoGroup.jsx';
import Input from '../Form/Input.jsx';

const bigCategorySearcherSelector = formValueSelector(FORM_PNMODEL_BIGSUBCATEGORY_LIST_SEARCHER);
const smallCategorySearcherSelector = formValueSelector(FORM_PNMODEL_SMALLSUBCATEGORY_LIST_SEARCHER);

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 999,
  },
  box: {
    width: 600,
    height: 600,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    border: '1px solid #979797',
    backgroundColor: 'white',
    position: 'relative',
    padding: 40,
  },
  contentWrapper: {
    width: 520,
    height: 520,
    overflow: 'auto',
    display: 'inline-grid',
  },
  innerContentWrap: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  groupWrap: {
    margin: '10px 0',
  },
  closeBtn: {
    position: 'relative',
    cursor: 'pointer',
    border: 'none',
    outline: 'none',
    fontSize: 20,
    backgroundColor: 'transparent',
  },
  circle: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: 30,
    height: 30,
    borderRadius: '100%',
    border: '2px solid black',
    position: 'absolute',
    top: 20,
    right: 20,
  },
  btnWrap: {
    margin: '10px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  imgWrap: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgInnerWrap: {
    width: 129,
    height: 300,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  },
  goEditBtn: {
    backgroundColor: 'rgb(182, 184, 204)',
  },
  title: {
    ...TextStyles.labelText,
    width: '100%',
    height: 'auto',
    textAlign: 'center',
    fontSize: 18,
    padding: '18px 0',
  },
};

type Props = {
  toggleCreateBox: Function,
  createPNSubCategory: Function,
  handleSubmit: Function,
  currentSelectedPNCategory: Number,
  targetId: Number,
  initializeForm: Function,
  pnSubCategoryItem: Object,
  editPNSubCategory: Function,
  currentSelectedBigSubCategory: Number,
  isSmallCategory: Boolean,
};

class PNSubCategoryPopUp extends Component<Props> {
  componentDidMount() {
    const {
      pnSubCategoryItem,
      initializeForm,
    } = this.props;

    if (pnSubCategoryItem) {
      initializeForm({
        ...pnSubCategoryItem,
      });
    }
  }

  componentDidUpdate(prevProps) {
    const {
      pnSubCategoryItem,
      initializeForm,
    } = this.props;

    if (pnSubCategoryItem && pnSubCategoryItem !== prevProps.pnSubCategoryItem) {
      initializeForm({
        ...pnSubCategoryItem,
      });
    }
  }

  close() {
    const {
      toggleCreateBox,
    } = this.props;

    toggleCreateBox({ status: false });
  }

  async submit(d) {
    const errors = validatePNSubCategoryCreateEdit(d);

    if (errors) {
      throw new SubmissionError(errors);
    }

    const {
      createPNSubCategory,
      toggleCreateBox,
      currentSelectedPNCategory,
      editPNSubCategory,
      targetId,
      currentSelectedBigSubCategory,
      isSmallCategory,
    } = this.props;

    if (targetId) {
      const payload = {
        id: parseInt(targetId, 10),
        ...d,
      };

      const {
        data: {
          editPNSubCategory: {
            id,
          },
        },
      } = await editPNSubCategory(payload);

      if (id) {
        return toggleCreateBox(false);
      }

      return toggleCreateBox(false);
    }

    const payload = {
      ...d,
      PNCategoryId: parseInt(currentSelectedPNCategory, 10),
    };

    if (isSmallCategory) payload.PNSubCategoryId = parseInt(currentSelectedBigSubCategory, 10);

    const {
      data: {
        createPNSubCategory: {
          id,
        },
      },
    } = await createPNSubCategory(payload);

    if (id) {
      toggleCreateBox(false);
    }

    return toggleCreateBox(false);
  }

  render() {
    const {
      handleSubmit,
      targetId,
      isSmallCategory,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div
          style={styles.box}>
          <div style={styles.circle}>
            <button
              onClick={() => this.close()}
              style={styles.closeBtn}
              type="button">
              X
            </button>
          </div>
          <form
            onSubmit={handleSubmit(d => this.submit(d))}
            className="hideScrollBar"
            style={styles.contentWrapper}>
            <div style={styles.title}>
              {targetId ? '修改類別' : `新增${isSmallCategory ? '小' : ''}類別`}
            </div>
            <div style={styles.imgWrap}>
              <Field name="picture" component={FileInput} />
            </div>
            <div style={styles.innerContentWrap}>
              <div style={styles.groupWrap}>
                <Field
                  label="名稱:"
                  name="name"
                  component={Input} />
              </div>
              <div style={styles.groupWrap}>
                <Field
                  label="是否顯示在此料號在客製化網站:"
                  name="isOnSale"
                  component={CheckboxYesNoGroup} />
              </div>
              <div style={styles.btnWrap}>
                <SubmitButton
                  style={styles.editBtn}
                  label={targetId ? '確定修改' : '確定新增'} />
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_PNMODEL_BIGSUBCATEGORY_CREATE_EDIT_FORM,
});

const reduxHook = connect(
  state => ({
    currentSelectedPNCategory: state.PNModel.currentSelectedPNCategory,
    currentSelectedBigSubCategory: state.PNModel.currentSelectedBigSubCategory,
    isSmallCategory: state.PNModel.subCategoryCreateEditBoxOptions.isSmallCategory,
    targetId: state.PNModel.subCategoryCreateEditBoxOptions.targetId,
    selectedBigKeyword: bigCategorySearcherSelector(state, 'keyword'),
    selectedSmallKeyword: smallCategorySearcherSelector(state, 'keyword'),
  }),
  dispatch => bindActionCreators({
    toggleCreateBox: togglePNSubcategoryCreateBox,
    initializeForm: d => initialize(FORM_PNMODEL_BIGSUBCATEGORY_CREATE_EDIT_FORM, d),
  }, dispatch),
);

const queryHook = graphql(FETCH_SINGLE_PNSUBCATEGORY, {
  options: ownProps => ({
    variables: {
      id: (ownProps.targetId && parseInt(ownProps.targetId, 10)) || -1,
    },
  }),
  props: ({
    data: {
      pnSubCategoryItem,
    },
  }) => ({
    pnSubCategoryItem: pnSubCategoryItem || null,
  }),
});

const createMutationHook = graphql(CREATE_PNSUBCATEGORY, {
  props: ({ ownProps, mutate }: { mutate: Function }) => ({
    createPNSubCategory: ({
      name,
      picture,
      isOnSale,
      PNCategoryId,
      PNSubCategoryId,
    }) => mutate({
      variables: {
        name,
        picture,
        isOnSale,
        PNCategoryId,
        PNSubCategoryId,
      },
      update: (store, { data: { createPNSubCategory } }) => {
        const {
          currentSelectedBigSubCategory,
          currentSelectedPNCategory,
          isSmallCategory,
          selectedBigKeyword,
          selectedSmallKeyword,
        } = ownProps;

        const id = isSmallCategory ? currentSelectedBigSubCategory : currentSelectedPNCategory;
        const data = store.readQuery({
          query: isSmallCategory ? FETCH_SMALLSUBCATEGORIES : FETCH_BIGSUBCATEGORIES,
          variables: {
            id: (id && parseInt(id, 10)) || -1,
            keyword: isSmallCategory ? selectedSmallKeyword || null : selectedBigKeyword || null,
          },
        });

        if (isSmallCategory) {
          const {
            subCategoriesBySubCategory,
          } = data;

          if (createPNSubCategory) {
            store.writeQuery({
              query: FETCH_SMALLSUBCATEGORIES,
              variables: {
                id: (id && parseInt(id, 10)) || -1,
                keyword: selectedSmallKeyword || null,
              },
              data: {
                subCategoriesBySubCategory: [
                  ...subCategoriesBySubCategory,
                  {
                    ...createPNSubCategory,
                  },
                ],
              },
            });
          }

          return;
        }

        const {
          subCategoriesByPNCategory,
        } = data;

        if (createPNSubCategory) {
          store.writeQuery({
            query: FETCH_BIGSUBCATEGORIES,
            variables: {
              id: (id && parseInt(id, 10)) || -1,
              keyword: selectedBigKeyword || null,
            },
            data: {
              subCategoriesByPNCategory: [
                ...subCategoriesByPNCategory,
                {
                  ...createPNSubCategory,
                },
              ],
            },
          });
        }
      },
    }),
  }),
});

const editMutationHook = graphql(EDIT_PNSUBCATEGORY, {
  props: ({ ownProps, mutate }: { mutate: Function }) => ({
    editPNSubCategory: ({
      id,
      name,
      picture,
      isOnSale,
    }) => mutate({
      variables: {
        id,
        name,
        picture,
        isOnSale,
      },
      update: (store, { data: { editPNSubCategory } }) => {
        const {
          currentSelectedBigSubCategory,
          currentSelectedPNCategory,
          isSmallCategory,
          selectedBigKeyword,
          selectedSmallKeyword,
        } = ownProps;

        const categoryId = isSmallCategory
          ? currentSelectedBigSubCategory : currentSelectedPNCategory;

        const data = store.readQuery({
          query: isSmallCategory ? FETCH_SMALLSUBCATEGORIES : FETCH_BIGSUBCATEGORIES,
          variables: {
            id: (categoryId && parseInt(categoryId, 10)) || -1,
            keyword: isSmallCategory ? selectedSmallKeyword || null : selectedBigKeyword || null,
          },
        });

        if (isSmallCategory) {
          const {
            subCategoriesBySubCategory,
          } = data;

          if (editPNSubCategory) {
            const targetIdx = subCategoriesBySubCategory.findIndex(x => x.id === editPNSubCategory.id);

            if (~targetIdx) {
              store.writeQuery({
                query: FETCH_SMALLSUBCATEGORIES,
                variables: {
                  id: (categoryId && parseInt(categoryId, 10)) || -1,
                  keyword: selectedSmallKeyword || null,
                },
                data: {
                  subCategoriesBySubCategory: [
                    ...subCategoriesBySubCategory.slice(0, targetIdx),
                    editPNSubCategory,
                    ...subCategoriesBySubCategory.slice(targetIdx + 1),
                  ],
                },
              });
            }
          }

          return;
        }

        const {
          subCategoriesByPNCategory,
        } = data;

        if (editPNSubCategory) {
          const targetIdx = subCategoriesByPNCategory.findIndex(x => x.id === editPNSubCategory.id);

          if (~targetIdx) {
            store.writeQuery({
              query: FETCH_BIGSUBCATEGORIES,
              variables: {
                id: (
                  ownProps.currentSelectedPNCategory
                  && parseInt(ownProps.currentSelectedPNCategory, 10)
                ) || -1,
                keyword: selectedBigKeyword || null,
              },
              data: {
                subCategoriesByPNCategory: [
                  ...subCategoriesByPNCategory.slice(0, targetIdx),
                  editPNSubCategory,
                  ...subCategoriesByPNCategory.slice(targetIdx + 1),
                ],
              },
            });
          }
        }
      },
    }),
  }),
});

export const PNSubCategoryEditForm = reduxHook(
  queryHook(
    editMutationHook(
      formHook(
        radium(PNSubCategoryPopUp)
      )
    )
  )
);

export const PNSubCategoryCreateForm = reduxHook(
  createMutationHook(
    formHook(
      radium(PNSubCategoryPopUp)
    )
  )
);

export default null;
