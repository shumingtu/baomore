// @flow
import React, {
  PureComponent,
  Fragment,
} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import radium from 'radium';
import {
  reduxForm,
  Field,
  formValueSelector,
  change,
} from 'redux-form';
import {
  Query,
  Mutation,
} from 'react-apollo';
// config
import TextStyles from '../../styles/Text.js';
import { FORM_PNMODEL_PNCATEGORY_LIST_SEARCHER } from '../../shared/form.js';
import {
  FETCH_PNCATEGORYBYKEYWORD,
} from '../../queries/PNTable.js';
import {
  CREATE_PNCATEGORY,
  EDIT_PNCATEGORY,
} from '../../mutations/PNCategory.js';
// component
import Button from '../Global/Button.jsx';
import Input from '../Form/Input.jsx';
import StateInput from '../Global/Input.jsx';
import Table, { TableField } from '../Table/Table.jsx';
import EditableLabel from '../Table/Custom/EditableLabel.jsx';
import { wrapManageActionsIntoPNCategory } from '../Table/Actions/PNModelManageActions.jsx';

const PNModelManageActions = wrapManageActionsIntoPNCategory();

const selector = formValueSelector(FORM_PNMODEL_PNCATEGORY_LIST_SEARCHER);

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  groupWrapper: {
    width: 400,
    height: 'auto',
    padding: '24px 0 0 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  label: {
    ...TextStyles.labelText,
    fontSize: 16,
  },
  tableWrapper: {
    width: '100%',
    maxWidth: 400,
    height: 160,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  inputWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 12px 0 0',
  },
};

function TableMutationContext({
  render,
  selectedKeyword,
}: {
  selectedKeyword: String,
  render: Function,
}) {
  return (
    <Mutation
      mutation={EDIT_PNCATEGORY}
      update={(cache, { data: { editPNCategory } }) => {
        const {
          pnCategoriesByKeyword,
        } = cache.readQuery({
          query: FETCH_PNCATEGORYBYKEYWORD,
          variables: {
            keyword: selectedKeyword || null,
          },
        });

        const editingPNCategoryIdx = pnCategoriesByKeyword.findIndex(b => b.id === editPNCategory.id);

        if (~editingPNCategoryIdx) {
          cache.writeQuery({
            query: FETCH_PNCATEGORYBYKEYWORD,
            variables: {
              keyword: selectedKeyword || null,
            },
            data: {
              pnCategoriesByKeyword: [
                ...pnCategoriesByKeyword.slice(0, editingPNCategoryIdx),
                {
                  ...editPNCategory,
                },
                ...pnCategoriesByKeyword.slice(editingPNCategoryIdx + 1),
              ],
            },
          });
        }
      }}>
      {(editPNCategory, { loading }) => render(editPNCategory, { loading })}
    </Mutation>
  );
}

const TableMutationContextWithRedux = connect(
  state => ({
    selectedKeyword: state.PNModel.pnCategoryListKeyword,
  }),
)(TableMutationContext);

type Props = {
  selectedKeyword: String,
  setListKeyword: Function,
};

type State = {
  isCreating: boolean,
};

class PNCategoryListManage extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      isCreating: false,
    };
  }

  render() {
    const {
      isCreating,
    } = this.state;

    const {
      selectedKeyword,
      setListKeyword,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.groupWrapper}>
          <span style={styles.label}>
            料號類型
          </span>
          {isCreating ? (
            <Fragment>
              <div style={styles.inputWrapper}>
                <Mutation
                  mutation={CREATE_PNCATEGORY}
                  update={(cache, { data: { createPNCategory } }) => {
                    const {
                      pnCategoriesByKeyword,
                    } = cache.readQuery({
                      query: FETCH_PNCATEGORYBYKEYWORD,
                      variables: {
                        keyword: selectedKeyword || null,
                      },
                    });

                    cache.writeQuery({
                      query: FETCH_PNCATEGORYBYKEYWORD,
                      variables: {
                        keyword: selectedKeyword || null,
                      },
                      data: {
                        pnCategoriesByKeyword: [
                          ...pnCategoriesByKeyword,
                          createPNCategory,
                        ],
                      },
                    });
                  }}>
                  {createPNCategory => (
                    <StateInput
                      placeholder="請輸入欲新增的料號名稱"
                      onSubmit={async (n) => {
                        if (n) {
                          await createPNCategory({
                            variables: {
                              name: n,
                            },
                          });
                        }

                        this.setState({ isCreating: false });
                      }} />
                  )}
                </Mutation>
              </div>
              <Button
                label="取消"
                onClick={() => this.setState({ isCreating: false })} />
            </Fragment>
          ) : (
            <Button
              label="新增料號類型"
              onClick={() => this.setState({ isCreating: true })} />
          )}
        </div>
        <div style={styles.groupWrapper}>
          <Field
            name="keyword"
            label="關鍵字搜尋："
            placeholder="請輸入料號關鍵字"
            component={Input} />
        </div>
        <div style={styles.tableWrapper}>
          <Query
            variables={{ keyword: selectedKeyword || null }}
            query={FETCH_PNCATEGORYBYKEYWORD}>
            {({
              data: {
                pnCategoriesByKeyword = [],
              },
              refetch,
            }) => (
              <Table
                dataSource={pnCategoriesByKeyword}
                actionTitles={['操作']}
                getActions={() => [
                  <PNModelManageActions refetch={() => {
                    setListKeyword(null);
                    refetch();
                  }} />,
                ]}>
                <TableField name="流水號" fieldKey="id" flex={0.5} isCenter />
                <TableField
                  name="料號名稱"
                  fieldKey="name"
                  flex={1}
                  isCenter
                  MutationContext={TableMutationContextWithRedux}
                  Component={EditableLabel} />
              </Table>
            )}
          </Query>
        </div>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_PNMODEL_PNCATEGORY_LIST_SEARCHER,
});

const reduxHook = connect(
  state => ({
    selectedKeyword: selector(state, 'keyword'),
  }),
  dispatch => bindActionCreators({
    setListKeyword: v => change(FORM_PNMODEL_PNCATEGORY_LIST_SEARCHER, 'keyword', v),
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      PNCategoryListManage
    )
  )
);
