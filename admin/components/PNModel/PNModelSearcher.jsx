// @flow
import React, { Component } from 'react';
import radium from 'radium';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import qs from 'qs';
import {
  reduxForm,
  Field,
  Fields,
  formValueSelector,
  change,
} from 'redux-form';
import {
  Query,
} from 'react-apollo';

import {
  FORM_PNMODEL_SEARCHER,
} from '../../shared/form.js';

import {
  FETCH_PNCATEGORIES,
} from '../../queries/PNTable.js';

import {
  FETCH_VENDOR_LIST,
} from '../../queries/Global.js';

import * as PNModelActions from '../../actions/PNModel.js';

import Selector from '../Form/Selector.jsx';
import Input from '../Form/Input.jsx';
import SubmitButton from '../Form/SubmitButton.jsx';
import Button from '../Global/Button.jsx';
import CodeInput from '../Form/CodeInput.jsx';
import MaterialSearcherBind from '../Form/MaterialSearcherBind.jsx';
import ProtectorSearcherBind from '../Form/ProtectorSearcherBind.jsx';

const selector = formValueSelector(FORM_PNMODEL_SEARCHER);

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  searchWrapper: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    padding: '12px 0',
  },
  rowWrapper: {
    display: 'flex',
    width: '100%',
    alignItems: 'flex-start',
    flexDirection: 'column',
    justifyContent: 'center',
    margin: '0px 5px',
  },
  rowItem: {
    margin: '5px 0px',
  },
  searchBtn: {
    backgroundColor: 'rgb(182, 184, 204)',
    border: '1px solid rgb(182, 184, 204)',
    color: 'white',
    margin: '0 5px',
  },
  btnWrap: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  createBtn: {
    backgroundColor: 'rgb(182, 184, 204)',
    border: '1px solid rgb(182, 184, 204)',
    color: 'white',
    margin: '0 5px',
  },
};

type Props = {
  cachePNModelListSearchOption: Function,
  handleSubmit: Function,
  selectedPNCategoryId: Number,
  setBigSubCategorylId: Function,
  setSmallSubCategorylId: Function,
  setBrandId: Function,
  setModelId: Function,
  history: Object,
  options: Object,
};

class PNModelSearcher extends Component<Props> {
  componentDidUpdate(prevProps) {
    const {
      selectedPNCategoryId,
      setBrandId,
      setModelId,
      setSmallSubCategorylId,
      setBigSubCategorylId,
    } = this.props;

    if (selectedPNCategoryId && selectedPNCategoryId !== prevProps.selectedPNCategoryId) {
      setBrandId('-1');
      setModelId('-1');
      setSmallSubCategorylId('-1');
      setBigSubCategorylId('-1');
    }
  }

  submit(d) {
    const {
      cachePNModelListSearchOption,
    } = this.props;

    const {
      code,
      pnCategoryId,
      bigSubCategoryId,
      smallSubCategoryId,
      vendorId,
      keyword,
      brandId,
      modelId,
    } = d;

    const payload = {
      code: code ? `${code}` : null,
      pnCategoryId: pnCategoryId === '-1' ? null : parseInt(pnCategoryId, 10),
      bigSubCategoryId: bigSubCategoryId === '-1' ? null : parseInt(bigSubCategoryId, 10),
      smallSubCategoryId: smallSubCategoryId === '-1' ? null : parseInt(smallSubCategoryId, 10),
      brandId: brandId === '-1' ? null : parseInt(brandId, 10),
      modelId: modelId === '-1' ? null : parseInt(modelId, 10),
      vendorId: vendorId === '-1' ? null : parseInt(vendorId, 10),
      keyword: keyword ? `${keyword}` : null,
    };

    cachePNModelListSearchOption(payload);
  }

  render() {
    const {
      handleSubmit,
      selectedPNCategoryId,
      history,
      options,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <form onSubmit={handleSubmit(d => this.submit(d))} style={styles.searchWrapper}>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Field
                name="code"
                label="料號編號:"
                component={CodeInput} />
            </div>
            <div style={styles.rowItem}>
              <Query query={FETCH_PNCATEGORIES}>
                {({ data }) => {
                  if (!data) return null;
                  const {
                    pnCategories,
                  } = data;

                  return (
                    <Field
                      name="pnCategoryId"
                      label="料號類型:"
                      nullable
                      placeholder="請選擇"
                      options={pnCategories || []}
                      component={Selector} />
                  );
                }}
              </Query>
            </div>
            {parseInt(selectedPNCategoryId, 10) === 1 && (
              <Fields
                component={MaterialSearcherBind}
                pnCategoryId={selectedPNCategoryId}
                names={[
                  'bigSubCategoryId',
                  'smallSubCategoryId',
                ]} />
            )}
            {parseInt(selectedPNCategoryId, 10) === 2 && (
              <Fields
                component={ProtectorSearcherBind}
                names={[
                  'brandId',
                  'modelId',
                ]} />
            )}
          </div>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Query query={FETCH_VENDOR_LIST}>
                {({ data }) => {
                  if (!data) return null;
                  const {
                    vendorList,
                  } = data;

                  return (
                    <Field
                      name="vendorId"
                      label="廠商:"
                      nullable
                      placeholder="請選擇"
                      options={vendorList || []}
                      component={Selector} />
                  );
                }}
              </Query>
            </div>
            <div style={styles.rowItem}>
              <Field
                name="keyword"
                label="關鍵字:"
                component={Input} />
            </div>
            <div style={styles.rowItem}>
              <div style={styles.btnWrap}>
                <SubmitButton style={styles.searchBtn} label="查詢" />
                <Button style={styles.createBtn} onClick={() => history.push('/pnModel/create')} label="新增料號" />
                <Button
                  style={{ margin: '0px 0px 0px 5px' }}
                  onClick={() => {
                    const queryString = qs.stringify(qs.parse(options), { skipNulls: true });
                    window.open(`${API_HOST}/export/pnTable?${queryString}&token=${localStorage.accessToken}`);
                  }}
                  label="匯出" />
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_PNMODEL_SEARCHER,
});

const reduxHook = connect(
  state => ({
    selectedPNCategoryId: selector(state, 'pnCategoryId'),
    options: state.PNModel.listSearchOptions,
  }),
  dispatch => bindActionCreators({
    ...PNModelActions,
    setBigSubCategorylId: v => change(FORM_PNMODEL_SEARCHER, 'bigSubCategoryId', v),
    setSmallSubCategorylId: v => change(FORM_PNMODEL_SEARCHER, 'smallSubCategoryId', v),
    setBrandId: v => change(FORM_PNMODEL_SEARCHER, 'brandId', v),
    setModelId: v => change(FORM_PNMODEL_SEARCHER, 'modelId', v),
  }, dispatch)
);

export default reduxHook(
  withRouter(
    formHook(
      radium(PNModelSearcher)
    )
  )
);
