// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  formValueSelector,
  change,
} from 'redux-form';
import {
  Query,
} from 'react-apollo';
// config
import TextStyles from '../../styles/Text.js';
import { FORM_PNMODEL_BIGSUBCATEGORY_LIST_SEARCHER } from '../../shared/form.js';
import {
  FETCH_BIGSUBCATEGORIES,
} from '../../queries/PNTable.js';
import {
  togglePNSubcategoryCreateBox,
} from '../../actions/PNModel.js';
// component
import Button from '../Global/Button.jsx';
import Input from '../Form/Input.jsx';
import Table, { TableField } from '../Table/Table.jsx';
import { wrapManageActionsIntoBigSubCategory } from '../Table/Actions/PNModelManageActions.jsx';

const PNModelManageActions = wrapManageActionsIntoBigSubCategory();

const selector = formValueSelector(FORM_PNMODEL_BIGSUBCATEGORY_LIST_SEARCHER);

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  groupWrapper: {
    width: 400,
    height: 'auto',
    padding: '24px 0 0 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  label: {
    ...TextStyles.labelText,
    fontSize: 16,
  },
  tableWrapper: {
    width: '100%',
    maxWidth: 400,
    height: 160,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  inputWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 12px 0 0',
  },
};


type Props = {
  currentSelectedPNCategory: Number,
  toggleCreateBox: Function,
  selectedKeyword: String,
  setListKeyword: Function,
};

class BigSubCategoryListManage extends PureComponent<Props> {
  render() {
    const {
      currentSelectedPNCategory,
      toggleCreateBox,
      selectedKeyword,
      setListKeyword,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.groupWrapper}>
          <span style={styles.label}>
            大類
          </span>
          <Button
            disabled={!currentSelectedPNCategory}
            onClick={() => toggleCreateBox({ status: true })}
            label="新增大類" />
        </div>
        <div style={styles.groupWrapper}>
          <Field
            name="keyword"
            label="關鍵字搜尋："
            placeholder="請輸入大類關鍵字"
            component={Input} />
        </div>
        <div style={styles.tableWrapper}>
          <Query
            variables={{
              id: currentSelectedPNCategory || -1,
              keyword: selectedKeyword || null,
            }}
            query={FETCH_BIGSUBCATEGORIES}>
            {({
              data: {
                subCategoriesByPNCategory = [],
              },
              refetch,
            }) => (
              <Table
                dataSource={subCategoriesByPNCategory}
                showPlaceholder={!currentSelectedPNCategory}
                placeholder="尚未選擇料號類型"
                actionTitles={['操作']}
                getActions={() => [
                  <PNModelManageActions
                    refetch={() => {
                      setListKeyword(null);
                      refetch();
                    }}
                    isSubCategoryActions />,
                ]}>
                <TableField name="流水號" fieldKey="id" flex={0.5} isCenter />
                <TableField
                  name="料號名稱"
                  fieldKey="name"
                  flex={1}
                  isCenter />
              </Table>
            )}
          </Query>
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    currentSelectedPNCategory: state.PNModel.currentSelectedPNCategory,
    selectedKeyword: selector(state, 'keyword'),
  }),
  dispatch => bindActionCreators({
    toggleCreateBox: togglePNSubcategoryCreateBox,
    setListKeyword: v => change(FORM_PNMODEL_BIGSUBCATEGORY_LIST_SEARCHER, 'keyword', v),
  }, dispatch),
);

const formHook = reduxForm({
  form: FORM_PNMODEL_BIGSUBCATEGORY_LIST_SEARCHER,
});

export default reduxHook(
  formHook(
    radium(
      BigSubCategoryListManage
    )
  )
);
