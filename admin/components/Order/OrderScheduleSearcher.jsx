// @flow
import React, { Component } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  initialize,
} from 'redux-form';
import {
  Query,
  graphql,
} from 'react-apollo';

import {
  FORM_ORDER_SCHEDULE_SEARCHER,
} from '../../shared/form.js';

import {
  FETCH_STORES_LIST,
} from '../../queries/Store.js';

import { GET_EMPLOYEE_LIST, GET_MEMBER_ME } from '../../queries/Member.js';

import * as OrderActions from '../../actions/Order.js';
import { getMyRoles } from '../../helper/roles.js';

import Selector from '../Form/Selector.jsx';
import Input from '../Form/Input.jsx';
import SubmitButton from '../Form/SubmitButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  searchWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    padding: '12px 0',
  },
  rowWrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    margin: '5px 0',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'flex-start',
    },
  },
  rowItem: {
    margin: '0 5px',
  },
  searchBtnWrapper: {
    flex: 1,
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    '@media (max-width: 767px)': {
      width: '100%',
      margin: '12px 0',
    },
  },
  searchBtn: {
    backgroundColor: 'rgb(182, 184, 204)',
    border: '1px solid rgb(182, 184, 204)',
    color: 'white',
  },
};

type Props = {
  cacheOrderScheduleSearchOption: Function,
  handleSubmit: Function,
  initializeForm: Function,
  options: {
    startDate: string,
    endDate: string,
    bookedMemberId: number,
    storeId: number,
  },
  me: object,
};

class OrderScheduleSearcher extends Component<Props> {
  componentDidMount() {
    const {
      options,
      initializeForm,
    } = this.props;

    if (options) {
      initializeForm({
        startDate: options.startDate || null,
        endDate: options.endDate || null,
        bookedMemberId: options.bookedMemberId || '-1',
        storeId: options.bookedMemberId || '-1',
      });
    }
  }

  submit(d) {
    const {
      cacheOrderScheduleSearchOption,
    } = this.props;

    const {
      startDate,
      endDate,
      bookedMemberId,
      storeId,
    } = d;

    cacheOrderScheduleSearchOption({
      startDate: startDate === '-1' ? null : startDate,
      endDate: endDate === '-1' ? null : endDate,
      bookedMemberId: bookedMemberId === '-1' ? null : parseInt(bookedMemberId, 10),
      storeId: storeId === '-1' ? null : parseInt(storeId, 10),
    });
  }

  render() {
    const {
      handleSubmit,
      me,
    } = this.props;

    const roles = getMyRoles();
    const isAdmin = roles.includes('ADMIN_MEMBER_MANAGE');

    return (
      <div style={styles.wrapper}>
        <form onSubmit={handleSubmit(d => this.submit(d))} style={styles.searchWrapper}>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Field
                name="startDate"
                label="開始時間:"
                type="date"
                noDefaultValue
                component={Input} />
            </div>
            <div style={styles.rowItem}>
              <Field
                name="endDate"
                label="結束時間:"
                type="date"
                noDefaultValue
                component={Input} />
            </div>
          </div>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Query query={FETCH_STORES_LIST}>
                {({ data }) => {
                  if (!data) return null;

                  const {
                    stores,
                  } = data;

                  return (
                    <Field
                      name="storeId"
                      label="門市:"
                      nullable
                      placeholder="請選擇"
                      options={stores || []}
                      component={Selector} />
                  );
                }}
              </Query>
            </div>
          </div>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Query
                variables={{
                  areaId: (!isAdmin && me && me.AreaId && parseInt(me.AreaId, 10)) || null,
                  lineIdRequired: true,
                }}
                query={GET_EMPLOYEE_LIST}>
                {({ data }) => {
                  if (!data) return null;
                  const {
                    employeeMemberlist,
                  } = data;

                  return (
                    <Field
                      name="bookedMemberId"
                      label="包膜師:"
                      nullable
                      placeholder="請選擇"
                      options={employeeMemberlist || []}
                      component={Selector} />
                  );
                }}
              </Query>
            </div>
            <div style={styles.searchBtnWrapper}>
              <SubmitButton style={styles.searchBtn} label="查詢" />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_ORDER_SCHEDULE_SEARCHER,
});

const reduxHook = connect(
  state => ({
    options: state.Order.scheduleSearchOptions,
  }),
  dispatch => bindActionCreators({
    initializeForm: v => initialize(FORM_ORDER_SCHEDULE_SEARCHER, v),
    ...OrderActions,
  }, dispatch)
);

const queryHook = graphql(GET_MEMBER_ME, {
  props: ({
    data: {
      me,
    },
  }) => ({
    me: me || null,
  }),
});

export default reduxHook(
  queryHook(
    formHook(
      radium(OrderScheduleSearcher)
    )
  )
);
