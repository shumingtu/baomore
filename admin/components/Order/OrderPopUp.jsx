// @flow
import React, { Component, Fragment } from 'react';
import radium from 'radium';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Mutation } from 'react-apollo';

import TextStyles from '../../styles/Text.js';
import { CANCEL_SCHEDULE } from '../../mutations/Order.js';
import { cancelScheduleUpdate } from '../../helper/schedule.js';

import * as OrderActions from '../../actions/Order.js';
import Button from '../Global/Button.jsx';
import Error from '../Form/Error.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    left: 0,
    top: 0,
    zIndex: 999,
    '@media (max-width: 767px)': {
      position: 'fixed',
    },
  },
  box: {
    width: 600,
    height: 600,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    border: '1px solid #979797',
    backgroundColor: 'white',
    position: 'relative',
    padding: 40,
    '@media (max-width: 767px)': {
      width: '90vw',
      height: '60vh',
      padding: '36px 24px',
    },
  },
  contentWrapper: {
    width: 520,
    height: 520,
    overflow: 'auto',
    display: 'inline-grid',
    '@media (max-width: 767px)': {
      width: 'auto',
      height: '100%',
    },
  },
  contentOuterWrap: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  contentInnerWrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    width: '100%',
  },
  contentItem: {
    width: 280,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      width: '100%',
    },
  },
  content: {
    margin: '5px 0',
    ...TextStyles.text,
    fontSize: 14,
  },
  separateLine: {
    width: '100%',
    height: 2,
    backgroundColor: '#a7d3e1',
    margin: 24,
  },
  closeBtn: {
    position: 'relative',
    cursor: 'pointer',
    border: 'none',
    outline: 'none',
    fontSize: 20,
    backgroundColor: 'transparent',
  },
  circle: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: 30,
    height: 30,
    borderRadius: '100%',
    border: '2px solid black',
    position: 'absolute',
    top: 20,
    right: 20,
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '12px 0',
  },
  functionBtnWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 6px',
  },
};

type Props = {
  data: Object,
  openScheduleBox: Function,
  history: {
    push: Function,
  },
  scheduleSearchOptions: {
    startDate: string,
    endDate: string,
    bookedMemberId: number,
    storeId: number,
  },
};

class OrderPopUp extends Component<Props> {
  close() {
    const {
      openScheduleBox,
    } = this.props;

    openScheduleBox(null);
  }

  render() {
    const {
      data,
      history,
      scheduleSearchOptions,
    } = this.props;

    if (!data || !data.length) return null;

    return (
      <div style={styles.wrapper}>
        <div
          style={styles.box}>
          <div style={styles.circle}>
            <button
              onClick={() => this.close()}
              style={styles.closeBtn}
              type="button">
              X
            </button>
          </div>
          <div className="hideScrollBar" style={styles.contentWrapper}>
            <div style={styles.contentOuterWrap}>
              {data.map((order, idx) => (
                <div key={order.id} style={styles.contentInnerWrapper}>
                  <div style={styles.contentItem}>
                    <span style={styles.content}>
                      訂購人姓名：
                      {order.name}
                    </span>
                    <span style={styles.content}>
                      訂購人電話：
                      {order.phone}
                    </span>
                    <span style={styles.content}>
                      訂購人Email：
                      {order.email}
                    </span>
                    <span style={styles.content}>
                      預約門市：
                      {order.store && order.store.name}
                    </span>
                    <span style={styles.content}>
                      預約時間：
                      {`${order.date} ${order.time}`}
                    </span>
                    <span style={styles.content}>
                      包膜師：
                      {order.employee && order.employee.name}
                    </span>
                  </div>
                  <Mutation
                    mutation={CANCEL_SCHEDULE}
                    update={(cache, { data: { cancelSchedule } }) => cancelScheduleUpdate({
                      cache,
                      newSchedule: cancelSchedule,
                      searchOptions: scheduleSearchOptions,
                    })}>
                    {(cancelSchedule, { loading, error }) => (
                      <Fragment>
                        {error ? (
                          <Error error={error} />
                        ) : null}
                        <div style={styles.functionWrapper}>
                          <div style={styles.functionBtnWrapper}>
                            <Button
                              label="修改"
                              onClick={() => history.push(`/order/schedule/${order.id}/edit`)} />
                          </div>
                          <div style={styles.functionBtnWrapper}>
                            <Button
                              disabled={loading}
                              isWhiteBg
                              label="取消行程"
                              onClick={async () => {
                                if (confirm('是否確定取消？')) {
                                  const mutateProps = await cancelSchedule({
                                    variables: {
                                      onlineOrderId: order.id,
                                    },
                                  });

                                  if (mutateProps.data
                                    && mutateProps.data.cancelSchedule
                                    && mutateProps.data.cancelSchedule.id
                                  ) {
                                    this.close();
                                  }
                                }
                              }} />
                          </div>
                        </div>
                      </Fragment>
                    )}
                  </Mutation>
                  {idx === data.length - 1 ? null : (
                    <div style={styles.separateLine} />
                  )}
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    scheduleSearchOptions: state.Order.scheduleSearchOptions,
  }),
  dispatch => bindActionCreators({
    ...OrderActions,
  }, dispatch),
);

export default withRouter(
  reduxHook(
    radium(OrderPopUp)
  )
);
