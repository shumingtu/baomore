// @flow
import React, { Component } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  initialize,
  Fields,
  SubmissionError,
} from 'redux-form';
import { Mutation } from 'react-apollo';

import Input from '../Form/Input.jsx';
import SubmitButton from '../Form/SubmitButton.jsx';
import StoreDistrictChannel from '../Form/StoreDistritctChannel.jsx';

import TextStyles from '../../styles/Text.js';
import * as OrderActions from '../../actions/Order.js';
import { FORM_ORDER_EDIT_FORM } from '../../shared/form.js';
import { EDIT_ORDER_BY_ADMIN } from '../../mutations/Order.js';
import { validateOrderEditForm } from '../../helper/validator/OrderEditValidator.js';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 999,
  },
  box: {
    width: 390,
    height: 480,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    border: '1px solid #979797',
    backgroundColor: 'white',
    position: 'relative',
    padding: 40,
  },
  closeBtn: {
    position: 'relative',
    cursor: 'pointer',
    border: 'none',
    outline: 'none',
    fontSize: 20,
    backgroundColor: 'transparent',
  },
  circle: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: 30,
    height: 30,
    borderRadius: '100%',
    border: '2px solid black',
    position: 'absolute',
    top: 20,
    right: 20,
  },
  contentWrapper: {
    width: 310,
    height: 400,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  contentBox: {
    width: '100%',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  content: {
    margin: '10px 0',
    ...TextStyles.text,
    fontSize: 14,
    padding: '4px 5px',
  },
  submitBtnWrap: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '5px 0',
  },
};

type Props = {
  toggleOrderEditBox: Function,
  data: Object,
  initializeForm: Function,
  handleSubmit: Function,
};

class OrderEditPopUp extends Component<Props> {
  componentDidMount() {
    const {
      data,
      initializeForm,
    } = this.props;

    if (!data) return;

    const {
      channelId,
      districtId,
      storeId,
      quantity,
    } = data;

    initializeForm({
      channelId,
      districtId,
      storeId,
      quantity,
    });
  }

  close() {
    const {
      toggleOrderEditBox,
    } = this.props;

    toggleOrderEditBox(null);
  }

  async submit(d, mutate) {
    const errors = validateOrderEditForm(d);

    if (errors) {
      throw new SubmissionError(errors);
    }

    const {
      storeId,
      quantity,
    } = d;

    const {
      data,
    } = this.props;

    await mutate({
      variables: {
        orderId: parseInt(data.orderId, 10),
        quantity: parseInt(quantity, 10),
        storeId: parseInt(storeId, 10),
      },
    });

    this.close();
  }

  render() {
    const {
      data,
      handleSubmit,
    } = this.props;

    if (!data) return null;

    const {
      code,
      pnTableName,
      pnCategory,
    } = data;

    return (
      <div style={styles.wrapper}>
        <div
          style={styles.box}>
          <div style={styles.circle}>
            <button
              onClick={() => this.close()}
              style={styles.closeBtn}
              type="button">
              X
            </button>
          </div>
          <Mutation
            mutation={EDIT_ORDER_BY_ADMIN}>
            {editOrderByAdmin => (
              <form
                onSubmit={handleSubmit(d => this.submit(d, editOrderByAdmin))}
                style={styles.contentWrapper}>
                <h1>訂單修改</h1>
                <div style={styles.contentBox}>
                  <span style={styles.content}>
                    料號編號：
                    {code}
                  </span>
                  <span style={styles.content}>
                    料號類別：
                    {pnCategory}
                  </span>
                  <span style={styles.content}>
                    品名：
                    {pnTableName}
                  </span>
                  <Fields
                    names={[
                      'channelId',
                      'districtId',
                      'storeId',
                    ]}
                    component={StoreDistrictChannel} />
                  <Field
                    label="進貨數量:"
                    type="number"
                    name="quantity"
                    component={Input} />
                  <div style={styles.submitBtnWrap}>
                    <SubmitButton
                      label="確定" />
                  </div>
                </div>
              </form>
            )}
          </Mutation>
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  () => ({
  }),
  dispatch => bindActionCreators({
    initializeForm: d => initialize(FORM_ORDER_EDIT_FORM, d),
    ...OrderActions,
  }, dispatch),
);

const formHook = reduxForm({
  form: FORM_ORDER_EDIT_FORM,
});

export default reduxHook(
  formHook(
    radium(OrderEditPopUp)
  )
);
