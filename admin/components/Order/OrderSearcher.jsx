// @flow
import React, { Component } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  formValueSelector,
  change,
} from 'redux-form';
import {
  Query,
} from 'react-apollo';

import {
  FORM_ORDER_SEARCHER,
} from '../../shared/form.js';

import {
  FETCH_STORES_LIST,
} from '../../queries/Store.js';

import { GET_EMPLOYEE_LIST } from '../../queries/Member.js';
import * as OrderActions from '../../actions/Order.js';

import {
  shipStatus,
  payStatus,
} from '../../shared/order.js';

import Selector from '../Form/Selector.jsx';
import Input from '../Form/Input.jsx';
import SubmitButton from '../Form/SubmitButton.jsx';

const selector = formValueSelector(FORM_ORDER_SEARCHER);

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  searchWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    padding: '12px 0',
  },
  rowWrapper: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: '5px 0',
  },
  rowItem: {
    margin: '0 5px',
  },
  searchBtn: {
    backgroundColor: 'rgb(182, 184, 204)',
    border: '1px solid rgb(182, 184, 204)',
    color: 'white',
  },
};

type Props = {
  cacheOrderListSearchOption: Function,
  handleSubmit: Function,
  selectedStoreId: String,
  setBookedMemberId: Function,
};

class OrderSearcher extends Component<Props> {
  componentDidUpdate(prevProps) {
    const {
      selectedStoreId,
      setBookedMemberId,
    } = this.props;

    if (prevProps.selectedStoreId !== selectedStoreId) {
      setBookedMemberId('-1');
    }
  }

  submit(d) {
    const {
      cacheOrderListSearchOption,
    } = this.props;

    const {
      startDate,
      endDate,
      bookedMemberId,
      shippingStatus,
      storeId,
      keyword,
      payedStatus,
    } = d;

    cacheOrderListSearchOption({
      startDate: startDate === '-1' ? null : startDate,
      endDate: endDate === '-1' ? null : endDate,
      bookedMemberId: bookedMemberId === '-1' ? null : parseInt(bookedMemberId, 10),
      shippingStatus: shippingStatus === '-1' ? null : shippingStatus,
      storeId: storeId === '-1' ? null : parseInt(storeId, 10),
      keyword: keyword ? `${keyword}` : null,
      payedStatus: payedStatus === '-1' ? null : payedStatus,
    });
  }

  render() {
    const {
      handleSubmit,
      selectedStoreId,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <form onSubmit={handleSubmit(d => this.submit(d))} style={styles.searchWrapper}>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Field
                name="startDate"
                label="開始時間:"
                type="date"
                component={Input} />
            </div>
            <div style={styles.rowItem}>
              <Field
                name="endDate"
                label="結束時間:"
                type="date"
                component={Input} />
            </div>
          </div>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Query query={FETCH_STORES_LIST}>
                {({ data }) => {
                  if (!data) return null;

                  const {
                    stores,
                  } = data;

                  return (
                    <Field
                      name="storeId"
                      label="門市:"
                      nullable
                      placeholder="請選擇"
                      options={stores || []}
                      component={Selector} />
                  );
                }}
              </Query>
            </div>
            <div style={styles.rowItem}>
              <Field
                name="shippingStatus"
                label="出貨狀態:"
                nullable
                placeholder="請選擇"
                options={shipStatus}
                component={Selector} />
            </div>
          </div>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Query
                variables={{ storeId: selectedStoreId ? parseInt(selectedStoreId, 10) : -1 }}
                query={GET_EMPLOYEE_LIST}>
                {({ data }) => {
                  if (!data) return null;
                  const {
                    employeeMemberlist,
                  } = data;

                  return (
                    <Field
                      name="bookedMemberId"
                      label="包膜師:"
                      nullable
                      placeholder="請選擇"
                      options={employeeMemberlist || []}
                      component={Selector} />
                  );
                }}
              </Query>
            </div>
            <div style={styles.rowItem}>
              <Field
                name="keyword"
                placeholder="訂購人姓名、電話、Email"
                label="關鍵字:"
                component={Input} />
            </div>
          </div>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Field
                name="payedStatus"
                label="付款狀態:"
                nullable
                placeholder="請選擇"
                options={payStatus}
                component={Selector} />
            </div>
            <div style={styles.rowItem}>
              <SubmitButton style={styles.searchBtn} label="查詢" />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_ORDER_SEARCHER,
});

const reduxHook = connect(
  state => ({
    selectedStoreId: selector(state, 'storeId'),
  }),
  dispatch => bindActionCreators({
    setBookedMemberId: v => change(FORM_ORDER_SEARCHER, 'bookedMemberId', v),
    ...OrderActions,
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(OrderSearcher)
  )
);
