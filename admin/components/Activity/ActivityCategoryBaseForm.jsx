// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  initialize,
  SubmissionError,
} from 'redux-form';

// config
import TextStyles from '../../styles/Text.js';
import {
  FORM_ACTIVITY_CATEGORY_CREATE_EDIT_FORM,
} from '../../shared/form.js';
// components
import FormTitle from '../Global/FormTitle.jsx';
import FileInput from '../Form/FileInput.jsx';
import SubmitButton from '../Form/SubmitButton.jsx';
import Button from '../Global/Button.jsx';
import Input from '../Form/Input.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 12,
  },
  formWrapper: {
    width: 650,
    height: 'auto',
    padding: '16px 32px',
    backgroundColor: '#fff',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  label: {
    ...TextStyles.labelText,
  },
  inputWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  desktopImgWrapper: {
    width: 480,
    height: 134,
  },
  mobileImgWrapper: {
    width: 200,
    height: 290,
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  buttonWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 6px',
  },
};

type Props = {
  handleSubmit: Function,
  initializeForm: Function,
  history: {
    push: Function,
  },
  data: {
    id: number,
    name: string,
    desktopImg: string,
    mobileImg: string,
  },
  mutate: Function,
};

class ActivityCategoryBaseForm extends PureComponent<Props> {
  componentDidMount() {
    const {
      data,
    } = this.props;

    this.initializing(data);
  }

  componentDidUpdate(prevProps) {
    const {
      data,
    } = this.props;

    if (data !== prevProps.data && data) {
      this.initializing(data);
    }
  }

  getFields(d) {
    const {
      name,
      desktopImg,
      mobileImg,
    } = d;

    if (!name) {
      throw new SubmissionError({
        name: 'failed',
      });
    }

    if (!desktopImg || desktopImg === 'uploading') {
      throw new SubmissionError({
        desktopImg: 'failed',
      });
    }

    if (!mobileImg || mobileImg === 'uploading') {
      throw new SubmissionError({
        mobileImg: 'failed',
      });
    }

    return ({
      name,
      desktopImg,
      mobileImg,
    });
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    initializeForm({
      name: (obj && obj.name) || '',
      desktopImg: (obj && obj.desktopImg) || '',
      mobileImg: (obj && obj.mobileImg) || '',
    });
  }

  render() {
    const {
      handleSubmit,
      history,
      data,
      mutate,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <FormTitle
          title={data ? '修改活動分類' : '新增活動分類'} />
        <form
          style={styles.formWrapper}
          onSubmit={handleSubmit(async (d) => {
            const {
              name,
              desktopImg,
              mobileImg,
            } = this.getFields(d);

            if (!data) {
              await mutate({
                variables: {
                  name,
                  desktopImg,
                  mobileImg,
                },
              });
            } else {
              await mutate({
                variables: {
                  id: data && data.id ? parseInt(data.id, 10) : null,
                  name,
                  desktopImg,
                  mobileImg,
                },
              });
            }

            return history.push('/activity/official');
          })}>
          <div style={styles.fieldWrapper}>
            <Field
              name="name"
              label="分類名稱："
              placeholder="分類名稱"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <span style={styles.label}>
              桌面版圖片：
            </span>
            <div style={styles.inputWrapper}>
              <Field
                name="desktopImg"
                annotation="圖片尺寸：2100x1400"
                component={FileInput}
                style={styles.desktopImgWrapper} />
            </div>
          </div>
          <div style={styles.fieldWrapper}>
            <span style={styles.label}>
              手機版圖片：
            </span>
            <div style={styles.inputWrapper}>
              <Field
                name="mobileImg"
                annotation="圖片尺寸：1600x2400"
                component={FileInput}
                style={styles.mobileImgWrapper} />
            </div>
          </div>
          <div style={styles.functionWrapper}>
            <div style={styles.buttonWrapper}>
              <SubmitButton label="送出" />
            </div>
            <div style={styles.buttonWrapper}>
              <Button
                label="返回"
                onClick={() => history.push('/activity/official')} />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_ACTIVITY_CATEGORY_CREATE_EDIT_FORM,
});

const reduxHook = connect(
  () => ({}),
  dispatch => bindActionCreators({
    initializeForm: v => initialize(FORM_ACTIVITY_CATEGORY_CREATE_EDIT_FORM, v),
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      ActivityCategoryBaseForm
    )
  )
);
