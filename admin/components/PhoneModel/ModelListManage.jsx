// @flow
import React, {
  PureComponent,
  Fragment,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import {
  reduxForm,
  Field,
} from 'redux-form';
import {
  Query,
  Mutation,
} from 'react-apollo';
// config
import TextStyles from '../../styles/Text.js';
import { FORM_PHONE_MODEL_LIST_SEARCHER } from '../../shared/form.js';
import {
  FETCH_BRAND_MODEL_LIST,
} from '../../queries/Brand.js';
import {
  EDIT_BRAND_MODEL,
  CREATE_BRAND_MODEL,
} from '../../mutations/Brand.js';
// component
import Button from '../Global/Button.jsx';
import Input from '../Form/Input.jsx';
import StateInput from '../Global/Input.jsx';
import Table, { TableField } from '../Table/Table.jsx';
import EditableLabel from '../Table/Custom/EditableLabel.jsx';
import { wrapManageActionsIntoModel } from '../Table/Actions/PhoneModelManageActions.jsx';

const PhoneModelManageActions = wrapManageActionsIntoModel();

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  groupWrapper: {
    width: 400,
    height: 'auto',
    padding: '24px 0 0 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  label: {
    ...TextStyles.labelText,
    fontSize: 16,
  },
  tableWrapper: {
    width: '100%',
    maxWidth: 400,
    height: 240,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  inputWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 12px 0 0',
  },
};

function TableMutationContext({
  currentSelectedBrand,
  render,
}: {
  currentSelectedBrand: Number,
  render: Function,
}) {
  return (
    <Mutation
      mutation={EDIT_BRAND_MODEL}
      update={(cache, { data: { editBrandModel } }) => {
        const {
          brandModelList,
        } = cache.readQuery({
          query: FETCH_BRAND_MODEL_LIST,
          variables: {
            brandId: currentSelectedBrand,
          },
        });

        const editingIdx = brandModelList.findIndex(b => b.id === editBrandModel.id);

        if (~editingIdx) {
          cache.writeQuery({
            query: FETCH_BRAND_MODEL_LIST,
            variables: {
              brandId: currentSelectedBrand,
            },
            data: {
              brandModelList: [
                ...brandModelList.slice(0, editingIdx),
                editBrandModel,
                ...brandModelList.slice(editingIdx + 1),
              ],
            },
          });
        }
      }}>
      {(editBrandModel, { loading }) => render(editBrandModel, { loading })}
    </Mutation>
  );
}

const TableMutationContextWithRedux = connect(
  state => ({
    currentSelectedBrand: state.PhoneModel.currentSelectedBrand,
  }),
)(TableMutationContext);

type Props = {
  currentSelectedBrand: number,
};

type State = {
  isCreating: boolean,
};

class ModelListManage extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      isCreating: false,
      keyword: null,
    };
  }

  render() {
    const {
      currentSelectedBrand,
    } = this.props;

    const {
      isCreating,
      keyword,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        <div style={styles.groupWrapper}>
          <span style={styles.label}>
            型號
          </span>
          {isCreating && currentSelectedBrand ? (
            <Fragment>
              <div style={styles.inputWrapper}>
                <Mutation
                  mutation={CREATE_BRAND_MODEL}
                  update={(cache, { data: { createBrandModel } }) => {
                    const {
                      brandModelList,
                    } = cache.readQuery({
                      query: FETCH_BRAND_MODEL_LIST,
                      variables: {
                        brandId: currentSelectedBrand,
                        keyword,
                      },
                    });

                    cache.writeQuery({
                      query: FETCH_BRAND_MODEL_LIST,
                      variables: {
                        brandId: currentSelectedBrand,
                        keyword,
                      },
                      data: {
                        brandModelList: [
                          ...brandModelList,
                          createBrandModel,
                        ],
                      },
                    });
                  }}>
                  {createBrandModel => (
                    <StateInput
                      placeholder="請輸入欲新增型號名稱"
                      onSubmit={async (n) => {
                        if (n) {
                          await createBrandModel({
                            variables: {
                              name: n,
                              brandId: currentSelectedBrand,
                            },
                          });
                        }

                        this.setState({ isCreating: false });
                      }} />
                  )}
                </Mutation>
              </div>
              <Button
                label="取消"
                onClick={() => this.setState({ isCreating: false })} />
            </Fragment>
          ) : (
            <Button
              label="新增型號"
              disabled={!currentSelectedBrand}
              onClick={() => this.setState({ isCreating: true })} />
          )}
        </div>
        <div style={styles.groupWrapper}>
          <Field
            name="modelId"
            label="關鍵字搜尋："
            placeholder="請輸入型號關鍵字"
            onSubmit={(v) => { this.setState({ keyword: v }); }}
            component={Input} />
        </div>
        <div style={styles.tableWrapper}>
          <Query
            query={FETCH_BRAND_MODEL_LIST}
            variables={{
              brandId: currentSelectedBrand || -1,
              keyword,
            }}>
            {({
              data: {
                brandModelList = [],
              },
            }) => (
              <Table
                dataSource={brandModelList}
                showPlaceholder={!currentSelectedBrand}
                placeholder="尚未選擇手機品牌"
                actionTitles={['操作']}
                getActions={() => [
                  <PhoneModelManageActions />,
                ]}>
                <TableField name="流水號" fieldKey="id" flex={0.5} isCenter />
                <TableField
                  name="型號名稱"
                  fieldKey="name"
                  flex={1}
                  isCenter
                  MutationContext={TableMutationContextWithRedux}
                  Component={EditableLabel} />
              </Table>
            )}
          </Query>
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    currentSelectedBrand: state.PhoneModel.currentSelectedBrand,
  }),
);

const formHook = reduxForm({
  form: FORM_PHONE_MODEL_LIST_SEARCHER,
});

export default reduxHook(
  formHook(
    radium(
      ModelListManage
    )
  )
);
