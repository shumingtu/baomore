// @flow
import React, {
  PureComponent,
  Fragment,
} from 'react';
import radium from 'radium';
import {
  reduxForm,
  Field,
} from 'redux-form';
import {
  Query,
  Mutation,
} from 'react-apollo';
// config
import TextStyles from '../../styles/Text.js';
import { FORM_PHONE_BRAND_LIST_SEARCHER } from '../../shared/form.js';
import {
  FETCH_BRAND_LIST,
} from '../../queries/Brand.js';
import {
  EDIT_BRAND,
  CREATE_BRAND,
} from '../../mutations/Brand.js';
// component
import Button from '../Global/Button.jsx';
import Input from '../Form/Input.jsx';
import StateInput from '../Global/Input.jsx';
import Table, { TableField } from '../Table/Table.jsx';
import EditableLabel from '../Table/Custom/EditableLabel.jsx';
import { wrapManageActionsIntoBrand } from '../Table/Actions/PhoneModelManageActions.jsx';

const PhoneModelManageActions = wrapManageActionsIntoBrand();

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  groupWrapper: {
    width: 400,
    height: 'auto',
    padding: '24px 0 0 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  label: {
    ...TextStyles.labelText,
    fontSize: 16,
  },
  tableWrapper: {
    width: '100%',
    maxWidth: 400,
    height: 160,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  inputWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 12px 0 0',
  },
};

function TableMutationContext({
  render,
  data,
}: {
  render: Function,
  data: object,
}) {
  return (
    <Mutation
      mutation={EDIT_BRAND}
      update={(cache, { data: { editBrand } }) => {
        const {
          brandList,
        } = cache.readQuery({
          query: FETCH_BRAND_LIST,
          variables: {
            keyword: data.keyword,
          },
        });

        const editingBrandIdx = brandList.findIndex(b => b.id === editBrand.id);


        if (~editingBrandIdx) {
          cache.writeQuery({
            query: FETCH_BRAND_LIST,
            variables: {
              keyword: data.keyword,
            },
            data: {
              brandList: [
                ...brandList.slice(0, editingBrandIdx),
                editBrand,
                ...brandList.slice(editingBrandIdx + 1),
              ],
            },
          });
        }
      }}>
      {(editBrand, { loading }) => render(editBrand, { loading })}
    </Mutation>
  );
}

type Props = {

};

type State = {
  isCreating: boolean,
};

class BrandListManage extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      isCreating: false,
      keyword: null,
    };
  }

  render() {
    const {
      isCreating,
      keyword,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        <div style={styles.groupWrapper}>
          <span style={styles.label}>
            品牌
          </span>
          {isCreating ? (
            <Fragment>
              <div style={styles.inputWrapper}>
                <Mutation
                  mutation={CREATE_BRAND}
                  update={(cache, { data: { createBrand } }) => {
                    const {
                      brandList,
                    } = cache.readQuery({
                      query: FETCH_BRAND_LIST,
                      variables: {
                        keyword,
                      },
                    });

                    cache.writeQuery({
                      query: FETCH_BRAND_LIST,
                      variables: {
                        keyword,
                      },
                      data: {
                        brandList: [
                          ...brandList,
                          createBrand,
                        ],
                      },
                    });
                  }}>
                  {createBrand => (
                    <StateInput
                      placeholder="請輸入欲新增品牌名稱"
                      onSubmit={async (n) => {
                        if (n) {
                          await createBrand({
                            variables: {
                              name: n,
                            },
                          });
                        }

                        this.setState({ isCreating: false });
                      }} />
                  )}
                </Mutation>
              </div>
              <Button
                label="取消"
                onClick={() => this.setState({ isCreating: false })} />
            </Fragment>
          ) : (
            <Button
              label="新增品牌"
              onClick={() => this.setState({ isCreating: true })} />
          )}
        </div>
        <div style={styles.groupWrapper}>
          <Field
            name="brandId"
            label="關鍵字搜尋："
            placeholder="請輸入品牌關鍵字"
            onSubmit={(v) => { this.setState({ keyword: v }); }}
            component={Input} />
        </div>
        <div style={styles.tableWrapper}>
          <Query
            variables={{
              keyword,
            }}
            query={FETCH_BRAND_LIST}>
            {({
              data: {
                brandList = [],
              },
            }) => (
              <Table
                dataSource={brandList.map(b => ({
                  ...b,
                  keyword,
                }))}
                actionTitles={['操作']}
                getActions={() => [
                  <PhoneModelManageActions />,
                ]}>
                <TableField name="流水號" fieldKey="id" flex={0.5} isCenter />
                <TableField
                  name="品牌名稱"
                  fieldKey="name"
                  flex={1}
                  isCenter
                  MutationContext={TableMutationContext}
                  Component={EditableLabel} />
              </Table>
            )}
          </Query>
        </div>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_PHONE_BRAND_LIST_SEARCHER,
});

export default formHook(
  radium(
    BrandListManage
  )
);
