// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  formValueSelector,
  change,
} from 'redux-form';
import {
  Query,
} from 'react-apollo';
// config
import {
  FORM_PHONE_MODEL_SEARCHER,
} from '../../shared/form.js';
import {
  FETCH_BRAND_LIST,
  FETCH_BRAND_MODEL_LIST,
  FETCH_MODEL_COLOR_LIST,
} from '../../queries/Brand.js';
import * as PhoneModelActions from '../../actions/PhoneModel.js';
// components
import Selector from '../Form/Selector.jsx';
import SubmitButton from '../Form/SubmitButton.jsx';

const selector = formValueSelector(FORM_PHONE_MODEL_SEARCHER);

const styles = {
  placement: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  searchWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
  },
  selectorsWrapper: {
    flex: 1,
    width: 'auto',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  selector: {
    width: 'auto',
    height: 'auto',
    padding: 4,
  },
  functionWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '4px 36px',
  },
};

type Props = {
  handleSubmit: Function,
  setModelId: Function,
  setColorId: Function,
  cachePhoneListSearchOption: Function,
  selectedBrandId: number,
  selectedModelId: number,
};

class PhoneModelListSearcher extends PureComponent<Props> {
  componentDidUpdate(prevProps) {
    const {
      selectedBrandId,
      selectedModelId,
      setModelId,
      setColorId,
    } = this.props;

    if (prevProps.selectedBrandId !== selectedBrandId) {
      setModelId('-1');
      setColorId('-1');
    } else if (prevProps.selectedModelId !== selectedModelId) {
      setColorId('-1');
    }
  }

  submit(d) {
    const {
      cachePhoneListSearchOption,
    } = this.props;

    const {
      brandId,
      modelId,
      colorId,
    } = d;

    cachePhoneListSearchOption({
      brandId: brandId || '-1',
      modelId: modelId || '-1',
      colorId: colorId || '-1',
    });
  }

  render() {
    const {
      handleSubmit,
      selectedBrandId,
      selectedModelId,
    } = this.props;

    return (
      <div style={styles.placement}>
        <form style={styles.searchWrapper} onSubmit={handleSubmit(d => this.submit(d))}>
          <div style={styles.selectorsWrapper}>
            <div style={styles.selector}>
              <Query query={FETCH_BRAND_LIST}>
                {({
                  data: {
                    brandList = [],
                  },
                }) => (
                  <Field
                    name="brandId"
                    label="品牌:"
                    nullable
                    placeholder="請選擇"
                    options={brandList}
                    component={Selector} />
                )}
              </Query>
            </div>
            <div style={styles.selector}>
              <Query
                query={FETCH_BRAND_MODEL_LIST}
                variables={{
                  brandId: (selectedBrandId && parseInt(selectedBrandId, 10)) || -1,
                }}>
                {({
                  data: {
                    brandModelList = [],
                  },
                }) => (
                  <Field
                    name="modelId"
                    label="型號:"
                    nullable
                    placeholder="請選擇"
                    options={brandModelList}
                    component={Selector} />
                )}
              </Query>
            </div>
            <div style={styles.selector}>
              <Query
                query={FETCH_MODEL_COLOR_LIST}
                variables={{
                  modelId: (selectedModelId && parseInt(selectedModelId, 10)) || -1,
                }}>
                {({
                  data: {
                    brandModelColorList = [],
                  },
                }) => (
                  <Field
                    name="colorId"
                    label="顏色:"
                    nullable
                    placeholder="請選擇"
                    options={brandModelColorList}
                    component={Selector} />
                )}
              </Query>
            </div>
          </div>
          <div style={styles.functionWrapper}>
            <SubmitButton label="查詢" />
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_PHONE_MODEL_SEARCHER,
});

const reduxHook = connect(
  state => ({
    selectedBrandId: selector(state, 'brandId'),
    selectedModelId: selector(state, 'modelId'),
  }),
  dispatch => bindActionCreators({
    ...PhoneModelActions,
    setModelId: v => change(FORM_PHONE_MODEL_SEARCHER, 'modelId', v),
    setColorId: v => change(FORM_PHONE_MODEL_SEARCHER, 'colorId', v),
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      PhoneModelListSearcher
    )
  )
);
