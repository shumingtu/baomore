// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  reduxForm,
  Field,
} from 'redux-form';
import {
  Query,
  Mutation,
} from 'react-apollo';
// config
import TextStyles from '../../styles/Text.js';
import { FORM_PHONE_COLOR_LIST_SEARCHER } from '../../shared/form.js';
import {
  FETCH_MODEL_COLOR_LIST,
} from '../../queries/Brand.js';
import {
  EDIT_BRAND_MODEL_COLOR,
} from '../../mutations/Brand.js';
// component
import Button from '../Global/Button.jsx';
import Input from '../Form/Input.jsx';
import Table, { TableField } from '../Table/Table.jsx';
import EditableLabel from '../Table/Custom/EditableLabel.jsx';
import PhoneColorManageActions from '../Table/Actions/PhoneColorManageActions.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  groupWrapper: {
    width: 400,
    height: 'auto',
    padding: '24px 0 0 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  label: {
    ...TextStyles.labelText,
    fontSize: 16,
  },
  tableWrapper: {
    width: '100%',
    maxWidth: 760,
    height: 320,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  inputWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 12px 0 0',
  },
};

function TableMutationContext({
  currentSelectedModel,
  render,
}: {
  currentSelectedModel: Number,
  render: Function,
}) {
  return (
    <Mutation
      mutation={EDIT_BRAND_MODEL_COLOR}
      update={(cache, { data: { editBrandModelColor } }) => {
        const {
          brandModelColorList,
        } = cache.readQuery({
          query: FETCH_MODEL_COLOR_LIST,
          variables: {
            modelId: currentSelectedModel,
          },
        });

        const editingIdx = brandModelColorList.findIndex(b => b.id === editBrandModelColor.id);

        if (~editingIdx) {
          cache.writeQuery({
            query: FETCH_MODEL_COLOR_LIST,
            variables: {
              modelId: currentSelectedModel,
            },
            data: {
              brandModelColorList: [
                ...brandModelColorList.slice(0, editingIdx),
                editBrandModelColor,
                ...brandModelColorList.slice(editingIdx + 1),
              ],
            },
          });
        }
      }}>
      {(editBrandModelColor, { loading }) => render(editBrandModelColor, { loading })}
    </Mutation>
  );
}

const TableMutationContextWithRedux = connect(
  state => ({
    currentSelectedModel: state.PhoneModel.currentSelectedModel,
  }),
)(TableMutationContext);

type Props = {
  currentSelectedModel: number,
  currentSelectedBrand: number,
  history: Object,
};

class ColorListManage extends PureComponent<Props> {
  state = {
    keyword: null,
  };

  render() {
    const {
      currentSelectedModel,
      currentSelectedBrand,
      history,
    } = this.props;

    const {
      keyword,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        <div style={styles.groupWrapper}>
          <span style={styles.label}>
            顏色
          </span>
          <Button
            label="新增顏色"
            disabled={!currentSelectedModel}
            onClick={() => history.push(`/phoneModel/${currentSelectedModel}/${currentSelectedBrand}/create`)} />
        </div>
        <div style={styles.groupWrapper}>
          <Field
            name="colorId"
            label="關鍵字搜尋："
            placeholder="請輸入顏色關鍵字"
            onSubmit={(v) => { this.setState({ keyword: v }); }}
            component={Input} />
        </div>
        <div style={styles.tableWrapper}>
          <Query
            query={FETCH_MODEL_COLOR_LIST}
            variables={{
              modelId: currentSelectedModel || -1,
              keyword,
            }}>
            {({
              data: {
                brandModelColorList = [],
              },
            }) => (
              <Table
                dataSource={brandModelColorList}
                showPlaceholder={!currentSelectedModel}
                placeholder="尚未選擇手機型號"
                actionTitles={['操作']}
                getActions={() => [
                  <PhoneColorManageActions id={-1} name="" />,
                ]}>
                <TableField name="流水號" fieldKey="id" flex={0.5} isCenter />
                <TableField
                  name="顏色名稱"
                  fieldKey="name"
                  flex={1}
                  isCenter
                  MutationContext={TableMutationContextWithRedux}
                  Component={EditableLabel} />
                <TableField name="底圖" fieldKey="phoneBg" flex={1} isImage />
                <TableField name="遮罩圖" fieldKey="phoneMask" flex={1} isImage needBlackBg />
                <TableField name="覆蓋圖" fieldKey="phoneCover" flex={1} isImage />
              </Table>
            )}
          </Query>
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    currentSelectedModel: state.PhoneModel.currentSelectedModel,
    currentSelectedBrand: state.PhoneModel.currentSelectedBrand,
  }),
);

const formHook = reduxForm({
  form: FORM_PHONE_COLOR_LIST_SEARCHER,
});

export default reduxHook(
  withRouter(
    formHook(
      radium(
        ColorListManage
      )
    )
  )
);
