// @flow
import React, { memo } from 'react';
import radium from 'radium';
// config
import ButtonStyles from '../../styles/Button.js';

const styles = {
  ...ButtonStyles,
  disabled: {
    opacity: 0.3,
  },
};

type Props = {
  label: string,
  style: object,
  disabled?: boolean,
};

function SubmitButton({
  label = '送出',
  style = {},
  disabled,
}: Props) {
  const customStyles = Array.isArray(style) ? style : [style];

  return (
    <button
      type="submit"
      disabled={disabled}
      style={[
        styles.button,
        ...customStyles,
        disabled && styles.disabled,
      ]}>
      {label}
    </button>
  );
}

SubmitButton.defaultProps = {
  disabled: false,
};

export default memo(
  radium(
    SubmitButton
  )
);
