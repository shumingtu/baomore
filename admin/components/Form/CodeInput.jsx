// @flow
/* eslint jsx-a11y/label-has-for: 0 */
import React, { Component } from 'react';
import radium from 'radium';
import {
  Query,
} from 'react-apollo';
// config
import Theme from '../../styles/Theme.js';
import TextStyles from '../../styles/Text.js';

import {
  FETCH_PNTABLE_CODE,
} from '../../queries/PNTable.js';

const styles = {
  wrapper: {
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  label: {
    width: '100%',
    padding: '4px 0',
    letterSpacing: 1.2,
    whiteSpace: 'nowrap',
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  labelText: {
    ...TextStyles.labelText,
  },
  input: {
    flex: 1,
    fontSize: 14,
    padding: '8px 8px 10px 10px',
    border: `1px solid ${Theme.BLACK_COLOR}`,
    borderRadius: 2,
    backgroundColor: 'transparent',
    outline: 0,
    '::-webkit-input-placeholder': {
      color: 'rgba(0, 0, 0, 0.28)',
    },
  },
  disable: {
    backgroundColor: 'transparent',
    border: 'none',
  },
  errorInput: {
    border: '1px solid rgb(149, 28, 28)',
  },
  disableText: {
    color: 'rgba(0, 0, 0, 0.38)',
    cursor: 'default',
  },
  codeListWrap: {
    width: 168,
    height: 160,
    right: 0,
    top: 45,
    backgroundColor: 'rgb(177, 193, 210)',
    border: '1px solid black',
    position: 'absolute',
    zIndex: 1,
    overflow: 'auto',
  },
  codeItem: {
    width: '100%',
    color: 'black',
    fontSize: 14,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    border: 'none',
    outline: 'none',
    cursor: 'pointer',
    backgroundColor: 'transparent',
  },
};

type Props = {
  input: {
    value: String,
    name: String,
    onChange: Function,
  },
  onSubmit: Function,
  placeholder?: String,
  label?: String,
  type?: String,
  style?: Object,
  wrapperStyle?: Object,
  disabled?: Boolean,
  meta: {
    error: String,
  },
};

class CodeInput extends Component<Props> {
  static defaultProps = {
    type: 'text',
    style: [],
    wrapperStyle: [],
    placeholder: '',
    label: null,
    disabled: false,
  };

  state = {
    showCodeList: false,
  }

  onFocus() {
    this.setState({ showCodeList: true });
  }

  onBlur() {
    this.setState({ showCodeList: false });
  }


  render() {
    const {
      input: {
        value,
        onChange,
        name,
      },
      placeholder,
      label,
      type,
      style,
      wrapperStyle,
      disabled,
      onSubmit,
      meta: {
        error,
      },
    } = this.props;

    const {
      showCodeList,
    } = this.state;

    const customStyles = Array.isArray(style) ? style : [style];
    const customWrapperStyles = Array.isArray(wrapperStyle) ? wrapperStyle : [wrapperStyle];

    return (
      <div
        style={[
          styles.wrapper,
          ...customWrapperStyles,
        ]}>
        <label htmlFor={name} style={[styles.label]}>
          {error ? (
            <span style={{ display: 'block', color: 'rgb(149, 28, 28)' }}>
              !
            </span>
          ) : null}
          {label ? (
            <span style={styles.labelText}>
              {label}
            </span>
          ) : null}
          <input
            id={name}
            key="input"
            type={type}
            autoComplete="off"
            value={value}
            onFocus={() => this.onFocus()}
            onBlur={() => this.onBlur()}
            onChange={e => onChange(e)}
            onKeyPress={(e) => {
              if (e.key === 'Enter') {
                e.preventDefault();

                if (onSubmit) onSubmit(value);
              }
            }}
            name={name}
            placeholder={placeholder}
            disabled={disabled}
            style={[
              styles.input,
              error && styles.errorInput,
              disabled && styles.disableText,
              ...customStyles,
            ]} />
        </label>
        <Query variables={{ code: value }} query={FETCH_PNTABLE_CODE}>
          {({ data }) => {
            if (!data || !value) return null;

            const {
              pnTableCodeList,
            } = data;

            if (!pnTableCodeList || !pnTableCodeList.length || !showCodeList) return null;

            return (
              <div className="hideScrollBar" style={styles.codeListWrap}>
                {pnTableCodeList.map(item => (
                  <button
                    onMouseDown={() => {
                      onChange(item.code);
                      this.setState({ showCodeList: false });
                    }}
                    type="button"
                    key={item.id}
                    style={styles.codeItem}>
                    {item.code}
                  </button>
                ))}
              </div>
            );
          }}
        </Query>
      </div>
    );
  }
}

export default radium(CodeInput);
