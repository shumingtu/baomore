// @flow

import React, { Fragment } from 'react';
import { Field } from 'redux-form';
import {
  Query,
} from 'react-apollo';

import {
  FETCH_BEHAVIOR_LIST,
} from '../../queries/Behavior.js';

import Selector from './Selector.jsx';
import Input from './Input.jsx';

const styles = {
  wrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  fiedWrap: {
    margin: '5px 0',
  },
};

type Props = {
  behaviorId: Object,
};

function BehaviorDate({
  behaviorId,
}: Props) {
  return (
    <div style={styles.wrapper}>
      <Query query={FETCH_BEHAVIOR_LIST}>
        {({ data }) => {
          if (!data) return null;
          const {
            behaviorList = [],
          } = data;

          return (
            <div style={styles.fiedWrap}>
              <Field
                placeholder="請選擇"
                nullable
                options={behaviorList}
                label="行為類型"
                component={Selector}
                name="behaviorId" />
            </div>
          );
        }}
      </Query>
      {parseInt(behaviorId.input.value, 10) === 1 ? (
        <Fragment>
          <div style={styles.fiedWrap}>
            <Field
              name="startDate"
              label="銷貨開始時間"
              type="date"
              component={Input} />
          </div>
          <div style={styles.fiedWrap}>
            <Field
              name="endDate"
              label="銷貨結束時間"
              type="date"
              component={Input} />
          </div>
        </Fragment>
      ) : null}
    </div>
  );
}

export default BehaviorDate;
