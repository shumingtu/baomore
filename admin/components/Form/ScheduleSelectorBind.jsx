// @flow

import React from 'react';
import radium from 'radium';
import { Field } from 'redux-form';
import {
  Query,
  graphql,
} from 'react-apollo';

import TextStyles from '../../styles/Text.js';
import { getMyRoles } from '../../helper/roles.js';

import { FETCH_STORES_LIST } from '../../queries/Store.js';
import {
  GET_ALREADY_BOOKED_DATES,
  GET_AVAILABLE_BOOK_TIMES,
} from '../../queries/Order.js';
import { GET_EMPLOYEE_LIST, GET_MEMBER_ME } from '../../queries/Member.js';

import Selector from './Selector.jsx';
import CalendarBoard from './Calendar/CalendarBoard.jsx';

const styles = {
  wrapper: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  fieldLabel: {
    ...TextStyles.labelText,
    width: 120,
    '@media (max-width: 767px)': {
      width: 108,
    },
  },
};

type Props = {
  storeId: object,
  employeeId: object,
  time: object,
  date: object,
  cacheOriginTime: string,
  memberAreaId: number,
  me: object,
};

function ScheduleSelectorBind({
  storeId,
  employeeId,
  time,
  date,
  cacheOriginTime,
  me,
}: Props) {
  const myRoles = getMyRoles();
  const isAdmin = myRoles.includes('ADMIN_MEMBER_MANAGE');

  return (
    <div style={styles.wrapper}>
      <Query
        variables={{
          isForClient: true,
        }}
        query={FETCH_STORES_LIST}>
        {({ data }) => {
          if (!data) return null;

          const {
            stores = [],
          } = data;

          return (
            <div style={styles.fieldWrapper}>
              <Field
                placeholder="請選擇"
                nullable
                options={stores}
                label="預約門市"
                onChange={() => {
                  employeeId.input.onChange('-1');
                }}
                component={Selector}
                name="storeId" />
            </div>
          );
        }}
      </Query>
      <Query
        variables={{
          lineIdRequired: true,
          areaId: (!isAdmin && me && me.AreaId && parseInt(me.AreaId, 10)) || null,
        }}
        query={GET_EMPLOYEE_LIST}>
        {({ data }) => {
          if (!data) return null;

          const employeeMemberlist = (data && data.employeeMemberlist) || [];

          return (
            <div style={styles.fieldWrapper}>
              <Field
                nullable
                name="employeeId"
                label="包膜師："
                options={employeeMemberlist}
                placeholder="請選擇包膜師"
                component={Selector} />
            </div>
          );
        }}
      </Query>
      <Query
        skip={!storeId.input.value
          || storeId.input.value === '-1'
          || !employeeId.input.value
          || employeeId.input.value === '-1'}
        query={GET_ALREADY_BOOKED_DATES}
        fetchPolicy="network-only"
        variables={{
          storeId: parseInt(storeId.input.value, 10),
          employeeId: parseInt(employeeId.input.value, 10),
        }}>
        {({ data }) => {
          if (!data) return null;

          const alreadyBookedDates = (data && data.alreadyBookedDates) || [];

          return (
            <div style={styles.fieldWrapper}>
              <span style={styles.fieldLabel}>
                預約日期：
              </span>
              <Field
                name="date"
                isNotEditable={!storeId}
                notAvailableDateList={alreadyBookedDates.map(d => ({
                  id: d.date || null,
                  name: d.date || null,
                }))}
                placeholder={!storeId ? '尚未選擇門市' : '選擇預約日期'}
                component={CalendarBoard} />
            </div>
          );
        }}
      </Query>
      <div style={styles.fieldWrapper}>
        <span
          style={[
            styles.fieldLabel,
            {
              '@media (max-width: 767px)': {
                width: 'auto',
              },
            },
          ]}>
          預約時間：
          {time.input.value && time.input.value !== '-1' ? `${time.input.value}` : cacheOriginTime}
        </span>
        <Query
          skip={!storeId.input.value
            || storeId.input.value === '-1'
            || !employeeId.input.value
            || employeeId.input.value === '-1'}
          query={GET_AVAILABLE_BOOK_TIMES}
          fetchPolicy="network-only"
          variables={{
            date: date.input.value || '',
            storeId: parseInt(storeId.input.value, 10),
            employeeId: parseInt(employeeId.input.value, 10),
          }}>
          {({
            data,
          }) => {
            const availableBookedTimes = (data && data.availableBookedTimes) || [];

            return (
              <Field
                nullable
                name="time"
                options={availableBookedTimes.map(t => ({
                  id: t.time ? `${t.time}` : null,
                  name: t.time || null,
                }))}
                placeholder={!storeId.input.value || !date.input.value ? '尚未選擇日期' : '請選擇新時間'}
                component={Selector}
                style={{ minWidth: 'auto' }} />
            );
          }}
        </Query>
      </div>
    </div>
  );
}

const queryHook = graphql(GET_MEMBER_ME, {
  props: ({
    data: {
      me,
    },
  }) => ({
    me: me || null,
  }),
});


export default queryHook(radium(ScheduleSelectorBind));
