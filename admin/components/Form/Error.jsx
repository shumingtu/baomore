// @flow
import React from 'react';

import Theme from '../../styles/Theme.js';

const styles = {
  errorWrapper: {
    width: '100%',
    height: 'auto',
    padding: '4px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  error: {
    fontSize: 13,
    fontWeight: 400,
    color: Theme.ERROR_COLOR,
  },
};

function errorTranslator(error) {
  switch (error) {
    case 'GraphQL error: LINE Login Error Or Code Expired':
      return '權限過期，請回到登入頁面重新登入';
    case 'GraphQL error: Member Stock Record Not Found Error':
      return '查無存貨，操作失敗';
    default:
      return error;
  }
}

function Error({
  error,
}: {
  error: string,
}) {
  if (!error) return null;

  return (
    <div style={styles.errorWrapper}>
      <span style={styles.error}>
        {errorTranslator(error)}
      </span>
    </div>
  );
}

export default Error;
