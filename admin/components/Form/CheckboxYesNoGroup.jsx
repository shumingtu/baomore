// @flow

import React, { Component } from 'react';
import radium from 'radium';
import TextStyles from '../../styles/Text.js';

const styles = {
  wrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  labelText: {
    ...TextStyles.text,
    fontSize: 14,
    margin: '0 10px 0 0',
  },
  checkText: {
    ...TextStyles.text,
    fontSize: 14,
    margin: '0 5px',
  },
  checkBoxWrap: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  checkBoxItemWrap: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '0 10px',
  },
  checkBox: {
    width: 20,
    height: 20,
    padding: 0,
    borderRadius: '100%',
    cursor: 'pointer',
    backgroundColor: 'rgb(150, 147, 147)',
    border: 'none',
    outline: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    ':hover': {
      opacity: 0.88,
    },
  },
  checkedInner: {
    width: 10,
    height: 10,
    borderRadius: '100%',
    backgroundColor: 'rgb(78, 118, 232)',
  },
};

type Props = {
  input: {
    value: String,
    name: String,
    onChange: Function,
  },
  label?: String,
  meta: {
    error: String,
  },
};

class CheckboxYesNoGroup extends Component<Props> {
  static defaultProps = {
    label: null,
  };

  click(status) {
    const {
      input: {
        onChange,
      },
    } = this.props;

    onChange(status);
  }

  render() {
    const {
      input: {
        value,
      },
      label,
      meta: {
        error,
      },
    } = this.props;

    return (
      <div style={styles.wrapper}>
        {error ? (
          <span style={{ display: 'block', color: 'rgb(149, 28, 28)' }}>
            !
          </span>
        ) : null}
        {label ? (
          <span style={styles.labelText}>
            {label}
          </span>
        ) : null}
        <div style={styles.checkBoxWrap}>
          <div style={styles.checkBoxItemWrap}>
            <button
              key="checkBox"
              onClick={() => this.click(true)}
              style={styles.checkBox}
              type="button">
              {value === true ? (
                <div style={styles.checkedInner} />
              ) : null}
            </button>
            <span style={styles.checkText}>是</span>
          </div>
          <div style={styles.checkBoxItemWrap}>
            <button
              key="checkBox2"
              onClick={() => this.click(false)}
              style={styles.checkBox}
              type="button">
              {value === false ? (
                <div style={styles.checkedInner} />
              ) : null}
            </button>
            <span style={styles.checkText}>否</span>
          </div>
        </div>
      </div>
    );
  }
}

export default radium(CheckboxYesNoGroup);
