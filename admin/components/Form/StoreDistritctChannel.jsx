// @flow

import React from 'react';
import { Field } from 'redux-form';
import {
  Query,
} from 'react-apollo';

import {
  FETCH_DISTRICT_LIST,
} from '../../queries/CityDistrict.js';
import {
  FETCH_CHANNEL_LIST,
} from '../../queries/Channel.js';
import {
  FETCH_STORES_LIST,
} from '../../queries/Store.js';

import Selector from './Selector.jsx';

const styles = {
  wrapper: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  fiedWrap: {
    margin: '5px 0',
  },
};

type Props = {
  channelId: number,
  districtId: number,
  storeId: number,
};

function StoreDistrictChannel({
  channelId,
  districtId,
  storeId,
}: Props) {
  return (
    <div style={styles.wrapper}>
      <Query
        query={FETCH_CHANNEL_LIST}>
        {({ data }) => {
          if (!data) return null;
          const {
            channelList = [],
          } = data;

          return (
            <div style={styles.fiedWrap}>
              <Field
                placeholder="請選擇"
                nullable
                options={channelList}
                label="通路"
                onChange={() => storeId.input.onChange('-1')}
                component={Selector}
                name="channelId" />
            </div>
          );
        }}
      </Query>
      <Query
        query={FETCH_DISTRICT_LIST}>
        {({ data }) => {
          if (!data) return null;

          const {
            districts = [],
          } = data;

          return (
            <div style={styles.fiedWrap}>
              <Field
                placeholder="請選擇"
                nullable
                options={districts}
                label="區域"
                onChange={() => storeId.input.onChange('-1')}
                component={Selector}
                name="districtId" />
            </div>
          );
        }}
      </Query>
      <Query
        variables={{
          channelId: (
            channelId.input.value
            && channelId.input.value !== '-1'
            && parseInt(channelId.input.value, 10)
          ) || null,
          districtId: (
            districtId.input.value
            && districtId.input.value !== '-1'
            && parseInt(districtId.input.value, 10)
          ) || null,
        }}
        query={FETCH_STORES_LIST}>
        {({ data }) => {
          if (!data) return null;

          const {
            stores = [],
          } = data;

          return (
            <div style={styles.fiedWrap}>
              <Field
                placeholder="請選擇"
                nullable
                options={stores}
                label="門市"
                component={Selector}
                name="storeId" />
            </div>
          );
        }}
      </Query>
    </div>
  );
}

export default StoreDistrictChannel;
