// @flow
import React from 'react';
import radium from 'radium';

import Theme from '../../styles/Theme.js';
import TextStyles from '../../styles/Text.js';

const styles = {
  selectGroup: {
    width: 'auto',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    position: 'relative',
  },
  label: {
    ...TextStyles.labelText,
  },
  required: {
    position: 'absolute',
    left: -3,
    top: 2,
    fontSize: 12,
    color: Theme.BLACK_COLOR,
  },
  selectSvgWrap: {
    position: 'absolute',
    right: 16,
    top: 16,
    width: 16,
    height: 16,
    pointerEvents: 'none',
    borderRadius: '0 1px 1px 0',
  },
  selector: {
    width: 'auto',
    minWidth: 167,
    border: `1px solid ${Theme.MAIN_THEME_COLOR}`,
    backgroundColor: 'transparent',
    appearance: 'none',
    WebkitAppearance: 'none',
    MozAppearance: 'none',
    borderRadius: 2,
    padding: '8px 35px 8px 14px',
    cursor: 'pointer',
    outline: 'none',
  },
  selectorTrangle: {
    position: 'absolute',
    top: 0,
    right: 0,
    width: 0,
    height: 0,
    borderStyle: 'solid',
    borderWidth: '6px 6px 0 6px',
    borderColor: `${Theme.MAIN_THEME_COLOR} transparent transparent transparent`,
  },
  errorSelect: {
    border: '1px solid rgb(149, 28, 28)',
  },
};

type Props = {
  input: {
    name: string | number,
    value: string,
    onChange: Function,
  },
  label?: string,
  options?: Array<{
    id: number,
    name: string,
  }>,
  style?: Object,
  nullable?: boolean,
  placeholder?: string,
  meta: {
    error: string,
  },
  disabled?: boolean,
  required?: boolean,
  customOnChange?: Function,
};

function Selector({
  input: {
    value,
    onChange,
  },
  label,
  options,
  style,
  nullable,
  placeholder,
  disabled,
  meta: {
    error,
  },
  required,
  customOnChange,
}: Props) {
  const customStyles = Array.isArray(style) ? style : [style];

  return (
    <div style={styles.selectGroup}>
      {error ? (
        <span style={{ display: 'block', color: 'rgb(149, 28, 28)' }}>
          !
        </span>
      ) : null}
      {required ? (
        <div style={styles.required}>
          *
        </div>
      ) : null}
      {label ? (
        <span style={styles.label}>
          {label}
        </span>
      ) : null}
      <select
        disabled={disabled}
        style={[
          styles.selector,
          error && styles.errorSelect,
          ...customStyles,
        ]}
        value={value}
        onChange={(e) => {
          onChange(e.target.value);

          if (customOnChange) {
            customOnChange();
          }
        }}>
        {nullable ? (
          <option key="-1" value="-1">
            {placeholder}
          </option>
        ) : null}
        {(options || []).map(t => (
          <option
            key={t.id}
            value={t.id}>
            {t.name}
          </option>
        ))}
      </select>
      <div style={styles.selectSvgWrap}>
        <div style={styles.selectorTrangle} />
      </div>
    </div>
  );
}

Selector.defaultProps = {
  options: [],
  style: {},
  nullable: false,
  placeholder: '無',
  label: null,
  disabled: false,
  required: false,
  customOnChange: null,
};

export default radium(Selector);
