// @flow

import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import moment from 'moment';
import CalendarTheme from '../../../styles/Calendar.js';
import PrevButton from '../../Global/PrevButton.jsx';
import NextButton from '../../Global/NextButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    backgroundColor: CalendarTheme.CALENDAR_SELECTOR_BACKGROUND,
    border: `1px solid ${CalendarTheme.CALENDAR_SELECTOR_BORDER_COLOR}`,
    padding: '8px 0',
    margin: '0 0 10px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  selectorWrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: 220,
    height: 24,
    margin: '0px auto',
  },
  btnWrapper: {
    width: 12,
    height: 12,
    backgroundColor: 'transparent',
  },
  sliderWrapper: {
    width: 160,
    height: 24,
    display: 'flex',
    flexWrap: 'no-wrap',
    alignItems: 'center',
    justifyContent: 'flex-start',
    overflow: 'hidden',
    position: 'relative',
  },
  month: {
    position: 'absolute',
    display: 'block',
    transition: 'left 0.24s ease-out',
    whiteSpace: 'nowrap',
    color: CalendarTheme.CALENDAR_SELECTOR_TEXT_DEFAULT_COLOR,
    fontSize: 13,
    opacity: 0.64,
  },
  activeMonth: {
    color: CalendarTheme.CALENDAR_SELECTOR_TEXT_ACTIVE_COLOR,
    fontWeight: 500,
    opacity: 1,
  },
};

type Props = {
  defaultDateBound: {
    minDate: string,
    maxDate: string,
  },
  onChange: Function,
  yearOnChange: Function,
  minDate: string,
  maxDate: string,
  currentYear: number,
  currentMonth: number,
};

const monthsInYear = moment.monthsShort();

function getAvailableYears(defaultDateBound) {
  if (defaultDateBound) {
    const setMinYear = moment(defaultDateBound.minDate).year();
    const setMaxYear = moment(defaultDateBound.maxDate).year();

    const yearArray = Array.apply(
      {},
      { length: (setMaxYear - setMinYear) + 1 }
    ).map(s => s);

    return yearArray.map((y, idx) => ({
      id: setMinYear + idx,
      name: (setMinYear + idx).toString(),
    }));
  }

  return [];
}

function getAvailableMonths(defaultDateBound) {
  if (defaultDateBound) {
    const monthArray = Array.apply(
      {},
      { length: (moment(defaultDateBound.maxDate).diff(moment(defaultDateBound.minDate), 'month')) + 1 }
    ).map(s => s);

    return monthArray.map((y, idx) => {
      const temp = moment(defaultDateBound.minDate);
      const date = (temp.add(idx, 'month')).format('YYYY-MM');
      return ({
        id: date,
        name: date,
      });
    });
  }

  return [];
}

class CalendarSelector extends PureComponent<Props> {
  monthSwitched(month) {
    const {
      onChange,
    } = this.props;

    if (onChange) {
      const monthNumber = parseInt(month, 10) || 0;

      if (monthNumber < 1 || monthNumber > 12) return null;

      return onChange(month);
    }

    return null;
  }

  yearSwitched(year) {
    const {
      yearOnChange,
    } = this.props;

    if (yearOnChange) {
      const newMonth = year === moment().year() ? moment().month() + 1 : 1;

      return yearOnChange(year, newMonth);
    }

    return null;
  }

  render() {
    const {
      defaultDateBound,
      currentYear,
      currentMonth,
    } = this.props;

    const yearAvailable = getAvailableYears(defaultDateBound);
    const monthAvailables = getAvailableMonths(defaultDateBound);
    const currentMonthAvailable = monthAvailables.filter((m) => {
      const wrapYear = m.id.split('-').filter(y => y)[0];
      return parseInt(wrapYear, 10) === currentYear;
    });
    const monthStrArray = currentMonthAvailable.map((d) => {
      const month = moment(d.id).month();
      const monthStr = monthsInYear[month] || null;

      return ({
        index: month,
        name: monthStr,
      });
    });
    const currentMinMonth = currentMonthAvailable[0]
      ? moment(currentMonthAvailable[0].id).month() + 1
      : 0;
    const currentMaxMonth = currentMonthAvailable[currentMonthAvailable.length - 1]
      ? moment(currentMonthAvailable[currentMonthAvailable.length - 1].id).month() + 1
      : 0;

    return (
      <div style={styles.wrapper}>
        <div style={styles.selectorWrapper}>
          {currentYear > yearAvailable[0].id ? (
            <PrevButton
              key="year-prev-button"
              onClick={() => this.yearSwitched(currentYear - 1)}
              style={styles.btnWrapper} />
          ) : <div style={styles.btnWrapper} />}
          <div style={styles.sliderWrapper}>
            {yearAvailable.map((year, idx) => (
              <span
                key={year.id}
                style={[
                  styles.month,
                  yearAvailable[idx].id === currentYear && styles.activeMonth,
                  { left: `calc(70px + ${50 * (yearAvailable[idx].id - currentYear)}px)` },
                ]}>
                {year.name}
              </span>
            ))}
          </div>
          {currentYear < yearAvailable[yearAvailable.length - 1].id ? (
            <NextButton
              key="year-next-button"
              onClick={() => this.yearSwitched(currentYear + 1)}
              style={styles.btnWrapper} />
          ) : <div style={styles.btnWrapper} />}
        </div>
        <div style={styles.selectorWrapper}>
          {currentMonth > parseInt(currentMinMonth, 10) ? (
            <PrevButton
              key="month-prev-button"
              onClick={() => this.monthSwitched(currentMonth - 1)}
              style={styles.btnWrapper} />
          ) : <div style={styles.btnWrapper} />}
          <div style={styles.sliderWrapper}>
            {monthStrArray.map(month => (
              <span
                key={month.name}
                style={[
                  styles.month,
                  month.index + 1 === currentMonth && styles.activeMonth,
                  {
                    left: `calc(70px + ${50 * ((month.index + 1) - currentMonth)}px)`,
                  },
                ]}>
                {month.name}
              </span>
            ))}
          </div>
          {currentMonth < parseInt(currentMaxMonth, 10) ? (
            <NextButton
              key="month-next-button"
              onClick={() => this.monthSwitched(currentMonth + 1)}
              style={styles.btnWrapper} />
          ) : <div style={styles.btnWrapper} />}
        </div>
      </div>
    );
  }
}

export default radium(
  CalendarSelector
);
