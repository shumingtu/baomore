// @flow

import React, { PureComponent } from 'react';
import { Field } from 'redux-form';
import {
  Query,
} from 'react-apollo';

import {
  GET_EMPLOYEE_LIST,
} from '../../queries/Member.js';

import Selector from './Selector.jsx';
import Input from './Input.jsx';
import Button from '../Global/Button.jsx';

const styles = {
  wrapper: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    width: '100%',
    flexDirection: 'column',
  },
  settingWrap: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    flexDirection: 'column',
    margin: '10px 0',
    border: '1px solid rgb(31, 107, 132)',
    padding: 10,
  },
  fiedWrap: {
    margin: '5px 0',
  },
};

const types = [{
  id: 'QUANTITY',
  name: '總數量',
}, {
  id: 'MONEY',
  name: '總金額',
}];

type Props = {
  fields: Object,
};

class Settings extends PureComponent<Props> {
  render() {
    const {
      fields,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <Button
          label="新增設定"
          onClick={() => fields.push({})} />
        {fields.map((setting, idx) => (
          <div key={idx} style={styles.settingWrap}>
            <Button
              label="刪除"
              onClick={() => fields.remove(idx)} />
            <Query query={GET_EMPLOYEE_LIST}>
              {({ data }) => {
                if (!data) return null;
                const {
                  employeeMemberlist = [],
                } = data;

                const options = [
                  {
                    id: 'ALL',
                    name: '全部',
                  },
                  ...employeeMemberlist,
                ];

                return (
                  <div style={styles.fiedWrap}>
                    <Field
                      placeholder="請選擇"
                      nullable
                      options={options}
                      label="包膜師"
                      component={Selector}
                      name={`${setting}.MemberId`} />
                  </div>
                );
              }}
            </Query>
            <div style={styles.fiedWrap}>
              <Field
                placeholder="請選擇"
                nullable
                options={types}
                label="設定類別："
                component={Selector}
                name={`${setting}.type`} />
            </div>
            <div style={styles.fiedWrap}>
              <Field
                type="number"
                component={Input}
                label="值："
                placeholder="請輸入設定值"
                name={`${setting}.value`} />
            </div>
            <div style={styles.fiedWrap}>
              <Field
                component={Input}
                label="提醒訊息："
                placeholder="請輸入提醒訊息"
                name={`${setting}.message`} />
            </div>
          </div>
        ))}
      </div>
    );
  }
}

export default Settings;
