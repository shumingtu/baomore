// @flow
import React, { Component } from 'react';
import radium from 'radium';
import {
  Field,
} from 'redux-form';
// config
import Theme from '../../../styles/Theme.js';
import TextStyles from '../../../styles/Text.js';
// components
import Button from '../../Global/Button.jsx';
import Input from '../Input.jsx';
import Selector from '../Selector.jsx';
import DeleteButton from '../../Global/DeleteButton.jsx';
import Dragger from '../../Global/Dragger.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  titleWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  fragmentTitle: {
    ...TextStyles.labelText,
  },
  buttonWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 12px',
  },
  mainWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
  },
  fragmentWrapper: {
    width: '100%',
    height: 'auto',
    border: `1px solid ${Theme.MAIN_THEME_COLOR}`,
    borderRadius: 4,
    padding: 12,
    margin: '6px 0',
  },
  fragmentFuncWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  selectorWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 12px 0 0',
  },
  functionWrapper: {
    flex: 1,
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  fragmentContentWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  fragmentInputWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  textareaStyle: {
    width: '100%',
  },
  funcWrap: {
    width: 'auto',
    height: 'auto',
    padding: '0 4px',
  },
};

const OPTIONS = [{
  id: 'TEXT',
  name: '文字',
}];

type Props = {
  fields: {
    insert: Function,
    remove: Function,
    get: Function,
    push: Function,
  },
};

type State = {
  reorderCandidateIdx: number,
};

function fragmentRenderBundle(type) {
  switch (type) {
    case 'TEXT':
      return ({
        title: {
          type: 'text',
          placeholder: '請輸入標題',
          component: Input,
        },
        content: {
          type: 'textarea',
          placeholder: '請輸入文字',
          component: Input,
          style: styles.textareaStyle,
        },
      });
    default:
      return null;
  }
}

class FragmentForm extends Component<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      reorderCandidateIdx: -1,
    };
  }

  onSwapping(idx) {
    const {
      fields,
    } = this.props;

    const {
      reorderCandidateIdx,
    } = this.state;

    if (reorderCandidateIdx === idx) {
      return this.setState({
        reorderCandidateIdx: -1,
      });
    }

    if (~reorderCandidateIdx) {
      fields.swap(reorderCandidateIdx, idx);

      return this.setState({
        reorderCandidateIdx: -1,
      });
    }

    return this.setState({
      reorderCandidateIdx: idx,
    });
  }

  addNewFragment() {
    const {
      fields,
    } = this.props;

    fields.push({
      type: 'TEXT',
      title: '',
      content: '',
    });
  }

  render() {
    const {
      fields,
    } = this.props;

    const {
      reorderCandidateIdx,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        <div style={styles.titleWrapper}>
          <span style={styles.fragmentTitle}>
            內容：
          </span>
          <div style={styles.buttonWrapper}>
            <Button
              label="新增內容"
              onClick={() => this.addNewFragment()} />
          </div>
        </div>
        <div style={styles.mainWrapper}>
          {fields.map((fragment, idx) => {
            const fragmentBundle = fragmentRenderBundle(fields.get(idx).type);
            const isOnSwapping = idx === reorderCandidateIdx;

            return (
              <div key={`${idx + 1}-fragment`} style={styles.fragmentWrapper}>
                <div style={styles.fragmentFuncWrapper}>
                  <div style={styles.selectorWrapper}>
                    <Field
                      name={`${fragment}.type`}
                      options={OPTIONS}
                      component={Selector} />
                  </div>
                  <div style={styles.functionWrapper}>
                    <div style={styles.funcWrap}>
                      <DeleteButton onDelete={() => fields.remove(idx)} />
                    </div>
                    <div style={styles.funcWrap}>
                      <Dragger
                        isSwapMode={~reorderCandidateIdx}
                        isActived={isOnSwapping}
                        onClick={() => this.onSwapping(idx)} />
                    </div>
                  </div>
                </div>
                <div style={styles.fragmentContentWrapper}>
                  {fragmentBundle && fragmentBundle.title ? (
                    <div style={styles.fragmentInputWrapper}>
                      <Field
                        name={`${fragment}.title`}
                        type={fragmentBundle.title.type || 'text'}
                        placeholder={fragmentBundle.title.placeholder || null}
                        label={fragmentBundle.title.label || null}
                        component={fragmentBundle.title.component || null}
                        style={fragmentBundle.title.style || null} />
                    </div>
                  ) : null}
                  {fragmentBundle && fragmentBundle.content ? (
                    <div style={styles.fragmentInputWrapper}>
                      <Field
                        name={`${fragment}.content`}
                        type={fragmentBundle.content.type || 'text'}
                        placeholder={fragmentBundle.content.placeholder || null}
                        label={fragmentBundle.content.label || null}
                        component={fragmentBundle.content.component || null}
                        style={fragmentBundle.content.style || null} />
                    </div>
                  ) : null}
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default radium(FragmentForm);
