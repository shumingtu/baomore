// @flow
/* eslint jsx-a11y/label-has-for: 0 */
import React, { PureComponent } from 'react';
import radium from 'radium';
// config
import Theme from '../../styles/Theme.js';
import TextStyles from '../../styles/Text.js';

const styles = {
  wrapper: {
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  label: {
    width: '100%',
    height: 'auto',
    padding: '4px 0',
    letterSpacing: 1.2,
    whiteSpace: 'nowrap',
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'flex-start',
    position: 'relative',
  },
  required: {
    position: 'absolute',
    left: -3,
    top: 8,
    fontSize: 12,
    color: Theme.BLACK_COLOR,
  },
  labelText: {
    ...TextStyles.labelText,
  },
  input: {
    flex: 1,
    fontSize: 14,
    padding: '8px 8px 10px 10px',
    border: `1px solid ${Theme.BLACK_COLOR}`,
    borderRadius: 2,
    backgroundColor: 'transparent',
    outline: 0,
    minWidth: 168,
    '::-webkit-input-placeholder': {
      color: 'rgba(0, 0, 0, 0.28)',
    },
  },
  disable: {
    backgroundColor: 'transparent',
    border: 'none',
  },
  errorInput: {
    border: '1px solid rgb(149, 28, 28)',
  },
  disableText: {
    color: 'rgba(0, 0, 0, 0.38)',
    cursor: 'default',
  },
  textarea: {
    ...TextStyles.text,
    padding: 8,
    fontSize: 14,
    border: `1px solid ${Theme.BLACK_COLOR}`,
    backgroundColor: 'transparent',
    resize: 'none',
    width: 500,
    height: 150,
    outline: 0,
    '@media (max-width: 767px)': {
      width: '100%',
    },
  },
};

type Props = {
  input: {
    value: String,
    name: String,
    onChange: Function,
  },
  onSubmit: Function,
  placeholder?: String,
  label?: String,
  type?: String,
  style?: Object,
  wrapperStyle?: Object,
  disabled?: Boolean,
  meta: {
    error: String,
  },
  required?: boolean,
  noDefaultValue?: boolean,
};

class Input extends PureComponent<Props> {
  static defaultProps = {
    type: 'text',
    style: [],
    wrapperStyle: [],
    placeholder: '',
    label: null,
    disabled: false,
    required: false,
    noDefaultValue: false,
  };

  render() {
    const {
      input: {
        value,
        onChange,
        name,
      },
      placeholder,
      label,
      type,
      style,
      wrapperStyle,
      disabled,
      onSubmit,
      meta: {
        error,
      },
      required,
      noDefaultValue,
    } = this.props;

    const customStyles = Array.isArray(style) ? style : [style];
    const customWrapperStyles = Array.isArray(wrapperStyle) ? wrapperStyle : [wrapperStyle];

    return (
      <div
        style={[
          styles.wrapper,
          ...customWrapperStyles,
        ]}>
        <label htmlFor={name} style={[styles.label]}>
          {error ? (
            <span style={{ display: 'block', color: 'rgb(149, 28, 28)' }}>
              !
            </span>
          ) : null}
          {required ? (
            <div style={styles.required}>
              *
            </div>
          ) : null}
          {label ? (
            <span style={styles.labelText}>
              {label}
            </span>
          ) : null}
          {type === 'textarea' ? (
            <textarea
              key="textarea"
              value={value}
              onChange={e => onChange(e.target.value)}
              placeholder={placeholder}
              style={[
                styles.textarea,
                error && styles.errorInput,
                ...customStyles,
              ]} />
          ) : (
            <input
              id={name}
              key="input"
              type={type}
              value={noDefaultValue ? undefined : value}
              onChange={e => onChange(e)}
              onKeyPress={(e) => {
                if (e.key === 'Enter') {
                  e.preventDefault();

                  if (onSubmit) onSubmit(value);
                }
              }}
              name={name}
              placeholder={placeholder}
              disabled={disabled}
              style={[
                styles.input,
                error && styles.errorInput,
                disabled && styles.disableText,
                ...customStyles,
              ]} />
          )}
        </label>
      </div>
    );
  }
}

export default radium(Input);
