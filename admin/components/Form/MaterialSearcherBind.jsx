// @flow
import React from 'react';
import { Field } from 'redux-form';
import {
  Query,
} from 'react-apollo';

import {
  FETCH_BIGSUBCATEGORIES,
  FETCH_SMALLSUBCATEGORIES,
} from '../../queries/PNTable.js';
import Selector from './Selector.jsx';

const styles = {
  rowItem: {
    margin: '5px 0px',
  },
};

type Props = {
  pnCategoryId: number,
  bigSubCategoryId: object,
  smallSubCategoryId: object,
};

function MaterialSearcherBind({
  pnCategoryId,
  bigSubCategoryId,
  smallSubCategoryId,
}: Props) {
  return (
    <>
      <div style={styles.rowItem}>
        <Query
          variables={{
            id: parseInt(pnCategoryId, 10),
          }}
          query={FETCH_BIGSUBCATEGORIES}>
          {({ data }) => {
            if (!data) return null;

            const {
              subCategoriesByPNCategory,
            } = data;

            return (
              <Field
                name="bigSubCategoryId"
                label="大類:"
                nullable
                placeholder="請選擇"
                onChange={() => smallSubCategoryId.input.onChange('-1')}
                options={subCategoriesByPNCategory || []}
                component={Selector} />
            );
          }}
        </Query>
      </div>
      <div style={styles.rowItem}>
        <Query
          skip={!bigSubCategoryId.input.value || bigSubCategoryId.input.value === '-1'}
          variables={{
            id: parseInt(bigSubCategoryId.input.value, 10),
          }}
          query={FETCH_SMALLSUBCATEGORIES}>
          {({ data }) => {
            if (!data) return null;

            const {
              subCategoriesBySubCategory,
            } = data;

            return (
              <Field
                name="smallSubCategoryId"
                label="小類:"
                nullable
                placeholder="請選擇"
                options={subCategoriesBySubCategory || []}
                component={Selector} />
            );
          }}
        </Query>
      </div>
    </>
  );
}

export default MaterialSearcherBind;
