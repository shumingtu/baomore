// @flow
import React from 'react';
import radium from 'radium';

import Theme from '../../styles/Theme.js';
import importFile from '../../helper/import.js';

const styles = {
  wrapper: {
    width: 110,
    height: 42,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundSize: 'contain',
    borderRadius: 3,
    position: 'relative',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundColor: Theme.ACTIVE_COLOR,
  },
  editInput: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    opacity: 0,
    cursor: 'pointer',
  },
  text: {
    fontSize: 14,
    color: 'white',
    fontWeight: 500,
  },
};

type Props = {
  label: string,
  onClick: Function,
  style: Object,
  disabled: boolean,
  input: Object,
  importType: String,
};

function ImportCSVButton({
  label,
  input: {
    name,
    onChange,
    value,
  },
  importType,
}: Props) {
  if (value === 'importing') {
    return (
      <div style={styles.wrapper}>
        <p style={styles.text}>上傳中</p>
      </div>
    );
  }

  return (
    <div style={styles.wrapper}>
      <p style={styles.text}>{label}</p>
      <input
        type="file"
        onChange={(e) => {
          importFile(e.target.files[0], importType, onChange);
        }}
        style={[
          styles.editInput,
        ]}
        id={name} />
      <input
        type="hidden"
        key={name}
        name={name}
        value={value} />
    </div>
  );
}

export default radium(
  ImportCSVButton
);
