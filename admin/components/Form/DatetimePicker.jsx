// @flow
import React from 'react';
import radium from 'radium';
import Datetime from 'react-datetime';
import 'react-datetime/css/react-datetime.css';

import TextStyles from '../../styles/Text.js';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  labelText: {
    ...TextStyles.labelText,
  },
  fieldWrapper: {
    width: 'auto',
    height: 'auto',
  },
};

type Props = {
  input: {
    value: string,
    onChange: Function,
  },
  meta: {
    error: string,
  },
  label: string,
  disableDate?: boolean,
  disableTime?: boolean,
};

function DatetimePicker({
  input: {
    value,
    onChange,
  },
  meta: {
    error,
  },
  label,
  disableDate,
  disableTime,
}: Props) {
  return (
    <div style={styles.wrapper}>
      {error ? (
        <span style={{ display: 'block', color: 'rgb(149, 28, 28)' }}>
          !
        </span>
      ) : null}
      {label ? (
        <span style={styles.labelText}>
          {label}
        </span>
      ) : null}
      <div style={styles.fieldWrapper}>
        <Datetime
          dateFormat={!disableDate ? 'YYYY-MM-DD' : false}
          timeFormat={!disableTime ? 'HH:mm' : false}
          value={value}
          onChange={onChange}
          closeOnSelect />
      </div>
    </div>
  );
}

DatetimePicker.defaultProps = {
  disableDate: false,
  disableTime: false,
};

export default radium(DatetimePicker);
