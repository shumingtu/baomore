// @flow
import React from 'react';
import { Field } from 'redux-form';
import {
  Query,
} from 'react-apollo';

import {
  FETCH_BRAND_LIST,
  FETCH_BRAND_MODEL_LIST,
} from '../../queries/Brand.js';

import Selector from './Selector.jsx';

const styles = {
  rowItem: {
    margin: '5px 0px',
  },
};

type Props = {
  brandId: object,
  modelId: object,
};

function ProtectorSearcherBind({
  brandId,
  modelId,
}: Props) {
  return (
    <>
      <div style={styles.rowItem}>
        <Query query={FETCH_BRAND_LIST}>
          {({
            data: {
              brandList = [],
            },
          }) => (
            <Field
              name="brandId"
              label="品牌:"
              nullable
              placeholder="請選擇"
              onChange={() => modelId.input.onChange('-1')}
              options={brandList}
              component={Selector} />
          )}
        </Query>
      </div>
      <div style={styles.rowItem}>
        <Query
          query={FETCH_BRAND_MODEL_LIST}
          variables={{
            brandId: parseInt(brandId.input.value, 10) || -1,
          }}>
          {({
            data: {
              brandModelList = [],
            },
          }) => (
            <Field
              name="modelId"
              label="型號:"
              nullable
              placeholder="請選擇"
              options={brandModelList}
              component={Selector} />
          )}
        </Query>
      </div>
    </>
  );
}

export default ProtectorSearcherBind;
