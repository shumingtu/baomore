// @flow
import React, { Component } from 'react';
import radium from 'radium';

import uploadFile from '../../helper/uploader.js';

import TextStyles from '../../styles/Text.js';

const styles = {
  text: {
    ...TextStyles.text,
    fontSize: 12,
    color: '#a7d3e1',
  },
  imgWrap: {
    width: 150,
    height: 251,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundSize: 'contain',
    position: 'relative',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundColor: 'transparent',
    border: '1px solid #a7d3e1',
  },
  editInput: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    opacity: 0,
    cursor: 'pointer',
  },
  errorImg: {
    border: '1px solid rgb(149, 28, 28)',
  },
};

type Props = {
  input: {
    name: String,
    onChange: Function,
    value: String,
  },
  meta: {
    error: String,
  },
  annotation?: string,
  style?: Object,
};

class FileInput extends Component<Props> {
  static defaultProps = {
    annotation: '圖片尺寸：1418x2362',
    style: {},
  };

  render() {
    const {
      input: {
        name,
        onChange,
        value,
      },
      meta: {
        error,
      },
      annotation,
      style,
    } = this.props;

    if (value === 'uploading') {
      return (
        <div style={[
          styles.imgWrap,
          style,
          error && styles.errorImg,
        ]}>
          <p style={styles.text}>上傳中</p>
        </div>
      );
    }

    if (value && value !== 'uploading') {
      const bgColor = name === 'phoneMask' ? 'rgba(0, 0, 0, 0.6)' : 'transparent';
      return (
        <div style={[
          styles.imgWrap,
          {
            backgroundImage: `url(${value})`,
            backgroundColor: `${bgColor}`,
            border: 'none',
          },
          style,
          error && styles.errorImg,
        ]}>
          <input
            type="file"
            accept="image/*"
            onChange={e => uploadFile(e.target.files[0], onChange)}
            style={styles.editInput}
            id={name} />
          <input
            type="hidden"
            key={name}
            name={name}
            value={value} />
        </div>
      );
    }

    return (
      <div style={[
        styles.imgWrap,
        style,
        error && styles.errorImg,
      ]}>
        <p style={styles.text}>
          {annotation || null}
        </p>
        <input
          type="file"
          accept="image/*"
          onChange={e => uploadFile(e.target.files[0], onChange)}
          style={styles.editInput}
          id={name} />
        <input
          type="hidden"
          key={name}
          name={name}
          value={value} />
      </div>
    );
  }
}


export default radium(FileInput);
