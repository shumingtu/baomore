// @flow
import React from 'react';
import radium from 'radium';

import TextStyles from '../../../styles/Text.js';
import Button from '../../Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  actionWrapper: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
  },
  actionBlock: {
    margin: 5,
  },
  labelText: {
    ...TextStyles.labelText,
  },
};

type Props = {
  input: Object,
  label: String,
};

function Actions({
  input: {
    value,
    onChange,
    name,
  },
  label,
}: Props) {
  const handleOnClick = (action) => {
    const existIdx = value.findIndex(a => a.code === action.code);

    if (~existIdx) {
      onChange([
        ...value.slice(0, existIdx),
        ...value.slice(existIdx + 1),
      ]);

      return;
    }

    onChange([
      ...value,
      {
        code: action.code,
        name: action.name,
      },
    ]);
  };

  if (!value || !value.length) return null;

  return (
    <div style={styles.wrapper}>
      <span style={styles.labelText}>{label}</span>
      <div style={styles.actionWrapper}>
        {value.map(a => (
          <div key={a.code} name={name} style={styles.actionBlock}>
            <Button onClick={() => handleOnClick(a)} label={a.name} />
          </div>
        ))}
      </div>
    </div>
  );
}

export default radium(Actions);
