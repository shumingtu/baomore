// @flow

import React from 'react';
import { Field } from 'redux-form';
import {
  Query,
} from 'react-apollo';

import {
  FETCH_CITY_LIST,
  FETCH_DISTRICT_LIST,
} from '../../queries/CityDistrict.js';

import Selector from './Selector.jsx';

const styles = {
  wrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  fiedWrap: {
    margin: '5px 0',
  },
};

type Props = {
  cityId: Object,
  districtId: Object,
};

function CityDistricts({
  cityId,
  districtId,
}: Props) {
  return (
    <div style={styles.wrapper}>
      <Query query={FETCH_CITY_LIST}>
        {({ data }) => {
          if (!data) return null;
          const {
            cityDistricts = [],
          } = data;

          return (
            <div style={styles.fiedWrap}>
              <Field
                placeholder="請選擇"
                nullable
                options={cityDistricts}
                label="城市"
                onChange={() => districtId.input.onChange('-1')}
                component={Selector}
                name="cityId" />
            </div>
          );
        }}
      </Query>
      <Query
        variables={{ cityId: (cityId.input.value && parseInt(cityId.input.value, 10)) || -1 }}
        query={FETCH_DISTRICT_LIST}>
        {({ data }) => {
          if (!data) return null;
          const {
            districts = [],
          } = data;

          return (
            <div style={styles.fiedWrap}>
              <Field
                placeholder="請選擇"
                nullable
                options={districts}
                label="區域"
                component={Selector}
                name="districtId" />
            </div>
          );
        }}
      </Query>
    </div>
  );
}

export default CityDistricts;
