// @flow
import React from 'react';
import radium from 'radium';

import TextStyles from '../../styles/Text.js';
import Button from '../Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  roleWrapper: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
  },
  roleBlock: {
    margin: 5,
  },
  labelText: {
    ...TextStyles.labelText,
  },
};

type Props = {
  input: Object,
  label: String,
};

function Roles({
  input: {
    value,
    onChange,
    name,
  },
  label,
}: Props) {
  const handleOnClick = (role) => {
    const existIdx = value.findIndex(v => v.id === role.id);

    if (~existIdx) {
      onChange([
        ...value.slice(0, existIdx),
        ...value.slice(existIdx + 1),
      ]);

      return;
    }

    onChange([
      ...value,
      {
        id: role.id,
        name: role.name,
      },
    ]);
  };

  if (!value) return null;

  return (
    <div style={styles.wrapper}>
      <span style={styles.labelText}>{label}</span>
      <div style={styles.roleWrapper}>
        {value.map(r => (
          <div key={r.id} name={name} style={styles.roleBlock}>
            <Button onClick={() => handleOnClick(r)} label={r.name} />
          </div>
        ))}
      </div>
    </div>
  );
}

export default radium(Roles);
