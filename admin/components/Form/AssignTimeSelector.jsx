// @flow

import React from 'react';
import { Field } from 'redux-form';
import {
  Query,
  graphql,
} from 'react-apollo';

import {
  FETCH_STORES_LIST,
} from '../../queries/Store.js';
import {
  GET_EMPLOYEE_LIST,
  GET_MEMBER_ME,
} from '../../queries/Member.js';
import {
  GET_ALREADY_BOOKED_DATES,
  GET_AVAILABLE_BOOK_TIMES,
} from '../../queries/Order.js';
import TextStyles from '../../styles/Text.js';
import { getMyRoles } from '../../helper/roles.js';

import Selector from './Selector.jsx';
import CalendarBoard from './Calendar/CalendarBoard.jsx';

const styles = {
  wrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    width: '100%',
  },
  fiedWrap: {
    width: '100%',
    margin: '5px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  fieldLabel: {
    ...TextStyles.labelText,
    width: 117,
  },
};

type Props = {
  storeId: number,
  employeeId: number,
  date: string,
  time: string,
  originTime: string,
  me: object,
};

function AssignTimeSelector({
  storeId,
  employeeId,
  date,
  time,
  originTime,
  me,
}: Props) {
  const myRoles = getMyRoles();
  const isAdmin = myRoles.includes('ADMIN_MEMBER_MANAGE');
  return (
    <div style={styles.wrapper}>
      <Query
        variables={{
          isForClient: true,
        }}
        query={FETCH_STORES_LIST}>
        {({ data }) => {
          if (!data) return null;

          const {
            stores,
          } = data;

          return (
            <div style={styles.fiedWrap}>
              <Field
                name="storeId"
                label="門市:"
                nullable
                onChange={() => {
                  employeeId.input.onChange('-1');
                  date.input.onChange(null);
                  time.input.onChange('-1');
                }}
                placeholder="請選擇"
                options={stores || []}
                component={Selector} />
            </div>
          );
        }}
      </Query>
      <Query
        variables={{
          areaId: (!isAdmin && me && me.AreaId && parseInt(me.AreaId, 10)) || null,
          lineIdRequired: true,
        }}
        query={GET_EMPLOYEE_LIST}>
        {({ data }) => {
          const employeeMemberlist = (data && data.employeeMemberlist) || [];

          return (
            <div style={styles.fiedWrap}>
              <Field
                name="employeeId"
                label="包膜師:"
                onChange={() => {
                  date.input.onChange(null);
                  time.input.onChange('-1');
                }}
                nullable
                placeholder="請選擇"
                options={employeeMemberlist}
                component={Selector} />
            </div>
          );
        }}
      </Query>
      <Query
        skip={
          !storeId.input.value
          || !employeeId.input.value
          || storeId.input.value === '-1'
          || employeeId.input.value === '-1'
        }
        variables={{
          storeId: parseInt(storeId.input.value, 10),
          employeeId: parseInt(employeeId.input.value, 10),
        }}
        fetchPolicy="network-only"
        query={GET_ALREADY_BOOKED_DATES}>
        {({ data }) => {
          const alreadyBookedDates = (data && data.alreadyBookedDates) || [];

          return (
            <div style={styles.fiedWrap}>
              <span style={styles.fieldLabel}>
                預約日期:
              </span>
              <Field
                name="date"
                isNotEditable={!storeId.input.value || !employeeId.input.value}
                notAvailableDateList={alreadyBookedDates.map(d => ({
                  id: d.date || null,
                  name: d.date || null,
                }))}
                onChange={() => time.input.onChange('-1')}
                placeholder="選擇預約日期"
                component={CalendarBoard} />
            </div>
          );
        }}
      </Query>
      <Query
        skip={
          !storeId.input.value
          || !employeeId.input.value
          || !date.input.value
          || storeId.input.value === '-1'
          || employeeId.input.value === '-1'
        }
        query={GET_AVAILABLE_BOOK_TIMES}
        fetchPolicy="network-only"
        variables={{
          storeId: parseInt(storeId.input.value, 10),
          date: date.input.value,
          employeeId: parseInt(employeeId.input.value, 10),
        }}>
        {({
          data,
        }) => {
          const availableBookedTimes = (data && data.availableBookedTimes) || [];

          return (
            <div style={styles.fiedWrap}>
              <Field
                name="time"
                label="預約時間:"
                nullable
                placeholder="請選擇"
                options={availableBookedTimes.map(t => ({
                  id: t.time || null,
                  name: t.time || null,
                }))}
                component={Selector} />
              <span style={{ margin: '0px 5px' }}>
                原預約時間:
                {originTime}
              </span>
            </div>
          );
        }}
      </Query>
    </div>
  );
}

const queryHook = graphql(GET_MEMBER_ME, {
  props: ({
    data: {
      me,
    },
  }) => ({
    me: me || null,
  }),
});

export default queryHook(AssignTimeSelector);
