// @flow
import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
import { Mutation } from 'react-apollo';

// config
import Theme from '../../styles/Theme.js';
import TextStyles from '../../styles/Text.js';
import { VERIFY_BEHAVIOR_RECORD } from '../../mutations/Rollback.js';

// components
import Button from '../Global/Button.jsx';
import Error from '../Form/Error.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '8px 6px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    border: `1px solid ${Theme.SECONDARY_THEME_COLOR}`,
    borderRadius: 4,
    overflowY: 'hidden',
    overflowX: 'auto',
  },
  label: {
    ...TextStyles.labelText,
    width: 'auto',
    padding: '0 6px',
  },
  switchBtn: {
    width: 'auto',
    height: 'auto',
    color: Theme.NAV_COLOR,
    fontSize: 14,
    fontWeight: 500,
    padding: '6px 12px',
    border: 0,
    margin: '0 4px',
    outline: 0,
    backgroundColor: 'transparent',
    cursor: 'pointer',
    ':hover': {
      opacity: 0.8,
    },
  },
  btnActive: {
    color: Theme.ACTIVE_COLOR,
  },
  buttonWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 6px',
  },
  errorWrapper: {
    flex: 1,
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  errorBox: {
    width: 530,
    height: 400,
    position: 'absolute',
    backgroundColor: 'white',
    borderRadius: 3,
    border: '1px solid black',
    overflow: 'auto',
    padding: 10,
  },
  closeBtn: {
    position: 'absolute',
    right: 5,
    top: 5,
    border: 'none',
    outline: 'none',
    fontSize: 16,
    cursor: 'pointer',
  },
};

type Props = {
  dataSource: Array<{
    id: number,
  }>,
  checkedList: Array<number>,
  onOverride: Function,
};

class TableActionBar extends PureComponent<Props> {
  state = {
    errorRecords: [],
  }

  render() {
    const {
      dataSource,
      checkedList,
      onOverride,
    } = this.props;

    const {
      errorRecords,
    } = this.state;

    const hasCheckableData = dataSource.length !== 0;
    const isAllSelected = !hasCheckableData ? false : dataSource.length === checkedList.length;
    const isAllDeselected = !hasCheckableData ? false : checkedList.length === 0;

    return (
      <div style={styles.wrapper}>
        <span style={styles.label}>
          審核工具：
        </span>
        <button
          key="select-all-button"
          type="button"
          onClick={hasCheckableData
            ? () => onOverride(dataSource.map(d => d.id))
            : null}
          style={[
            styles.switchBtn,
            isAllSelected && styles.btnActive,
          ]}>
          全選
        </button>
        <button
          key="deselect-all-button"
          type="button"
          onClick={hasCheckableData
            ? () => onOverride([])
            : null}
          style={[
            styles.switchBtn,
            isAllDeselected && styles.btnActive,
          ]}>
          全不選
        </button>
        <Mutation mutation={VERIFY_BEHAVIOR_RECORD}>
          {(verifyBehaviorRecords, { loading, error }) => (
            <Fragment>
              <div style={styles.buttonWrapper}>
                <Button
                  disabled={!checkedList.length || loading}
                  label="確認審核"
                  onClick={async () => {
                    if (hasCheckableData) {
                      const {
                        data,
                      } = await verifyBehaviorRecords({
                        variables: {
                          recordIds: checkedList,
                        },
                      });

                      if (data && data.verifyBehaviorRecords) {
                        onOverride([]);

                        if (data.verifyBehaviorRecords.errorRecords.length) {
                          this.setState({ errorRecords: data.verifyBehaviorRecords.errorRecords });
                        }
                      }
                    }
                  }} />
              </div>
              {error ? (
                <div style={styles.errorWrapper}>
                  <Error error={error.message || null} />
                </div>
              ) : null}
            </Fragment>
          )}
        </Mutation>
        {errorRecords.length ? (
          <div style={styles.errorBox}>
            <button
              onClick={() => this.setState({ errorRecords: [] })}
              type="button"
              style={styles.closeBtn}>
              X
            </button>
            <span>以下紀錄退貨失敗，請截圖聯絡管理員！</span>
            {errorRecords.map(errorRecord => (
              <div key={errorRecord.id} style={{ margin: '5px 0px' }}>
                <span style={{ margin: '0px 3px' }}>
                  recordId：
                  {errorRecord.id}
                </span>
                <span style={{ margin: '0px 3px' }}>
                  姓名：
                  {errorRecord.member.name}
                </span>
                <span style={{ margin: '0px 3px' }}>
                  工號：
                  {errorRecord.member.serialNumber}
                </span>
                <span style={{ margin: '0px 3px' }}>
                  料號：
                  {errorRecord.pnTable.code}
                </span>
              </div>
            ))}
          </div>
        ) : null}
      </div>
    );
  }
}

export default radium(TableActionBar);
