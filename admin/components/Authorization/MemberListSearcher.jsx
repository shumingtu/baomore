// @flow
import React, { Component } from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
} from 'redux-form';

import {
  cacheSearchOptions,
  CACHE_ADMIN_MEMBER_LIST_SEARCH,
} from '../../actions/Search.js';
import { FETCH_ROLE_LIST } from '../../queries/Role.js';
import {
  FORM_MEMBER_LIST_SEARCHER,
} from '../../shared/form.js';

import Input from '../Form/Input.jsx';
import Selector from '../Form/Selector.jsx';
import SubmitButton from '../Form/SubmitButton.jsx';
import Button from '../Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  searchWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  rowWrapper: {
    display: 'flex',
    width: '100%',
    alignItems: 'flex-start',
    flexDirection: 'column',
    justifyContent: 'center',
    margin: '5px 0',
  },
  rowItem: {
    margin: '5px 0',
  },
  searchBtn: {
    backgroundColor: 'rgb(182, 184, 204)',
    border: '1px solid rgb(182, 184, 204)',
    color: 'white',
  },
  createBtn: {
    backgroundColor: 'rgb(182, 184, 204)',
    border: '1px solid rgb(182, 184, 204)',
    color: 'white',
    margin: '0 5px',
  },
};

type Props = {
  cacheMemberSearch: Function,
  handleSubmit: Function,
  history: Obeject,
};

class MemberListSearcher extends Component<Props> {
  submit(d) {
    const {
      cacheMemberSearch,
    } = this.props;

    const {
      keyword,
      roleId,
    } = d;

    cacheMemberSearch(CACHE_ADMIN_MEMBER_LIST_SEARCH, {
      keyword: keyword || null,
      roleId: roleId && roleId !== '-1' ? parseInt(roleId, 10) : null,
    });
  }

  render() {
    const {
      handleSubmit,
      history,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <form onSubmit={handleSubmit(d => this.submit(d))} style={styles.searchWrapper}>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Field
                name="keyword"
                label="關鍵字:"
                placeholder="姓名、工號、電話"
                component={Input} />
            </div>
            <Query query={FETCH_ROLE_LIST}>
              {({ data }) => {
                if (!data) return null;

                const {
                  roleList = [],
                } = data;

                return (
                  <div style={styles.rowItem}>
                    <Field
                      name="roleId"
                      label="角色:"
                      nullable
                      options={roleList}
                      component={Selector} />
                  </div>
                );
              }}
            </Query>
            <div style={styles.rowItem}>
              <SubmitButton style={styles.searchBtn} label="查詢" />
              <Button
                style={styles.createBtn}
                onClick={() => history.push('/authorization/member/create')}
                label="新增會員" />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_MEMBER_LIST_SEARCHER,
});

const reduxHook = connect(
  () => ({
  }),
  dispatch => bindActionCreators({
    cacheMemberSearch: cacheSearchOptions,
  }, dispatch)
);

export default withRouter(
  reduxHook(
    formHook(
      radium(MemberListSearcher)
    )
  )
);
