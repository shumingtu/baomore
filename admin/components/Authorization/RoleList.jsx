// @flow

import React from 'react';
import radium from 'radium';
import {
  Query,
} from 'react-apollo';
// config

import Button from '../Global/Button.jsx';

import {
  FETCH_ROLE_LIST,
} from '../../queries/Role.js';

const styles = {
  wrapper: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
  },
  roleWrap: {
    margin: 5,
  },
};

type Props = {
  currentRoles?: array,
  setRole: Function,
};

function RoleList({
  currentRoles,
  setRole,
}: Props) {
  return (
    <Query query={FETCH_ROLE_LIST}>
      {({ data }) => {
        if (!data) return null;

        const {
          roleList = [],
        } = data;

        if (!roleList.length) return null;

        return (
          <div style={styles.wrapper}>
            {roleList.map((r) => {
              const isRoleExist = currentRoles.find(x => x.id === r.id);
              return (
                <div key={r.id} style={styles.roleWrap}>
                  <Button
                    onClick={() => {
                      setRole([
                        {
                          id: r.id,
                          name: r.name,
                        },
                        ...currentRoles,
                      ]);
                    }}
                    disabled={!!isRoleExist}
                    label={r.name} />
                </div>
              );
            })}
          </div>
        );
      }}
    </Query>
  );
}

RoleList.defaultProps = {
  currentRoles: [],
};


export default radium(RoleList);
