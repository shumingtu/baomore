// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { Mutation } from 'react-apollo';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  initialize,
  formValueSelector,
  SubmissionError,
  change,
} from 'redux-form';

import Input from '../Form/Input.jsx';
import SubmitButton from '../Form/SubmitButton.jsx';
import FormTitle from '../Global/FormTitle.jsx';
import ActionList from './ActionList.jsx';
import Actions from '../Form/Role/Actions.jsx';

import { FORM_ROLE_EDIT_FORM } from '../../shared/form.js';
import { EDIT_ROLE } from '../../mutations/Role.js';
import { validateRoleEdit } from '../../helper/validator/RoleEditValidator.js';

const selector = formValueSelector(FORM_ROLE_EDIT_FORM);

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 999,
    top: 0,
  },
  box: {
    width: 600,
    height: 600,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    border: '1px solid #979797',
    backgroundColor: 'white',
    position: 'relative',
    padding: 40,
  },
  closeBtn: {
    position: 'relative',
    cursor: 'pointer',
    border: 'none',
    outline: 'none',
    fontSize: 20,
    backgroundColor: 'transparent',
  },
  circle: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: 30,
    height: 30,
    borderRadius: '100%',
    border: '2px solid black',
    position: 'absolute',
    top: 20,
    right: 20,
  },
  contentOuterWrapper: {
    width: 520,
    height: 520,
    overflow: 'auto',
  },
  contentWrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  contentBox: {
    width: '100%',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  fieldWrapper: {
    margin: '10px 0',
  },
  submitBtnWrap: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '5px 0',
  },
};

type Props = {
  roleData: object,
  close: Function,
  handleSubmit: Function,
  initializeForm: Function,
  currentActions: array,
  setAction: Function,
};

class RoleEditPopUp extends PureComponent<Props> {
  componentDidMount() {
    const {
      roleData,
      initializeForm,
    } = this.props;

    if (roleData) initializeForm(roleData);
  }

  async submit(data, mutate) {
    const errors = validateRoleEdit(data);

    if (errors) throw new SubmissionError(errors);

    const {
      close,
    } = this.props;

    await mutate({
      variables: {
        roleId: data.id,
        name: data.name,
        actionCodes: data.actions.map(a => a.code),
      },
    });

    close();
  }

  render() {
    const {
      close,
      handleSubmit,
      currentActions,
      setAction,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div
          style={styles.box}>
          <div style={styles.circle}>
            <button
              onClick={() => close()}
              style={styles.closeBtn}
              type="button">
              X
            </button>
          </div>
          <Mutation
            mutation={EDIT_ROLE}>
            {editRole => (
              <div style={styles.contentOuterWrapper}>
                <form
                  onSubmit={handleSubmit(d => this.submit(d, editRole))}
                  style={styles.contentWrapper}>
                  <FormTitle title="編輯角色" />
                  <div style={styles.contentBox}>
                    <div style={styles.fieldWrapper}>
                      <Field
                        label="角色名稱:"
                        name="name"
                        disabled
                        component={Input} />
                    </div>
                    <div style={styles.fieldWrapper}>
                      <Field
                        label="權限:"
                        name="actions"
                        component={Actions} />
                    </div>
                    <div style={styles.fieldWrapper}>
                      <h2>選擇權限</h2>
                      <ActionList
                        setAction={setAction}
                        currentActions={currentActions} />
                    </div>
                    <div style={styles.submitBtnWrap}>
                      <SubmitButton
                        label="確定" />
                    </div>
                  </div>
                </form>
              </div>
            )}
          </Mutation>
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    currentActions: selector(state, 'actions'),
  }),
  dispatch => bindActionCreators({
    initializeForm: d => initialize(FORM_ROLE_EDIT_FORM, d),
    setAction: v => change(FORM_ROLE_EDIT_FORM, 'actions', v),
  }, dispatch),
);

const formHook = reduxForm({
  form: FORM_ROLE_EDIT_FORM,
});

export default reduxHook(
  formHook(
    radium(RoleEditPopUp)
  )
);
