// @flow

import React from 'react';
import radium from 'radium';
import {
  Query,
} from 'react-apollo';
// config

import Button from '../Global/Button.jsx';

import {
  FETCH_ACTION_LIST,
} from '../../queries/Action.js';

const styles = {
  wrapper: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
  },
  actionWrap: {
    margin: 5,
  },
};

type Props = {
  currentActions: array,
  setAction: Function,
};

function ActionList({
  currentActions,
  setAction,
}: Props) {
  return (
    <Query query={FETCH_ACTION_LIST}>
      {({ data }) => {
        if (!data) return null;

        const {
          actionList = [],
        } = data;

        if (!actionList.length || !currentActions) return null;

        return (
          <div style={styles.wrapper}>
            {actionList.map((a) => {
              const isActionExist = currentActions.find(x => x.code === a.code);
              return (
                <div key={a.code} style={styles.actionWrap}>
                  <Button
                    onClick={() => {
                      setAction([
                        {
                          code: a.code,
                          name: a.name,
                        },
                        ...currentActions,
                      ]);
                    }}
                    disabled={!!isActionExist}
                    label={a.name} />
                </div>
              );
            })}
          </div>
        );
      }}
    </Query>
  );
}


export default radium(ActionList);
