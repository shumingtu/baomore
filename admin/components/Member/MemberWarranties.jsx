// @flow
import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { toggleWarrantyBox } from '../../actions/Member.js';

import Theme from '../../styles/Theme.js';
import artmoLogo from '../../static/images/artmo-logo-top.png';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 999,
  },
  box: {
    width: 600,
    height: 600,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    border: '1px solid #979797',
    backgroundColor: 'white',
    position: 'relative',
    padding: 40,
  },
  contentWrapper: {
    width: 520,
    height: 520,
    overflow: 'auto',
    display: 'inline-grid',
  },
  innerContentWrap: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  circle: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: 30,
    height: 30,
    borderRadius: '100%',
    border: '2px solid black',
    position: 'absolute',
    top: 20,
    right: 20,
  },
  closeBtn: {
    position: 'relative',
    cursor: 'pointer',
    border: 'none',
    outline: 'none',
    fontSize: 20,
    backgroundColor: 'transparent',
  },
  cardWrapper: {
    width: 483,
    height: 360,
    backgroundColor: '#fff',
    borderRadius: 12,
    boxShadow: '0 5px 24px -8px rgba(0, 0, 0, 0.3)',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    margin: '10px 0',
  },
  logo: {
    width: 135,
    height: 52,
    margin: '4px 0',
  },
  linePlacement: {
    width: '100%',
    height: 15,
    margin: '4px 0',
    borderTop: `2px solid ${Theme.YELLOW_COLOR}`,
    borderBottom: `2px solid ${Theme.PURPLE_COLOR}`,
    position: 'relative',
    overflow: 'hidden',
  },
  heavyLine: {
    position: 'absolute',
    zIndex: 3,
    width: 250,
    height: 8,
    backgroundColor: Theme.YELLOW_COLOR,
    '@media (max-width: 767px)': {
      width: 160,
    },
  },
  triangle: {
    position: 'absolute',
    zIndex: 3,
    width: 20,
    height: 20,
    transform: 'rotate(45deg)',
    backgroundColor: Theme.YELLOW_COLOR,
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '8px 24px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  label: {
    width: 150,
    height: 'auto',
    fontSize: 16,
    fontWeight: 400,
    letterSpacing: 1,
    color: Theme.BLACK_COLOR,
  },
  text: {
    fontSize: 16,
    fontWeight: 300,
    letterSpacing: 1,
    color: Theme.BLACK_COLOR,
    padding: '0 4px',
  },
};

type Props = {
  closeWarrantyBox: Function,
  warranties: Array,
};

class MemberWarranties extends PureComponent<Props> {
  render() {
    const {
      closeWarrantyBox,
      warranties,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.box}>
          <div style={styles.circle}>
            <button
              onClick={() => closeWarrantyBox(null)}
              style={styles.closeBtn}
              type="button">
              X
            </button>
          </div>
          <div className="hideScrollBar" style={styles.contentWrapper}>
            <div style={styles.innerContentWrap}>
              {warranties && warranties.length ? (
                <Fragment>
                  {warranties.map(x => (
                    <div key={x.id} style={styles.cardWrapper}>
                      <img
                        alt="Artmo Logo"
                        src={artmoLogo}
                        style={styles.logo} />
                      <div style={styles.linePlacement}>
                        <div
                          style={[
                            styles.heavyLine,
                            {
                              left: -15,
                              top: 0,
                              backgroundColor: Theme.YELLOW_COLOR,
                            },
                          ]} />
                        <div
                          style={[
                            styles.triangle,
                            {
                              left: 225,
                              top: -16,
                              backgroundColor: Theme.YELLOW_COLOR,
                              '@media (max-width: 767px)': {
                                left: 135,
                              },
                            },
                          ]} />
                        <div
                          style={[
                            styles.heavyLine,
                            {
                              right: -15,
                              bottom: 0,
                              backgroundColor: Theme.PURPLE_COLOR,
                            },
                          ]} />
                        <div
                          style={[
                            styles.triangle,
                            {
                              right: 225,
                              bottom: -16,
                              backgroundColor: Theme.PURPLE_COLOR,
                              '@media (max-width: 767px)': {
                                right: 135,
                              },
                            },
                          ]} />
                      </div>
                      <div style={styles.fieldWrapper}>
                        <span style={styles.label}>
                          服務項目
                        </span>
                        ：
                        <span style={styles.text}>
                          {x.service || '無'}
                        </span>
                      </div>
                      <div style={styles.fieldWrapper}>
                        <span style={styles.label}>
                          手機型號
                        </span>
                        ：
                        <span style={styles.text}>
                          {x.phoneModel || '無'}
                        </span>
                      </div>
                      <div style={styles.fieldWrapper}>
                        <span style={styles.label}>
                          IME後五碼
                        </span>
                        ：
                        <span style={styles.text}>
                          {x.IME || '無'}
                        </span>
                      </div>
                      <div style={styles.fieldWrapper}>
                        <span style={styles.label}>
                          手機末六碼
                        </span>
                        ：
                        <span style={styles.text}>
                          {x.phoneLastSix || '無'}
                        </span>
                      </div>
                      <div style={styles.fieldWrapper}>
                        <span style={styles.label}>
                          門市名稱
                        </span>
                        ：
                        <span style={styles.text}>
                          {x.store || '無'}
                        </span>
                      </div>
                      <div style={styles.fieldWrapper}>
                        <span style={styles.label}>
                          包膜師工號
                        </span>
                        ：
                        <span style={styles.text}>
                          {x.employeeCode || '無'}
                        </span>
                      </div>
                      <div style={styles.fieldWrapper}>
                        <span style={styles.label}>
                          審核狀態
                        </span>
                        ：
                        <span style={styles.text}>
                          {x.isApproved ? '已確認生效' : '尚未生效'}
                        </span>
                      </div>
                      <div style={styles.fieldWrapper}>
                        <span style={styles.label}>
                          消費日期(登入日期)
                        </span>
                        ：
                        <span style={styles.text}>
                          {x.createdAt || '無'}
                        </span>
                      </div>
                    </div>
                  ))}
                </Fragment>
              ) : '無保固資料'}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  () => ({
  }),
  dispatch => bindActionCreators({
    closeWarrantyBox: toggleWarrantyBox,
  }, dispatch),
);

export default reduxHook(radium(MemberWarranties));
