// @flow
import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import OrderItemField from './OrderItemField.jsx';

import { toggleOrderBox } from '../../actions/Member.js';

import {
  PAYED_STATUS_SPEC,
  SHIPPING_STATUS_SPEC,
} from '../../shared/Global.js';

import Theme from '../../styles/Theme.js';
import artmoLogo from '../../static/images/artmo-logo-top.png';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 999,
  },
  box: {
    width: 600,
    height: 600,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    border: '1px solid #979797',
    backgroundColor: 'white',
    position: 'relative',
    padding: 40,
  },
  contentWrapper: {
    width: 520,
    height: 520,
    overflow: 'auto',
    display: 'inline-grid',
  },
  innerContentWrap: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  circle: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: 30,
    height: 30,
    borderRadius: '100%',
    border: '2px solid black',
    position: 'absolute',
    top: 20,
    right: 20,
  },
  closeBtn: {
    position: 'relative',
    cursor: 'pointer',
    border: 'none',
    outline: 'none',
    fontSize: 20,
    backgroundColor: 'transparent',
  },
  cardWrapper: {
    width: 500,
    height: 700,
    backgroundColor: '#fff',
    borderRadius: 12,
    boxShadow: '0 5px 24px -8px rgba(0, 0, 0, 0.3)',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    margin: '10px 0',
  },
  logo: {
    width: 135,
    height: 52,
    margin: '4px 0',
  },
  linePlacement: {
    width: '100%',
    height: 15,
    margin: '4px 0',
    borderTop: `2px solid ${Theme.YELLOW_COLOR}`,
    borderBottom: `2px solid ${Theme.PURPLE_COLOR}`,
    position: 'relative',
    overflow: 'hidden',
  },
  heavyLine: {
    position: 'absolute',
    zIndex: 3,
    width: 250,
    height: 8,
    backgroundColor: Theme.YELLOW_COLOR,
    '@media (max-width: 767px)': {
      width: 160,
    },
  },
  triangle: {
    position: 'absolute',
    zIndex: 3,
    width: 20,
    height: 20,
    transform: 'rotate(45deg)',
    backgroundColor: Theme.YELLOW_COLOR,
  },
  groupImageWrapper: {
    width: '100%',
    height: 'auto',
    padding: '8px 24px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  imageWrapper: {
    width: '100%',
    height: 'auto',
    padding: '8px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  groupLabel: {
    width: 'auto',
    minWidth: 90,
    fontSize: 16,
    fontWeight: 400,
    letterSpacing: 1,
    color: Theme.BLACK_COLOR,
  },
  groupImage: {
    width: 200,
    height: 280,
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
};

type Props = {
  closeOrderBox: Function,
  orders: Array,
};

class MemberWarranties extends PureComponent<Props> {
  render() {
    const {
      closeOrderBox,
      orders,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.box}>
          <div style={styles.circle}>
            <button
              onClick={() => closeOrderBox(null)}
              style={styles.closeBtn}
              type="button">
              X
            </button>
          </div>
          <div className="hideScrollBar" style={styles.contentWrapper}>
            <div style={styles.innerContentWrap}>
              {orders && orders.length ? (
                <Fragment>
                  {orders.map(order => (
                    <div key={order.id} style={styles.cardWrapper}>
                      <img
                        alt="Artmo Logo"
                        src={artmoLogo}
                        style={styles.logo} />
                      <div style={styles.linePlacement}>
                        <div
                          style={[
                            styles.heavyLine,
                            {
                              left: -15,
                              top: 0,
                              backgroundColor: Theme.YELLOW_COLOR,
                            },
                          ]} />
                        <div
                          style={[
                            styles.triangle,
                            {
                              left: 225,
                              top: -16,
                              backgroundColor: Theme.YELLOW_COLOR,
                              '@media (max-width: 767px)': {
                                left: 135,
                              },
                            },
                          ]} />
                        <div
                          style={[
                            styles.heavyLine,
                            {
                              right: -15,
                              bottom: 0,
                              backgroundColor: Theme.PURPLE_COLOR,
                            },
                          ]} />
                        <div
                          style={[
                            styles.triangle,
                            {
                              right: 225,
                              bottom: -16,
                              backgroundColor: Theme.PURPLE_COLOR,
                              '@media (max-width: 767px)': {
                                right: 135,
                              },
                            },
                          ]} />
                      </div>
                      <OrderItemField
                        label="付款狀態："
                        value={PAYED_STATUS_SPEC[order.payedStatus] || null} />
                      <OrderItemField
                        label="送貨狀態："
                        value={SHIPPING_STATUS_SPEC[order.shippingStatus] || null} />
                      <OrderItemField
                        label="訂購會員："
                        value={order.name || null} />
                      <OrderItemField
                        label="訂購日期："
                        value={order.createdAt || null} />
                      <OrderItemField
                        label="聯絡電話："
                        value={order.phone || null} />
                      <OrderItemField
                        label="預約日期："
                        value={order.date || null} />
                      <OrderItemField
                        label="預約時段："
                        value={order.time || null} />
                      <div style={styles.groupImageWrapper}>
                        <span style={styles.groupLabel}>
                          訂購樣式：
                        </span>
                        <div style={styles.imageWrapper}>
                          <div
                            style={[
                              styles.groupImage,
                              {
                                backgroundImage: order.picture ? `url(${order.picture})` : null,
                              },
                            ]} />
                        </div>
                      </div>
                      <OrderItemField
                        label="訂購金額："
                        value={order.price ? `$${order.price}` : null} />
                    </div>
                  ))}
                </Fragment>
              ) : '無訂單資料'}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  () => ({
  }),
  dispatch => bindActionCreators({
    closeOrderBox: toggleOrderBox,
  }, dispatch),
);

export default reduxHook(radium(MemberWarranties));
