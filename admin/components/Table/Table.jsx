// @flow
import React, { Component } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';

import TableContext from '../../context/table.js';
import TableHeader from './TableHeader.jsx';
import TableList from './TableList.jsx';
import TablePlaceholder from './TablePlaceholder.jsx';

const styles = {
  tableWrapper: {
    flex: 1,
    width: '100%',
    height: '100%',
    overflowY: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      overflowX: 'auto',
    },
  },
};

type Props = {
  children: Array<Object>,
  dataSource: Array<Object>,
  actionTitles: Array<String>,
  fetchMore: Function,
  getActions: Function,
  showPlaceholder: Boolean,
  placeholder: String,
  options: Object,
  style?: Object,
  fixedItemWidth?: Number,
};

class Table extends Component<Props> {
  static defaultProps = {
    style: null,
    fixedItemWidth: null,
  };

  render() {
    const {
      children,
      getActions,
      dataSource,
      fetchMore,
      actionTitles,
      showPlaceholder,
      placeholder,
      options,
      style,
      fixedItemWidth,
    } = this.props;

    const wrapChildren = children || [];
    const wrapStyle = Array.isArray(style) ? style : [style];

    return (
      <TableContext.Provider
        value={{
          children: Array.isArray(wrapChildren) ? wrapChildren : [wrapChildren],
          getActions: getActions || (() => []),
          dataSource: dataSource || [],
          fetchMore,
          options,
          fixedItemWidth,
          actionTitles: actionTitles || [],
        }}>
        <div
          style={[
            styles.tableWrapper,
            ...wrapStyle,
            fixedItemWidth && { width: 'auto' },
          ]}>
          <TableHeader />
          {!showPlaceholder ? (
            <TableList />
          ) : (
            <TablePlaceholder placeholder={placeholder} />
          )}
        </div>
      </TableContext.Provider>
    );
  }
}

const reduxHook = connect(
  state => ({
    options: state.Order.listSearchOptions,
  }),
);

export default reduxHook(
  radium(Table)
);

export function TableField() {
  return null;
}
