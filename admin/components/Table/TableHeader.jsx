// @flow
import React from 'react';
import radium from 'radium';

import Theme from '../../styles/Theme.js';
import TableContext from '../../context/table.js';

const styles = {
  tableFrozenHeaderWrapper: {
    width: '100%',
    height: 44,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
    padding: '0 4px 0 0',
    borderBottom: `4px solid ${Theme.SECONDARY_THEME_COLOR}`,
  },
  headerField: {
    flex: 1,
    margin: '0 8px',
    textAlign: 'center',
    fontSize: 14,
    fontWeight: 500,
    color: Theme.BLACK_COLOR,
  },
};

function TableHeader() {
  return (
    <TableContext.Consumer>
      {({
        children,
        getActions,
        actionTitles,
        fixedItemWidth,
      }) => (
        <div style={styles.tableFrozenHeaderWrapper}>
          {(children && children.map((field, idx) => {
            if (!field.props) return null;

            const {
              name,
              flex,
              minWidth,
            } = field.props;

            return (
              <span
                key={`${name}-${idx + 1}`}
                style={[
                  styles.headerField,
                  {
                    flex: flex || 1,
                    minWidth: minWidth || 'auto',
                  },
                  fixedItemWidth && { width: fixedItemWidth, flex: 'none' },
                ]}>
                {name || null}
              </span>
            );
          })) || null}
          {getActions ? getActions().map((element, idx) => (
            <span key={`${element}-${idx + 1}`} style={styles.headerField}>
              {actionTitles[idx] || null}
            </span>
          )) : null}
        </div>
      )}
    </TableContext.Consumer>
  );
}

export default radium(TableHeader);
