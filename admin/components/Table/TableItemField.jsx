// @flow
import * as React from 'react';
import radium from 'radium';
import isNumber from 'lodash/isNumber';

import Theme from '../../styles/Theme.js';

const styles = {
  tableItemPlacement: {
    flex: 1,
    height: '100%',
    margin: '0 8px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    wordBreak: 'break-all',
  },
  tableItem: {
    fontSize: 13,
    fontWeight: 500,
    lineHeight: 1.618,
    whiteSpace: 'pre-line',
    color: Theme.BLACK_COLOR,
  },
  tableImage: {
    width: 65,
    height: 100,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  },
  textCenter: {
    textAlign: 'center',
  },
};

type Props = {
  data: Object,
  field: {
    props: {
      fieldKey: string,
      flex: number,
      prefix: string,
      isCenter: boolean,
      isImage: boolean,
      isClickable: boolean,
      needBlackBg: boolean,
      Component?: React.Node,
      MutationContext?: any,
      minWidth?: number,
      color?: string,
    },
  },
  fixedItemWidth?: number,
};

function dataTypeSetter(data, options) {
  if (!data && !isNumber(data)) return null;
  if (Array.isArray(data)) return data;

  const {
    prefix,
  } = options;

  if (prefix) return `${prefix}${data}`;

  return data;
}

class TableItemField extends React.Component<Props> {
  static defaultProps = {
    fixedItemWidth: null,
  };

  itemFieldRenderer(fieldProps, myData = null) {
    if (!fieldProps) return null;

    const {
      isImage,
      isCenter,
      isClickable,
      prefix,
      needBlackBg,
      color,
    } = fieldProps;

    if (isImage) {
      return (
        <a rel="noopener noreferrer" target="_blank" href={myData}>
          <div
            style={[
              styles.tableImage,
              {
                backgroundColor: needBlackBg ? 'rgba(0, 0, 0, 0.6)' : 'transparent',
                backgroundImage: myData ? `url(${myData})` : null,
              },
            ]} />
        </a>
      );
    }

    if (isClickable) return null;

    return (
      <span
        style={[
          styles.tableItem,
          isCenter && styles.textCenter,
          color ? {
            color,
          } : null,
        ]}>
        {Array.isArray(myData)
          ? myData.map(d => (prefix ? `${prefix}${d} ` : `${d} `))
          : myData}
      </span>
    );
  }

  wrapComponentWithContext({
    field,
    wrapData,
    data,
  }) {
    const {
      MutationContext,
      Component,
    } = field.props;

    if (MutationContext) {
      return (
        <MutationContext
          data={data}
          render={(mutate, { loading }) => React.cloneElement(
            <Component />,
            {
              tableField: field,
              tableData: wrapData,
              originData: data,
              mutate: mutate || null,
              loading: loading || false,
            },
          )} />
      );
    }

    return React.cloneElement(
      <Component />,
      {
        tableField: field,
        tableData: wrapData,
        originData: data,
      },
    );
  }

  render() {
    const {
      data,
      field,
      fixedItemWidth,
    } = this.props;

    const {
      fieldKey,
      flex,
      minWidth,
      prefix,
      Component = null,
    } = field.props;

    const wrapData = dataTypeSetter(data[fieldKey], {
      prefix: prefix || null,
    });

    return (
      <div
        style={[
          styles.tableItemPlacement,
          {
            flex: flex || 1,
            minWidth: minWidth || 'auto',
          },
          fixedItemWidth && { width: fixedItemWidth, flex: 'none' },
        ]}>
        {Component ? (
          this.wrapComponentWithContext({
            field,
            wrapData,
            data,
          })
        ) : (
          this.itemFieldRenderer(field.props, wrapData)
        )}
      </div>
    );
  }
}

export default radium(TableItemField);
