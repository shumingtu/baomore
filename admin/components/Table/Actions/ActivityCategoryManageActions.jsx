// @flow
import React, { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';
import { Mutation } from 'react-apollo';
// config
import { DELETE_ACTIVITY_CATEGORY } from '../../../mutations/Activity.js';
import { FETCH_ACTIVITY_CATEGORIES } from '../../../queries/Activity.js';
// components
import EditButton from '../../Global/EditButton.jsx';
import DeleteButton from '../../Global/DeleteButton.jsx';
import Button from '../../Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
  },
};

type Props = {
  id: number,
  history: {
    push: Function,
  },
};

class ActivityCategoryManageActions extends PureComponent<Props> {
  render() {
    const {
      id,
      history,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.btnWrapper}>
          <EditButton
            path={`/activity/official/category/${id}`} />
        </div>
        <div style={styles.btnWrapper}>
          <Mutation
            mutation={DELETE_ACTIVITY_CATEGORY}
            update={(cache, { data: { deleteActivityCategory } }) => {
              const {
                activityCategories,
              } = cache.readQuery({
                query: FETCH_ACTIVITY_CATEGORIES,
              });

              const dIdx = activityCategories.findIndex(l => l.id === deleteActivityCategory.id);

              if (~dIdx) {
                cache.writeQuery({
                  query: FETCH_ACTIVITY_CATEGORIES,
                  data: {
                    activityCategories: [
                      ...activityCategories.slice(0, dIdx),
                      ...activityCategories.slice(dIdx + 1),
                    ],
                  },
                });
              }
            }}>
            {deleteActivityCategory => (
              <DeleteButton
                disabled={false}
                onDelete={async () => {
                  if (id) {
                    await deleteActivityCategory({
                      variables: {
                        id: parseInt(id, 10),
                      },
                    });
                  }
                }} />
            )}
          </Mutation>
        </div>
        <div style={styles.btnWrapper}>
          <Button
            label="查看活動"
            onClick={() => history.push(`/activity/official/${id}/activities`)} />
        </div>
      </div>
    );
  }
}

export default withRouter(ActivityCategoryManageActions);
