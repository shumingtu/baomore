// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// components
import EditButton from '../../Global/EditButton.jsx';
import Button from '../../Global/Button.jsx';

import { toggleWarrantyBox, toggleOrderBox } from '../../../actions/Member.js';

const styles = {
  wrapper: {
    width: 135,
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
    flexDirection: 'column',
  },
};

type Props = {
  id: number,
  match: Object,
  history: Object,
  openWarrantyBox: Function,
  memberWarranties: Array,
  onlineOrders: Array,
  openOrderBox: Function,
};

class MemberManagementActions extends PureComponent<Props> {
  render() {
    const {
      id,
      match: {
        url,
      },
      openOrderBox,
      openWarrantyBox,
      memberWarranties,
      onlineOrders,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.btnWrapper}>
          <EditButton
            path={`${url}/edit/${id}`} />
        </div>
        <div style={styles.btnWrapper}>
          <Button
            disabled={!memberWarranties.length}
            label="保固資料"
            onClick={() => openWarrantyBox(memberWarranties)} />
        </div>
        <div style={styles.btnWrapper}>
          <Button
            disabled={!onlineOrders.length}
            label="訂單資料"
            onClick={() => openOrderBox(onlineOrders)} />
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  () => ({
  }),
  dispatch => bindActionCreators({
    openWarrantyBox: toggleWarrantyBox,
    openOrderBox: toggleOrderBox,
  }, dispatch),
);

export default reduxHook(radium(MemberManagementActions));
