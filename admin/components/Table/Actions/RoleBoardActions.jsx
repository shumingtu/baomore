// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
// components
import EditButton from '../../Global/EditButton.jsx';

import { toggleRoleEditBox } from '../../../actions/Authorization.js';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
  },
};

type Props = {
  id: number,
  name: string,
  actions: array,
  toggleBox: Function,
};

class RoleBoardActions extends Component<Props> {
  render() {
    const {
      id,
      name,
      actions,
      toggleBox,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.btnWrapper}>
          <EditButton
            action={() => toggleBox({
              id,
              name,
              actions,
            })}
            isPopUp />
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  () => ({
  }),
  dispatch => bindActionCreators({
    toggleBox: toggleRoleEditBox,
  }, dispatch),
);

export default reduxHook(RoleBoardActions);
