// @flow
import React, { PureComponent } from 'react';
import { Mutation } from 'react-apollo';
// config
import { DELETE_LANDING } from '../../../mutations/Landing.js';
import { FETCH_LANDINGS } from '../../../queries/Landing.js';
// components
import EditButton from '../../Global/EditButton.jsx';
import DeleteButton from '../../Global/DeleteButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
  },
};

type Props = {
  id: number,
};

class LandingManageActions extends PureComponent<Props> {
  render() {
    const {
      id,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.btnWrapper}>
          <EditButton
            path={`/landing/manage/${id}`} />
        </div>
        <div style={styles.btnWrapper}>
          <Mutation
            mutation={DELETE_LANDING}
            update={(cache, { data: { deleteLanding } }) => {
              const {
                landings,
              } = cache.readQuery({
                query: FETCH_LANDINGS,
              });

              const deletingIdx = landings.findIndex(l => l.id === deleteLanding.id);

              if (~deletingIdx) {
                cache.writeQuery({
                  query: FETCH_LANDINGS,
                  data: {
                    landings: [
                      ...landings.slice(0, deletingIdx),
                      ...landings.slice(deletingIdx + 1),
                    ],
                  },
                });
              }
            }}>
            {deleteLanding => (
              <DeleteButton
                disabled={false}
                onDelete={async () => {
                  if (id) {
                    await deleteLanding({
                      variables: {
                        id: parseInt(id, 10),
                      },
                    });
                  }
                }} />
            )}
          </Mutation>
        </div>
      </div>
    );
  }
}

export default LandingManageActions;
