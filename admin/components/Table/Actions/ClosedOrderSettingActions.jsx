// @flow
import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
// shared
import {
  DELETE_CLOSEDORDERSETTING,
} from '../../../mutations/ClosedOrderSetting.js';
import {
  FETCH_CLOSEDORDERSETTING_LIST,
} from '../../../queries/ClosedOrderSetting.js';
// components
import EditButton from '../../Global/EditButton.jsx';
import DeleteButton from '../../Global/DeleteButton.jsx';

import {
  toggleCreateEditBox,
} from '../../../actions/Invoicing.js';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
  },
};

type Props = {
  id: Number,
  toggleBox: Function,
  closedDay: Number,
  closedTime: String,
  code: String,
};

class ClosedOrderSettingActions extends Component<Props> {
  render() {
    const {
      id,
      toggleBox,
      code,
    } = this.props;

    return (
      <Mutation
        mutation={DELETE_CLOSEDORDERSETTING}
        update={(cache, { data: { deleteClosedOrderSetting } }) => {
          const {
            closedOrderSettings,
          } = cache.readQuery({
            query: FETCH_CLOSEDORDERSETTING_LIST,
          });

          const deletingIdx = closedOrderSettings.findIndex(b => b.id === deleteClosedOrderSetting.id);

          if (~deletingIdx) {
            cache.writeQuery({
              query: FETCH_CLOSEDORDERSETTING_LIST,
              data: {
                closedOrderSettings: [
                  ...closedOrderSettings.slice(0, deletingIdx),
                  ...closedOrderSettings.slice(deletingIdx + 1),
                ],
              },
            });
          }
        }}>
        {(deleteClosedOrderSetting, { loading }) => (
          <div style={styles.wrapper}>
            <div style={styles.btnWrapper}>
              <EditButton
                isPopUp
                action={() => toggleBox({ status: true, targetId: id })} />
            </div>
            <div style={styles.btnWrapper}>
              { code === '統一設定' ? null : (
                <DeleteButton
                  disabled={loading}
                  onDelete={async () => {
                    await deleteClosedOrderSetting({
                      variables: {
                        id: parseInt(id, 10),
                      },
                    });
                  }} />
              )}
            </div>
          </div>
        )}
      </Mutation>
    );
  }
}

const reduxHook = connect(
  () => ({
  }),
  dispatch => bindActionCreators({
    toggleBox: toggleCreateEditBox,
  }, dispatch),
);

export default reduxHook(ClosedOrderSettingActions);
