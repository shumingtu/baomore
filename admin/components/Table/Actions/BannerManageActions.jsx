// @flow
import React, { PureComponent } from 'react';
import { Mutation } from 'react-apollo';
// config
import { DELETE_BANNER } from '../../../mutations/Landing.js';
import { FETCH_LANDING_BANNERS } from '../../../queries/Landing.js';
// components
import EditButton from '../../Global/EditButton.jsx';
import DeleteButton from '../../Global/DeleteButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
  },
};

type Props = {
  id: number,
};

class BannerManageActions extends PureComponent<Props> {
  render() {
    const {
      id,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.btnWrapper}>
          <EditButton
            path={`/landing/banner/${id}`} />
        </div>
        <div style={styles.btnWrapper}>
          <Mutation
            mutation={DELETE_BANNER}
            update={(cache, { data: { deleteBanner } }) => {
              const {
                banners,
              } = cache.readQuery({
                query: FETCH_LANDING_BANNERS,
              });

              const deletingIdx = banners.findIndex(l => l.id === deleteBanner.id);

              if (~deletingIdx) {
                cache.writeQuery({
                  query: FETCH_LANDING_BANNERS,
                  data: {
                    banners: [
                      ...banners.slice(0, deletingIdx),
                      ...banners.slice(deletingIdx + 1),
                    ],
                  },
                });
              }
            }}>
            {deleteBanner => (
              <DeleteButton
                disabled={false}
                onDelete={async () => {
                  if (id) {
                    await deleteBanner({
                      variables: {
                        id: parseInt(id, 10),
                      },
                    });
                  }
                }} />
            )}
          </Mutation>
        </div>
      </div>
    );
  }
}

export default BannerManageActions;
