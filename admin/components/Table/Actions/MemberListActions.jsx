// @flow
import React, { PureComponent } from 'react';
import { Mutation } from 'react-apollo';
import { connect } from 'react-redux';
// shared
import {
  DELETE_MEMBER,
  TOGGLE_ARCHIVE,
} from '../../../mutations/Member.js';
import {
  FETCH_ALL_MEMBER_LIST,
} from '../../../queries/Member.js';
// components
import EditButton from '../../Global/EditButton.jsx';
import DeleteButton from '../../Global/DeleteButton.jsx';
import Button from '../../Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
  },
};

type Props = {
  id: number,
  options: Object,
  archive: boolean,
};

class MemberListActions extends PureComponent<Props> {
  render() {
    const {
      id,
      options,
      archive,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.btnWrapper}>
          <Mutation
            mutation={TOGGLE_ARCHIVE}
            update={(cache, { data: { toggleMemberArchive } }) => {
              const {
                allMemberList,
              } = cache.readQuery({
                query: FETCH_ALL_MEMBER_LIST,
                variables: {
                  ...options,
                  limit: 10,
                  offset: 0,
                },
              });

              const toggleIdx = allMemberList.findIndex(b => b.id === toggleMemberArchive.id);

              if (~toggleIdx) {
                cache.writeQuery({
                  query: FETCH_ALL_MEMBER_LIST,
                  variables: {
                    limit: 10,
                    offset: 0,
                    ...options,
                  },
                  data: {
                    allMemberList: [
                      ...allMemberList.slice(0, toggleIdx),
                      {
                        ...allMemberList[toggleIdx],
                        archive: !allMemberList[toggleIdx].archive,
                      },
                      ...allMemberList.slice(toggleIdx + 1),
                    ],
                  },
                });
              }
            }}>
            {(toggleMemberArchive, { loading }) => (
              <Button
                disabled={loading}
                onClick={async () => {
                  await toggleMemberArchive({
                    variables: {
                      id: parseInt(id, 10),
                    },
                  });
                }}
                style={archive && { backgroundColor: 'gray' }}
                label={archive ? '復職' : '離職'} />
            )}
          </Mutation>
        </div>
        <div style={styles.btnWrapper}>
          <EditButton
            path={`/authorization/member/edit/${id}`} />
        </div>
        <div style={styles.btnWrapper}>
          <Mutation
            mutation={DELETE_MEMBER}
            update={(cache, { data: { deleteMember } }) => {
              const {
                allMemberList,
              } = cache.readQuery({
                query: FETCH_ALL_MEMBER_LIST,
                variables: {
                  ...options,
                  limit: 10,
                  offset: 0,
                },
              });

              const deletingIdx = allMemberList.findIndex(b => b.id === deleteMember.id);

              if (~deletingIdx) {
                cache.writeQuery({
                  query: FETCH_ALL_MEMBER_LIST,
                  variables: {
                    limit: 10,
                    offset: 0,
                    ...options,
                  },
                  data: {
                    allMemberList: [
                      ...allMemberList.slice(0, deletingIdx),
                      ...allMemberList.slice(deletingIdx + 1),
                    ],
                  },
                });
              }
            }}>
            {(deleteMember, { loading }) => (
              <DeleteButton
                disabled={loading}
                onDelete={async () => {
                  await deleteMember({
                    variables: {
                      id: parseInt(id, 10),
                    },
                  });
                }} />
            )}
          </Mutation>
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    options: state.Search.adminMemberList,
  }),
);

export default reduxHook(MemberListActions);
