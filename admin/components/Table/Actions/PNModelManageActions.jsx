// @flow
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  formValueSelector,
} from 'redux-form';
import { Mutation } from 'react-apollo';
// shared
import {
  DELETE_PNCATEGORY,
} from '../../../mutations/PNCategory.js';
import {
  DELETE_PNSUBCATEGORY,
} from '../../../mutations/PNSubCategory.js';
import {
  FETCH_PNCATEGORYBYKEYWORD,
  FETCH_BIGSUBCATEGORIES,
  FETCH_SMALLSUBCATEGORIES,
} from '../../../queries/PNTable.js';
import {
  FORM_PNMODEL_PNCATEGORY_LIST_SEARCHER,
  FORM_PNMODEL_BIGSUBCATEGORY_LIST_SEARCHER,
  FORM_PNMODEL_SMALLSUBCATEGORY_LIST_SEARCHER,
} from '../../../shared/form.js';

// components
import SwitchButton from '../../Global/SwitchButton.jsx';
import DeleteButton from '../../Global/DeleteButton.jsx';
import EditButton from '../../Global/EditButton.jsx';
// actions
import {
  setCurrentSelectedPNCategory,
  setCurrentSelectedBigSubCategory,
  togglePNSubcategoryCreateBox,
  setCurrentSelectedSmallSubCategory,
} from '../../../actions/PNModel.js';

const pnCategorySearcherSelector = formValueSelector(FORM_PNMODEL_PNCATEGORY_LIST_SEARCHER);
const bigCategorySearcherSelector = formValueSelector(FORM_PNMODEL_BIGSUBCATEGORY_LIST_SEARCHER);
const smallCategorySearcherSelector = formValueSelector(FORM_PNMODEL_SMALLSUBCATEGORY_LIST_SEARCHER);

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    padding: '0 6px',
  },
  editBtnWrap: {
    display: 'flex',
    margin: '0 6px',
  },
};

type Props = {
  id: Number,
  name: String,
  currentSelected: Number,
  setSelection: Function,
  DeleteMutationContext: Function,
  isSubCategoryActions?: Boolean,
  toggleEditBox: Function,
  isSmallCategory?: Boolean,
  refetch: Function,
};

function PNCategoryDeleteMutation({
  render,
  selectedKeyword,
}: {
  render: Function,
  selectedKeyword: String,
}) {
  return (
    <Mutation
      mutation={DELETE_PNCATEGORY}
      update={(cache, { data: { deletePNCategory } }) => {
        const {
          pnCategoriesByKeyword,
        } = cache.readQuery({
          query: FETCH_PNCATEGORYBYKEYWORD,
          variables: {
            keyword: selectedKeyword || null,
          },
        });

        const deleteingPNCategoryIdx = pnCategoriesByKeyword.findIndex(b => b.id === deletePNCategory.id);

        if (~deleteingPNCategoryIdx) {
          cache.writeQuery({
            query: FETCH_PNCATEGORYBYKEYWORD,
            variables: {
              keyword: selectedKeyword || null,
            },
            data: {
              pnCategoriesByKeyword: [
                ...pnCategoriesByKeyword.slice(0, deleteingPNCategoryIdx),
                ...pnCategoriesByKeyword.slice(deleteingPNCategoryIdx + 1),
              ],
            },
          });
        }
      }}>
      {(deletePNCategory, { loading }) => render(deletePNCategory, { loading })}
    </Mutation>
  );
}

const PNCategoryDeleteMutationWithRedux = connect(
  state => ({
    selectedKeyword: pnCategorySearcherSelector(state, 'keyword'),
  }),
)(PNCategoryDeleteMutation);

function BigSubCategoryDeleteMutation({
  currentSelectedPNCategory,
  render,
  selectedKeyword,
}: {
  render: Function,
  selectedKeyword: String,
  currentSelectedPNCategory: number,
}) {
  return (
    <Mutation
      mutation={DELETE_PNSUBCATEGORY}
      update={(cache, { data: { deletePNSubCategory } }) => {
        const {
          subCategoriesByPNCategory,
        } = cache.readQuery({
          query: FETCH_BIGSUBCATEGORIES,
          variables: {
            id: parseInt(currentSelectedPNCategory, 10),
            keyword: selectedKeyword || null,
          },
        });

        const deletingIdx = subCategoriesByPNCategory.findIndex(b => b.id === deletePNSubCategory.id);

        if (~deletingIdx) {
          cache.writeQuery({
            query: FETCH_BIGSUBCATEGORIES,
            variables: {
              id: parseInt(currentSelectedPNCategory, 10),
              keyword: selectedKeyword || null,
            },
            data: {
              subCategoriesByPNCategory: [
                ...subCategoriesByPNCategory.slice(0, deletingIdx),
                ...subCategoriesByPNCategory.slice(deletingIdx + 1),
              ],
            },
          });
        }
      }}>
      {(deletePNSubCategory, { loading }) => render(deletePNSubCategory, { loading })}
    </Mutation>
  );
}

const BigSubCategoryDeleteMutationWithRedux = connect(
  state => ({
    currentSelectedPNCategory: state.PNModel.currentSelectedPNCategory,
    selectedKeyword: bigCategorySearcherSelector(state, 'keyword'),
  }),
)(BigSubCategoryDeleteMutation);

function SmallSubCategoryDeleteMutation({
  currentSelectedBigSubCategory,
  render,
  selectedKeyword,
}: {
  render: Function,
  currentSelectedBigSubCategory: number,
  selectedKeyword: String,
}) {
  return (
    <Mutation
      mutation={DELETE_PNSUBCATEGORY}
      update={(cache, { data: { deletePNSubCategory } }) => {
        const {
          subCategoriesBySubCategory,
        } = cache.readQuery({
          query: FETCH_SMALLSUBCATEGORIES,
          variables: {
            id: parseInt(currentSelectedBigSubCategory, 10),
            keyword: selectedKeyword || null,
          },
        });

        const deletingIdx = subCategoriesBySubCategory.findIndex(b => b.id === deletePNSubCategory.id);

        if (~deletingIdx) {
          cache.writeQuery({
            query: FETCH_SMALLSUBCATEGORIES,
            variables: {
              id: parseInt(currentSelectedBigSubCategory, 10),
              keyword: selectedKeyword || null,
            },
            data: {
              subCategoriesBySubCategory: [
                ...subCategoriesBySubCategory.slice(0, deletingIdx),
                ...subCategoriesBySubCategory.slice(deletingIdx + 1),
              ],
            },
          });
        }
      }}>
      {(deletePNSubCategory, { loading }) => render(deletePNSubCategory, { loading })}
    </Mutation>
  );
}

const SmallSubCategoryDeleteMutationWithRedux = connect(
  state => ({
    currentSelectedBigSubCategory: state.PNModel.currentSelectedBigSubCategory,
    selectedKeyword: smallCategorySearcherSelector(state, 'keyword'),
  }),
)(SmallSubCategoryDeleteMutation);

class PNModelManageActions extends PureComponent<Props> {
  static defaultProps = {
    isSubCategoryActions: false,
    isSmallCategory: false,
  };

  render() {
    const {
      id,
      currentSelected,
      setSelection,
      DeleteMutationContext,
      toggleEditBox,
      isSubCategoryActions,
      isSmallCategory,
      refetch,
    } = this.props;

    const isSelected = (currentSelected && currentSelected === id) || false;

    if (DeleteMutationContext) {
      return (
        <DeleteMutationContext render={(deleteMutate, { loading }) => (
          <div style={styles.wrapper}>
            <div style={styles.btnWrapper}>
              { isSubCategoryActions ? (
                <div style={styles.editBtnWrap}>
                  <EditButton
                    isPopUp
                    action={() => toggleEditBox({ status: true, targetId: id, isSmallCategory })} />
                </div>
              ) : null }
              <DeleteButton
                disabled={loading}
                onDelete={async () => {
                  if (id) {
                    await deleteMutate({
                      variables: {
                        id: parseInt(id, 10),
                      },
                    });
                    refetch();
                  }
                }} />
            </div>
            { isSmallCategory ? null : (
              <SwitchButton
                activeLabel="取消選擇"
                inactiveLabel="選擇"
                isSelected={isSelected}
                onClick={setSelection
                  ? () => setSelection(!isSelected ? id : null)
                  : null} />
            )}
          </div>
        )} />
      );
    }

    return (
      <div style={styles.wrapper}>
        <SwitchButton
          activeLabel="取消選擇"
          inactiveLabel="選擇"
          isSelected={isSelected}
          onClick={setSelection
            ? () => setSelection(!isSelected ? id : null)
            : null} />
      </div>
    );
  }
}

export default PNModelManageActions;

export function wrapManageActionsIntoPNCategory() {
  return connect(
    state => ({
      currentSelected: state.PNModel.currentSelectedPNCategory,
      DeleteMutationContext: PNCategoryDeleteMutationWithRedux,
    }),
    dispatch => bindActionCreators({
      setSelection: setCurrentSelectedPNCategory,
    }, dispatch)
  )(PNModelManageActions);
}

export function wrapManageActionsIntoBigSubCategory() {
  return connect(
    state => ({
      currentSelected: state.PNModel.currentSelectedBigSubCategory,
      DeleteMutationContext: BigSubCategoryDeleteMutationWithRedux,
    }),
    dispatch => bindActionCreators({
      toggleEditBox: togglePNSubcategoryCreateBox,
      setSelection: setCurrentSelectedBigSubCategory,
    }, dispatch)
  )(PNModelManageActions);
}

export function wrapManageActionsIntoSmallSubCategory() {
  return connect(
    state => ({
      currentSelected: state.PNModel.currentSelectedSmallSubCategory,
      DeleteMutationContext: SmallSubCategoryDeleteMutationWithRedux,
    }),
    dispatch => bindActionCreators({
      toggleEditBox: togglePNSubcategoryCreateBox,
      setSelection: setCurrentSelectedSmallSubCategory,
    }, dispatch)
  )(PNModelManageActions);
}
