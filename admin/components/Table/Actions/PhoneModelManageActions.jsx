// @flow
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Mutation } from 'react-apollo';
// shared
import {
  DELETE_BRAND,
  DELETE_BRAND_MODEL,
} from '../../../mutations/Brand.js';
import {
  FETCH_BRAND_LIST,
  FETCH_BRAND_MODEL_LIST,
} from '../../../queries/Brand.js';
// components
import SwitchButton from '../../Global/SwitchButton.jsx';
import DeleteButton from '../../Global/DeleteButton.jsx';
// actions
import {
  setCurrentSelectedBrand,
  setCurrentSelectedModel,
} from '../../../actions/PhoneModel.js';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    padding: '0 6px',
  },
};

type Props = {
  id: Number,
  name: String,
  currentSelected: Number,
  setSelection: Function,
  DeleteMutationContext: Function,
};

function BrandDeleteMutation({
  render,
}: {
  render: Function,
}) {
  return (
    <Mutation
      mutation={DELETE_BRAND}
      update={(cache, { data: { deleteBrand } }) => {
        const {
          brandList,
        } = cache.readQuery({
          query: FETCH_BRAND_LIST,
        });

        const deleteingBrandIdx = brandList.findIndex(b => b.id === deleteBrand.id);

        if (~deleteingBrandIdx) {
          cache.writeQuery({
            query: FETCH_BRAND_LIST,
            data: {
              brandList: [
                ...brandList.slice(0, deleteingBrandIdx),
                ...brandList.slice(deleteingBrandIdx + 1),
              ],
            },
          });
        }
      }}>
      {(deleteBrand, { loading }) => render(deleteBrand, { loading })}
    </Mutation>
  );
}

function BrandModelDeleteMutation({
  currentSelectedBrand,
  render,
}: {
  render: Function,
  currentSelectedBrand: number,
}) {
  return (
    <Mutation
      mutation={DELETE_BRAND_MODEL}
      update={(cache, { data: { deleteBrandModel } }) => {
        const {
          brandModelList,
        } = cache.readQuery({
          query: FETCH_BRAND_MODEL_LIST,
          variables: {
            brandId: currentSelectedBrand,
          },
        });

        const deletingIdx = brandModelList.findIndex(b => b.id === deleteBrandModel.id);

        if (~deletingIdx) {
          cache.writeQuery({
            query: FETCH_BRAND_MODEL_LIST,
            variables: {
              brandId: currentSelectedBrand,
            },
            data: {
              brandModelList: [
                ...brandModelList.slice(0, deletingIdx),
                ...brandModelList.slice(deletingIdx + 1),
              ],
            },
          });
        }
      }}>
      {(deleteBrandModel, { loading }) => render(deleteBrandModel, { loading })}
    </Mutation>
  );
}

const BrandModelDeleteMutationWithRedux = connect(
  state => ({
    currentSelectedBrand: state.PhoneModel.currentSelectedBrand,
  }),
)(BrandModelDeleteMutation);

class PhoneModelManageActions extends PureComponent<Props> {
  render() {
    const {
      id,
      currentSelected,
      setSelection,
      DeleteMutationContext,
    } = this.props;

    const isSelected = (currentSelected && currentSelected === id) || false;

    if (DeleteMutationContext) {
      return (
        <DeleteMutationContext render={(deleteMutate, { loading }) => (
          <div style={styles.wrapper}>
            <div style={styles.btnWrapper}>
              <DeleteButton
                disabled={loading}
                onDelete={async () => {
                  if (id) {
                    await deleteMutate({
                      variables: {
                        id: parseInt(id, 10),
                      },
                    });
                  }
                }} />
            </div>
            <SwitchButton
              activeLabel="取消選擇"
              inactiveLabel="選擇"
              isSelected={isSelected}
              onClick={setSelection
                ? () => setSelection(!isSelected ? id : null)
                : null} />
          </div>
        )} />
      );
    }

    return (
      <div style={styles.wrapper}>
        <SwitchButton
          activeLabel="取消選擇"
          inactiveLabel="選擇"
          isSelected={isSelected}
          onClick={setSelection
            ? () => setSelection(!isSelected ? id : null)
            : null} />
      </div>
    );
  }
}

export default PhoneModelManageActions;

export function wrapManageActionsIntoBrand() {
  return connect(
    state => ({
      currentSelected: state.PhoneModel.currentSelectedBrand,
      DeleteMutationContext: BrandDeleteMutation,
    }),
    dispatch => bindActionCreators({
      setSelection: setCurrentSelectedBrand,
    }, dispatch)
  )(PhoneModelManageActions);
}

export function wrapManageActionsIntoModel() {
  return connect(
    state => ({
      currentSelected: state.PhoneModel.currentSelectedModel,
      DeleteMutationContext: BrandModelDeleteMutationWithRedux,
    }),
    dispatch => bindActionCreators({
      setSelection: setCurrentSelectedModel,
    }, dispatch)
  )(PhoneModelManageActions);
}
