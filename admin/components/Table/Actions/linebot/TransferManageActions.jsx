// @flow
import React, { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';

import detailIcon from '../../../../static/images/detail-icon.png';
import transferIcon from '../../../../static/images/transfer-icon.png';
// components
import ActionButton from './Element/ActionButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
  },
};

type Props = {
  id: number,
  history: {
    push: Function,
  },
  match: {
    url: string,
  },
};

class TransferManageActions extends PureComponent<Props> {
  render() {
    const {
      id,
      match: {
        url,
      },
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.btnWrapper}>
          <ActionButton
            icon={detailIcon}
            path={`${url}/detail/${id}`} />
        </div>
        <div style={styles.btnWrapper}>
          <ActionButton
            icon={transferIcon}
            path={`${url}/action/${id}`} />
        </div>
      </div>
    );
  }
}

export default withRouter(TransferManageActions);
