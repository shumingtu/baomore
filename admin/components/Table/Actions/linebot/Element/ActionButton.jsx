// @flow
import React, { memo } from 'react';
import radium from 'radium';
import { withRouter } from 'react-router-dom';

const styles = {
  wrapper: {
    width: 24,
    height: 24,
    padding: 0,
    margin: 0,
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    cursor: 'pointer',
    ':hover': {
      opacity: 0.9,
    },
    '@media (max-width: 767px)': {
      width: 20,
      height: 20,
    },
  },
  disabled: {
    opacity: 0.3,
  },
};

function ActionButton({
  icon,
  onClick,
  path,
  disabled = false,
  history,
  doActionAndChangePath,
}: {
  icon: string,
  path: string,
  disabled: boolean,
  history: {
    push: Function,
  },
  onClick?: Function,
  doActionAndChangePath?: boolean,
}) {
  return (
    <button
      type="button"
      disabled={disabled}
      onClick={() => {
        if (onClick) {
          onClick();

          if (doActionAndChangePath) {
            history.push(path);
          }
          return;
        }
        history.push(path);
      }}
      style={[
        styles.wrapper,
        {
          backgroundImage: icon ? `url(${icon})` : null,
        },
        disabled && styles.disabled,
      ]} />
  );
}

ActionButton.defaultProps = {
  onClick: null,
  doActionAndChangePath: false,
};

export default memo(
  withRouter(
    radium(
      ActionButton
    )
  )
);
