// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

import Theme from '../../../../styles/Theme.js';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 16,
    margin: 0,
    outline: 0,
    border: 0,
    backgroundColor: 'transparent',
    cursor: 'pointer',
  },
  disabled: {
    cursor: 'default',
  },
  name: {
    fontSize: 13,
    letterSpacing: 1,
    color: 'rgb(64, 97, 238)',
    whiteSpace: 'pre-line',
  },
  empty: {
    fontSize: 13,
    letterSpacing: 1,
    color: '#000',
    whiteSpace: 'pre-line',
  },
};

type Props = {
  order: {
    id: number,
    name: string,
  },
  onClick: Function,
};

class ScheduleManageActions extends PureComponent<Props> {
  render() {
    const {
      order,
      onClick,
    } = this.props;

    const hasOrder = (order && order.id) || false;

    return (
      <div style={styles.wrapper}>
        <button
          type="button"
          disabled={!hasOrder}
          onClick={onClick ? () => onClick(order) : null}
          style={[
            styles.btnWrapper,
            !hasOrder && styles.disabled,
          ]}>
          {hasOrder ? (
            <span style={styles.name}>
              {order.name || null}
            </span>
          ) : (
            <span style={styles.empty}>
              尚無行程
            </span>
          )}
        </button>
      </div>
    );
  }
}

export default radium(ScheduleManageActions);
