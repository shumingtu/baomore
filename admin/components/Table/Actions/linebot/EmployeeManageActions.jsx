// @flow
import React, { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';

import detailIcon from '../../../../static/images/detail-icon.png';
// components
import ActionButton from './Element/ActionButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
  },
};

type Props = {
  id: number,
  history: {
    push: Function,
  },
  match: {
    url: string,
  },
  onClick: Function,
};

class EmployeeManageActions extends PureComponent<Props> {
  render() {
    const {
      onClick,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.btnWrapper}>
          <ActionButton
            icon={detailIcon}
            onClick={() => onClick(this.props)} />
        </div>
      </div>
    );
  }
}

export default withRouter(EmployeeManageActions);
