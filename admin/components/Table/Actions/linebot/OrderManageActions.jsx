// @flow
import React, { PureComponent } from 'react';
import qs from 'qs';
import { withRouter } from 'react-router-dom';

import detailIcon from '../../../../static/images/detail-icon.png';
import popupIcon from '../../../../static/images/popup-icon.png';
import editIcon from '../../../../static/images/edit-icon.png';
import qrcodeIcon from '../../../../static/images/qrcodeIcon.png';
// components
import ActionButton from './Element/ActionButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
  },
};

type Props = {
  id: number, // order id
  name: string,
  pnTableId: number,
  quantity: number,
  status: string,
  store?: {
    id: number,
    name: string,
    channel: {
      id: number,
      name: string,
    },
    district: {
      id: number,
      name: string,
      city: {
        id: number,
        name: string,
      },
    },
  },
  history: {
    push: Function,
  },
  match: {
    url: string,
  },
  closeDate: string,
  cacheCloseDate: Function,
};

class OrderManageActions extends PureComponent<Props> {
  static defaultProps = {
    store: null,
  };

  render() {
    const {
      id,
      name,
      quantity,
      pnTableId,
      status,
      store,
      match: {
        url,
      },
      closeDate,
      cacheCloseDate,
    } = this.props;

    const orderAmountEditable = status === '訂貨中';
    // 訂貨中不會有商品序號
    const orderCheckable = status === '已出貨';
    const storeId = (store && store.id) || '';
    const channelId = (store && store.channel && store.channel.id) || '';
    const cityId = (store && store.district && store.district.city && store.district.city.id) || '';

    const qsString = qs.stringify(qs.parse({
      qty: quantity,
      name,
      storeId,
      channelId,
      cityId,
    }), { skipNulls: true });

    return (
      <div style={styles.wrapper}>
        <div style={styles.btnWrapper}>
          <ActionButton
            doActionAndChangePath
            onClick={() => cacheCloseDate(closeDate)}
            icon={detailIcon}
            path={`${url}/detail/${pnTableId}`} />
        </div>
        {orderAmountEditable ? (
          <div style={styles.btnWrapper}>
            <ActionButton
              icon={editIcon}
              path={`${url}/action/edit/${id}?${qsString}`} />
          </div>
        ) : (
          <div style={styles.btnWrapper}>
            <ActionButton
              icon={qrcodeIcon}
              path={`${url}/qrcodes/${id}`} />
          </div>
        )}
        {orderCheckable ? (
          <div style={styles.btnWrapper}>
            <ActionButton
              icon={popupIcon}
              path="/linebot/checkInventory" />
          </div>
        ) : null}
      </div>
    );
  }
}

export default withRouter(OrderManageActions);
