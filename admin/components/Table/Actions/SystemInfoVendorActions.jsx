// @flow
import React, { PureComponent } from 'react';
import { Mutation } from 'react-apollo';
// shared
import {
  DELETE_VENDOR,
} from '../../../mutations/Vendor.js';
import {
  FETCH_VENDOR_LIST,
} from '../../../queries/Global.js';
// components
import EditButton from '../../Global/EditButton.jsx';
import DeleteButton from '../../Global/DeleteButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
  },
};

type Props = {
  id: number,
  match: Object,
};

class SystemInfoVendorActions extends PureComponent<Props> {
  render() {
    const {
      id,
      match: {
        url,
      },
    } = this.props;

    return (
      <Mutation
        mutation={DELETE_VENDOR}
        update={(cache, { data: { deleteVendor } }) => {
          const {
            vendorList,
          } = cache.readQuery({
            query: FETCH_VENDOR_LIST,
            variables: {
              limit: 10,
              offset: 0,
            },
          });

          const deletingIdx = vendorList.findIndex(b => b.id === deleteVendor.id);

          if (~deletingIdx) {
            cache.writeQuery({
              query: FETCH_VENDOR_LIST,
              variables: {
                limit: 10,
                offset: 0,
              },
              data: {
                vendorList: [
                  ...vendorList.slice(0, deletingIdx),
                  ...vendorList.slice(deletingIdx + 1),
                ],
              },
            });
          }
        }}>
        {(deleteStore, { loading }) => (
          <div style={styles.wrapper}>
            <div style={styles.btnWrapper}>
              <EditButton
                path={`${url}/edit/${id}`} />
            </div>
            <div style={styles.btnWrapper}>
              <DeleteButton
                disabled={loading}
                onDelete={async () => {
                  await deleteStore({
                    variables: {
                      id: parseInt(id, 10),
                    },
                  });
                }} />
            </div>
          </div>
        )}
      </Mutation>
    );
  }
}


export default SystemInfoVendorActions;
