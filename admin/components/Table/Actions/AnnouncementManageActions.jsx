// @flow
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Mutation } from 'react-apollo';
// config
import { DELETE_ANNOUNCEMENT } from '../../../mutations/Announcement.js';
import { FETCH_ANNOUNCEMENT_LIST } from '../../../queries/Announcement.js';
// components
import EditButton from '../../Global/EditButton.jsx';
import DeleteButton from '../../Global/DeleteButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
  },
};

type Props = {
  id: number,
  history: {
    push: Function,
  },
  match: {
    params: {
      categoryId: string,
    },
  },
  location: {
    pathname: string,
  },
  searchOptions: {
    keyword?: string,
    startDate?: string,
    endDate?: string,
  },
};

class AnnouncementManageActions extends PureComponent<Props> {
  render() {
    const {
      id,
      location: {
        pathname,
      },
      searchOptions,
    } = this.props;

    const queryOptions = {
      offset: 0,
      limit: 10,
    };

    if (searchOptions) {
      const {
        keyword,
        startDate,
        endDate,
      } = searchOptions;

      if (keyword) queryOptions.keyword = keyword;
      if (startDate) queryOptions.startDate = startDate;
      if (endDate) queryOptions.endDate = endDate;
    }

    return (
      <div style={styles.wrapper}>
        <div style={styles.btnWrapper}>
          <EditButton
            path={`${pathname}/${id}/edit`} />
        </div>
        <div style={styles.btnWrapper}>
          <Mutation
            mutation={DELETE_ANNOUNCEMENT}
            update={(cache, { data: { deleteAnnouncement } }) => {
              const {
                announcements,
              } = cache.readQuery({
                query: FETCH_ANNOUNCEMENT_LIST,
                variables: queryOptions,
              });

              const dIdx = announcements.findIndex(l => l.id === deleteAnnouncement.id);

              if (~dIdx) {
                cache.writeQuery({
                  query: FETCH_ANNOUNCEMENT_LIST,
                  variables: queryOptions,
                  data: {
                    announcements: [
                      ...announcements.slice(0, dIdx),
                      ...announcements.slice(dIdx + 1),
                    ],
                  },
                });
              }
            }}>
            {deleteAnnouncement => (
              <DeleteButton
                disabled={false}
                onDelete={async () => {
                  if (id) {
                    await deleteAnnouncement({
                      variables: {
                        id: parseInt(id, 10),
                      },
                    });
                  }
                }} />
            )}
          </Mutation>
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    searchOptions: state.Search.adminAnnouncements,
  }),
);

export default withRouter(
  reduxHook(
    AnnouncementManageActions
  )
);
