// @flow
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Mutation } from 'react-apollo';
// shared
import {
  DELETE_BRAND_MODEL_COLOR,
} from '../../../mutations/Brand.js';
import {
  FETCH_MODEL_COLOR_LIST,
} from '../../../queries/Brand.js';
// components
import EditButton from '../../Global/EditButton.jsx';
import DeleteButton from '../../Global/DeleteButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
  },
};

type Props = {
  id: number,
  name: string,
  assignModelId: number,
  currentSelectedModel: number,
};

class PhoneColorManageActions extends PureComponent<Props> {
  render() {
    const {
      id,
      assignModelId,
      currentSelectedModel,
    } = this.props;

    return (
      <Mutation
        mutation={DELETE_BRAND_MODEL_COLOR}
        update={(cache, { data: { deleteBrandModelColor } }) => {
          const {
            brandModelColorList,
          } = cache.readQuery({
            query: FETCH_MODEL_COLOR_LIST,
            variables: {
              modelId: assignModelId || currentSelectedModel,
            },
          });

          const deletingIdx = brandModelColorList.findIndex(b => b.id === deleteBrandModelColor.id);

          if (~deletingIdx) {
            cache.writeQuery({
              query: FETCH_MODEL_COLOR_LIST,
              variables: {
                modelId: assignModelId || currentSelectedModel,
              },
              data: {
                brandModelColorList: [
                  ...brandModelColorList.slice(0, deletingIdx),
                  ...brandModelColorList.slice(deletingIdx + 1),
                ],
              },
            });
          }
        }}>
        {(deleteBrandModelColor, { loading }) => (
          <div style={styles.wrapper}>
            <div style={styles.btnWrapper}>
              <EditButton
                path={`/phoneModel/${id}/edit`} />
            </div>
            <div style={styles.btnWrapper}>
              <DeleteButton
                disabled={loading}
                onDelete={async () => {
                  await deleteBrandModelColor({
                    variables: {
                      id: parseInt(id, 10),
                    },
                  });
                }} />
            </div>
          </div>
        )}
      </Mutation>
    );
  }
}

const reduxHook = connect(
  state => ({
    currentSelectedModel: state.PhoneModel.currentSelectedModel,
  }),
);

export default reduxHook(PhoneColorManageActions);
