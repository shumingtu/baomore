// @flow
import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import { connect } from 'react-redux';
// shared
import {
  DELETE_PNTABLE_ITEM,
} from '../../../mutations/PNTable.js';
import {
  FETCH_PN_LIST,
} from '../../../queries/PNTable.js';
// components
import EditButton from '../../Global/EditButton.jsx';
import DeleteButton from '../../Global/DeleteButton.jsx';
import LookOverButton from '../../Global/LookOverButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
  },
};

type Props = {
  id: number,
  name: string,
  assignModelId: number,
  currentSelectedModel: number,
  code: string,
  pnCategory: string,
  bigSubCategory: string,
  smallSubCategory: string,
  price: string,
  vendor: string,
  suitableDevice: string,
  description: string,
  picture: string,
  options: object,
};

class PNModelActions extends Component<Props> {
  render() {
    const {
      id,
      code,
      pnCategory,
      bigSubCategory,
      smallSubCategory,
      name,
      price,
      vendor,
      suitableDevice,
      picture,
      description,
      options,
    } = this.props;

    const boxData = {
      id,
      code,
      pnCategory,
      bigSubCategory,
      smallSubCategory,
      name,
      price,
      vendor,
      suitableDevice,
      description,
      picture,
    };

    return (
      <Mutation
        mutation={DELETE_PNTABLE_ITEM}
        update={(cache, { data: { deletePNTableItem } }) => {
          const {
            pntableList,
          } = cache.readQuery({
            query: FETCH_PN_LIST,
            variables: options,
          });

          const deletingIdx = pntableList.findIndex(b => b.id === deletePNTableItem.id);

          if (~deletingIdx) {
            cache.writeQuery({
              query: FETCH_PN_LIST,
              variables: options,
              data: {
                pntableList: [
                  ...pntableList.slice(0, deletingIdx),
                  ...pntableList.slice(deletingIdx + 1),
                ],
              },
            });
          }
        }}>
        {(deletePNTableItem, { loading }) => (
          <div style={styles.wrapper}>
            <div style={styles.btnWrapper}>
              <LookOverButton
                data={boxData} />
            </div>
            <div style={styles.btnWrapper}>
              <EditButton
                path={`/pnModel/${id}/edit`} />
            </div>
            <div style={styles.btnWrapper}>
              <DeleteButton
                disabled={loading}
                onDelete={async () => {
                  await deletePNTableItem({
                    variables: {
                      id: parseInt(id, 10),
                    },
                  });
                }} />
            </div>
          </div>
        )}
      </Mutation>
    );
  }
}

const reduxHook = connect(
  state => ({
    options: state.PNModel.listSearchOptions,
  }),
);

export default reduxHook(PNModelActions);
