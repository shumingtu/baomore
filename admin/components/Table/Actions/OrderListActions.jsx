// @flow
import React, { Component, Fragment } from 'react';
import { Mutation } from 'react-apollo';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
// components
import EditButton from '../../Global/EditButton.jsx';
import DeleteButton from '../../Global/DeleteButton.jsx';
import LookOverButton from '../../Global/LookOverButton.jsx';
import ActionButton from './linebot/Element/ActionButton.jsx';

import {
  DELETE_ORDER,
} from '../../../mutations/Order.js';
import { GET_ORDER_LIST } from '../../../queries/Order.js';

import { toggleOrderEditBox } from '../../../actions/Order.js';
import qrcodeIcon from '../../../static/images/qrcodeIcon.png';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
  },
};

type Props = {
  id: number,
  pnTableId: number,
  pnTableName: string,
  code: string,
  pnCategory: string,
  bigSubCategory: string,
  smallSubCategory: string,
  price: string,
  vendor: string,
  suitableDevice: string,
  description: string,
  picture: string,
  status: string,
  options: object,
  toggleBox: Function,
  quantity: number,
  storeId: number,
  channelId: number,
  districtId: number,
  url: string,
};

const DeleteMutation = ({ options, targetId }: { options: object, targetId: number }) => (
  <Mutation
    mutation={DELETE_ORDER}
    update={(cache, { data: { deleteOrderByAdmin } }) => {
      const {
        orderList,
      } = cache.readQuery({
        query: GET_ORDER_LIST,
        variables: options,
      });

      const deletingIdx = orderList.findIndex(b => b.id === deleteOrderByAdmin.id);

      if (~deletingIdx) {
        cache.writeQuery({
          query: GET_ORDER_LIST,
          variables: options,
          data: {
            orderList: [
              ...orderList.slice(0, deletingIdx),
              ...orderList.slice(deletingIdx + 1),
            ],
          },
        });
      }
    }}>
    {(deleteOrderByAdmin, { loading }) => (
      <div style={styles.btnWrapper}>
        <DeleteButton
          disabled={loading}
          onDelete={async () => {
            await deleteOrderByAdmin({
              variables: {
                orderId: parseInt(targetId, 10),
              },
            });
          }} />
      </div>
    )}
  </Mutation>
);

class OrderListActions extends Component<Props> {
  render() {
    const {
      id,
      pnTableId,
      code,
      pnCategory,
      bigSubCategory,
      smallSubCategory,
      pnTableName,
      price,
      vendor,
      suitableDevice,
      picture,
      description,
      status,
      options,
      toggleBox,
      quantity,
      storeId,
      channelId,
      districtId,
      url,
    } = this.props;

    const boxData = {
      id: pnTableId,
      code,
      pnCategory,
      bigSubCategory,
      smallSubCategory,
      name: pnTableName,
      price,
      vendor,
      suitableDevice,
      description,
      picture,
    };

    return (
      <div style={styles.wrapper}>
        <div style={styles.btnWrapper}>
          <LookOverButton
            data={boxData} />
        </div>
        {status === '訂貨中' || status === '等待出貨' ? (
          <Fragment>
            <div style={styles.btnWrapper}>
              <EditButton
                action={() => toggleBox({
                  orderId: id,
                  code,
                  pnTableName,
                  pnCategory,
                  channelId,
                  districtId,
                  storeId,
                  quantity,
                })}
                isPopUp />
            </div>
            <DeleteMutation options={options} targetId={id} />
          </Fragment>
        ) : null}
        {status !== '訂貨中' ? (
          <div style={styles.btnWrapper}>
            <ActionButton
              icon={qrcodeIcon}
              path={`${url}/qrcodes/${id}`} />
          </div>
        ) : null}
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    options: state.Search.adminOrderList,
  }),
  dispatch => bindActionCreators({
    toggleBox: toggleOrderEditBox,
  }, dispatch),
);

export default reduxHook(OrderListActions);
