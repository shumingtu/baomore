// @flow
import React, { PureComponent } from 'react';
import { Mutation } from 'react-apollo';
import { connect } from 'react-redux';
// shared
import {
  DELETE_STORE,
  TOGGLE_ARCHIVE,
} from '../../../mutations/Store.js';
import {
  FETCH_STORES_LIST,
} from '../../../queries/Store.js';
// components
import EditButton from '../../Global/EditButton.jsx';
import DeleteButton from '../../Global/DeleteButton.jsx';
import Button from '../../Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
  },
};

type Props = {
  id: number,
  match: Object,
  archive: boolean,
  options: Object,
};

class SystemInfoStoreActions extends PureComponent<Props> {
  render() {
    const {
      id,
      match: {
        url,
      },
      archive,
      options,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <Mutation
          mutation={TOGGLE_ARCHIVE}
          update={(cache, { data: { toggleStoreArchive } }) => {
            const {
              stores,
            } = cache.readQuery({
              query: FETCH_STORES_LIST,
              variables: {
                limit: 10,
                offset: 0,
                keyword: (options && options.keyword) || null,
              },
            });

            const toggleIdx = stores.findIndex(s => s.id === toggleStoreArchive.id);

            if (~toggleIdx) {
              cache.writeQuery({
                query: FETCH_STORES_LIST,
                variables: {
                  limit: 10,
                  offset: 0,
                  keyword: (options && options.keyword) || null,
                },
                data: {
                  stores: [
                    ...stores.slice(0, toggleIdx),
                    {
                      ...stores[toggleIdx],
                      archive: !stores[toggleIdx].archive,
                    },
                    ...stores.slice(toggleIdx + 1),
                  ],
                },
              });
            }
          }}>
          {(toggleStoreArchive, { loading }) => (
            <div style={styles.btnWrapper}>
              <Button
                disabled={loading}
                onClick={async () => {
                  await toggleStoreArchive({
                    variables: {
                      id: parseInt(id, 10),
                    },
                  });
                }}
                style={archive && { backgroundColor: 'gray' }}
                label={archive ? '復店' : '閉店'} />
            </div>
          )}
        </Mutation>
        <div style={styles.btnWrapper}>
          <EditButton
            path={`${url}/edit/${id}`} />
        </div>
        <Mutation
          mutation={DELETE_STORE}
          update={(cache, { data: { deleteStore } }) => {
            const {
              stores,
            } = cache.readQuery({
              query: FETCH_STORES_LIST,
              variables: {
                limit: 10,
                offset: 0,
              },
            });

            const deletingIdx = stores.findIndex(b => b.id === deleteStore.id);

            if (~deletingIdx) {
              cache.writeQuery({
                query: FETCH_STORES_LIST,
                variables: {
                  limit: 10,
                  offset: 0,
                },
                data: {
                  stores: [
                    ...stores.slice(0, deletingIdx),
                    ...stores.slice(deletingIdx + 1),
                  ],
                },
              });
            }
          }}>
          {(deleteStore, { loading }) => (
            <div style={styles.btnWrapper}>
              <DeleteButton
                disabled={loading}
                onDelete={async () => {
                  await deleteStore({
                    variables: {
                      id: parseInt(id, 10),
                    },
                  });
                }} />
            </div>
          )}
        </Mutation>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    options: state.Search.adminStoreList,
  }),
);


export default reduxHook(SystemInfoStoreActions);
