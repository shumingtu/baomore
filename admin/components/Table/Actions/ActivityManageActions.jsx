// @flow
import React, { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';
import { Mutation } from 'react-apollo';
// config
import { DELETE_ACTIVITY, TOGGLE_ACTIVITY } from '../../../mutations/Activity.js';
import { FETCH_ACTIVITIES } from '../../../queries/Activity.js';
// components
import EditButton from '../../Global/EditButton.jsx';
import DeleteButton from '../../Global/DeleteButton.jsx';
import Button from '../../Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
  },
};

type Props = {
  id: number,
  history: {
    push: Function,
  },
  match: {
    params: {
      categoryId: string,
    },
  },
  location: {
    pathname: string,
  },
  isActived: boolean,
};


class ActivityCategoryManageActions extends PureComponent<Props> {
  render() {
    const {
      id,
      location: {
        pathname,
      },
      match: {
        params: {
          categoryId,
        },
      },
      isActived,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.btnWrapper}>
          <Mutation
            update={(cache, { data: { toggleActivity } }) => {
              const {
                activities,
              } = cache.readQuery({
                query: FETCH_ACTIVITIES,
                variables: {
                  categoryId: categoryId ? parseInt(categoryId, 10) : -1,
                },
              });

              const tIdx = activities.findIndex(l => l.id === toggleActivity.id);

              if (~tIdx) {
                activities[tIdx].isActived = !activities[tIdx].isActived;
                cache.writeQuery({
                  query: FETCH_ACTIVITIES,
                  variables: {
                    categoryId: categoryId ? parseInt(categoryId, 10) : -1,
                  },
                  data: {
                    activities: [
                      ...activities,
                    ],
                  },
                });
              }
            }}
            mutation={TOGGLE_ACTIVITY}>
            {toggleActivity => (
              <div style={styles.btnWrapper}>
                <Button
                  onClick={async () => {
                    await toggleActivity({
                      variables: {
                        id: parseInt(id, 10),
                      },
                    });
                  }}
                  label={isActived ? '下架' : '上架'} />
              </div>
            )}
          </Mutation>
        </div>
        <div style={styles.btnWrapper}>
          <EditButton
            path={`${pathname}/${id}`} />
        </div>
        <div style={styles.btnWrapper}>
          <Mutation
            mutation={DELETE_ACTIVITY}
            update={(cache, { data: { deleteActivity } }) => {
              const {
                activities,
              } = cache.readQuery({
                query: FETCH_ACTIVITIES,
                variables: {
                  categoryId: categoryId ? parseInt(categoryId, 10) : -1,
                },
              });

              const dIdx = activities.findIndex(l => l.id === deleteActivity.id);

              if (~dIdx) {
                cache.writeQuery({
                  query: FETCH_ACTIVITIES,
                  variables: {
                    categoryId: categoryId ? parseInt(categoryId, 10) : -1,
                  },
                  data: {
                    activities: [
                      ...activities.slice(0, dIdx),
                      ...activities.slice(dIdx + 1),
                    ],
                  },
                });
              }
            }}>
            {deleteActivity => (
              <DeleteButton
                disabled={false}
                onDelete={async () => {
                  if (id) {
                    await deleteActivity({
                      variables: {
                        id: parseInt(id, 10),
                      },
                    });
                  }
                }} />
            )}
          </Mutation>
        </div>
      </div>
    );
  }
}

export default withRouter(ActivityCategoryManageActions);
