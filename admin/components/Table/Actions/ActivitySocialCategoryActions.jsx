// @flow
import React, { PureComponent } from 'react';

import EditButton from '../../Global/EditButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
  },
};

type Props = {
  id: number,
};

class ActivitySocialCategoryActions extends PureComponent<Props> {
  render() {
    const {
      id,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.btnWrapper}>
          <EditButton
            path={`/activity/social/edit/${id}`} />
        </div>
      </div>
    );
  }
}

export default ActivitySocialCategoryActions;
