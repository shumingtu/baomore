// @flow
import React, { PureComponent } from 'react';
import { Mutation } from 'react-apollo';
// shared
import {
  DELETE_MEMBER,
  TOGGLE_ARCHIVE,
} from '../../../mutations/Member.js';
import {
  GET_EMPLOYEE_LIST,
} from '../../../queries/Member.js';
// components
import DeleteButton from '../../Global/DeleteButton.jsx';
import Button from '../../Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
  },
};

type Props = {
  id: number,
  type: String,
  archive: boolean,
};

class SystemInfoMemberActions extends PureComponent<Props> {
  render() {
    const {
      id,
      type,
      archive,
    } = this.props;

    return (
      <Mutation
        mutation={DELETE_MEMBER}
        update={(cache, { data: { deleteMember } }) => {
          const {
            employeeMemberlist,
          } = cache.readQuery({
            query: GET_EMPLOYEE_LIST,
            variables: {
              employeeType: type,
              limit: 10,
              offset: 0,
            },
          });

          const deletingIdx = employeeMemberlist.findIndex(b => b.id === deleteMember.id);

          if (~deletingIdx) {
            cache.writeQuery({
              query: GET_EMPLOYEE_LIST,
              variables: {
                employeeType: type,
                limit: 10,
                offset: 0,
              },
              data: {
                employeeMemberlist: [
                  ...employeeMemberlist.slice(0, deletingIdx),
                  ...employeeMemberlist.slice(deletingIdx + 1),
                ],
              },
            });
          }
        }}>
        {(deleteMember, { loading }) => (
          <div style={styles.wrapper}>
            <div style={styles.btnWrapper}>
              <DeleteButton
                disabled={loading}
                onDelete={async () => {
                  await deleteMember({
                    variables: {
                      id: parseInt(id, 10),
                    },
                  });
                }} />
            </div>
            <div style={styles.btnWrapper}>
              <Mutation
                mutation={TOGGLE_ARCHIVE}
                update={(cache, { data: { toggleMemberArchive } }) => {
                  const {
                    employeeMemberlist,
                  } = cache.readQuery({
                    query: GET_EMPLOYEE_LIST,
                    variables: {
                      employeeType: type,
                      limit: 10,
                      offset: 0,
                    },
                  });

                  const toggleIdx = employeeMemberlist.findIndex(b => b.id === toggleMemberArchive.id);

                  if (~toggleIdx) {
                    cache.writeQuery({
                      query: GET_EMPLOYEE_LIST,
                      variables: {
                        employeeType: type,
                        limit: 10,
                        offset: 0,
                      },
                      data: {
                        employeeMemberlist: [
                          ...employeeMemberlist.slice(0, toggleIdx),
                          {
                            ...employeeMemberlist[toggleIdx],
                            archive: !employeeMemberlist[toggleIdx].archive,
                          },
                          ...employeeMemberlist.slice(toggleIdx + 1),
                        ],
                      },
                    });
                  }
                }}>
                {(toggleMemberArchive, { loading: toggleArchiveLoading }) => (
                  <Button
                    disabled={toggleArchiveLoading}
                    onClick={async () => {
                      await toggleMemberArchive({
                        variables: {
                          id: parseInt(id, 10),
                        },
                      });
                    }}
                    style={archive && { backgroundColor: 'gray' }}
                    label={archive ? '復職' : '離職'} />
                )}
              </Mutation>
            </div>
          </div>
        )}
      </Mutation>
    );
  }
}


export default SystemInfoMemberActions;
