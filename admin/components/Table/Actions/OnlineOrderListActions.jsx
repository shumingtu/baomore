// @flow
import React, { PureComponent } from 'react';
// components
import EditButton from '../../Global/EditButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
  },
};

type Props = {
  id: string,
  payedStatus: string,
};

class OnlineOrderListActions extends PureComponent<Props> {
  render() {
    const {
      id,
      payedStatus,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        { payedStatus === 'PAIDNOTCONFIRMED' ? (
          <div style={styles.btnWrapper}>
            <EditButton
              path={`/order/assign/${id}`} />
          </div>
        ) : null }
      </div>
    );
  }
}

export default OnlineOrderListActions;
