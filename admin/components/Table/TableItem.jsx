// @flow
import React, { Component } from 'react';
import radium from 'radium';
// component
import TableItemField from './TableItemField.jsx';
// config
import Theme from '../../styles/Theme.js';

const styles = {
  tableItemWrapper: {
    width: '100%',
    minHeight: 66,
    padding: '12px 0',
    margin: '0 0 8px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#fff',
    borderRadius: 2,
    border: 0,
    boxShadow: Theme.BLOCK_SHADOW,
  },
  tableFunction: {
    flex: 1,
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '0 8px',
  },
  textCenter: {
    textAlign: 'center',
  },
};

type Props = {
  data: Object,
  children: Array<Object>,
  getActions: Function,
  fixedItemWidth?: number,
};

type Field = {
  props: {
    fieldKey: string,
    flex: number,
    prefix: string,
    isCenter: boolean,
    isImage: boolean,
    isClickable: boolean,
    needBlackBg: boolean,
    Component?: React.Node,
    MutationContext?: any,
    minWidth?: number,
    color?: string,
  },
};

class TableItem extends Component<Props> {
  static defaultProps = {
    fixedItemWidth: null,
  };

  render() {
    const {
      data,
      children,
      getActions,
      fixedItemWidth,
    } = this.props;

    return (
      <div style={styles.tableItemWrapper}>
        {children.map((field: Field) => {
          if (!field.props) return null;

          const {
            fieldKey,
          } = field.props;

          return (
            <TableItemField
              fixedItemWidth={fixedItemWidth}
              key={`${data.id}-${fieldKey}-placement`}
              field={field}
              data={data} />
          );
        })}
        {getActions ? getActions().map((element, idx) => {
          const renderElementWithProps = React.cloneElement(
            element,
            {
              ...data,
            },
          );

          return (
            <div
              key={`function-wrapper-${idx + 1}`}
              style={[
                styles.tableFunction,
                fixedItemWidth && { width: fixedItemWidth, flex: 'none' },
              ]}>
              {renderElementWithProps || null}
            </div>
          );
        }) : null}
      </div>
    );
  }
}

export default radium(TableItem);
