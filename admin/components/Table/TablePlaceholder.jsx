// @flow
import React, { memo } from 'react';
// config
import Theme from '../../styles/Theme.js';

const styles = {
  tablePlaceholder: {
    width: '100%',
    height: 66,
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderRadius: 2,
    border: 0,
  },
  placeholder: {
    fontSize: 12,
    fontWeight: 500,
    color: Theme.BLACK_COLOR,
    letterSpacing: 1,
  },
};

type Props = {
  placeholder: string,
};

function TablePlaceholder({
  placeholder = '無資料',
}: Props) {
  return (
    <div style={styles.tablePlaceholder}>
      <span style={styles.placeholder}>
        {placeholder}
      </span>
    </div>
  );
}

export default memo(TablePlaceholder);
