// @flow
/* eslint jsx-a11y/no-autofocus: 0 */
import React, { PureComponent } from 'react';
import radium from 'radium';
// config
import Theme from '../../../styles/Theme.js';
import TextStyles from '../../../styles/Text.js';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  label: {
    ...TextStyles.labelText,
    fontSize: 13,
    lineHeight: 1.618,
    whiteSpace: 'pre-line',
  },
  center: {
    textAlign: 'center',
  },
  input: {
    flex: 1,
    fontSize: 14,
    padding: '8px 8px 10px 10px',
    border: `1px solid ${Theme.BLACK_COLOR}`,
    borderRadius: 2,
    backgroundColor: 'transparent',
    outline: 0,
  },
};

type Props = {
  tableField: Object,
  tableData: string,
  originData: {
    id: number,
    name: string,
  },
  mutate: Function,
  loading: boolean,
};

type State = {
  isEditing: boolean,
  editingValue: string,
};

class EditableLabel extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      isEditing: false,
      editingValue: '',
    };
  }

  render() {
    const {
      tableField,
      tableData,
      originData,
      mutate,
      loading = false,
    } = this.props;

    const {
      isCenter,
    } = tableField.props;

    const {
      isEditing,
      editingValue,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        {!isEditing ? (
          <span
            onDoubleClick={() => this.setState({
              isEditing: true,
              editingValue: originData.name || '',
            })}
            style={[
              styles.label,
              isCenter && styles.center,
            ]}>
            {tableData || null}
          </span>
        ) : (
          <input
            key="table-input"
            type="text"
            autoFocus
            value={editingValue}
            disabled={loading}
            onBlur={() => this.setState({
              editingValue: '',
              isEditing: false,
            })}
            onChange={e => this.setState({
              editingValue: e.target.value,
            })}
            onKeyPress={async (e) => {
              if (e.key === 'Enter') {
                e.preventDefault();

                if (mutate) {
                  await mutate({
                    variables: {
                      id: (originData && originData.id) || null,
                      name: editingValue,
                    },
                  });
                }

                this.setState({
                  editingValue: '',
                  isEditing: false,
                });
              }
            }}
            style={styles.input} />
        )}
      </div>
    );
  }
}

export default radium(EditableLabel);
