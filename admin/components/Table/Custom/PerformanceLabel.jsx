// @flow
import React from 'react';
import radium from 'radium';

import Theme from '../../../styles/Theme.js';

const styles = {
  tableField: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 13,
    fontWeight: 500,
    color: Theme.BLACK_COLOR,
    letterSpacing: 1,
  },
  highlight: {
    color: Theme.ERROR_COLOR,
  },
};

type Props = {
  tableData: {
    value: string,
    isRed: boolean,
  },
};

function PerformanceLabel({
  tableData,
}: Props) {
  return (
    <div style={styles.tableField}>
      <span
        style={[
          styles.text,
          tableData && tableData.isRed && styles.highlight,
        ]}>
        {tableData && tableData.value}
      </span>
    </div>
  );
}

export default radium(PerformanceLabel);
