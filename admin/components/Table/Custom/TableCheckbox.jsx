// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

import TextStyles from '../../../styles/Text.js';
// components
import Checkbox from '../../Global/Checkbox.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  label: {
    ...TextStyles.labelText,
    width: 'auto',
    fontSize: 13,
    lineHeight: 1.618,
    whiteSpace: 'pre-line',
  },
  center: {
    textAlign: 'center',
  },
};

type Props = {
  tableField: Object,
  tableData: string,
  originData: {
    id: number,
    name: string,
  },
};

class TableCheckbox extends PureComponent<Props> {
  render() {
    const {
      tableField,
      tableData,
      originData,
    } = this.props;

    const {
      isCenter,
      checkedList,
      onChange,
    } = tableField.props;

    return (
      <div style={styles.wrapper}>
        {tableData ? (
          <span
            style={[
              styles.label,
              isCenter && styles.center,
            ]}>
            {tableData || null}
          </span>
        ) : (
          <Checkbox
            value={originData.id}
            onChange={onChange}
            isChecked={checkedList.find(v => v === originData.id)} />
        )}
      </div>
    );
  }
}

export default radium(TableCheckbox);
