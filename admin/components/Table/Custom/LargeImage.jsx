// @flow
import React from 'react';
import radium from 'radium';

const styles = {
  tableImage: {
    width: '100%',
    height: 200,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  },
};

type Props = {
  tableData: string,
};

function LargeImage({
  tableData,
}: Props) {
  return (
    <div
      style={[
        styles.tableImage,
        {
          backgroundImage: tableData ? `url(${tableData})` : null,
        },
      ]} />
  );
}

export default radium(LargeImage);
