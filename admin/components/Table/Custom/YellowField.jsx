// @flow
import React from 'react';
import radium from 'radium';

import Theme from '../../../styles/Theme.js';

const styles = {
  tableField: {
    width: '100%',
    height: 70,
    backgroundColor: Theme.ACTIVE_COLOR,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 13,
    fontWeight: 500,
    color: '#fff',
    letterSpacing: 1,
  },
};

type Props = {
  tableData: string,
};

function YellowField({
  tableData,
}: Props) {
  return (
    <div style={styles.tableField}>
      <span style={styles.text}>
        {tableData || null}
      </span>
    </div>
  );
}

export default radium(YellowField);
