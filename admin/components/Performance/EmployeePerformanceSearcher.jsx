// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  initialize,
} from 'redux-form';
import {
  Query,
} from 'react-apollo';

import {
  FORM_EMPLOYEE_PERFORMANCE_SEARCHER,
} from '../../shared/form.js';
import {
  GET_EMPLOYEE_LIST,
} from '../../queries/Member.js';
import {
  FETCH_AREA_LIST,
} from '../../queries/Area.js';

import Selector from '../Form/Selector.jsx';
import Input from '../Form/Input.jsx';
import SubmitButton from '../Form/SubmitButton.jsx';

import {
  CACHE_ADMIN_PERFORMANCE_SEARCH,
  CACHE_ADMIN_DIRECTOR_PERFORMANCE_SEARCH,
  cacheSearchOptions,
} from '../../actions/Search.js';


const styles = {
  wrapper: {
    width: '100%',
    margin: '10px 0',
    height: 'fit-content',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  searchWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    padding: '12px 0',
  },
  rowWrapper: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: '5px 0',
  },
  rowItem: {
    margin: '0 5px',
  },
  searchBtn: {
    backgroundColor: 'rgb(182, 184, 204)',
    border: '1px solid rgb(182, 184, 204)',
    color: 'white',
  },
};

type Props = {
  handleSubmit: Function,
  employeeType: String,
  performanceSearchOptions: Object,
  performanceDirectorSearchOptions: Object,
  cachePerformanceSearchOptions: Function,
  initializeForm: Function,
};

class EmployeePerformanceSearcher extends PureComponent<Props> {
  componentDidMount() {
    const {
      performanceSearchOptions,
      performanceDirectorSearchOptions,
      employeeType,
      initializeForm,
    } = this.props;

    const initialTarget = employeeType === '包膜師' ? performanceSearchOptions : performanceDirectorSearchOptions;

    initializeForm({
      ...initialTarget,
    });
  }

  submit(d) {
    const {
      cachePerformanceSearchOptions,
      employeeType,
    } = this.props;

    const {
      startDate,
      endDate,
      employeeId,
      areaId,
      directorId,
    } = d;

    const cacheTarget = employeeType === '包膜師' ? CACHE_ADMIN_PERFORMANCE_SEARCH : CACHE_ADMIN_DIRECTOR_PERFORMANCE_SEARCH;

    cachePerformanceSearchOptions(cacheTarget, {
      startDate,
      endDate,
      employeeId: employeeId === '-1' || !employeeId ? null : parseInt(employeeId, 10),
      directorId: directorId === '-1' || !directorId ? null : parseInt(directorId, 10),
      areaId: areaId === '-1' || !areaId ? null : parseInt(areaId, 10),
    });
  }

  render() {
    const {
      handleSubmit,
      employeeType,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <form onSubmit={handleSubmit(d => this.submit(d))} style={styles.searchWrapper}>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Field
                name="startDate"
                label="開始時間:"
                type="date"
                component={Input} />
            </div>
            <div style={styles.rowItem}>
              <Field
                name="endDate"
                label="結束時間:"
                type="date"
                component={Input} />
            </div>
          </div>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Query
                variables={{ employeeType }}
                query={GET_EMPLOYEE_LIST}>
                {({ data }) => {
                  if (!data) return null;

                  const {
                    employeeMemberlist,
                  } = data;

                  return (
                    <Field
                      name={employeeType === '包膜師' ? 'employeeId' : 'directorId'}
                      label={employeeType === '包膜師' ? '包膜師:' : '主任:'}
                      nullable
                      placeholder="請選擇"
                      options={employeeMemberlist || []}
                      component={Selector} />
                  );
                }}
              </Query>
            </div>
            <div style={styles.rowItem}>
              <Query query={FETCH_AREA_LIST}>
                {({ data }) => {
                  if (!data) return null;

                  const {
                    areaList,
                  } = data;

                  return (
                    <Field
                      name="areaId"
                      label="地區:"
                      nullable
                      placeholder="請選擇"
                      options={areaList || []}
                      component={Selector} />
                  );
                }}
              </Query>
            </div>
          </div>
          <div style={[
            styles.rowWrapper,
            { justifyContent: 'flex-end' },
          ]}>
            <div style={styles.rowItem}>
              <SubmitButton style={styles.searchBtn} label="查詢" />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_EMPLOYEE_PERFORMANCE_SEARCHER,
});

const reduxHook = connect(
  state => ({
    performanceSearchOptions: state.Search.adminPerformance,
    performanceDirectorSearchOptions: state.Search.adminDirectorPerformance,
  }),
  dispatch => bindActionCreators({
    initializeForm: d => initialize(FORM_EMPLOYEE_PERFORMANCE_SEARCHER, d),
    cachePerformanceSearchOptions: cacheSearchOptions,
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      EmployeePerformanceSearcher
    )
  )
);
