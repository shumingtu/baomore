// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  initialize,
} from 'redux-form';
import {
  Query,
} from 'react-apollo';

import {
  FORM_STORE_PERFORMANCE_SEARCHER,
} from '../../shared/form.js';
import {
  FETCH_STORES_LIST,
} from '../../queries/Store.js';
import {
  FETCH_CHANNEL_LIST,
} from '../../queries/Channel.js';

import Selector from '../Form/Selector.jsx';
import Input from '../Form/Input.jsx';
import SubmitButton from '../Form/SubmitButton.jsx';

import {
  CACHE_ADMIN_STORE_PERFORMANCE_SEARCH,
  cacheSearchOptions,
} from '../../actions/Search.js';


const styles = {
  wrapper: {
    width: '100%',
    margin: '10px 0',
    height: 'fit-content',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  searchWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    padding: '12px 0',
  },
  rowWrapper: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: '5px 0',
  },
  rowItem: {
    margin: '0 5px',
  },
  searchBtn: {
    backgroundColor: 'rgb(182, 184, 204)',
    border: '1px solid rgb(182, 184, 204)',
    color: 'white',
  },
};

type Props = {
  handleSubmit: Function,
  performanceSearchOptions: Object,
  cachePerformanceSearchOptions: Function,
  initializeForm: Function,
};

class StorePerformanceSearcher extends PureComponent<Props> {
  componentDidMount() {
    const {
      performanceSearchOptions,
      initializeForm,
    } = this.props;

    initializeForm({
      startDate: performanceSearchOptions.startDate,
      endDate: performanceSearchOptions.endDate,
    });
  }

  submit(d) {
    const {
      cachePerformanceSearchOptions,
    } = this.props;

    const {
      startDate,
      endDate,
      channelId,
      storeId,
    } = d;

    cachePerformanceSearchOptions(CACHE_ADMIN_STORE_PERFORMANCE_SEARCH, {
      startDate,
      endDate,
      channelId: channelId === '-1' || !channelId ? null : parseInt(channelId, 10),
      storeId: storeId === '-1' || !storeId ? null : parseInt(storeId, 10),
    });
  }

  render() {
    const {
      handleSubmit,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <form onSubmit={handleSubmit(d => this.submit(d))} style={styles.searchWrapper}>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Field
                name="startDate"
                label="開始時間:"
                type="date"
                component={Input} />
            </div>
            <div style={styles.rowItem}>
              <Field
                name="endDate"
                label="結束時間:"
                type="date"
                component={Input} />
            </div>
          </div>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Query
                query={FETCH_STORES_LIST}>
                {({ data }) => {
                  if (!data) return null;

                  const {
                    stores,
                  } = data;

                  return (
                    <Field
                      name="storeId"
                      label="門市:"
                      nullable
                      placeholder="請選擇"
                      options={stores || []}
                      component={Selector} />
                  );
                }}
              </Query>
            </div>
            <div style={styles.rowItem}>
              <Query query={FETCH_CHANNEL_LIST}>
                {({ data }) => {
                  if (!data) return null;

                  const {
                    channelList,
                  } = data;

                  return (
                    <Field
                      name="channelId"
                      label="通路:"
                      nullable
                      placeholder="請選擇"
                      options={channelList || []}
                      component={Selector} />
                  );
                }}
              </Query>
            </div>
          </div>
          <div style={[
            styles.rowWrapper,
            { justifyContent: 'flex-end' },
          ]}>
            <div style={styles.rowItem}>
              <SubmitButton style={styles.searchBtn} label="查詢" />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_STORE_PERFORMANCE_SEARCHER,
});

const reduxHook = connect(
  state => ({
    performanceSearchOptions: state.Search.storePerformance,
  }),
  dispatch => bindActionCreators({
    initializeForm: d => initialize(FORM_STORE_PERFORMANCE_SEARCHER, d),
    cachePerformanceSearchOptions: cacheSearchOptions,
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      StorePerformanceSearcher
    )
  )
);
