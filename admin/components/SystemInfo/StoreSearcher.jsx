// @flow
import React, { Component } from 'react';
import radium from 'radium';
import {
  reduxForm,
  Field,
} from 'redux-form';
import { connect } from 'react-redux';

import {
  FORM_STORE_INFO_SEARCHER,
} from '../../shared/form.js';
import {
  cacheSearchOptions,
  CACHE_ADMIN_STORE_INFO_LIST_SEARCH,
} from '../../actions/Search.js';

import Input from '../Form/Input.jsx';
import SubmitButton from '../Form/SubmitButton.jsx';

const styles = {
  wrapper: {
    width: 'auto',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  searchWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    padding: '12px 0',
  },
  rowWrapper: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: '5px 0',
  },
  rowItem: {
    margin: '0 5px',
  },
  searchBtn: {
    backgroundColor: 'rgb(182, 184, 204)',
    border: '1px solid rgb(182, 184, 204)',
    color: 'white',
  },
};

type Props = {
  cacheKeyword: Function,
  handleSubmit: Function,
};

class StoreSearcher extends Component<Props> {
  submit(d) {
    const {
      cacheKeyword,
    } = this.props;

    const {
      keyword,
    } = d;

    cacheKeyword(CACHE_ADMIN_STORE_INFO_LIST_SEARCH, {
      keyword: keyword ? `${keyword}` : null,
    });
  }

  render() {
    const {
      handleSubmit,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <form onSubmit={handleSubmit(d => this.submit(d))} style={styles.searchWrapper}>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Field
                name="keyword"
                placeholder="店名"
                label="關鍵字:"
                component={Input} />
            </div>
            <div style={styles.rowItem}>
              <SubmitButton style={styles.searchBtn} label="查詢" />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const reduxHook = connect(
  () => ({
  }),
  {
    cacheKeyword: cacheSearchOptions,
  },
);

const formHook = reduxForm({
  form: FORM_STORE_INFO_SEARCHER,
});

export default reduxHook(
  formHook(
    radium(StoreSearcher)
  )
);
