// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  initialize,
} from 'redux-form';
import {
  Query,
} from 'react-apollo';
import qs from 'qs';
import {
  GET_EMPLOYEE_LIST,
} from '../../queries/Member.js';
// config
import {
  FORM_ADMIN_PUNCH_SEARCHER,
} from '../../shared/form.js';
import * as SearchActions from '../../actions/Search.js';
// components
import Input from '../Form/Input.jsx';
import SubmitButton from '../Form/SubmitButton.jsx';
import Button from '../Global/Button.jsx';
import Selector from '../Form/Selector.jsx';

const styles = {
  placement: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: 'auto',
    height: 'auto',
    padding: 6,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    display: 'flex',
    width: 'auto',
    height: 'auto',
    padding: '4px 36px',
  },
  exportBtn: {
    margin: '0 5px',
  },
};

type Props = {
  handleSubmit: Function,
  cacheSearchOptions: Function,
  initializeForm: Function,
  punchOptions: {
    startDate?: string,
    endDate?: string,
  },
};

class PunchSearchForm extends PureComponent<Props> {
  componentDidMount() {
    const {
      punchOptions,
    } = this.props;

    this.initializing(punchOptions);
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    initializeForm({
      startDate: (obj && obj.startDate) || null,
      endDate: (obj && obj.endDate) || null,
    });
  }

  submit(d) {
    const {
      cacheSearchOptions,
    } = this.props;

    const {
      CACHE_ADMIN_PUNCH_LIST_SEARCH,
    } = SearchActions;

    const {
      startDate,
      endDate,
      employeeId,
    } = d;

    cacheSearchOptions(CACHE_ADMIN_PUNCH_LIST_SEARCH, {
      startDate: startDate || null,
      endDate: endDate || null,
      employeeId: (employeeId && employeeId !== '-1' && parseInt(employeeId, 10)) || null,
    });
  }

  render() {
    const {
      handleSubmit,
      punchOptions,
    } = this.props;

    return (
      <div style={styles.placement}>
        <form style={styles.searchWrapper} onSubmit={handleSubmit(d => this.submit(d))}>
          <div style={styles.fieldWrapper}>
            <Field
              type="date"
              name="startDate"
              label="開始時間:"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              type="date"
              name="endDate"
              label="結束時間:"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <Query
              variables={{ employeeType: '包膜師' }}
              query={GET_EMPLOYEE_LIST}>
              {({ data }) => {
                if (!data) return null;

                const {
                  employeeMemberlist,
                } = data;

                return (
                  <Field
                    name="employeeId"
                    label="包膜師:"
                    nullable
                    placeholder="請選擇"
                    options={employeeMemberlist || []}
                    component={Selector} />
                );
              }}
            </Query>
          </div>
          <div style={styles.functionWrapper}>
            <SubmitButton label="查詢" />
            <div style={styles.exportBtn}>
              <Button
                onClick={() => {
                  const queryString = qs.stringify(qs.parse(punchOptions), { skipNulls: true });
                  window.open(`${API_HOST}/export/punch?${queryString}&token=${localStorage.accessToken}`);
                }}
                label="匯出" />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_ADMIN_PUNCH_SEARCHER,
});

const reduxHook = connect(
  state => ({
    punchOptions: state.Search.adminPunchList,
  }),
  dispatch => bindActionCreators({
    ...SearchActions,
    initializeForm: v => initialize(FORM_ADMIN_PUNCH_SEARCHER, v),
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      PunchSearchForm
    )
  )
);
