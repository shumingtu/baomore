// @flow

import React from 'react';
import { Query } from 'react-apollo';

import closeIcon from '../../static/images/close-icon.png';
import { FETCH_ORDER_QRCODES } from '../../queries/Order.js';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    left: 0,
    top: 0,
  },
  box: {
    width: 300,
    height: 300,
    border: '1px solid #979797',
    backgroundColor: 'white',
    position: 'relative',
    overflow: 'auto',
  },
  qrcodeWrap: {
    width: '100%',
    textAlign: 'center',
    margin: '5px 0',
  },
  closeBtn: {
    position: 'absolute',
    top: 12,
    right: 12,
    width: 24,
    height: 24,
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundImage: `url(${closeIcon})`,
    cursor: 'pointer',
  },
};

type Props = {
  onClose: Function,
  match: object,
};

function QRCodePopup({
  onClose,
  match: {
    params: {
      orderId,
    },
  },
}: Props) {
  return (
    <div style={styles.wrapper}>
      <div style={styles.box}>
        <Query
          fetchPolicy="network-only"
          query={FETCH_ORDER_QRCODES}
          variables={{
            orderId: orderId ? parseInt(orderId, 10) : -1,
          }}>
          {({
            data,
          }) => {
            const qrcodes = (data && data.orderQRCodeList) || [];

            return (
              <div style={styles.mainWrapper}>
                <button
                  type="button"
                  onClick={onClose || null}
                  style={styles.closeBtn} />
                {qrcodes.map(q => (
                  <div key={q.id} style={styles.qrcodeWrap}>
                    {q.qrcode}
                  </div>
                ))}
              </div>
            );
          }}
        </Query>
      </div>
    </div>
  );
}

export default QRCodePopup;
