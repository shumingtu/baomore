// @flow
import React, { Component } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
} from 'redux-form';
import {
  Query,
} from 'react-apollo';

import {
  FORM_INVOICING_SEARCHER_FORM,
} from '../../shared/form.js';
import {
  FETCH_AREA_LIST,
} from '../../queries/Area.js';
import {
  GET_EMPLOYEE_LIST,
} from '../../queries/Member.js';
import {
  FETCH_VENDOR_LIST,
} from '../../queries/Global.js';
import * as InvoicingActions from '../../actions/Invoicing.js';

import {
  shipStatus,
} from '../../shared/Invoicing.js';

import Selector from '../Form/Selector.jsx';
import Input from '../Form/Input.jsx';
import SubmitButton from '../Form/SubmitButton.jsx';
import CodeInput from '../Form/CodeInput.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  searchWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    padding: '12px 0',
  },
  rowWrapper: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: '5px 0',
  },
  rowItem: {
    margin: '0 5px',
  },
  searchBtn: {
    backgroundColor: 'rgb(182, 184, 204)',
    border: '1px solid rgb(182, 184, 204)',
    color: 'white',
  },
};

type Props = {
  cacheInvoicingSearcher: Function,
  handleSubmit: Function,
};

class OrderGroupShipmentSearcher extends Component<Props> {
  submit(d) {
    const {
      cacheInvoicingSearcher,
    } = this.props;

    const {
      areaId,
      endDate,
      startDate,
      memberId,
      orderSN,
      shippingStatus,
      vendorId,
      code,
    } = d;

    cacheInvoicingSearcher({
      startDate: startDate === '-1' ? null : startDate,
      endDate: endDate === '-1' ? null : endDate,
      orderSN: orderSN === '-1' ? null : orderSN,
      vendorId: vendorId === '-1' ? null : parseInt(vendorId, 10),
      memberId: memberId === '-1' ? null : parseInt(memberId, 10),
      areaId: areaId === '-1' ? null : parseInt(areaId, 10),
      shippingStatus: shippingStatus === '-1' ? null : shippingStatus,
      code: code || null,
    });
  }

  render() {
    const {
      handleSubmit,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <form onSubmit={handleSubmit(d => this.submit(d))} style={styles.searchWrapper}>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Field
                name="startDate"
                label="開始日期:"
                type="date"
                component={Input} />
            </div>
            <div style={styles.rowItem}>
              <Field
                name="endDate"
                label="結束日期:"
                type="date"
                component={Input} />
            </div>
          </div>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Query query={FETCH_VENDOR_LIST}>
                {({ data }) => {
                  if (!data) return null;

                  const {
                    vendorList,
                  } = data;

                  return (
                    <Field
                      name="vendorId"
                      label="廠商:"
                      nullable
                      placeholder="請選擇"
                      options={vendorList || []}
                      component={Selector} />
                  );
                }}
              </Query>
            </div>
            <div style={styles.rowItem}>
              <Field
                name="shippingStatus"
                label="出貨狀態:"
                nullable
                placeholder="請選擇"
                options={shipStatus}
                component={Selector} />
            </div>
          </div>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Query query={GET_EMPLOYEE_LIST}>
                {({ data }) => {
                  if (!data) return null;
                  const {
                    employeeMemberlist,
                  } = data;

                  return (
                    <Field
                      name="memberId"
                      label="包膜師:"
                      nullable
                      placeholder="請選擇"
                      options={employeeMemberlist || []}
                      component={Selector} />
                  );
                }}
              </Query>
            </div>
            <div style={styles.rowItem}>
              <Query query={FETCH_AREA_LIST}>
                {({ data }) => {
                  if (!data) return null;

                  const {
                    areaList,
                  } = data;

                  return (
                    <Field
                      name="areaId"
                      label="區域:"
                      nullable
                      placeholder="請選擇"
                      options={areaList || []}
                      component={Selector} />
                  );
                }}
              </Query>
            </div>
          </div>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Field
                name="code"
                label="料號編號:"
                component={CodeInput} />
            </div>
            <div style={styles.rowItem}>
              <Field
                name="orderSN"
                label="訂單編號:"
                component={Input} />
            </div>
          </div>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <SubmitButton style={styles.searchBtn} label="查詢" />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_INVOICING_SEARCHER_FORM,
});

const reduxHook = connect(
  () => ({
  }),
  dispatch => bindActionCreators({
    ...InvoicingActions,
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(OrderGroupShipmentSearcher)
  )
);
