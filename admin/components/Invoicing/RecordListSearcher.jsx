// @flow
import React, { Component } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import qs from 'qs';
import {
  reduxForm,
  Field,
  Fields,
} from 'redux-form';
import {
  Query,
} from 'react-apollo';

import {
  GET_EMPLOYEE_LIST,
} from '../../queries/Member.js';

import {
  cacheSearchOptions,
  CACHE_ADMIN_RECORD_LIST_SEARCH,
} from '../../actions/Search.js';

import {
  FORM_RECORD_LIST_SEARCHER,
} from '../../shared/form.js';

import Selector from '../Form/Selector.jsx';
import BehaviorDate from '../Form/BehaviorDate.jsx';
import SubmitButton from '../Form/SubmitButton.jsx';
import Button from '../Global/Button.jsx';
import Input from '../Form/Input.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  searchWrapper: {
    display: 'flex',
    alignItems: 'flex-start',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: '12px 0',
  },
  containWrapper: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  rowWrapper: {
    display: 'flex',
    width: '100%',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  rowItem: {
    margin: '0 5px',
  },
  searchBtn: {
    backgroundColor: 'rgb(182, 184, 204)',
    border: '1px solid rgb(182, 184, 204)',
    color: 'white',
  },
  createBtn: {
    backgroundColor: 'rgb(182, 184, 204)',
    border: '1px solid rgb(182, 184, 204)',
    color: 'white',
    margin: '0 5px',
  },
};

type Props = {
  cacheRecordSearch: Function,
  handleSubmit: Function,
  history: object,
  options: object,
};

class RecordListSearcher extends Component<Props> {
  submit(d) {
    const {
      cacheRecordSearch,
    } = this.props;

    const {
      employeeId,
      behaviorId,
      startDate,
      endDate,
      recordStartDate,
      recordEndDate,
    } = d;

    cacheRecordSearch(CACHE_ADMIN_RECORD_LIST_SEARCH, {
      behaviorId: behaviorId === '-1' ? null : parseInt(behaviorId, 10),
      employeeId: employeeId === '-1' ? null : parseInt(employeeId, 10),
      startDate: startDate || null,
      endDate: endDate || null,
      recordStartDate: recordStartDate || null,
      recordEndDate: recordEndDate || null,
    });
  }

  render() {
    const {
      handleSubmit,
      options,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <form onSubmit={handleSubmit(d => this.submit(d))} style={styles.searchWrapper}>
          <div style={styles.containWrapper}>
            <div style={styles.rowWrapper}>
              <div style={styles.rowItem}>
                <Query query={GET_EMPLOYEE_LIST}>
                  {({ data }) => {
                    if (!data) return null;
                    const {
                      employeeMemberlist,
                    } = data;

                    return (
                      <Field
                        name="employeeId"
                        label="包膜師:"
                        nullable
                        placeholder="請選擇"
                        options={employeeMemberlist || []}
                        component={Selector} />
                    );
                  }}
                </Query>
              </div>
              <div style={styles.rowItem}>
                <Fields
                  component={BehaviorDate}
                  names={['behaviorId', 'startDate', 'endDate']} />
              </div>
            </div>
            <div style={styles.rowWrapper}>
              <div style={styles.rowItem}>
                <Field
                  name="recordStartDate"
                  label="開始日期:"
                  type="date"
                  component={Input} />
              </div>
              <div style={styles.rowItem}>
                <Field
                  name="recordEndDate"
                  label="結束日期:"
                  type="date"
                  component={Input} />
              </div>
            </div>
          </div>
          <div>
            <SubmitButton style={styles.searchBtn} label="查詢" />
            <Button
              style={{ margin: '0px 0px 0px 5px' }}
              onClick={() => {
                const queryString = qs.stringify(qs.parse(options), { skipNulls: true });
                window.open(`${API_HOST}/export/behaviorRecord?${queryString}&token=${localStorage.accessToken}`);
              }}
              label="匯出" />
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_RECORD_LIST_SEARCHER,
});

const reduxHook = connect(
  state => ({
    options: state.Search.adminRecordList,
  }),
  dispatch => bindActionCreators({
    cacheRecordSearch: cacheSearchOptions,
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(RecordListSearcher)
  )
);
