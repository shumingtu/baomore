// @flow
import React, { Component } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  initialize,
  SubmissionError,
} from 'redux-form';
import {
  graphql,
} from 'react-apollo';

import {
  CREATE_CLOSEDORDERSETTING,
  EDIT_CLOSEDORDERSETTING,
} from '../../mutations/ClosedOrderSetting.js';
import {
  FETCH_CLOSEDORDERSETTING_LIST,
  FETCH_SINGLE_CLOSEDORDERSETTING,
} from '../../queries/ClosedOrderSetting.js';

import {
  FORM_CLOSEDORDERSETTING_CREATE_EDIT_FORM,
} from '../../shared/form.js';

import { validateClosedOrderSetting } from '../../helper/validator/ClosedOrderSetting.js';

import TextStyles from '../../styles/Text.js';

import {
  toggleCreateEditBox,
} from '../../actions/Invoicing.js';
import {
  days,
} from '../../shared/Global.js';

import SubmitButton from '../Form/SubmitButton.jsx';
import Selector from '../Form/Selector.jsx';
import CodeInput from '../Form/CodeInput.jsx';
import Input from '../Form/Input.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 999,
  },
  box: {
    width: 500,
    height: 400,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    border: '1px solid #979797',
    backgroundColor: 'white',
    position: 'relative',
    padding: 40,
  },
  contentWrapper: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  groupWrap: {
    margin: '10px 0',
  },
  closeBtn: {
    position: 'relative',
    cursor: 'pointer',
    border: 'none',
    outline: 'none',
    fontSize: 20,
    backgroundColor: 'transparent',
  },
  circle: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: 30,
    height: 30,
    borderRadius: '100%',
    border: '2px solid black',
    position: 'absolute',
    top: 20,
    right: 20,
  },
  btnWrap: {
    margin: '10px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  title: {
    ...TextStyles.labelText,
    width: '100%',
    height: 'auto',
    textAlign: 'center',
    fontSize: 18,
    padding: '18px 0',
  },
  errorText: {
    fontSize: 18,
    color: 'red',
  },
};

type Props = {
  handleSubmit: Function,
  toggleBox: Function,
  createClosedOrderSetting: Function,
  targetId: Number,
  singleClosedOrderSetting: Object,
  initializeForm: Function,
  editClosedOrderSetting: Function,
  error: String,
};

class ClosedSettingPopUp extends Component<Props> {
  componentDidMount() {
    const {
      singleClosedOrderSetting,
      initializeForm,
    } = this.props;

    if (singleClosedOrderSetting) {
      initializeForm({
        ...singleClosedOrderSetting,
        code: singleClosedOrderSetting.PNTable ? singleClosedOrderSetting.PNTable.code : null,
      });
    }
  }

  componentDidUpdate(prevProps) {
    const {
      singleClosedOrderSetting,
      initializeForm,
    } = this.props;

    if (
      singleClosedOrderSetting
      && singleClosedOrderSetting
      !== prevProps.singleClosedOrderSetting
    ) {
      initializeForm({
        ...singleClosedOrderSetting,
        code: singleClosedOrderSetting.PNTable ? singleClosedOrderSetting.PNTable.code : null,
      });
    }
  }

  close() {
    const {
      toggleBox,
    } = this.props;

    toggleBox({ status: false });
  }

  async submit(d) {
    const {
      createClosedOrderSetting,
      targetId,
      singleClosedOrderSetting,
      editClosedOrderSetting,
    } = this.props;

    const isEditDefaultSetting = (targetId && singleClosedOrderSetting && !singleClosedOrderSetting.PNTable);

    const error = validateClosedOrderSetting(d, isEditDefaultSetting);
    if (error) throw new SubmissionError(error);

    if (targetId) {
      const payload = {
        id: parseInt(targetId, 10),
        closedDay: parseInt(d.closedDay, 10),
        closedTime: d.closedTime,
        code: d.code ? d.code : null,
      };

      await editClosedOrderSetting(payload)
        .then((res) => {
          if (!res.erroes) {
            this.close();
          }
        }).catch((e) => {
          throw new SubmissionError({
            _error: e.graphQLErrors[0].message,
          });
        });

      return;
    }

    await createClosedOrderSetting({
      ...d,
      closedDay: parseInt(d.closedDay, 10),
    }).then((res) => {
      if (!res.erroes) {
        this.close();
      }
    }).catch((e) => {
      throw new SubmissionError({
        _error: e.graphQLErrors[0].message,
      });
    });
  }

  render() {
    const {
      handleSubmit,
      targetId,
      singleClosedOrderSetting,
      error,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div
          style={styles.box}>
          <div style={styles.circle}>
            <button
              onClick={() => this.close()}
              style={styles.closeBtn}
              type="button">
              X
            </button>
          </div>
          <form
            onSubmit={handleSubmit(d => this.submit(d))}
            style={styles.contentWrapper}>
            {error ? (
              <span style={styles.errorText}>{error}</span>
            ) : null}
            <div style={styles.title}>
              {targetId ? '修改結單日' : '新增結單日'}
            </div>
            <div style={styles.groupWrap}>
              <Field
                name="closedDay"
                label="結單日:"
                nullable
                placeholder="請選擇"
                options={days}
                component={Selector} />
            </div>
            <div style={styles.groupWrap}>
              <Field
                type="time"
                label="時間:"
                name="closedTime"
                component={Input} />
            </div>
            {targetId && singleClosedOrderSetting && !singleClosedOrderSetting.PNTable ? null : (
              <div style={styles.groupWrap}>
                <Field
                  disabled={targetId}
                  name="code"
                  label="選擇編號:"
                  component={CodeInput} />
              </div>
            )}
            <div style={styles.btnWrap}>
              <SubmitButton
                style={styles.editBtn}
                label={targetId ? '確定修改' : '確定新增'} />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_CLOSEDORDERSETTING_CREATE_EDIT_FORM,
});

const reduxHook = connect(
  state => ({
    targetId: state.Invoicing.boxData.targetId,
  }),
  dispatch => bindActionCreators({
    toggleBox: toggleCreateEditBox,
    initializeForm: d => initialize(FORM_CLOSEDORDERSETTING_CREATE_EDIT_FORM, d),
  }, dispatch),
);

const queryHook = graphql(FETCH_SINGLE_CLOSEDORDERSETTING, {
  options: ownProps => ({
    variables: {
      id: (ownProps.targetId && parseInt(ownProps.targetId, 10)) || -1,
    },
  }),
  props: ({
    data: {
      singleClosedOrderSetting,
    },
  }) => ({
    singleClosedOrderSetting: singleClosedOrderSetting || null,
  }),
});

const createMutationHook = graphql(CREATE_CLOSEDORDERSETTING, {
  props: ({ mutate }) => ({
    createClosedOrderSetting: ({
      closedDay,
      closedTime,
      code,
    }) => mutate({
      variables: {
        closedDay,
        closedTime,
        code,
      },
      update: (store, { data: { createClosedOrderSetting } }) => {
        const data = store.readQuery({
          query: FETCH_CLOSEDORDERSETTING_LIST,
        });

        const {
          closedOrderSettings,
        } = data;

        if (createClosedOrderSetting) {
          store.writeQuery({
            query: FETCH_CLOSEDORDERSETTING_LIST,
            data: {
              closedOrderSettings: [
                ...closedOrderSettings,
                {
                  ...createClosedOrderSetting,
                },
              ],
            },
          });
        }
      },
    }),
  }),
});

const editMutationHook = graphql(EDIT_CLOSEDORDERSETTING, {
  props: ({ mutate }: { mutate: Function }) => ({
    editClosedOrderSetting: ({
      id,
      name,
      closedDay,
      closedTime,
      code,
    }) => mutate({
      variables: {
        id,
        name,
        closedDay,
        closedTime,
        code,
      },
      update: (store, { data: { editClosedOrderSetting } }) => {
        const data = store.readQuery({
          query: FETCH_CLOSEDORDERSETTING_LIST,
        });

        const {
          closedOrderSettings,
        } = data;

        if (editClosedOrderSetting) {
          const targetIdx = closedOrderSettings.findIndex(x => x.id === editClosedOrderSetting.id);

          if (~targetIdx) {
            store.writeQuery({
              query: FETCH_CLOSEDORDERSETTING_LIST,
              data: {
                closedOrderSettings: [
                  ...closedOrderSettings.slice(0, targetIdx),
                  editClosedOrderSetting,
                  ...closedOrderSettings.slice(targetIdx + 1),
                ],
              },
            });
          }
        }
      },
    }),
  }),
});

export const ClosedSettingEditForm = reduxHook(
  queryHook(
    editMutationHook(
      formHook(
        radium(ClosedSettingPopUp)
      )
    )
  )
);

export const ClosedSettingCreateForm = reduxHook(
  createMutationHook(
    formHook(
      radium(ClosedSettingPopUp)
    )
  )
);

export default radium(ClosedSettingPopUp);
