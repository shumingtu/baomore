// @flow
import React, { Component } from 'react';
import radium from 'radium';
import qs from 'qs';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
} from 'redux-form';
import {
  Query,
} from 'react-apollo';

import {
  GET_EMPLOYEE_LIST,
} from '../../queries/Member.js';

import {
  cacheSearchOptions,
  CACHE_ADMIN_STOCK_LIST_SEARCH,
} from '../../actions/Search.js';

import {
  FORM_STOCK_LIST_SEARCHER,
} from '../../shared/form.js';

import Selector from '../Form/Selector.jsx';
import Input from '../Form/Input.jsx';
import SubmitButton from '../Form/SubmitButton.jsx';
import Button from '../Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  searchWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    padding: '12px 0',
  },
  rowWrapper: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: '5px 0',
  },
  rowItem: {
    margin: '0 5px',
  },
  searchBtn: {
    backgroundColor: 'rgb(182, 184, 204)',
    border: '1px solid rgb(182, 184, 204)',
    color: 'white',
  },
  createBtn: {
    backgroundColor: 'rgb(182, 184, 204)',
    border: '1px solid rgb(182, 184, 204)',
    color: 'white',
    margin: '0 5px',
  },
};

type Props = {
  cacheStockSearch: Function,
  handleSubmit: Function,
  history: object,
  options: object,
};

class StockListSearcher extends Component<Props> {
  submit(d) {
    const {
      cacheStockSearch,
    } = this.props;

    const {
      keyword,
      memberId,
    } = d;

    cacheStockSearch(CACHE_ADMIN_STOCK_LIST_SEARCH, {
      keyword: keyword || null,
      memberId: memberId === '-1' ? null : parseInt(memberId, 10),
    });
  }

  render() {
    const {
      handleSubmit,
      options,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <form onSubmit={handleSubmit(d => this.submit(d))} style={styles.searchWrapper}>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Query query={GET_EMPLOYEE_LIST}>
                {({ data }) => {
                  if (!data) return null;
                  const {
                    employeeMemberlist,
                  } = data;

                  return (
                    <Field
                      name="memberId"
                      label="包膜師:"
                      nullable
                      placeholder="請選擇"
                      options={employeeMemberlist || []}
                      component={Selector} />
                  );
                }}
              </Query>
            </div>
          </div>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Field
                name="keyword"
                label="關鍵字:"
                placeholder="品名、料號"
                component={Input} />
            </div>
            <div style={styles.rowItem}>
              <SubmitButton style={styles.searchBtn} label="查詢" />
              <Button
                style={{ margin: '0px 0px 0px 5px' }}
                onClick={() => {
                  const queryString = qs.stringify(qs.parse(options), { skipNulls: true });
                  window.open(`${API_HOST}/export/memberStock?${queryString}&token=${localStorage.accessToken}`);
                }}
                label="匯出" />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_STOCK_LIST_SEARCHER,
});

const reduxHook = connect(
  state => ({
    options: state.Search.adminStockList,
  }),
  dispatch => bindActionCreators({
    cacheStockSearch: cacheSearchOptions,
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(StockListSearcher)
  )
);
