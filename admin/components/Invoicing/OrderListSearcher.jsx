// @flow
import React, { Component } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
} from 'redux-form';
import {
  Query,
} from 'react-apollo';

import {
  GET_EMPLOYEE_LIST,
} from '../../queries/Member.js';

import {
  cacheSearchOptions,
  CACHE_ADMIN_ORDER_LIST_SEARCH,
} from '../../actions/Search.js';

import {
  shipStatus,
} from '../../shared/order.js';
import {
  FORM_ORDER_LIST_SEARCHER,
} from '../../shared/form.js';

import Selector from '../Form/Selector.jsx';
import Input from '../Form/Input.jsx';
import SubmitButton from '../Form/SubmitButton.jsx';
import Button from '../Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  searchWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    padding: '12px 0',
  },
  rowWrapper: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    margin: '5px 0',
  },
  rowItem: {
    margin: '0 5px',
  },
  searchBtn: {
    backgroundColor: 'rgb(182, 184, 204)',
    border: '1px solid rgb(182, 184, 204)',
    color: 'white',
  },
  createBtn: {
    backgroundColor: 'rgb(182, 184, 204)',
    border: '1px solid rgb(182, 184, 204)',
    color: 'white',
    margin: '0 5px',
  },
};

type Props = {
  cacheOrderSearch: Function,
  handleSubmit: Function,
  history: object,
};

class OrderListSearcher extends Component<Props> {
  submit(d) {
    const {
      cacheOrderSearch,
    } = this.props;

    const {
      keyword,
      memberId,
      shippingStatus,
      orderSN,
      closeDate,
      qrcode,
    } = d;

    cacheOrderSearch(CACHE_ADMIN_ORDER_LIST_SEARCH, {
      keyword: keyword || null,
      memberId: memberId === '-1' ? null : parseInt(memberId, 10),
      status: shippingStatus === '-1' ? null : shippingStatus,
      orderSN: orderSN || null,
      closeDate: closeDate || null,
      qrcode: qrcode || null,
    });
  }

  render() {
    const {
      handleSubmit,
      history,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <form onSubmit={handleSubmit(d => this.submit(d))} style={styles.searchWrapper}>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Query query={GET_EMPLOYEE_LIST}>
                {({ data }) => {
                  if (!data) return null;
                  const {
                    employeeMemberlist,
                  } = data;

                  return (
                    <Field
                      name="memberId"
                      label="包膜師:"
                      nullable
                      placeholder="請選擇"
                      options={employeeMemberlist || []}
                      component={Selector} />
                  );
                }}
              </Query>
            </div>
            <div style={styles.rowItem}>
              <Field
                name="closeDate"
                label="結單日期："
                placeholder="結單日期"
                type="date"
                component={Input} />
            </div>
          </div>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Field
                name="shippingStatus"
                label="出貨狀態:"
                nullable
                placeholder="請選擇"
                options={shipStatus}
                component={Selector} />
            </div>
            <div style={styles.rowItem}>
              <Field
                name="orderSN"
                label="訂單編號："
                placeholder="訂單編號"
                component={Input} />
            </div>
          </div>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <Field
                name="keyword"
                label="關鍵字:"
                placeholder="品名、料號"
                component={Input} />
            </div>
            <div style={styles.rowItem}>
              <Field
                name="qrcode"
                label="商品序號："
                placeholder="商品序號"
                component={Input} />
            </div>
          </div>
          <div style={styles.rowWrapper}>
            <div style={styles.rowItem}>
              <SubmitButton style={styles.searchBtn} label="查詢" />
              <Button
                style={styles.createBtn}
                onClick={() => history.push('/invoicing/createOrder')}
                label="新增訂單" />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_ORDER_LIST_SEARCHER,
});

const reduxHook = connect(
  () => ({
  }),
  dispatch => bindActionCreators({
    cacheOrderSearch: cacheSearchOptions,
  }, dispatch)
);

export default withRouter(
  reduxHook(
    formHook(
      radium(OrderListSearcher)
    )
  )
);
