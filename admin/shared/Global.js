export const ADMIN_LINE_LOGIN_PATH = `https://access.line.me/oauth2/v2.1/authorize?response_type=code&client_id=1599855723&redirect_uri=${API_HOST}/auth/adminLineLogin&state=test&scope=openid%20profile`;
export const LINEBOT_LINE_LOGIN_PATH = `https://access.line.me/oauth2/v2.1/authorize?response_type=code&client_id=1599855723&redirect_uri=${ADMIN_HOST}/linebot/register&state=test&scope=openid%20profile`;

export const days = [{
  id: 1,
  name: '一',
}, {
  id: 2,
  name: '二',
}, {
  id: 3,
  name: '三',
}, {
  id: 4,
  name: '四',
}, {
  id: 5,
  name: '五',
}, {
  id: 6,
  name: '六',
}, {
  id: 7,
  name: '七',
}];

export const navBarLinks = [{
  id: 1,
  pathname: '/landing',
  title: '官網首頁管理',
  actionKeys: ['ADMIN_WEBSITE_MANAGE'],
}, {
  id: 2,
  pathname: '/activity',
  title: '官網活動管理',
  actionKeys: ['ADMIN_WEBSITE_MANAGE'],
}, {
  id: 3,
  pathname: '/phoneModel',
  title: '手機資料上架管理',
  actionKeys: ['PHONE_MODEL_MANAGE'],
}, {
  id: 4,
  pathname: '/order',
  title: '線上訂單管理',
  actionKeys: ['ONLINE_ORDER_MANAGE'],
}, {
  id: 5,
  pathname: '/pnModel',
  title: '料號管理系統',
  actionKeys: ['PNTABLE_MANAGE'],
}, {
  id: 6,
  pathname: '/invoicing',
  title: '進銷存管理系統',
  actionKeys: ['INVOICING_MANAGE'],
}, {
  id: 7,
  pathname: '/systemInfo',
  title: '系統資訊管理',
  actionKeys: ['ADMIN_SYSTEMINFO_MANAGE'],
}, {
  id: 8,
  pathname: '/performance',
  title: '業績管理',
  actionKeys: ['ADMIN_PERFORMANCE_MANAGE'],
}, {
  id: 9,
  pathname: '/rollback',
  title: '問題反應管理',
  actionKeys: ['ADMIN_DOA_MANAGE'],
}, {
  id: 10,
  pathname: '/announcement',
  title: '公告系統管理',
  actionKeys: ['ADMIN_NOTIFICATION_MANAGE'],
}, {
  id: 11,
  pathname: '/member',
  title: '線上會員管理',
  actionKeys: ['ADMIN_MEMBER_MANAGE'],
}, {
  id: 12,
  pathname: '/authorization',
  title: '會員權限管理',
  actionKeys: ['ADMIN_MEMBER_MANAGE'],
}, {
  id: 13,
  pathname: '/punchManagement',
  title: '打卡管理',
  actionKeys: ['ADMIN_MEMBER_MANAGE'],
}];

export const PAYED_STATUS_SPEC = {
  UNPAID: '未付款',
  PAIDNOTCONFIRMED: '已付款(處理中)',
  PAIDANDCONFIRMED: '已付款',
  CANCELED: '已取消',
  ROLLBACKED: '已退款',
};

export const SHIPPING_STATUS_SPEC = {
  ORDERING: '訂貨中',
  UNDISPATCHED: '等待出貨',
  DISPATCHED: '已出貨',
  DISPATCHEDANDCHECKED: '已點貨',
};

export default null;
