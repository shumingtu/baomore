export const pnCategoryTypes = [{
  id: 'protector',
  name: '保貼',
}, {
  id: 'material',
  name: '膜料/工具/其他',
}];

export const returnCategoryTypes = [{
  id: 'doa',
  name: 'DOA 退回',
}, {
  id: 'destroy',
  name: '下架退回',
}, {
  id: 'free',
  name: '公關銷帳',
}];

export const customerReceivedProtectorErrorTypes = [{
  id: '靜電膠外漏(移膠)',
  name: '靜電膠外漏(移膠)',
}, {
  id: '靜電膠不黏(保貼脫落)',
  name: '靜電膠不黏(保貼脫落)',
}, {
  id: '有汙點/靜電膠塗佈不均',
  name: '有汙點/靜電膠塗佈不均',
}, {
  id: '表面瑕疵:凹點/凸點',
  name: '表面瑕疵:凹點/凸點',
}, {
  id: '彩虹紋',
  name: '彩虹紋',
}, {
  id: '內外品項不符',
  name: '內外品項不符',
}, {
  id: '其他',
  name: '其他',
}];

export const customerReceivedMaterialErrorTypes = [{
  id: '表面刮傷/污漬',
  name: '表面刮傷/污漬',
}, {
  id: '膜料入塵',
  name: '膜料入塵',
}, {
  id: '膜料皺褶',
  name: '膜料皺褶',
}, {
  id: '壓紋不明顯/水波紋',
  name: '壓紋不明顯/水波紋',
}, {
  id: '浮雕層瑕疵',
  name: '浮雕層瑕疵',
}, {
  id: '其他',
  name: '其他',
}];

export const employeeProtectorErrorTypes = [{
  id: '異常浮邊',
  name: '異常浮邊',
}, {
  id: '靜電膠外漏(移膠)',
  name: '靜電膠外漏(移膠)',
}, {
  id: '靜電膠不黏(保貼脫落)',
  name: '靜電膠不黏(保貼脫落)',
}, {
  id: '到貨破裂',
  name: '到貨破裂',
}, {
  id: '施作破裂',
  name: '施作破裂',
}, {
  id: '有汙點/靜電膠塗佈不均',
  name: '有汙點/靜電膠塗佈不均',
}, {
  id: '表面瑕疵:凹點/凸點',
  name: '表面瑕疵:凹點/凸點',
}, {
  id: '觸控不良',
  name: '觸控不良',
}, {
  id: '彩虹紋',
  name: '彩虹紋',
}, {
  id: '內外品項不符',
  name: '內外品項不符',
}, {
  id: '其他',
  name: '其他',
}];

export const employeeMaterialErrorTypes = [{
  id: '表面刮傷/污漬',
  name: '表面刮傷/污漬',
}, {
  id: '膜料入塵',
  name: '膜料入塵',
}, {
  id: '膜料皺褶',
  name: '膜料皺褶',
}, {
  id: '壓紋不明顯/水波紋',
  name: '壓紋不明顯/水波紋',
}, {
  id: '浮雕層瑕疵',
  name: '浮雕層瑕疵',
}, {
  id: '其他',
  name: '其他',
}];

export const inventoryPullofErrorTypes = [{
  id: '商品下架',
  name: '商品下架',
}, {
  id: '到貨異常',
  name: '到貨異常',
}, {
  id: '其他',
  name: '其他',
}];

export const inventoryRollbackTypeSpec = {
  DOA: 'DOA', // DOA 退回
  RETURN: 'PULLOF', // 下架退回
  FREE: 'SOCIAL', // 公關銷帳
  CUSTOMERCOMPLAINT: 'CUSTOMERCOMPLAINT', // 客訴退回
  NONE: 'NONE', // 無
};

export default null;
