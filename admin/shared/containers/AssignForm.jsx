// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Fields,
  SubmissionError,
  initialize,
} from 'redux-form';
import {
  Mutation,
  graphql,
} from 'react-apollo';

import SubmitButton from '../../components/Form/SubmitButton.jsx';
import FormTitle from '../../components/Global/FormTitle.jsx';
import AssignTimeSelector from '../../components/Form/AssignTimeSelector.jsx';
import Button from '../../components/Global/Button.jsx';

import { validateAssignOrderForm } from '../../helper/validator/AssignOrderValidator.js';
import {
  ASSIGN_ORDER_TO_EMPLOYEE,
} from '../../mutations/Order.js';
import {
  FETCH_SINGLE_ORDER,
} from '../../queries/Order.js';
import { FORM_ASSIGN_ORDER_FORM } from '../form.js';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 12,
    '@media (max-width: 767px)': {
      justifyContent: 'center',
    },
  },
  formWrapper: {
    width: 650,
    height: 'auto',
    padding: '16px 32px',
    backgroundColor: '#fff',
    border: '1px solid rgb(182, 184, 204)',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    '@media (max-width: 767px)': {
      width: '100%',
      padding: '24px 12px',
    },
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
  },
  submitBtn: {
    width: 88,
    height: 32,
    backgroundColor: 'rgb(182, 184, 204)',
    margin: '0 3px',
  },
  cancelBtn: {
    width: 88,
    height: 32,
    margin: '0 3px',
    backgroundColor: 'white',
    border: '1px solid rgb(182, 184, 204)',
    color: 'rgb(182, 184, 204)',
  },
};

type Props = {
  history: object,
  handleSubmit: Function,
  match: object,
  onlineOrder: object,
  initializeForm: Function,
};

class AssignForm extends PureComponent<Props> {
  state = {
    originTime: null,
  };

  componentDidMount() {
    const {
      onlineOrder,
    } = this.props;

    if (onlineOrder) this.initializing(onlineOrder);
  }

  componentDidUpdate(prevProps) {
    const {
      onlineOrder,
    } = this.props;

    if (prevProps.onlineOrder !== onlineOrder && onlineOrder) {
      this.initializing(onlineOrder);
    }
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    initializeForm({
      storeId: (obj && obj.store && obj.store.id) || '-1',
      employeeId: (obj && obj.employee && obj.employee.id) || '-1',
      date: (obj && obj.date) || null,
      time: (obj && obj.time) || '-1',
    });

    if (obj && obj.time) this.setState({ originTime: obj.time });
  }


  async submit(d, mutate) {
    const errors = validateAssignOrderForm(d);

    if (errors) throw new SubmissionError(errors);

    const {
      employeeId,
      date,
      storeId,
      time,
    } = d;

    const {
      match: {
        params: {
          onlineOrderId,
          directorId,
        },
      },
      history,
    } = this.props;

    const payload = {
      onlineOrderId,
      employeeId: parseInt(employeeId, 10),
      date,
      storeId: parseInt(storeId, 10),
      time,
    };

    await mutate({
      variables: {
        ...payload,
      },
    }).catch((e) => {
      window.alert(e);
      throw e;
    });

    if (directorId) {
      window.alert('轉派成功, 請關閉視窗！');
      return;
    }

    history.push('/order/list');
  }

  render() {
    const {
      history,
      handleSubmit,
    } = this.props;

    const {
      originTime,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        <FormTitle
          title="轉派訂單" />
        <Mutation mutation={ASSIGN_ORDER_TO_EMPLOYEE}>
          {assignOrderToEmployee => (
            <form
              onSubmit={handleSubmit(d => this.submit(d, assignOrderToEmployee))}
              style={styles.formWrapper}>
              <Fields
                originTime={originTime}
                names={[
                  'storeId',
                  'employeeId',
                  'date',
                  'time',
                ]}
                component={AssignTimeSelector} />
              <div style={styles.functionWrapper}>
                <SubmitButton
                  style={styles.submitBtn} />
                <Button onClick={() => history.push('/order/list')} style={styles.cancelBtn} label="取消" />
              </div>
            </form>
          )}
        </Mutation>
      </div>
    );
  }
}

const queryHook = graphql(FETCH_SINGLE_ORDER, {
  options: ({
    match: {
      params: {
        onlineOrderId,
      },
    },
  }) => ({
    variables: {
      onlineOrderId: onlineOrderId || null,
    },
  }),
  props: ({
    data: {
      onlineOrder,
    },
  }) => ({
    onlineOrder: onlineOrder || null,
  }),
});

const formHook = reduxForm({
  form: FORM_ASSIGN_ORDER_FORM,
});

const reduxHook = connect(
  () => ({
  }),
  dispatch => bindActionCreators({
    initializeForm: v => initialize(FORM_ASSIGN_ORDER_FORM, v),
  }, dispatch)
);

export default reduxHook(
  queryHook(
    formHook(
      radium(AssignForm)
    )
  )
);
