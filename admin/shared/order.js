export const shipStatus = [{
  id: 'ORDERING',
  name: '訂貨中',
}, {
  id: 'UNDISPATCHED',
  name: '等待出貨',
}, {
  id: 'DISPATCHED',
  name: '已出貨',
}, {
  id: 'DISPATCHEDANDCHECKED',
  name: '已點貨',
}];

export const payStatus = [{
  id: 'UNPAID',
  name: '尚未付款',
}, {
  id: 'PAIDNOTCONFIRMED',
  name: '已付款包膜師未確認',
}, {
  id: 'PAIDANDCONFIRMED',
  name: '已付款包膜師已確認',
}, {
  id: 'CANCELED',
  name: '已取消',
}, {
  id: 'ROLLBACKED',
  name: '已退款',
}];

export default null;
