// @flow

import React from 'react';
import {
  Route,
  Redirect,
} from 'react-router-dom';
import { Switch } from 'react-router';
import debug from 'debug';
import { ApolloProvider } from 'react-apollo';
import { hot } from 'react-hot-loader';
import { StyleRoot } from 'radium';
import { Provider, connect } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';

import MainBoard from './containers/MainBoard.jsx';
import LoginPage from './containers/LoginPage.jsx';
import LinebotLoginPage from './linebot/LinebotLoginPage.jsx';
import LinebotRegisterPage from './linebot/LinebotRegisterPage.jsx';
import LinebotMainBoard from './linebot/LinebotMainBoard.jsx';

// Debug mode
if (process.env.NODE_ENV !== 'production') {
  debug.enable('Baomore_Admin:*');
}

const styles = {
  root: {
    width: '100%',
    height: '100%',
    display: 'block',
    position: 'relative',
    overflow: 'auto',
  },
};

function AdminRoute({
  component,
  history,
  accessToken,
  location: {
    pathname,
  },
  ...componentProps
}: {
  component: {},
  history: {
    push: Function,
  },
  location: {
    pathname: string,
    search: string,
  },
  accessToken: string,
}) {
  const isRedirectFromLine = pathname === '/oAuth';

  if (!accessToken && !isRedirectFromLine) {
    return (
      <Route
        {...componentProps}
        render={() => (
          <Redirect
            to={{ pathname: '/login' }} />
        )} />
    );
  }

  return (
    <Switch>
      <Route path="/" component={MainBoard} />
    </Switch>
  );
}

const AdminRouteWithToken = connect(
  state => ({
    accessToken: state.Member.accessToken,
  }),
)(AdminRoute);

function LinebotRoute({
  component,
  history,
  accessToken,
  match: {
    url,
  },
  ...componentProps
}: {
  component: {},
  history: {
    push: Function,
  },
  match: {
    url: string,
  },
  accessToken: string,
}) {
  if (!accessToken) {
    return (
      <Route
        {...componentProps}
        render={() => (
          <Redirect
            to={{ pathname: `${url}/login` }} />
        )} />
    );
  }

  return (
    <Switch>
      <Route path={`${url}`} component={LinebotMainBoard} />
    </Switch>
  );
}

const LinebotRouteWithToken = connect(
  state => ({
    accessToken: state.Member.accessToken,
  }),
)(LinebotRoute);

function App({
  client,
  store,
  history,
}: any) {
  return (
    <ApolloProvider client={client} store={store}>
      <Provider store={store}>
        <StyleRoot style={styles.root}>
          <ConnectedRouter history={history}>
            <Switch>
              <Route exact path="/linebot/login" component={LinebotLoginPage} />
              <Route exact path="/linebot/register" component={LinebotRegisterPage} />
              <Route path="/linebot" component={LinebotRouteWithToken} />
              <Route exact path="/login" component={LoginPage} />
              <Route path="/" component={AdminRouteWithToken} />
            </Switch>
          </ConnectedRouter>
        </StyleRoot>
      </Provider>
    </ApolloProvider>
  );
}

export default hot(module)(App);
