export const CACHE_PNMODEL_LIST_SEARCH_OPTION = 'CACHE_PNMODEL_LIST_SEARCH_OPTION';
export const TOGGLE_PNMODEL_POPUP = 'TOGGLE_PNMODEL_POPUP';
export const SET_CURRENT_SELECTED_PNCATEGORY = 'SET_CURRENT_SELECTED_PNCATEGORY';
export const SET_CURRENT_SELECTED_BIGSUBCATEGORY = 'SET_CURRENT_SELECTED_BIGSUBCATEGORY';
export const SET_CURRENT_SELECTED_SMALLSUBCATEGORY = 'SET_CURRENT_SELECTED_SMALLSUBCATEGORY';
export const TOGGLE_PNSUBCATEGORY_CREATEBOX = 'TOGGLEP_PNSUBCATEGORY_CREATEBOX';

export function togglePNSubcategoryCreateBox(options) {
  return {
    type: TOGGLE_PNSUBCATEGORY_CREATEBOX,
    options,
  };
}

export function setCurrentSelectedPNCategory(id) {
  return {
    type: SET_CURRENT_SELECTED_PNCATEGORY,
    id,
  };
}

export function setCurrentSelectedBigSubCategory(id) {
  return {
    type: SET_CURRENT_SELECTED_BIGSUBCATEGORY,
    id,
  };
}

export function setCurrentSelectedSmallSubCategory(id) {
  return {
    type: SET_CURRENT_SELECTED_SMALLSUBCATEGORY,
    id,
  };
}

export function cachePNModelListSearchOption(options) {
  return {
    type: CACHE_PNMODEL_LIST_SEARCH_OPTION,
    options,
  };
}

export function togglePNModelPopUp(options) {
  return {
    type: TOGGLE_PNMODEL_POPUP,
    options,
  };
}
