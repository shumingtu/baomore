export const TOGGLE_ROLE_EDIT_BOX = 'TOGGLE_ROLE_EDIT_BOX';

export function toggleRoleEditBox(options) {
  return {
    type: TOGGLE_ROLE_EDIT_BOX,
    options,
  };
}
