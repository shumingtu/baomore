export const TOGGLE_CREATE_EDIT_BOX = 'TOGGLE_CREATE_EDIT_BOX';
export const CACHE_INVOICING_SEARCHER_OPTIONS = 'CACHE_INVOICING_SEARCHER_OPTIONS';

export function toggleCreateEditBox(options) {
  return {
    type: TOGGLE_CREATE_EDIT_BOX,
    options,
  };
}

export function cacheInvoicingSearcher(options) {
  return {
    type: CACHE_INVOICING_SEARCHER_OPTIONS,
    options,
  };
}
