export const CACHE_ORDER_LIST_SEARCH_OPTION = 'CACHE_ORDER_LIST_SEARCH_OPTION';
export const CACHE_ORDER_SCHEDULE_SEARCH_OPTION = 'CACHE_ORDER_SCHEDULE_SEARCH_OPTION';
export const OPEN_SCHEDULE_BOX = 'OPEN_SCHEDULE_BOX';
export const TOGGLE_ORDER_EDIT_BOX = 'TOGGLE_ORDER_EDIT_BOX';

export function toggleOrderEditBox(options) {
  return {
    type: TOGGLE_ORDER_EDIT_BOX,
    options,
  };
}

export function cacheOrderListSearchOption(options) {
  return {
    type: CACHE_ORDER_LIST_SEARCH_OPTION,
    options,
  };
}

export function cacheOrderScheduleSearchOption(options) {
  return {
    type: CACHE_ORDER_SCHEDULE_SEARCH_OPTION,
    options,
  };
}

export function openScheduleBox(options) {
  return {
    type: OPEN_SCHEDULE_BOX,
    options,
  };
}
