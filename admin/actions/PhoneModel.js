export const SET_CURRENT_SELECTED_BRAND = 'PHONEMODEL/SET_CURRENT_SELECTED_BRAND';
export const SET_CURRENT_SELECTED_MODEL = 'PHONEMODEL/SET_CURRENT_SELECTED_MODEL';
export const CACHE_PHONE_LIST_SEARCH_OPTION = 'PHONEMODEL/CACHE_PHONE_LIST_SEARCH_OPTION';

export function setCurrentSelectedBrand(id) {
  return {
    type: SET_CURRENT_SELECTED_BRAND,
    id,
  };
}

export function setCurrentSelectedModel(id) {
  return {
    type: SET_CURRENT_SELECTED_MODEL,
    id,
  };
}

export function cachePhoneListSearchOption(options) {
  return {
    type: CACHE_PHONE_LIST_SEARCH_OPTION,
    options,
  };
}
