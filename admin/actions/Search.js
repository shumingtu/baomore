export const CACHE_LINEBOT_INVENTORY_PROTECTOR_SEARCH = 'SEARCH/CACHE_LINEBOT_INVENTORY_PROTECTOR_SEARCH';
export const CACHE_LINEBOT_INVENTORY_MATERIAL_SEARCH = 'SEARCH/CACHE_LINEBOT_INVENTORY_MATERIAL_SEARCH';
export const CACHE_LINEBOT_PURCHASE_PROTECTOR_SEARCH = 'SEARCH/CACHE_LINEBOT_PURCHASE_PROTECTOR_SEARCH';
export const CACHE_LINEBOT_PURCHASE_MATERIAL_SEARCH = 'SEARCH/CACHE_LINEBOT_PURCHASE_MATERIAL_SEARCH';
export const CACHE_LINEBOT_TRANSFER_PROTECTOR_SEARCH = 'SEARCH/CACHE_LINEBOT_TRANSFER_PROTECTOR_SEARCH';
export const CACHE_LINEBOT_TRANSFER_MATERIAL_SEARCH = 'SEARCH/CACHE_LINEBOT_TRANSFER_MATERIAL_SEARCH';
export const CACHE_LINEBOT_CUSTOMER_COMPLAINT_PROTECTOR_SEARCH = 'SEARCH/CACHE_LINEBOT_CUSTOMER_COMPLAINT_PROTECTOR_SEARCH';
export const CACHE_LINEBOT_CUSTOMER_COMPLAINT_MATERIAL_SEARCH = 'SEARCH/CACHE_LINEBOT_CUSTOMER_COMPLAINT_MATERIAL_SEARCH';
export const CACHE_LINEBOT_RETURN_PROTECTOR_SEARCH = 'SEARCH/CACHE_LINEBOT_RETURN_PROTECTOR_SEARCH';
export const CACHE_LINEBOT_RETURN_MATERIAL_SEARCH = 'SEARCH/CACHE_LINEBOT_RETURN_MATERIAL_SEARCH';
export const CACHE_LINEBOT_PUNCH_LIST_SEARCH = 'SEARCH/CACHE_LINEBOT_PUNCH_LIST_SEARCH';
export const CACHE_LINEBOT_EMPLOYEE_LIST_SEARCH = 'SEARCH/CACHE_LINEBOT_EMPLOYEE_LIST_SEARCH';
export const CACHE_LINEBOT_ANNOUNCEMENT_SEARCH = 'SEARCH/CACHE_LINEBOT_ANNOUNCEMENT_SEARCH';
export const CACHE_LINEBOT_PERFORMANCE_SEARCH = 'SEARCH/CACHE_LINEBOT_PERFORMANCE_SEARCH';
export const CACHE_ADMIN_PERFORMANCE_SEARCH = 'CACHE_ADMIN_PERFORMANCE_SEARCH';
export const CACHE_ADMIN_DIRECTOR_PERFORMANCE_SEARCH = 'CACHE_ADMIN_DIRECTOR_PERFORMANCE_SEARCH';
export const CACHE_ADMIN_STORE_PERFORMANCE_SEARCH = 'CACHE_ADMIN_STORE_PERFORMANCE_SEARCH';
export const CACHE_LINEBOT_STORE_SEARCH = 'SEARCH/CACHE_LINEBOT_STORE_SEARCH';
export const CACHE_LINEBOT_ORDER_SEARCH = 'SEARCH/CACHE_LINEBOT_ORDER_SEARCH';
// admin
export const CACHE_ADMIN_ROLLBACK_DOA_SEARCH = 'SEARCH/CACHE_ADMIN_ROLLBACK_DOA_SEARCH';
export const CACHE_ADMIN_ROLLBACK_PULLOF_SEARCH = 'SEARCH/CACHE_ADMIN_ROLLBACK_PULLOF_SEARCH';
export const CACHE_ADMIN_ROLLBACK_SOCIAL_SEARCH = 'SEARCH/CACHE_ADMIN_ROLLBACK_SOCIAL_SEARCH';
export const CACHE_ADMIN_ROLLBACK_CUSTOMER_SEARCH = 'SEARCH/CACHE_ADMIN_ROLLBACK_CUSTOMER_SEARCH';
export const CACHE_ADMIN_ANNOUNCEMENT_SEARCH = 'SEARCH/CACHE_ADMIN_ANNOUNCEMENT_SEARCH';
export const CACHE_ADMIN_ORDER_LIST_SEARCH = 'CACHE_ADMIN_ORDER_LIST_SEARCH';
export const CACHE_ADMIN_STOCK_LIST_SEARCH = 'CACHE_ADMIN_STOCK_LIST_SEARCH';
export const CACHE_ADMIN_RECORD_LIST_SEARCH = 'CACHE_ADMIN_RECORD_LIST_SEARCH';
export const CACHE_ADMIN_MEMBER_LIST_SEARCH = 'CACHE_ADMIN_MEMBER_LIST_SEARCH';
export const CACHE_ADMIN_STORE_INFO_LIST_SEARCH = 'CACHE_ADMIN_STORE_INFO_LIST_SEARCH';
export const CACHE_ADMIN_PUNCH_LIST_SEARCH = 'CACHE_ADMIN_PUNCH_LIST_SEARCH';

export function cacheSearchOptions(type, options) {
  return {
    type,
    options,
  };
}
