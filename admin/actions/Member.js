import { getMyRoles } from '../helper/roles.js';

export const MEMBER_INFO_CACHE = 'MEMBER/MEMBER_INFO_CACHE';
export const MEMBER_LOGOUT = 'MEMBER/MEMBER_LOGOUT';
export const MEMBER_SET_ROLE = 'MEMBER/MEMBER_SET_ROLE';
export const MEMBER_WARRANTY_BOX = 'MEMBER_WARRANTY_BOX';
export const MEMBER_ORDER_BOX = 'MEMBER_ORDER_BOX';

export function cacheMemberInfo(member) {
  return {
    type: MEMBER_INFO_CACHE,
    member,
  };
}

export function memberLogout() {
  return {
    type: MEMBER_LOGOUT,
  };
}

export function memberSetRole() {
  const roleActions = getMyRoles();

  return {
    type: MEMBER_SET_ROLE,
    roleActions,
  };
}

export function toggleWarrantyBox(options) {
  return {
    type: MEMBER_WARRANTY_BOX,
    options,
  };
}

export function toggleOrderBox(options) {
  return {
    type: MEMBER_ORDER_BOX,
    options,
  };
}
