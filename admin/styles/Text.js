import Theme from './Theme.js';

export default {
  labelText: {
    width: 100,
    fontSize: 14,
    fontWeight: 500,
    color: Theme.BLACK_COLOR,
    padding: '0 10px 5px 5px',
  },
  text: {
    fontSize: 16,
    fontWeight: 500,
    color: Theme.BLACK_COLOR,
  },
};
