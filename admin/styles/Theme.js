export default {
  MAIN_THEME_COLOR: '#22223e',
  SECONDARY_THEME_COLOR: '#a7d3e1',
  BLACK_COLOR: '#4a4a4a',
  ACTIVE_COLOR: '#ed7801',
  NAV_COLOR: '#6f778a',
  BLOCK_SHADOW: '0 5px 24px -9px rgba(0, 0, 0, 0.4)',
  ERROR_COLOR: 'rgb(200, 78, 66)',
  PURPLE_COLOR: 'rgb(173, 0, 129)',
  YELLOW_COLOR: 'rgb(242, 150, 0)',
};
