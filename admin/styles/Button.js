import Theme from './Theme.js';

export default {
  button: {
    width: 'auto',
    height: 'auto',
    padding: '6px 16px',
    fontSize: 14,
    fontWeight: 500,
    color: '#fff',
    border: 0,
    borderRadius: 3,
    backgroundColor: Theme.ACTIVE_COLOR,
    outline: 0,
    cursor: 'pointer',
    opacity: 1,
    ':hover': {
      opacity: 0.88,
    },
  },
  inactive: {
    backgroundColor: Theme.BLACK_COLOR,
  },
  btnDisabled: {
    opacity: 0.3,
    cursor: 'default',
  },
  subBtn: {
    backgroundColor: '#fff',
    border: `1px solid ${Theme.ACTIVE_COLOR}`,
    color: Theme.ACTIVE_COLOR,
  },
};
