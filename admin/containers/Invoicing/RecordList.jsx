// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';

import Table, { TableField } from '../../components/Table/Table.jsx';
import { FETCH_RECORD_LIST } from '../../queries/Behavior.js';

const styles = {
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 193px)',
    maxHeight: 610,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
};

type Props = {
  options: {
    behaviorId: number,
    employeeId: number,
    startDate: string,
    endDate: string,
  },
};

const RECORD_LIST_LIMIT = 10;

class RecordList extends PureComponent<Props> {
  render() {
    const {
      options,
    } = this.props;

    return (
      <Query
        query={FETCH_RECORD_LIST}
        variables={options}>
        {({ data, fetchMore }) => {
          if (!data) return null;

          const {
            recordList = [],
          } = data;

          return (
            <div style={[
              styles.tableWrapper,
              options && options.behaviorId === 1 && {
                height: 'calc(100% - 323px)',
              },
            ]}>
              <Table
                dataSource={recordList.map(s => ({
                  id: s.id,
                  behavior: s.behavior.name,
                  member: s.member.name,
                  remark: s.remark,
                  qrcode: s.qrcode,
                  orderSN: s.orderSN,
                  startTime: s.startTime,
                  endTime: s.endTime,
                  price: s.price,
                  createdAt: s.createdAt,
                }))}
                fetchMore={() => fetchMore({
                  variables: {
                    ...options,
                    offset: 0,
                    limit: recordList.length + RECORD_LIST_LIMIT,
                  },
                  updateQuery: (prev, { fetchMoreResult }) => {
                    if (!fetchMoreResult) return prev;
                    return {
                      ...prev,
                      recordList: [
                        ...fetchMoreResult.recordList,
                      ],
                    };
                  },
                })}
                showPlaceholder={!recordList.length}
                placeholder="查無資料">
                <TableField name="行為類型" fieldKey="behavior" flex={1} isCenter />
                <TableField name="包膜師" fieldKey="member" flex={1} isCenter />
                <TableField name="備註" fieldKey="remark" flex={2} isCenter />
                <TableField name="商品序號" fieldKey="qrcode" flex={2} isCenter />
                <TableField name="訂單編號" fieldKey="orderSN" flex={1} isCenter />
                <TableField name="銷貨開始時間" fieldKey="startTime" flex={1} isCenter />
                <TableField name="銷貨結束時間" fieldKey="endTime" flex={1} isCenter />
                <TableField name="價格" fieldKey="price" flex={1} isCenter />
                <TableField name="紀錄時間" fieldKey="createdAt" flex={1} isCenter />
              </Table>
            </div>
          );
        }}
      </Query>
    );
  }
}

const reduxHook = connect(
  state => ({
    options: state.Search.adminRecordList,
  }),
);

export default reduxHook(
  radium(
    RecordList
  )
);
