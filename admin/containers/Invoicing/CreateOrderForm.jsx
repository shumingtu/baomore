// @flow
import React, { Component } from 'react';
import radium from 'radium';
import {
  reduxForm,
  Field,
  SubmissionError,
} from 'redux-form';
import { Mutation, Query } from 'react-apollo';

import { validateOrderCreateForm } from '../../helper/validator/OrderCreateValidator.js';

import Input from '../../components/Form/Input.jsx';
import Selector from '../../components/Form/Selector.jsx';
import SubmitButton from '../../components/Form/SubmitButton.jsx';
import FormTitle from '../../components/Global/FormTitle.jsx';
import CodeInput from '../../components/Form/CodeInput.jsx';
import Button from '../../components/Global/Button.jsx';

import { FORM_ORDER_CREATE_FORM } from '../../shared/form.js';

import {
  GET_EMPLOYEE_LIST,
} from '../../queries/Member.js';
import {
  FETCH_STORES_LIST,
} from '../../queries/Store.js';
import {
  CREATE_ORDER,
} from '../../mutations/Order.js';

const styles = {
  wrapper: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 12,
  },
  formWrapper: {
    width: 650,
    height: 'auto',
    padding: '16px 32px',
    backgroundColor: '#fff',
    border: '1px solid rgb(182, 184, 204)',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    margin: 5,
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
  },
  editBtn: {
    width: 88,
    height: 32,
    backgroundColor: 'rgb(182, 184, 204)',
    margin: '0 3px',
  },
  cancelBtn: {
    width: 88,
    height: 32,
    margin: '0 3px',
    backgroundColor: 'white',
    border: '1px solid rgb(182, 184, 204)',
    color: 'rgb(182, 184, 204)',
  },
};

const productTypes = [{
  id: 'MATERIAL',
  name: '膜料',
}, {
  id: 'PROTECTOR',
  name: '保貼',
}];

type Props = {
  history: object,
  handleSubmit: Function,
};

class CreateOrderForm extends Component<Props> {
  async submit(d, mutate) {
    const errors = validateOrderCreateForm(d);

    if (errors) throw new SubmissionError(errors);

    const {
      history,
    } = this.props;

    const {
      quantity,
      productType,
      employeeId,
      code,
      storeId,
    } = d;

    await mutate({
      variables: {
        quantity: parseInt(quantity, 10),
        productType,
        employeeId: parseInt(employeeId, 10),
        code,
        storeId: parseInt(storeId, 10),
      },
    });

    history.push('/invoicing');
  }

  render() {
    const {
      history,
      handleSubmit,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <FormTitle
          title="新增訂單" />
        <Mutation mutation={CREATE_ORDER}>
          {createOrder => (
            <form
              onSubmit={handleSubmit(d => this.submit(d, createOrder))}
              style={styles.formWrapper}>
              <div style={styles.fieldWrapper}>
                <Query query={GET_EMPLOYEE_LIST}>
                  {({ data }) => {
                    if (!data) return null;
                    const {
                      employeeMemberlist,
                    } = data;

                    return (
                      <Field
                        name="employeeId"
                        label="包膜師:"
                        nullable
                        placeholder="請選擇"
                        options={employeeMemberlist || []}
                        component={Selector} />
                    );
                  }}
                </Query>
              </div>
              <div style={styles.fieldWrapper}>
                <Field
                  label="料號編號:"
                  name="code"
                  component={CodeInput} />
              </div>
              <div style={styles.fieldWrapper}>
                <Field
                  label="數量:"
                  type="number"
                  name="quantity"
                  component={Input} />
              </div>
              <div style={styles.fieldWrapper}>
                <Field
                  label="產品類型:"
                  name="productType"
                  nullable
                  placeholder="請選擇"
                  options={productTypes || []}
                  component={Selector} />
              </div>
              <div style={styles.fieldWrapper}>
                <Query query={FETCH_STORES_LIST}>
                  {({ data }) => {
                    if (!data) return null;
                    const {
                      stores,
                    } = data;

                    return (
                      <Field
                        name="storeId"
                        label="門市:"
                        nullable
                        placeholder="請選擇"
                        options={stores || []}
                        component={Selector} />
                    );
                  }}
                </Query>
              </div>
              <div style={styles.functionWrapper}>
                <SubmitButton
                  style={styles.editBtn} />
                <Button onClick={() => history.goBack()} style={styles.cancelBtn} label="取消" />
              </div>
            </form>
          )}
        </Mutation>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_ORDER_CREATE_FORM,
});

export default formHook(
  radium(CreateOrderForm)
);
