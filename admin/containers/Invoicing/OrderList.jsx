// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';
import {
  Switch,
  Route,
  withRouter,
} from 'react-router-dom';

import OrderListActions from '../../components/Table/Actions/OrderListActions.jsx';
import Table, { TableField } from '../../components/Table/Table.jsx';
import QRCodePopup from '../../components/Invoicing/QRCodePopup.jsx';
import { GET_ORDER_LIST } from '../../queries/Order.js';
import { shippingStatusDecider } from '../../helper/statusDecider';

const styles = {
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 279px)',
    maxHeight: 810,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
};

type Props = {
  options: {
    memberId: number,
    status: string,
    keyword: string,
  },
  match: object,
  history: object,
};

const ORDER_LIST_LIMIT = 10;

class OrderList extends PureComponent<Props> {
  render() {
    const {
      options,
      match: {
        url,
      },
      history,
    } = this.props;

    return (
      <>
        <Switch>
          <Route
            path={`${url}/qrcodes/:orderId`}
            component={props => (
              <QRCodePopup {...props} onClose={() => history.push(url)} />
            )} />
        </Switch>
        <Query
          query={GET_ORDER_LIST}
          variables={options}>
          {({ data, fetchMore }) => {
            if (!data) return null;

            const {
              orderList = [],
            } = data;

            return (
              <div style={styles.tableWrapper}>
                <Table
                  dataSource={orderList.map((o) => {
                    const {
                      onlineOrder,
                    } = o;
                    const orderSN = `${o.orderSN || '無'}${onlineOrder ? `\n(${onlineOrder.id})` : ''}`;

                    return {
                      id: o.id,
                      pnTableId: o.pnTable.id,
                      orderSN,
                      closeDate: (o.orderGropShipment && o.orderGropShipment.createdAt) || '尚未結單',
                      code: o.pnTable.code,
                      pnTableName: o.pnTable.name,
                      pnCategory: o.pnTable.pnCategory && o.pnTable.pnCategory.name,
                      bigSubCategory: o.pnTable.bigSubCategory && o.pnTable.bigSubCategory.name,
                      smallSubCategory: o.pnTable.smallSubCategory && o.pnTable.smallSubCategory.name,
                      price: o.pnTable.price,
                      vendor: o.pnTable.vendor.name,
                      suitableDevice: o.pnTable.suitableDevice,
                      picture: o.pnTable.picture,
                      description: o.pnTable.description,
                      quantity: o.quantity,
                      status: shippingStatusDecider(o.status),
                      member: o.Member && o.Member.name,
                      storeId: o.store && o.store.id,
                      channelId: o.store && o.store.channel.id,
                      districtId: o.store && o.store.district.id,
                    };
                  })}
                  fetchMore={() => fetchMore({
                    variables: {
                      ...options,
                      offset: 0,
                      limit: orderList.length + ORDER_LIST_LIMIT,
                    },
                    updateQuery: (prev, { fetchMoreResult }) => {
                      if (!fetchMoreResult) return prev;
                      return {
                        ...prev,
                        orderList: [
                          ...fetchMoreResult.orderList,
                        ],
                      };
                    },
                  })}
                  showPlaceholder={!orderList.length}
                  placeholder="查無資料"
                  actionTitles={['操作']}
                  getActions={() => [
                    <OrderListActions url={url} />,
                  ]}>
                  <TableField name="訂單編號" fieldKey="orderSN" flex={1} isCenter />
                  <TableField name="結單日期" fieldKey="closeDate" flex={1} isCenter />
                  <TableField name="品名" fieldKey="pnTableName" flex={1} isCenter />
                  <TableField name="數量" fieldKey="quantity" flex={1} isCenter />
                  <TableField name="狀態" fieldKey="status" flex={1} isCenter />
                  <TableField name="包膜師" fieldKey="member" flex={1} isCenter />
                </Table>
              </div>
            );
          }}
        </Query>
      </>
    );
  }
}

const reduxHook = connect(
  state => ({
    options: state.Search.adminOrderList,
  }),
);

export default withRouter(
  reduxHook(
    radium(
      OrderList
    )
  )
);
