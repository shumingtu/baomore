// @flow
import React, {
  Component,
  Fragment,
} from 'react';
import radium from 'radium';
import qs from 'qs';
import { connect } from 'react-redux';
import { Query, graphql, Mutation } from 'react-apollo';

import TablePlaceholder from '../../components/Table/TablePlaceholder.jsx';
import Button from '../../components/Global/Button.jsx';
import LookOverButton from '../../components/Global/LookOverButton.jsx';

import { FETCH_ORDER_GROUP_SHIPMENT_LIST } from '../../queries/OrderGroupShipment.js';
import { GET_EMPLOYEE_LIST } from '../../queries/Member.js';
import { UPDATE_GROUP_ORDER_STATUS } from '../../mutations/OrderGroupShipment.js';

import Theme from '../../styles/Theme.js';

const styles = {
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 343px)',
    maxHeight: 810,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    overflowY: 'hidden',
    overflowX: 'auto',
  },
  tableFrozenHeaderWrapper: {
    width: 'auto',
    height: 44,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
    padding: '0 4px 0 0',
    borderBottom: `4px solid ${Theme.SECONDARY_THEME_COLOR}`,
  },
  headerField: {
    width: 150,
    margin: '0 8px',
    textAlign: 'center',
    fontSize: 14,
    fontWeight: 500,
    color: Theme.BLACK_COLOR,
  },
  tableListWrapper: {
    width: 'auto',
    height: 'calc(100% - 44px)',
    overflow: 'auto',
  },
  tableItemWrapper: {
    width: 'auto',
    minHeight: 66,
    padding: '12px 0',
    margin: '0 0 8px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#fff',
    borderRadius: 2,
    border: 0,
    boxShadow: Theme.BLOCK_SHADOW,
  },
  tableItemPlacement: {
    width: 'auto',
    height: '100%',
    margin: '0 8px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  tableItem: {
    width: 150,
    fontSize: 13,
    fontWeight: 500,
    lineHeight: 1.618,
    whiteSpace: 'pre-line',
    color: Theme.BLACK_COLOR,
    textAlign: 'center',
  },
  tableFunction: {
    width: 150,
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '0 8px',
  },
  functionWrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 6px',
  },
  exportBtnWrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  exportBtn: {
    margin: '0 5px',
  },
  paddingWrapper: {
    display: 'flex',
    transition: '1s',
    transitionTImingFunction: 'ease-in-out',
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.04)',
    minHeight: 5,
  },
};

const INVOICING_LIST_LIMIT = 10;

type Props = {
  options: Object,
  orderGroupShipmentList: Array,
  fetchMore: Function,
  refetch: Function,
  match: Object,
  history: Object,
};

class InvoicingList extends Component<Props> {
  constructor(props) {
    super(props);

    this.fetching = false;
    this.reachEnd = false;
    this.scrollListener = () => this.onScroll();
    this.state = {
      paddingHeight: null,
    };
  }

  componentDidMount() {
    this.scroller.addEventListener('scroll', this.scrollListener, false);
  }

  componentDidUpdate(prevProps) {
    const {
      options,
      orderGroupShipmentList,
    } = this.props;

    if (prevProps.orderGroupShipmentList !== orderGroupShipmentList) {
      this.fetching = false;
    }

    if (prevProps.options !== options) {
      document.getElementById('invoicingScrollBar').scrollTop = 0;
    }
  }

  componentWillUnmount() {
    this.scroller.removeEventListener('scroll', this.scrollListener, false);
  }

  async onScroll() {
    const {
      scrollHeight,
      scrollTop,
      clientHeight,
    } = this.scroller;

    const {
      paddingHeight,
    } = this.state;

    const remainingHeight = scrollHeight - (scrollTop + clientHeight);

    if (remainingHeight < 150) {
      if (this.fetching) return;

      this.handleFetchMore();

      if (paddingHeight === null) {
        this.setState({
          paddingHeight: 30,
        });
      } else if (this.reachEnd) {
        this.setState({
          paddingHeight: 0,
        });
      }
    }
  }

  async handleFetchMore() {
    const {
      fetchMore,
      options,
      orderGroupShipmentList,
    } = this.props;

    if (this.fetching || !orderGroupShipmentList.length || !fetchMore) return;

    this.fetching = true;

    const { data } = await fetchMore(options);

    this.reachEnd = !data[Object.keys(data)[0]].length;
  }

  render() {
    const {
      orderGroupShipmentList,
      options,
      refetch,
    } = this.props;

    const {
      paddingHeight,
    } = this.state;

    return (
      <Fragment>
        <div style={styles.exportBtnWrapper}>
          <div style={styles.exportBtn}>
            <Button
              onClick={() => {
                const queryString = qs.stringify(qs.parse(options), { skipNulls: true });
                window.open(`${API_HOST}/export/e01?${queryString}&token=${localStorage.accessToken}`);
              }}
              label="匯出ERP訂單" />
          </div>
          <div style={styles.exportBtn}>
            <Button
              onClick={() => {
                const queryString = qs.stringify(qs.parse(options), { skipNulls: true });
                window.open(`${API_HOST}/export/orderDetail?${queryString}&token=${localStorage.accessToken}`);
              }}
              label="匯出廠商訂單" />
          </div>
          <div style={styles.exportBtn}>
            <Button
              onClick={() => {
                const queryString = qs.stringify(qs.parse(options), { skipNulls: true });
                window.open(`${API_HOST}/export/sendingInfo?${queryString}&token=${localStorage.accessToken}`);
              }}
              label="匯出訂單寄送地址" />
          </div>
          <div style={styles.exportBtn}>
            <Button
              onClick={() => {
                const queryString = qs.stringify(qs.parse(options), { skipNulls: true });
                window.open(`${API_HOST}/export/qrcodes?${queryString}&token=${localStorage.accessToken}`);
              }}
              label="匯出商品序號" />
          </div>
          <div style={styles.exportBtn}>
            <Button
              onClick={() => {
                const queryString = qs.stringify(qs.parse(options), { skipNulls: true });
                window.open(`${API_HOST}/export/e02?${queryString}&token=${localStorage.accessToken}`);
              }}
              label="匯出點貨記錄" />
          </div>
          <div style={styles.exportBtn}>
            <Button
              onClick={() => {
                const queryString = qs.stringify(qs.parse(options), { skipNulls: true });
                window.open(`${API_HOST}/export/e03?${queryString}&token=${localStorage.accessToken}`);
              }}
              label="匯出銷貨單" />
          </div>
          <div style={styles.exportBtn}>
            <Button
              onClick={() => {
                const queryString = qs.stringify(qs.parse(options), { skipNulls: true });
                window.open(`${API_HOST}/export/e04?${queryString}&token=${localStorage.accessToken}`);
              }}
              label="匯出借出歸還單" />
          </div>
        </div>
        <Query
          variables={{
            memberId: options.memberId || null,
            areaId: options.areaId || null,
          }}
          query={GET_EMPLOYEE_LIST}>
          {({ data }) => {
            if (!data) return null;
            const {
              employeeMemberlist = [],
            } = data;

            return (
              <div style={styles.tableWrapper}>
                <div style={styles.tableFrozenHeaderWrapper}>
                  <span style={[styles.headerField]}>
                    料號
                  </span>
                  <span style={[styles.headerField]}>
                    品名
                  </span>
                  <span style={[styles.headerField]}>
                    價格
                  </span>
                  <span style={[styles.headerField]}>
                    廠商
                  </span>
                  <span style={[styles.headerField, { flex: 2 }]}>
                    操作
                  </span>
                  <span style={[styles.headerField]}>
                    結單日期
                  </span>
                  <span style={[styles.headerField]}>
                    總數
                  </span>
                  {employeeMemberlist.map(member => (
                    <span key={member.id} style={styles.headerField}>
                      {`${member.area.name} ${member.name}`}
                    </span>
                  ))}
                </div>
                {orderGroupShipmentList.length === 0 ? <TablePlaceholder /> : null}
                <div
                  id="invoicingScrollBar"
                  className="hideScrollBar"
                  ref={(ref) => { this.scroller = ref; }}
                  style={styles.tableListWrapper}>
                  {orderGroupShipmentList.map((orderGroup) => {
                    const {
                      PNTable,
                      Orders,
                    } = orderGroup;

                    const boxData = {
                      id: PNTable.id,
                      bigSubCategory: PNTable.bigSubCategory && PNTable.bigSubCategory.name,
                      code: PNTable.code,
                      description: PNTable.description,
                      name: PNTable.name,
                      picture: PNTable.picture,
                      pnCategory: PNTable.pnCategory && PNTable.pnCategory.name,
                      price: PNTable.price,
                      smallSubCategory: PNTable.smallSubCategory && PNTable.smallSubCategory.name,
                      suitableDevice: PNTable.suitableDevice,
                      vendor: PNTable.vendor && PNTable.vendor.name,
                    };

                    return (
                      <div key={orderGroup.id} style={styles.tableItemWrapper}>
                        <div style={styles.tableItemPlacement}>
                          <span style={styles.tableItem}>
                            {(PNTable && PNTable.code) || null}
                          </span>
                        </div>
                        <div style={styles.tableItemPlacement}>
                          <span style={styles.tableItem}>
                            {(PNTable && PNTable.name) || null}
                          </span>
                          <span style={styles.tableItem}>
                            {Orders[0].orderSource === 'ONLINEORDER' ? Orders[0].orderSN : null}
                          </span>
                        </div>
                        <div style={styles.tableItemPlacement}>
                          <span style={styles.tableItem}>
                            {(PNTable && PNTable.price) || null}
                          </span>
                        </div>
                        <div style={styles.tableItemPlacement}>
                          <span style={styles.tableItem}>
                            {(PNTable && PNTable.vendor && PNTable.vendor.name) || null}
                          </span>
                        </div>
                        <div style={styles.tableFunction}>
                          <div style={styles.functionWrapper}>
                            <div style={styles.btnWrapper}>
                              <LookOverButton
                                data={boxData} />
                            </div>
                            <Mutation
                              mutation={UPDATE_GROUP_ORDER_STATUS}>
                              {updateGroupOrderStatus => (
                                <div style={styles.btnWrapper}>
                                  <Button
                                    onClick={async () => {
                                      await updateGroupOrderStatus({
                                        variables: {
                                          id: parseInt(orderGroup.id, 10),
                                        },
                                      });
                                      refetch(options);
                                    }}
                                    disabled={orderGroup.status === 'SHIPMENTED'}
                                    label={orderGroup.status === 'SHIPMENTED' ? '已出貨' : '出貨'} />
                                </div>
                              )}
                            </Mutation>
                          </div>
                        </div>
                        <div style={styles.tableItemPlacement}>
                          <span style={styles.tableItem}>
                            {orderGroup.SN}
                          </span>
                        </div>
                        <div style={styles.tableItemPlacement}>
                          <span style={styles.tableItem}>
                            {orderGroup.totalQuantity}
                          </span>
                        </div>
                        {employeeMemberlist.map((member) => {
                          const targetMemberOrder = Orders.find(x => x.Member.id === member.id);

                          if (targetMemberOrder) {
                            return (
                              <div key={targetMemberOrder.id} style={styles.tableItemPlacement}>
                                <span style={styles.tableItem}>
                                  {targetMemberOrder.quantity}
                                </span>
                              </div>
                            );
                          }

                          return (
                            <div key={member.id} style={styles.tableItemPlacement}>
                              <span style={styles.tableItem}>
                                0
                              </span>
                            </div>
                          );
                        })}
                      </div>
                    );
                  })}
                  <div style={{ opacity: this.reachEnd ? 1 : 0 }}>
                    <div style={{ ...styles.paddingWrapper, height: paddingHeight }} />
                  </div>
                </div>
              </div>
            );
          }}
        </Query>
      </Fragment>
    );
  }
}

const reduxHook = connect(
  state => ({
    options: state.Invoicing.listSearchOptions,
  }),
);

const queryHook = graphql(FETCH_ORDER_GROUP_SHIPMENT_LIST, {
  options: ownProps => ({
    variables: {
      orderSN: ownProps.options.orderSN || null,
      vendorId: ownProps.options.vendorId || null,
      memberId: ownProps.options.memberId || null,
      areaId: ownProps.options.areaId || null,
      shippingStatus: ownProps.options.shippingStatus || null,
      startDate: ownProps.options.startDate || null,
      endDate: ownProps.options.endDate || null,
      code: ownProps.options.code || null,
      limit: ownProps.options.limit || 10,
      offset: ownProps.options.offset || 0,
    },
    fetchPolicy: 'network-only',
  }),
  props: ({
    data: {
      orderGroupShipmentList,
      fetchMore,
      refetch,
    },
  }) => ({
    orderGroupShipmentList: orderGroupShipmentList || [],
    refetch: queryOptions => refetch({
      variables: {
        ...queryOptions,
      },
    }),
    fetchMore: queryOptions => fetchMore({
      variables: {
        ...queryOptions,
        offset: 0,
        limit: orderGroupShipmentList.length + INVOICING_LIST_LIMIT,
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prev;

        return {
          ...prev,
          orderGroupShipmentList: [
            ...fetchMoreResult.orderGroupShipmentList,
          ],
        };
      },
    }),
  }),
});

export default reduxHook(
  queryHook(
    radium(
      InvoicingList
    )
  )
);
