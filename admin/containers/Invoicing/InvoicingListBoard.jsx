// @flow

import React, { Component, Fragment } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';

import PageSwitcher from '../../components/Global/PageSwitcher.jsx';
import ClosedSettingBoard from './ClosedSettingBoard.jsx';
import {
  ClosedSettingEditForm,
  ClosedSettingCreateForm,
} from '../../components/Invoicing/ClosedSettingPopUp.jsx';
import OrderGroupShipmentSearcher from '../../components/Invoicing/OrderGroupShipmentSearcher.jsx';
import InvoicingList from './InvoicingList.jsx';
import PNModelPopUp from '../../components/PNModel/PNModelPopUp.jsx';
import OrderListSearcher from '../../components/Invoicing/OrderListSearcher.jsx';
import OrderList from './OrderList.jsx';
import OrderEditPopUp from '../../components/Order/OrderEditPopUp.jsx';
import StockListSearcher from '../../components/Invoicing/StockListSearcher.jsx';
import StockList from './StockList.jsx';
import RecordListSearcher from '../../components/Invoicing/RecordListSearcher.jsx';
import RecordList from './RecordList.jsx';


const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    position: 'relative',
  },
};

type Props = {
  boxData: Object,
  pnModelBoxData: Object,
  orderEditBoxData: Object,
};

class InvoicingListBoard extends Component<Props> {
  state = {
    currentPageIdx: 0,
  };

  pageSwitcher() {
    const {
      currentPageIdx,
    } = this.state;

    switch (currentPageIdx) {
      case 0:
        return (
          <Fragment>
            <OrderGroupShipmentSearcher />
            <InvoicingList />
          </Fragment>
        );

      case 1:
        return (
          <ClosedSettingBoard />
        );

      case 2:
        return (
          <Fragment>
            <OrderListSearcher />
            <OrderList />
          </Fragment>
        );

      case 3:
        return (
          <Fragment>
            <StockListSearcher />
            <StockList />
          </Fragment>
        );

      case 4:
        return (
          <Fragment>
            <RecordListSearcher />
            <RecordList />
          </Fragment>
        );

      default:
        return null;
    }
  }

  render() {
    const {
      currentPageIdx,
    } = this.state;

    const {
      boxData,
      pnModelBoxData,
      orderEditBoxData,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        {boxData && boxData.status ? (
          <Fragment>
            {boxData.targetId ? (
              <ClosedSettingEditForm />
            ) : (
              <ClosedSettingCreateForm />
            )}
          </Fragment>
        ) : null}
        {pnModelBoxData ? (
          <PNModelPopUp data={pnModelBoxData} />
        ) : null }
        {orderEditBoxData ? (
          <OrderEditPopUp data={orderEditBoxData} />
        ) : null}
        <PageSwitcher
          pages={['總表', '結單設定', '訂貨列表', '存貨管理', '進銷存紀錄']}
          currentPageIdx={currentPageIdx}
          onChange={i => this.setState({ currentPageIdx: i })} />
        {this.pageSwitcher()}
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    boxData: state.Invoicing.boxData,
    pnModelBoxData: state.PNModel.boxData,
    orderEditBoxData: state.Order.orderEditBoxData,
  }),
);

export default reduxHook(
  radium(InvoicingListBoard)
);
