// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';

import Table, { TableField } from '../../components/Table/Table.jsx';
import { FETCH_STOCK_LIST } from '../../queries/Inventory.js';

const styles = {
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 162px)',
    maxHeight: 630,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
};

type Props = {
  options: {
    memberId: number,
    keyword: string,
  },
};

const STOCK_LIST_LIMIT = 10;

class StockList extends PureComponent<Props> {
  render() {
    const {
      options,
    } = this.props;

    return (
      <Query
        query={FETCH_STOCK_LIST}
        variables={options}>
        {({ data, fetchMore }) => {
          if (!data) return null;

          const {
            stockList = [],
          } = data;

          return (
            <div style={styles.tableWrapper}>
              <Table
                dataSource={stockList.map(s => ({
                  id: s.id,
                  memberName: s.member.name,
                  pnTableName: s.pnTable.name,
                  code: s.pnTable.code,
                  storageCount: s.storageCount,
                }))}
                fetchMore={() => fetchMore({
                  variables: {
                    ...options,
                    offset: 0,
                    limit: stockList.length + STOCK_LIST_LIMIT,
                  },
                  updateQuery: (prev, { fetchMoreResult }) => {
                    if (!fetchMoreResult) return prev;
                    return {
                      ...prev,
                      stockList: [
                        ...fetchMoreResult.stockList,
                      ],
                    };
                  },
                })}
                showPlaceholder={!stockList.length}
                placeholder="查無資料">
                <TableField name="包膜師" fieldKey="memberName" flex={1} isCenter />
                <TableField name="品名" fieldKey="pnTableName" flex={1} isCenter />
                <TableField name="料號" fieldKey="code" flex={1} isCenter />
                <TableField name="庫存數量" fieldKey="storageCount" flex={1} isCenter />
              </Table>
            </div>
          );
        }}
      </Query>
    );
  }
}

const reduxHook = connect(
  state => ({
    options: state.Search.adminStockList,
  }),
);

export default reduxHook(
  radium(
    StockList
  )
);
