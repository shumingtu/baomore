// @flow
import React, {
  PureComponent,
  Fragment,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Query } from 'react-apollo';
// components
import ClosedOrderSettingActions from '../../components/Table/Actions/ClosedOrderSettingActions.jsx';
import Table, { TableField } from '../../components/Table/Table.jsx';
import Button from '../../components/Global/Button.jsx';
// config
import { FETCH_CLOSEDORDERSETTING_LIST } from '../../queries/ClosedOrderSetting.js';

import {
  toggleCreateEditBox,
} from '../../actions/Invoicing.js';

import {
  days,
} from '../../shared/Global.js';


const styles = {
  tableWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  createBtn: {
    backgroundColor: 'rgb(182, 184, 204)',
    border: '1px solid rgb(182, 184, 204)',
    color: 'white',
    margin: '20px 0 0 0',
  },
};

type Props = {
  toggleBox: Function,
};

class ClosedSettingBoard extends PureComponent<Props> {
  render() {
    const {
      toggleBox,
    } = this.props;

    return (
      <Query
        query={FETCH_CLOSEDORDERSETTING_LIST}>
        {({ data }) => {
          if (!data) return null;

          const {
            closedOrderSettings = [],
          } = data;

          return (
            <Fragment>
              <Button style={styles.createBtn} onClick={() => toggleBox({ status: true })} label="新增" />
              <div style={styles.tableWrapper}>
                <Table
                  dataSource={closedOrderSettings.map(c => ({
                    id: c.id,
                    closedDay: days.find(x => x.id === c.closedDay) ? days.find(x => x.id === c.closedDay).name : null,
                    closedTime: c.closedTime,
                    code: c.PNTable && c.PNTable.code ? c.PNTable.code : '統一設定',
                  }))}
                  showPlaceholder={!closedOrderSettings.length}
                  placeholder="查無資料"
                  actionTitles={['操作']}
                  getActions={() => [
                    <ClosedOrderSettingActions />,
                  ]}>
                  <TableField name="結單日" fieldKey="closedDay" flex={1} isCenter />
                  <TableField name="時間" fieldKey="closedTime" flex={1} isCenter />
                  <TableField name="料號" fieldKey="code" flex={1} isCenter />
                </Table>
              </div>
            </Fragment>
          );
        }}
      </Query>
    );
  }
}

const reduxHook = connect(
  () => ({
  }),
  dispatch => bindActionCreators({
    toggleBox: toggleCreateEditBox,
  }, dispatch)
);

export default reduxHook(
  radium(ClosedSettingBoard)
);
