// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';


import Table, { TableField } from '../../components/Table/Table.jsx';
import MemberManagementActions from '../../components/Table/Actions/MemberManagementActions.jsx';

import { GET_EMPLOYEE_LIST } from '../../queries/Member.js';

const styles = {
  tableWrapper: {
    width: '100%',
    height: '100%',
    maxHeight: 810,
    padding: '24px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
};

type Props = {
  history: Object,
};

const MEMBER_LIST_LIMIT = 10;

class MemberList extends PureComponent<Props> {
  render() {
    return (
      <Query
        variables={{
          employeeType: '一般登入用戶',
          limit: 10,
          offset: 0,
        }}
        query={GET_EMPLOYEE_LIST}>
        {({ data, fetchMore }) => {
          if (!data) return null;

          const {
            employeeMemberlist = [],
          } = data;

          return (
            <div style={styles.tableWrapper}>
              <Table
                dataSource={employeeMemberlist.map(c => ({
                  id: c.id,
                  lineId: c.lineId,
                  name: c.name,
                  birthday: c.birthday,
                  phone: c.phone,
                  email: c.email,
                  gender: c.gender,
                  memberWarranties: c.memberWarranties,
                  onlineOrders: c.onlineOrders,
                }))}
                fetchMore={() => fetchMore({
                  variables: {
                    offset: 0,
                    limit: employeeMemberlist.length + MEMBER_LIST_LIMIT,
                    employeeType: '一般登入用戶',
                  },
                  updateQuery: (prev, { fetchMoreResult }) => {
                    if (!fetchMoreResult) return prev;

                    return {
                      ...prev,
                      employeeMemberlist: [
                        ...fetchMoreResult.employeeMemberlist,
                      ],
                    };
                  },
                })}
                showPlaceholder={!employeeMemberlist.length}
                placeholder="查無資料"
                actionTitles={['操作']}
                getActions={() => [<MemberManagementActions {...this.props} />]}>
                <TableField name="LINEID" fieldKey="lineId" flex={2} isCenter />
                <TableField name="姓名" fieldKey="name" flex={1} isCenter />
                <TableField name="生日" fieldKey="birthday" flex={1} isCenter />
                <TableField name="行動電話" fieldKey="phone" flex={1} isCenter />
                <TableField name="Email" fieldKey="email" flex={1} isCenter />
                <TableField name="性別" fieldKey="gender" flex={1} isCenter />
              </Table>
            </div>
          );
        }}
      </Query>
    );
  }
}

export default radium(MemberList);
