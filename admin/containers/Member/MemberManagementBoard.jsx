// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';

import MemberList from './MemberList.jsx';
import MemberWarranties from '../../components/Member/MemberWarranties.jsx';
import MemberOrders from '../../components/Member/MemberOrders.jsx';


const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    flexDirection: 'column',
    position: 'relative',
  },
};

type Props = {
  warranties: Array,
  orders: Array,
};

class MemberManagementBoard extends PureComponent<Props> {
  render() {
    const {
      warranties,
      orders,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        {warranties ? (
          <MemberWarranties {...this.props} />
        ) : null}
        {orders ? (
          <MemberOrders {...this.props} />
        ) : null}
        <MemberList {...this.props} />
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    warranties: state.Member.warranties,
    orders: state.Member.orders,
  }),
);


export default reduxHook(
  radium(MemberManagementBoard)
);
