// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  initialize,
  SubmissionError,
} from 'redux-form';

import {
  FORM_CONSUMER_EDIT_FORM,
} from '../../shared/form.js';
import {
  validateConsumberEditForm,
} from '../../helper/validator/ConsumerEditFormValidator.js';

import FormTitle from '../../components/Global/FormTitle.jsx';
import Input from '../../components/Form/Input.jsx';
import SubmitButton from '../../components/Form/SubmitButton.jsx';
import Button from '../../components/Global/Button.jsx';
import Selector from '../../components/Form/Selector.jsx';

const styles = {
  wrapper: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 12,
  },
  formWrapper: {
    width: 650,
    height: 'auto',
    padding: '16px 32px',
    backgroundColor: '#fff',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    margin: 5,
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  buttonWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 6px',
  },
};

type Props = {
  memberData: Object,
  handleSubmit: Function,
  initializeForm: Function,
  mutate: Function,
  history: Object,
};

const genders = [{
  id: 'male',
  name: '男',
}, {
  id: 'female',
  name: '女',
}];

class ConsumerEditBaseForm extends PureComponent<Props> {
  componentDidMount() {
    const {
      memberData,
    } = this.props;

    if (memberData) {
      this.initializing(memberData);
    }
  }

  componentDidUpdate(prevProps) {
    const {
      memberData,
    } = this.props;

    if (memberData !== prevProps.memberData && memberData) {
      this.initializing(memberData);
    }
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    const initializePayload = {
      lineId: obj.lineId,
      name: obj.name,
      phone: obj.phone,
      birthday: obj.birthday,
      email: obj.email,
      gender: obj.gender,
    };

    initializeForm(initializePayload);
  }

  async submit(d) {
    const errors = validateConsumberEditForm(d);

    if (errors) {
      throw new SubmissionError(errors);
    }

    const {
      mutate,
      memberData,
      history,
    } = this.props;

    const payload = {
      ...d,
      id: memberData.id,
    };

    await mutate({
      variables: {
        ...payload,
      },
    });

    history.goBack();
  }

  render() {
    const {
      history,
      handleSubmit,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <FormTitle
          title="修改消費者資料" />
        <form
          onSubmit={handleSubmit(d => this.submit(d))}
          style={styles.formWrapper}>
          <div style={styles.fieldWrapper}>
            <Field
              name="lineId"
              label="LINEID："
              placeholder="請輸入LINEID"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              name="name"
              label="姓名："
              placeholder="請輸入姓名"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              name="birthday"
              label="生日："
              type="date"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              name="phone"
              label="行動電話："
              placeholder="請輸入行動電話"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              name="email"
              label="信箱："
              placeholder="請輸入信箱"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              placeholder="請選擇"
              nullable
              component={Selector}
              options={genders}
              label="性別："
              name="gender" />
          </div>
          <div style={styles.functionWrapper}>
            <div style={styles.buttonWrapper}>
              <SubmitButton label="送出" />
            </div>
            <div style={styles.buttonWrapper}>
              <Button
                label="返回"
                onClick={() => history.goBack()} />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_CONSUMER_EDIT_FORM,
});


const reduxHook = connect(
  () => ({}),
  dispatch => bindActionCreators({
    initializeForm: v => initialize(FORM_CONSUMER_EDIT_FORM, v),
  }, dispatch)
);


export default reduxHook(
  formHook(
    radium(
      ConsumerEditBaseForm
    )
  )
);
