// @flow

import React, { PureComponent } from 'react';
import {
  Query,
  Mutation,
} from 'react-apollo';

import {
  EDIT_CONSUMER,
} from '../../mutations/Member.js';
import {
  GET_EMPLOYEE_LIST,
} from '../../queries/Member.js';
import ConsumerEditBaseForm from './ConsumerEditBaseForm.jsx';

const styles = {
  wrapper: {
    width: '100%',
  },
};

type Props = {
  match: Object,
};

class ConsumerEditForm extends PureComponent<Props> {
  render() {
    const {
      match: {
        params: {
          memberId,
        },
      },
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <Query
          query={GET_EMPLOYEE_LIST}
          variables={{
            memberId: parseInt(memberId, 10),
            employeeType: '一般登入用戶',
            limit: 10,
            offset: 0,
          }}>
          {({
            data,
          }) => {
            const memberDatas = (data && data.employeeMemberlist) || [];

            return (
              <Mutation mutation={EDIT_CONSUMER}>
                {editMemberForConsumer => (
                  <ConsumerEditBaseForm
                    {...this.props}
                    mutate={editMemberForConsumer}
                    memberData={memberDatas[0] || null} />
                )}
              </Mutation>
            );
          }}
        </Query>
      </div>
    );
  }
}

export default ConsumerEditForm;
