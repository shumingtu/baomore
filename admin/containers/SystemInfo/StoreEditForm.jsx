// @flow

import React, { PureComponent } from 'react';
import {
  Query,
  Mutation,
} from 'react-apollo';

import {
  EDIT_STORE,
} from '../../mutations/Store.js';
import {
  FETCH_STORES_LIST,
} from '../../queries/Store.js';
import StoreBaseForm from './StoreBaseForm.jsx';

const styles = {
  wrapper: {
    width: '100%',
  },
};

type Props = {
  match: Object,
};

class StoreEditForm extends PureComponent<Props> {
  render() {
    const {
      match: {
        params: {
          storeId,
        },
      },
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <Query
          query={FETCH_STORES_LIST}
          variables={{
            storeId: parseInt(storeId, 10),
          }}>
          {({
            data,
          }) => {
            const storeDatas = (data && data.stores) || [];

            return (
              <Mutation mutation={EDIT_STORE}>
                {editStore => (
                  <StoreBaseForm
                    {...this.props}
                    mutate={editStore}
                    storeData={storeDatas[0] || null} />
                )}
              </Mutation>
            );
          }}
        </Query>
      </div>
    );
  }
}

export default StoreEditForm;
