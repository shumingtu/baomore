// @flow
import React, {
  PureComponent,
  Fragment,
} from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';

import Table, { TableField } from '../../components/Table/Table.jsx';
import SystemInfoVendorActions from '../../components/Table/Actions/SystemInfoVendorActions.jsx';
import Button from '../../components/Global/Button.jsx';

import { FETCH_VENDOR_LIST } from '../../queries/Global.js';

const styles = {
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 87px)',
    maxHeight: 810,
    padding: '24px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  buttonWrapper: {
    margin: 10,
  },
};

type Props = {
  history: Object,
};

const VENDOR_LIST_LIMIT = 10;

class SystemInfoVendor extends PureComponent<Props> {
  render() {
    const {
      history,
    } = this.props;

    return (
      <Query
        variables={{
          limit: 10,
          offset: 0,
        }}
        query={FETCH_VENDOR_LIST}>
        {({ data, fetchMore }) => {
          if (!data) return null;

          const {
            vendorList = [],
          } = data;

          return (
            <Fragment>
              <div style={styles.buttonWrapper}>
                <Button
                  label="新增"
                  onClick={() => history.push('/systemInfo/vendor/create')} />
              </div>
              <div style={styles.tableWrapper}>
                <Table
                  dataSource={vendorList.map(c => ({
                    id: c.id,
                    name: c.name,
                    contactPersonName: c.contactPersonName || '無',
                    phone: c.phone || '無',
                    address: c.address || '無',
                    vendorSettings: c.VendorSettings,
                  }))}
                  fetchMore={() => fetchMore({
                    variables: {
                      offset: 0,
                      limit: vendorList.length + VENDOR_LIST_LIMIT,
                    },
                    updateQuery: (prev, { fetchMoreResult }) => {
                      if (!fetchMoreResult) return prev;

                      return {
                        ...prev,
                        vendorList: [
                          ...fetchMoreResult.vendorList,
                        ],
                      };
                    },
                  })}
                  showPlaceholder={!vendorList.length}
                  placeholder="查無資料"
                  actionTitles={['操作']}
                  getActions={() => [<SystemInfoVendorActions {...this.props} />]}>
                  <TableField name="廠商名稱" fieldKey="name" flex={1} isCenter />
                  <TableField name="聯絡人姓名" fieldKey="contactPersonName" flex={1} isCenter />
                  <TableField name="電話" fieldKey="phone" flex={1} isCenter />
                  <TableField name="地址" fieldKey="address" flex={1} isCenter />
                </Table>
              </div>
            </Fragment>
          );
        }}
      </Query>
    );
  }
}


export default radium(SystemInfoVendor);
