// @flow
import React, {
  Component,
} from 'react';
import radium from 'radium';
import {
  reduxForm,
  Field,
} from 'redux-form';

import {
  FORM_IMPORT_FORM,
} from '../../shared/form.js';
import ImportCSVButton from '../../components/Form/ImportCSVButton.jsx';

const styles = {
  tableWrapper: {
    width: '100%',
    padding: '24px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  fieldWrap: {
    margin: '10px 0',
  },
};


class SystemInfoImportBoard extends Component {
  render() {
    return (
      <div style={styles.tableWrapper}>
        <div style={styles.fieldWrap}>
          <Field
            importType="director"
            label="匯入主任資訊"
            name="directorCsv"
            component={ImportCSVButton} />
        </div>
        <div style={styles.fieldWrap}>
          <Field
            importType="employee"
            label="匯入包膜師資訊"
            name="employeeCsv"
            component={ImportCSVButton} />
        </div>
        <div style={styles.fieldWrap}>
          <Field
            importType="channel"
            label="匯入通路資訊"
            name="channelCsv"
            component={ImportCSVButton} />
        </div>
        <div style={styles.fieldWrap}>
          <Field
            importType="store"
            label="匯入門市資訊"
            name="storeCsv"
            component={ImportCSVButton} />
        </div>
        <div style={styles.fieldWrap}>
          <Field
            importType="schedule"
            label="匯入包膜師排班"
            name="scheduleCsv"
            component={ImportCSVButton} />
        </div>
        <div style={styles.fieldWrap}>
          <Field
            importType="warranty"
            label="匯入會員保卡"
            name="warrantyCsv"
            component={ImportCSVButton} />
        </div>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_IMPORT_FORM,
});

export default formHook(
  radium(
    SystemInfoImportBoard
  )
);
