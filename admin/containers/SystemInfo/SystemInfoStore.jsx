// @flow
import React, {
  PureComponent,
  Fragment,
} from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';
import { connect } from 'react-redux';

import Table, { TableField } from '../../components/Table/Table.jsx';
import SystemInfoStoreActions from '../../components/Table/Actions/SystemInfoStoreActions.jsx';
import Button from '../../components/Global/Button.jsx';
import StoreSearcher from '../../components/SystemInfo/StoreSearcher.jsx';

import { FETCH_STORES_LIST } from '../../queries/Store.js';

const styles = {
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 117px)',
    maxHeight: 810,
    padding: '24px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  searchWrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
    height: 'auto',
  },
  buttonWrapper: {
    margin: 10,
  },
};

type Props = {
  history: Object,
  options: Object,
};

const STORE_LIST_LIMIT = 10;


class SystemInfoStore extends PureComponent<Props> {
  render() {
    const {
      history,
      options,
    } = this.props;

    return (
      <>
        <Query
          variables={{
            limit: 10,
            offset: 0,
            keyword: (options && options.keyword) || null,
          }}
          query={FETCH_STORES_LIST}>
          {({ data, fetchMore }) => {
            if (!data) return null;

            const {
              stores = [],
            } = data;

            return (
              <Fragment>
                <div style={styles.searchWrapper}>
                  <StoreSearcher />
                  <div style={styles.buttonWrapper}>
                    <Button
                      label="新增"
                      onClick={() => history.push('/systemInfo/store/create')} />
                  </div>
                </div>
                <div style={styles.tableWrapper}>
                  <Table
                    dataSource={stores.map((c) => {
                      const mainEmployee = c.members && c.members.find(x => x.memberStore.type === 'MAIN');
                      const backupEmployee = c.members && c.members.find(x => x.memberStore.type === 'BACKUP');

                      return {
                        id: c.id,
                        area: c.area && c.area.name,
                        channel: c.channel && c.channel.name,
                        name: c.name,
                        phone: c.phone,
                        address: c.address,
                        mainSN: mainEmployee ? mainEmployee.serialNumber : '無',
                        backupSN: backupEmployee ? backupEmployee.serialNumber : '無',
                        manager: c.manager || '無',
                        archive: c.archive,
                      };
                    })}
                    fetchMore={() => fetchMore({
                      variables: {
                        offset: 0,
                        limit: stores.length + STORE_LIST_LIMIT,
                      },
                      updateQuery: (prev, { fetchMoreResult }) => {
                        if (!fetchMoreResult) return prev;

                        return {
                          ...prev,
                          stores: [
                            ...fetchMoreResult.stores,
                          ],
                        };
                      },
                    })}
                    showPlaceholder={!stores.length}
                    placeholder="查無資料"
                    actionTitles={['操作']}
                    getActions={() => [<SystemInfoStoreActions {...this.props} />]}>
                    <TableField name="區域" fieldKey="area" flex={1} isCenter />
                    <TableField name="通路" fieldKey="channel" flex={1} isCenter />
                    <TableField name="店名" fieldKey="name" flex={1} isCenter />
                    <TableField name="電話" fieldKey="phone" flex={1} isCenter />
                    <TableField name="地址" fieldKey="address" flex={1} isCenter />
                    <TableField name="主包膜師工號" fieldKey="mainSN" flex={1} isCenter />
                    <TableField name="次包膜師工號" fieldKey="backupSN" flex={1} isCenter />
                    <TableField name="職務代理人" fieldKey="manager" flex={1} isCenter />
                  </Table>
                </div>
              </Fragment>
            );
          }}
        </Query>
      </>
    );
  }
}

const reduxHook = connect(
  state => ({
    options: state.Search.adminStoreList,
  }),
);


export default reduxHook(radium(SystemInfoStore));
