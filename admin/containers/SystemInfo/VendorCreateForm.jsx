// @flow

import React, { PureComponent } from 'react';
import { Mutation } from 'react-apollo';

import {
  CREATE_VENDOR,
} from '../../mutations/Vendor.js';
import {
  FETCH_VENDOR_LIST,
} from '../../queries/Global.js';
import VendorBaseForm from './VendorBaseForm.jsx';

const styles = {
  wrapper: {
    width: '100%',
  },
};

type Props = {
};

class VendorCreateForm extends PureComponent<Props> {
  render() {
    return (
      <div style={styles.wrapper}>
        <Mutation
          mutation={CREATE_VENDOR}
          update={(store, { data: { createVendor } }) => {
            const data = store.readQuery({
              query: FETCH_VENDOR_LIST,
              variables: {
                limit: 10,
                offset: 0,
              },
            });

            const {
              vendorList,
            } = data;

            if (createVendor) {
              store.writeQuery({
                query: FETCH_VENDOR_LIST,
                variables: {
                  limit: 10,
                  offset: 0,
                },
                data: {
                  vendorList: [
                    ...vendorList,
                    {
                      ...createVendor,
                    },
                  ],
                },
              });
            }
          }}>
          {createVendor => (
            <VendorBaseForm
              {...this.props}
              mutate={createVendor} />
          )}
        </Mutation>
      </div>
    );
  }
}

export default VendorCreateForm;
