// @flow

import React, { PureComponent } from 'react';
import { Mutation } from 'react-apollo';

import {
  CREATE_STORE,
} from '../../mutations/Store.js';
import {
  FETCH_STORES_LIST,
} from '../../queries/Store.js';
import StoreBaseForm from './StoreBaseForm.jsx';

const styles = {
  wrapper: {
    width: '100%',
  },
};

type Props = {
};

class StoreCreateForm extends PureComponent<Props> {
  render() {
    return (
      <div style={styles.wrapper}>
        <Mutation
          mutation={CREATE_STORE}
          update={(store, { data: { createStore } }) => {
            const data = store.readQuery({
              query: FETCH_STORES_LIST,
              variables: {
                limit: 10,
                offset: 0,
                keyword: null,
              },
            });

            const {
              stores,
            } = data;

            if (createStore) {
              store.writeQuery({
                query: FETCH_STORES_LIST,
                variables: {
                  limit: 10,
                  offset: 0,
                },
                data: {
                  stores: [
                    ...stores,
                    {
                      ...createStore,
                    },
                  ],
                },
              });
            }
          }}>
          {createStore => (
            <StoreBaseForm
              {...this.props}
              mutate={createStore} />
          )}
        </Mutation>
      </div>
    );
  }
}

export default StoreCreateForm;
