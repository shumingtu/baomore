// @flow
import React, {
  PureComponent,
  Fragment,
} from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';

import Table, { TableField } from '../../components/Table/Table.jsx';
import SystemInfoMemberActions from '../../components/Table/Actions/SystemInfoMemberActions.jsx';

import { GET_EMPLOYEE_LIST } from '../../queries/Member.js';

const styles = {
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 35px)',
    maxHeight: 810,
    padding: '24px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  buttonWrapper: {
    margin: 10,
  },
};

type Props = {
  type: String,
  match: Object,
  history: Object,
};

const EMPLOYEE_MEMBER_LIST_LIMIT = 10;


class SystemInfoMember extends PureComponent<Props> {
  render() {
    const {
      type,
    } = this.props;

    return (
      <Query
        variables={{
          employeeType: type,
          limit: EMPLOYEE_MEMBER_LIST_LIMIT,
          offset: 0,
        }}
        query={GET_EMPLOYEE_LIST}>
        {({ data, fetchMore }) => {
          if (!data) return null;

          const {
            employeeMemberlist = [],
          } = data;

          return (
            <Fragment>
              <div style={styles.tableWrapper}>
                <Table
                  dataSource={employeeMemberlist.map(c => ({
                    id: c.id,
                    area: c.area && c.area.name,
                    serialNumber: c.serialNumber,
                    name: c.name,
                    phone: c.phone,
                    directorSN: type === '包膜師' ? c.director && c.director.serialNumber : '無',
                    archive: c.archive,
                  }))}
                  fetchMore={() => fetchMore({
                    variables: {
                      offset: 0,
                      limit: employeeMemberlist.length + EMPLOYEE_MEMBER_LIST_LIMIT,
                    },
                    updateQuery: (prev, { fetchMoreResult }) => {
                      if (!fetchMoreResult) return prev;

                      return {
                        ...prev,
                        employeeMemberlist: [
                          ...fetchMoreResult.employeeMemberlist,
                        ],
                      };
                    },
                  })}
                  showPlaceholder={!employeeMemberlist.length}
                  placeholder="查無資料"
                  actionTitles={['操作']}
                  getActions={() => [<SystemInfoMemberActions {...this.props} />]}>
                  <TableField name="區域" fieldKey="area" flex={1} isCenter />
                  <TableField name="工號" fieldKey="serialNumber" flex={1} isCenter />
                  <TableField name="姓名" fieldKey="name" flex={1} isCenter />
                  <TableField name="電話" fieldKey="phone" flex={1} isCenter />
                  <TableField name="主任工號" fieldKey="directorSN" flex={1} isCenter />
                </Table>
              </div>
            </Fragment>
          );
        }}
      </Query>
    );
  }
}


export default radium(SystemInfoMember);
