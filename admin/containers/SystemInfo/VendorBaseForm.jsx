// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  FieldArray,
  initialize,
  SubmissionError,
} from 'redux-form';

import {
  FORM_VENDOR_CREATE_EDIT_FORM,
} from '../../shared/form.js';
import {
  validateVendorForm,
} from '../../helper/validator/VendorFormValidator.js';

import FormTitle from '../../components/Global/FormTitle.jsx';
import Input from '../../components/Form/Input.jsx';
import SubmitButton from '../../components/Form/SubmitButton.jsx';
import Settings from '../../components/Form/Settings.jsx';
import Button from '../../components/Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 12,
  },
  formWrapper: {
    width: 650,
    height: 'auto',
    padding: '16px 32px',
    backgroundColor: '#fff',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    margin: 5,
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  buttonWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 6px',
  },
};

type Props = {
  vendorData: Object,
  handleSubmit: Function,
  initializeForm: Function,
  mutate: Function,
  history: Object,
};

class VendorBaseForm extends PureComponent<Props> {
  componentDidMount() {
    const {
      vendorData,
    } = this.props;

    if (vendorData) {
      this.initializing(vendorData);
    }
  }

  componentDidUpdate(prevProps) {
    const {
      vendorData,
    } = this.props;

    if (vendorData !== prevProps.vendorData && vendorData) {
      this.initializing(vendorData);
    }
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    const {
      VendorSettings,
    } = obj;

    const initialVendorSetting = VendorSettings.map(v => ({
      ...v,
      MemberId: v.MemberId || 'ALL',
    }));

    initializeForm({
      ...obj,
      settings: initialVendorSetting,
    });
  }

  async submit(d) {
    const errors = validateVendorForm(d);

    if (errors) throw new SubmissionError(errors);

    const {
      address,
      contactPersonName,
      name,
      phone,
      settings,
    } = d;

    const {
      mutate,
      history,
      vendorData,
    } = this.props;

    const payload = {
      address,
      contactPersonName,
      name,
      phone,
    };

    if (vendorData) payload.id = parseInt(vendorData.id, 10);

    if (settings && settings.length) {
      payload.settings = settings.map((s) => {
        const memberId = s.MemberId === 'ALL' ? null : parseInt(s.MemberId, 10);
        return {
          type: s.type,
          MemberId: memberId,
          value: parseInt(s.value, 10),
          message: s.message,
        };
      });
    }

    await mutate({
      variables: payload,
    });

    history.goBack();
  }

  render() {
    const {
      history,
      vendorData,
      handleSubmit,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <FormTitle
          title={vendorData ? '修改廠商' : '新增廠商'} />
        <form
          onSubmit={handleSubmit(d => this.submit(d))}
          style={styles.formWrapper}>
          <div style={styles.fieldWrapper}>
            <Field
              name="name"
              label="廠商名稱："
              placeholder="請輸入廠商名稱"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              name="contactPersonName"
              label="聯絡人姓名："
              placeholder="請輸入聯絡人姓名"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              name="phone"
              label="電話："
              placeholder="請輸入廠商電話"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              name="address"
              label="地址："
              placeholder="請輸入地址"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <FieldArray
              component={Settings}
              name="settings" />
          </div>
          <div style={styles.functionWrapper}>
            <div style={styles.buttonWrapper}>
              <SubmitButton label="送出" />
            </div>
            <div style={styles.buttonWrapper}>
              <Button
                label="返回"
                onClick={() => history.goBack()} />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_VENDOR_CREATE_EDIT_FORM,
});


const reduxHook = connect(
  () => ({}),
  dispatch => bindActionCreators({
    initializeForm: v => initialize(FORM_VENDOR_CREATE_EDIT_FORM, v),
  }, dispatch)
);


export default reduxHook(
  formHook(
    radium(
      VendorBaseForm
    )
  )
);
