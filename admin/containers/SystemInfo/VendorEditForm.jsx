// @flow

import React, { PureComponent } from 'react';
import {
  Query,
  Mutation,
} from 'react-apollo';

import {
  EDIT_VENDOR,
} from '../../mutations/Vendor.js';
import {
  FETCH_VENDOR_LIST,
} from '../../queries/Global.js';
import VendorBaseForm from './VendorBaseForm.jsx';

const styles = {
  wrapper: {
    width: '100%',
  },
};

type Props = {
  match: Object,
};

class StoreEditForm extends PureComponent<Props> {
  render() {
    const {
      match: {
        params: {
          vendorId,
        },
      },
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <Query
          query={FETCH_VENDOR_LIST}
          variables={{
            id: parseInt(vendorId, 10),
          }}>
          {({
            data,
          }) => {
            const vendorDatas = (data && data.vendorList) || [];

            return (
              <Mutation mutation={EDIT_VENDOR}>
                {editVendor => (
                  <VendorBaseForm
                    {...this.props}
                    mutate={editVendor}
                    vendorData={vendorDatas[0] || null} />
                )}
              </Mutation>
            );
          }}
        </Query>
      </div>
    );
  }
}

export default StoreEditForm;
