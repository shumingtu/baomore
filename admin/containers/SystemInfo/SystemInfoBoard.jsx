// @flow
import React, { Component } from 'react';
import radium from 'radium';
import {
  Switch,
  Route,
} from 'react-router-dom';

import PageLinkSwitcher from '../../components/Global/PageLinkSwitcher.jsx';
import SystemInfoVendor from './SystemInfoVendor.jsx';
import SystemInfoStore from './SystemInfoStore.jsx';
import SystemInfoImportBoard from './SystemInfoImportBoard.jsx';
import StoreCreateForm from './StoreCreateForm.jsx';
import StoreEditForm from './StoreEditForm.jsx';
import VendorCreateForm from './VendorCreateForm.jsx';
import VendorEditForm from './VendorEditForm.jsx';

import SystemMemberRoute from '../../routes/SystemMember.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    position: 'relative',
    overflow: 'auto',
  },
};

type Props = {
  location: {
    pathname: String,
  },
  history: Object,
};

const MY_PAGES = [{
  name: '主任資訊',
  path: '/systemInfo/director',
}, {
  name: '包膜師資訊',
  path: '/systemInfo/employee',
}, {
  name: '廠商資訊',
  path: '/systemInfo/vendor',
}, {
  name: '門市資訊',
  path: '/systemInfo/store',
}, {
  name: '匯入資訊',
  path: '/systemInfo/import',
}];

class SystemInfoBoard extends Component<Props> {
  componentDidMount() {
    const {
      location: {
        pathname,
      },
      history,
    } = this.props;

    if (pathname === '/systemInfo') {
      history.replace('/systemInfo/director');
    }
  }

  componentDidUpdate(prevProps) {
    const {
      location: {
        pathname,
      },
      history,
    } = this.props;

    if (pathname !== prevProps.pathname && pathname === '/systemInfo') {
      history.replace('/systemInfo/director');
    }
  }

  render() {
    return (
      <div style={styles.wrapper}>
        <PageLinkSwitcher pages={MY_PAGES} />
        <Switch>
          <Route path="/systemInfo/director" component={SystemMemberRoute} />
          <Route path="/systemInfo/employee" component={SystemMemberRoute} />
          <Route exact path="/systemInfo/vendor" component={SystemInfoVendor} />
          <Route exact path="/systemInfo/store" component={SystemInfoStore} />
          <Route exact path="/systemInfo/import" component={SystemInfoImportBoard} />
          <Route path="/systemInfo/store/create" component={StoreCreateForm} />
          <Route path="/systemInfo/vendor/create" component={VendorCreateForm} />
          <Route path="/systemInfo/store/edit/:storeId" component={StoreEditForm} />
          <Route path="/systemInfo/vendor/edit/:vendorId" component={VendorEditForm} />
        </Switch>
      </div>
    );
  }
}

export default radium(SystemInfoBoard);
