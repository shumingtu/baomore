// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  Fields,
  initialize,
  SubmissionError,
  change,
  formValueSelector,
} from 'redux-form';
import {
  Query,
  ApolloConsumer,
} from 'react-apollo';

import {
  FORM_STORE_CREATE_EDIT_FORM,
} from '../../shared/form.js';
import {
  validateStoreForm,
} from '../../helper/validator/StoreFormValidator.js';

import {
  FETCH_AREA_LIST,
} from '../../queries/Area.js';
import {
  FETCH_CHANNEL_LIST,
} from '../../queries/Channel.js';
import {
  GET_EMPLOYEE_LIST,
} from '../../queries/Member.js';
import {
  FETCH_LOCATION_INFO,
} from '../../queries/Store.js';

import FormTitle from '../../components/Global/FormTitle.jsx';
import Input from '../../components/Form/Input.jsx';
import SubmitButton from '../../components/Form/SubmitButton.jsx';
import Button from '../../components/Global/Button.jsx';
import Selector from '../../components/Form/Selector.jsx';
import CityDistricts from '../../components/Form/CityDistricts.jsx';

const styles = {
  wrapper: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 12,
  },
  formWrapper: {
    width: 650,
    height: 'auto',
    padding: '16px 32px',
    backgroundColor: '#fff',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    // flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    margin: 5,
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  buttonWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 6px',
  },
};

type Props = {
  storeData: Object,
  handleSubmit: Function,
  initializeForm: Function,
  mutate: Function,
  history: Object,
  setLat: Function,
  setLon: Function,
  address: String,
};

const selector = formValueSelector(FORM_STORE_CREATE_EDIT_FORM);

class StoreBaseForm extends PureComponent<Props> {
  state = {
    locationLoading: false,
  };

  componentDidMount() {
    const {
      storeData,
    } = this.props;

    if (storeData) {
      this.initializing(storeData);
    }
  }

  componentDidUpdate(prevProps) {
    const {
      storeData,
    } = this.props;

    if (storeData !== prevProps.storeData && storeData) {
      this.initializing(storeData);
    }
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    const mainEmployee = obj.members && obj.members.find(x => x.memberStore.type === 'MAIN');
    const backupEmployee = obj.members && obj.members.find(x => x.memberStore.type === 'BACKUP');

    initializeForm({
      ...obj,
      areaId: obj.area && obj.area.id,
      channelId: obj.channel && obj.channel.id,
      cityId: obj.district && obj.district.city && obj.district.city.id,
      districtId: obj.district && obj.district.id,
      mainEmployeeId: mainEmployee ? mainEmployee.memberId : '-1',
      backupEmployeeId: backupEmployee ? backupEmployee.memberId : '-1',
    });
  }

  async submit(d) {
    const errors = validateStoreForm(d);
    if (errors) throw new SubmissionError(errors);

    const {
      storeData,
      mutate,
      history,
    } = this.props;

    const {
      areaId,
      channelId,
      districtId,
      manager,
      mainEmployeeId,
      backupEmployeeId,
      name,
      phone,
      address,
      latitude,
      longitude,
    } = d;

    const payload = {
      name,
      phone,
      address,
      manager,
      areaId: parseInt(areaId, 10),
      channelId: parseInt(channelId, 10),
      districtId: parseInt(districtId, 10),
      latitude,
      longitude,
    };

    if (mainEmployeeId && parseInt(mainEmployeeId, 10) !== -1) {
      payload.mainEmployeeId = parseInt(mainEmployeeId, 10);
    }

    if (backupEmployeeId && parseInt(backupEmployeeId, 10) !== -1) {
      payload.backupEmployeeId = parseInt(backupEmployeeId, 10);
    }

    if (storeData) payload.id = parseInt(storeData.id, 10);

    await mutate({
      variables: payload,
    }).catch((e) => {
      window.alert(e);
    });

    history.goBack();
  }

  render() {
    const {
      history,
      storeData,
      handleSubmit,
      setLat,
      setLon,
      address,
    } = this.props;

    const {
      locationLoading,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        <FormTitle
          title={storeData ? '修改門市' : '新增門市'} />
        <form
          onSubmit={handleSubmit(d => this.submit(d))}
          style={styles.formWrapper}>
          <Query query={FETCH_AREA_LIST}>
            {({ data }) => {
              if (!data) return null;

              const {
                areaList,
              } = data;

              return (
                <div style={styles.fieldWrapper}>
                  <Field
                    placeholder="請選擇"
                    nullable
                    component={Selector}
                    options={areaList}
                    label="區域:"
                    name="areaId" />
                </div>
              );
            }}
          </Query>
          <Query query={FETCH_CHANNEL_LIST}>
            {({ data }) => {
              if (!data) return null;

              const {
                channelList,
              } = data;

              return (
                <div style={styles.fieldWrapper}>
                  <Field
                    placeholder="請選擇"
                    nullable
                    component={Selector}
                    options={channelList}
                    label="通路:"
                    name="channelId" />
                </div>
              );
            }}
          </Query>
          <div style={styles.fieldWrapper}>
            <Fields
              names={[
                'cityId',
                'districtId',
              ]}
              component={CityDistricts} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              name="name"
              label="店名："
              placeholder="請輸入店名"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              name="phone"
              label="電話："
              placeholder="請輸入電話"
              component={Input} />
          </div>
          <ApolloConsumer>
            {client => (
              <div style={styles.fieldWrapper}>
                <Field
                  name="address"
                  label="地址："
                  placeholder="請輸入地址"
                  component={Input} />
                <div style={{ margin: '0px 5px' }}>
                  <Button
                    label="取得經緯度"
                    disabled={locationLoading}
                    onClick={async () => {
                      if (!address) {
                        alert('請輸入地址！');
                        return;
                      }
                      if (locationLoading) return;
                      try {
                        this.setState({ locationLoading: true });
                        const { data } = await client.query({
                          query: FETCH_LOCATION_INFO,
                          variables: {
                            address,
                          },
                        });

                        if (data && data.locationInfo) {
                          setLat(data.locationInfo.lat);
                          setLon(data.locationInfo.lon);
                          alert('成功');
                        }
                      } catch (e) {
                        alert('無法取得經緯度，請輸入正確地址或手動輸入！');
                      } finally {
                        this.setState({ locationLoading: false });
                      }
                    }} />
                </div>
              </div>
            )}
          </ApolloConsumer>
          <div style={styles.fieldWrapper}>
            <Field
              name="latitude"
              label="緯度："
              placeholder="請輸入緯度"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              name="longitude"
              label="經度："
              placeholder="請輸入經度"
              component={Input} />
          </div>
          <Query query={GET_EMPLOYEE_LIST}>
            {({ data }) => {
              if (!data) return null;

              const {
                employeeMemberlist,
              } = data;

              return (
                <div style={styles.fieldWrapper}>
                  <Field
                    placeholder="請選擇"
                    nullable
                    component={Selector}
                    options={employeeMemberlist}
                    label="包膜師:"
                    name="mainEmployeeId" />
                </div>
              );
            }}
          </Query>
          <Query query={GET_EMPLOYEE_LIST}>
            {({ data }) => {
              if (!data) return null;

              const {
                employeeMemberlist,
              } = data;

              return (
                <div style={styles.fieldWrapper}>
                  <Field
                    placeholder="請選擇"
                    nullable
                    component={Selector}
                    options={employeeMemberlist}
                    label="次包膜師:"
                    name="backupEmployeeId" />
                </div>
              );
            }}
          </Query>
          <div style={styles.fieldWrapper}>
            <Field
              name="manager"
              label="職務代理人："
              placeholder="請輸入職務代理人姓名"
              component={Input} />
          </div>
          <div style={styles.functionWrapper}>
            <div style={styles.buttonWrapper}>
              <SubmitButton label="送出" />
            </div>
            <div style={styles.buttonWrapper}>
              <Button
                label="返回"
                onClick={() => history.goBack()} />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_STORE_CREATE_EDIT_FORM,
});


const reduxHook = connect(
  state => ({
    address: selector(state, 'address'),
  }),
  dispatch => bindActionCreators({
    initializeForm: v => initialize(FORM_STORE_CREATE_EDIT_FORM, v),
    setLat: v => change(FORM_STORE_CREATE_EDIT_FORM, 'latitude', v),
    setLon: v => change(FORM_STORE_CREATE_EDIT_FORM, 'longitude', v),
  }, dispatch)
);


export default reduxHook(
  formHook(
    radium(
      StoreBaseForm
    )
  )
);
