// @flow
import React from 'react';
import radium from 'radium';
// config
import Theme from '../styles/Theme.js';
import { ADMIN_LINE_LOGIN_PATH } from '../shared/Global.js';
// static
import lineLoginIcon from '../static/images/line-login-button.png';

const styles = {
  wrapper: {
    width: '100vw',
    height: '100vh',
    padding: 24,
    backgroundColor: Theme.MAIN_THEME_COLOR,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 500,
    letterSpacing: 1,
    color: Theme.SECONDARY_THEME_COLOR,
    margin: '36px 0',
    padding: 0,
  },
  lineLink: {
    width: 'auto',
    height: 'auto',
    textDecoration: 'none',
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    cursor: 'pointer',
  },
  lineLogin: {
    width: 151,
    height: 44,
    ':hover': {
      opacity: 0.9,
    },
  },
};

type Props = {
  link?: string,
};

function LoginPage({
  link,
}: Props) {
  return (
    <div style={styles.wrapper}>
      <h1 style={styles.title}>
        Artmo 後台管理系統
      </h1>
      <a
        target="_self"
        href={link}
        style={styles.lineLink}>
        <img
          alt="Line 登入"
          src={lineLoginIcon}
          style={styles.lineLogin} />
      </a>
    </div>
  );
}

LoginPage.defaultProps = {
  link: ADMIN_LINE_LOGIN_PATH,
};

export default radium(LoginPage);
