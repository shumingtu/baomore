// @flow
import React, { Component } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  initialize,
  formValueSelector,
  change,
  SubmissionError,
} from 'redux-form';
import {
  graphql,
  Query,
} from 'react-apollo';

import TextStyles from '../../styles/Text.js';
import {
  FETCH_PNCATEGORIES,
  FETCH_BIGSUBCATEGORIES,
  FETCH_SMALLSUBCATEGORIES,
  FETCH_PN_LIST,
  FETCH_PN_ITEM,
} from '../../queries/PNTable.js';
import {
  FETCH_VENDOR_LIST,
} from '../../queries/Global.js';
import {
  FETCH_BRAND_LIST,
  FETCH_BRAND_MODEL_LIST,
} from '../../queries/Brand.js';
import {
  CREATE_PNTABLE_ITEM,
  EDIT_PNTABLE_ITEM,
} from '../../mutations/PNTable.js';
import { FORM_PNMODEL_CREATE_EDIT_FORM } from '../../shared/form.js';
import { PNTableDevice } from '../../shared/PNTable.js';
import { validatePNModelCreateEdit } from '../../helper/validator/PNModelCreateEdit.js';

// components
import Selector from '../../components/Form/Selector.jsx';
import SubmitButton from '../../components/Form/SubmitButton.jsx';
import Button from '../../components/Global/Button.jsx';
import FileInput from '../../components/Form/FileInput.jsx';
import Input from '../../components/Form/Input.jsx';
import CheckboxYesNoGroup from '../../components/Form/CheckboxYesNoGroup.jsx';
import CodeInput from '../../components/Form/CodeInput.jsx';

const selector = formValueSelector(FORM_PNMODEL_CREATE_EDIT_FORM);

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    overflow: 'auto',
  },
  title: {
    ...TextStyles.labelText,
    width: '100%',
    height: 'auto',
    textAlign: 'center',
    fontSize: 18,
    padding: '24px 0',
  },
  mainForm: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formWrapper: {
    width: 650,
    height: 'auto',
    padding: '16px 32px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: '#fff',
    border: '1px solid rgb(182, 184, 204)',
  },
  formGroup: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    // width: '100%',
    margin: 5,
  },
  upperGroupWrap: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    width: 'auto',
    flexDirection: 'column',
    margin: '0 15px',
  },
  upperItemWrap: {
    width: 'auto',
    height: 'auto',
    padding: 4,
  },
  functionWrapper: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    margin: '30px 0 5px 0',
  },
  editBtn: {
    width: 88,
    height: 32,
    backgroundColor: 'rgb(182, 184, 204)',
    margin: '0 3px',
  },
  cancelBtn: {
    width: 88,
    height: 32,
    margin: '0 3px',
    backgroundColor: 'white',
    border: '1px solid rgb(182, 184, 204)',
    color: 'rgb(182, 184, 204)',
  },
};

type Props = {
  match: Object,
  selectedPNCategoryId: Number,
  selectedBigSubCategoryId: Number,
  setBigSubCategorylId: Function,
  setSmallSubCategorylId: Function,
  selectedBrandId: Number,
  setModelId: Function,
  setDeviceId: Function,
  history: Object,
  handleSubmit: Function,
  createPNTableItem: Function,
  initializeForm: Function,
  pnTableItem: Object,
  editPNTableItem: Function,
  setBrandId: Function,
};

class PNModelCreateEditForm extends Component<Props> {
  componentDidMount() {
    const {
      initializeForm,
      pnTableItem,
    } = this.props;

    if (pnTableItem) {
      initializeForm({
        ...pnTableItem,
        pnCategoryId: (pnTableItem.pnCategory && `${pnTableItem.pnCategory.id}`) || '-1',
        bigSubCategoryId: (pnTableItem.bigSubCategory && pnTableItem.bigSubCategory.id) || '-1',
        smallSubCategoryId: (pnTableItem.smallSubCategory && pnTableItem.smallSubCategory.id) || '-1',
        device: pnTableItem.suitableDevice || null,
        brandId: (pnTableItem.Brand && pnTableItem.Brand.id) || '-1',
        modelId: (pnTableItem.BrandModel && pnTableItem.BrandModel.id) || '-1',
        VendorId: (pnTableItem.vendor && pnTableItem.vendor.id) || '-1',
      });
    }
  }

  componentDidUpdate(prevProps) {
    const {
      selectedPNCategoryId,
      selectedBigSubCategoryId,
      setBigSubCategorylId,
      setSmallSubCategorylId,
      selectedBrandId,
      setModelId,
      setDeviceId,
      initializeForm,
      pnTableItem,
      setBrandId,
    } = this.props;

    if (pnTableItem && prevProps.pnTableItem !== pnTableItem) {
      initializeForm({
        ...pnTableItem,
        pnCategoryId: (pnTableItem.pnCategory && `${pnTableItem.pnCategory.id}`) || '-1',
        bigSubCategoryId: (pnTableItem.bigSubCategory && pnTableItem.bigSubCategory.id) || '-1',
        smallSubCategoryId: (pnTableItem.smallSubCategory && pnTableItem.smallSubCategory.id) || '-1',
        device: pnTableItem.suitableDevice || null,
        brandId: (pnTableItem.Brand && pnTableItem.Brand.id) || '-1',
        modelId: (pnTableItem.BrandModel && pnTableItem.BrandModel.id) || '-1',
        VendorId: (pnTableItem.vendor && pnTableItem.vendor.id) || '-1',
      });
    }

    if (
      prevProps.selectedPNCategoryId !== selectedPNCategoryId
      && selectedPNCategoryId === '2'
    ) {
      setDeviceId('PHONE');
    }

    if (prevProps.selectedPNCategoryId && prevProps.selectedPNCategoryId !== selectedPNCategoryId) {
      setBigSubCategorylId('-1');

      if (prevProps.selectedPNCategoryId === '2') {
        setBrandId(null);
        setModelId(null);
      }
    }

    if (
      prevProps.selectedBigSubCategoryId
      && prevProps.selectedBigSubCategoryId !== selectedBigSubCategoryId
    ) {
      setSmallSubCategorylId('-1');
    }

    if (prevProps.selectedBrandId && prevProps.selectedBrandId !== selectedBrandId) {
      setModelId('-1');
    }
  }

  async submit(d) {
    const errors = validatePNModelCreateEdit(d);

    if (errors) {
      throw new SubmissionError(errors);
    }

    const {
      code,
      pnCategoryId,
      bigSubCategoryId,
      smallSubCategoryId,
      name,
      picture,
      price,
      device,
      modelId,
      VendorId,
      description,
      isOnSale,
      onlinePrice,
    } = d;

    const payload = {
      code,
      pnCategoryId: parseInt(pnCategoryId, 10),
      bigSubCategoryId: parseInt(bigSubCategoryId, 10),
      VendorId: parseInt(VendorId, 10),
      name,
      picture,
      price: parseInt(price, 10),
      device,
      description,
      isOnSale,
    };

    if (smallSubCategoryId) payload.smallSubCategoryId = parseInt(smallSubCategoryId, 10);
    if (modelId && modelId !== '-1') payload.BrandModelId = parseInt(modelId, 10);
    if (onlinePrice) payload.onlinePrice = parseInt(onlinePrice, 10);

    const {
      createPNTableItem,
      editPNTableItem,
      history,
      match: {
        params: {
          pnItemId,
        },
      },
    } = this.props;

    if (pnItemId) {
      payload.id = parseInt(pnItemId, 10);

      const {
        data: {
          editPNTableItem: {
            id,
          },
        },
      } = await editPNTableItem(payload);

      if (id) {
        return history.push('/pnModel');
      }
    }

    const {
      data: {
        createPNTableItem: {
          id,
        },
      },
    } = await createPNTableItem(payload);

    if (id) {
      history.push('/pnModel');
    }
  }

  render() {
    const {
      match: {
        params: {
          pnItemId,
        },
      },
      selectedPNCategoryId,
      selectedBigSubCategoryId,
      selectedBrandId,
      history,
      handleSubmit,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.title}>
          {pnItemId ? '修改料號' : '新增料號'}
        </div>
        <div style={styles.mainForm}>
          <form
            onSubmit={handleSubmit(d => this.submit(d))}
            style={styles.formWrapper}>
            <div style={styles.formGroup}>
              <Field name="picture" component={FileInput} />
              <div style={styles.upperGroupWrap}>
                <div style={styles.upperItemWrap}>
                  <Field
                    label="料號編號:"
                    name="code"
                    component={CodeInput} />
                </div>
                <div style={styles.upperItemWrap}>
                  <Query query={FETCH_PNCATEGORIES}>
                    {({ data }) => {
                      if (!data) return null;

                      const {
                        pnCategories = [],
                      } = data;

                      return (
                        <Field
                          placeholder="請選擇"
                          nullable
                          component={Selector}
                          options={pnCategories}
                          label="料號類別:"
                          name="pnCategoryId" />
                      );
                    }}
                  </Query>
                </div>
                <div style={styles.upperItemWrap}>
                  <Query
                    variables={{
                      id: (selectedPNCategoryId && parseInt(selectedPNCategoryId, 10)) || -1,
                    }}
                    query={FETCH_BIGSUBCATEGORIES}>
                    {({ data }) => {
                      if (!data) return null;

                      const {
                        subCategoriesByPNCategory = [],
                      } = data;

                      return (
                        <Field
                          placeholder="請選擇"
                          nullable
                          component={Selector}
                          options={subCategoriesByPNCategory}
                          label="大類:"
                          name="bigSubCategoryId" />
                      );
                    }}
                  </Query>
                </div>
                <div style={styles.upperItemWrap}>
                  <Query
                    variables={{
                      id: (selectedBigSubCategoryId
                        && parseInt(selectedBigSubCategoryId, 10)) || -1,
                    }}
                    query={FETCH_SMALLSUBCATEGORIES}>
                    {({ data }) => {
                      if (!data) return null;

                      const {
                        subCategoriesBySubCategory = [],
                      } = data;

                      return (
                        <Field
                          placeholder="請選擇"
                          nullable
                          component={Selector}
                          options={subCategoriesBySubCategory}
                          label="小類:"
                          name="smallSubCategoryId" />
                      );
                    }}
                  </Query>
                </div>
                <div style={styles.upperItemWrap}>
                  <Field
                    label="品名:"
                    name="name"
                    component={Input} />
                </div>
              </div>
            </div>
            <div style={styles.formGroup}>
              <Field
                label="價格:"
                name="price"
                component={Input} />
            </div>
            <div style={styles.formGroup}>
              <Field
                placeholder="請選擇"
                nullable
                disabled={selectedPNCategoryId === '2'}
                component={Selector}
                options={PNTableDevice}
                label="裝置類型:"
                name="device" />
            </div>
            {selectedPNCategoryId === '2' ? (
              <div style={styles.formGroup}>
                <Query query={FETCH_BRAND_LIST}>
                  {({ data }) => {
                    if (!data) return null;

                    const {
                      brandList = [],
                    } = data;

                    return (
                      <Field
                        placeholder="請選擇"
                        nullable
                        component={Selector}
                        options={brandList}
                        label="手機品牌:"
                        name="brandId" />
                    );
                  }}
                </Query>
              </div>
            ) : null}
            {selectedPNCategoryId === '2' ? (
              <div style={styles.formGroup}>
                <Query
                  variables={{ brandId: (selectedBrandId && parseInt(selectedBrandId, 10)) || -1 }}
                  query={FETCH_BRAND_MODEL_LIST}>
                  {({ data }) => {
                    if (!data) return null;

                    const {
                      brandModelList = [],
                    } = data;

                    return (
                      <Field
                        placeholder="請選擇"
                        nullable
                        component={Selector}
                        options={brandModelList}
                        label="手機型號:"
                        name="modelId" />
                    );
                  }}
                </Query>
              </div>
            ) : null}
            <div style={styles.formGroup}>
              <Query query={FETCH_VENDOR_LIST}>
                {({ data }) => {
                  if (!data) return null;

                  const {
                    vendorList = [],
                  } = data;

                  return (
                    <Field
                      placeholder="請選擇"
                      nullable
                      component={Selector}
                      options={vendorList}
                      label="廠商:"
                      name="VendorId" />
                  );
                }}
              </Query>
            </div>
            <div style={styles.formGroup}>
              <Field
                label="描述:"
                name="description"
                type="textarea"
                component={Input} />
            </div>
            <div style={styles.formGroup}>
              <Field
                label="是否顯示此料號在客製化網站:"
                name="isOnSale"
                component={CheckboxYesNoGroup} />
            </div>
            <div style={styles.formGroup}>
              <Field
                label="線上價格:"
                name="onlinePrice"
                component={Input} />
            </div>
            <div style={styles.functionWrapper}>
              <SubmitButton
                style={styles.editBtn}
                label={pnItemId ? '確定修改' : '確定新增'} />
              <Button onClick={() => history.goBack()} style={styles.cancelBtn} label="取消" />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_PNMODEL_CREATE_EDIT_FORM,
});

const reduxHook = connect(
  state => ({
    selectedPNCategoryId: selector(state, 'pnCategoryId'),
    selectedBigSubCategoryId: selector(state, 'bigSubCategoryId'),
    selectedBrandId: selector(state, 'brandId'),
    options: state.PNModel.listSearchOptions,
  }),
  dispatch => bindActionCreators({
    initializeForm: d => initialize(FORM_PNMODEL_CREATE_EDIT_FORM, d),
    setBigSubCategorylId: v => change(FORM_PNMODEL_CREATE_EDIT_FORM, 'bigSubCategoryId', v),
    setSmallSubCategorylId: v => change(FORM_PNMODEL_CREATE_EDIT_FORM, 'smallSubCategoryId', v),
    setBrandId: v => change(FORM_PNMODEL_CREATE_EDIT_FORM, 'brandId', v),
    setModelId: v => change(FORM_PNMODEL_CREATE_EDIT_FORM, 'modelId', v),
    setDeviceId: v => change(FORM_PNMODEL_CREATE_EDIT_FORM, 'device', v),
  }, dispatch)
);

const queryHook = graphql(FETCH_PN_ITEM, {
  options: ownProps => ({
    variables: {
      id: ownProps.match.params.pnItemId ? parseInt(ownProps.match.params.pnItemId, 10) : -1,
    },
  }),
  props: ({
    data: {
      pnTableItem,
    },
  }) => ({
    pnTableItem: pnTableItem || null,
  }),
});

const editMutationHook = graphql(EDIT_PNTABLE_ITEM, {
  props: ({ ownProps, mutate }: { mutate: Function }) => ({
    editPNTableItem: ({
      id,
      code,
      pnCategoryId,
      bigSubCategoryId,
      smallSubCategoryId,
      BrandModelId,
      VendorId,
      name,
      picture,
      price,
      device,
      description,
      isOnSale,
      onlinePrice,
    }) => mutate({
      variables: {
        id,
        code,
        pnCategoryId,
        bigSubCategoryId,
        smallSubCategoryId,
        BrandModelId,
        VendorId,
        name,
        picture,
        price,
        device,
        description,
        isOnSale,
        onlinePrice,
      },
      update: (store, { data: { editPNTableItem } }) => {
        const data = store.readQuery({
          query: FETCH_PN_LIST,
          variables: ownProps.options,
        });

        const {
          pntableList,
        } = data;

        if (editPNTableItem) {
          const targetIdx = pntableList.findIndex(x => x.id === editPNTableItem.id);

          if (~targetIdx) {
            store.writeQuery({
              query: FETCH_PN_LIST,
              variables: ownProps.options,
              data: {
                pntableList: [
                  ...pntableList.slice(0, targetIdx),
                  editPNTableItem,
                  ...pntableList.slice(targetIdx + 1),
                ],
              },
            });
          }
        }
      },
    }),
  }),
});

const createMutationHook = graphql(CREATE_PNTABLE_ITEM, {
  props: ({ ownProps, mutate }: { mutate: Function }) => ({
    createPNTableItem: ({
      code,
      pnCategoryId,
      bigSubCategoryId,
      smallSubCategoryId,
      BrandModelId,
      VendorId,
      name,
      picture,
      price,
      device,
      description,
      isOnSale,
      onlinePrice,
    }) => mutate({
      variables: {
        code,
        pnCategoryId,
        bigSubCategoryId,
        smallSubCategoryId,
        BrandModelId,
        VendorId,
        name,
        picture,
        price,
        device,
        description,
        isOnSale,
        onlinePrice,
      },
      update: (store, { data: { createPNTableItem } }) => {
        const data = store.readQuery({
          query: FETCH_PN_LIST,
          variables: ownProps.options,
        });

        const {
          pntableList,
        } = data;

        if (createPNTableItem) {
          store.writeQuery({
            query: FETCH_PN_LIST,
            variables: ownProps.options,
            data: {
              pntableList: [
                ...pntableList,
                {
                  ...createPNTableItem,
                },
              ],
            },
          });
        }
      },
    }),
  }),
});

export const PNModelEditForm = reduxHook(
  editMutationHook(
    queryHook(
      formHook(
        radium(
          PNModelCreateEditForm
        )
      )
    )
  )
);

export const PNModelCreateForm = reduxHook(
  createMutationHook(
    formHook(
      radium(
        PNModelCreateEditForm
      )
    )
  )
);

export default reduxHook(
  formHook(
    radium(
      PNModelCreateEditForm
    )
  )
);
