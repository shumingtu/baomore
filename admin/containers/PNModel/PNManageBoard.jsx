// @flow
import React, {
  PureComponent,
  Fragment,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
// component
import {
  PNSubCategoryCreateForm,
  PNSubCategoryEditForm,
} from '../../components/PNModel/PNSubCategoryPopUp.jsx';
import PNCategoryListManage from '../../components/PNModel/PNCategoryListManage.jsx';
import BigSubCategoryListManage from '../../components/PNModel/BigSubCategoryListManage.jsx';
import SmallSubCategoryListManage from '../../components/PNModel/SmallSubCategoryListManage.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

type Props = {
  subCategoryCreateEditBoxOptions: Object,
};

class PNManageBoard extends PureComponent<Props> {
  render() {
    const {
      subCategoryCreateEditBoxOptions,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        {subCategoryCreateEditBoxOptions.status ? (
          <Fragment>
            {subCategoryCreateEditBoxOptions.targetId ? (
              <PNSubCategoryEditForm />
            ) : (
              <PNSubCategoryCreateForm />
            )}
          </Fragment>
        ) : null}
        <PNCategoryListManage />
        <BigSubCategoryListManage />
        <SmallSubCategoryListManage />
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    subCategoryCreateEditBoxOptions: state.PNModel.subCategoryCreateEditBoxOptions,
  }),
);

export default reduxHook(
  radium(
    PNManageBoard
  )
);
