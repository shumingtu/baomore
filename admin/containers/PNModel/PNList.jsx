// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';
// components
import PNModelActions from '../../components/Table/Actions/PNModelActions.jsx';
import Table, { TableField } from '../../components/Table/Table.jsx';
// config
import { FETCH_PN_LIST } from '../../queries/PNTable.js';

const styles = {
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 206px)',
    maxHeight: 630,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
};

type Props = {
  options: {
    brandId: string,
    modelId: string,
    colorId: string,
  },
};

const PN_LIST_LIMIT = 10;

class PNList extends PureComponent<Props> {
  render() {
    const {
      options,
    } = this.props;

    return (
      <Query
        query={FETCH_PN_LIST}
        variables={options}>
        {({ data, fetchMore }) => {
          if (!data) return null;

          const {
            pntableList = [],
          } = data;

          return (
            <div style={styles.tableWrapper}>
              <Table
                dataSource={pntableList.map(c => ({
                  id: c.id,
                  code: c.code,
                  pnCategory: (c.pnCategory && c.pnCategory.name) || null,
                  bigSubCategory: (c.bigSubCategory && c.bigSubCategory.name) || null,
                  smallSubCategory: (c.smallSubCategory && c.smallSubCategory.name) || null,
                  name: c.name,
                  price: c.price,
                  onlinePrice: c.onlinePrice || '無線上價格',
                  vendor: (c.vendor && c.vendor.name) || null,
                  suitableDevice: c.suitableDevice,
                  description: c.description,
                  picture: c.picture,
                  brand: (c.Brand && c.Brand.name) || null,
                  brandModel: (c.BrandModel && c.BrandModel.name) || null,
                }))}
                fetchMore={() => fetchMore({
                  variables: {
                    ...options,
                    offset: 0,
                    limit: pntableList.length + PN_LIST_LIMIT,
                  },
                  updateQuery: (prev, { fetchMoreResult }) => {
                    if (!fetchMoreResult) return prev;
                    return {
                      ...prev,
                      pntableList: [
                        ...fetchMoreResult.pntableList,
                      ],
                    };
                  },
                })}
                showPlaceholder={!pntableList.length}
                placeholder="查無資料"
                actionTitles={['操作']}
                getActions={() => [
                  <PNModelActions
                    id={-1}
                    name=""
                    assignModelId={-1} />,
                ]}>
                <TableField name="料號編號" fieldKey="code" flex={1} isCenter />
                <TableField name="料號類型" fieldKey="pnCategory" flex={1} isCenter />
                <TableField
                  name={options.pnCategoryId === 2 ? '品牌' : '大類'}
                  fieldKey={options.pnCategoryId === 2 ? 'brand' : 'bigSubCategory'}
                  flex={1}
                  isCenter />
                <TableField
                  name={options.pnCategoryId === 2 ? '型號' : '小類'}
                  fieldKey={options.pnCategoryId === 2 ? 'brandModel' : 'smallSubCategory'}
                  flex={1}
                  isCenter />
                <TableField name="品名" fieldKey="name" flex={1} isCenter />
                <TableField name="價格" fieldKey="price" flex={1} isCenter />
                <TableField name="線上價格" fieldKey="onlinePrice" flex={1} isCenter />
                <TableField name="廠商" fieldKey="vendor" flex={1} isCenter />
              </Table>
            </div>
          );
        }}
      </Query>
    );
  }
}

const reduxHook = connect(
  state => ({
    options: state.PNModel.listSearchOptions,
  }),
);

export default reduxHook(
  radium(
    PNList
  )
);
