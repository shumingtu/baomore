// @flow

import React, { Component, Fragment } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';

import PageSwitcher from '../../components/Global/PageSwitcher.jsx';

import PNModelSearcher from '../../components/PNModel/PNModelSearcher.jsx';
import PNList from './PNList.jsx';
import PNManageBoard from './PNManageBoard.jsx';
import PNModelPopUp from '../../components/PNModel/PNModelPopUp.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    position: 'relative',
    overflow: 'auto',
  },
};

type Props = {
  boxData: Object,
};

class PNModelListBoard extends Component<Props> {
  state = {
    currentPageIdx: 0,
  };

  pageSwitcher() {
    const {
      currentPageIdx,
    } = this.state;

    switch (currentPageIdx) {
      case 0:
        return (
          <Fragment>
            <PNModelSearcher />
            <PNList />
          </Fragment>
        );
      case 1:
        return (
          <PNManageBoard />
        );
      default:
        return null;
    }
  }

  render() {
    const {
      currentPageIdx,
    } = this.state;

    const {
      boxData,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        {boxData ? (
          <PNModelPopUp data={boxData} />
        ) : null }
        <PageSwitcher
          pages={['料號管理', '料號類別管理']}
          currentPageIdx={currentPageIdx}
          onChange={i => this.setState({ currentPageIdx: i })} />
        {this.pageSwitcher()}
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    boxData: state.PNModel.boxData,
  }),
);

export default reduxHook(
  radium(
    PNModelListBoard
  )
);
