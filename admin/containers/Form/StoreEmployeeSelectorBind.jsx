// @flow
import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';
import {
  Field,
} from 'redux-form';

// config
import { GET_EMPLOYEE_LIST } from '../../queries/Member.js';
import { FETCH_STORES } from '../../queries/Global.js';
// components
import Selector from '../../components/Form/Selector.jsx';

const styles = {
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 10px 0 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
};

type Props = {
  storeId?: {
    input: Object,
    meta: Object,
  },
  memberId?: {
    input: Object,
    meta: Object,
  },
  disableRequirement?: boolean,
};

class StoreEmployeeSelectorBind extends PureComponent<Props> {
  static defaultProps = {
    storeId: null,
    memberId: null,
    disableRequirement: false,
  };

  render() {
    const {
      storeId,
      memberId,
      disableRequirement,
    } = this.props;

    return (
      <Fragment>
        {storeId ? (
          <Query query={FETCH_STORES}>
            {({
              data,
            }) => {
              const stores = (data && data.stores) || [];
              return (
                <div style={styles.fieldWrapper}>
                  <Field
                    required={!disableRequirement}
                    nullable
                    name="storeId"
                    label="門市："
                    options={stores}
                    placeholder="請選擇門市"
                    component={Selector} />
                </div>
              );
            }}
          </Query>
        ) : null}
        {memberId ? (
          <Query
            query={GET_EMPLOYEE_LIST}
            variables={storeId && storeId.input ? {
              storeId: storeId.input.value !== '-1' ? parseInt(storeId.input.value, 10) : null,
            } : {}}>
            {({
              data,
            }) => {
              const employeeMemberlist = (data && data.employeeMemberlist) || [];

              return (
                <div style={styles.fieldWrapper}>
                  <Field
                    required={!disableRequirement}
                    nullable
                    name="memberId"
                    label="包膜師："
                    options={employeeMemberlist}
                    placeholder="請選擇包膜師"
                    component={Selector} />
                </div>
              );
            }}
          </Query>
        ) : null}
      </Fragment>
    );
  }
}

export default radium(StoreEmployeeSelectorBind);
