// @flow
import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
import {
  Field,
} from 'redux-form';
import { Query } from 'react-apollo';
import unionBy from 'lodash/unionBy';

// config
import { pnCategoryTypeIsProtector } from '../../helper/linebot.js';
import {
  customerReceivedProtectorErrorTypes,
  customerReceivedMaterialErrorTypes,
  employeeProtectorErrorTypes,
  employeeMaterialErrorTypes,
} from '../../shared/linebot.js';
import { FETCH_PNCATEGORIES } from '../../queries/PNTable.js';
// components
import Selector from '../../components/Form/Selector.jsx';

const styles = {
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 10px 0 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
};

type Props = {
  pnCategoryId?: {
    input: Object,
    meta: Object,
  },
  errorType?: {
    input: Object,
    meta: Object,
  },
  names: Array<string>,
};

const PROTECTOR_ID = 2;

class PNTableWithErrorSelectorBind extends PureComponent<Props> {
  static defaultProps = {
    errorType: null,
    pnCategoryId: null,
  }

  errorTypesDecider() {
    const {
      pnCategoryId,
      errorType,
    } = this.props;

    if (errorType && pnCategoryId) {
      if (pnCategoryId.input.value && pnCategoryId.input.value !== '-1') {
        const isProtector = pnCategoryTypeIsProtector(
          parseInt(pnCategoryId.input.value, 10) === PROTECTOR_ID
            ? 'protector'
            : 'others'
        );

        if (isProtector) {
          return unionBy(employeeProtectorErrorTypes.concat(customerReceivedProtectorErrorTypes), 'id');
        }

        return unionBy(employeeMaterialErrorTypes.concat(customerReceivedMaterialErrorTypes), 'id');
      }
    }

    return [];
  }

  render() {
    const {
      pnCategoryId,
      errorType,
    } = this.props;

    if (!errorType && !pnCategoryId) return null;

    return (
      <Fragment>
        {pnCategoryId ? (
          <Query query={FETCH_PNCATEGORIES}>
            {({
              data,
            }) => {
              const pnCategories = (data && data.pnCategories) || [];

              return (
                <div style={styles.fieldWrapper}>
                  <Field
                    nullable
                    name="pnCategoryId"
                    label="商品類型："
                    options={pnCategories}
                    placeholder="請選擇商品類型"
                    customOnChange={() => {
                      if (errorType && errorType.input) {
                        errorType.input.onChange('-1');
                      }
                    }}
                    {...pnCategoryId}
                    component={Selector} />
                </div>
              );
            }}
          </Query>
        ) : null}
        {errorType ? (
          <div style={styles.fieldWrapper}>
            <Field
              nullable
              name="errorType"
              label="異常類型："
              options={this.errorTypesDecider()}
              placeholder="請選擇異常類型"
              {...errorType}
              component={Selector} />
          </div>
        ) : null}
      </Fragment>
    );
  }
}

export default radium(
  PNTableWithErrorSelectorBind
);
