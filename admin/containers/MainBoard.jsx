// @flow

import React, { Component } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Switch,
  Route,
} from 'react-router';
import qs from 'qs';

import * as MemberActions from '../actions/Member.js';
import SiteNavBar from './SiteNavBar.jsx';
import Button from '../components/Global/Button.jsx';
import { navBarLinks } from '../shared/Global.js';
import { isPermissionAllowed } from '../helper/roles.js';
// routes
import PhoneModelRoutes from '../routes/PhoneModel.jsx';
import OrderRoutes from '../routes/Order.jsx';
import PNModelRoutes from '../routes/PNModel.jsx';
import InvoicingRoutes from '../routes/Invoicing.jsx';
import LandingRoutes from '../routes/Landing.jsx';
import ActivityRoutes from '../routes/Activity.jsx';
import RollbackRoutes from '../routes/Rollback.jsx';
import AnnouncementRoutes from '../routes/Announcement.jsx';
import SystemInfoRoutes from '../routes/SystemInfo.jsx';
import PerformanceBoard from './Performance/PerformanceBoard.jsx';
import MemberManagementRoutes from '../routes/MemberManagement.jsx';
import AuthorizationRoutes from '../routes/Authorization.jsx';
import PunchRoutes from '../routes/Punch.jsx';

const styles = {
  placement: {
    width: '100%',
    height: '100%',
    minHeight: '100vh',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    position: 'relative',
  },
  main: {
    flex: 1,
    width: 'auto',
    height: '100vh',
    padding: 24,
    margin: 0,
    display: 'flex',
    flexDirection: 'column',
    overflow: 'hidden',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      padding: 12,
    },
  },
  mainWrapper: {
    width: '100%',
    height: 'auto',
    flex: 1,
    overflow: 'hidden',
  },
  logoutBtnWrap: {
    position: 'absolute',
    right: 10,
    top: 10,
    zIndex: 999,
  },
};

type Props = {
  history: {
    replace: Function,
  },
  location: {
    pathname: string,
    search: string,
  },
  cacheMemberInfo: Function,
  memberLogout: Function,
};

class MainBoard extends Component<Props> {
  componentDidMount() {
    const {
      location: {
        pathname,
        search,
      },
      history,
      cacheMemberInfo,
      memberLogout,
    } = this.props;

    const width = window.innerWidth
    || document.documentElement.clientWidth
    || document.body.clientWidth;

    if (width < 768) {
      document.title = 'Artmo 行動管理系統';
    }

    const isRedirectFromLine = pathname === '/oAuth';

    if (isRedirectFromLine) {
      if (!search) {
        history.replace('/login');
      } else {
        const qsString = qs.parse(search, { ignoreQueryPrefix: true });
        const token = qsString.token || '';

        cacheMemberInfo({
          accessToken: token,
        });

        const filteredNavBar = navBarLinks.filter(l => isPermissionAllowed(l.actionKeys));

        if (!filteredNavBar.length) {
          window.alert('沒有權限');
          memberLogout();
          history.replace('/login');
          return;
        }

        history.replace('/');
      }
    }
  }

  render() {
    const {
      memberLogout,
    } = this.props;

    const filteredNavBar = navBarLinks.filter(l => isPermissionAllowed(l.actionKeys));

    return (
      <div style={styles.placement}>
        <SiteNavBar filteredNavBar={filteredNavBar} />
        <div style={styles.logoutBtnWrap}>
          <Button onClick={() => memberLogout()} label="登出" />
        </div>
        <main style={styles.main}>
          <div style={styles.mainWrapper}>
            <Switch>
              <Route path="/phoneModel" component={PhoneModelRoutes} />
              <Route path="/order" component={OrderRoutes} />
              <Route path="/pnModel" component={PNModelRoutes} />
              <Route path="/invoicing" component={InvoicingRoutes} />
              <Route path="/landing" component={LandingRoutes} />
              <Route path="/activity" component={ActivityRoutes} />
              <Route path="/rollback" component={RollbackRoutes} />
              <Route path="/announcement" component={AnnouncementRoutes} />
              <Route path="/systemInfo" component={SystemInfoRoutes} />
              <Route path="/performance" component={PerformanceBoard} />
              <Route path="/member" component={MemberManagementRoutes} />
              <Route path="/authorization" component={AuthorizationRoutes} />
              <Route path="/punchManagement" component={PunchRoutes} />
            </Switch>
          </div>
        </main>
      </div>
    );
  }
}

const reduxHook = connect(
  () => ({}),
  dispatch => bindActionCreators({
    ...MemberActions,
  }, dispatch)
);

export default reduxHook(radium(MainBoard));
