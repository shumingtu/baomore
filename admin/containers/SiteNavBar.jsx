// @flow
import React, { Component } from 'react';
import radium from 'radium';
import {
  NavLink as navLink,
} from 'react-router-dom';
// config
import Theme from '../styles/Theme.js';

const NavLink = radium(navLink);

const styles = {
  wrapper: {
    width: 200,
    minWidth: 200,
    height: 'auto',
    minHeight: '100vh',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: Theme.MAIN_THEME_COLOR,
    padding: '12px 16px',
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
  mainInfoWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderBottom: `3px solid ${Theme.SECONDARY_THEME_COLOR}`,
  },
  logo: {
    width: 90,
    height: 90,
    textDecoration: 'none',
    border: 0,
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
  labelWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '8px 0',
    textAlign: 'center',
  },
  label: {
    fontSize: 14,
    fontWeight: 500,
    letterSpacing: 1.2,
    whiteSpace: 'pre-line',
    color: Theme.NAV_COLOR,
    textAlign: 'center',
    border: 0,
    textDecoration: 'none',
    transition: 'color 0.2s ease-in',
  },
  navBarWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  navLink: {
    fontSize: 14,
    fontWeight: 500,
    letterSpacing: 1.2,
    whiteSpace: 'pre-line',
    color: Theme.NAV_COLOR,
    border: 0,
    textDecoration: 'none',
    transition: 'color 0.2s ease-in',
    cursor: 'pointer',
  },
  navActive: {
    color: Theme.SECONDARY_THEME_COLOR,
  },
};

type Props = {
  filteredNavBar: array,
};

class SiteNavBar extends Component<Props> {
  render() {
    const {
      filteredNavBar,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.mainInfoWrapper}>
          <NavLink
            to={{ pathname: '/' }}
            style={styles.logo} />
          <div style={styles.labelWrapper}>
            <span style={styles.label}>
              Artmo
              <br />
              後台管理系統
            </span>
          </div>
        </div>
        <div style={styles.navBarWrapper}>
          {filteredNavBar.map(x => (
            <div key={x.id} style={styles.labelWrapper}>
              <NavLink
                key={x.pathname}
                to={{ pathname: x.pathname }}
                style={styles.navLink}
                activeStyle={styles.navActive}>
                {x.title}
              </NavLink>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default radium(SiteNavBar);
