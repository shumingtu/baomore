// @flow
import React, { Component, Fragment } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import {
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

import PageLinkSwitcher from '../../components/Global/PageLinkSwitcher.jsx';

import OrderSearcher from '../../components/Order/OrderSearcher.jsx';
import OrderList from './OrderList.jsx';

import OrderScheduleSearcher from '../../components/Order/OrderScheduleSearcher.jsx';
import OrderScheduleList from './OrderScheduleList.jsx';

import OrderPopUp from '../../components/Order/OrderPopUp.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    position: 'relative',
    overflow: 'auto',
  },
  linkSwitcherWrapper: {
    width: '100%',
    height: 'auto',
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
};

type Props = {
  boxData: Array,
  match: {
    url: string,
  },
};

const pageLinks = [{
  path: '/order/list',
  name: '訂單總表',
}, {
  path: '/order/schedule',
  name: '預約行程表',
}];

class OrderListBoard extends Component<Props> {
  render() {
    const {
      boxData,
      match: {
        url,
      },
    } = this.props;

    return (
      <div style={styles.wrapper}>
        {boxData ? (
          <OrderPopUp data={boxData} />
        ) : null}
        <div style={styles.linkSwitcherWrapper}>
          <PageLinkSwitcher pages={pageLinks} />
        </div>
        <Switch>
          <Route
            path={`${url}/list`}
            component={() => (
              <Fragment>
                <OrderSearcher />
                <OrderList />
              </Fragment>
            )} />
          <Route
            path={`${url}/schedule`}
            component={() => (
              <Fragment>
                <OrderScheduleSearcher />
                <OrderScheduleList />
              </Fragment>
            )} />
          <Redirect to={{ pathname: `${url}/list` }} />
        </Switch>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    boxData: state.Order.boxData,
  }),
);

export default reduxHook(
  radium(OrderListBoard)
);
