// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Fields,
  SubmissionError,
  initialize,
} from 'redux-form';
import {
  Mutation,
  graphql,
} from 'react-apollo';

// config
import Theme from '../../styles/Theme.js';
import TextStyles from '../../styles/Text.js';
import { FORM_ORDER_SCHEDULE_EDIT } from '../../shared/form.js';
import { validateOrderScheduleEditForm } from '../../helper/validator/OrderScheduleEditForm.js';
import {
  FETCH_SINGLE_ORDER,
} from '../../queries/Order.js';
import { CHANGE_SCHEDULE } from '../../mutations/Order.js';
import { changeScheduleUpdate } from '../../helper/schedule.js';
import * as OrderActions from '../../actions/Order.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import SubmitButton from '../../components/Form/SubmitButton.jsx';
import Button from '../../components/Global/Button.jsx';
import Error from '../../components/Form/Error.jsx';
import ScheduleSelectorBind from '../../components/Form/ScheduleSelectorBind.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  formWrapper: {
    width: 640,
    height: 'auto',
    padding: '24px 36px',
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    borderRadius: 3,
    border: `1px solid ${Theme.BLACK_COLOR}`,
    '@media (max-width: 767px)': {
      width: '100%',
      padding: '24px 12px',
    },
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  fieldLabel: {
    ...TextStyles.labelText,
    width: 120,
    '@media (max-width: 767px)': {
      width: 108,
    },
  },
  content: {
    ...TextStyles.labelText,
    width: 'auto',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  btnWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 6px',
  },
};

type Props = {
  handleSubmit: Function,
  match: {
    params: {
      onlineOrderId: string,
    },
  },
  history: {
    push: Function,
  },
  storeId: string,
  employeeId: string,
  selectedDate: string,
  selectedTime: string,
  initializeForm: Function,
  onlineOrder: {
    id: string,
    store: {
      id: number,
      name: string,
    },
    employee: {
      id: number,
      name: string,
    },
    date: string,
    time: string,
  },
  scheduleSearchOptions: {
    startDate: string,
    endDate: string,
    bookedMemberId: number,
    storeId: number,
  },
  openScheduleBox: Function,
};

type State = {
  cacheOriginTime: string,
};

class OrderScheduleEditPage extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      cacheOriginTime: null,
    };
  }

  componentDidMount() {
    const {
      onlineOrder = null,
    } = this.props;

    this.initializing(onlineOrder);
  }

  componentDidUpdate(prevProps) {
    const {
      onlineOrder,
    } = this.props;

    if (prevProps.onlineOrder !== onlineOrder && onlineOrder) {
      this.initializing(onlineOrder);
    }
  }

  getFields(d) {
    const {
      storeId,
      employeeId,
      date,
      time,
    } = d;

    const {
      match: {
        params: {
          onlineOrderId,
        },
      },
    } = this.props;

    const {
      cacheOriginTime,
    } = this.state;

    const errors = validateOrderScheduleEditForm({
      ...d,
      time: time === '-1' ? cacheOriginTime : time,
    });

    if (errors) {
      throw new SubmissionError(errors);
    }

    return ({
      onlineOrderId: onlineOrderId || null,
      date,
      time: time === '-1' ? cacheOriginTime : time,
      employeeId: parseInt(employeeId, 10),
      storeId: parseInt(storeId, 10),
    });
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    initializeForm({
      storeId: (obj && obj.store && obj.store.id) || '-1',
      employeeId: (obj && obj.employee && obj.employee.id) || '-1',
      date: (obj && obj.date) || null,
      time: (obj && obj.time) || '-1',
    });

    if (obj && obj.time) {
      this.setState({
        cacheOriginTime: obj.time,
      });
    }
  }

  close() {
    const {
      openScheduleBox,
    } = this.props;

    openScheduleBox(null);
  }

  render() {
    const {
      handleSubmit,
      history,
      onlineOrder,
      scheduleSearchOptions = {},
    } = this.props;

    const {
      cacheOriginTime,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        <PageTitle title="修改行程" />
        <Mutation
          mutation={CHANGE_SCHEDULE}
          update={(cache, { data: { changeSchedule } }) => changeScheduleUpdate({
            cache,
            newSchedule: changeSchedule,
            searchOptions: scheduleSearchOptions,
          })}>
          {(changeSchedule, { loading, error }) => (
            <form
              style={styles.formWrapper}
              onSubmit={handleSubmit(async (d) => {
                const fields = this.getFields(d);

                const {
                  data,
                } = await changeSchedule({
                  variables: fields,
                });

                if (data && data.changeSchedule && data.changeSchedule.id) {
                  this.close();
                  history.push('/order/schedule');
                }
              })}>
              <div style={styles.fieldWrapper}>
                <span style={styles.fieldLabel}>
                  訂購人姓名：
                </span>
                {onlineOrder && onlineOrder.name ? (
                  <span style={styles.content}>
                    {onlineOrder.name}
                  </span>
                ) : null}
              </div>
              <div style={styles.fieldWrapper}>
                <span style={styles.fieldLabel}>
                  訂購人電話：
                </span>
                {onlineOrder && onlineOrder.phone ? (
                  <span style={styles.content}>
                    {onlineOrder.phone}
                  </span>
                ) : null}
              </div>
              <div style={styles.fieldWrapper}>
                <span style={styles.fieldLabel}>
                  訂購人Email：
                </span>
                {onlineOrder && onlineOrder.email ? (
                  <span style={styles.content}>
                    {onlineOrder.email}
                  </span>
                ) : null}
              </div>
              <Fields
                component={ScheduleSelectorBind}
                cacheOriginTime={cacheOriginTime}
                names={[
                  'storeId',
                  'employeeId',
                  'time',
                  'date',
                ]} />
              {error ? (
                <Error error={(error && error.message) || null} />
              ) : null}
              <div style={styles.functionWrapper}>
                <div style={styles.btnWrapper}>
                  <SubmitButton
                    disabled={loading}
                    label="確定修改" />
                </div>
                <div style={styles.btnWrapper}>
                  <Button
                    isWhiteBg
                    label="取消"
                    onClick={() => history.push('/order/schedule')} />
                </div>
              </div>
            </form>
          )}
        </Mutation>
      </div>
    );
  }
}

const queryHook = graphql(FETCH_SINGLE_ORDER, {
  options: ({
    match: {
      params: {
        onlineOrderId,
      },
    },
  }) => ({
    fetchPolicy: 'cache-and-network',
    variables: {
      onlineOrderId: onlineOrderId || null,
    },
  }),
  props: ({
    data: {
      onlineOrder,
    },
  }) => ({
    onlineOrder: onlineOrder || null,
  }),
});

const formHook = reduxForm({
  form: FORM_ORDER_SCHEDULE_EDIT,
});

const reduxHook = connect(
  state => ({
    scheduleSearchOptions: state.Order.scheduleSearchOptions,
  }),
  dispatch => bindActionCreators({
    initializeForm: v => initialize(FORM_ORDER_SCHEDULE_EDIT, v),
    ...OrderActions,
  }, dispatch)
);

export default reduxHook(
  queryHook(
    formHook(
      radium(
        OrderScheduleEditPage
      )
    )
  )
);
