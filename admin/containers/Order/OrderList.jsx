// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';
// components
import Table, { TableField } from '../../components/Table/Table.jsx';
import OnlineOrderListActions from '../../components/Table/Actions/OnlineOrderListActions.jsx';
// config
import { FETCH_ONLINEORDER_LIST } from '../../queries/Order.js';

import {
  shippingStatusDecider,
  payedStatusDecider,
} from '../../helper/statusDecider.js';

const styles = {
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 267px)',
    maxHeight: 810,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    overflow: 'auto',
  },
};

type Props = {
  options: {
    startDate: string,
    endDate: string,
    bookedMemberId: number,
    shippingStatus: string,
    storeId: number,
    keyword: string,
    payedStatus: string,
  },
};

const ORDER_LIST_LIMIT = 10;

class OrderList extends PureComponent<Props> {
  render() {
    const {
      options,
    } = this.props;

    return (
      <Query
        fetchPolicy="network-only"
        query={FETCH_ONLINEORDER_LIST}
        variables={options}>
        {({ data, fetchMore }) => {
          if (!data) return null;

          const {
            onlineOrderlist = [],
          } = data;

          return (
            <div style={styles.tableWrapper}>
              <Table
                fixedItemWidth={100}
                dataSource={onlineOrderlist.map(c => ({
                  id: c.id,
                  picture: c.picture,
                  name: c.name,
                  phone: c.phone,
                  email: c.email,
                  store: (c.store && c.store.name) || null,
                  dateTime: `${c.date}\n${c.time}`,
                  employee: (c.employee && c.employee.name) || '尚未指定包膜師',
                  createdAt: c.createdAt,
                  payedStatusText: payedStatusDecider(c.payedStatus),
                  payedStatus: c.payedStatus,
                  shippingStatus: shippingStatusDecider(c.shippingStatus),
                  isShared: c.isShared ? '是' : '否',
                  orderSN: (c.order && c.order.orderSN) || '無',
                }))}
                fetchMore={() => fetchMore({
                  variables: {
                    ...options,
                    offset: 0,
                    limit: onlineOrderlist.length + ORDER_LIST_LIMIT,
                  },
                  updateQuery: (prev, { fetchMoreResult }) => {
                    if (!fetchMoreResult) return prev;

                    return {
                      ...prev,
                      onlineOrderlist: [
                        ...fetchMoreResult.onlineOrderlist,
                      ],
                    };
                  },
                })}
                actionTitles={['轉派']}
                getActions={() => [
                  <OnlineOrderListActions />,
                ]}
                showPlaceholder={!onlineOrderlist.length}
                placeholder="查無資料">
                <TableField name="線上訂單編號" fieldKey="id" isCenter flex={3} />
                <TableField name="訂單編號" fieldKey="orderSN" isCenter flex={1} />
                <TableField name="圖片" fieldKey="picture" isImage isCenter flex={1} />
                <TableField name="共享" fieldKey="isShared" isCenter flex={1} />
                <TableField name="訂購人姓名" fieldKey="name" flex={1} isCenter />
                <TableField name="訂購人電話" fieldKey="phone" flex={1} isCenter />
                <TableField name="訂購人Email" fieldKey="email" flex={2} isCenter />
                <TableField name="預約門市" fieldKey="store" flex={1} isCenter />
                <TableField name="預約時間" fieldKey="dateTime" flex={1} isCenter />
                <TableField name="包膜師" fieldKey="employee" flex={1} isCenter />
                <TableField name="創建時間" fieldKey="createdAt" flex={1} isCenter />
                <TableField name="付款狀態" fieldKey="payedStatusText" flex={1} isCenter />
                <TableField name="出貨狀態" fieldKey="shippingStatus" flex={1} isCenter />
              </Table>
            </div>
          );
        }}
      </Query>
    );
  }
}

const reduxHook = connect(
  state => ({
    options: state.Order.listSearchOptions,
  }),
);

export default reduxHook(
  radium(
    OrderList
  )
);
