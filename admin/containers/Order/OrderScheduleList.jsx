// @flow
import React, {
  Component,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import {
  Query,
  graphql,
} from 'react-apollo';
// components
import Theme from '../../styles/Theme.js';
// config
import {
  FETCH_SCHEDULE_LIST,
  FETCH_BOOK_TOME_SLOT_LIST,
} from '../../queries/Order.js';

import * as OrderActions from '../../actions/Order.js';
import TablePlaceholder from '../../components/Table/TablePlaceholder.jsx';

const styles = {
  tableWrapper: {
    width: '100%',
    minWidth: 680,
    height: 'calc(100% - 209px)',
    maxHeight: 810,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      minWidth: 450,
    },
  },
  tableInnerWrapper: {
    flex: 1,
    width: '100%',
    height: 'auto',
    overflowY: 'auto',
  },
  tableFrozenHeaderWrapper: {
    width: '100%',
    height: 44,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
    padding: '0 4px 0 0',
    borderBottom: `4px solid ${Theme.SECONDARY_THEME_COLOR}`,
  },
  headerField: {
    flex: 1,
    margin: '0 8px',
    textAlign: 'center',
    fontSize: 14,
    fontWeight: 500,
    color: Theme.BLACK_COLOR,
    '@media (max-width: 767px)': {
      fontSize: 13,
      margin: '0 4px',
    },
  },
  tableListWrapper: {
    flex: 1,
    width: '100%',
    height: 'calc(100% - 44px)',
    overflowY: 'auto',
    overflowX: 'hidden',
  },
  tableItemWrapper: {
    width: '100%',
    minHeight: 66,
    padding: '12px 0',
    margin: '0 0 8px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#fff',
    borderRadius: 2,
    border: 0,
    boxShadow: Theme.BLOCK_SHADOW,
  },
  tableItemPlacement: {
    flex: 1,
    height: '100%',
    margin: '0 8px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      margin: '0 4px',
    },
  },
  tableItem: {
    fontSize: 13,
    fontWeight: 500,
    lineHeight: 1.618,
    whiteSpace: 'pre-line',
    color: Theme.BLACK_COLOR,
    textAlign: 'center',
    '@media (max-width: 767px)': {
      fontSize: 12,
    },
  },
  tableItemButton: {
    border: 'none',
    outline: 'none',
    color: 'blue',
    textDecoration: 'underline',
    cursor: 'pointer',
  },
};

type Props = {
  options: {
    startDate: string,
    endDate: string,
    bookedMemberId: number,
    storeId: number,
  },
  openScheduleBox: Function,
  schedulelist: Array,
  fetchMore: Function,
};

const ORDER_SCHEDULE_LIST_LIMIT = 10;

class OrderScheduleList extends Component<Props> {
  constructor(props) {
    super(props);

    this.fetching = false;
    this.reachEnd = false;
    this.scrollListener = () => this.onScroll();
    this.state = {
      paddingHeight: null,
    };
  }

  componentDidMount() {
    this.scroller.addEventListener('scroll', this.scrollListener, false);
    moment.locale('zh-tw');
  }

  componentDidUpdate(prevProps) {
    const {
      options,
      schedulelist,
    } = this.props;

    if (prevProps.schedulelist !== schedulelist) {
      this.fetching = false;
    }

    if (prevProps.options !== options) {
      document.getElementById('orderScrollBar').scrollTop = 0;
    }
  }

  componentWillUnmount() {
    this.scroller.removeEventListener('scroll', this.scrollListener, false);
  }

  async onScroll() {
    const {
      scrollHeight,
      scrollTop,
      clientHeight,
    } = this.scroller;

    const {
      paddingHeight,
    } = this.state;

    const {
      fetchMore,
      options,
    } = this.props;

    const remainingHeight = scrollHeight - (scrollTop + clientHeight);

    if (remainingHeight < 150) {
      if (this.fetching) return;

      fetchMore(options);

      if (paddingHeight === null) {
        this.setState({
          paddingHeight: 30,
        });
      } else if (this.reachEnd) {
        this.setState({
          paddingHeight: 0,
        });
      }
    }
  }

  openBox(onlineOrders) {
    const {
      openScheduleBox,
    } = this.props;

    openScheduleBox(onlineOrders);
  }

  render() {
    const {
      schedulelist,
    } = this.props;

    return (
      <Query query={FETCH_BOOK_TOME_SLOT_LIST}>
        {({ data }) => {
          if (!data) return null;
          const {
            bookTimeSlotList = [],
          } = data;

          return (
            <div style={styles.tableWrapper}>
              <div style={styles.tableFrozenHeaderWrapper}>
                <span style={[styles.headerField]}>
                  日期
                </span>
                {bookTimeSlotList.map(slot => (
                  <span key={slot.time} style={styles.headerField}>
                    {slot.time}
                  </span>
                ))}
              </div>
              {schedulelist.length === 0 ? <TablePlaceholder /> : null}
              <div
                id="orderScrollBar"
                className="hideScrollBar"
                ref={(ref) => { this.scroller = ref; }}
                style={styles.tableListWrapper}>
                {schedulelist.map((schedule) => {
                  const {
                    assignments,
                  } = schedule;

                  return (
                    <div key={schedule.date} style={styles.tableItemWrapper}>
                      <div style={styles.tableItemPlacement}>
                        <span style={styles.tableItem}>{moment(schedule.date).format('M/D/dddd')}</span>
                      </div>
                      {bookTimeSlotList.map((slot) => {
                        const slotAssign = assignments.find(x => x.time === slot.time);

                        if (!slotAssign) {
                          return (
                            <div key={`noServe${slot.time}`} style={styles.tableItemPlacement}>
                              <span style={styles.tableItem}>N/A</span>
                            </div>
                          );
                        }
                        return (
                          <div key={`serve${slot.time}`} style={styles.tableItemPlacement}>
                            <button
                              type="button"
                              onClick={() => this.openBox(slotAssign.onlineOrders)}
                              style={[
                                styles.tableItem,
                                styles.tableItemButton,
                              ]}>
                              {`${slotAssign.onlineOrders.length}/${slotAssign.denominator}`}
                            </button>
                          </div>
                        );
                      })}
                    </div>
                  );
                })}
              </div>
            </div>
          );
        }}
      </Query>
    );
  }
}

const queryHook = graphql(FETCH_SCHEDULE_LIST, {
  options: ownProps => ({
    variables: {
      startDate: ownProps.options.startDate || null,
      endDate: ownProps.options.endDate || null,
      storeId: ownProps.options.storeId || null,
      bookedMemberId: ownProps.options.bookedMemberId || null,
      limit: ownProps.options.limit || 10,
      offset: ownProps.options.offset || 0,
    },
  }),
  props: ({
    data: {
      schedulelist,
      fetchMore,
    },
  }) => ({
    schedulelist: schedulelist || [],
    fetchMore: queryOptions => fetchMore({
      variables: {
        ...queryOptions,
        offset: 0,
        limit: schedulelist.length + ORDER_SCHEDULE_LIST_LIMIT,
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prev;

        return {
          ...prev,
          schedulelist: [
            ...fetchMoreResult.schedulelist,
          ],
        };
      },
    }),
  }),
});

const reduxHook = connect(
  state => ({
    options: state.Order.scheduleSearchOptions,
  }),
  dispatch => bindActionCreators({
    ...OrderActions,
  }, dispatch),
);

export default reduxHook(
  queryHook(
    radium(
      OrderScheduleList
    )
  )
);
