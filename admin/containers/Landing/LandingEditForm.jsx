// @flow
import React, { PureComponent } from 'react';
import {
  Query,
  Mutation,
} from 'react-apollo';

import {
  FETCH_LANDINGS,
} from '../../queries/Landing.js';
import {
  EDIT_LANDING,
} from '../../mutations/Landing.js';
import LandingBaseForm from './LandingBaseForm.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
};

type Props = {
  match: {
    params: {
      landingId: string,
    },
  },
};

class LandingEditForm extends PureComponent<Props> {
  render() {
    const {
      match: {
        params: {
          landingId,
        },
      },
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <Query
          query={FETCH_LANDINGS}
          variables={{
            id: parseInt(landingId, 10),
          }}>
          {({
            data,
          }) => {
            const wrapLanding = (data && data.landings) || [];

            return (
              <Mutation mutation={EDIT_LANDING}>
                {editLanding => (
                  <LandingBaseForm
                    {...this.props}
                    mutate={editLanding}
                    data={wrapLanding[0] || null} />
                )}
              </Mutation>
            );
          }}
        </Query>
      </div>
    );
  }
}

export default LandingEditForm;
