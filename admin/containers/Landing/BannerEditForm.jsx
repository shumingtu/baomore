// @flow
import React, { PureComponent } from 'react';
import {
  Query,
  Mutation,
} from 'react-apollo';

import {
  FETCH_LANDING_BANNERS,
} from '../../queries/Landing.js';
import {
  EDIT_BANNER,
} from '../../mutations/Landing.js';
import BannerBaseForm from './BannerBaseForm.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
};

type Props = {
  match: {
    params: {
      bannerId: string,
    },
  },
};

class BannerEditForm extends PureComponent<Props> {
  render() {
    const {
      match: {
        params: {
          bannerId,
        },
      },
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <Query
          query={FETCH_LANDING_BANNERS}
          variables={{
            id: parseInt(bannerId, 10),
          }}>
          {({
            data,
          }) => {
            const wrapBanners = (data && data.banners) || [];

            return (
              <Mutation mutation={EDIT_BANNER}>
                {editBanner => (
                  <BannerBaseForm
                    {...this.props}
                    mutate={editBanner}
                    data={wrapBanners[0] || null} />
                )}
              </Mutation>
            );
          }}
        </Query>
      </div>
    );
  }
}

export default BannerEditForm;
