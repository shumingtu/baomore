// @flow
import React, { Component } from 'react';
import radium from 'radium';
import {
  Switch,
  Route,
} from 'react-router-dom';
// components
import PageLinkSwitcher from '../../components/Global/PageLinkSwitcher.jsx';
import LandingManage from './LandingManage.jsx';
import LandingCreateForm from './LandingCreateForm.jsx';
import LandingEditForm from './LandingEditForm.jsx';
import BannerManage from './BannerManage.jsx';
import BannerCreateForm from './BannerCreateForm.jsx';
import BannerEditForm from './BannerEditForm.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    position: 'relative',
    overflow: 'auto',
  },
};

type Props = {
  location: {
    pathname: string,
  },
  history: {
    replace: Function,
  },
};

const MY_PAGES = [{
  name: '大圖管理',
  path: '/landing/manage',
}, {
  name: '輪播管理',
  path: '/landing/banner',
}];

class LandingMainBoard extends Component<Props> {
  componentDidMount() {
    const {
      location: {
        pathname,
      },
      history,
    } = this.props;

    if (pathname === '/landing') {
      history.replace('/landing/manage');
    }
  }

  componentDidUpdate(prevProps) {
    const {
      location: {
        pathname,
      },
      history,
    } = this.props;

    if (pathname !== prevProps.pathname && pathname === '/landing') {
      history.replace('/landing/manage');
    }
  }

  render() {
    return (
      <div style={styles.wrapper}>
        <PageLinkSwitcher pages={MY_PAGES} />
        <Switch>
          <Route exact path="/landing/manage/create" component={LandingCreateForm} />
          <Route exact path="/landing/manage/:landingId" component={LandingEditForm} />
          <Route exact path="/landing/manage" component={LandingManage} />
          <Route exact path="/landing/banner/create" component={BannerCreateForm} />
          <Route exact path="/landing/banner/:bannerId" component={BannerEditForm} />
          <Route exact path="/landing/banner" component={BannerManage} />
        </Switch>
      </div>
    );
  }
}

export default radium(
  LandingMainBoard
);
