// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  initialize,
  SubmissionError,
} from 'redux-form';

// config
import TextStyles from '../../styles/Text.js';
import {
  FORM_LANDING_BANNER_CREATE_EDIT_FORM,
} from '../../shared/form.js';
// components
import FormTitle from '../../components/Global/FormTitle.jsx';
import FileInput from '../../components/Form/FileInput.jsx';
import Input from '../../components/Form/Input.jsx';
import SubmitButton from '../../components/Form/SubmitButton.jsx';
import Button from '../../components/Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 12,
  },
  formWrapper: {
    width: 650,
    height: 'auto',
    padding: '16px 32px',
    backgroundColor: '#fff',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  label: {
    ...TextStyles.labelText,
  },
  inputWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  desktopImgWrapper: {
    width: 427,
    height: 238,
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  buttonWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 6px',
  },
};

type Props = {
  handleSubmit: Function,
  initializeForm: Function,
  history: {
    push: Function,
  },
  data: {
    id: number,
    picture: string,
    link: string,
  },
  mutate: Function,
};

class BannerBaseForm extends PureComponent<Props> {
  componentDidMount() {
    const {
      data,
    } = this.props;

    this.initializing(data);
  }

  componentDidUpdate(prevProps) {
    const {
      data,
    } = this.props;

    if (data !== prevProps.data && data) {
      this.initializing(data);
    }
  }

  getFields(d) {
    const {
      picture,
      link,
      mobileImg,
    } = d;

    if (!picture || picture === 'uploading') {
      throw new SubmissionError({
        picture: 'failed',
      });
    }

    return ({
      picture,
      link: link || null,
      mobileImg: mobileImg || null,
    });
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    initializeForm({
      picture: (obj && obj.picture) || '',
      link: (obj && obj.link) || '',
      mobileImg: (obj && obj.mobileImg) || '',
    });
  }

  render() {
    const {
      handleSubmit,
      history,
      data,
      mutate,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <FormTitle
          title={data ? '修改橫幅' : '新增橫幅'} />
        <form
          style={styles.formWrapper}
          onSubmit={handleSubmit(async (d) => {
            const {
              picture,
              link,
              mobileImg,
            } = this.getFields(d);

            if (!data) {
              await mutate({
                variables: {
                  picture,
                  link,
                  mobileImg,
                },
              });
            } else {
              await mutate({
                variables: {
                  id: data && data.id ? parseInt(data.id, 10) : null,
                  picture,
                  link,
                  mobileImg,
                },
              });
            }

            return history.push('/landing/banner');
          })}>
          <div style={styles.fieldWrapper}>
            <span style={styles.label}>
              圖片：
            </span>
            <div style={styles.inputWrapper}>
              <Field
                name="picture"
                annotation="圖片尺寸：2560x1300"
                component={FileInput}
                style={styles.desktopImgWrapper} />
            </div>
          </div>
          <div style={styles.fieldWrapper}>
            <span style={styles.label}>
              手機版圖片：
            </span>
            <div style={styles.inputWrapper}>
              <Field
                name="mobileImg"
                annotation="圖片尺寸：800x1170"
                component={FileInput}
                style={styles.mobileImgWrapper} />
            </div>
          </div>
          <div style={styles.fieldWrapper}>
            <div style={styles.inputWrapper}>
              <Field
                type="text"
                name="link"
                label="連結："
                placeholder="圖片連結"
                component={Input} />
            </div>
          </div>
          <div style={styles.functionWrapper}>
            <div style={styles.buttonWrapper}>
              <SubmitButton label="送出" />
            </div>
            <div style={styles.buttonWrapper}>
              <Button
                label="返回"
                onClick={() => history.push('/landing/banner')} />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_LANDING_BANNER_CREATE_EDIT_FORM,
});

const reduxHook = connect(
  () => ({}),
  dispatch => bindActionCreators({
    initializeForm: v => initialize(FORM_LANDING_BANNER_CREATE_EDIT_FORM, v),
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      BannerBaseForm
    )
  )
);
