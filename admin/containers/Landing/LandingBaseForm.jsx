// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  initialize,
  SubmissionError,
} from 'redux-form';

// config
import TextStyles from '../../styles/Text.js';
import {
  FORM_LANDING_MANAGE_CREATE_EDIT_FORM,
} from '../../shared/form.js';
// components
import FormTitle from '../../components/Global/FormTitle.jsx';
import FileInput from '../../components/Form/FileInput.jsx';
import SubmitButton from '../../components/Form/SubmitButton.jsx';
import Button from '../../components/Global/Button.jsx';
import Input from '../../components/Form/Input.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 12,
  },
  formWrapper: {
    width: 650,
    height: 'auto',
    padding: '16px 32px',
    backgroundColor: '#fff',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  label: {
    ...TextStyles.labelText,
  },
  inputWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  desktopImgWrapper: {
    width: 427,
    height: 175,
  },
  mobileImgWrapper: {
    width: 200,
    height: 295,
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  buttonWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 6px',
  },
};

type Props = {
  handleSubmit: Function,
  initializeForm: Function,
  history: {
    push: Function,
  },
  data: {
    id: number,
    desktopImg: string,
    mobileImg: string,
  },
  mutate: Function,
};

class LandingBaseForm extends PureComponent<Props> {
  componentDidMount() {
    const {
      data,
    } = this.props;

    this.initializing(data);
  }

  componentDidUpdate(prevProps) {
    const {
      data,
    } = this.props;

    if (data !== prevProps.data && data) {
      this.initializing(data);
    }
  }

  getFields(d) {
    const {
      desktopImg,
      mobileImg,
      link,
    } = d;

    if (!desktopImg || desktopImg === 'uploading') {
      throw new SubmissionError({
        desktopImg: 'failed',
      });
    }

    if (!mobileImg || mobileImg === 'uploading') {
      throw new SubmissionError({
        mobileImg: 'failed',
      });
    }

    return ({
      desktopImg,
      mobileImg,
      link: link || null,
    });
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    initializeForm({
      desktopImg: (obj && obj.desktopImg) || '',
      mobileImg: (obj && obj.mobileImg) || '',
      link: (obj && obj.link) || '',
    });
  }

  render() {
    const {
      handleSubmit,
      history,
      data,
      mutate,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <FormTitle
          title={data ? '修改大圖' : '新增大圖'} />
        <form
          style={styles.formWrapper}
          onSubmit={handleSubmit(async (d) => {
            const {
              desktopImg,
              mobileImg,
              link,
            } = this.getFields(d);

            if (!data) {
              await mutate({
                variables: {
                  desktopImg,
                  mobileImg,
                  link,
                },
              });
            } else {
              await mutate({
                variables: {
                  id: data && data.id ? parseInt(data.id, 10) : null,
                  desktopImg,
                  mobileImg,
                  link,
                },
              });
            }

            return history.push('/landing/manage');
          })}>
          <div style={styles.fieldWrapper}>
            <span style={styles.label}>
              桌面版圖片：
            </span>
            <div style={styles.inputWrapper}>
              <Field
                name="desktopImg"
                annotation="圖片尺寸：1280x524"
                component={FileInput}
                style={styles.desktopImgWrapper} />
            </div>
          </div>
          <div style={styles.fieldWrapper}>
            <span style={styles.label}>
              手機版圖片：
            </span>
            <div style={styles.inputWrapper}>
              <Field
                name="mobileImg"
                annotation="圖片尺寸：400x590"
                component={FileInput}
                style={styles.mobileImgWrapper} />
            </div>
          </div>
          <div style={styles.fieldWrapper}>
            <div style={styles.inputWrapper}>
              <Field
                type="text"
                name="link"
                label="連結："
                placeholder="圖片連結"
                component={Input} />
            </div>
          </div>
          <div style={styles.functionWrapper}>
            <div style={styles.buttonWrapper}>
              <SubmitButton label="送出" />
            </div>
            <div style={styles.buttonWrapper}>
              <Button
                label="返回"
                onClick={() => history.push('/landing/manage')} />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_LANDING_MANAGE_CREATE_EDIT_FORM,
});

const reduxHook = connect(
  () => ({}),
  dispatch => bindActionCreators({
    initializeForm: v => initialize(FORM_LANDING_MANAGE_CREATE_EDIT_FORM, v),
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      LandingBaseForm
    )
  )
);
