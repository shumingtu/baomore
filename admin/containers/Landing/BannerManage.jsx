// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';
// components
import Table, { TableField } from '../../components/Table/Table.jsx';
import LargeImage from '../../components/Table/Custom/LargeImage.jsx';
import Button from '../../components/Global/Button.jsx';
import BannerManageActions from '../../components/Table/Actions/BannerManageActions.jsx';
// config
import { FETCH_LANDING_BANNERS } from '../../queries/Landing.js';

const styles = {
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 35px)',
    maxHeight: 810,
    padding: '24px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
};

type Props = {
  history: {
    push: Function,
  },
  location: {
    pathname: string,
  },
};

class BannerManage extends PureComponent<Props> {
  render() {
    const {
      history,
      location: {
        pathname,
      },
    } = this.props;

    return (
      <Query query={FETCH_LANDING_BANNERS}>
        {({ data }) => {
          if (!data) return null;

          const {
            banners = [],
          } = data;

          return (
            <div style={styles.tableWrapper}>
              <Button
                label="新增橫幅"
                onClick={() => history.push(`${pathname}/create`)} />
              <Table
                dataSource={banners.map(l => ({
                  id: l.id,
                  picture: l.picture,
                  link: l.link || null,
                }))}
                getActions={() => [
                  <BannerManageActions />,
                ]}
                actionTitles={['操作']}
                showPlaceholder={!banners.length}
                placeholder="尚無首頁橫幅">
                <TableField
                  name="圖片"
                  fieldKey="picture"
                  flex={2}
                  Component={LargeImage} />
              </Table>
            </div>
          );
        }}
      </Query>
    );
  }
}

export default radium(
  BannerManage
);
