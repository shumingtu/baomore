import React, { PureComponent } from 'react';
import { Mutation } from 'react-apollo';

import {
  CREATE_LANDING,
} from '../../mutations/Landing.js';
import {
  FETCH_LANDINGS,
} from '../../queries/Landing.js';
import LandingBaseForm from './LandingBaseForm.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
};

class LandingCreateForm extends PureComponent {
  render() {
    return (
      <div style={styles.wrapper}>
        <Mutation
          mutation={CREATE_LANDING}
          update={(store, { data: { createLanding } }) => {
            const data = store.readQuery({
              query: FETCH_LANDINGS,
            });

            const {
              landings,
            } = data;

            if (createLanding) {
              store.writeQuery({
                query: FETCH_LANDINGS,
                data: {
                  landings: [
                    ...landings,
                    {
                      ...createLanding,
                    },
                  ],
                },
              });
            }
          }}>
          {(createLanding, { loading }) => (
            <LandingBaseForm
              {...this.props}
              mutate={createLanding} />
          )}
        </Mutation>
      </div>
    );
  }
}

export default LandingCreateForm;
