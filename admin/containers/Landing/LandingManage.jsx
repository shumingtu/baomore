// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';
// components
import Table, { TableField } from '../../components/Table/Table.jsx';
import LargeImage from '../../components/Table/Custom/LargeImage.jsx';
import Button from '../../components/Global/Button.jsx';
import LandingManageActions from '../../components/Table/Actions/LandingManageActions.jsx';
// config
import { FETCH_LANDINGS } from '../../queries/Landing.js';

const styles = {
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 35px)',
    maxHeight: 810,
    padding: '24px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
};

type Props = {
  history: {
    push: Function,
  },
  location: {
    pathname: string,
  },
};

class LandingManage extends PureComponent<Props> {
  render() {
    const {
      history,
      location: {
        pathname,
      },
    } = this.props;

    return (
      <Query query={FETCH_LANDINGS}>
        {({ data }) => {
          if (!data) return null;

          const {
            landings = [],
          } = data;

          return (
            <div style={styles.tableWrapper}>
              <Button
                label="新增大圖"
                onClick={() => history.push(`${pathname}/create`)} />
              <Table
                dataSource={landings.map(l => ({
                  id: l.id,
                  desktopImg: l.desktopImg,
                  mobileImg: l.mobileImg,
                }))}
                getActions={() => [
                  <LandingManageActions />,
                ]}
                actionTitles={['操作']}
                showPlaceholder={!landings.length}
                placeholder="尚無首頁大圖">
                <TableField
                  name="桌面版圖片"
                  fieldKey="desktopImg"
                  flex={1}
                  Component={LargeImage} />
                <TableField
                  name="手機版圖片"
                  fieldKey="mobileImg"
                  flex={1}
                  Component={LargeImage} />
              </Table>
            </div>
          );
        }}
      </Query>
    );
  }
}

export default radium(
  LandingManage
);
