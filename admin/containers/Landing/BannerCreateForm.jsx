import React, { PureComponent } from 'react';
import { Mutation } from 'react-apollo';

import {
  CREATE_BANNER,
} from '../../mutations/Landing.js';
import {
  FETCH_LANDING_BANNERS,
} from '../../queries/Landing.js';
import BannerBaseForm from './BannerBaseForm.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
};

class BannerCreateForm extends PureComponent {
  render() {
    return (
      <div style={styles.wrapper}>
        <Mutation
          mutation={CREATE_BANNER}
          update={(store, { data: { createBanner } }) => {
            const data = store.readQuery({
              query: FETCH_LANDING_BANNERS,
            });

            const {
              banners,
            } = data;

            if (createBanner) {
              store.writeQuery({
                query: FETCH_LANDING_BANNERS,
                data: {
                  banners: [
                    ...banners,
                    {
                      ...createBanner,
                    },
                  ],
                },
              });
            }
          }}>
          {(createBanner, { loading }) => (
            <BannerBaseForm
              {...this.props}
              mutate={createBanner} />
          )}
        </Mutation>
      </div>
    );
  }
}

export default BannerCreateForm;
