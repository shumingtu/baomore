// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import {
  Switch,
  Route,
} from 'react-router-dom';

import PageLinkSwitcher from '../../components/Global/PageLinkSwitcher.jsx';
import EmployeePerformanceBoard from './EmployeePerformanceBoard.jsx';
import StorePerformanceBoard from './StorePerformanceBoard.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
};

type Props = {
  location: {
    pathname: String,
  },
  history: Object,
};

const MY_PAGES = [{
  name: '包膜師業績',
  path: '/performance/employee',
}, {
  name: '地區門市業績',
  path: '/performance/store',
}, {
  name: '主任業績',
  path: '/performance/director',
}];

class PerformanceBoard extends PureComponent<Props> {
  componentDidMount() {
    const {
      location: {
        pathname,
      },
      history,
    } = this.props;

    if (pathname === '/performance') {
      history.replace('/performance/employee');
    }
  }

  componentDidUpdate(prevProps) {
    const {
      location: {
        pathname,
      },
      history,
    } = this.props;

    if (pathname !== prevProps.pathname && pathname === '/performance') {
      history.replace('/performance/employee');
    }
  }

  render() {
    return (
      <div style={styles.wrapper}>
        <PageLinkSwitcher pages={MY_PAGES} />
        <Switch>
          <Route exact path="/performance/employee" component={EmployeePerformanceBoard} />
          <Route exact path="/performance/director" component={EmployeePerformanceBoard} />
          <Route exact path="/performance/store" component={StorePerformanceBoard} />
        </Switch>
      </div>
    );
  }
}

export default radium(PerformanceBoard);
