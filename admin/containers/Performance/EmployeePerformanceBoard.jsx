// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import {
  Query,
} from 'react-apollo';

import Theme from '../../styles/Theme.js';

import {
  FETCH_EMPLOYEE_PERFORMANCE_FOR_ADMIN,
} from '../../queries/Performance.js';

import EmployeePerformanceSearcher from '../../components/Performance/EmployeePerformanceSearcher.jsx';
import TablePlaceholder from '../../components/Table/TablePlaceholder.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'calc(100% - 35px)',
    display: 'flex',
    flexDirection: 'column',
  },
  tableWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    height: 'calc(100% - 193px)',
    maxHeight: 810,
    overflow: 'auto',
  },
  tableFrozenHeaderWrapper: {
    height: 44,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    padding: '0 4px 0 0',
    borderBottom: `4px solid ${Theme.SECONDARY_THEME_COLOR}`,
  },
  tableItemField: {
    width: 75,
    margin: '0 8px',
    textAlign: 'center',
    fontSize: 14,
    fontWeight: 500,
    color: Theme.BLACK_COLOR,
  },
  tableContentWrap: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    minWidth: 1277,
    height: 'calc(100% - 44px)',
    overflow: 'auto',
  },
  tablePlacementWrap: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: 44,
    padding: '12px 0',
    margin: '0 0 8px 0',
    backgroundColor: '#fff',
    borderRadius: 2,
    border: 0,
    boxShadow: Theme.BLOCK_SHADOW,
  },
};

type Props = {
  location: Object,
  performanceSearchOptions: Object,
  performanceDirectorSearchOptions: Object,
};

class EmployeePerformanceBoard extends PureComponent<Props> {
  render() {
    const {
      location: {
        pathname,
      },
      performanceSearchOptions,
      performanceDirectorSearchOptions,
    } = this.props;

    const employeeType = pathname === '/performance/employee' ? '包膜師' : '主任包膜師';

    const searchOptions = employeeType === '包膜師' ? performanceSearchOptions : performanceDirectorSearchOptions;

    return (
      <Query
        variables={{
          ...searchOptions,
        }}
        query={FETCH_EMPLOYEE_PERFORMANCE_FOR_ADMIN}>
        {({ data, loading }) => {
          if (!data) return null;
          const {
            employeePerformanceForAdmin = [],
          } = data;

          return (
            <div style={styles.wrapper}>
              <EmployeePerformanceSearcher employeeType={employeeType} />
              <div
                style={styles.tableWrapper}>
                <div style={styles.tableFrozenHeaderWrapper}>
                  <span style={styles.tableItemField}>區域</span>
                  <span style={styles.tableItemField}>包膜師</span>
                  <span style={styles.tableItemField}>膜料總量</span>
                  <span style={styles.tableItemField}>保貼總量</span>
                  <span style={styles.tableItemField}>合計總量</span>
                  <span style={styles.tableItemField}>膜料總額</span>
                  <span style={styles.tableItemField}>保貼總額</span>
                  <span style={styles.tableItemField}>合計總額</span>
                  <span style={styles.tableItemField}>膜料達成率</span>
                  <span style={styles.tableItemField}>保貼達成率</span>
                  <span style={styles.tableItemField}>總達成率</span>
                  <span style={styles.tableItemField}>膜料週轉率</span>
                  <span style={styles.tableItemField}>保貼週轉率</span>
                  <span style={styles.tableItemField}>總週轉率</span>
                </div>
                <div
                  className="hideScrollBar"
                  style={styles.tableContentWrap}>
                  {loading ? <TablePlaceholder placeholder="讀取中.." /> : (
                    <>
                      {employeePerformanceForAdmin.length ? employeePerformanceForAdmin.map(x => (
                        <div key={x.id} style={styles.tablePlacementWrap}>
                          <span style={styles.tableItemField}>{x.area.name}</span>
                          <span style={styles.tableItemField}>{x.name}</span>
                          <span style={styles.tableItemField}>{x.amounts[0]}</span>
                          <span style={styles.tableItemField}>{x.amounts[1]}</span>
                          <span style={styles.tableItemField}>{x.amounts[2]}</span>
                          <span style={styles.tableItemField}>{x.sales[0]}</span>
                          <span style={styles.tableItemField}>{x.sales[1]}</span>
                          <span style={styles.tableItemField}>{x.sales[2]}</span>
                          <span style={styles.tableItemField}>{`${Math.round(x.achieveRate[0])}%`}</span>
                          <span style={styles.tableItemField}>{`${Math.round(x.achieveRate[1])}%`}</span>
                          <span style={styles.tableItemField}>{`${Math.round(x.achieveRate[2])}%`}</span>
                          <span style={styles.tableItemField}>{x.turnover[0]}</span>
                          <span style={styles.tableItemField}>{x.turnover[1]}</span>
                          <span style={styles.tableItemField}>{x.turnover[2]}</span>
                        </div>
                      )) : <TablePlaceholder />}
                    </>
                  )}
                </div>
              </div>
            </div>
          );
        }}
      </Query>
    );
  }
}

const reduxHook = connect(
  state => ({
    performanceSearchOptions: state.Search.adminPerformance,
    performanceDirectorSearchOptions: state.Search.adminDirectorPerformance,
  }),
);

export default reduxHook(
  radium(EmployeePerformanceBoard)
);
