// @flow
import React, { PureComponent, Fragment } from 'react';
import { connect } from 'react-redux';
import radium from 'radium';
import { Query } from 'react-apollo';

// config
import {
  FORM_ROLLBACK_DOA_SEARCH_FORM,
} from '../../shared/form.js';
import {
  CACHE_ADMIN_ROLLBACK_DOA_SEARCH,
} from '../../actions/Search.js';
import {
  FETCH_INVENTORY_ROLLBACKS,
} from '../../queries/Inventory.js';
import { rollbackTableDataFormatter } from '../../helper/rollback.js';
// components
import { wrapFormToRollbackSearchForm } from './RollbackSearchForm.jsx';
import Table, { TableField } from '../../components/Table/Table.jsx';
import TableCheckbox from '../../components/Table/Custom/TableCheckbox.jsx';
import TableActionBar from '../../components/Rollback/TableActionBar.jsx';

const DOASearchForm = wrapFormToRollbackSearchForm(FORM_ROLLBACK_DOA_SEARCH_FORM);

const styles = {
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 242px)',
    maxHeight: 810,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
};

type Props = {
  doaOptions: {
    rollbackType: string,
    startDate: string,
    endDate: string,
    errorType: string,
    pnCategoryId: number,
    code: string,
    memberId: number,
  },
};

type State = {
  checkedList: Array<number>,
};

const LIMIT_LENGTH = 10;

class RollbackDoaMainBoard extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      checkedList: [],
    };
  }

  setCheckedList(value) {
    const {
      checkedList,
    } = this.state;

    if (!value) return null;

    const existValueIdx = checkedList.findIndex(v => v === value);

    if (~existValueIdx) {
      return this.setState({
        checkedList: [
          ...checkedList.slice(0, existValueIdx),
          ...checkedList.slice(existValueIdx + 1),
        ],
      });
    }

    return this.setState({
      checkedList: [
        ...checkedList,
        value,
      ],
    });
  }

  render() {
    const {
      doaOptions,
    } = this.props;

    const {
      checkedList,
    } = this.state;

    const queryOptions = {
      rollbackType: 'DOA',
      offset: 0,
      limit: LIMIT_LENGTH,
    };

    if (doaOptions) {
      const {
        rollbackType,
        startDate,
        endDate,
        errorType,
        pnCategoryId,
        code,
        memberId,
      } = doaOptions;

      if (rollbackType) queryOptions.rollbackType = rollbackType;
      if (startDate) queryOptions.startDate = startDate;
      if (endDate) queryOptions.endDate = endDate;
      if (errorType) queryOptions.errorType = errorType;
      if (pnCategoryId) queryOptions.pnCategoryId = parseInt(pnCategoryId, 10);
      if (code) queryOptions.code = code;
      if (memberId) queryOptions.memberId = parseInt(memberId, 10);
    }

    const wrapCacheOptions = doaOptions ? {
      rollbackType: 'DOA',
      ...doaOptions,
    } : {
      rollbackType: 'DOA',
    };

    return (
      <Fragment>
        <DOASearchForm
          fieldNames={[
            'startDate',
            'endDate',
            'errorType',
            'pnCategoryId',
            'code',
            'memberId',
          ]}
          actionType={CACHE_ADMIN_ROLLBACK_DOA_SEARCH}
          initialValue={wrapCacheOptions} />
        <Query
          query={FETCH_INVENTORY_ROLLBACKS}
          variables={queryOptions}>
          {({ data, fetchMore }) => {
            if (!data) return null;

            const {
              behaviorRecordList = [],
            } = data;

            const dataSource = rollbackTableDataFormatter(behaviorRecordList);

            return (
              <div style={styles.tableWrapper}>
                <TableActionBar
                  dataSource={dataSource.filter(d => !d.approverName)}
                  checkedList={checkedList}
                  onOverride={list => this.setState({ checkedList: list })} />
                <Table
                  dataSource={dataSource}
                  fetchMore={() => fetchMore({
                    variables: {
                      ...queryOptions,
                      offset: 0,
                      limit: behaviorRecordList.length + LIMIT_LENGTH,
                    },
                    updateQuery: (prev, { fetchMoreResult }) => {
                      if (!fetchMoreResult) return prev;

                      return {
                        ...prev,
                        behaviorRecordList: [
                          ...fetchMoreResult.behaviorRecordList,
                        ],
                      };
                    },
                  })}
                  showPlaceholder={!behaviorRecordList.length}
                  placeholder="查無相關資料">
                  <TableField
                    name="審核"
                    fieldKey="approverName"
                    flex={0.3}
                    isCenter
                    checkedList={checkedList}
                    onChange={v => this.setCheckedList(v)}
                    Component={TableCheckbox} />
                  <TableField name="商品類型" fieldKey="pnCategoryName" flex={1} isCenter />
                  <TableField name="料號" fieldKey="pnTableCode" flex={1} isCenter />
                  <TableField name="異常類型" fieldKey="errorType" flex={1} isCenter />
                  <TableField name="照片" fieldKey="picture" flex={1} isImage isCenter />
                  <TableField name="包膜師" fieldKey="memberName" flex={1} isCenter />
                  <TableField name="回報時間" fieldKey="createdAt" flex={1} inCenter />
                </Table>
              </div>
            );
          }}
        </Query>
      </Fragment>
    );
  }
}

const reduxHook = connect(
  state => ({
    doaOptions: state.Search.adminRollbackDoa,
  }),
);

export default reduxHook(
  radium(
    RollbackDoaMainBoard
  )
);
