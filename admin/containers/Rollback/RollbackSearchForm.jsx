// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Fields,
  Field,
  initialize,
  formValueSelector,
} from 'redux-form';
import qs from 'qs';

import * as SearchActions from '../../actions/Search.js';
// components
import PNTableWithErrorSelectorBind from '../Form/PNTableWithErrorSelectorBind.jsx';
import StoreEmployeeSelectorBind from '../Form/StoreEmployeeSelectorBind.jsx';
import Input from '../../components/Form/Input.jsx';
import SubmitButton from '../../components/Form/SubmitButton.jsx';
import Button from '../../components/Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  searchWrapper: {
    width: '100%',
    maxWidth: 860,
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: '12px 0',
  },
  fieldWrapper: {
    width: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    margin: '0 5px',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 24px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
};

type Props = {
  onSearch: Function,
  handleSubmit: Function,
  initializeForm: Function,
  cacheSearchOptions: Function,
  actionType?: string,
  fieldNames: Array<string>,
  initialValue?: {
    rollbackType: string,
    pnCategoryId?: number,
    startDate?: string,
    endDate?: string,
    code?: string,
    memberId?: number,
    errorType?: string,
    target?: string,
    storeId?: number,
    vat?: string,
    customerName?: string,
    customerPhone?: string,
  },
  doaOptions: object,
  pullofOptions: object,
  socialOptions: object,
  customerOptions: object,
  rollbackType: string,
};

class RollbackSearchForm extends PureComponent<Props> {
  static defaultProps = {
    initialValue: null,
    actionType: null,
  };

  componentDidMount() {
    const {
      initialValue,
    } = this.props;

    this.initializing(initialValue);
  }

  componentDidUpdate(prevProps) {
    const {
      initialValue,
    } = this.props;

    if (initialValue && initialValue !== prevProps.initialValue) {
      this.initializing(initialValue);
    }
  }

  getExportOptions = () => {
    const {
      doaOptions,
      pullofOptions,
      socialOptions,
      customerOptions,
      rollbackType,
    } = this.props;

    switch (rollbackType) {
      case 'DOA':
        return {
          options: doaOptions,
          path: 'doa',
        };

      case 'PULLOF':
        return {
          options: pullofOptions,
          path: 'pullof',
        };

      case 'SOCIAL':
        return {
          options: socialOptions,
          path: 'social',
        };

      case 'CUSTOMERCOMPLAINT':
        return {
          options: customerOptions,
          path: 'customerComplaint',
        };

      default:
        return {
          options: doaOptions,
          path: 'doa',
        };
    }
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    initializeForm({
      rollbackType: (obj && obj.rollbackType) || null,
      pnCategoryId: (obj && obj.pnCategoryId) || '-1',
      startDate: (obj && obj.startDate) || null,
      endDate: (obj && obj.endDate) || null,
      code: (obj && obj.code) || null,
      memberId: (obj && obj.memberId) || '-1',
      errorType: (obj && obj.errorType) || '-1',
      target: (obj && obj.target) || null,
      storeId: (obj && obj.storeId) || '-1',
      vat: (obj && obj.vat) || null,
      customerName: (obj && obj.customerName) || null,
      customerPhone: (obj && obj.customerPhone) || null,
    });
  }

  submit(d) {
    const {
      onSearch,
      actionType,
      cacheSearchOptions,
    } = this.props;

    const payload = {
      rollbackType: d.rollbackType || null,
      pnCategoryId: d.pnCategoryId && d.pnCategoryId !== '-1' ? parseInt(d.pnCategoryId, 10) : null,
      startDate: d.startDate || null,
      endDate: d.endDate || null,
      code: d.code || null,
      memberId: d.memberId && d.memberId !== '-1' ? parseInt(d.memberId, 10) : null,
      errorType: d.errorType && d.errorType !== '-1' ? d.errorType : null,
      target: d.target || null,
      storeId: d.storeId && d.storeId !== '-1' ? parseInt(d.storeId, 10) : null,
      vat: d.vat || null,
      customerName: d.customerName || null,
      customerPhone: d.customerPhone || null,
    };

    if (onSearch) {
      onSearch(payload);
    }

    if (actionType) {
      cacheSearchOptions(actionType, payload);
    }
  }

  render() {
    const {
      handleSubmit,
      fieldNames = [],
    } = this.props;

    if (!fieldNames || !Array.isArray(fieldNames)) return null;

    const needStartDate = fieldNames.find(n => n === 'startDate') || false;
    const needEndDate = fieldNames.find(n => n === 'endDate') || false;
    const needPNCategory = fieldNames.find(n => n === 'pnCategoryId') || false;
    const needCode = fieldNames.find(n => n === 'code') || false;
    const needMember = fieldNames.find(n => n === 'memberId') || false;
    const needErrorType = fieldNames.find(n => n === 'errorType') || false;
    const needTarget = fieldNames.find(n => n === 'target') || false;
    const needStoreId = fieldNames.find(n => n === 'storeId') || false;
    const needVat = fieldNames.find(n => n === 'vat') || false;
    const needCustomerName = fieldNames.find(n => n === 'customerName') || false;
    const needCustomerPhone = fieldNames.find(n => n === 'customerPhone') || false;

    return (
      <div style={styles.wrapper}>
        <form
          onSubmit={handleSubmit(d => this.submit(d))}
          style={styles.searchWrapper}>
          {needStartDate ? (
            <div style={styles.fieldWrapper}>
              <Field
                name="startDate"
                label="開始時間:"
                type="date"
                component={Input} />
            </div>
          ) : null}
          {needEndDate ? (
            <div style={styles.fieldWrapper}>
              <Field
                name="endDate"
                label="結束時間:"
                type="date"
                component={Input} />
            </div>
          ) : null}
          {needPNCategory ? (
            <div style={styles.fieldWrapper}>
              <Fields
                names={['pnCategoryId'].concat(needErrorType ? ['errorType'] : [])}
                component={PNTableWithErrorSelectorBind} />
            </div>
          ) : null}
          {needCode ? (
            <div style={styles.fieldWrapper}>
              <Field
                name="code"
                label="料號:"
                placeholder="請輸入料號"
                component={Input} />
            </div>
          ) : null}
          {needTarget ? (
            <div style={styles.fieldWrapper}>
              <Field
                name="target"
                label="對象:"
                placeholder="請輸入對象"
                component={Input} />
            </div>
          ) : null}
          {needVat ? (
            <div style={styles.fieldWrapper}>
              <Field
                name="vat"
                label="發票號碼:"
                placeholder="請輸入發票號碼"
                component={Input} />
            </div>
          ) : null}
          {needCustomerName ? (
            <div style={styles.fieldWrapper}>
              <Field
                name="customerName"
                label="客戶姓名:"
                placeholder="請輸入客戶姓名"
                component={Input} />
            </div>
          ) : null}
          {needCustomerPhone ? (
            <div style={styles.fieldWrapper}>
              <Field
                name="customerPhone"
                label="客戶電話:"
                placeholder="請輸入手機號碼"
                component={Input} />
            </div>
          ) : null}
          {needMember ? (
            <div style={styles.fieldWrapper}>
              <Fields
                disableRequirement
                names={['memberId'].concat(needStoreId ? ['storeId'] : [])}
                component={StoreEmployeeSelectorBind} />
            </div>
          ) : null}
          <div style={styles.functionWrapper}>
            <SubmitButton style={styles.searchBtn} label="查詢" />
            <Button
              style={{ margin: '0px 0px 0px 5px' }}
              onClick={() => {
                const {
                  options,
                  path,
                } = this.getExportOptions();

                const queryString = qs.stringify(qs.parse(options), { skipNulls: true });
                window.open(`${API_HOST}/export/rollback/${path}?${queryString}&token=${localStorage.accessToken}`);
              }}
              label="匯出" />
          </div>
        </form>
      </div>
    );
  }
}

export default null;

export function wrapFormToRollbackSearchForm(form) {
  if (!form) return null;

  const selector = formValueSelector(form);

  return connect(
    state => ({
      doaOptions: state.Search.adminRollbackDoa,
      pullofOptions: state.Search.adminRollbackPullof,
      socialOptions: state.Search.adminRollbackSocial,
      customerOptions: state.Search.adminRollbackCustomer,
      rollbackType: selector(state, 'rollbackType'),
    }),
    dispatch => bindActionCreators({
      ...SearchActions,
      initializeForm: v => initialize(form, v),
    }, dispatch),
  )(reduxForm({
    form,
  })(radium(RollbackSearchForm)));
}
