// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import {
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

// config
// components
import PageLinkSwitcher from '../../components/Global/PageLinkSwitcher.jsx';
import RollbackDoaMainBoard from './RollbackDoaMainBoard.jsx';
import RollbackPullofMainBoard from './RollbackPullofMainBoard.jsx';
import RollbackSocialMainBoard from './RollbackSocialMainBoard.jsx';
import RollbackCustomerMainBoard from './RollbackCustomerMainBoard.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    minWidth: 860,
    position: 'relative',
  },
  linkSwitcherWrapper: {
    width: '100%',
    height: 'auto',
  },
};

type Props = {
  match: {
    url: string,
  },
};

const pageLinks = [{
  path: '/rollback/doa',
  name: 'DOA',
}, {
  path: '/rollback/pullof',
  name: '下架退回',
}, {
  path: '/rollback/social',
  name: '公關銷帳',
}, {
  path: '/rollback/customerComplaint',
  name: '客訴退錢',
}];

class RollbackMainBoard extends PureComponent<Props> {
  render() {
    const {
      match: {
        url,
      },
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.linkSwitcherWrapper}>
          <PageLinkSwitcher pages={pageLinks} />
        </div>
        <Switch>
          <Route
            path={`${url}/doa`}
            component={RollbackDoaMainBoard} />
          <Route
            path={`${url}/pullof`}
            component={RollbackPullofMainBoard} />
          <Route
            path={`${url}/social`}
            component={RollbackSocialMainBoard} />
          <Route
            path={`${url}/customerComplaint`}
            component={RollbackCustomerMainBoard} />
          <Redirect to={{ pathname: `${url}/doa` }} />
        </Switch>
      </div>
    );
  }
}

export default radium(RollbackMainBoard);
