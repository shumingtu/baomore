// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
// component
import BrandListManage from '../../components/PhoneModel/BrandListManage.jsx';
import ModelListManage from '../../components/PhoneModel/ModelListManage.jsx';
import ColorListManage from '../../components/PhoneModel/ColorListManage.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
  },
};

type Props = {

};

class ManageBoard extends PureComponent<Props> {
  render() {
    return (
      <div style={styles.wrapper}>
        <BrandListManage />
        <ModelListManage />
        <ColorListManage />
      </div>
    );
  }
}

export default radium(
  ManageBoard
);
