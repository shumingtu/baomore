// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';
// components
import PhoneColorManageActions from '../../components/Table/Actions/PhoneColorManageActions.jsx';
import Table, { TableField } from '../../components/Table/Table.jsx';
// config
import { FETCH_ADMIN_PHONE_LIST } from '../../queries/Brand.js';

const styles = {
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 188px)',
    maxHeight: 810,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
};

type Props = {
  options: {
    brandId: string,
    modelId: string,
    colorId: string,
  },
};

const PHONE_LIST_LIMIT = 10;

class PhoneList extends PureComponent<Props> {
  render() {
    const {
      options,
    } = this.props;

    const queryOptions: {
      brandId?: number,
      modelId?: number,
      colorId?: number,
      limit: number,
      offset: number,
    } = {
      limit: 10,
      offset: 0,
    };

    if (options && parseInt(options.brandId, 10) !== -1) queryOptions.brandId = parseInt(options.brandId, 10);
    if (options && parseInt(options.modelId, 10) !== -1) queryOptions.modelId = parseInt(options.modelId, 10);
    if (options && parseInt(options.colorId, 10) !== -1) queryOptions.colorId = parseInt(options.colorId, 10);

    return (
      <Query
        query={FETCH_ADMIN_PHONE_LIST}
        variables={queryOptions}>
        {({
          data: {
            adminBrandModelColorList = [],
          },
          fetchMore,
        }) => (
          <div style={styles.tableWrapper}>
            <Table
              dataSource={adminBrandModelColorList.map(c => ({
                id: c.id,
                brand: (c.brand && c.brand.name) || null,
                model: (c.model && c.model.name) || null,
                assignModelId: (c.model && c.model.id) || -1,
                color: c.name || null,
                phoneBg: c.phoneBg || null,
                phoneMask: c.phoneMask || null,
                phoneCover: c.phoneCover || null,
              }))}
              fetchMore={() => fetchMore({
                variables: {
                  ...queryOptions,
                  offset: 0,
                  limit: adminBrandModelColorList.length + PHONE_LIST_LIMIT,
                },
                updateQuery: (prev, { fetchMoreResult }) => {
                  if (!fetchMoreResult) return prev;

                  return {
                    ...prev,
                    adminBrandModelColorList: [
                      ...fetchMoreResult.adminBrandModelColorList,
                    ],
                  };
                },
              })}
              showPlaceholder={!adminBrandModelColorList.length}
              placeholder="查無資料"
              actionTitles={['操作']}
              getActions={() => [
                <PhoneColorManageActions
                  id={-1}
                  name=""
                  assignModelId={-1} />,
              ]}>
              <TableField name="手機品牌" fieldKey="brand" flex={1} isCenter />
              <TableField name="手機型號" fieldKey="model" flex={1} isCenter />
              <TableField name="手機顏色" fieldKey="color" flex={1} isCenter />
              <TableField name="底圖" fieldKey="phoneBg" flex={1} isImage />
              <TableField name="遮罩圖" fieldKey="phoneMask" flex={1} isImage needBlackBg />
              <TableField name="覆蓋圖" fieldKey="phoneCover" flex={1} isImage />
            </Table>
          </div>
        )}
      </Query>
    );
  }
}

const reduxHook = connect(
  state => ({
    options: state.PhoneModel.listSearchOptions,
  }),
);

export default reduxHook(
  radium(
    PhoneList
  )
);
