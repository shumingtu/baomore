import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
// components
import PageSwitcher from '../../components/Global/PageSwitcher.jsx';
import PhoneList from './PhoneList.jsx';
import PhoneModelListSearcher from '../../components/PhoneModel/PhoneModelListSearcher.jsx';
import ManageBoard from './ManageBoard.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    overflow: 'auto',
  },
};

class PhoneModelBoard extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      currentPageIdx: 0,
    };
  }

  pageSwitcher() {
    const {
      currentPageIdx,
    } = this.state;

    switch (currentPageIdx) {
      case 0:
        return (
          <Fragment>
            <PhoneModelListSearcher />
            <PhoneList />
          </Fragment>
        );
      case 1:
        return (
          <ManageBoard />
        );
      default:
        return null;
    }
  }

  render() {
    const {
      currentPageIdx,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        <PageSwitcher
          pages={['手機資料列表', '品牌型號管理']}
          currentPageIdx={currentPageIdx}
          onChange={i => this.setState({ currentPageIdx: i })} />
        {this.pageSwitcher()}
      </div>
    );
  }
}

export default radium(PhoneModelBoard);
