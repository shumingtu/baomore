// @flow
import React, { Component } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  formValueSelector,
  change,
  SubmissionError,
  initialize,
} from 'redux-form';
import {
  Query,
  Mutation,
} from 'react-apollo';

import { validatePhoneEdit } from '../../helper/validator/PhoneElementEdit.js';

// config
import TextStyles from '../../styles/Text.js';
import {
  FETCH_BRAND_LIST,
  FETCH_BRAND_MODEL_LIST,
  FETCH_ADMIN_PHONE_LIST,
} from '../../queries/Brand.js';
import {
  CREATE_BRAND_MODEL_COLOR,
} from '../../mutations/Brand.js';
import { FORM_PHONE_COLOR_CREATE_FORM } from '../../shared/form.js';

// components
import Selector from '../../components/Form/Selector.jsx';
import SubmitButton from '../../components/Form/SubmitButton.jsx';
import Button from '../../components/Global/Button.jsx';
import FileInput from '../../components/Form/FileInput.jsx';
import Input from '../../components/Form/Input.jsx';
import DeleteButton from '../../components/Global/DeleteButton.jsx';

const selector = formValueSelector(FORM_PHONE_COLOR_CREATE_FORM);

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
  title: {
    ...TextStyles.labelText,
    width: '100%',
    height: 'auto',
    textAlign: 'center',
    fontSize: 18,
    padding: '24px 0',
  },
  mainForm: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formWrapper: {
    width: 630,
    height: 'auto',
    padding: '16px 32px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    border: '1px solid rgb(182, 184, 204)',
  },
  formGroup: {
    width: '100%',
    margin: 10,
  },
  selectorWrap: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    width: 'auto',
    flexDirection: 'column',
  },
  selector: {
    width: 'auto',
    height: 'auto',
    padding: 4,
  },
  imgGroup: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  imgItemWrap: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgLabel: {
    ...TextStyles.text,
    margin: '0 5px 0 0',
  },
  functionWrapper: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    margin: '30px 0 5px 0',
  },
  editBtn: {
    width: 88,
    height: 32,
    backgroundColor: 'rgb(182, 184, 204)',
    margin: '0 3px',
  },
  cancelBtn: {
    width: 88,
    height: 32,
    margin: '0 3px',
    backgroundColor: 'white',
    border: '1px solid rgb(182, 184, 204)',
    color: 'rgb(182, 184, 204)',
  },
  imgFuncWrap: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '0 0 5px 0',
  },
};

type Props = {
  options: {
    brandId: string,
    modelId: string,
    colorId: string,
  },
  history: Object,
  initializeForm: Function,
  setModelId: Function,
  selectedBrandId: number,
  handleSubmit: Function,
  setImgNull: Function,
  phoneBg: String,
  phoneCover: String,
  phoneMask: String,
  match: Object,
};

class PhoneElementCreatePage extends Component<Props> {
  componentDidMount() {
    const {
      match: {
        params: {
          modelId,
          brandId,
        },
      },
      initializeForm,
    } = this.props;

    initializeForm({
      modelId,
      brandId,
    });
  }

  componentDidUpdate(prevProps) {
    const {
      selectedBrandId,
      setModelId,
    } = this.props;

    if (prevProps.selectedBrandId && prevProps.selectedBrandId !== selectedBrandId) {
      setModelId(-1);
    }
  }

  render() {
    const {
      history,
      selectedBrandId,
      handleSubmit,
      options,
      setImgNull,
      phoneBg,
      phoneCover,
      phoneMask,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.title}>
          新增手機顏色
        </div>
        <div style={styles.mainForm}>
          <Mutation
            mutation={CREATE_BRAND_MODEL_COLOR}
            update={(cache, { data: { createBrandModelColor } }) => {
              const queryOptions: {
                brandId?: number,
                modelId?: number,
                colorId?: number,
                limit: number,
                offset: number,
              } = {
                limit: 10,
                offset: 0,
              };

              if (options && options.brandId !== '-1') queryOptions.brandId = parseInt(options.brandId, 10);
              if (options && options.modelId !== '-1') queryOptions.modelId = parseInt(options.modelId, 10);
              if (options && options.colorId !== '-1') queryOptions.colorId = parseInt(options.colorId, 10);

              const data = cache.readQuery({
                query: FETCH_ADMIN_PHONE_LIST,
                variables: queryOptions,
              });

              const {
                adminBrandModelColorList,
              } = data;

              if (createBrandModelColor) {
                cache.writeQuery({
                  query: FETCH_ADMIN_PHONE_LIST,
                  variables: queryOptions,
                  data: {
                    adminBrandModelColorList: [
                      ...adminBrandModelColorList,
                      {
                        ...createBrandModelColor,
                      },
                    ],
                  },
                });
              }
            }}>
            {createBrandModelColor => (
              <form
                style={styles.formWrapper}
                onSubmit={handleSubmit(async (data) => {
                  const {
                    colorName,
                    modelId,
                    brandId,
                  } = data;

                  const error = validatePhoneEdit(data, brandId);

                  if (error) {
                    throw new SubmissionError(error);
                  }


                  const payload = {
                    name: colorName,
                    phoneBg,
                    phoneCover,
                    phoneMask,
                    modelId: parseInt(modelId, 10),
                  };

                  const {
                    data: {
                      createBrandModelColor: {
                        id,
                      },
                    },
                  } = await createBrandModelColor({ variables: payload });

                  if (id) {
                    history.push('/phoneModel');
                  }
                })}>
                <div style={styles.formGroup}>
                  <div style={styles.selectorWrap}>
                    <div style={styles.selector}>
                      <Query query={FETCH_BRAND_LIST}>
                        {({
                          data: {
                            brandList = [],
                          },
                        }) => (
                          <Field
                            name="brandId"
                            label="手機品牌:"
                            nullable
                            placeholder="請選擇"
                            options={brandList}
                            component={Selector} />
                        )}
                      </Query>
                    </div>
                    <div style={styles.selector}>
                      <Query
                        query={FETCH_BRAND_MODEL_LIST}
                        variables={{
                          brandId: (selectedBrandId && parseInt(selectedBrandId, 10)) || -1,
                        }}>
                        {({
                          data: {
                            brandModelList = [],
                          },
                        }) => (
                          <Field
                            name="modelId"
                            label="手機型號:"
                            nullable
                            placeholder="請選擇"
                            options={brandModelList}
                            component={Selector} />
                        )}
                      </Query>
                    </div>
                    <div style={styles.selector}>
                      <Field
                        name="colorName"
                        label="手機顏色:"
                        component={Input} />
                    </div>
                  </div>
                </div>
                <div style={styles.formGroup}>
                  <div style={styles.imgGroup}>
                    <div style={styles.imgItemWrap}>
                      <div style={styles.imgFuncWrap}>
                        <p style={styles.imgLabel}>底圖</p>
                        {phoneBg ? (
                          <DeleteButton onDelete={() => setImgNull('phoneBg')} />
                        ) : null}
                      </div>
                      <Field
                        name="phoneBg"
                        component={FileInput} />
                    </div>
                    <div style={styles.imgItemWrap}>
                      <div style={styles.imgFuncWrap}>
                        <p style={styles.imgLabel}>遮罩圖</p>
                        {phoneMask ? (
                          <DeleteButton onDelete={() => setImgNull('phoneMask')} />
                        ) : null}
                      </div>
                      <Field
                        name="phoneMask"
                        component={FileInput} />
                    </div>
                    <div style={styles.imgItemWrap}>
                      <div style={styles.imgFuncWrap}>
                        <p style={styles.imgLabel}>覆蓋圖</p>
                        {phoneCover ? (
                          <DeleteButton onDelete={() => setImgNull('phoneCover')} />
                        ) : null}
                      </div>
                      <Field
                        name="phoneCover"
                        component={FileInput} />
                    </div>
                  </div>
                </div>
                <div style={styles.functionWrapper}>
                  <SubmitButton style={styles.editBtn} label="確定新增" />
                  <Button onClick={() => history.goBack()} style={styles.cancelBtn} label="取消" />
                </div>
              </form>
            )}
          </Mutation>
        </div>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_PHONE_COLOR_CREATE_FORM,
});

const reduxHook = connect(
  state => ({
    selectedBrandId: selector(state, 'brandId'),
    phoneBg: selector(state, 'phoneBg'),
    phoneCover: selector(state, 'phoneCover'),
    phoneMask: selector(state, 'phoneMask'),
    options: state.PhoneModel.listSearchOptions,
  }),
  dispatch => bindActionCreators({
    initializeForm: d => initialize(FORM_PHONE_COLOR_CREATE_FORM, d),
    setModelId: v => change(FORM_PHONE_COLOR_CREATE_FORM, 'modelId', v),
    setImgNull: type => change(FORM_PHONE_COLOR_CREATE_FORM, type, null),
  }, dispatch),
);

export default reduxHook(
  formHook(
    radium(
      PhoneElementCreatePage
    )
  )
);
