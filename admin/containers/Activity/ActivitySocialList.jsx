// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';
// components
import Table, { TableField } from '../../components/Table/Table.jsx';
import PageTitle from '../../components/Global/PageTitle.jsx';
import ActivitySocialCategoryActions from '../../components/Table/Actions/ActivitySocialCategoryActions.jsx';

import { FETCH_SOCIAL_ACTIVITY_CATEGORIES } from '../../queries/Activity.js';

const styles = {
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 35px)',
    maxHeight: 810,
    padding: '24px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
};

type Props = {
};

class ActivitySocialList extends PureComponent<Props> {
  render() {
    return (
      <Query query={FETCH_SOCIAL_ACTIVITY_CATEGORIES}>
        {({ data }) => {
          if (!data) return null;

          const {
            socialCategories = [],
          } = data;

          return (
            <div style={styles.tableWrapper}>
              <PageTitle title="社群活動分類列表" />
              <Table
                dataSource={socialCategories.map(l => ({
                  id: l.id,
                  name: l.name,
                  link: l.link,
                }))}
                showPlaceholder={!socialCategories.length}
                getActions={() => [
                  <ActivitySocialCategoryActions />,
                ]}
                actionTitles={['操作']}
                placeholder="尚無活動分類">
                <TableField
                  name="名稱"
                  fieldKey="name"
                  flex={1}
                  isCenter />
                <TableField
                  name="連結"
                  fieldKey="link"
                  flex={1} />
              </Table>
            </div>
          );
        }}
      </Query>
    );
  }
}

export default radium(
  ActivitySocialList
);
