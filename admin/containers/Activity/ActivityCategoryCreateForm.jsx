import React, { PureComponent } from 'react';
import { Mutation } from 'react-apollo';

import {
  CREATE_ACTIVITY_CATEGORY,
} from '../../mutations/Activity.js';
import {
  FETCH_ACTIVITY_CATEGORIES,
} from '../../queries/Activity.js';
import ActivityCategoryBaseForm from '../../components/Activity/ActivityCategoryBaseForm.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
};

class ActivityCategoryCreateForm extends PureComponent {
  render() {
    return (
      <div style={styles.wrapper}>
        <Mutation
          mutation={CREATE_ACTIVITY_CATEGORY}
          update={(store, { data: { createActivityCategory } }) => {
            const data = store.readQuery({
              query: FETCH_ACTIVITY_CATEGORIES,
            });

            const {
              activityCategories,
            } = data;

            if (createActivityCategory) {
              store.writeQuery({
                query: FETCH_ACTIVITY_CATEGORIES,
                data: {
                  activityCategories: [
                    ...activityCategories,
                    {
                      ...createActivityCategory,
                    },
                  ],
                },
              });
            }
          }}>
          {createActivityCategory => (
            <ActivityCategoryBaseForm
              {...this.props}
              mutate={createActivityCategory} />
          )}
        </Mutation>
      </div>
    );
  }
}

export default ActivityCategoryCreateForm;
