// @flow
import React, { PureComponent } from 'react';
import {
  Query,
  Mutation,
} from 'react-apollo';

import {
  FETCH_ACTIVITY_CATEGORY,
} from '../../queries/Activity.js';
import {
  EDIT_ACTIVITY_CATEGORY,
} from '../../mutations/Activity.js';
import ActivityCategoryBaseForm from '../../components/Activity/ActivityCategoryBaseForm.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
};

type Props = {
  match: {
    params: {
      categoryId: string,
    },
  },
};

class ActivityCategoryEditForm extends PureComponent<Props> {
  render() {
    const {
      match: {
        params: {
          categoryId,
        },
      },
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <Query
          query={FETCH_ACTIVITY_CATEGORY}
          fetchPolicy="cache-and-network"
          variables={{
            categoryId: parseInt(categoryId, 10),
          }}>
          {({
            data,
          }) => {
            const wrapCategory = (data && data.activityCategory) || null;

            return (
              <Mutation mutation={EDIT_ACTIVITY_CATEGORY}>
                {editActivityCategory => (
                  <ActivityCategoryBaseForm
                    {...this.props}
                    mutate={editActivityCategory}
                    data={wrapCategory} />
                )}
              </Mutation>
            );
          }}
        </Query>
      </div>
    );
  }
}

export default ActivityCategoryEditForm;
