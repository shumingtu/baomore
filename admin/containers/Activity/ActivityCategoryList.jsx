// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';
// components
import Table, { TableField } from '../../components/Table/Table.jsx';
import LargeImage from '../../components/Table/Custom/LargeImage.jsx';
import Button from '../../components/Global/Button.jsx';
import PageTitle from '../../components/Global/PageTitle.jsx';
import ActivityCategoryManageActions from '../../components/Table/Actions/ActivityCategoryManageActions.jsx';
// config
import { FETCH_ACTIVITY_CATEGORIES } from '../../queries/Activity.js';

const styles = {
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 35px)',
    maxHeight: 810,
    padding: '24px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
};

type Props = {
  history: {
    push: Function,
  },
  location: {
    pathname: string,
  },
};

class ActivityCategoryList extends PureComponent<Props> {
  render() {
    const {
      history,
      location: {
        pathname,
      },
    } = this.props;

    return (
      <Query query={FETCH_ACTIVITY_CATEGORIES}>
        {({ data }) => {
          if (!data) return null;

          const {
            activityCategories = [],
          } = data;

          return (
            <div style={styles.tableWrapper}>
              <PageTitle title="官方活動分類列表" />
              <Button
                label="新增活動分類"
                onClick={() => history.push(`${pathname}/create`)} />
              <Table
                dataSource={activityCategories.map(l => ({
                  id: l.id,
                  name: l.name,
                  desktopImg: l.desktopImg,
                  mobileImg: l.mobileImg,
                }))}
                getActions={() => [
                  <ActivityCategoryManageActions />,
                ]}
                actionTitles={['操作']}
                showPlaceholder={!activityCategories.length}
                placeholder="尚無活動分類">
                <TableField
                  name="名稱"
                  fieldKey="name"
                  flex={1}
                  isCenter />
                <TableField
                  name="桌面版圖片"
                  fieldKey="desktopImg"
                  flex={2}
                  Component={LargeImage} />
                <TableField
                  name="手機版圖片"
                  fieldKey="mobileImg"
                  flex={1}
                  Component={LargeImage} />
              </Table>
            </div>
          );
        }}
      </Query>
    );
  }
}

export default radium(
  ActivityCategoryList
);
