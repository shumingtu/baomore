// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';
// components
import Table, { TableField } from '../../components/Table/Table.jsx';
import LargeImage from '../../components/Table/Custom/LargeImage.jsx';
import Button from '../../components/Global/Button.jsx';
import PageTitle from '../../components/Global/PageTitle.jsx';
import ActivityManageActions from '../../components/Table/Actions/ActivityManageActions.jsx';
// config
import { FETCH_ACTIVITIES } from '../../queries/Activity.js';

const styles = {
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 35px)',
    maxHeight: 810,
    padding: '24px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  buttonWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  btn: {
    width: 'auto',
    height: 'auto',
    padding: '0 12px 0 0',
  },
};

type Props = {
  history: {
    push: Function,
  },
  location: {
    pathname: string,
  },
  match: {
    params: {
      categoryId: string,
    },
  },
};

class ActivityList extends PureComponent<Props> {
  render() {
    const {
      history,
      location: {
        pathname,
      },
      match: {
        params: {
          categoryId,
        },
      },
    } = this.props;

    return (
      <Query
        query={FETCH_ACTIVITIES}
        variables={{
          categoryId: categoryId ? parseInt(categoryId, 10) : -1,
        }}>
        {({ data }) => {
          if (!data) return null;

          const {
            activities = [],
          } = data;

          return (
            <div style={styles.tableWrapper}>
              <PageTitle title="活動列表" />
              <div style={styles.buttonWrapper}>
                <div style={styles.btn}>
                  <Button
                    label="返回"
                    onClick={() => history.push('/activity')} />
                </div>
                <div style={styles.btn}>
                  <Button
                    label="新增活動"
                    onClick={() => history.push(`${pathname}/create`)} />
                </div>
              </div>
              <Table
                dataSource={activities.map(l => ({
                  id: l.id,
                  name: l.name,
                  desktopImg: l.desktopImg,
                  mobileImg: l.mobileImg,
                  description: l.description || null,
                  isActived: l.isActived,
                }))}
                getActions={() => [
                  <ActivityManageActions />,
                ]}
                actionTitles={['操作']}
                showPlaceholder={!activities.length}
                placeholder="尚無活動">
                <TableField
                  name="名稱"
                  fieldKey="name"
                  flex={1}
                  isCenter />
                <TableField
                  name="桌面版圖片"
                  fieldKey="desktopImg"
                  flex={2}
                  Component={LargeImage} />
                <TableField
                  name="手機版圖片"
                  fieldKey="mobileImg"
                  flex={1}
                  Component={LargeImage} />
                <TableField
                  name="活動描述"
                  fieldKey="description"
                  flex={2}
                  isCenter />
              </Table>
            </div>
          );
        }}
      </Query>
    );
  }
}

export default radium(
  ActivityList
);
