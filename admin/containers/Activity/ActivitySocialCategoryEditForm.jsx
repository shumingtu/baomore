// @flow
import React, { Component } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  initialize,
  SubmissionError,
} from 'redux-form';
import {
  graphql,
  Mutation,
} from 'react-apollo';

import TextStyles from '../../styles/Text.js';
import {
  FETCH_SOCIAL_ACTIVITY_CATEGORIES,
} from '../../queries/Activity.js';
import {
  EDIT_ACTIVITY_SOCIAL_CATEGORY,
} from '../../mutations/Activity.js';
import { FORM_ACTIVITY_SOCIAL_CATEGORY_EDIT_FORM } from '../../shared/form.js';

import SubmitButton from '../../components/Form/SubmitButton.jsx';
import Button from '../../components/Global/Button.jsx';
import Input from '../../components/Form/Input.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
  title: {
    ...TextStyles.labelText,
    width: '100%',
    height: 'auto',
    textAlign: 'center',
    fontSize: 18,
    padding: '24px 0',
  },
  mainForm: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formWrapper: {
    width: 650,
    height: 'auto',
    padding: '16px 32px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    border: '1px solid rgb(182, 184, 204)',
  },
  formGroup: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    // width: '100%',
    margin: 5,
  },
  functionWrapper: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    margin: '30px 0 5px 0',
  },
  editBtn: {
    width: 88,
    height: 32,
    backgroundColor: 'rgb(182, 184, 204)',
    margin: '0 3px',
  },
  cancelBtn: {
    width: 88,
    height: 32,
    margin: '0 3px',
    backgroundColor: 'white',
    border: '1px solid rgb(182, 184, 204)',
    color: 'rgb(182, 184, 204)',
  },
};

type Props = {
  socialCategories: Array,
  initializeForm: Function,
  handleSubmit: Function,
  match: Object,
  history: Object,
};

class ActivitySocialCategoryEditForm extends Component<Props> {
  componentDidMount() {
    const {
      initializeForm,
      socialCategories,
    } = this.props;

    if (socialCategories.length) {
      initializeForm({
        ...socialCategories[0],
      });
    }
  }

  componentDidUpdate(prevProps) {
    const {
      initializeForm,
      socialCategories,
    } = this.props;

    if (socialCategories && socialCategories !== prevProps.socialCategories) {
      if (socialCategories.length) {
        initializeForm({
          ...socialCategories[0],
        });
      }
    }
  }

  render() {
    const {
      handleSubmit,
      match: {
        params: {
          categoryId,
        },
      },
      history,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.title}>
          修改社群活動
        </div>
        <div style={styles.mainForm}>
          <Mutation mutation={EDIT_ACTIVITY_SOCIAL_CATEGORY}>
            {editSocialCategory => (
              <form
                onSubmit={handleSubmit(async (d) => {
                  const {
                    name,
                    link,
                  } = d;

                  if (!name) {
                    throw new SubmissionError({
                      name: '請確認已填寫',
                      link: '請確認已填寫',
                    });
                  }

                  const payload = {
                    id: categoryId,
                    link: link || null,
                    ...d,
                  };

                  const {
                    data: {
                      editSocialCategory: {
                        id,
                      },
                    },
                  } = await editSocialCategory({ variables: payload });

                  if (id) {
                    history.push('/activity/social');
                  }
                })}
                style={styles.formWrapper}>
                <div style={styles.formGroup}>
                  <Field
                    label="名稱:"
                    name="name"
                    component={Input} />
                </div>
                <div style={styles.formGroup}>
                  <Field
                    label="連結:"
                    name="link"
                    component={Input} />
                </div>
                <div style={styles.functionWrapper}>
                  <SubmitButton
                    style={styles.editBtn}
                    label="確定修改" />
                  <Button onClick={() => history.goBack()} style={styles.cancelBtn} label="取消" />
                </div>
              </form>
            )}
          </Mutation>
        </div>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_ACTIVITY_SOCIAL_CATEGORY_EDIT_FORM,
});

const reduxHook = connect(
  () => ({
  }),
  dispatch => bindActionCreators({
    initializeForm: d => initialize(FORM_ACTIVITY_SOCIAL_CATEGORY_EDIT_FORM, d),
  }, dispatch)
);

const queryHook = graphql(FETCH_SOCIAL_ACTIVITY_CATEGORIES, {
  options: ownProps => ({
    variables: {
      id: ownProps.match.params.categoryId ? parseInt(ownProps.match.params.categoryId, 10) : -1,
    },
  }),
  props: ({
    data: {
      socialCategories,
    },
  }) => ({
    socialCategories: socialCategories || [],
  }),
});


export default reduxHook(
  queryHook(
    formHook(
      radium(ActivitySocialCategoryEditForm)
    )
  )
);
