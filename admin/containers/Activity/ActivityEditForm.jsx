// @flow
import React, { PureComponent } from 'react';
import {
  Query,
  Mutation,
} from 'react-apollo';

import {
  FETCH_ACTIVITY_DETAIL,
} from '../../queries/Activity.js';
import {
  EDIT_ACTIVITY,
} from '../../mutations/Activity.js';
import ActivityBaseForm from '../../components/Activity/ActivityBaseForm.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
};

type Props = {
  match: {
    params: {
      activityId: string,
    },
  },
};

class ActivityEditForm extends PureComponent<Props> {
  render() {
    const {
      match: {
        params: {
          activityId,
        },
      },
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <Query
          query={FETCH_ACTIVITY_DETAIL}
          fetchPolicy="cache-and-network"
          variables={{
            activityId: parseInt(activityId, 10),
          }}>
          {({
            data,
          }) => {
            const wrapActivity = (data && data.activity) || null;

            return (
              <Mutation mutation={EDIT_ACTIVITY}>
                {editActivity => (
                  <ActivityBaseForm
                    {...this.props}
                    mutate={editActivity}
                    data={wrapActivity} />
                )}
              </Mutation>
            );
          }}
        </Query>
      </div>
    );
  }
}

export default ActivityEditForm;
