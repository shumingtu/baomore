// @flow
import React, { Component } from 'react';
import radium from 'radium';
import {
  Switch,
  Route,
} from 'react-router-dom';
// components
import PageLinkSwitcher from '../../components/Global/PageLinkSwitcher.jsx';
import ActivityCategoryList from './ActivityCategoryList.jsx';
import ActivityList from './ActivityList.jsx';
import ActivityCategoryCreateForm from './ActivityCategoryCreateForm.jsx';
import ActivityCategoryEditForm from './ActivityCategoryEditForm.jsx';
import ActivityCreateForm from './ActivityCreateForm.jsx';
import ActivityEditForm from './ActivityEditForm.jsx';
import ActivitySocialList from './ActivitySocialList.jsx';
import ActivitySocialCategoryEditForm from './ActivitySocialCategoryEditForm.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    position: 'relative',
    overflow: 'auto',
  },
};

type Props = {
  location: {
    pathname: string,
  },
  history: {
    replace: Function,
  },
};

const MY_PAGES = [{
  name: '官方活動管理',
  path: '/activity/official',
}, {
  name: '社群活動管理',
  path: '/activity/social',
}];

class ActivityMainBoard extends Component<Props> {
  componentDidMount() {
    const {
      location: {
        pathname,
      },
      history,
    } = this.props;

    if (pathname === '/activity') {
      history.replace('/activity/official');
    }
  }

  componentDidUpdate(prevProps) {
    const {
      location: {
        pathname,
      },
      history,
    } = this.props;

    if (pathname !== prevProps.pathname && pathname === '/activity') {
      history.replace('/activity/official');
    }
  }

  render() {
    return (
      <div style={styles.wrapper}>
        <PageLinkSwitcher pages={MY_PAGES} />
        <Switch>
          <Route exact path="/activity/official/category/:categoryId" component={ActivityCategoryEditForm} />
          <Route exact path="/activity/official/create" component={ActivityCategoryCreateForm} />
          <Route exact path="/activity/official/:categoryId/activities" component={ActivityList} />
          <Route exact path="/activity/official/:categoryId/activities/create" component={ActivityCreateForm} />
          <Route exact path="/activity/official/:categoryId/activities/:activityId" component={ActivityEditForm} />
          <Route exact path="/activity/official" component={ActivityCategoryList} />
          <Route exact path="/activity/social/edit/:categoryId" component={ActivitySocialCategoryEditForm} />
          <Route exact path="/activity/social" component={ActivitySocialList} />
        </Switch>
      </div>
    );
  }
}

export default radium(
  ActivityMainBoard
);
