// @flow
import React, { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';
import { Mutation } from 'react-apollo';

import {
  CREATE_ACTIVITY,
} from '../../mutations/Activity.js';
import {
  FETCH_ACTIVITIES,
} from '../../queries/Activity.js';
import ActivityBaseForm from '../../components/Activity/ActivityBaseForm.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
};

type Props = {
  match: {
    params: {
      categoryId: string,
    },
  },
};

class ActivityCreateForm extends PureComponent<Props> {
  render() {
    const {
      match: {
        params: {
          categoryId,
        },
      },
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <Mutation
          mutation={CREATE_ACTIVITY}
          update={(store, { data: { createActivity } }) => {
            const data = store.readQuery({
              query: FETCH_ACTIVITIES,
              variables: {
                categoryId: categoryId ? parseInt(categoryId, 10) : -1,
              },
            });

            const {
              activities,
            } = data;

            if (createActivity) {
              store.writeQuery({
                query: FETCH_ACTIVITIES,
                variables: {
                  categoryId: categoryId ? parseInt(categoryId, 10) : -1,
                },
                data: {
                  activities: [
                    ...activities,
                    {
                      ...createActivity,
                    },
                  ],
                },
              });
            }
          }}>
          {createActivity => (
            <ActivityBaseForm
              {...this.props}
              mutate={createActivity} />
          )}
        </Mutation>
      </div>
    );
  }
}

export default withRouter(ActivityCreateForm);
