// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';
import moment from 'moment';
// components
import Table, { TableField } from '../../components/Table/Table.jsx';
import PageTitle from '../../components/Global/PageTitle.jsx';
import PunchSearchForm from '../../components/Punch/PunchSearchForm.jsx';
// config
import { FETCH_ADMIN_PUNCH_RECORDS } from '../../queries/Punch.js';

const styles = {
  tableWrapper: {
    width: '100%',
    height: '100%',
    padding: '24px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
};

type Props = {
  searchOptions: {
    startDate?: string,
    endDate?: string,
  },
};

const LIMIT_LENGTH = 10;

const punchText = (type) => {
  switch (type) {
    case 'ON':
      return '上班';

    case 'OFF':
      return '下班';

    default:
      return '';
  }
};

class PunchList extends PureComponent<Props> {
  render() {
    const {
      searchOptions,
    } = this.props;

    const queryOptions = {
      offset: 0,
      limit: LIMIT_LENGTH,
    };

    if (searchOptions) {
      const {
        startDate,
        endDate,
        employeeId,
      } = searchOptions;

      if (startDate) queryOptions.startDate = startDate;
      if (endDate) queryOptions.endDate = endDate;
      if (employeeId) queryOptions.employeeId = employeeId;
    }

    return (
      <Query
        variables={queryOptions}
        query={FETCH_ADMIN_PUNCH_RECORDS}>
        {({ data, fetchMore }) => {
          if (!data) return null;

          const {
            adminPunchList = [],
          } = data;

          return (
            <div style={styles.tableWrapper}>
              <PageTitle title="打卡列表" />
              <PunchSearchForm />
              <Table
                dataSource={adminPunchList.map(a => ({
                  id: a.id,
                  punchTime: moment(a.punchTime).format('YYYY-MM-DD HH:mm'),
                  store: a.store && a.store.name,
                  member: a.member && a.member.name,
                  type: punchText(a.type),
                }))}
                fetchMore={() => fetchMore({
                  variables: {
                    ...queryOptions,
                    offset: 0,
                    limit: adminPunchList.length + LIMIT_LENGTH,
                  },
                  updateQuery: (prev, { fetchMoreResult }) => {
                    if (!fetchMoreResult) return prev;

                    return {
                      ...prev,
                      adminPunchList: [
                        ...fetchMoreResult.adminPunchList,
                      ],
                    };
                  },
                })}
                showPlaceholder={!adminPunchList.length}
                placeholder="尚無打卡資料">
                <TableField
                  name="打卡別"
                  fieldKey="type"
                  flex={1}
                  isCenter />
                <TableField
                  name="時間"
                  fieldKey="punchTime"
                  flex={1}
                  isCenter />
                <TableField
                  name="店家"
                  fieldKey="store"
                  flex={1}
                  isCenter />
                <TableField
                  name="包膜師"
                  fieldKey="member"
                  flex={1}
                  isCenter />
              </Table>
            </div>
          );
        }}
      </Query>
    );
  }
}

const reduxHook = connect(
  state => ({
    searchOptions: state.Search.adminPunchList,
  }),
);

export default reduxHook(
  radium(
    PunchList
  )
);
