// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';
// components
import Table, { TableField } from '../../components/Table/Table.jsx';
import AnnouncementManageActions from '../../components/Table/Actions/AnnouncementManageActions.jsx';
import Button from '../../components/Global/Button.jsx';
import PageTitle from '../../components/Global/PageTitle.jsx';
import AnnouncementSearchForm from '../../components/Announcement/AnnouncementSearchForm.jsx';
// config
import { FETCH_ANNOUNCEMENT_LIST } from '../../queries/Announcement.js';

const styles = {
  tableWrapper: {
    width: '100%',
    height: '100%',
    padding: '24px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
};

type Props = {
  history: {
    push: Function,
  },
  location: {
    pathname: string,
  },
  searchOptions: {
    keyword?: string,
    startDate?: string,
    endDate?: string,
  },
};

const LIMIT_LENGTH = 10;

class AnnouncementList extends PureComponent<Props> {
  render() {
    const {
      history,
      location: {
        pathname,
      },
      searchOptions,
    } = this.props;

    const queryOptions = {
      offset: 0,
      limit: LIMIT_LENGTH,
    };

    if (searchOptions) {
      const {
        keyword,
        startDate,
        endDate,
      } = searchOptions;

      if (keyword) queryOptions.keyword = keyword;
      if (startDate) queryOptions.startDate = startDate;
      if (endDate) queryOptions.endDate = endDate;
    }

    return (
      <Query
        query={FETCH_ANNOUNCEMENT_LIST}
        fetchPolicy="cache-and-network"
        variables={queryOptions}>
        {({ data, fetchMore }) => {
          if (!data) return null;

          const {
            announcements = [],
          } = data;

          return (
            <div style={styles.tableWrapper}>
              <PageTitle title="公告列表" />
              <AnnouncementSearchForm />
              <Button
                label="新增公告"
                onClick={() => history.push(`${pathname}/create`)} />
              <Table
                dataSource={announcements.map(a => ({
                  id: a.id,
                  title: a.title || null,
                  content: a.content || null,
                  createdAt: a.createdAt || null,
                }))}
                fetchMore={() => fetchMore({
                  variables: {
                    ...queryOptions,
                    offset: 0,
                    limit: announcements.length + LIMIT_LENGTH,
                  },
                  updateQuery: (prev, { fetchMoreResult }) => {
                    if (!fetchMoreResult) return prev;

                    return {
                      ...prev,
                      announcements: [
                        ...fetchMoreResult.announcements,
                      ],
                    };
                  },
                })}
                getActions={() => [
                  <AnnouncementManageActions />,
                ]}
                actionTitles={['操作']}
                showPlaceholder={!announcements.length}
                placeholder="尚無公告">
                <TableField
                  name="公告標題"
                  fieldKey="title"
                  flex={1}
                  isCenter />
                <TableField
                  name="公告內容"
                  fieldKey="content"
                  flex={2}
                  isCenter />
                <TableField
                  name="公告日期"
                  fieldKey="createdAt"
                  flex={1}
                  isCenter />
              </Table>
            </div>
          );
        }}
      </Query>
    );
  }
}

const reduxHook = connect(
  state => ({
    searchOptions: state.Search.adminAnnouncements,
  }),
);

export default reduxHook(
  radium(
    AnnouncementList
  )
);
