// @flow
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Mutation } from 'react-apollo';

import {
  CREATE_ANNOUNCEMENT,
} from '../../mutations/Announcement.js';
import {
  FETCH_ANNOUNCEMENT_LIST,
} from '../../queries/Announcement.js';
import AnnouncementBaseForm from '../../components/Announcement/AnnouncementBaseForm.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
};

type Props = {
  searchOptions: {
    keyword?: string,
    startDate?: string,
    endDate?: string,
  },
};

class AnnouncementCreateForm extends PureComponent<Props> {
  render() {
    const {
      searchOptions,
    } = this.props;

    const queryOptions = {
      offset: 0,
      limit: 10,
    };

    if (searchOptions) {
      const {
        keyword,
        startDate,
        endDate,
      } = searchOptions;

      if (keyword) queryOptions.keyword = keyword;
      if (startDate) queryOptions.startDate = startDate;
      if (endDate) queryOptions.endDate = endDate;
    }

    return (
      <div style={styles.wrapper}>
        <Mutation
          mutation={CREATE_ANNOUNCEMENT}
          update={(store, { data: { createAnnouncement } }) => {
            const data = store.readQuery({
              query: FETCH_ANNOUNCEMENT_LIST,
              variables: queryOptions,
            });

            const {
              announcements,
            } = data;

            if (createAnnouncement) {
              store.writeQuery({
                query: FETCH_ANNOUNCEMENT_LIST,
                variables: queryOptions,
                data: {
                  announcements: [
                    {
                      ...createAnnouncement,
                    },
                    ...announcements,
                  ],
                },
              });
            }
          }}>
          {createAnnouncement => (
            <AnnouncementBaseForm
              {...this.props}
              mutate={createAnnouncement} />
          )}
        </Mutation>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    searchOptions: state.Search.adminAnnouncements,
  }),
);

export default reduxHook(AnnouncementCreateForm);
