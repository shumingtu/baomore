// @flow
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Mutation, Query } from 'react-apollo';

import {
  EDIT_ANNOUNCEMENT,
} from '../../mutations/Announcement.js';
import {
  FETCH_ANNOUNCEMENT_LIST,
  FETCH_ANNOUNCEMENT_DETAIL,
} from '../../queries/Announcement.js';
import AnnouncementBaseForm from '../../components/Announcement/AnnouncementBaseForm.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
};

type Props = {
  searchOptions: {
    keyword?: string,
    startDate?: string,
    endDate?: string,
  },
  match: {
    params: {
      announcementId: string,
    },
  },
};

class AnnouncementEditForm extends PureComponent<Props> {
  render() {
    const {
      searchOptions,
      match: {
        params: {
          announcementId,
        },
      },
    } = this.props;

    const queryOptions = {
      offset: 0,
      limit: 10,
    };

    if (searchOptions) {
      const {
        keyword,
        startDate,
        endDate,
      } = searchOptions;

      if (keyword) queryOptions.keyword = keyword;
      if (startDate) queryOptions.startDate = startDate;
      if (endDate) queryOptions.endDate = endDate;
    }

    return (
      <div style={styles.wrapper}>
        <Query
          query={FETCH_ANNOUNCEMENT_DETAIL}
          fetchPolicy="cache-and-network"
          variables={{
            id: parseInt(announcementId, 10),
          }}>
          {({
            data,
          }) => {
            const announcement = (data && data.announcement) || null;

            return (
              <Mutation
                mutation={EDIT_ANNOUNCEMENT}
                update={(store, { data: { createAnnouncement } }) => {
                  const cacheData = store.readQuery({
                    query: FETCH_ANNOUNCEMENT_LIST,
                    variables: queryOptions,
                  });

                  const {
                    announcements,
                  } = cacheData;

                  if (createAnnouncement) {
                    store.writeQuery({
                      query: FETCH_ANNOUNCEMENT_LIST,
                      variables: queryOptions,
                      data: {
                        announcements: [
                          {
                            ...createAnnouncement,
                          },
                          ...announcements,
                        ],
                      },
                    });
                  }
                }}>
                {createAnnouncement => (
                  <AnnouncementBaseForm
                    {...this.props}
                    data={announcement}
                    mutate={createAnnouncement} />
                )}
              </Mutation>
            );
          }}
        </Query>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    searchOptions: state.Search.adminAnnouncements,
  }),
);

export default reduxHook(AnnouncementEditForm);
