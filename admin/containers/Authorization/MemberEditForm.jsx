// @flow

import React, { PureComponent } from 'react';
import {
  Query,
  Mutation,
} from 'react-apollo';

import {
  EDIT_MEMBER,
} from '../../mutations/Member.js';
import {
  FETCH_ALL_MEMBER_LIST,
} from '../../queries/Member.js';
import MemberBaseForm from './MemberBaseForm.jsx';

const styles = {
  wrapper: {
    width: '100%',
  },
};

type Props = {
  match: Object,
};

class MemberEditForm extends PureComponent<Props> {
  render() {
    const {
      match: {
        params: {
          memberId,
        },
      },
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <Query
          query={FETCH_ALL_MEMBER_LIST}
          variables={{
            memberId: parseInt(memberId, 10),
          }}>
          {({
            data,
          }) => {
            const memberDatas = (data && data.allMemberList) || [];

            return (
              <Mutation mutation={EDIT_MEMBER}>
                {editMemberForAdmin => (
                  <MemberBaseForm
                    {...this.props}
                    mutate={editMemberForAdmin}
                    memberData={memberDatas[0] || null} />
                )}
              </Mutation>
            );
          }}
        </Query>
      </div>
    );
  }
}

export default MemberEditForm;
