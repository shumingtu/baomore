// @flow
import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PageLinkSwitcher from '../../components/Global/PageLinkSwitcher.jsx';
import RoleBoard from './RoleBoard.jsx';
import { toggleRoleEditBox } from '../../actions/Authorization.js';
import RoleEditPopUp from '../../components/Authorization/RoleEditPopUp.jsx';
import MemberListSearcher from '../../components/Authorization/MemberListSearcher.jsx';
import MemberList from './MemberList.jsx';
import MemberCreateForm from './MemberCreateForm.jsx';
import MemberEditForm from './MemberEditForm.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    position: 'relative',
    overflow: 'auto',
  },
};

type Props = {
  location: {
    pathname: String,
  },
  history: Object,
  pathname: String,
  toggleBox: Function,
  roleData: Object,
};

const MY_PAGES = [{
  name: '會員管理',
  path: '/authorization/member',
}, {
  name: '角色管理',
  path: '/authorization/role',
}];

class AuthorizationBoard extends PureComponent<Props> {
  componentDidMount() {
    const {
      location: {
        pathname,
      },
      history,
    } = this.props;

    if (pathname === '/authorization') {
      history.replace('/authorization/member');
    }
  }

  componentDidUpdate(prevProps) {
    const {
      location: {
        pathname,
      },
      history,
    } = this.props;

    if (pathname !== prevProps.pathname && pathname === '/authorization') {
      history.replace('/authorization/member');
    }
  }

  render() {
    const {
      toggleBox,
      roleData,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <PageLinkSwitcher pages={MY_PAGES} />
        {roleData ? (
          <RoleEditPopUp roleData={roleData} close={() => toggleBox(null)} />
        ) : null}
        <Switch>
          <Route
            exact
            path="/authorization/member"
            component={() => (
              <Fragment>
                <MemberListSearcher />
                <MemberList />
              </Fragment>
            )} />
          <Route path="/authorization/member/create" component={MemberCreateForm} />
          <Route path="/authorization/member/edit/:memberId" component={MemberEditForm} />
          <Route path="/authorization/role" component={RoleBoard} />
        </Switch>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    roleData: state.Authorization.boxData,
  }),
  dispatch => bindActionCreators({
    toggleBox: toggleRoleEditBox,
  }, dispatch),
);

export default reduxHook(radium(AuthorizationBoard));
