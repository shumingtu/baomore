// @flow
import React from 'react';
import { Query } from 'react-apollo';

import { FETCH_ROLE_LIST } from '../../queries/Role.js';
import Table, { TableField } from '../../components/Table/Table.jsx';
import RoleBoardActions from '../../components/Table/Actions/RoleBoardActions.jsx';

const styles = {
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 35px)',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
};

const ROLE_LIST_LIMIT = 10;

function RoleBoard() {
  return (
    <Query query={FETCH_ROLE_LIST}>
      {({ data, fetchMore }) => {
        if (!data) return null;

        const {
          roleList = [],
        } = data;

        return (
          <div style={styles.tableWrapper}>
            <Table
              dataSource={roleList.map(r => ({
                id: r.id,
                name: r.name,
                actions: r.actions,
              }))}
              fetchMore={() => fetchMore({
                variables: {
                  offset: 0,
                  limit: roleList.length + ROLE_LIST_LIMIT,
                },
                updateQuery: (prev, { fetchMoreResult }) => {
                  if (!fetchMoreResult) return prev;
                  return {
                    ...prev,
                    roleList: [
                      ...fetchMoreResult.roleList,
                    ],
                  };
                },
              })}
              showPlaceholder={!roleList.length}
              placeholder="查無資料"
              actionTitles={['編輯']}
              getActions={() => [
                <RoleBoardActions />,
              ]}>
              <TableField name="角色名稱" fieldKey="name" flex={1} isCenter />
            </Table>
          </div>
        );
      }}
    </Query>
  );
}

export default RoleBoard;
