// @flow

import React, { PureComponent } from 'react';
import { Mutation } from 'react-apollo';

import {
  CREATE_MEMBER,
} from '../../mutations/Member.js';

import MemberBaseForm from './MemberBaseForm.jsx';

const styles = {
  wrapper: {
    width: '100%',
  },
};

class MemberCreateForm extends PureComponent {
  render() {
    return (
      <div style={styles.wrapper}>
        <Mutation
          mutation={CREATE_MEMBER}>
          {createMember => (
            <MemberBaseForm
              {...this.props}
              mutate={createMember} />
          )}
        </Mutation>
      </div>
    );
  }
}

export default MemberCreateForm;
