// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';

import MemberListActions from '../../components/Table/Actions/MemberListActions.jsx';
import Table, { TableField } from '../../components/Table/Table.jsx';
import { FETCH_ALL_MEMBER_LIST } from '../../queries/Member.js';

const styles = {
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 192px)',
    maxHeight: 630,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
};

type Props = {
  options: {
    keyword: string,
    roleId: number,
  },
};

const MEMBER_LIST_LIMIT = 10;

class MemberList extends PureComponent<Props> {
  render() {
    const {
      options,
    } = this.props;

    return (
      <Query
        query={FETCH_ALL_MEMBER_LIST}
        variables={{
          ...options,
          limit: MEMBER_LIST_LIMIT,
          offset: 0,
        }}>
        {({ data, fetchMore }) => {
          if (!data) return null;

          const {
            allMemberList = [],
          } = data;

          return (
            <div style={styles.tableWrapper}>
              <Table
                dataSource={allMemberList.map(m => ({
                  id: m.id,
                  name: m.name,
                  serialNumber: m.serialNumber || null,
                  lineId: m.lineId,
                  phone: m.phone || null,
                  lineAccount: m.lineAccount || '尚未設定',
                  archive: m.archive,
                }))}
                fetchMore={() => fetchMore({
                  variables: {
                    ...options,
                    offset: 0,
                    limit: allMemberList.length + MEMBER_LIST_LIMIT,
                  },
                  updateQuery: (prev, { fetchMoreResult }) => {
                    if (!fetchMoreResult) return prev;
                    return {
                      ...prev,
                      allMemberList: [
                        ...fetchMoreResult.allMemberList,
                      ],
                    };
                  },
                })}
                showPlaceholder={!allMemberList.length}
                placeholder="查無資料"
                actionTitles={['操作']}
                getActions={() => [
                  <MemberListActions />,
                ]}>
                <TableField name="姓名" fieldKey="name" flex={1} isCenter />
                <TableField name="工號" fieldKey="serialNumber" flex={1} isCenter />
                <TableField name="電話" fieldKey="phone" flex={1} isCenter />
                <TableField name="LineId" fieldKey="lineId" flex={2} isCenter />
                <TableField name="Line帳號" fieldKey="lineAccount" flex={1} isCenter />
              </Table>
            </div>
          );
        }}
      </Query>
    );
  }
}

const reduxHook = connect(
  state => ({
    options: state.Search.adminMemberList,
  }),
);

export default reduxHook(
  radium(
    MemberList
  )
);
