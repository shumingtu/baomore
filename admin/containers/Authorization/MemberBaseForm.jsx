// @flow
import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  initialize,
  SubmissionError,
  formValueSelector,
  change,
} from 'redux-form';
import {
  Query,
} from 'react-apollo';

import {
  FORM_MEMBER_CREATE_EDIT_FORM,
} from '../../shared/form.js';
import {
  validateMemberCreateEdit,
} from '../../helper/validator/MemberCreateEdit.js';

import {
  FETCH_AREA_LIST,
} from '../../queries/Area.js';
import {
  GET_EMPLOYEE_LIST,
} from '../../queries/Member.js';

import FormTitle from '../../components/Global/FormTitle.jsx';
import Input from '../../components/Form/Input.jsx';
import SubmitButton from '../../components/Form/SubmitButton.jsx';
import Button from '../../components/Global/Button.jsx';
import Selector from '../../components/Form/Selector.jsx';
import RoleList from '../../components/Authorization/RoleList.jsx';
import Roles from '../../components/Form/Roles.jsx';

const selector = formValueSelector(FORM_MEMBER_CREATE_EDIT_FORM);

const styles = {
  wrapper: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 12,
  },
  formWrapper: {
    width: 650,
    height: 'auto',
    padding: '16px 32px',
    backgroundColor: '#fff',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    margin: 5,
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  buttonWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 6px',
  },
};

type Props = {
  memberData: Object,
  handleSubmit: Function,
  initializeForm: Function,
  mutate: Function,
  history: Object,
  currentRoles: Array,
  setRole: Function,
};

const genders = [{
  id: 'male',
  name: '男',
}, {
  id: 'female',
  name: '女',
}];

class MemberBaseForm extends PureComponent<Props> {
  componentDidMount() {
    const {
      memberData,
    } = this.props;

    if (memberData) {
      this.initializing(memberData);
    }
  }

  componentDidUpdate(prevProps) {
    const {
      memberData,
    } = this.props;

    if (memberData !== prevProps.memberData && memberData) {
      this.initializing(memberData);
    }
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    const initializePayload = {
      serialNumber: obj.serialNumber && obj.serialNumber,
      name: obj.name && obj.name,
      email: obj.email && obj.email,
      gender: obj.gender && obj.gender,
      birthday: obj.birthday && obj.birthday,
      phone: obj.phone && obj.phone,
      areaId: obj.area && obj.area.id,
      directorId: obj.director && obj.director.id,
      roles: obj.roles,
      lineId: obj.lineId && obj.lineId,
      lineAccount: obj.lineAccount && obj.lineAccount,
    };

    initializeForm(initializePayload);
  }

  async submit(d, isNormalMember, isEmployee) {
    const errors = validateMemberCreateEdit(d, isNormalMember, isEmployee);

    if (errors) {
      throw new SubmissionError(errors);
    }

    const {
      serialNumber,
      name,
      email,
      gender,
      birthday,
      phone,
      areaId,
      directorId,
      roles,
      lineId,
      lineAccount,
    } = d;

    const {
      mutate,
      memberData,
      history,
    } = this.props;

    const payload = {
      serialNumber: serialNumber || null,
      name,
      email: email || null,
      gender: gender || null,
      birthday: birthday || null,
      phone: phone || null,
      areaId: (areaId && parseInt(areaId, 10)) || null,
      directorId: (directorId && parseInt(directorId, 10)) || null,
      roleIds: roles.map(x => x.id),
      lineId,
      lineAccount: lineAccount || null,
    };

    if (memberData) payload.memberId = memberData.id;

    try {
      await mutate({
        variables: {
          ...payload,
        },
      });

      history.push('/authorization/member');
    } catch (e) {
      alert(e);
    }
  }

  render() {
    const {
      history,
      memberData,
      handleSubmit,
      setRole,
      currentRoles,
    } = this.props;

    const formTitle = `${memberData ? '修改' : '新增'}會員`;

    const isNormalMember = currentRoles && currentRoles.find(x => x.name === '一般登入用戶');
    const isEmployee = currentRoles && currentRoles.find(x => x.name === '包膜師');
    const isDirector = currentRoles && currentRoles.find(x => x.name === '主任包膜師');

    return (
      <div style={styles.wrapper}>
        <FormTitle
          title={formTitle} />
        <form
          onSubmit={handleSubmit(d => this.submit(d, isNormalMember, isEmployee))}
          style={styles.formWrapper}>
          <div style={styles.fieldWrapper}>
            <h2>選擇角色</h2>
            <RoleList
              setRole={setRole}
              currentRoles={currentRoles} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              label="角色："
              name="roles"
              component={Roles} />
          </div>
          {isEmployee || isDirector ? (
            <Fragment>
              <div style={styles.fieldWrapper}>
                <Field
                  name="serialNumber"
                  label="工號："
                  placeholder="請輸入工號"
                  component={Input} />
              </div>
              <Query query={FETCH_AREA_LIST}>
                {({ data }) => {
                  if (!data) return null;

                  const {
                    areaList,
                  } = data;

                  return (
                    <div style={styles.fieldWrapper}>
                      <Field
                        placeholder="請選擇"
                        nullable
                        component={Selector}
                        options={areaList}
                        label="區域:"
                        name="areaId" />
                    </div>
                  );
                }}
              </Query>
            </Fragment>
          ) : null}
          {isEmployee && !isDirector ? (
            <Query
              fetchPolicy="network-only"
              variables={{ employeeType: '主任包膜師' }}
              query={GET_EMPLOYEE_LIST}>
              {({ data }) => {
                if (!data) return null;

                const {
                  employeeMemberlist,
                } = data;

                return (
                  <div style={styles.fieldWrapper}>
                    <Field
                      placeholder="請選擇"
                      nullable
                      component={Selector}
                      options={employeeMemberlist}
                      label="主任:"
                      name="directorId" />
                  </div>
                );
              }}
            </Query>
          ) : null}
          <div style={styles.fieldWrapper}>
            <Field
              name="lineId"
              label="LINEID："
              placeholder="請輸入LINEID"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              name="lineAccount"
              label="LINE帳號："
              placeholder="請輸入LINE帳號"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              name="name"
              label="姓名："
              placeholder="請輸入姓名"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              name="email"
              label="Email："
              placeholder="請輸入信箱"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              placeholder="請選擇"
              nullable
              component={Selector}
              options={genders}
              label="性別：:"
              name="gender" />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              name="birthday"
              label="生日："
              type="date"
              placeholder="請輸入生日"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <Field
              name="phone"
              label="手機："
              placeholder="請輸入手機"
              component={Input} />
          </div>
          <div style={styles.functionWrapper}>
            <div style={styles.buttonWrapper}>
              <SubmitButton label="送出" />
            </div>
            <div style={styles.buttonWrapper}>
              <Button
                label="返回"
                onClick={() => history.goBack()} />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_MEMBER_CREATE_EDIT_FORM,
});


const reduxHook = connect(
  state => ({
    currentRoles: selector(state, 'roles'),
  }),
  dispatch => bindActionCreators({
    initializeForm: v => initialize(FORM_MEMBER_CREATE_EDIT_FORM, v),
    setRole: v => change(FORM_MEMBER_CREATE_EDIT_FORM, 'roles', v),
  }, dispatch)
);


export default reduxHook(
  formHook(
    radium(
      MemberBaseForm
    )
  )
);
