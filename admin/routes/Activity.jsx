// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import ActivityMainBoard from '../containers/Activity/ActivityMainBoard.jsx';

function Activity({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}`} component={ActivityMainBoard} />
    </Switch>
  );
}

export default Activity;
