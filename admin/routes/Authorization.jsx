// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import AuthorizationBoard from '../containers/Authorization/AuthorizationBoard.jsx';

function Authorization({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}`} component={AuthorizationBoard} />
    </Switch>
  );
}

export default Authorization;
