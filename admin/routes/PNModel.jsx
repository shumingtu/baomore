// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import PNModelListBoard from '../containers/PNModel/PNModelListBoard.jsx';
import {
  PNModelCreateForm,
  PNModelEditForm,
} from '../containers/PNModel/PNModelCreateEditForm.jsx';

function PNModel({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route exact path={`${url}`} component={PNModelListBoard} />
      <Route exact path={`${url}/create`} component={PNModelCreateForm} />
      <Route exact path={`${url}/:pnItemId/edit`} component={PNModelEditForm} />
    </Switch>
  );
}

export default PNModel;
