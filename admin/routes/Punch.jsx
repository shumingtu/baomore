// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import PunchList from '../containers/Punch/PunchList.jsx';

function Punch({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}`} component={PunchList} />
    </Switch>
  );
}

export default Punch;
