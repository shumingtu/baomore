// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import OrderListBoard from '../containers/Order/OrderListBoard.jsx';
import OrderScheduleEditPage from '../containers/Order/OrderScheduleEditPage.jsx';
import AssignForm from '../shared/containers/AssignForm.jsx';

function Order({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}/schedule/:onlineOrderId/edit`} component={OrderScheduleEditPage} />
      <Route path={`${url}/assign/:onlineOrderId`} component={AssignForm} />
      <Route path={`${url}`} component={OrderListBoard} />
    </Switch>
  );
}

export default Order;
