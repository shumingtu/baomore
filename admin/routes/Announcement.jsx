// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import AnnouncementList from '../containers/Announcement/AnnouncementList.jsx';
import AnnouncementCreateForm from '../containers/Announcement/AnnouncementCreateForm.jsx';
import AnnouncementEditForm from '../containers/Announcement/AnnouncementEditForm.jsx';

function Announcement({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}/create`} component={AnnouncementCreateForm} />
      <Route path={`${url}/:announcementId/edit`} component={AnnouncementEditForm} />
      <Route path={`${url}`} component={AnnouncementList} />
    </Switch>
  );
}

export default Announcement;
