// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import SystemInfoMember from '../containers/SystemInfo/SystemInfoMember.jsx';

function SystemMember({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  const type = url === '/systemInfo/employee' ? '包膜師' : '主任包膜師';

  return (
    <Switch>
      <Route exact path={`${url}`} component={props => <SystemInfoMember type={type} {...props} />} />
    </Switch>
  );
}

export default SystemMember;
