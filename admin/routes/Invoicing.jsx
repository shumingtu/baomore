// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import InvoicingListBoard from '../containers/Invoicing/InvoicingListBoard.jsx';
import CreateOrderForm from '../containers/Invoicing/CreateOrderForm.jsx';

function Invoicing({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}/createOrder`} component={CreateOrderForm} />
      <Route path={`${url}`} component={InvoicingListBoard} />
    </Switch>
  );
}

export default Invoicing;
