// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import RollbackMainBoard from '../containers/Rollback/RollbackMainBoard.jsx';

function Rollback({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}`} component={RollbackMainBoard} />
    </Switch>
  );
}

export default Rollback;
