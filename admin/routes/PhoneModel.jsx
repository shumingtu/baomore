// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import PhoneModelBoard from '../containers/PhoneModel/PhoneModelBoard.jsx';
import PhoneElementEditPage from '../containers/PhoneModel/PhoneElementEditPage.jsx';
import PhoneElementCreatePage from '../containers/PhoneModel/PhoneElementCreatePage.jsx';

function PhoneModel({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route exact path={`${url}`} component={PhoneModelBoard} />
      <Route exact path={`${url}/:modelId/:brandId/create`} component={PhoneElementCreatePage} />
      <Route exact path={`${url}/:colorId/edit`} component={PhoneElementEditPage} />
    </Switch>
  );
}

export default PhoneModel;
