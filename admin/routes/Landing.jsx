// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import LandingMainBoard from '../containers/Landing/LandingMainBoard.jsx';

function Landing({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}`} component={LandingMainBoard} />
    </Switch>
  );
}

export default Landing;
