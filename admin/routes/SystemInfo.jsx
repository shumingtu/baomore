// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import SystemInfoBoard from '../containers/SystemInfo/SystemInfoBoard.jsx';

function SystemInfo({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}`} component={SystemInfoBoard} />
    </Switch>
  );
}

export default SystemInfo;
