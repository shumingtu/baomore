// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import MemberManagementBoard from '../containers/Member/MemberManagementBoard.jsx';
import ConsumerEditForm from '../containers/Member/ConsumerEditForm.jsx';

function MemberManagement({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route exact path={`${url}`} component={MemberManagementBoard} />
      <Route path={`${url}/edit/:memberId`} component={ConsumerEditForm} />
    </Switch>
  );
}

export default MemberManagement;
