import gql from 'graphql-tag';

export const DELETE_VENDOR = gql`
  mutation deleteVendor(
    $id: Int!
  ) {
    deleteVendor(
      id: $id
    ) {
      id
      message
      status
    }
  }
`;

export const EDIT_VENDOR = gql`
  mutation editVendor(
    $id: Int!
    $name: String!
    $contactPersonName: String!
    $phone: String!
    $address: String!
    $settings: [SettingObject]
  ) {
    editVendor(
      id: $id
      name: $name
      contactPersonName: $contactPersonName
      phone: $phone
      address: $address
      settings: $settings
    ) {
      id
      name
      contactPersonName
      phone
      address
      VendorSettings {
        id
        type
        value
        VendorId
        MemberId
      }
    }
  }
`;

export const CREATE_VENDOR = gql`
  mutation createVendor(
    $name: String!
    $contactPersonName: String!
    $phone: String!
    $address: String!
    $settings: [SettingObject]
  ) {
    createVendor(
      name: $name
      contactPersonName: $contactPersonName
      phone: $phone
      address: $address
      settings: $settings
    ) {
      id
      name
      contactPersonName
      phone
      address
      VendorSettings {
        id
        type
        value
        VendorId
        MemberId
      }
    }
  }
`;

export default null;
