import gql from 'graphql-tag';

export const CREATE_PNSUBCATEGORY = gql`
  mutation createPNSubCategory(
    $name: String!
    $picture: String
    $isOnSale: Boolean!
    $PNCategoryId: Int!
    $PNSubCategoryId: Int
  ) {
    createPNSubCategory(
      name: $name
      picture: $picture
      isOnSale: $isOnSale
      PNCategoryId: $PNCategoryId
      PNSubCategoryId: $PNSubCategoryId
    ) {
      id
      name
      picture
    }
  }
`;

export const EDIT_PNSUBCATEGORY = gql`
  mutation editPNSubCategory($id: Int!, $name: String!, $picture: String, $isOnSale: Boolean!) {
    editPNSubCategory(
      id: $id
      name: $name
      picture: $picture
      isOnSale: $isOnSale
    ) {
      id
      name
      picture
      isOnSale
    }
  }
`;

export const DELETE_PNSUBCATEGORY = gql`
  mutation deletePNSubCategory($id: Int!) {
    deletePNSubCategory(id: $id) {
      id
      status
      message
    }
  }
`;
