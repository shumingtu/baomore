import gql from 'graphql-tag';

export const CREATE_CLOSEDORDERSETTING = gql`
  mutation createClosedOrderSetting($closedDay: Int!, $closedTime: String!, $code: String) {
    createClosedOrderSetting(closedDay: $closedDay, closedTime: $closedTime, code: $code) {
      id
      closedDay
      closedTime
      PNTable {
        id
        code
      }
    }
  }
`;

export const EDIT_CLOSEDORDERSETTING = gql`
  mutation editClosedOrderSetting($id: Int!, $closedDay: Int!, $closedTime: String!, $code: String) {
    editClosedOrderSetting(id: $id, closedDay: $closedDay, closedTime: $closedTime, code: $code) {
      id
      closedDay
      closedTime
      PNTable {
        id
        code
      }
    }
  }
`;

export const DELETE_CLOSEDORDERSETTING = gql`
  mutation deleteClosedOrderSetting($id: Int!) {
    deleteClosedOrderSetting(id: $id) {
      id
      status
      message
    }
  }
`;
