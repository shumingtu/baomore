import gql from 'graphql-tag';

export const EDIT_ROLE = gql`
  mutation editRole($roleId: Int!, $name: String!, $actionCodes: [Int]!) {
    editRole(
      roleId: $roleId,
      name: $name,
      actionCodes: $actionCodes,
    ) {
      id
      name
      actions {
        name
        code
      }
    }
  }
`;

export default null;
