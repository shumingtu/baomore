import gql from 'graphql-tag';

export const SALE_START = gql`
  mutation saleStart(
    $startTime: String!
    $storeId: Int!
    $vat: String
    $saleCode: String
    $productCode: String!
  ) {
    saleStart(
      startTime: $startTime
      storeId: $storeId
      vat: $vat
      saleCode: $saleCode
      productCode: $productCode
    ) {
      id
      message
      status
    }
  }
`;

export const SALE_STOP = gql`
  mutation saleStop(
    $endTime: String!
    $recordId: Int!
  ) {
    saleStop(
      endTime: $endTime
      recordId: $recordId
    ) {
      id
      message
      status
    }
  }
`;
