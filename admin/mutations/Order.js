import gql from 'graphql-tag';

export const ASSIGN_ORDER_TO_EMPLOYEE = gql`
  mutation assignOrderToEmployee(
    $onlineOrderId: String!,
    $storeId: Int!,
    $date: String!,
    $time: String!,
    $employeeId: Int!,
  ) {
    assignOrderToEmployee(
      onlineOrderId: $onlineOrderId,
      storeId: $storeId,
      date: $date,
      time: $time,
      employeeId: $employeeId,
    ) {
      id
      picture
      name
      phone
      email
      store {
        id
        name
      }
      time
      employee {
        id
        name
      }
      date
      payedStatus
      createdAt
      shippingStatus
    }
  }
`;

export const CREATE_ORDER = gql`
  mutation createOrder(
    $employeeId: Int!,
    $code: String!,
    $quantity: Int!,
    $productType: String!,
    $storeId: Int!,
  ) {
    createOrder(
      employeeId: $employeeId,
      code: $code,
      quantity: $quantity,
      productType: $productType,
      storeId: $storeId,
    ) {
      id
      quantity
      status
      Member {
        id
        name
      }
      pnTable {
        id
        picture
        code
        pnCategory {
          id
          name
        }
        bigSubCategory {
          id
          name
        }
        smallSubCategory {
          id
          name
        }
        name
        price
        onlinePrice
        vendor {
          id
          name
        }
        suitableDevice
        description
      }
      store {
        id
        name
        channel {
          id
          name
        }
        district {
          id
          name
        }
      }
    }
  }
`;

export const EDIT_ORDER_BY_ADMIN = gql`
  mutation editOrderByAdmin($orderId: Int!, $quantity: Int!, $storeId: Int!) {
    editOrderByAdmin(orderId: $orderId, quantity: $quantity, storeId: $storeId) {
      id
      quantity
      status
      Member {
        id
        name
      }
      pnTable {
        id
        picture
        code
        pnCategory {
          id
          name
        }
        bigSubCategory {
          id
          name
        }
        smallSubCategory {
          id
          name
        }
        name
        price
        onlinePrice
        vendor {
          id
          name
        }
        suitableDevice
        description
      }
      store {
        id
        name
        channel {
          id
          name
        }
        district {
          id
          name
        }
      }
    }
  }
`;

export const DELETE_ORDER = gql`
  mutation deleteOrderByAdmin($orderId: Int!) {
    deleteOrderByAdmin(orderId: $orderId) {
      id
      status
      message
    }
  }
`;

export const CHANGE_SCHEDULE = gql`
  mutation changeSchedule(
    $onlineOrderId: String!
    $date: String!
    $time: String!
    $employeeId: Int!
    $storeId: Int!
  ) {
    changeSchedule(
      onlineOrderId: $onlineOrderId
      date: $date
      time: $time
      employeeId: $employeeId
      storeId: $storeId
    ) {
      id
      picture
      name
      phone
      email
      store {
        id
        name
      }
      time
      employee {
        id
        name
      }
      date
      payedStatus
      createdAt
      shippingStatus
    }
  }
`;

export const CANCEL_SCHEDULE = gql`
  mutation cancelSchedule($onlineOrderId: String!) {
    cancelSchedule(
      onlineOrderId: $onlineOrderId
    ) {
      id
      picture
      name
      phone
      email
      store {
        id
        name
      }
      time
      employee {
        id
        name
      }
      date
      payedStatus
      createdAt
      shippingStatus
    }
  }
`;

export const EDIT_ORDER_BY_LINEBOT = gql`
  mutation editOrderByLineBot(
    $orderId: Int!
    $quantity: Int!
    $storeId: Int!
  ) {
    editOrderByLineBot(
      orderId: $orderId
      quantity: $quantity
      storeId: $storeId
    ) {
      id
      quantity
      status
      pnTable {
        id
        code
        name
      }
    }
  }
`;

export const DELETE_ORDER_BY_LINEBOT = gql`
  mutation deleteOrderByLineBot($orderId: Int!) {
    deleteOrderByLineBot(orderId: $orderId) {
      id
      status
      message
    }
  }
`;

export default null;
