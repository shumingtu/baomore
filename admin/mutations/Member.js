import gql from 'graphql-tag';

export const TOGGLE_ARCHIVE = gql`
  mutation toggleMemberArchive($id: Int!) {
    toggleMemberArchive(id: $id) {
      id
      status
      message
    }
  }
`;

export const EDIT_CONSUMER = gql`
  mutation editMemberForConsumer(
    $id: Int!
    $lineId: String!
    $birthday: String!
    $name: String!,
    $phone: String!,
    $email: String!,
    $gender: String!,
  ) {
    editMemberForConsumer(
      id: $id
      lineId: $lineId
      birthday: $birthday
      name: $name
      phone: $phone
      email: $email
      gender: $gender
    ) {
      id
      lineId
      name
      birthday
      phone
      email
      gender
    }
  }
`;

export const DELETE_MEMBER = gql`
  mutation deleteMember(
    $id: Int!
  ) {
    deleteMember(
      id: $id
    ) {
      id
      message
      status
    }
  }
`;

export const EDIT_MEMBER = gql`
  mutation editMemberForAdmin(
    $memberId: Int!,
    $serialNumber: String,
    $name: String!,
    $email: String,
    $gender: String,
    $birthday: String,
    $phone: String,
    $areaId: Int,
    $directorId: Int,
    $roleIds: [Int]!,
    $lineId: String!,
    $lineAccount: String,
  ) {
    editMemberForAdmin(
      memberId: $memberId,
      serialNumber: $serialNumber,
      name: $name,
      email: $email,
      gender: $gender,
      birthday: $birthday,
      phone: $phone,
      areaId: $areaId,
      directorId: $directorId,
      roleIds: $roleIds,
      lineId: $lineId,
      lineAccount: $lineAccount,
    ) {
      id
      name
      serialNumber
      lineId
      lineAccount
      area {
        id
        name
      }
      director {
        id
        name
      }
      email
      gender
      birthday
      phone
      roles {
        id
        name
      }
    }
  }
`;

export const CREATE_MEMBER = gql`
  mutation createMember(
    $serialNumber: String,
    $name: String!,
    $email: String,
    $gender: String,
    $birthday: String,
    $phone: String,
    $areaId: Int,
    $directorId: Int,
    $roleIds: [Int]!,
    $lineId: String!,
  ) {
    createMember(
      serialNumber: $serialNumber,
      name: $name,
      email: $email,
      gender: $gender,
      birthday: $birthday,
      phone: $phone,
      areaId: $areaId,
      directorId: $directorId,
      roleIds: $roleIds,
      lineId: $lineId,
    ) {
      id
      name
      lineId
      lineAccount
      roles {
        id
        name
      }
    }
  }
`;

export default null;
