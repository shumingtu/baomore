import gql from 'graphql-tag';

export const TRANSFER_ITEMS = gql`
  mutation transfer(
    $qrcode: String!
    $employeeId: Int!
    $remark: String!
  ) {
    transfer(
      qrcode: $qrcode
      employeeId: $employeeId
      remark: $remark
    ) {
      id
      message
      status
    }
  }
`;

export default null;
