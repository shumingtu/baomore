import gql from 'graphql-tag';

export const CREATE_ANNOUNCEMENT = gql`
  mutation createAnnouncement($title: String!, $content: String!) {
    createAnnouncement(
      title: $title
      content: $content
    ) {
      id
      title
      content
      createdAt
    }
  }
`;

export const EDIT_ANNOUNCEMENT = gql`
  mutation editAnnouncement($id: Int!, $title: String!, $content: String!) {
    editAnnouncement(
      id: $id
      title: $title
      content: $content
    ) {
      id
      title
      content
      createdAt
    }
  }
`;

export const DELETE_ANNOUNCEMENT = gql`
  mutation deleteAnnouncement($id: Int!) {
    deleteAnnouncement(
      id: $id
    ) {
      id
      status
      message
    }
  }
`;
