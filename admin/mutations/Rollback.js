import gql from 'graphql-tag';

export const ROLLBACK_FOR_CUSTOMER = gql`
  mutation rollBackForCustomer(
    $quantity: Int!
    $pnTableId: Int!
    $picture: String
    $storeId: Int
    $remark: String
  ) {
    rollBackForCustomer(
      quantity: $quantity
      pnTableId: $pnTableId
      picture: $picture
      storeId: $storeId
      remark: $remark
    ) {
      id
      message
      status
    }
  }
`;

export const INVENTORY_ROLLBACK = gql`
  mutation inventoryRollback(
    $qrcode: String!
    $picture: String
    $storeId: Int
    $remark: String
    $rollbackType: String!
    $productType: String,
  ) {
    inventoryRollback(
      qrcode: $qrcode
      picture: $picture
      storeId: $storeId
      remark: $remark
      rollbackType: $rollbackType
      productType: $productType
    ) {
      id
      message
      status
    }
  }
`;

export const VERIFY_BEHAVIOR_RECORD = gql`
  mutation verifyBehaviorRecords($recordIds: [Int]!) {
    verifyBehaviorRecords(
      recordIds: $recordIds
    ) {
      behaviorRecords {
        id
        rollbackType
        picture
        remarks
        createdAt
        member {
          id
          name
          serialNumber
        }
        vendor {
          id
          name
        }
        store {
          id
          name
        }
        pnTable {
          id
          code
          name
        }
        pnCategory {
          id
          name
        }
        approver {
          id
          name
        }
      }
      errorRecords {
        id
        rollbackType
        picture
        remarks
        createdAt
        member {
          id
          name
          serialNumber
        }
        vendor {
          id
          name
        }
        store {
          id
          name
        }
        pnTable {
          id
          code
          name
        }
        pnCategory {
          id
          name
        }
        approver {
          id
          name
        }
      }
    }
  }
`;

export default null;
