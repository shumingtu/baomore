import gql from 'graphql-tag';

export const PUNCH = gql`
  mutation punch(
    $punchTime: String!
    $latitude: Float!
    $longitude: Float!
    $storeId: Int!
  ) {
    punch(
      punchTime: $punchTime
      latitude: $latitude
      longitude: $longitude
      storeId: $storeId
    ) {
      id
      message
      status
    }
  }
`;

export const MANAGER_PUNCH = gql`
  mutation managerPunch(
    $punchStartTime: String!
    $punchEndTime: String!
    $grade: Float!
    $remark: String!
    $employeeId: Int!
    $storeId: Int!
  ) {
    managerPunch(
      punchStartTime: $punchStartTime
      punchEndTime: $punchEndTime
      grade: $grade
      remark: $remark
      employeeId: $employeeId
      storeId: $storeId
    ) {
      id
      message
      status
    }
  }
`;

export default null;
