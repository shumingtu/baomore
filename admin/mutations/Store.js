import gql from 'graphql-tag';

export const TOGGLE_ARCHIVE = gql`
  mutation toggleStoreArchive($id: Int!) {
    toggleStoreArchive(id: $id) {
      id
      status
      message
    }
  }
`;

export const DELETE_STORE = gql`
  mutation deleteStore(
    $id: Int!
  ) {
    deleteStore(
      id: $id
    ) {
      id
      message
      status
    }
  }
`;

export const EDIT_STORE = gql`
  mutation editStore(
    $id: Int!
    $areaId: Int!
    $channelId: Int!
    $districtId: Int!
    $name: String!
    $phone: String!
    $address: String!
    $mainEmployeeId: Int
    $backupEmployeeId: Int
    $manager: String!
    $latitude: Float!
    $longitude: Float!
  ) {
    editStore(
      id: $id
      areaId: $areaId
      channelId: $channelId
      districtId: $districtId
      name: $name
      phone: $phone
      address: $address
      mainEmployeeId: $mainEmployeeId
      backupEmployeeId: $backupEmployeeId
      manager: $manager
      latitude: $latitude
      longitude: $longitude
    ) {
      id
      name
      phone
      address
      manager
      latitude
      longitude
      area {
        id
        name
      }
      channel {
        id
        name
      }
      members {
        memberId: id
        name
        serialNumber
        memberStore {
          type
        }
      }
      district {
        id
        name
        city {
          id
          name
        }
      }
    }
  }
`;

export const CREATE_STORE = gql`
  mutation createStore(
    $areaId: Int!
    $channelId: Int!
    $districtId: Int!
    $name: String!
    $phone: String!
    $address: String!
    $mainEmployeeId: Int
    $backupEmployeeId: Int
    $manager: String!
    $latitude: Float!
    $longitude: Float!
  ) {
    createStore(
      areaId: $areaId
      channelId: $channelId
      districtId: $districtId
      name: $name
      phone: $phone
      address: $address
      mainEmployeeId: $mainEmployeeId
      backupEmployeeId: $backupEmployeeId
      manager: $manager
      latitude: $latitude
      longitude: $longitude
    ) {
      id
      name
      phone
      address
      manager
      latitude
      longitude
      area {
        id
        name
      }
      channel {
        id
        name
      }
      members {
        memberId: id
        name
        serialNumber
        memberStore {
          type
        }
      }
      district {
        id
        name
        city {
          id
          name
        }
      }
    }
  }
`;

export default null;
