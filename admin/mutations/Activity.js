import gql from 'graphql-tag';

export const TOGGLE_ACTIVITY = gql`
  mutation toggleActivity($id: Int!) {
    toggleActivity(id: $id) {
      id
      status
      message
    }
  }
`;

export const EDIT_ACTIVITY_SOCIAL_CATEGORY = gql`
  mutation editSocialCategory($id: Int!, $name: String!, $link: String) {
    editSocialCategory(id: $id, name: $name, link: $link) {
      id
      name
      link
    }
  }
`;

export const CREATE_ACTIVITY_CATEGORY = gql`
  mutation createActivityCategory(
    $name: String!
    $desktopImg: String!
    $mobileImg: String!
  ) {
    createActivityCategory(
      name: $name
      desktopImg: $desktopImg
      mobileImg: $mobileImg
    ) {
      id
      name
      desktopImg
      mobileImg
    }
  }
`;

export const EDIT_ACTIVITY_CATEGORY = gql`
  mutation editActivityCategory(
    $id: Int!
    $name: String!
    $desktopImg: String!
    $mobileImg: String!
  ) {
    editActivityCategory(
      id: $id
      name: $name
      desktopImg: $desktopImg
      mobileImg: $mobileImg
    ) {
      id
      name
      desktopImg
      mobileImg
    }
  }
`;

export const DELETE_ACTIVITY_CATEGORY = gql`
  mutation deleteActivityCategory($id: Int!) {
    deleteActivityCategory(id: $id) {
      id
      status
      message
    }
  }
`;

export const CREATE_ACTIVITY = gql`
  mutation createActivity(
    $name: String!
    $desktopImg: String!
    $mobileImg: String!
    $description: String
    $fragments: [ActivityFragmentInput]
    $categoryId: Int!
  ) {
    createActivity(
      name: $name
      desktopImg: $desktopImg
      mobileImg: $mobileImg
      description: $description
      fragments: $fragments
      categoryId: $categoryId
    ) {
      id
      name
      desktopImg
      mobileImg
      description
    }
  }
`;

export const EDIT_ACTIVITY = gql`
  mutation editActivity(
    $id: Int!
    $name: String!
    $desktopImg: String!
    $mobileImg: String!
    $description: String
    $fragments: [ActivityFragmentInput]
  ) {
    editActivity(
      id: $id
      name: $name
      desktopImg: $desktopImg
      mobileImg: $mobileImg
      description: $description
      fragments: $fragments
    ) {
      id
      name
      desktopImg
      mobileImg
      description
    }
  }
`;

export const DELETE_ACTIVITY = gql`
  mutation deleteActivity($id: Int!) {
    deleteActivity(id: $id) {
      id
      status
      message
    }
  }
`;
