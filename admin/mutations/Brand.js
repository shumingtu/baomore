import gql from 'graphql-tag';

export const DELETE_BRAND = gql`
  mutation deleteBrand($id: Int!) {
    deleteBrand(
      id: $id
    ) {
      id
      status
      message
    }
  }
`;

export const DELETE_BRAND_MODEL = gql`
  mutation deleteBrandModel($id: Int!) {
    deleteBrandModel(
      id: $id
    ) {
      id
      status
      message
    }
  }
`;

export const DELETE_BRAND_MODEL_COLOR = gql`
  mutation deleteBrandModelColor($id: Int!) {
    deleteBrandModelColor(
      id: $id
    ) {
      id
      status
      message
    }
  }
`;

export const EDIT_BRAND = gql`
  mutation editBrand($id: Int!, $name: String!) {
    editBrand(
      id: $id
      name: $name
    ) {
      id
      name
    }
  }
`;

export const EDIT_BRAND_MODEL = gql`
  mutation editBrandModel($id: Int!, $name: String!) {
    editBrandModel(
      id: $id
      name: $name
    ) {
      id
      name
      BrandId
    }
  }
`;

export const EDIT_BRAND_MODEL_COLOR = gql`
  mutation editBrandModelColor(
    $id: Int!,
    $name: String!,
    $phoneBg: String!,
    $phoneMask: String!,
    $phoneCover: String!,
    $brandModelId: Int!) {
      editBrandModelColor(
        id: $id
        name: $name
        phoneBg: $phoneBg
        phoneCover: $phoneCover
        phoneMask: $phoneMask
        brandModelId: $brandModelId
      ) {
        id
        name
        phoneBg
        phoneMask
        phoneCover
        brand {
          id
          name
        }
        model {
          id
          name
        }
      }
  }
`;

export const CREATE_BRAND = gql`
  mutation createBrand($name: String!) {
    createBrand(name: $name) {
      id
      name
    }
  }
`;

export const CREATE_BRAND_MODEL = gql`
  mutation createBrandModel($name: String!, $brandId: Int!) {
    createBrandModel(
      name: $name
      brandId: $brandId
    ) {
      id
      name
      BrandId
    }
  }
`;

export const CREATE_BRAND_MODEL_COLOR = gql`
  mutation createBrandModelColor($name: String!, $modelId: Int!, $phoneBg: String!, $phoneMask: String!, $phoneCover: String!,) {
    createBrandModelColor(
      name: $name
      modelId: $modelId
      phoneBg: $phoneBg
      phoneMask: $phoneMask
      phoneCover: $phoneCover
    ) {
      id
      name
      BrandModelId
      phoneBg
      phoneMask
      phoneCover
    }
  }
`;
