import gql from 'graphql-tag';

export const CREATE_LANDING = gql`
  mutation createLanding($desktopImg: String!, $mobileImg: String!, $link: String) {
    createLanding(
      desktopImg: $desktopImg
      mobileImg: $mobileImg
      link: $link
    ) {
      id
      desktopImg
      mobileImg
      link
    }
  }
`;

export const EDIT_LANDING = gql`
  mutation editLanding($id: Int!, $desktopImg: String!, $mobileImg: String!, $link: String) {
    editLanding(
      id: $id
      desktopImg: $desktopImg
      mobileImg: $mobileImg
      link: $link
    ) {
      id
      desktopImg
      mobileImg
      link
    }
  }
`;

export const DELETE_LANDING = gql`
  mutation deleteLanding($id: Int!) {
    deleteLanding(
      id: $id
    ) {
      id
      status
      message
    }
  }
`;

export const CREATE_BANNER = gql`
  mutation createBanner($picture: String!, $link: String, $mobileImg: String) {
    createBanner(
      picture: $picture
      link: $link
      mobileImg: $mobileImg
    ) {
      id
      picture
      link
      mobileImg
    }
  }
`;

export const EDIT_BANNER = gql`
  mutation editBanner($id: Int!, $picture: String!, $link: String, $mobileImg: String) {
    editBanner(
      id: $id
      picture: $picture
      link: $link
      mobileImg: $mobileImg
    ) {
      id
      picture
      link
      mobileImg
    }
  }
`;

export const DELETE_BANNER = gql`
  mutation deleteBanner($id: Int!) {
    deleteBanner(
      id: $id
    ) {
      id
      status
      message
    }
  }
`;
