import gql from 'graphql-tag';

export const DELETE_PNTABLE_ITEM = gql`
  mutation deletePNTableItem($id: Int!) {
    deletePNTableItem(
      id: $id
    ) {
      id
      status
      message
    }
  }
`;

export const CREATE_PNTABLE_ITEM = gql`
  mutation createPNTableItem(
    $code: String!
    $pnCategoryId: Int!
    $bigSubCategoryId: Int!
    $smallSubCategoryId: Int
    $BrandModelId: Int
    $VendorId: Int!
    $name: String!
    $picture: String!
    $price: Int!
    $device: String!
    $description: String!
    $isOnSale: Boolean!
    $onlinePrice: Int,
  ) {
    createPNTableItem(
      code: $code
      pnCategoryId: $pnCategoryId
      bigSubCategoryId: $bigSubCategoryId
      smallSubCategoryId: $smallSubCategoryId
      BrandModelId: $BrandModelId
      VendorId: $VendorId
      name: $name
      picture: $picture
      price: $price
      device: $device
      description: $description
      isOnSale: $isOnSale
      onlinePrice: $onlinePrice
    ) {
      id
      code
      name
      picture
      suitableDevice
      description
      # inOnSale
      price
      onlinePrice
      pnCategory {
        id
        name
      }
      bigSubCategory {
        id
        name
      }
      vendor {
        id
        name
      }
      smallSubCategory {
        id
        name
      }
    }
  }
`;

export const EDIT_PNTABLE_ITEM = gql`
  mutation editPNTableItem(
    $id: Int!
    $code: String!
    $pnCategoryId: Int!
    $bigSubCategoryId: Int!
    $smallSubCategoryId: Int
    $BrandModelId: Int
    $VendorId: Int!
    $name: String!
    $picture: String!
    $price: Int!
    $device: String!
    $description: String!
    $isOnSale: Boolean!
    $onlinePrice: Int,
  ) {
    editPNTableItem(
      id: $id
      code: $code
      pnCategoryId: $pnCategoryId
      bigSubCategoryId: $bigSubCategoryId
      smallSubCategoryId: $smallSubCategoryId
      BrandModelId: $BrandModelId
      VendorId: $VendorId
      name: $name
      picture: $picture
      price: $price
      device: $device
      description: $description
      isOnSale: $isOnSale
      onlinePrice: $onlinePrice
    ) {
      id
      code
      name
      picture
      suitableDevice
      description
      isOnSale
      price
      onlinePrice
      pnCategory {
        id
        name
      }
      bigSubCategory {
        id
        name
      }
      vendor {
        id
        name
      }
      smallSubCategory {
        id
        name
      }
      vendor {
        id
        name
      }
    }
  }
`;

export default null;
