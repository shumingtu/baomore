import gql from 'graphql-tag';

export const UPDATE_GROUP_ORDER_STATUS = gql`
  mutation updateGroupOrderStatus($id: Int!) {
    updateGroupOrderStatus(
      id: $id
    ) {
      id
    }
  }
`;

export default null;
