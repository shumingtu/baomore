import gql from 'graphql-tag';

export const PURCHASE_CONFIRM = gql`
  mutation purchase(
    $pnTableId: Int!
    $storeId: Int!
    $quantity: Int!
  ) {
    purchase(
      pnTableId: $pnTableId
      storeId: $storeId
      quantity: $quantity
    ) {
      pnTableId
      message
      status
      order {
        id
        quantity
        status
        store {
          id
          name
          channel {
            id
            name
          }
          district {
            id
            name
            city {
              id
              name
            }
          }
        }
      }
    }
  }
`;

export default null;
