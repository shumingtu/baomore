import gql from 'graphql-tag';

export const EMPLOYEE_LOGIN = gql`
  mutation employeeLogin(
    $code: String!
    $SN: String!
    $phone: String!
  ) {
    employeeLogin(
      code: $code
      SN: $SN
      phone: $phone
    ) {
      accessToken
      member {
        id
        serialNumber
        phone
        AreaId
      }
    }
  }
`;

export default null;
