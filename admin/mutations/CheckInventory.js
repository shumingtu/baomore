import gql from 'graphql-tag';

export const CHECK_INVENTORY = gql`
  mutation checkInventory($checkCode: String!) {
    checkInventory(
      checkCode: $checkCode
    ) {
      id
      message
      status
    }
  }
`;

export default null;
