import gql from 'graphql-tag';

export const CREATE_PNCATEGORY = gql`
  mutation createPNCategory($name: String!) {
    createPNCategory(
      name: $name
    ) {
      id
      name
    }
  }
`;

export const EDIT_PNCATEGORY = gql`
  mutation editPNCategory($id: Int!, $name: String!) {
    editPNCategory(
      id: $id
      name: $name
    ) {
      id
      name
    }
  }
`;

export const DELETE_PNCATEGORY = gql`
  mutation deletePNCategory($id: Int!) {
    deletePNCategory(id: $id) {
      id
      status
      message
    }
  }
`;
