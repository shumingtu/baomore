import moment from 'moment';

export default function getWeeksInMonth(month, year) {
  const monthDate = year ? moment(`${year}-${month}`, 'YYYY-MM').month(month - 1) : moment().month(month - 1);
  const weeks = [];
  const workingMonth = monthDate.month();
  let workingWeek;

  moment(`${year}-${month}-01`, 'YYYY-MM-DD');

  // Fill actived month dates
  while (monthDate.month() === workingMonth) {
    if (!workingWeek) workingWeek = [];

    workingWeek.push(monthDate.clone());

    monthDate.add(1, 'd');
    if (monthDate.day() === 1) {
      weeks.push(workingWeek);
      workingWeek = null;
    }
  }

  if (workingWeek) {
    weeks.push(workingWeek);
  }

  // Fill previous month dates
  const monthStart = weeks[0][0].clone();

  while (monthStart.day() !== 1) {
    weeks[0] = [
      monthStart.subtract(1, 'd').clone(),
      ...weeks[0],
    ];
  }

  // Fill next month dates;
  const monthEnd = weeks[weeks.length - 1][weeks[weeks.length - 1].length - 1].clone();

  while (monthEnd.day() !== 0) {
    weeks[weeks.length - 1] = [
      ...weeks[weeks.length - 1],
      monthEnd.add(1, 'd').clone(),
    ];
  }

  return weeks;
}
