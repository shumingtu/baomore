import {
  shipStatus,
  payStatus,
} from '../shared/order.js';

export function shippingStatusDecider(status) {
  const result = shipStatus.find(x => x.id === status);
  if (result) return result.name;
  return null;
}

export function payedStatusDecider(status) {
  const result = payStatus.find(x => x.id === status);
  if (result) return result.name;
  return null;
}
