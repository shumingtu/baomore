export function validateSalesForm(data) {
  if (!data) return null;

  const {
    areaId,
    channelId,
    cityId,
    storeId,
    vat,
    saleCode,
    itemId,
  } = data;

  const errors = {};

  if (!areaId) errors.areaId = 'required';
  if (!cityId) errors.cityId = 'required';
  if (!channelId) errors.channelId = 'required';
  if (!storeId) errors.storeId = 'required';
  if (!itemId) errors.itemId = 'required';
  // selectors
  if (channelId === '-1') errors.channelId = 'failed';
  if (cityId === '-1') errors.cityId = 'failed';
  if (storeId === '-1') errors.storeId = 'failed';

  if (!vat && !saleCode) {
    // 擇一必填
    errors.vat = 'failed';
    errors.saleCode = 'failed';
  }

  if (Object.keys(errors).length === 0) return null;

  return errors;
}

export default null;
