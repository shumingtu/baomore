export function validateOrderScheduleEditForm(data) {
  if (!data) return null;

  const {
    storeId,
    employeeId,
    date,
    time,
  } = data;

  const errors = {};

  if (!storeId) errors.storeId = 'required';
  if (!employeeId) errors.employeeId = 'required';
  if (!date) errors.date = 'required';
  if (!time) errors.time = 'required';
  // selectors
  if (storeId === '-1') errors.storeId = 'failed';
  if (employeeId === '-1') errors.employeeId = 'failed';
  if (time === '-1') errors.time = 'failed';

  if (Object.keys(errors).length === 0) return null;

  return errors;
}

export default null;
