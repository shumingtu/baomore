export function validateMemberCreateEdit(data, isNormalMember, isEmployee) {
  if (!data) return null;

  const {
    serialNumber,
    name,
    areaId,
    directorId,
    roles,
    lineId,
  } = data;

  const errors = {};

  if (!roles || !roles.length) errors.roles = 'failed';

  if (!isNormalMember) {
    if (!serialNumber) errors.serialNumber = 'failed';
    if (!areaId || areaId === '-1') errors.areaId = 'failed';
  }

  if (isEmployee) {
    if (!directorId || directorId === '-1') errors.directorId = 'failed';
  }

  if (!name) errors.name = 'failed';
  if (!lineId) errors.lineId = 'failed';

  if (Object.keys(errors).length === 0) return null;

  return errors;
}

export default null;
