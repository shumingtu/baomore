export function validateVendorForm(data) {
  if (!data) return null;

  const {
    contactPersonName,
    name,
    phone,
    settings,
    address,
  } = data;

  const errors = {};

  if (!name) errors.name = 'failed';
  if (!contactPersonName) errors.contactPersonName = 'failed';
  if (!phone) errors.phone = 'failed';
  if (!address) errors.address = 'failed';

  if (settings && settings.length) {
    const settingsArrayErrors = [];
    settings.forEach((setting, idx) => {
      const settingError = {};

      if (!setting.MemberId || setting.MemberId === '-1') {
        settingError.MemberId = 'failed';
        settingsArrayErrors[idx] = settingError;
      }

      if (!setting.type || setting.type === '-1') {
        settingError.type = 'failed';
        settingsArrayErrors[idx] = settingError;
      }

      if (!setting.value) {
        settingError.value = 'failed';
        settingsArrayErrors[idx] = settingError;
      }

      if (!setting.message) {
        settingError.message = 'failed';
        settingsArrayErrors[idx] = settingError;
      }

      return settingError;
    });

    if (settingsArrayErrors.length) {
      errors.settings = settingsArrayErrors;
    }
  }

  if (Object.keys(errors).length === 0) return null;

  return errors;
}

export default null;
