/* eslint no-restricted-globals: ["error", "event"] */

export function validateClosedOrderSetting(data, isEditDefaultSetting) {
  if (!data) return null;

  const {
    closedDay,
    closedTime,
    code,
  } = data;

  const errors = {};

  if (!closedDay) errors.closedDay = 'failed';
  if (!closedTime) errors.closedTime = 'failed';
  if (!isEditDefaultSetting) {
    if (!code) errors.code = 'failed';
  }
  if (Object.keys(errors).length === 0) return null;

  return errors;
}

export default null;
