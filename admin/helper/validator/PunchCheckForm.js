export function validatePunchCheckForm(data) {
  if (!data) return null;

  const {
    employeeId,
    startDate,
    endDate,
    grade,
    remark,
  } = data;

  const errors = {};

  if (!employeeId) errors.employeeId = 'required';
  if (!startDate) errors.startDate = 'required';
  if (!endDate) errors.endDate = 'required';
  if (!grade) errors.grade = 'failed';
  // selectors
  if (employeeId === '-1') errors.employeeId = 'failed';

  if (Object.keys(errors).length === 0) return null;

  return errors;
}

export default null;
