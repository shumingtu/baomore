export function validateDOAForm(data) {
  if (!data) return null;

  const {
    qrcode,
    picture,
    errorType,
    errorDesc,
  } = data;

  const errors = {};

  if (!errorType) errors.errorType = 'required';
  if (!qrcode) errors.qrcode = 'required';
  if (errorType && errorType === '-1') errors.errorType = 'required';
  if (picture && picture === 'uploading') errors.picture = 'uploading';

  if (errorType && errorType === '其他') {
    if (!errorDesc) errors.errorDesc = 'required';
  }

  if (Object.keys(errors).length === 0) return null;

  return errors;
}

export function validateFreeForm(data) {
  if (!data) return null;

  const {
    qrcode,
    picture,
    target,
    description,
  } = data;

  const errors = {};

  if (!target) errors.target = 'required';
  if (!description) errors.description = 'required';
  if (!qrcode) errors.qrcode = 'required';

  if (picture && picture === 'uploading') errors.picture = 'uploading';

  if (Object.keys(errors).length === 0) return null;

  return errors;
}

export function validateDestroyForm(data) {
  if (!data) return null;

  const {
    qrcode,
    errorType,
    errorDesc,
  } = data;

  const errors = {};

  if (!errorType) errors.errorType = 'required';
  if (!qrcode) errors.qrcode = 'required';
  if (errorType && errorType === '-1') errors.errorType = 'required';

  if (errorType && errorType === '其他') {
    if (!errorDesc) errors.errorDesc = 'required';
  }

  if (Object.keys(errors).length === 0) return null;

  return errors;
}

export default null;
