export function validateConsumberEditForm(data) {
  if (!data) return null;

  const {
    lineId,
    name,
    birthday,
    phone,
    email,
    gender,
  } = data;

  const errors = {};

  if (!lineId) errors.serialNumber = 'failed';
  if (!gender || gender === '-1') errors.gender = 'failed';
  if (!name) errors.name = 'failed';
  if (!phone) errors.phone = 'failed';
  if (!birthday) errors.birthday = 'failed';
  if (!email) errors.email = 'failed';

  if (Object.keys(errors).length === 0) return null;

  return errors;
}

export default null;
