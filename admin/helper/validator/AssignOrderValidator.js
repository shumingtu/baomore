export function validateAssignOrderForm(data) {
  if (!data) return null;

  const {
    employeeId,
    date,
    storeId,
    time,
  } = data;

  const errors = {};

  if (!date) errors.date = '必填';
  if (!employeeId || employeeId === '-1') errors.employeeId = 'failed';
  if (!storeId || storeId === '-1') errors.storeId = 'failed';
  if (!time || time === '-1') errors.time = 'failed';

  if (Object.keys(errors).length === 0) return null;

  return errors;
}

export default null;
