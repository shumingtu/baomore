/* eslint no-restricted-globals: ["error", "event"] */

export function validatePNSubCategoryCreateEdit(data) {
  if (!data) return null;

  const {
    name,
    isOnSale,
  } = data;

  const errors = {};

  if (!name) errors.name = 'failed';
  if (isOnSale !== true && isOnSale !== false && !isOnSale) errors.isOnSale = 'failed';

  if (Object.keys(errors).length === 0) return null;

  return errors;
}

export default null;
