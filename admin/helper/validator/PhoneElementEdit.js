export function validatePhoneEdit(data) {
  if (!data) return null;

  const {
    colorName,
    phoneBg,
    phoneMask,
    phoneCover,
    modelId,
    brandId,
  } = data;

  const errors = {};

  if (!colorName) errors.colorName = 'failed';
  if (!phoneBg) errors.phoneBg = 'failed';
  if (!phoneMask) errors.phoneMask = 'failed';
  if (!phoneCover) errors.phoneCover = 'failed';
  if (!modelId || modelId === -1) errors.modelId = 'failed';
  if (!brandId || brandId === -1) errors.brandId = 'failed';

  if (Object.keys(errors).length === 0) return null;

  return errors;
}

export default null;
