/* eslint no-restricted-globals: ["error", "event"] */

export function validatePNModelCreateEdit(data) {
  if (!data) return null;

  const {
    code,
    pnCategoryId,
    bigSubCategoryId,
    smallSubCategoryId,
    name,
    picture,
    price,
    device,
    brandId,
    modelId,
    VendorId,
    description,
    isOnSale,
    onlinePrice,
  } = data;

  const errors = {};

  if (!code) errors.code = 'failed';
  if (!pnCategoryId || pnCategoryId === '-1') errors.pnCategoryId = 'failed';
  if (!bigSubCategoryId || bigSubCategoryId === '-1') errors.bigSubCategoryId = 'failed';
  if (pnCategoryId !== '2') {
    if (!smallSubCategoryId || smallSubCategoryId === '-1') errors.smallSubCategoryId = 'failed';
  }
  if (!name) errors.name = 'failed';
  if (!picture) errors.picture = 'failed';
  if (!price || isNaN(parseInt(price, 10))) errors.price = 'failed';
  if (!device) errors.device = 'failed';
  if (!VendorId) errors.VendorId = 'failed';
  if (!description) errors.description = 'failed';
  if (pnCategoryId === '2') {
    if (!modelId || modelId === '-1') errors.modelId = 'failed';
    if (!brandId || brandId === '-1') errors.brandId = 'failed';
  }
  if (isOnSale !== true && isOnSale !== false && !isOnSale) errors.isOnSale = 'failed';
  if (isOnSale === true) {
    if (!onlinePrice || isNaN(parseInt(onlinePrice, 10))) errors.onlinePrice = 'failed';
  }

  if (Object.keys(errors).length === 0) return null;

  return errors;
}

export default null;
