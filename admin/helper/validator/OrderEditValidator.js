export function validateOrderEditForm(data) {
  if (!data) return null;

  const {
    storeId,
    quantity,
  } = data;

  const errors = {};

  if (!storeId || storeId === '-1') errors.storeId = 'failed';
  if (!quantity || quantity === '0') errors.quantity = 'failed';

  if (Object.keys(errors).length === 0) return null;

  return errors;
}

export default null;
