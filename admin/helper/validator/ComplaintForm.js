export function validateComplaintForm(data) {
  if (!data) return null;

  const {
    picture,
    vat,
    customerName,
    customerPhone,
    errorType,
    errorDesc,
    storeId,
    quantity,
  } = data;

  const errors = {};

  if (!picture) errors.picture = 'required';
  if (!storeId) errors.storeId = 'required';
  if (!quantity) errors.quantity = 'required';

  if (picture === 'uploading') errors.picture = 'uploading';

  if (errorType && errorType === '其他') {
    if (!errorDesc) errors.errorDesc = 'required';
  }

  if (Object.keys(errors).length === 0) return null;

  return errors;
}

export default null;
