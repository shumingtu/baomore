export function validateStoreForm(data) {
  if (!data) return null;

  const {
    areaId,
    cityId,
    channelId,
    districtId,
    name,
    phone,
    address,
    manager,
    latitude,
    longitude,
  } = data;

  const errors = {};

  if (!channelId || channelId === '-1') errors.channelId = 'failed';
  if (!cityId || cityId === '-1') errors.cityId = 'failed';
  if (!districtId || districtId === '-1') errors.districtId = 'failed';
  if (!areaId || areaId === '-1') errors.areaId = 'failed';
  if (!name) errors.name = 'failed';
  if (!phone) errors.phone = 'failed';
  if (!address) errors.address = 'failed';
  if (!manager) errors.manager = 'failed';
  if (!latitude) errors.latitude = 'failed';
  if (!longitude) errors.longitude = 'failed';


  if (Object.keys(errors).length === 0) return null;

  return errors;
}

export default null;
