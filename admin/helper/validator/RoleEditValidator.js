export function validateRoleEdit(data) {
  if (!data) return null;

  const {
    name,
    actions,
  } = data;

  const errors = {};

  if (!name) errors.name = 'failed';
  if (!actions || !actions.length) errors.actions = 'failed';

  if (Object.keys(errors).length === 0) return null;

  return errors;
}

export default null;
