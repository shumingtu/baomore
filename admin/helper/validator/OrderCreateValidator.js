export function validateOrderCreateForm(data) {
  if (!data) return null;

  const {
    employeeId,
    code,
    quantity,
    productType,
    storeId,
  } = data;

  const errors = {};

  if (!storeId || storeId === '-1') errors.storeId = 'failed';
  if (!quantity || quantity === '0') errors.quantity = 'failed';
  if (!employeeId || employeeId === '-1') errors.employeeId = 'failed';
  if (!code) errors.code = 'failed';
  if (!productType) errors.productType = 'failed';

  if (Object.keys(errors).length === 0) return null;

  return errors;
}

export default null;
