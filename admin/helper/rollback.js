export function remarksGetter(remarks) {
  const remarkCache = {};
  if (remarks && Array.isArray(remarks)) {
    remarks.forEach((remark) => {
      if (remark.startsWith('發票號碼')) {
        remarkCache.vat = remark.replace(/發票號碼：/g, '');
      }
      if (remark.startsWith('客戶姓名')) {
        remarkCache.customerName = remark.replace(/客戶姓名：/g, '');
      }
      if (remark.startsWith('手機號碼')) {
        remarkCache.customerPhone = remark.replace(/手機號碼：/g, '');
      }
      if (remark.startsWith('異常類型')) {
        remarkCache.errorType = remark.replace(/異常類型：/g, '');
      }
      if (remark.startsWith('異常說明')) {
        remarkCache.errorDesc = remark.replace(/異常說明：/g, '');
      }
      if (remark.startsWith('對象')) {
        remarkCache.target = remark.replace(/對象：/g, '');
      }
      if (remark.startsWith('說明')) {
        remarkCache.socialDesc = remark.replace(/說明：/g, '');
      }
    });
  }

  return remarkCache;
}

export function rollbackTableDataFormatter(recordList) {
  if (!recordList) return [];

  return recordList.map((r) => {
    const remarkCache = remarksGetter(r.remarks);

    return ({
      id: r.id,
      rollbackType: r.rollbackType || null,
      picture: r.picture || null,
      createdAt: r.createdAt || null,
      memberName: (r.member && r.member.name) || null,
      vendorName: (r.vendor && r.vendor.name) || null,
      storeName: (r.store && r.store.name) || null,
      pnTableCode: (r.pnTable && r.pnTable.code) || null,
      pnTableName: (r.pnTable && r.pnTable.name) || null,
      pnCategoryName: (r.pnCategory && r.pnCategory.name) || null,
      approverName: (r.approver && r.approver.name) || null,
      errorType: remarkCache.errorType || null,
      vat: remarkCache.vat || null,
      customerName: remarkCache.customerName || null,
      customerPhone: remarkCache.customerPhone || null,
      target: remarkCache.target || null,
      socialDesc: remarkCache.socialDesc || null,
    });
  });
}
