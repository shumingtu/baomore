function upload(file) {
  return new Promise(async (resolve, reject) => {
    const fd = new FormData();
    fd.append('file', file);

    try {
      const result = await fetch(`${API_HOST}/upload`, {
        method: 'POST',
        body: fd,
      }).then(res => res.json());

      if (result) {
        resolve(result.url);
      }
    } catch (e) {
      reject(e);
    }
  });
}

export default async function uploadFile(file, cb) {
  cb('uploading');

  const url = await upload(file);

  cb(url);
}
