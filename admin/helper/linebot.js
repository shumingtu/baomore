import { pnCategoryTypes } from '../shared/linebot.js';

export function getInventoryIdFromItemId(itemId) {
  if (!itemId) return null;
  if (itemId === 'undefined') return null;

  return itemId.slice(0, itemId.length - 3);
}

export function pnCategoryTypeIsProtector(pnCategoryType) {
  if (pnCategoryType) {
    const typeName = pnCategoryTypes.find(t => t.id === pnCategoryType)
      ? pnCategoryTypes.find(t => t.id === pnCategoryType).name
      : null;

    return typeName === '保貼';
  }

  return null;
}

export function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

export function isPerformancePositive(num) {
  if (parseFloat(num) < 0) return false;

  return true;
}

export function performanceTableDataFormatted(data) {
  if (!data) return [];

  const {
    targets,
    amounts,
    sales,
    achieveRate,
    asp,
    mom,
    workHours,
    expectAchieves,
    expectProfits,
  } = data;

  const KEY_0 = 'id'; // Table key
  const KEY_1 = 'fieldName'; // 欄位
  const KEY_2 = 'baomore'; // 包膜
  const KEY_3 = 'protector'; // 保貼
  const KEY_4 = 'total'; // 小記

  const wrapTarget = targets && Array.isArray(targets) ? targets : [];
  const wrapAmount = amounts && Array.isArray(amounts) ? amounts : [];
  const wrapSales = sales && Array.isArray(sales) ? sales : [];
  const wrapAchieveRate = achieveRate && Array.isArray(achieveRate) ? achieveRate : [];
  const wrapAsp = asp && Array.isArray(asp) ? asp : [];
  const wrapMom = mom && Array.isArray(mom) ? mom : [];
  const wrapWorkHours = workHours && Array.isArray(workHours) ? workHours : [];
  const wrapExpectAchieves = expectAchieves && Array.isArray(expectAchieves) ? expectAchieves : [];
  const wrapExpectProfits = expectProfits && Array.isArray(expectProfits) ? expectProfits : [];

  const targetData = {
    [KEY_0]: 'target-data',
    [KEY_1]: '目標',
    [KEY_2]: {
      value: wrapTarget[0] ? numberWithCommas(wrapTarget[0]) : 0,
      isRed: true,
    },
    [KEY_3]: {
      value: wrapTarget[1] ? numberWithCommas(wrapTarget[1]) : 0,
      isRed: true,
    },
    [KEY_4]: {
      value: wrapTarget[2] ? numberWithCommas(wrapTarget[2]) : 0,
      isRed: true,
    },
  };
  const amountData = {
    [KEY_0]: 'amount-data',
    [KEY_1]: '量',
    [KEY_2]: {
      value: wrapAmount[0] ? numberWithCommas(wrapAmount[0]) : 0,
      isRed: !isPerformancePositive(wrapAmount[0]),
    },
    [KEY_3]: {
      value: wrapAmount[1] ? numberWithCommas(wrapAmount[1]) : 0,
      isRed: !isPerformancePositive(wrapAmount[1]),
    },
    [KEY_4]: {
      value: wrapAmount[2] ? numberWithCommas(wrapAmount[2]) : 0,
      isRed: !isPerformancePositive(wrapAmount[2]),
    },
  };
  const salesData = {
    [KEY_0]: 'sales-data',
    [KEY_1]: '額',
    [KEY_2]: {
      value: wrapSales[0] ? numberWithCommas(wrapSales[0]) : 0,
      isRed: !isPerformancePositive(wrapSales[0]),
    },
    [KEY_3]: {
      value: wrapSales[1] ? numberWithCommas(wrapSales[1]) : 0,
      isRed: !isPerformancePositive(wrapSales[1]),
    },
    [KEY_4]: {
      value: wrapSales[2] ? numberWithCommas(wrapSales[2]) : 0,
      isRed: !isPerformancePositive(wrapSales[2]),
    },
  };
  const achieveRateData = {
    [KEY_0]: 'achieveRate-data',
    [KEY_1]: '達成率',
    [KEY_2]: {
      value: wrapAchieveRate[0] ? `${wrapAchieveRate[0]}%` : '0%',
      isRed: !isPerformancePositive(wrapAchieveRate[0]),
    },
    [KEY_3]: {
      value: wrapAchieveRate[1] ? `${wrapAchieveRate[1]}%` : '0%',
      isRed: !isPerformancePositive(wrapAchieveRate[1]),
    },
    [KEY_4]: {
      value: wrapAchieveRate[2] ? `${wrapAchieveRate[2]}%` : '0%',
      isRed: !isPerformancePositive(wrapAchieveRate[2]),
    },
  };
  const aspData = {
    [KEY_0]: 'asp-data',
    [KEY_1]: 'ASP',
    [KEY_2]: {
      value: wrapAsp[0] ? numberWithCommas(wrapAsp[0]) : 0,
      isRed: !isPerformancePositive(wrapAsp[0]),
    },
    [KEY_3]: {
      value: wrapAsp[1] ? numberWithCommas(wrapAsp[1]) : 0,
      isRed: !isPerformancePositive(wrapAsp[1]),
    },
    [KEY_4]: {
      value: wrapAsp[2] ? numberWithCommas(wrapAsp[2]) : 0,
      isRed: !isPerformancePositive(wrapAsp[2]),
    },
  };
  const momData = {
    [KEY_0]: 'MOM-data',
    [KEY_1]: 'MOM',
    [KEY_2]: {
      value: wrapMom[0] ? `${wrapMom[0]}%` : '0%',
      isRed: !isPerformancePositive(wrapMom[0]),
    },
    [KEY_3]: {
      value: wrapMom[1] ? `${wrapMom[1]}%` : '0%',
      isRed: !isPerformancePositive(wrapMom[1]),
    },
    [KEY_4]: {
      value: wrapMom[2] ? `${wrapMom[2]}%` : '0%',
      isRed: !isPerformancePositive(wrapMom[2]),
    },
  };
  const workHoursData = {
    [KEY_0]: 'workHours-data',
    [KEY_1]: '平均工時',
    [KEY_2]: {
      value: wrapWorkHours[0] ? `${wrapWorkHours[0]}hr` : '0hr',
      isRed: !isPerformancePositive(wrapWorkHours[0]),
    },
    [KEY_3]: {
      value: wrapWorkHours[1] ? `${wrapWorkHours[1]}hr` : '0hr',
      isRed: !isPerformancePositive(wrapWorkHours[1]),
    },
    [KEY_4]: {
      value: wrapWorkHours[2] ? `${wrapWorkHours[2]}hr` : '0hr',
      isRed: !isPerformancePositive(wrapWorkHours[2]),
    },
  };
  const expectAchievesData = {
    [KEY_0]: 'expectAchieve-data',
    [KEY_1]: '預估達成',
    [KEY_2]: {
      value: wrapExpectAchieves[0] ? numberWithCommas(wrapExpectAchieves[0]) : 0,
      isRed: !isPerformancePositive(wrapExpectAchieves[0]),
    },
    [KEY_3]: {
      value: wrapExpectAchieves[1] ? numberWithCommas(wrapExpectAchieves[1]) : 0,
      isRed: !isPerformancePositive(wrapExpectAchieves[1]),
    },
    [KEY_4]: {
      value: wrapExpectAchieves[2] ? numberWithCommas(wrapExpectAchieves[2]) : 0,
      isRed: !isPerformancePositive(wrapExpectAchieves[2]),
    },
  };
  const expectProfitsData = {
    [KEY_0]: 'expectProfit-data',
    [KEY_1]: '預估收益',
    [KEY_2]: {
      value: wrapExpectProfits[0] ? numberWithCommas(wrapExpectProfits[0]) : 0,
      isRed: !isPerformancePositive(wrapExpectProfits[0]),
    },
    [KEY_3]: {
      value: wrapExpectProfits[1] ? numberWithCommas(wrapExpectProfits[1]) : 0,
      isRed: !isPerformancePositive(wrapExpectProfits[1]),
    },
    [KEY_4]: {
      value: wrapExpectProfits[2] ? numberWithCommas(wrapExpectProfits[2]) : 0,
      isRed: !isPerformancePositive(wrapExpectProfits[2]),
    },
  };

  return [
    targetData,
    amountData,
    salesData,
    achieveRateData,
    aspData,
    momData,
    workHoursData,
    expectAchievesData,
    expectProfitsData,
  ];
}

export default null;
