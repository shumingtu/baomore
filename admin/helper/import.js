function importCSV(file, type) {
  return new Promise(async (resolve, reject) => {
    const fd = new FormData();
    fd.append('file', file);

    try {
      await fetch(`${API_HOST}/import/${type}?token=${localStorage.accessToken}`, {
        method: 'POST',
        body: fd,
      }).then((res) => {
        resolve(res);
      });
    } catch (e) {
      reject(e);
    }
  });
}

export default async function importFile(file, type, cb) {
  cb('importing');

  const result = await importCSV(file, type);

  if (result.ok) {
    window.alert('上傳成功');
    cb('done');
    return;
  }

  window.alert('上傳失敗, 請再試一次');
  cb('done');
}
