import jwtDecode from 'jwt-decode';

import {
  actions,
} from '../../shared/roleActions.js';

export function getMyRoles() {
  const token = localStorage.getItem('accessToken') || null;

  if (token) {
    const {
      permissions,
    } = jwtDecode(token);

    const actionKeys = Object.keys(actions)
      .map(actionKey => ({
        key: actionKey,
        code: actions[actionKey].code,
      }))
      .filter(c => permissions & c.code)
      .map(action => action.key);

    return actionKeys;
  }

  return null;
}

export function isPermissionAllowed(targetRoles = []) {
  if (!targetRoles || !Array.isArray(targetRoles)) {
    console.error('Employee roles should be Array type');
    return false;
  }

  const myRolePermission = getMyRoles();

  if (myRolePermission) {
    return myRolePermission.some(r => ~targetRoles.indexOf(r));
  }

  return false;
}

export default null;
