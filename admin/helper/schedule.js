/* eslint max-len: 0 */
import moment from 'moment';
import { FETCH_SCHEDULE_LIST } from '../queries/Order.js';

function findScheduleAndDelete(originSchedule, newSchedule) {
  if (!newSchedule) return originSchedule;

  const deletedSchedule = originSchedule.map(dateSchedule => ({
    ...dateSchedule,
    assignments: dateSchedule.assignments.map(timeSchedule => ({
      ...timeSchedule,
      onlineOrders: timeSchedule.onlineOrders
        .filter(onlineOrder => onlineOrder.id !== newSchedule.id),
    })),
  }));

  return deletedSchedule;
}

function insertNewSchedule(originSchedule, newSchedule) {
  if (!newSchedule) return originSchedule;

  const newScheduleDate = moment(new Date(newSchedule.date)).format('YYYY-MM-DD');
  const originDateScheduleIdx = originSchedule
    .findIndex(dateSchedule => dateSchedule.date === newScheduleDate);

  if (~originDateScheduleIdx) {
    const originTimeScheduleIdx = originSchedule[originDateScheduleIdx].assignments
      .findIndex(timeSchedule => timeSchedule.time === newSchedule.time);

    if (~originTimeScheduleIdx) {
      return [
        ...originSchedule.slice(0, originDateScheduleIdx),
        {
          ...originSchedule[originDateScheduleIdx],
          assignments: [
            ...originSchedule[originDateScheduleIdx].assignments.slice(0, originTimeScheduleIdx),
            {
              ...originSchedule[originDateScheduleIdx].assignments[originTimeScheduleIdx],
              onlineOrders: [
                newSchedule,
                ...originSchedule[originDateScheduleIdx].assignments[originTimeScheduleIdx].onlineOrders,
              ],
            },
            ...originSchedule[originDateScheduleIdx].assignments.slice(originTimeScheduleIdx + 1),
          ],
        },
        ...originSchedule.slice(originDateScheduleIdx + 1),
      ];
    }
    // time schedule not found
    return [
      ...originSchedule.slice(0, originDateScheduleIdx),
      {
        ...originSchedule[originDateScheduleIdx],
        assignments: [
          {
            time: newSchedule.time,
            denominator: 4, // mock
            onlineOrders: [newSchedule],
            __typename: 'Assignment',
          },
          ...originSchedule[originDateScheduleIdx].assignments,
        ],
      },
      ...originSchedule.slice(originDateScheduleIdx + 1),
    ];
  }

  return originSchedule;
}

export function changeScheduleUpdate({
  cache,
  newSchedule,
  searchOptions,
}) {
  const {
    schedulelist,
  } = cache.readQuery({
    query: FETCH_SCHEDULE_LIST,
    variables: {
      ...searchOptions,
      limit: searchOptions.limit || 10,
      offset: searchOptions.offset || 0,
    },
  });

  const deletedScheduleList = findScheduleAndDelete(schedulelist, newSchedule);
  const newScheduleList = insertNewSchedule(deletedScheduleList, newSchedule);

  cache.writeQuery({
    query: FETCH_SCHEDULE_LIST,
    variables: {
      ...searchOptions,
      limit: searchOptions.limit || 10,
      offset: searchOptions.offset || 0,
    },
    data: {
      schedulelist: newScheduleList,
    },
  });
}

export function cancelScheduleUpdate({
  cache,
  newSchedule,
  searchOptions,
}) {
  const {
    schedulelist,
  } = cache.readQuery({
    query: FETCH_SCHEDULE_LIST,
    variables: {
      ...searchOptions,
      limit: searchOptions.limit || 10,
      offset: searchOptions.offset || 0,
    },
  });

  const deletedScheduleList = findScheduleAndDelete(schedulelist, newSchedule);

  cache.writeQuery({
    query: FETCH_SCHEDULE_LIST,
    variables: {
      ...searchOptions,
      limit: searchOptions.limit || 10,
      offset: searchOptions.offset || 0,
    },
    data: {
      schedulelist: deletedScheduleList,
    },
  });
}
