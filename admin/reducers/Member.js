import {
  MEMBER_INFO_CACHE,
  MEMBER_LOGOUT,
  MEMBER_SET_ROLE,
  MEMBER_WARRANTY_BOX,
  MEMBER_ORDER_BOX,
} from '../actions/Member.js';

export default (state = {
  accessToken: localStorage.getItem('accessToken') || null,
  memberSerialNumber: null,
  memberPhone: null,
  roleActions: [],
  memberId: null,
  memberAreaId: null,
  warranties: null,
  orders: null,
}, action) => {
  switch (action.type) {
    case MEMBER_ORDER_BOX:
      return {
        ...state,
        orders: action.options || null,
      };

    case MEMBER_WARRANTY_BOX:
      return {
        ...state,
        warranties: action.options || null,
      };

    case MEMBER_INFO_CACHE: {
      if (action.member.accessToken) {
        localStorage.setItem('accessToken', action.member.accessToken);
      }

      return {
        ...state,
        accessToken: action.member.accessToken || localStorage.getItem('accessToken') || null,
        memberSerialNumber: action.member.serialNumber || state.memberSerialNumber || null,
        memberPhone: action.member.phone || state.memberPhone || null,
        memberId: action.member.id || state.mebmerId || null,
        memberAreaId: action.member.AreaId || state.memberAreaId || null,
      };
    }

    case MEMBER_LOGOUT: {
      localStorage.removeItem('accessToken');

      return {
        ...state,
        accessToken: null,
        roleActions: [],
        memberId: null,
      };
    }

    case MEMBER_SET_ROLE:
      return {
        ...state,
        roleActions: action.roleActions || [],
      };

    default:
      return state;
  }
};
