// @flow

import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { routerReducer } from 'react-router-redux';
// reducers
import PhoneModel from './PhoneModel.js';
import Member from './Member.js';
import Order from './Order.js';
import PNModel from './PNModel.js';
import Search from './Search.js';
import Invoicing from './Invoicing.js';
import Authorization from './Authorization.js';

export default combineReducers({
  PhoneModel,
  Member,
  Order,
  PNModel,
  Search,
  Invoicing,
  Authorization,
  form: formReducer,
  routing: routerReducer,
});
