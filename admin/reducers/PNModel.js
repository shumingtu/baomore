import {
  CACHE_PNMODEL_LIST_SEARCH_OPTION,
  TOGGLE_PNMODEL_POPUP,
  SET_CURRENT_SELECTED_PNCATEGORY,
  SET_CURRENT_SELECTED_BIGSUBCATEGORY,
  SET_CURRENT_SELECTED_SMALLSUBCATEGORY,
  TOGGLE_PNSUBCATEGORY_CREATEBOX,
} from '../actions/PNModel.js';

export default (state = {
  currentSelectedPNCategory: null,
  currentSelectedBigSubCategory: null,
  currentSelectedSmallSubCategory: null,
  listSearchOptions: {
    limit: 10,
    offset: 0,
  },
  boxData: null,
  subCategoryCreateEditBoxOptions: {
    status: false,
    targetId: null,
    isSmallCategory: false,
  },
}, action) => {
  switch (action.type) {
    case TOGGLE_PNSUBCATEGORY_CREATEBOX:
      return {
        ...state,
        listSearchOptions: {
          code: (action.options && action.options.code) || null,
          pnCategoryId: (action.options && action.options.pnCategoryId) || null,
          bigSubCategoryId: (action.options && action.options.bigSubCategoryId) || null,
          smallSubCategoryId: (action.options && action.options.smallSubCategoryId) || null,
          vendorId: (action.options && action.options.vendorId) || null,
          keyword: (action.options && action.options.keyword) || null,
          limit: 10,
          offset: 0,
        },
        subCategoryCreateEditBoxOptions: {
          status: (action.options && action.options.status) || false,
          targetId: (action.options && action.options.targetId) || null,
          isSmallCategory: (action.options && action.options.isSmallCategory) || false,
        },
      };
    case SET_CURRENT_SELECTED_PNCATEGORY:
      return {
        ...state,
        currentSelectedPNCategory: action.id,
        currentSelectedBigSubCategory: null,
        currentSelectedSmallSubCategory: null,
      };
    case SET_CURRENT_SELECTED_BIGSUBCATEGORY:
      return {
        ...state,
        currentSelectedBigSubCategory: action.id,
        currentSelectedSmallSubCategory: null,
      };
    case SET_CURRENT_SELECTED_SMALLSUBCATEGORY:
      return {
        ...state,
        currentSelectedSmallSubCategory: action.id,
      };
    case CACHE_PNMODEL_LIST_SEARCH_OPTION:
      return {
        ...state,
        listSearchOptions: {
          ...action.options,
          limit: 10,
          offset: 0,
        },
      };
    case TOGGLE_PNMODEL_POPUP:
      return {
        ...state,
        boxData: action.options || null,
      };
    default:
      return state;
  }
};
