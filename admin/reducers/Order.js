import {
  CACHE_ORDER_LIST_SEARCH_OPTION,
  CACHE_ORDER_SCHEDULE_SEARCH_OPTION,
  OPEN_SCHEDULE_BOX,
  TOGGLE_ORDER_EDIT_BOX,
} from '../actions/Order.js';

export default (state = {
  listSearchOptions: {
    startDate: null,
    endDate: null,
    bookedMemberId: null,
    shippingStatus: null,
    storeId: null,
    keyword: null,
    payedStatus: null,
    limit: 10,
    offset: 0,
  },
  scheduleSearchOptions: {
    startDate: null,
    endDate: null,
    storeId: null,
    bookedMemberId: null,
  },
  boxData: null,
  orderEditBoxData: null,
}, action) => {
  switch (action.type) {
    case TOGGLE_ORDER_EDIT_BOX:
      return {
        ...state,
        orderEditBoxData: action.options || null,
      };
    case OPEN_SCHEDULE_BOX:
      return {
        ...state,
        boxData: action.options || null,
      };
    case CACHE_ORDER_LIST_SEARCH_OPTION:
      return {
        ...state,
        listSearchOptions: {
          startDate: (action.options && action.options.startDate) || null,
          endDate: (action.options && action.options.endDate) || null,
          bookedMemberId: (action.options && action.options.bookedMemberId) || null,
          shippingStatus: (action.options && action.options.shippingStatus) || null,
          storeId: (action.options && action.options.storeId) || null,
          keyword: (action.options && action.options.keyword) || null,
          payedStatus: (action.options && action.options.payedStatus) || null,
          limit: 10,
          offset: 0,
        },
      };
    case CACHE_ORDER_SCHEDULE_SEARCH_OPTION:
      return {
        ...state,
        scheduleSearchOptions: {
          startDate: (action.options && action.options.startDate) || null,
          endDate: (action.options && action.options.endDate) || null,
          storeId: (action.options && action.options.storeId) || null,
          bookedMemberId: (action.options && action.options.bookedMemberId) || null,
        },
      };
    default:
      return state;
  }
};
