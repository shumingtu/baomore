import {
  SET_CURRENT_SELECTED_BRAND,
  SET_CURRENT_SELECTED_MODEL,
  CACHE_PHONE_LIST_SEARCH_OPTION,
} from '../actions/PhoneModel.js';

export default (state = {
  currentSelectedBrand: null,
  currentSelectedModel: null,
  listSearchOptions: {
    brandId: '-1',
    modelId: '-1',
    colorId: '-1',
  },
}, action) => {
  switch (action.type) {
    case SET_CURRENT_SELECTED_BRAND:
      return {
        ...state,
        currentSelectedBrand: action.id,
        currentSelectedModel: null,
      };

    case SET_CURRENT_SELECTED_MODEL:
      return {
        ...state,
        currentSelectedModel: action.id,
      };

    case CACHE_PHONE_LIST_SEARCH_OPTION:
      return {
        ...state,
        listSearchOptions: {
          brandId: (action.options && action.options.brandId) || '-1',
          modelId: (action.options && action.options.modelId) || '-1',
          colorId: (action.options && action.options.colorId) || '-1',
        },
      };
    default:
      return state;
  }
};
