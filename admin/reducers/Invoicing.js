import {
  TOGGLE_CREATE_EDIT_BOX,
  CACHE_INVOICING_SEARCHER_OPTIONS,
} from '../actions/Invoicing.js';

export default (state = {
  boxData: null,
  listSearchOptions: {
    startDate: null,
    endDate: null,
    orderSN: null,
    vendorId: null,
    memberId: null,
    areaId: null,
    shippingStatus: null,
    code: null,
    limit: 10,
    offset: 0,
  },
}, action) => {
  switch (action.type) {
    case CACHE_INVOICING_SEARCHER_OPTIONS:
      return {
        ...state,
        listSearchOptions: {
          startDate: (action.options && action.options.startDate) || null,
          endDate: (action.options && action.options.endDate) || null,
          orderSN: (action.options && action.options.orderSN) || null,
          vendorId: (action.options && action.options.vendorId) || null,
          memberId: (action.options && action.options.memberId) || null,
          areaId: (action.options && action.options.areaId) || null,
          shippingStatus: (action.options && action.options.shippingStatus) || null,
          code: (action.options && action.options.code) || null,
          limit: 10,
          offset: 0,
        },
      };
    case TOGGLE_CREATE_EDIT_BOX:
      return {
        ...state,
        boxData: action.options || null,
      };
    default:
      return state;
  }
};
