import {
  TOGGLE_ROLE_EDIT_BOX,
} from '../actions/Authorization.js';

export default (state = {
  boxData: null,
}, action) => {
  switch (action.type) {
    case TOGGLE_ROLE_EDIT_BOX:
      return {
        ...state,
        boxData: action.options || null,
      };

    default:
      return state;
  }
};
