import moment from 'moment';

import {
  CACHE_LINEBOT_INVENTORY_PROTECTOR_SEARCH,
  CACHE_LINEBOT_INVENTORY_MATERIAL_SEARCH,
  CACHE_LINEBOT_PURCHASE_PROTECTOR_SEARCH,
  CACHE_LINEBOT_PURCHASE_MATERIAL_SEARCH,
  CACHE_LINEBOT_TRANSFER_PROTECTOR_SEARCH,
  CACHE_LINEBOT_TRANSFER_MATERIAL_SEARCH,
  CACHE_LINEBOT_CUSTOMER_COMPLAINT_PROTECTOR_SEARCH,
  CACHE_LINEBOT_CUSTOMER_COMPLAINT_MATERIAL_SEARCH,
  CACHE_LINEBOT_RETURN_PROTECTOR_SEARCH,
  CACHE_LINEBOT_RETURN_MATERIAL_SEARCH,
  CACHE_LINEBOT_PUNCH_LIST_SEARCH,
  CACHE_LINEBOT_EMPLOYEE_LIST_SEARCH,
  CACHE_LINEBOT_ANNOUNCEMENT_SEARCH,
  CACHE_LINEBOT_PERFORMANCE_SEARCH,
  CACHE_ADMIN_PERFORMANCE_SEARCH,
  CACHE_ADMIN_DIRECTOR_PERFORMANCE_SEARCH,
  CACHE_ADMIN_STORE_PERFORMANCE_SEARCH,
  CACHE_LINEBOT_STORE_SEARCH,
  CACHE_LINEBOT_ORDER_SEARCH,
  CACHE_ADMIN_ROLLBACK_DOA_SEARCH,
  CACHE_ADMIN_ROLLBACK_PULLOF_SEARCH,
  CACHE_ADMIN_ROLLBACK_SOCIAL_SEARCH,
  CACHE_ADMIN_ROLLBACK_CUSTOMER_SEARCH,
  CACHE_ADMIN_ANNOUNCEMENT_SEARCH,
  CACHE_ADMIN_ORDER_LIST_SEARCH,
  CACHE_ADMIN_STOCK_LIST_SEARCH,
  CACHE_ADMIN_RECORD_LIST_SEARCH,
  CACHE_ADMIN_MEMBER_LIST_SEARCH,
  CACHE_ADMIN_STORE_INFO_LIST_SEARCH,
  CACHE_ADMIN_PUNCH_LIST_SEARCH,
} from '../actions/Search.js';

export default (state = {
  inventoryProtector: null,
  inventoryMaterial: null,
  purchaseProtector: null,
  puchaseMaterial: null,
  transferProtector: null,
  transferMaterial: null,
  complaintProtector: null,
  complaintMaterial: null,
  returnProtector: null,
  returnMaterial: null,
  punchList: null,
  employeeList: null,
  announcements: null,
  performance: null,
  order: null,
  adminPerformance: {
    startDate: moment().format('YYYY-MM-DD'),
    endDate: moment().format('YYYY-MM-DD'),
    employeeId: null,
    directorId: null,
    areaId: null,
  },
  adminDirectorPerformance: {
    startDate: moment().format('YYYY-MM-DD'),
    endDate: moment().format('YYYY-MM-DD'),
    areaId: null,
    employeeId: null,
    directorId: null,
  },
  storePerformance: {
    startDate: moment().format('YYYY-MM-DD'),
    endDate: moment().format('YYYY-MM-DD'),
    channelId: null,
    storeId: null,
  },
  store: null,
  adminRollbackDoa: null,
  adminRollbackPullof: null,
  adminRollbackSocial: null,
  adminRollbackCustomer: null,
  adminAnnouncements: null,
  adminOrderList: null,
  adminStockList: null,
  adminRecordList: null,
  adminMemberList: null,
  adminStoreList: null,
  adminPunchList: null,
}, action) => {
  switch (action.type) {
    case CACHE_ADMIN_PUNCH_LIST_SEARCH:
      return {
        ...state,
        adminPunchList: action.options,
      };

    case CACHE_ADMIN_STORE_INFO_LIST_SEARCH:
      return {
        ...state,
        adminStoreList: action.options,
      };

    case CACHE_ADMIN_MEMBER_LIST_SEARCH:
      return {
        ...state,
        adminMemberList: action.options,
      };

    case CACHE_ADMIN_RECORD_LIST_SEARCH:
      return {
        ...state,
        adminRecordList: action.options,
      };

    case CACHE_ADMIN_STOCK_LIST_SEARCH:
      return {
        ...state,
        adminStockList: action.options,
      };

    case CACHE_ADMIN_ORDER_LIST_SEARCH:
      return {
        ...state,
        adminOrderList: action.options,
      };

    case CACHE_ADMIN_STORE_PERFORMANCE_SEARCH:
      return {
        ...state,
        storePerformance: action.options,
      };

    case CACHE_ADMIN_PERFORMANCE_SEARCH:
      return {
        ...state,
        adminPerformance: action.options,
      };

    case CACHE_ADMIN_DIRECTOR_PERFORMANCE_SEARCH:
      return {
        ...state,
        adminDirectorPerformance: action.options,
      };

    case CACHE_LINEBOT_INVENTORY_PROTECTOR_SEARCH:
      return {
        ...state,
        inventoryProtector: action.options,
      };

    case CACHE_LINEBOT_INVENTORY_MATERIAL_SEARCH:
      return {
        ...state,
        inventoryMaterial: action.options,
      };

    case CACHE_LINEBOT_PURCHASE_PROTECTOR_SEARCH:
      return {
        ...state,
        purchaseProtector: action.options,
      };

    case CACHE_LINEBOT_PURCHASE_MATERIAL_SEARCH:
      return {
        ...state,
        purchaseMaterial: action.options,
      };

    case CACHE_LINEBOT_TRANSFER_PROTECTOR_SEARCH:
      return {
        ...state,
        transferProtector: action.options,
      };

    case CACHE_LINEBOT_TRANSFER_MATERIAL_SEARCH:
      return {
        ...state,
        transferMaterial: action.options,
      };

    case CACHE_LINEBOT_CUSTOMER_COMPLAINT_PROTECTOR_SEARCH:
      return {
        ...state,
        complaintProtector: action.options,
      };

    case CACHE_LINEBOT_CUSTOMER_COMPLAINT_MATERIAL_SEARCH:
      return {
        ...state,
        complaintMaterial: action.options,
      };

    case CACHE_LINEBOT_RETURN_PROTECTOR_SEARCH:
      return {
        ...state,
        returnProtector: action.options,
      };

    case CACHE_LINEBOT_RETURN_MATERIAL_SEARCH:
      return {
        ...state,
        returnMaterial: action.options,
      };

    case CACHE_LINEBOT_PUNCH_LIST_SEARCH:
      return {
        ...state,
        punchList: action.options,
      };

    case CACHE_LINEBOT_EMPLOYEE_LIST_SEARCH:
      return {
        ...state,
        employeeList: action.options,
      };

    case CACHE_LINEBOT_ANNOUNCEMENT_SEARCH:
      return {
        ...state,
        announcements: action.options,
      };

    case CACHE_LINEBOT_PERFORMANCE_SEARCH:
      return {
        ...state,
        performance: action.options,
      };

    case CACHE_LINEBOT_STORE_SEARCH:
      return {
        ...state,
        store: action.options,
      };

    case CACHE_ADMIN_ROLLBACK_DOA_SEARCH:
      return {
        ...state,
        adminRollbackDoa: action.options,
      };

    case CACHE_ADMIN_ROLLBACK_PULLOF_SEARCH:
      return {
        ...state,
        adminRollbackPullof: action.options,
      };

    case CACHE_ADMIN_ROLLBACK_SOCIAL_SEARCH:
      return {
        ...state,
        adminRollbackSocial: action.options,
      };

    case CACHE_ADMIN_ROLLBACK_CUSTOMER_SEARCH:
      return {
        ...state,
        adminRollbackCustomer: action.options,
      };

    case CACHE_ADMIN_ANNOUNCEMENT_SEARCH:
      return {
        ...state,
        adminAnnouncements: action.options,
      };

    case CACHE_LINEBOT_ORDER_SEARCH:
      return {
        ...state,
        order: action.options,
      };

    default:
      return state;
  }
};
