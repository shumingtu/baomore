// @flow

import React, { Component } from 'react';
import {
  Switch,
  Route,
  Redirect,
} from 'react-router';

import MemberMeBoard from './MemberMeBoard.jsx';
import SiteHeader from './SiteHeader.jsx';
import ManageBoard from './containers/ManageBoard.jsx';
// routes
import QrcodeRoutes from './routes/Qrcode.jsx';
import CheckInventoryRoutes from './routes/CheckInventory.jsx';
import SalesRoutes from './routes/Sales.jsx';
import InventoryRoutes from './routes/Inventory.jsx';
import PurchaseRoutes from './routes/Purchase.jsx';
import TransferRoutes from './routes/Transfer.jsx';
import ReturnRoutes from './routes/Return.jsx';
import CustomerComplaintRoutes from './routes/CustomerComplaint.jsx';
import PunchRoutes from './routes/Punch.jsx';
import EmployeeRoutes from './routes/Employee.jsx';
import AnnouncementRoutes from './routes/Announcement.jsx';
import ScheduleRoutes from './routes/Schedule.jsx';
import PerformanceRoutes from './routes/Performance.jsx';
import StoreRoutes from './routes/Store.jsx';
import AssignForm from '../shared/containers/AssignForm.jsx';

const styles = {
  placement: {
    width: '100%',
    height: '100%',
    minHeight: '100vh',
    position: 'relative',
  },
  main: {
    width: '100%',
    height: '100%',
    minHeight: '100vh',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  mainWrapper: {
    width: '100%',
    height: 'auto',
    flex: 1,
    minWidth: '100vw',
    minHeight: 'calc(100vh - 50px)',
    overflow: 'auto',
  },
};

type Props = {
  history: {
    replace: Function,
  },
  match: {
    url: string,
  },
};

class LinebotMainBoard extends Component<Props> {
  componentDidMount() {
    const width = window.innerWidth
    || document.documentElement.clientWidth
    || document.body.clientWidth;

    if (width < 768) {
      document.title = 'Artmo 行動管理系統';
    }
  }

  render() {
    const {
      match: {
        url,
      },
    } = this.props;

    return (
      <div style={styles.placement}>
        <MemberMeBoard />
        <main style={styles.main}>
          <SiteHeader />
          <div style={styles.mainWrapper}>
            <Switch>
              <Route path={`${url}/management`} component={ManageBoard} />
              <Route path={`${url}/checkInventory`} component={CheckInventoryRoutes} />
              <Route path={`${url}/qrcode`} component={QrcodeRoutes} />
              <Route path={`${url}/sales`} component={SalesRoutes} />
              <Route path={`${url}/inventory`} component={InventoryRoutes} />
              <Route path={`${url}/purchase`} component={PurchaseRoutes} />
              <Route path={`${url}/transfer`} component={TransferRoutes} />
              <Route path={`${url}/return`} component={ReturnRoutes} />
              <Route path={`${url}/customerComplaint`} component={CustomerComplaintRoutes} />
              <Route path={`${url}/punch`} component={PunchRoutes} />
              <Route path={`${url}/employee`} component={EmployeeRoutes} />
              <Route path={`${url}/announcement`} component={AnnouncementRoutes} />
              <Route path={`${url}/schedule`} component={ScheduleRoutes} />
              <Route path={`${url}/performance`} component={PerformanceRoutes} />
              <Route path={`${url}/store`} component={StoreRoutes} />
              <Route path={`${url}/assignOrder/:onlineOrderId/:directorId`} component={AssignForm} />
              <Redirect to={{ pathname: `${url}/login` }} />
            </Switch>
          </div>
        </main>
      </div>
    );
  }
}

export default LinebotMainBoard;
