import React from 'react';

import { LINEBOT_LINE_LOGIN_PATH } from '../shared/Global.js';

import LoginPage from '../containers/LoginPage.jsx';

function LinebotLoginPage() {
  return (
    <LoginPage link={LINEBOT_LINE_LOGIN_PATH} />
  );
}

export default LinebotLoginPage;
