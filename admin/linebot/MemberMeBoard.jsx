// @flow

import React from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { graphql } from 'react-apollo';
// actions
import * as MemberActions from '../actions/Member.js';
import { GET_MEMBER_ME } from '../queries/Member.js';

type Props = {
  cacheMemberInfo: Function,
  cacheMemberToken: Function,
  memberLogout: Function,
  accessToken: string,
  me: {
    id: number,
    name: string,
    phone: string,
  },
  meError: string,
  memberLogout: Function,
  memberSetRole: Function,
};

class MemberMeBoard extends React.PureComponent<Props> {
  componentDidMount() {
    const {
      memberLogout,
      meError,
      memberSetRole,
    } = this.props;

    memberSetRole();

    if (meError) {
      memberLogout();
    }
  }

  componentDidUpdate(prevProps) {
    const {
      me,
      meError,
      cacheMemberInfo,
      memberLogout,
      memberSetRole,
    } = this.props;

    if (meError && prevProps.meError !== meError) {
      memberLogout();
    }

    if (me && me !== prevProps.me) {
      cacheMemberInfo(me);
      memberSetRole();
    }
  }

  render() {
    return null;
  }
}

const reduxHook = connect(
  () => ({}),
  dispatch => bindActionCreators({
    ...MemberActions,
  }, dispatch)
);

const queryHook = graphql(GET_MEMBER_ME, {
  props: ({
    data: {
      me,
      error,
    },
  }) => ({
    me: me || null,
    meError: (error && error.message) || null,
  }),
});

export default reduxHook(
  queryHook(
    radium(
      MemberMeBoard
    )
  )
);
