// @flow
import React, { memo } from 'react';
import { Link } from 'react-router-dom';

import Theme from '../../../styles/Theme.js';

const styles = {
  wrapper: {
    width: 140,
    height: 140,
    margin: 4,
    borderRadius: 3,
    border: `1px solid ${Theme.BLACK_COLOR}`,
    backgroundColor: Theme.ACTIVE_COLOR,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 15,
    color: '#fff',
    letterSpacing: 1,
    textDecoration: 'none',
    textAlign: 'center',
    outline: 0,
    cursor: 'pointer',
  },
};

function PageLink({
  name,
  path,
  style,
}: {
  name: string,
  path: string,
  style?: Object,
}) {
  return (
    <Link
      to={{ pathname: path || '/linebot' }}
      style={{
        ...styles.wrapper,
        ...style,
      }}>
      {name || null}
    </Link>
  );
}

PageLink.defaultProps = {
  style: {},
};

export default memo(
  PageLink
);
