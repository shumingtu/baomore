// @flow
import React, { memo } from 'react';
import radium from 'radium';

import Theme from '../../../styles/Theme.js';

const styles = {
  wrapper: {
    width: '100%',
    height: 40,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Theme.SECONDARY_THEME_COLOR,
  },
  title: {
    fontSize: 13,
    letterSpacing: 1,
    color: Theme.BLACK_COLOR,
  },
};

type Props = {
  title: string,
};

function PageTitle({
  title,
}: Props) {
  return (
    <div style={styles.wrapper}>
      <span style={styles.title}>
        {title || null}
      </span>
    </div>
  );
}

export default memo(
  radium(
    PageTitle
  )
);
