// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import moment from 'moment';
import Calendar from 'react-calendar';
import { Query } from 'react-apollo';

import {
  FETCH_MY_SCHEDULES,
} from '../../../queries/Schedule.js';

const styles = {
  calendarWrapper: {
    width: '100%',
    height: 'auto',
    padding: '4px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  customSchedule: {
    width: '100%',
    height: 'auto',
    position: 'relative',
  },
  hasScheduleDot: {
    position: 'absolute',
    top: -20,
    right: 0,
    width: 4,
    height: 4,
    backgroundColor: 'rgba(214, 95, 78, 0.7)',
  },
};

type Props = {
  onChange: Function,
  value: string,
  hide?: boolean,
};

class ScheduleCalendar extends PureComponent<Props> {
  static defaultProps = {
    hide: false,
  };

  render() {
    const {
      onChange,
      value,
      hide,
    } = this.props;

    const wrapValue = value ? new Date(value) : null;

    return (
      <Query
        query={FETCH_MY_SCHEDULES}
        fetchPolicy="cache-and-network">
        {({
          data,
        }) => {
          const meScheduleDates = (data && data.meScheduleDates) || [];

          return (
            <div
              style={[
                styles.calendarWrapper,
                hide && { display: 'none' },
              ]}>
              <Calendar
                tileContent={({ date, view }) => {
                  if (view === 'month') {
                    const wrapDate = date ? moment(date).format('YYYY-MM-DD') : null;

                    if (wrapDate) {
                      const existSchedule = meScheduleDates.find(d => d === wrapDate);

                      if (existSchedule) {
                        return (
                          <div style={styles.customSchedule}>
                            <div style={styles.hasScheduleDot} />
                          </div>
                        );
                      }
                    }
                  }

                  return null;
                }}
                onChange={d => onChange(moment(d).format('YYYY-MM-DD'))}
                value={wrapValue} />
            </div>
          );
        }}
      </Query>
    );
  }
}

export default radium(
  ScheduleCalendar
);
