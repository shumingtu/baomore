// @flow
import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import {
  Field,
  formValueSelector,
} from 'redux-form';
import { withRouter } from 'react-router-dom';

import { FORM_LINEBOT_RETURN_DOA_FORM } from '../../../shared/form.js';

// config
import { pnCategoryTypeIsProtector } from '../../../helper/linebot.js';
import {
  customerReceivedProtectorErrorTypes,
  customerReceivedMaterialErrorTypes,
  employeeProtectorErrorTypes,
  employeeMaterialErrorTypes,
  inventoryPullofErrorTypes,
} from '../../../shared/linebot.js';
// components
import Selector from '../../../components/Form/Selector.jsx';
import Input from '../../../components/Form/Input.jsx';

const selector = formValueSelector(FORM_LINEBOT_RETURN_DOA_FORM);

const styles = {
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
};

type Props = {
  errorType?: {
    input: Object,
    meta: Object,
  },
  errorDesc?: {
    input: Object,
    meta: Object,
  },
  match: {
    params: {
      pnCategoryType: string,
    },
  },
  customPNCategoryType?: string,
  type?: 'customer' | 'employee' | 'pullof',
  names: Array<string>,
  productType?: string,
  label?: string,
};

class ErrorTypeSelectorBind extends PureComponent<Props> {
  static defaultProps = {
    errorType: null,
    errorDesc: null,
    type: 'customer',
    customPNCategoryType: null,
    productType: null,
    label: '異常類型',
  };

  errorTypesDecider() {
    const {
      match: {
        params: {
          pnCategoryType,
        },
      },
      customPNCategoryType,
      type,
      productType,
    } = this.props;

    let isProtector = false;

    if (productType) {
      isProtector = productType === 'PROTECTOR';
    } else {
      isProtector = pnCategoryTypeIsProtector(customPNCategoryType || pnCategoryType);
    }

    if (type && type === 'pullof') {
      return inventoryPullofErrorTypes;
    }

    if (isProtector) {
      return type === 'customer'
        ? customerReceivedProtectorErrorTypes
        : employeeProtectorErrorTypes;
    }

    return type === 'customer'
      ? customerReceivedMaterialErrorTypes
      : employeeMaterialErrorTypes;
  }

  render() {
    const {
      errorType,
      errorDesc,
      label,
    } = this.props;

    if (!errorType && !errorDesc) return null;

    const showErrorDesc = (errorType && errorType.input && errorType.input.value === '其他') || false;

    return (
      <Fragment>
        {errorType ? (
          <div style={styles.fieldWrapper}>
            <Field
              nullable
              name="errorType"
              label={`${label}：`}
              options={this.errorTypesDecider()}
              placeholder={`請選擇${label}`}
              customOnChange={() => {
                if (errorDesc && errorDesc.input) {
                  errorDesc.input.onChange(null);
                }
              }}
              {...errorType}
              component={Selector} />
          </div>
        ) : null}
        {errorDesc && showErrorDesc ? (
          <div style={styles.fieldWrapper}>
            <Field
              required
              nullable
              name="errorDesc"
              label="異常說明："
              placeholder="請填寫異常原因"
              {...errorDesc}
              component={Input} />
          </div>
        ) : null}
      </Fragment>
    );
  }
}

const reduxHook = connect(
  state => ({
    productType: selector(state, 'productType'),
  }),
);


export default withRouter(
  reduxHook(
    radium(
      ErrorTypeSelectorBind
    )
  )
);
