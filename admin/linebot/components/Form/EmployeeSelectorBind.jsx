// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';
import {
  Field,
} from 'redux-form';

// config
import { GET_EMPLOYEE_LIST } from '../../../queries/Member.js';
import { isPermissionAllowed } from '../../../helper/roles.js';
// components
import Selector from '../../../components/Form/Selector.jsx';

const styles = {
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
};

type Props = {
  storeId?: number,
  memberAreaId: number,
  roles?: Array<string>,
};

class EmployeeSelectorBind extends PureComponent<Props> {
  static defaultProps = {
    storeId: null,
    roles: null,
  };

  render() {
    const {
      storeId,
      roles,
      memberAreaId,
    } = this.props;

    if (roles && !isPermissionAllowed(roles)) return null;

    return (
      <Query
        query={GET_EMPLOYEE_LIST}
        variables={{
          storeId: storeId ? parseInt(storeId, 10) : null,
          areaId: memberAreaId ? parseInt(memberAreaId, 10) : null,
        }}>
        {({
          data,
        }) => {
          const employeeMemberlist = (data && data.employeeMemberlist) || [];

          return (
            <div style={styles.fieldWrapper}>
              <Field
                nullable
                name="employeeId"
                label="包膜師："
                options={employeeMemberlist}
                placeholder="請選擇包膜師"
                component={Selector} />
            </div>
          );
        }}
      </Query>
    );
  }
}

const reduxHook = connect(
  state => ({
    memberAreaId: state.Member.memberAreaId,
  }),
);

export default reduxHook(radium(EmployeeSelectorBind));
