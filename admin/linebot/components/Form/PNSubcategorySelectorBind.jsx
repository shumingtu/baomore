// @flow
import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';
import {
  Field,
} from 'redux-form';

// config
import {
  FETCH_PNCATEGORIES,
  FETCH_BIGSUBCATEGORIES,
  FETCH_SMALLSUBCATEGORIES,
} from '../../../queries/PNTable.js';
// components
import Selector from '../../../components/Form/Selector.jsx';

const styles = {
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
};

type Props = {
  pnCategoryId?: {
    input: Object,
    meta: Object,
  },
  bigSubCategoryId?: {
    input: Object,
    meta: Object,
  },
  smallSubCategoryId?: {
    input: Object,
    meta: Object,
  },
  names: Array<string>,
};

class PNSubcategorySelectorBind extends PureComponent<Props> {
  static defaultProps = {
    pnCategoryId: null,
    bigSubCategoryId: null,
    smallSubCategoryId: null,
  };

  render() {
    const {
      pnCategoryId,
      bigSubCategoryId,
      smallSubCategoryId,
    } = this.props;

    if (!pnCategoryId && !bigSubCategoryId && !smallSubCategoryId) return null;

    return (
      <Fragment>
        {pnCategoryId ? (
          <Query query={FETCH_PNCATEGORIES}>
            {({
              data,
            }) => {
              const pnCategories = (data && data.pnCategories) || [];
              const filteredPNCategories = pnCategories.filter(c => c.name !== '保貼');

              return (
                <div style={styles.fieldWrapper}>
                  <Field
                    name="pnCategoryId"
                    label="料號類型："
                    options={filteredPNCategories}
                    placeholder="請選擇料號"
                    customOnChange={() => {
                      if (bigSubCategoryId && bigSubCategoryId.input) {
                        bigSubCategoryId.input.onChange('-1');
                      }

                      if (smallSubCategoryId && smallSubCategoryId.input) {
                        smallSubCategoryId.input.onChange('-1');
                      }
                    }}
                    {...pnCategoryId}
                    component={Selector} />
                </div>
              );
            }}
          </Query>
        ) : null}
        {bigSubCategoryId ? (
          <Query
            query={FETCH_BIGSUBCATEGORIES}
            variables={pnCategoryId && pnCategoryId.input ? {
              id: parseInt(pnCategoryId.input.value, 10) || -1,
            } : {}}>
            {({
              data,
            }) => {
              const subCategoriesByPNCategory = (data && data.subCategoriesByPNCategory) || [];
              return (
                <div style={styles.fieldWrapper}>
                  <Field
                    nullable
                    name="bigSubCategoryId"
                    label="大類："
                    options={subCategoriesByPNCategory}
                    placeholder="請選擇大類"
                    customOnChange={() => {
                      if (smallSubCategoryId && smallSubCategoryId.input) {
                        smallSubCategoryId.input.onChange('-1');
                      }
                    }}
                    {...bigSubCategoryId}
                    component={Selector} />
                </div>
              );
            }}
          </Query>
        ) : null}
        {smallSubCategoryId ? (
          <Query
            query={FETCH_SMALLSUBCATEGORIES}
            variables={bigSubCategoryId && bigSubCategoryId.input ? {
              id: parseInt(bigSubCategoryId.input.value, 10) || -1,
            } : {}}>
            {({
              data,
            }) => {
              const subCategoriesBySubCategory = (data && data.subCategoriesBySubCategory) || [];

              return (
                <div style={styles.fieldWrapper}>
                  <Field
                    nullable
                    name="smallSubCategoryId"
                    label="類型："
                    options={subCategoriesBySubCategory}
                    placeholder="請選擇類型"
                    {...smallSubCategoryId}
                    component={Selector} />
                </div>
              );
            }}
          </Query>
        ) : null}
      </Fragment>
    );
  }
}

export default radium(PNSubcategorySelectorBind);
