// @flow
import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
import {
  Field,
} from 'redux-form';

// components
import DatetimePicker from '../../../components/Form/DatetimePicker.jsx';

const styles = {
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  selector: {
    minWidth: 'auto',
    maxWidth: 130,
  },
};

type Props = {
  startDate?: {
    input: Object,
    meta: Object,
  },
  endDate?: {
    input: Object,
    meta: Object,
  },
  names: Array<string>,
  disableDate?: boolean,
  disableTime?: boolean,
  startDateLabel?: string,
  endDateLabel?: string,
};

class DatetimeRangeSelectorBind extends PureComponent<Props> {
  static defaultProps = {
    startDate: null,
    endDate: null,
    disableDate: false,
    disableTime: false,
    startDateLabel: '開始時間：',
    endDateLabel: '結束時間：',
  };

  render() {
    const {
      startDate,
      endDate,
      disableDate,
      disableTime,
      startDateLabel,
      endDateLabel,
    } = this.props;

    if (!startDate && !endDate) return null;

    return (
      <Fragment>
        {startDate ? (
          <div style={styles.fieldWrapper}>
            <Field
              disableDate={disableDate}
              disableTime={disableTime}
              name="startDate"
              label={startDateLabel}
              component={DatetimePicker} />
          </div>
        ) : null}
        {endDate ? (
          <div style={styles.fieldWrapper}>
            <Field
              disableDate={disableDate}
              disableTime={disableTime}
              name="endDate"
              label={endDateLabel}
              component={DatetimePicker} />
          </div>
        ) : null}
      </Fragment>
    );
  }
}

export default radium(DatetimeRangeSelectorBind);
