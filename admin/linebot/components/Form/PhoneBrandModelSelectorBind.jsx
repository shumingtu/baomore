// @flow
import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';
import {
  Field,
} from 'redux-form';

// config
import {
  FETCH_BRAND_LIST,
  FETCH_BRAND_MODEL_LIST,
} from '../../../queries/Brand.js';
// components
import Selector from '../../../components/Form/Selector.jsx';

const styles = {
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
};

type Props = {
  brandId?: {
    input: Object,
    meta: Object,
  },
  modelId?: {
    input: Object,
    meta: Object,
  },
  names: Array<string>,
};

class PhoneBrandModelSelectorBind extends PureComponent<Props> {
  static defaultProps = {
    brandId: null,
    modelId: null,
  };

  render() {
    const {
      brandId,
      modelId,
    } = this.props;

    if (!brandId && !modelId) return null;

    return (
      <Fragment>
        {brandId ? (
          <Query query={FETCH_BRAND_LIST}>
            {({
              data,
            }) => {
              const brandList = (data && data.brandList) || [];
              return (
                <div style={styles.fieldWrapper}>
                  <Field
                    nullable
                    name="brandId"
                    label="手機品牌："
                    options={brandList}
                    placeholder="請選擇品牌"
                    customOnChange={() => {
                      if (modelId && modelId.input) {
                        modelId.input.onChange('-1');
                      }
                    }}
                    {...brandId}
                    component={Selector} />
                </div>
              );
            }}
          </Query>
        ) : null}
        {modelId ? (
          <Query
            query={FETCH_BRAND_MODEL_LIST}
            variables={brandId && brandId.input ? {
              brandId: parseInt(brandId.input.value, 10) || -1,
            } : {}}>
            {({
              data,
            }) => {
              const brandModelList = (data && data.brandModelList) || [];
              return (
                <div style={styles.fieldWrapper}>
                  <Field
                    nullable
                    name="modelId"
                    label="手機型號："
                    options={brandModelList}
                    placeholder="請選擇型號"
                    {...modelId}
                    component={Selector} />
                </div>
              );
            }}
          </Query>
        ) : null}
      </Fragment>
    );
  }
}

export default radium(PhoneBrandModelSelectorBind);
