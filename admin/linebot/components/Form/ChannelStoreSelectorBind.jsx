// @flow
import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';
import {
  Field,
} from 'redux-form';

// config
import {
  FETCH_CHANNEL_LIST,
  FETCH_CITY_DISTRICTS,
  FETCH_STORES,
} from '../../../queries/Global.js';
// components
import Selector from '../../../components/Form/Selector.jsx';

const styles = {
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  selector: {
    minWidth: 'auto',
    maxWidth: 130,
  },
};

type Props = {
  channelId?: {
    input: Object,
    meta: Object,
  },
  cityId?: {
    input: Object,
    meta: Object,
  },
  storeId?: {
    input: Object,
    meta: Object,
  },
  names: Array<string>,
  disableRequirement?: boolean,
  isForPurchase?: boolean,
  lat?: number,
  lon?: number,
};

class ChannelStoreSelectorBind extends PureComponent<Props> {
  static defaultProps = {
    channelId: null,
    cityId: null,
    storeId: null,
    disableRequirement: false,
    isForPurchase: false,
    lat: null,
    lon: null,
  };

  render() {
    const {
      channelId,
      cityId,
      storeId,
      disableRequirement,
      isForPurchase,
      lat,
      lon,
    } = this.props;

    if (!channelId && !cityId && !storeId) return null;

    const storeVariables = {};

    if (cityId && cityId.input) {
      if (cityId.input.value !== '-1') {
        storeVariables.cityId = parseInt(cityId.input.value, 10);
      }
    }

    if (channelId && channelId.input) {
      if (channelId.input.value !== '-1') {
        storeVariables.channelId = parseInt(channelId.input.value, 10);
      }
    }

    if (lat && lon) {
      storeVariables.lat = lat;
      storeVariables.lon = lon;
    }

    return (
      <Fragment>
        {cityId ? (
          <Query query={FETCH_CITY_DISTRICTS}>
            {({
              data,
            }) => {
              const cityDistricts = (data && data.cityDistricts) || [];
              return (
                <div style={styles.fieldWrapper}>
                  <Field
                    required={!disableRequirement}
                    nullable
                    name="cityId"
                    label="縣市："
                    options={cityDistricts}
                    placeholder="請選擇縣市"
                    customOnChange={() => {
                      if (storeId && storeId.input) {
                        storeId.input.onChange('-1');
                      }
                    }}
                    component={Selector}
                    style={styles.selector} />
                </div>
              );
            }}
          </Query>
        ) : null}
        {channelId ? (
          <Query
            variables={isForPurchase ? {
              isPurchaseChannel: true,
            } : {}}
            query={FETCH_CHANNEL_LIST}>
            {({
              data,
            }) => {
              const channelList = (data && data.channelList) || [];
              return (
                <div style={styles.fieldWrapper}>
                  <Field
                    required={!disableRequirement}
                    nullable
                    name="channelId"
                    label="通路："
                    options={channelList}
                    placeholder="請選擇通路"
                    customOnChange={() => {
                      if (storeId && storeId.input) {
                        storeId.input.onChange('-1');
                      }
                    }}
                    component={Selector}
                    style={styles.selector} />
                </div>
              );
            }}
          </Query>
        ) : null}
        {storeId ? (
          <Query
            query={FETCH_STORES}
            variables={storeVariables}>
            {({
              data,
            }) => {
              const stores = (data && data.stores) || [];
              return (
                <div style={styles.fieldWrapper}>
                  <Field
                    required={!disableRequirement}
                    nullable
                    name="storeId"
                    label="門市："
                    options={stores}
                    placeholder="請選擇門市"
                    component={Selector}
                    style={styles.selector} />
                </div>
              );
            }}
          </Query>
        ) : null}
      </Fragment>
    );
  }
}

export default radium(ChannelStoreSelectorBind);
