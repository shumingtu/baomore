// @flow
/* eslint react/no-did-update-set-state: 0 */
import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  SubmissionError,
  initialize,
} from 'redux-form';
import { Mutation } from 'react-apollo';
import qs from 'qs';
// config
import {
  FORM_LINEBOT_REGISTER_FORM,
} from '../shared/form.js';
import {
  EMPLOYEE_LOGIN,
} from '../mutations/Global.js';
import Theme from '../styles/Theme.js';
import * as MemberActions from '../actions/Member.js';
// components
import Input from '../components/Form/Input.jsx';
import Error from '../components/Form/Error.jsx';
import SubmitButton from '../components/Form/SubmitButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    padding: 12,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Theme.MAIN_THEME_COLOR,
  },
  title: {
    fontSize: 24,
    fontWeight: 500,
    letterSpacing: 1,
    margin: '36px 0',
    padding: 0,
    color: Theme.SECONDARY_THEME_COLOR,
  },
  formWrapper: {
    width: '100%',
    maxWidth: 414,
    height: 'auto',
    padding: 12,
    borderRadius: 3,
    border: `1px solid ${Theme.SECONDARY_THEME_COLOR}`,
    backgroundColor: '#fff',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  annotation: {
    fontSize: 16,
    fontWeight: 500,
    letterSpacing: 1,
    padding: 0,
    textAlign: 'center',
    lineHeight: 1.6,
    color: Theme.SECONDARY_THEME_COLOR,
    whiteSpace: 'pre-line',
  },
};

type Props = {
  handleSubmit: Function,
  history: {
    replace: Function,
  },
  location: {
    search: string,
  },
  error: string,
  initializeForm: Function,
  cacheMemberInfo: Function,
};

type State = {
  isRegistered: boolean,
};

class LinebotRegisterPage extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      isRegistered: false,
    };
  }

  componentDidMount() {
    const {
      location: {
        search,
      },
      history,
      initializeForm,
    } = this.props;

    const qsString = qs.parse(search, { ignoreQueryPrefix: true }) || {};

    if (qsString.code) {
      initializeForm({
        code: qsString.code,
        serialNumber: '',
        phone: '',
      });
    } else {
      history.replace('/linebot/login');
    }
  }

  getFields(values) {
    const {
      code,
      serialNumber,
      phone,
    } = values;

    if (!code) {
      throw new SubmissionError({
        _error: '系統發生錯誤！',
      });
    }

    if (!serialNumber) {
      throw new SubmissionError({
        serialNumber: 'error',
      });
    }

    if (!phone) {
      throw new SubmissionError({
        phone: 'error',
      });
    }

    return values;
  }

  render() {
    const {
      handleSubmit,
      error,
      cacheMemberInfo,
    } = this.props;

    const {
      isRegistered,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        {!isRegistered ? (
          <Fragment>
            <h1 style={styles.title}>
              包膜師登入
            </h1>
            <Mutation mutation={EMPLOYEE_LOGIN}>
              {(employeeLogin, options) => (
                <form
                  style={styles.formWrapper}
                  onSubmit={handleSubmit(async (d) => {
                    const {
                      code,
                      serialNumber,
                      phone,
                    } = this.getFields(d);

                    const {
                      data,
                    } = await employeeLogin({
                      variables: {
                        code,
                        SN: serialNumber,
                        phone,
                      },
                    });

                    if (data && data.employeeLogin) {
                      const token = data.employeeLogin.accessToken || null;
                      const member = data.employeeLogin.member || null;
                      if (token) {
                        cacheMemberInfo({
                          accessToken: token,
                          AreaId: member && member.AreaId,
                        });
                      }
                      this.setState({
                        isRegistered: true,
                      });
                    }
                  })}>
                  <div style={styles.fieldWrapper}>
                    <Field
                      type="text"
                      name="serialNumber"
                      label="包膜師工號："
                      placeholder="請輸入您的工號"
                      component={Input} />
                  </div>
                  <div style={styles.fieldWrapper}>
                    <Field
                      type="tel"
                      name="phone"
                      label="手機號碼："
                      placeholder="請輸入您的手機號碼"
                      component={Input} />
                  </div>
                  {error || options.error ? (
                    <Error
                      error={(options.error && options.error.message) || error} />
                  ) : null}
                  <div style={styles.functionWrapper}>
                    <SubmitButton
                      disabled={options.loading}
                      label="登入/註冊" />
                  </div>
                </form>
              )}
            </Mutation>
          </Fragment>
        ) : (
          <span style={styles.annotation}>
            {`您已完成登入/註冊！
              請關閉瀏覽器並再次點選 Line Bot 選單重新進入後台`}
          </span>
        )}
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_LINEBOT_REGISTER_FORM,
});

const reduxHook = connect(
  () => ({}),
  dispatch => bindActionCreators({
    ...MemberActions,
    initializeForm: v => initialize(FORM_LINEBOT_REGISTER_FORM, v),
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      LinebotRegisterPage
    )
  )
);
