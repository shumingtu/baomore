// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import qs from 'qs';

import { returnCategoryTypes } from '../../../shared/linebot.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import PageLink from '../../components/Global/PageLink.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'start',
  },
  mainPlacement: {
    width: '100%',
    height: 'auto',
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainWrapper: {
    width: '100%',
    height: 'auto',
    maxWidth: 320,
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

type Props = {
  location: {
    search: string,
  },
};

class QrcodeMainBoard extends PureComponent<Props> {
  render() {
    const {
      location: {
        search,
      },
    } = this.props;

    const qsString = qs.parse(search, { ignoreQueryPrefix: true }) || {};

    return (
      <div style={styles.wrapper}>
        <PageTitle title="商品動作頁" />
        <div style={styles.mainPlacement}>
          <div style={styles.mainWrapper}>
            <PageLink
              name="銷貨"
              path={`/linebot/sales/${qsString.code}`} />
            <PageLink
              name="點貨"
              path={`/linebot/checkInventory/${qsString.code}`} />
            <PageLink
              name="調貨"
              path={`/linebot/transfer/action/${qsString.code}`} />
            {returnCategoryTypes.map(rc => (
              <PageLink
                key={rc.id}
                name={rc.name}
                path={`/linebot/return/type/${rc.id}/${qsString.code}`} />
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default radium(QrcodeMainBoard);
