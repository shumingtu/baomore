// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Fields,
  initialize,
} from 'redux-form';

import * as SearchActions from '../../../actions/Search.js';
import { LINEBOT_PERFORMANCE_EMPLOYEE_SELECTOR_KEYS } from '../../../../shared/selectorActionKeys.js';
// components
import DatetimeRangeSelectorBind from '../../components/Form/DatetimeRangeSelectorBind.jsx';
import EmployeeSelectorBind from '../../components/Form/EmployeeSelectorBind.jsx';
import SubmitButton from '../../../components/Form/SubmitButton.jsx';

const styles = {
  searchWrapper: {
    width: '100%',
    height: 'auto',
    padding: 16,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '6px 0',
  },
};

type Props = {
  onSearch: Function,
  handleSubmit: Function,
  initializeForm: Function,
  cacheSearchOptions: Function,
  actionType?: string,
  initialValue?: {
    startDate?: string,
    endDate?: string,
    employeeId?: string,
  },
  hide?: boolean,
};

class PerformanceSearchForm extends PureComponent<Props> {
  static defaultProps = {
    initialValue: null,
    actionType: null,
    hide: false,
  };

  componentDidMount() {
    const {
      initialValue,
    } = this.props;

    this.initializing(initialValue);
  }

  componentDidUpdate(prevProps) {
    const {
      initialValue,
    } = this.props;

    if (initialValue && initialValue !== prevProps.initialValue) {
      this.initializing(initialValue);
    }
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    initializeForm({
      employeeId: (obj && obj.employeeId) || '-1',
      startDate: (obj && obj.startDate) ? moment(obj.startDate) : null,
      endDate: (obj && obj.endDate) ? moment(obj.endDate) : null,
    });
  }

  submit(d) {
    const {
      onSearch,
      actionType,
      cacheSearchOptions,
    } = this.props;

    const payload = {
      employeeId: d.employeeId && d.employeeId !== '-1' ? d.employeeId : null,
      startDate: d.startDate ? moment(d.startDate).format('YYYY-MM-DD') : null,
      endDate: d.endDate ? moment(d.endDate).format('YYYY-MM-DD') : null,
    };

    if (onSearch) {
      onSearch(payload);
    }

    if (actionType) {
      cacheSearchOptions(actionType, payload);
    }
  }

  render() {
    const {
      handleSubmit,
      hide,
    } = this.props;

    return (
      <form
        style={[
          styles.searchWrapper,
          hide && { display: 'none' },
        ]}
        onSubmit={handleSubmit(d => this.submit(d))}>
        <Fields
          disableTime
          names={[
            'startDate',
            'endDate',
          ]}
          component={DatetimeRangeSelectorBind} />
        <EmployeeSelectorBind roles={LINEBOT_PERFORMANCE_EMPLOYEE_SELECTOR_KEYS} />
        <div style={styles.functionWrapper}>
          <SubmitButton label="查詢" />
        </div>
      </form>
    );
  }
}

export default null;

export function wrapFormToPerformanceForm(form) {
  if (!form) return null;

  return connect(
    () => ({}),
    dispatch => bindActionCreators({
      ...SearchActions,
      initializeForm: v => initialize(form, v),
    }, dispatch),
  )(reduxForm({
    form,
  })(radium(PerformanceSearchForm)));
}
