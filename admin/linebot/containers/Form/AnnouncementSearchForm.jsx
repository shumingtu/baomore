// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  initialize,
} from 'redux-form';

import * as SearchActions from '../../../actions/Search.js';
// components
import Input from '../../../components/Form/Input.jsx';
import SubmitButton from '../../../components/Form/SubmitButton.jsx';

const styles = {
  searchWrapper: {
    width: '100%',
    height: 'auto',
    padding: 16,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '6px 0',
  },
};

type Props = {
  onSearch: Function,
  handleSubmit: Function,
  initializeForm: Function,
  cacheSearchOptions: Function,
  actionType?: string,
  initialValue?: {
    keyword?: string,
  },
};

class AnnouncementSearchForm extends PureComponent<Props> {
  static defaultProps = {
    initialValue: null,
    actionType: null,
  };

  componentDidMount() {
    const {
      initialValue,
    } = this.props;

    this.initializing(initialValue);
  }

  componentDidUpdate(prevProps) {
    const {
      initialValue,
    } = this.props;

    if (initialValue && initialValue !== prevProps.initialValue) {
      this.initializing(initialValue);
    }
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    initializeForm({
      keyword: (obj && obj.keyword) || '',
    });
  }

  submit(d) {
    const {
      onSearch,
      actionType,
      cacheSearchOptions,
    } = this.props;

    const payload = {
      keyword: d.keyword || null,
    };

    if (onSearch) {
      onSearch(payload);
    }

    if (actionType) {
      cacheSearchOptions(actionType, payload);
    }
  }

  render() {
    const {
      handleSubmit,
    } = this.props;

    return (
      <form style={styles.searchWrapper} onSubmit={handleSubmit(d => this.submit(d))}>
        <div style={styles.fieldWrapper}>
          <Field
            name="keyword"
            label="關鍵字搜尋："
            placeholder="關鍵字"
            component={Input} />
        </div>
        <div style={styles.functionWrapper}>
          <SubmitButton label="查詢" />
        </div>
      </form>
    );
  }
}

export default null;

export function wrapFormToAnnoucementSearchForm(form) {
  if (!form) return null;

  return connect(
    () => ({}),
    dispatch => bindActionCreators({
      ...SearchActions,
      initializeForm: v => initialize(form, v),
    }, dispatch),
  )(reduxForm({
    form,
  })(radium(AnnouncementSearchForm)));
}
