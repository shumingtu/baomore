// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { withRouter } from 'react-router-dom';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Fields,
  initialize,
} from 'redux-form';

import * as SearchActions from '../../../actions/Search.js';
// components
import ChannelStoreSelectorBind from '../../components/Form/ChannelStoreSelectorBind.jsx';
import DatetimeRangeSelectorBind from '../../components/Form/DatetimeRangeSelectorBind.jsx';
import SubmitButton from '../../../components/Form/SubmitButton.jsx';
import Button from '../../../components/Global/Button.jsx';

const styles = {
  searchWrapper: {
    width: '100%',
    height: 'auto',
    padding: 16,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '6px 0',
  },
  btnWrapper: {
    width: 'auto',
    height: 'auto',
    padding: 4,
  },
};

type Props = {
  onSearch: Function,
  handleSubmit: Function,
  initializeForm: Function,
  cacheSearchOptions: Function,
  actionType?: string,
  initialValue?: {
    employeeId?: string,
    startDate?: string,
    endDate?: string,
    cityId?: string,
    storeId?: string,
  },
  history: {
    goBack: Function,
  },
  returnLabel: string,
};

class PunchSearchForm extends PureComponent<Props> {
  static defaultProps = {
    initialValue: null,
    actionType: null,
  };

  componentDidMount() {
    const {
      initialValue,
    } = this.props;

    this.initializing(initialValue);
  }

  componentDidUpdate(prevProps) {
    const {
      initialValue,
    } = this.props;

    if (initialValue && initialValue !== prevProps.initialValue) {
      this.initializing(initialValue);
    }
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    initializeForm({
      employeeId: (obj && obj.employeeId) || '-1',
      startDate: (obj && obj.startDate) ? moment(obj.startDate) : null,
      endDate: (obj && obj.endDate) ? moment(obj.endDate) : null,
      cityId: (obj && obj.cityId) || '-1',
      storeId: (obj && obj.storeId) || '-1',
    });
  }

  submit(d) {
    const {
      onSearch,
      actionType,
      cacheSearchOptions,
    } = this.props;

    const payload = {
      employeeId: d.employeeId && d.employeeId !== '-1' ? d.employeeId : null,
      startDate: d.startDate ? moment(d.startDate).format('YYYY-MM-DD HH:mm') : null,
      endDate: d.endDate ? moment(d.endDate).format('YYYY-MM-DD HH:mm') : null,
      cityId: d.cityId && d.cityId !== '-1' ? d.cityId : null,
      storeId: d.storeId && d.storeId !== '-1' ? d.storeId : null,
    };

    if (onSearch) {
      onSearch(payload);
    }

    if (actionType) {
      cacheSearchOptions(actionType, payload);
    }
  }

  render() {
    const {
      handleSubmit,
      history,
      returnLabel,
    } = this.props;

    return (
      <form style={styles.searchWrapper} onSubmit={handleSubmit(d => this.submit(d))}>
        <Fields
          names={[
            'startDate',
            'endDate',
          ]}
          component={DatetimeRangeSelectorBind} />
        <Fields
          disableRequirement
          names={[
            'cityId',
            'storeId',
          ]}
          component={ChannelStoreSelectorBind} />
        <div style={styles.functionWrapper}>
          <div style={styles.btnWrapper}>
            <Button
              isWhiteBg
              label={returnLabel}
              onClick={() => history.goBack()} />
          </div>
          <div style={styles.btnWrapper}>
            <SubmitButton label="查詢" />
          </div>
        </div>
      </form>
    );
  }
}

export default null;

export function wrapFormToPunchSearchForm(form) {
  if (!form) return null;

  return withRouter(connect(
    () => ({}),
    dispatch => bindActionCreators({
      ...SearchActions,
      initializeForm: v => initialize(form, v),
    }, dispatch),
  )(reduxForm({
    form,
  })(radium(PunchSearchForm))));
}
