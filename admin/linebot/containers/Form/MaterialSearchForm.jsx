// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Fields,
  Field,
  initialize,
} from 'redux-form';

import * as SearchActions from '../../../actions/Search.js';
import { LINEBOT_INVENTORY_EMPLOYEE_SELECTOR_KEYS } from '../../../../shared/selectorActionKeys.js';
// components
import PNSubcategorySelectorBind from '../../components/Form/PNSubcategorySelectorBind.jsx';
import EmployeeSelectorBind from '../../components/Form/EmployeeSelectorBind.jsx';
import Input from '../../../components/Form/Input.jsx';
import SubmitButton from '../../../components/Form/SubmitButton.jsx';

const styles = {
  searchWrapper: {
    width: '100%',
    height: 'auto',
    padding: 16,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '6px 0',
  },
};

type Props = {
  onSearch: Function,
  handleSubmit: Function,
  initializeForm: Function,
  cacheSearchOptions: Function,
  actionType?: string,
  initialValue?: {
    employeeId?: string,
    pnCategoryId?: string,
    bigSubCategoryId?: string,
    smallSubCategoryId?: string,
  },
  hide?: boolean,
};

class MaterialSearchForm extends PureComponent<Props> {
  static defaultProps = {
    initialValue: null,
    actionType: null,
    hide: false,
  };

  componentDidMount() {
    const {
      initialValue,
    } = this.props;

    this.initializing(initialValue);
  }

  componentDidUpdate(prevProps) {
    const {
      initialValue,
    } = this.props;

    if (initialValue && initialValue !== prevProps.initialValue) {
      this.initializing(initialValue);
    }
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    initializeForm({
      employeeId: (obj && obj.employeeId) || '-1',
      pnCategoryId: (obj && obj.pnCategoryId) || '-1',
      bigSubCategoryId: (obj && obj.bigSubCategoryId) || '-1',
      smallSubCategoryId: (obj && obj.smallSubCategoryId) || '-1',
      keyword: (obj && obj.keyword) || '',
    });
  }

  submit(d) {
    const {
      onSearch,
      actionType,
      cacheSearchOptions,
    } = this.props;

    const payload = {
      pnCategoryId: d.pnCategoryId && d.pnCategoryId !== '-1' ? d.pnCategoryId : null,
      bigSubCategoryId: d.bigSubCategoryId && d.bigSubCategoryId !== '-1' ? d.bigSubCategoryId : null,
      smallSubCategoryId: d.smallSubCategoryId && d.smallSubCategoryId !== '-1' ? d.smallSubCategoryId : null,
      keyword: d.keyword || null,
      employeeId: d.employeeId && d.employeeId !== '-1' ? d.employeeId : null,
    };

    if (onSearch) {
      onSearch(payload);
    }

    if (actionType) {
      cacheSearchOptions(actionType, payload);
    }
  }

  render() {
    const {
      handleSubmit,
      hide,
    } = this.props;

    return (
      <form
        style={[
          styles.searchWrapper,
          hide && { display: 'none' },
        ]}
        onSubmit={handleSubmit(d => this.submit(d))}>
        <EmployeeSelectorBind roles={LINEBOT_INVENTORY_EMPLOYEE_SELECTOR_KEYS} />
        <Fields
          names={[
            'pnCategoryId',
            'bigSubCategoryId',
            'smallSubCategoryId',
          ]}
          component={PNSubcategorySelectorBind} />
        <div style={styles.fieldWrapper}>
          <Field
            name="keyword"
            label="關鍵字搜尋："
            placeholder="品名"
            component={Input} />
        </div>
        <div style={styles.functionWrapper}>
          <SubmitButton label="查詢" />
        </div>
      </form>
    );
  }
}

export default null;

export function wrapFormToMaterialSearchForm(form) {
  if (!form) return null;

  return connect(
    () => ({}),
    dispatch => bindActionCreators({
      ...SearchActions,
      initializeForm: v => initialize(form, v),
    }, dispatch),
  )(reduxForm({
    form,
  })(radium(MaterialSearchForm)));
}
