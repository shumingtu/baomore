// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  Fields,
  initialize,
} from 'redux-form';

import * as SearchActions from '../../../actions/Search.js';
// components
import Input from '../../../components/Form/Input.jsx';
import ChannelStoreSelectorBind from '../../components/Form/ChannelStoreSelectorBind.jsx';
import SubmitButton from '../../../components/Form/SubmitButton.jsx';

const styles = {
  searchWrapper: {
    width: '100%',
    height: 'auto',
    padding: 16,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '6px 0',
  },
};

type Props = {
  onSearch: Function,
  handleSubmit: Function,
  initializeForm: Function,
  cacheSearchOptions: Function,
  actionType?: string,
  initialValue?: {
    cityId?: string,
    storeId?: string,
    keyword?: string,
  },
  hide?: boolean,
};

class StoreSearchForm extends PureComponent<Props> {
  static defaultProps = {
    initialValue: null,
    actionType: null,
    hide: false,
  };

  componentDidMount() {
    const {
      initialValue,
    } = this.props;

    this.initializing(initialValue);
  }

  componentDidUpdate(prevProps) {
    const {
      initialValue,
    } = this.props;

    if (initialValue && initialValue !== prevProps.initialValue) {
      this.initializing(initialValue);
    }
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    initializeForm({
      cityId: (obj && obj.cityId) || '-1',
      storeId: (obj && obj.storeId) || '-1',
      keyword: (obj && obj.keyword) || '',
    });
  }

  submit(d) {
    const {
      onSearch,
      actionType,
      cacheSearchOptions,
    } = this.props;

    const payload = {
      cityId: d.cityId && d.cityId !== '-1' ? d.cityId : null,
      storeId: d.storeId && d.storeId !== '-1' ? d.storeId : null,
      keyword: d.keyword || null,
    };

    if (onSearch) {
      onSearch(payload);
    }

    if (actionType) {
      cacheSearchOptions(actionType, payload);
    }
  }

  render() {
    const {
      handleSubmit,
      hide,
    } = this.props;

    return (
      <form
        style={[
          styles.searchWrapper,
          hide && { display: 'none' },
        ]}
        onSubmit={handleSubmit(d => this.submit(d))}>
        <Fields
          disableRequirement
          names={[
            'cityId',
            'storeId',
          ]}
          component={ChannelStoreSelectorBind} />
        <div style={styles.fieldWrapper}>
          <Field
            name="keyword"
            label="關鍵字搜尋："
            placeholder="名稱、電話、地址"
            component={Input} />
        </div>
        <div style={styles.functionWrapper}>
          <SubmitButton label="查詢" />
        </div>
      </form>
    );
  }
}

export default null;

export function wrapFormToStoreSearchForm(form) {
  if (!form) return null;

  return connect(
    () => ({}),
    dispatch => bindActionCreators({
      ...SearchActions,
      initializeForm: v => initialize(form, v),
    }, dispatch),
  )(reduxForm({
    form,
  })(radium(StoreSearchForm)));
}
