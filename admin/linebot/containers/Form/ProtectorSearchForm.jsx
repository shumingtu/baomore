// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Fields,
  Field,
  initialize,
} from 'redux-form';

import * as SearchActions from '../../../actions/Search.js';
import { LINEBOT_INVENTORY_EMPLOYEE_SELECTOR_KEYS } from '../../../../shared/selectorActionKeys.js';
// components
import PhoneBrandModelSelectorBind from '../../components/Form/PhoneBrandModelSelectorBind.jsx';
import EmployeeSelectorBind from '../../components/Form/EmployeeSelectorBind.jsx';
import Input from '../../../components/Form/Input.jsx';
import SubmitButton from '../../../components/Form/SubmitButton.jsx';

const styles = {
  searchWrapper: {
    width: '100%',
    height: 'auto',
    padding: 16,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '6px 0',
  },
};

type Props = {
  onSearch: Function,
  handleSubmit: Function,
  initializeForm: Function,
  cacheSearchOptions: Function,
  actionType?: string,
  initialValue?: {
    employeeId?: string,
    brandId?: string,
    modelId?: string,
    keyword?: string,
  },
  hide?: boolean,
};

class ProtectorSearchForm extends PureComponent<Props> {
  static defaultProps = {
    initialValue: null,
    actionType: null,
    hide: false,
  };

  componentDidMount() {
    const {
      initialValue,
    } = this.props;

    this.initializing(initialValue);
  }

  componentDidUpdate(prevProps) {
    const {
      initialValue,
    } = this.props;

    if (initialValue && initialValue !== prevProps.initialValue) {
      this.initializing(initialValue);
    }
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    initializeForm({
      employeeId: (obj && obj.employeeId) || '-1',
      brandId: (obj && obj.brandId) || '-1',
      modelId: (obj && obj.modelId) || '-1',
      keyword: (obj && obj.keyword) || '',
    });
  }

  submit(d) {
    const {
      onSearch,
      actionType,
      cacheSearchOptions,
    } = this.props;

    const payload = {
      brandId: d.brandId && d.brandId !== '-1' ? d.brandId : null,
      modelId: d.modelId && d.modelId !== '-1' ? d.modelId : null,
      keyword: d.keyword || null,
      employeeId: d.employeeId && d.employeeId !== '-1' ? d.employeeId : null,
    };

    if (onSearch) {
      onSearch(payload);
    }

    if (actionType) {
      cacheSearchOptions(actionType, payload);
    }
  }

  render() {
    const {
      handleSubmit,
      hide,
    } = this.props;

    return (
      <form
        style={[
          styles.searchWrapper,
          hide && { display: 'none' },
        ]}
        onSubmit={handleSubmit(d => this.submit(d))}>
        <EmployeeSelectorBind roles={LINEBOT_INVENTORY_EMPLOYEE_SELECTOR_KEYS} />
        <Fields
          names={[
            'brandId',
            'modelId',
          ]}
          component={PhoneBrandModelSelectorBind} />
        <div style={styles.fieldWrapper}>
          <Field
            name="keyword"
            label="關鍵字搜尋："
            placeholder="品名"
            component={Input} />
        </div>
        <div style={styles.functionWrapper}>
          <SubmitButton label="查詢" />
        </div>
      </form>
    );
  }
}

export default null;

export function wrapFormToProtectorSearchForm(form) {
  if (!form) return null;

  return connect(
    () => ({}),
    dispatch => bindActionCreators({
      ...SearchActions,
      initializeForm: v => initialize(form, v),
    }, dispatch),
  )(reduxForm({
    form,
  })(radium(ProtectorSearchForm)));
}
