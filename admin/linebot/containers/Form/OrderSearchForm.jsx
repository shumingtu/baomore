// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  initialize,
} from 'redux-form';

import { SHIPPING_STATUS_SPEC } from '../../../shared/Global.js';
import * as SearchActions from '../../../actions/Search.js';
// components
import Input from '../../../components/Form/Input.jsx';
import Selector from '../../../components/Form/Selector.jsx';
import SubmitButton from '../../../components/Form/SubmitButton.jsx';

const styles = {
  searchWrapper: {
    width: '100%',
    height: 'auto',
    padding: 16,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '3px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '6px 0',
  },
};

type Props = {
  onSearch: Function,
  handleSubmit: Function,
  initializeForm: Function,
  cacheSearchOptions: Function,
  actionType?: string,
  initialValue?: {
    status?: string,
    keyword?: string,
  },
  hide?: boolean,
};

const shippingStatus = Object.keys(SHIPPING_STATUS_SPEC).map(k => ({
  id: k,
  name: SHIPPING_STATUS_SPEC[k],
}));

class OrderSearchForm extends PureComponent<Props> {
  static defaultProps = {
    initialValue: null,
    actionType: null,
    hide: false,
  };

  componentDidMount() {
    const {
      initialValue,
    } = this.props;

    this.initializing(initialValue);
  }

  componentDidUpdate(prevProps) {
    const {
      initialValue,
    } = this.props;

    if (initialValue && initialValue !== prevProps.initialValue) {
      this.initializing(initialValue);
    }
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    initializeForm({
      status: (obj && obj.status) || '-1',
      keyword: (obj && obj.keyword) || '',
      orderSN: (obj && obj.orderSN) || '',
      closeDate: (obj && obj.closeDate) || '',
      qrcode: (obj && obj.qrcode) || '',
    });
  }

  submit(d) {
    const {
      onSearch,
      actionType,
      cacheSearchOptions,
    } = this.props;

    const payload = {
      status: d.status && d.status !== '-1' ? d.status : null,
      keyword: d.keyword || null,
      orderSN: d.orderSN || null,
      closeDate: d.closeDate || null,
      qrcode: d.qrcode || null,
    };

    if (onSearch) {
      onSearch(payload);
    }

    if (actionType) {
      cacheSearchOptions(actionType, payload);
    }
  }

  render() {
    const {
      handleSubmit,
      hide,
    } = this.props;

    return (
      <form
        style={[
          styles.searchWrapper,
          hide && { display: 'none' },
        ]}
        onSubmit={handleSubmit(d => this.submit(d))}>
        <div style={styles.fieldWrapper}>
          <Field
            nullable
            name="status"
            label="狀態："
            options={shippingStatus}
            component={Selector} />
        </div>
        <div style={styles.fieldWrapper}>
          <Field
            name="keyword"
            label="關鍵字搜尋："
            placeholder="品名, 料號"
            component={Input} />
        </div>
        <div style={styles.fieldWrapper}>
          <Field
            name="orderSN"
            label="訂單編號："
            placeholder="訂單編號"
            component={Input} />
        </div>
        <div style={styles.fieldWrapper}>
          <Field
            name="qrcode"
            label="商品序號："
            placeholder="商品序號"
            component={Input} />
        </div>
        <div style={styles.fieldWrapper}>
          <Field
            name="closeDate"
            label="結單日期："
            placeholder="結單日期"
            noDefaultValue
            type="date"
            component={Input} />
        </div>
        <div style={styles.functionWrapper}>
          <SubmitButton label="查詢" />
        </div>
      </form>
    );
  }
}

export default null;

export function wrapFormToOrderSearchForm(form) {
  if (!form) return null;

  return connect(
    () => ({}),
    dispatch => bindActionCreators({
      ...SearchActions,
      initializeForm: v => initialize(form, v),
    }, dispatch),
  )(reduxForm({
    form,
  })(radium(OrderSearchForm)));
}
