// @flow
import React, { PureComponent } from 'react';
import {
  Query,
} from 'react-apollo';

import { FETCH_MY_INVENTORY_DETAIL } from '../../../queries/Inventory.js';
// components
import TransferActionView from './TransferActionView.jsx';

type Props = {
  match: {
    params: {
      pnTableId: string,
    },
  },
};

class TransferActionWrapper extends PureComponent<Props> {
  render() {
    const {
      match: {
        params: {
          pnTableId,
        },
      },
    } = this.props;

    return (
      <Query
        query={FETCH_MY_INVENTORY_DETAIL}
        variables={{
          pnTableId: pnTableId ? parseInt(pnTableId, 10) : -1,
        }}>
        {({
          data,
          error,
        }) => {
          const meInventory = (data && data.meInventory) || {};

          return (
            <TransferActionView
              {...this.props}
              myInventory={meInventory}
              error={error} />
          );
        }}
      </Query>
    );
  }
}

export default TransferActionWrapper;
