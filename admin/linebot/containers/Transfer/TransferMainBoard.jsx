// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

import { pnCategoryTypeIsProtector } from '../../../helper/linebot.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import TransferProtectorBoard from './TransferProtectorBoard.jsx';
import TransferMaterialBoard from './TransferMaterialBoard.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
  mainWrapper: {
    width: '100%',
    height: 'calc(100vh - 90px)',
  },
};

type Props = {
  match: {
    params: {
      pnCategoryType: string,
    },
  },
};

class TransferMainBoard extends PureComponent<Props> {
  categoryDecider() {
    const {
      match: {
        params: {
          pnCategoryType,
          pnCategoryId,
        },
      },
    } = this.props;

    const isProtector = pnCategoryTypeIsProtector(pnCategoryType);

    if (isProtector) {
      return (
        <TransferProtectorBoard pnCategoryId={pnCategoryId} />
      );
    }

    return (
      <TransferMaterialBoard defaultPNCategoryId={pnCategoryId} />
    );
  }

  render() {
    return (
      <div style={styles.wrapper}>
        <PageTitle title="調貨" />
        <div style={styles.mainWrapper}>
          {this.categoryDecider()}
        </div>
      </div>
    );
  }
}

export default radium(TransferMainBoard);
