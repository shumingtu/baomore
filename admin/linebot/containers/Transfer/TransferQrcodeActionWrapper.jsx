// @flow
import React, { PureComponent } from 'react';

// components
import TransferActionView from './TransferActionView.jsx';

type Props = {
  match: {
    params: {
      itemId: string,
    },
  },
};

class TransferActionWrapper extends PureComponent<Props> {
  render() {
    const {
      match: {
        params: {
          qrcode,
        },
      },
    } = this.props;

    return (
      <TransferActionView {...this.props} />
    );
  }
}

export default TransferActionWrapper;
