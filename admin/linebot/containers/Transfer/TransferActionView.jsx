// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import {
  reduxForm,
  Field,
  SubmissionError,
  initialize,
} from 'redux-form';
import { bindActionCreators } from 'redux';
import {
  Query,
  Mutation,
} from 'react-apollo';
import { connect } from 'react-redux';

import Theme from '../../../styles/Theme.js';
import { GET_EMPLOYEE_LIST } from '../../../queries/Member.js';
import { TRANSFER_ITEMS } from '../../../mutations/Transfer.js';
import { FORM_LINEBOT_TRANSFER_CONFIRM_FORM } from '../../../shared/form.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import Error from '../../../components/Form/Error.jsx';
import Input from '../../../components/Form/Input.jsx';
import Selector from '../../../components/Form/Selector.jsx';
import SubmitButton from '../../../components/Form/SubmitButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
  mainWrapper: {
    width: '100%',
    height: 'calc(100vh - 90px)',
    overflow: 'auto',
  },
  infoWrapper: {
    width: '100%',
    height: 'auto',
    flex: 1,
    padding: 12,
    overflowY: 'auto',
    overflowX: 'hidden',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  label: {
    width: 65,
    fontSize: 13,
    fontWeight: 500,
    color: Theme.BLACK_COLOR,
  },
  text: {
    flex: 1,
    fontSize: 13,
    color: Theme.BLACK_COLOR,
    lineHeight: 1,
    whiteSpace: 'pre-line',
    wordBreak: 'break-word',
  },
  formWrapper: {
    width: '100%',
    height: 'auto',
    minHeight: '50vh',
    padding: '6px 0 24px 0',
  },
  formTitleWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottom: `2px solid ${Theme.SECONDARY_THEME_COLOR}`,
  },
  formTitle: {
    fontSize: 15,
    fontWeight: 500,
    letterSpacing: 1,
    color: Theme.BLACK_COLOR,
  },
  mainFormWrapper: {
    width: '100%',
    height: 'auto',
    padding: 12,
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

type Props = {
  pnTableId?: string,
  myInventory?: {
    amount: number,
    pnTable: {
      code: string,
      name: string,
      pnCategory: {
        name: string,
      },
      bigSubCategory: {
        name: string,
      },
      smallSubCategory: {
        name: string,
      },
    },
  },
  error?: {
    message: string,
  },
  handleSubmit: Function,
  memberAreaId: number,
  initializeForm: Function,
  match: object,
  memberId: number,
};

type State = {
  isComplete: boolean,
};

class TransferActionView extends PureComponent<Props, State> {
  static defaultProps = {
    pnTableId: null,
    myInventory: null,
    error: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isComplete: false,
    };
  }

  componentDidMount() {
    const {
      match: {
        params: {
          qrcode,
        },
      },
      initializeForm,
    } = this.props;

    initializeForm({
      qrcode,
    });
  }

  getFields(d) {
    const {
      pnTableId,
    } = this.props;

    const {
      qrcode,
      employeeId,
      remark,
    } = d;

    if (!qrcode) {
      throw new SubmissionError({
        qrcode: 'required',
      });
    }

    if (!employeeId || employeeId === '-1') {
      throw new SubmissionError({
        employeeId: 'required',
      });
    }

    if (!remark) {
      throw new SubmissionError({
        remark: 'required',
      });
    }

    if (!pnTableId) {
      return ({
        qrcode,
        employeeId: parseInt(employeeId, 10),
        remark: remark || null,
      });
    }

    return ({
      pnTableId: pnTableId ? parseInt(pnTableId, 10) : -1,
      qrcode,
      employeeId: parseInt(employeeId, 10),
      remark: remark || null,
    });
  }

  render() {
    const {
      myInventory,
      handleSubmit,
      error,
      memberAreaId,
      memberId,
    } = this.props;

    const {
      isComplete,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        <PageTitle title="調貨資料確認" />
        <div style={styles.mainWrapper}>
          {myInventory ? (
            <div style={styles.infoWrapper}>
              <div style={styles.fieldWrapper}>
                <span style={styles.label}>
                  料號編號：
                </span>
                {myInventory.pnTable && myInventory.pnTable.code ? (
                  <span style={styles.text}>
                    {myInventory.pnTable.code}
                  </span>
                ) : null}
              </div>
              <div style={styles.fieldWrapper}>
                <span style={styles.label}>
                  料號類別：
                </span>
                {myInventory.pnTable && myInventory.pnTable.pnCategory ? (
                  <span style={styles.text}>
                    {myInventory.pnTable.pnCategory.name || null}
                  </span>
                ) : null}
              </div>
              <div style={styles.fieldWrapper}>
                <span style={styles.label}>
                  大類：
                </span>
                {myInventory.pnTable && myInventory.pnTable.bigSubCategory ? (
                  <span style={styles.text}>
                    {myInventory.pnTable.bigSubCategory.name || null}
                  </span>
                ) : null}
              </div>
              <div style={styles.fieldWrapper}>
                <span style={styles.label}>
                  類型：
                </span>
                {myInventory.pnTable && myInventory.pnTable.smallSubCategory ? (
                  <span style={styles.text}>
                    {myInventory.pnTable.smallSubCategory.name || null}
                  </span>
                ) : null}
              </div>
              <div style={styles.fieldWrapper}>
                <span style={styles.label}>
                  品名：
                </span>
                {myInventory.pnTable && myInventory.pnTable.name ? (
                  <span style={styles.text}>
                    {myInventory.pnTable.name}
                  </span>
                ) : null}
              </div>
              <div style={styles.fieldWrapper}>
                <span style={styles.label}>
                  數量：
                </span>
                <span style={styles.text}>
                  {myInventory.amount || 0}
                </span>
              </div>
            </div>
          ) : null}
          {error ? (
            <Error error={error.message || null} />
          ) : (
            <Mutation mutation={TRANSFER_ITEMS}>
              {(transfer, mutateProps) => (
                <form
                  style={styles.formWrapper}
                  onSubmit={handleSubmit(async (d) => {
                    const payload = this.getFields(d);

                    const res = await transfer({
                      variables: payload,
                    });

                    if (res && res.data && res.data.transfer && res.data.transfer.status) {
                      this.setState({
                        isComplete: true,
                      });
                    }
                  })}>
                  <div style={styles.formTitleWrapper}>
                    <span style={styles.formTitle}>
                      填寫調貨資訊
                    </span>
                  </div>
                  <div style={styles.mainFormWrapper}>
                    <div style={styles.fieldWrapper}>
                      <Field
                        required
                        name="qrcode"
                        label="商品序號："
                        placeholder="請輸入商品序號"
                        component={Input} />
                    </div>
                    <Query
                      variables={{
                        areaId: memberAreaId || null,
                      }}
                      query={GET_EMPLOYEE_LIST}>
                      {({
                        data,
                      }) => {
                        let employeeMemberlist = (data && data.employeeMemberlist) || [];

                        if (memberId) {
                          employeeMemberlist = employeeMemberlist.filter(
                            member => member.id !== memberId
                          );
                        }

                        return (
                          <div style={styles.fieldWrapper}>
                            <Field
                              nullable
                              required
                              name="employeeId"
                              label="選擇包膜師："
                              options={employeeMemberlist}
                              placeholder="請選擇包膜師"
                              component={Selector} />
                          </div>
                        );
                      }}
                    </Query>
                    <div style={styles.fieldWrapper}>
                      <Field
                        required
                        type="textarea"
                        name="remark"
                        label="備註："
                        placeholder=""
                        component={Input} />
                    </div>
                    {isComplete || mutateProps.error ? (
                      <Error
                        error={(mutateProps.error && mutateProps.error.message) || (isComplete && '調貨成功')
                        || null} />
                    ) : null}
                    <div style={styles.functionWrapper}>
                      <SubmitButton
                        disabled={mutateProps.loading || false}
                        label="調貨確認" />
                    </div>
                  </div>
                </form>
              )}
            </Mutation>
          )}
        </div>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_LINEBOT_TRANSFER_CONFIRM_FORM,
});

const reduxHook = connect(
  state => ({
    memberAreaId: state.Member.memberAreaId,
    memberId: state.Member.memberId,
  }),
  dispatch => bindActionCreators({
    initializeForm: v => initialize(FORM_LINEBOT_TRANSFER_CONFIRM_FORM, v),
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      TransferActionView
    )
  )
);
