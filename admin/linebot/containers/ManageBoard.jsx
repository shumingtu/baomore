import React, { PureComponent } from 'react';
import radium from 'radium';

// components
import PageTitle from '../components/Global/PageTitle.jsx';
import PageLink from '../components/Global/PageLink.jsx';
import OrderListBoard from './Order/OrderListBoard.jsx';
import Button from '../../components/Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    position: 'relative',
  },
  mainPlacement: {
    width: '100%',
    height: 'calc(100vh - 90px)',
    padding: '12px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  linksPlacement: {
    width: '100%',
    height: 90,
    padding: '0 3px 12px',
    overflow: 'auto',
  },
  linksWrapper: {
    width: '100%',
    height: '100%',
    minWidth: 375,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  pagelink: {
    width: 70,
    height: 70,
    margin: '0 1px',
  },
  listWrapper: {
    width: '100%',
    height: '100%',
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  collapseBtn: {
    position: 'absolute',
    top: 4,
    right: 10,
    zIndex: 1,
  },
};

class ManageBoard extends PureComponent {
  state = {
    hideLinkAndForm: false,
  };

  render() {
    const {
      hideLinkAndForm,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        <PageTitle title="存貨管理" />
        <div style={styles.collapseBtn}>
          <Button
            onClick={() => this.setState({ hideLinkAndForm: !hideLinkAndForm })}
            label={hideLinkAndForm ? '展開' : '收合'} />
        </div>
        <div style={styles.mainPlacement}>
          {hideLinkAndForm ? null : (
            <div style={styles.linksPlacement}>
              <div style={styles.linksWrapper}>
                <PageLink
                  name="存貨"
                  path="/linebot/inventory"
                  style={styles.pagelink} />
                <PageLink
                  name="訂貨"
                  path="/linebot/purchase"
                  style={styles.pagelink} />
                <PageLink
                  name="調貨"
                  path="/linebot/transfer"
                  style={styles.pagelink} />
                <PageLink
                  name="退貨"
                  path="/linebot/return"
                  style={styles.pagelink} />
                <PageLink
                  name="客訴退回"
                  path="/linebot/customerComplaint"
                  style={styles.pagelink} />
              </div>
            </div>
          )}
          <div style={styles.listWrapper}>
            <OrderListBoard hideSearchFrom={hideLinkAndForm} />
          </div>
        </div>
      </div>
    );
  }
}

export default radium(ManageBoard);
