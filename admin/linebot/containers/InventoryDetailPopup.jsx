// @flow
import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';

// static, config
import Theme from '../../styles/Theme.js';
import closeIcon from '../../static/images/close-icon.png';
import { FETCH_MY_INVENTORY_DETAIL } from '../../queries/Inventory.js';
// components
import Error from '../../components/Form/Error.jsx';

const styles = {
  wrapper: {
    position: 'fixed',
    left: 0,
    top: 50,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    padding: 12,
    zIndex: 999,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainPlacement: {
    width: 'auto',
    height: 'auto',
    minWidth: 270,
    maxHeight: 'calc(100vh - 24px)',
    overflow: 'auto',
  },
  mainWrapper: {
    width: '100%',
    height: 'auto',
    maxWidth: 414,
    padding: '12px 24px',
    backgroundColor: '#fff',
    borderRadius: 3,
    border: `1px solid ${Theme.BLACK_COLOR}`,
    position: 'relative',
  },
  closeBtn: {
    position: 'absolute',
    top: 12,
    right: 12,
    width: 24,
    height: 24,
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundImage: `url(${closeIcon})`,
    cursor: 'pointer',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '4px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  image: {
    width: '100%',
    height: 200,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  },
  label: {
    width: 65,
    fontSize: 13,
    fontWeight: 500,
    color: Theme.BLACK_COLOR,
  },
  text: {
    flex: 1,
    fontSize: 13,
    color: Theme.BLACK_COLOR,
    lineHeight: 1,
    whiteSpace: 'pre-line',
    wordBreak: 'break-word',
  },
};

type Props = {
  location: {
    search: string,
  },
  onClose: Function,
  match: {
    params: {
      pnTableId: string,
    },
  },
  closeDate?: string,
  showCloseDate?: boolean,
  queryOptions?: object,
};

class InventoryDetailPopup extends PureComponent<Props> {
  static defaultProps = {
    closeDate: null,
    showCloseDate: false,
    queryOptions: null,
  };

  render() {
    const {
      onClose,
      match: {
        params: {
          pnTableId,
        },
      },
      closeDate,
      showCloseDate,
      queryOptions,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.mainPlacement}>
          <Query
            query={FETCH_MY_INVENTORY_DETAIL}
            variables={{
              pnTableId: pnTableId ? parseInt(pnTableId, 10) : -1,
              employeeId: queryOptions
              && queryOptions.employeeId
              && parseInt(queryOptions.employeeId, 10),
            }}>
            {({
              data,
              error,
            }) => {
              const meInventory = (data && data.meInventory) || {};
              const {
                pnTable = {},
              } = meInventory;

              return (
                <div style={styles.mainWrapper}>
                  <button
                    type="button"
                    onClick={onClose || null}
                    style={styles.closeBtn} />
                  {error ? (
                    <Error error={error.message || null} />
                  ) : (
                    <Fragment>
                      <div style={styles.fieldWrapper}>
                        <div
                          style={[
                            styles.image,
                            {
                              backgroundImage: pnTable.picture ? `url(${pnTable.picture})` : null,
                            },
                          ]} />
                      </div>
                      {showCloseDate ? (
                        <div style={styles.fieldWrapper}>
                          <span style={styles.label}>
                            結單日期：
                          </span>
                          {closeDate ? (
                            <span style={styles.text}>
                              {closeDate}
                            </span>
                          ) : (
                            <span style={styles.text}>
                              尚未結單
                            </span>
                          )}
                        </div>
                      ) : null}
                      <div style={styles.fieldWrapper}>
                        <span style={styles.label}>
                          料號編號：
                        </span>
                        {pnTable.code ? (
                          <span style={styles.text}>
                            {pnTable.code}
                          </span>
                        ) : null}
                      </div>
                      <div style={styles.fieldWrapper}>
                        <span style={styles.label}>
                          料號類別：
                        </span>
                        {pnTable && pnTable.pnCategory ? (
                          <span style={styles.text}>
                            {pnTable.pnCategory.name || null}
                          </span>
                        ) : null}
                      </div>
                      <div style={styles.fieldWrapper}>
                        <span style={styles.label}>
                          手機品牌：
                        </span>
                        {pnTable && pnTable.Brand ? (
                          <span style={styles.text}>
                            {pnTable.Brand.name || null}
                          </span>
                        ) : null}
                      </div>
                      <div style={styles.fieldWrapper}>
                        <span style={styles.label}>
                          手機機型：
                        </span>
                        {pnTable && pnTable.BrandModel ? (
                          <span style={styles.text}>
                            {pnTable.BrandModel.name || null}
                          </span>
                        ) : null}
                      </div>
                      <div style={styles.fieldWrapper}>
                        <span style={styles.label}>
                          品名：
                        </span>
                        {pnTable && pnTable.name ? (
                          <span style={styles.text}>
                            {pnTable.name || null}
                          </span>
                        ) : null}
                      </div>
                      <div style={styles.fieldWrapper}>
                        <span style={styles.label}>
                          價格：
                        </span>
                        {pnTable && pnTable.price ? (
                          <span style={styles.text}>
                            {pnTable.price}
                          </span>
                        ) : null}
                      </div>
                      <div style={styles.fieldWrapper}>
                        <span style={styles.label}>
                          廠商：
                        </span>
                        {pnTable && pnTable.vendor ? (
                          <span style={styles.text}>
                            {pnTable.vendor.name || null}
                          </span>
                        ) : null}
                      </div>
                      <div style={styles.fieldWrapper}>
                        <span style={styles.label}>
                          描述：
                        </span>
                        {pnTable && pnTable.description ? (
                          <span style={styles.text}>
                            {pnTable.description}
                          </span>
                        ) : null}
                      </div>
                    </Fragment>
                  )}
                </div>
              );
            }}
          </Query>
        </div>
      </div>
    );
  }
}

export default radium(InventoryDetailPopup);
