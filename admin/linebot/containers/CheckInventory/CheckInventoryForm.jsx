// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  initialize,
  Field,
  SubmissionError,
} from 'redux-form';
// config
import {
  FORM_LINEBOT_CHECK_INVENTORY_INPUT_FORM,
} from '../../../shared/form.js';
// import {
//   getInventoryIdFromItemId,
// } from '../../../helper/linebot.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import Input from '../../../components/Form/Input.jsx';
import SubmitButton from '../../../components/Form/SubmitButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  mainWrapper: {
    width: '100%',
    height: 'auto',
    flex: 1,
    padding: 24,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  fieldWrapper: {
    width: '100%',
    maxWidth: 414,
    height: 'auto',
    padding: '24px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  submitBtn: {
    width: '100%',
    maxWidth: 240,
    height: 'auto',
  },
};

type Props = {
  handleSubmit: Function,
  initializeForm: Function,
  history: {
    push: Function,
  },
  match: {
    params: {
      itemId: string,
    },
  },
};

class CheckInventoryForm extends PureComponent<Props> {
  componentDidMount() {
    const {
      match: {
        params: {
          itemId,
        },
      },
      initializeForm,
    } = this.props;

    initializeForm({
      itemId: itemId || '',
    });
  }

  submit(d) {
    const {
      history,
    } = this.props;

    const {
      itemId,
    } = d;

    if (!itemId) {
      throw new SubmissionError({
        itemId: 'error',
      });
    }

    history.push(`/linebot/checkInventory/detail/${itemId}`);
  }

  render() {
    const {
      handleSubmit,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <PageTitle title="點貨" />
        <form style={styles.mainWrapper} onSubmit={handleSubmit(d => this.submit(d))}>
          <div style={styles.fieldWrapper}>
            <Field
              name="itemId"
              label="商品序號："
              placeholder="請輸入商品序號"
              component={Input} />
          </div>
          <div style={styles.fieldWrapper}>
            <SubmitButton
              label="查詢點貨資訊"
              style={styles.submitBtn} />
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_LINEBOT_CHECK_INVENTORY_INPUT_FORM,
});

const reduxHook = connect(
  () => ({}),
  dispatch => bindActionCreators({
    initializeForm: v => initialize(FORM_LINEBOT_CHECK_INVENTORY_INPUT_FORM, v),
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      CheckInventoryForm
    )
  )
);
