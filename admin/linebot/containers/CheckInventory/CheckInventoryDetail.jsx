// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import {
  Query,
  Mutation,
} from 'react-apollo';

import { CHECK_INFO } from '../../../queries/CheckInventory.js';
import { CHECK_INVENTORY } from '../../../mutations/CheckInventory.js';
import Theme from '../../../styles/Theme.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import Error from '../../../components/Form/Error.jsx';
import Button from '../../../components/Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
  mainWrapper: {
    width: '100%',
    height: 'auto',
    flex: 1,
    padding: '12px 24px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formWrapper: {
    width: '100%',
    height: 'auto',
    maxWidth: 414,
  },
  image: {
    width: '100%',
    height: 200,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  label: {
    fontSize: 13,
    fontWeight: 500,
    letterSpacing: 1,
    color: Theme.BLACK_COLOR,
    minWidth: 65,
  },
  text: {
    fontSize: 13,
    letterSpacing: 1,
    color: Theme.BLACK_COLOR,
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonsWrapper: {
    width: '100%',
    height: 'auto',
    padding: '4px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 4px',
  },
};

type Props = {
  match: {
    params: {
      itemId: string,
    },
  },
  history: {
    push: Function,
  },
};

type State = {
  isChecked: boolean,
};

class CheckInventoryDetail extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      isChecked: false,
    };
  }

  render() {
    const {
      match: {
        params: {
          itemId,
        },
      },
      history,
    } = this.props;

    const {
      isChecked,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        <PageTitle title="點貨" />
        <div style={styles.mainWrapper}>
          <Query
            query={CHECK_INFO}
            variables={{
              checkCode: itemId || null,
            }}>
            {({
              data,
              loading,
            }) => {
              if (loading) return null;

              const wrapInfo = (data && data.checkInfo) || null;

              if (!wrapInfo) {
                return (
                  <div style={styles.formWrapper}>
                    <div style={styles.functionWrapper}>
                      <Error error="查無此商品序號，請確認是否輸入有誤" />
                    </div>
                  </div>
                );
              }

              const pnTableData = wrapInfo.pnTable || {};

              return (
                <div style={styles.formWrapper}>
                  <div
                    style={[
                      styles.image,
                      {
                        backgroundImage: pnTableData.picture ? `url(${pnTableData.picture})` : null,
                      },
                    ]} />
                  <div style={styles.fieldWrapper}>
                    <span style={styles.label}>
                      料號編號
                    </span>
                    ：
                    {pnTableData.code ? (
                      <span style={styles.text}>
                        {pnTableData.code}
                      </span>
                    ) : null}
                  </div>
                  <div style={styles.fieldWrapper}>
                    <span style={styles.label}>
                      料號類別
                    </span>
                    ：
                    {pnTableData.pnCategory ? (
                      <span style={styles.text}>
                        {pnTableData.pnCategory.name || null}
                      </span>
                    ) : null}
                  </div>
                  <div style={styles.fieldWrapper}>
                    <span style={styles.label}>
                      大類
                    </span>
                    ：
                    {pnTableData.bigSubCategory ? (
                      <span style={styles.text}>
                        {pnTableData.bigSubCategory.name || null}
                      </span>
                    ) : null}
                  </div>
                  <div style={styles.fieldWrapper}>
                    <span style={styles.label}>
                      類型
                    </span>
                    ：
                    {pnTableData.smallSubCategory ? (
                      <span style={styles.text}>
                        {pnTableData.smallSubCategory.name || null}
                      </span>
                    ) : null}
                  </div>
                  <div style={styles.fieldWrapper}>
                    <span style={styles.label}>
                      品名
                    </span>
                    ：
                    {pnTableData.name ? (
                      <span style={styles.text}>
                        {pnTableData.name}
                      </span>
                    ) : null}
                  </div>
                  <div style={styles.fieldWrapper}>
                    <span style={styles.label}>
                      數量
                    </span>
                    ：
                    {wrapInfo.amount ? (
                      <span style={styles.text}>
                        {wrapInfo.amount}
                      </span>
                    ) : null}
                  </div>
                  <Mutation mutation={CHECK_INVENTORY}>
                    {(checkInventory, { loading, error }) => (
                      <div style={styles.functionWrapper}>
                        {error || isChecked ? (
                          <>
                            <Error error={error ? error.message : '已完成點貨'} />
                            <div style={styles.buttonsWrapper}>
                              <div style={styles.btnWrapper}>
                                <Button
                                  label="返回"
                                  onClick={() => history.push('/linebot/management')} />
                              </div>
                            </div>
                          </>
                        ) : (
                          <div style={styles.buttonsWrapper}>
                            <div style={styles.btnWrapper}>
                              <Button
                                disabled={loading}
                                label="點貨確認"
                                onClick={async () => {
                                  const res = await checkInventory({
                                    variables: {
                                      checkCode: itemId,
                                    },
                                  });

                                  if (res && res.data
                                    && res.data.checkInventory
                                    && res.data.checkInventory.status
                                  ) {
                                    this.setState({
                                      isChecked: true,
                                    });
                                  }
                                }} />
                            </div>
                          </div>
                        )}
                      </div>
                    )}
                  </Mutation>
                </div>
              );
            }}
          </Query>
        </div>
      </div>
    );
  }
}

export default radium(CheckInventoryDetail);
