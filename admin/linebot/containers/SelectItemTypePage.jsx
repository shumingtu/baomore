// @flow
import React from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';

// config
import { FETCH_PNCATEGORIES } from '../../queries/PNTable.js';
import { pnCategoryTypes } from '../../shared/linebot.js';
// components
import PageTitle from '../components/Global/PageTitle.jsx';
import PageLink from '../components/Global/PageLink.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
  mainPlacement: {
    width: '100%',
    height: 'auto',
    minHeight: 'calc(100vh - 90px)',
    padding: 12,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainWrapper: {
    width: '100%',
    maxWidth: 320,
    height: 300,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

type Props = {
  title?: string,
  match: {
    url: string,
  },
};

function SelectItemTypePage({
  title,
  match: {
    url,
  },
}: Props) {
  return (
    <Query query={FETCH_PNCATEGORIES}>
      {({
        data,
      }) => {
        const pnCategories = (data && data.pnCategories) || [];
        const protectorId = pnCategories.find(c => c.name === '保貼')
          ? pnCategories.find(c => c.name === '保貼').id
          : null;
        const remainCategories = protectorId
          ? pnCategories.filter(c => c.id !== protectorId)
          : pnCategories;

        return (
          <div style={styles.wrapper}>
            <PageTitle title={title} />
            <div style={styles.mainPlacement}>
              <div style={styles.mainWrapper}>
                {protectorId ? (
                  <PageLink
                    name={pnCategoryTypes[0].name}
                    path={`${url}/${pnCategoryTypes[0].id}/${protectorId}`} />
                ) : null}
                {remainCategories.length ? (
                  <PageLink
                    name={pnCategoryTypes[1].name}
                    path={`${url}/${pnCategoryTypes[1].id}/${remainCategories[0].id}`} />
                ) : null}
              </div>
            </div>
          </div>
        );
      }}
    </Query>
  );
}

SelectItemTypePage.defaultProps = {
  title: null,
};

export default radium(SelectItemTypePage);
