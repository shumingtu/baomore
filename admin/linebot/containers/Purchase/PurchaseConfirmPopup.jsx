// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  graphql,
  Mutation,
} from 'react-apollo';
import {
  reduxForm,
  Fields,
  Field,
  initialize,
  SubmissionError,
} from 'redux-form';

// static, config
import Theme from '../../../styles/Theme.js';
import closeIcon from '../../../static/images/close-icon.png';
import { PURCHASE_CONFIRM } from '../../../mutations/Purchase.js';
import {
  FETCH_MY_INVENTORY_DETAIL,
  FETCH_INVENTORIES,
} from '../../../queries/Inventory.js';
import {
  DELETE_ORDER_BY_LINEBOT,
} from '../../../mutations/Order.js';
import { FORM_LINEBOT_PURCHASE_CONFIRM_FORM } from '../../../shared/form.js';
// components
import ChannelStoreSelectorBind from '../../components/Form/ChannelStoreSelectorBind.jsx';
import Input from '../../../components/Form/Input.jsx';
import SubmitButton from '../../../components/Form/SubmitButton.jsx';
import Error from '../../../components/Form/Error.jsx';
import Button from '../../../components/Global/Button.jsx';

const styles = {
  wrapper: {
    position: 'fixed',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    padding: 12,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 999,
  },
  mainWrapper: {
    width: '100%',
    maxWidth: 414,
    height: 'auto',
    padding: '12px 24px',
    backgroundColor: '#fff',
    borderRadius: 3,
    border: `1px solid ${Theme.BLACK_COLOR}`,
    position: 'relative',
  },
  titleWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 15,
    fontWeight: 500,
    letterSpacing: 1,
    color: Theme.BLACK_COLOR,
  },
  closeBtn: {
    position: 'absolute',
    top: 12,
    right: 12,
    width: 24,
    height: 24,
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundImage: `url(${closeIcon})`,
    cursor: 'pointer',
  },
  formWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '4px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  label: {
    width: 65,
    fontSize: 13,
    fontWeight: 500,
    color: Theme.BLACK_COLOR,
  },
  text: {
    flex: 1,
    fontSize: 13,
    color: Theme.BLACK_COLOR,
    lineHeight: 1,
    whiteSpace: 'pre-line',
    wordBreak: 'break-word',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnWrapper: {
    width: 'auto',
    height: 'auto',
    margin: '0 5px',
  },
};

type Props = {
  match: {
    params: {
      pnTableId: string,
    },
  },
  meInventory: {
    id: number,
  },
  queryError: ?{
    message: string,
  },
  handleSubmit: Function,
  onClose: Function,
  initializeForm: Function,
  error: string,
  queryOptions: object,
};

class PurchaseConfirmPopup extends PureComponent<Props> {
  componentDidMount() {
    const {
      meInventory,
    } = this.props;

    this.initializing(meInventory);
  }

  componentDidUpdate({
    meInventory: prevMeInventory,
  }) {
    const {
      meInventory,
    } = this.props;

    if (meInventory !== prevMeInventory && meInventory) {
      this.initializing(meInventory);
    }
  }

  getFields(d) {
    const {
      match: {
        params: {
          pnTableId,
        },
      },
    } = this.props;

    const {
      storeId,
      quantity,
      channelId,
    } = d;

    if (!pnTableId || pnTableId === 'undefined') {
      throw new SubmissionError({
        _error: '系統發生錯誤',
      });
    }

    if (!storeId || storeId === '-1') {
      throw new SubmissionError({
        storeId: 'required',
      });
    }

    if (!channelId || channelId === '-1') {
      throw new SubmissionError({
        channelId: 'required',
      });
    }

    if (!quantity || parseInt(quantity, 10) <= 0) {
      throw new SubmissionError({
        quantity: 'failed',
      });
    }

    return ({
      pnTableId: parseInt(pnTableId, 10),
      storeId: parseInt(storeId, 10),
      quantity: parseInt(quantity, 10),
    });
  }

  initializing(obj) {
    const {
      initializeForm,
    } = this.props;

    const orderingOrder = (obj && obj.orderingOrder) || null;
    const store = (orderingOrder && orderingOrder.store) || null;

    initializeForm({
      channelId: (store && store.channel && store.channel.id) || '-1',
      cityId: (store && store.district && store.district.city && store.district.city.id) || '-1',
      storeId: (store && store.id) || '-1',
      quantity: (orderingOrder && orderingOrder.quantity) || null,
    });
  }

  render() {
    const {
      onClose,
      handleSubmit,
      error,
      meInventory,
      queryError,
      queryOptions,
      match: {
        params: {
          pnTableId,
        },
      },
    } = this.props;

    const pnTable = (meInventory && meInventory.pnTable) || null;

    return (
      <div style={styles.wrapper}>
        <div style={styles.mainWrapper}>
          <button
            type="button"
            onClick={onClose || null}
            style={styles.closeBtn} />
          <div style={styles.titleWrapper}>
            <span style={styles.title}>
              訂貨確認
            </span>
          </div>
          <div style={styles.fieldWrapper}>
            <span style={styles.label}>
              料號編號：
            </span>
            {pnTable && pnTable.code ? (
              <span style={styles.text}>
                {pnTable.code}
              </span>
            ) : null}
          </div>
          <div style={styles.fieldWrapper}>
            <span style={styles.label}>
              料號類別：
            </span>
            {pnTable && pnTable.pnCategory ? (
              <span style={styles.text}>
                {pnTable.pnCategory.name || null}
              </span>
            ) : null}
          </div>
          <div style={styles.fieldWrapper}>
            <span style={styles.label}>
              品名：
            </span>
            {pnTable && pnTable.name ? (
              <span style={styles.text}>
                {pnTable.name}
              </span>
            ) : null}
          </div>
          {queryError ? (
            <Error error={queryError.message || null} />
          ) : (
            <Mutation
              update={(cache, { data: { purchase } }) => {
                const {
                  inventories,
                } = cache.readQuery({
                  query: FETCH_INVENTORIES,
                  variables: {
                    isForPurchase: true,
                    ...queryOptions,
                  },
                });

                const newInventories = inventories;

                const targetIdx = newInventories.findIndex(x => x.id === purchase.pnTableId);

                if (~targetIdx) {
                  newInventories[targetIdx] = {
                    ...newInventories[targetIdx],
                    orderingOrder: purchase.order,
                  };

                  cache.writeQuery({
                    query: FETCH_INVENTORIES,
                    variables: {
                      isForPurchase: true,
                      ...queryOptions,
                    },
                    data: {
                      inventories: newInventories,
                    },
                  });
                }
              }}
              mutation={PURCHASE_CONFIRM}>
              {(purchase, mutateProps) => (
                <form
                  style={styles.formWrapper}
                  onSubmit={handleSubmit(async (d) => {
                    const payload = this.getFields(d);

                    const {
                      data,
                    } = await purchase({
                      variables: payload,
                    });

                    if (data && data.purchase && data.purchase.pnTableId && data.purchase.message) {
                      if (onClose) {
                        window.alert(data.purchase.message);
                        onClose();
                      }
                    }
                  })}>
                  <Fields
                    names={[
                      'channelId',
                      'cityId',
                      'storeId',
                    ]}
                    isForPurchase
                    component={ChannelStoreSelectorBind} />
                  <div style={styles.fieldWrapper}>
                    <Field
                      required
                      type="number"
                      name="quantity"
                      label="進貨數量："
                      placeholder="數量"
                      component={Input} />
                  </div>
                  {mutateProps.error || error ? (
                    <Error
                      error={error || mutateProps.error.message || null} />
                  ) : null}
                  <div style={styles.functionWrapper}>
                    <div style={styles.btnWrapper}>
                      <SubmitButton
                        disabled={mutateProps.loading || false}
                        label="確認" />
                    </div>
                    {meInventory && meInventory.orderingOrder ? (
                      <Mutation
                        update={(cache) => {
                          const {
                            inventories,
                          } = cache.readQuery({
                            query: FETCH_INVENTORIES,
                            variables: {
                              isForPurchase: true,
                              ...queryOptions,
                            },
                          });

                          const newInventories = inventories;

                          const targetIdx = newInventories.findIndex(x => x.id === parseInt(pnTableId, 10));

                          if (~targetIdx) {
                            newInventories[targetIdx] = {
                              ...newInventories[targetIdx],
                              orderingOrder: null,
                            };

                            cache.writeQuery({
                              query: FETCH_INVENTORIES,
                              variables: {
                                isForPurchase: true,
                                ...queryOptions,
                              },
                              data: {
                                inventories: newInventories,
                              },
                            });
                          }
                        }}
                        mutation={DELETE_ORDER_BY_LINEBOT}>
                        {(deleteOrderByLineBot, deleteMutateProps) => (
                          <div style={styles.btnWrapper}>
                            <Button
                              disabled={deleteMutateProps.loading || false}
                              label="刪除訂單"
                              onClick={async () => {
                                const {
                                  data,
                                } = await deleteOrderByLineBot({
                                  variables: {
                                    orderId: parseInt(meInventory.orderingOrder.id, 10),
                                  },
                                });

                                if (data
                                  && data.deleteOrderByLineBot
                                  && data.deleteOrderByLineBot.status
                                ) {
                                  onClose();
                                }
                              }}
                              style={{ backgroundColor: Theme.ERROR_COLOR }} />
                          </div>
                        )}
                      </Mutation>
                    ) : null}
                  </div>
                </form>
              )}
            </Mutation>
          )}
        </div>
      </div>
    );
  }
}

const queryHook = graphql(FETCH_MY_INVENTORY_DETAIL, {
  options: ({
    match: {
      params: {
        pnTableId,
      },
    },
  }) => ({
    variables: {
      pnTableId: pnTableId ? parseInt(pnTableId, 10) : -1,
    },
    fetchPolicy: 'cache-and-network',
  }),
  props: ({
    data,
    error,
  }) => ({
    meInventory: (data && data.meInventory) || null,
    queryError: error || null,
  }),
});

const formHook = reduxForm({
  form: FORM_LINEBOT_PURCHASE_CONFIRM_FORM,
});

const reduxHook = connect(
  () => ({
  }),
  dispatch => bindActionCreators({
    initializeForm: v => initialize(FORM_LINEBOT_PURCHASE_CONFIRM_FORM, v),
  }, dispatch)
);

export default reduxHook(
  queryHook(
    formHook(
      radium(
        PurchaseConfirmPopup
      )
    )
  )
);
