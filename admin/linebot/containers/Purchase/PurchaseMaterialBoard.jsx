// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import {
  Switch,
  Route,
  withRouter,
} from 'react-router-dom';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';

import { CACHE_LINEBOT_PURCHASE_MATERIAL_SEARCH } from '../../../actions/Search.js';
import { FORM_LINEBOT_PURCHASE_MATERIAL_FORM } from '../../../shared/form.js';
import { FETCH_INVENTORIES } from '../../../queries/Inventory.js';
// components
import { wrapFormToMaterialSearchForm } from '../Form/MaterialSearchForm.jsx';
import Table, { TableField } from '../../../components/Table/Table.jsx';
import PurchaseManageActions from '../../../components/Table/Actions/linebot/PurchaseManageActions.jsx';
import InventoryDetailPopup from '../InventoryDetailPopup.jsx';
import PurchaseConfirmPopup from './PurchaseConfirmPopup.jsx';
import Button from '../../../components/Global/Button.jsx';

const MaterialSearchForm = wrapFormToMaterialSearchForm(FORM_LINEBOT_PURCHASE_MATERIAL_FORM);

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    position: 'relative',
  },
  searchWrapper: {
    width: '100%',
    height: 245,
    padding: 16,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  tableWrapper: {
    width: '100%',
    height: '100%',
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '6px 0',
  },
  collapseBtn: {
    position: 'absolute',
    top: -36,
    right: 10,
    zIndex: 1,
  },
};

type Props = {
  purchaseMaterialOptions: {
    pnCategoryId: string,
    bigSubCategoryId: string,
    smallSubCategoryId: string,
  },
  defaultPNCategoryId: string,
  match: {
    url: string,
  },
  history: {
    push: Function,
  },
};

const LIMIT_LENGTH = 10;

class PurchaseMaterialBoard extends PureComponent<Props> {
  state = {
    hideSearchForm: false,
  }

  render() {
    const {
      purchaseMaterialOptions,
      defaultPNCategoryId,
      match: {
        url,
      },
      history,
    } = this.props;

    const {
      hideSearchForm,
    } = this.state;

    const wrapPNCategoryId = (purchaseMaterialOptions && purchaseMaterialOptions.pnCategoryId)
      || defaultPNCategoryId
      || null;

    const queryOptions = {
      pnCategoryId: wrapPNCategoryId ? parseInt(wrapPNCategoryId, 10) : null,
      offset: 0,
      limit: LIMIT_LENGTH,
    };

    if (purchaseMaterialOptions) {
      const {
        bigSubCategoryId,
        smallSubCategoryId,
        keyword,
      } = purchaseMaterialOptions;

      if (bigSubCategoryId) queryOptions.bigSubCategoryId = parseInt(bigSubCategoryId, 10);
      if (smallSubCategoryId) queryOptions.smallSubCategoryId = parseInt(smallSubCategoryId, 10);
      if (keyword) queryOptions.keyword = keyword;
    }

    const wrapCacheOptions = purchaseMaterialOptions || {};

    return (
      <div style={styles.wrapper}>
        <div style={styles.collapseBtn}>
          <Button
            onClick={() => this.setState({ hideSearchForm: !hideSearchForm })}
            label={hideSearchForm ? '展開' : '收合'} />
        </div>
        <MaterialSearchForm
          hide={hideSearchForm}
          actionType={CACHE_LINEBOT_PURCHASE_MATERIAL_SEARCH}
          initialValue={{
            pnCategoryId: wrapPNCategoryId,
            ...wrapCacheOptions,
          }} />
        <Query
          query={FETCH_INVENTORIES}
          variables={{
            ...queryOptions,
            isForPurchase: true,
          }}>
          {({
            data,
            fetchMore,
          }) => {
            const inventories = (data && data.inventories) || [];

            return (
              <div style={styles.tableWrapper}>
                <Table
                  dataSource={inventories.map(i => ({
                    id: i.id,
                    name: (i.pnTable && i.pnTable.name) || null,
                    orderingCount: (i.orderingOrder && i.orderingOrder.quantity) || 0,
                    amount: i.amount || 0,
                  }))}
                  fetchMore={() => fetchMore({
                    variables: {
                      ...queryOptions,
                      offset: 0,
                      limit: inventories.length + LIMIT_LENGTH,
                    },
                    updateQuery: (prev, { fetchMoreResult }) => {
                      if (!fetchMoreResult) return prev;

                      return {
                        ...prev,
                        inventories: [
                          ...fetchMoreResult.inventories,
                        ],
                      };
                    },
                  })}
                  getActions={() => [
                    <PurchaseManageActions />,
                  ]}
                  actionTitles={['操作']}
                  showPlaceholder={!inventories.length}
                  placeholder="查無存貨">
                  <TableField
                    name="品名"
                    fieldKey="name"
                    flex={2}
                    isCenter />
                  <TableField
                    name="庫存"
                    fieldKey="amount"
                    flex={1}
                    isCenter />
                  <TableField
                    name="訂貨中數量"
                    fieldKey="orderingCount"
                    flex={1.2}
                    isCenter />
                </Table>
              </div>
            );
          }}
        </Query>
        <Switch>
          <Route
            path={`${url}/detail/:pnTableId`}
            component={props => (
              <InventoryDetailPopup
                {...props}
                onClose={() => history.push(url)} />
            )} />
          <Route
            path={`${url}/action/:pnTableId`}
            component={props => (
              <PurchaseConfirmPopup
                queryOptions={queryOptions}
                {...props}
                onClose={() => history.push(url)} />
            )} />
        </Switch>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    purchaseMaterialOptions: state.Search.purchaseMaterial,
  }),
);

export default withRouter(
  reduxHook(
    radium(
      PurchaseMaterialBoard
    )
  )
);
