// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

import { pnCategoryTypeIsProtector } from '../../../helper/linebot.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import PurchaseProtectorBoard from './PurchaseProtectorBoard.jsx';
import PurchaseMaterialBoard from './PurchaseMaterialBoard.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
  mainWrapper: {
    width: '100%',
    height: 'calc(100vh - 90px)',
  },
};

type Props = {
  match: {
    params: {
      pnCategoryType: string,
    },
  },
};

class PurchaseMainBoard extends PureComponent<Props> {
  categoryDecider() {
    const {
      match: {
        params: {
          pnCategoryType,
          pnCategoryId,
        },
      },
    } = this.props;

    const isProtector = pnCategoryTypeIsProtector(pnCategoryType);

    if (isProtector) {
      return (
        <PurchaseProtectorBoard pnCategoryId={pnCategoryId} />
      );
    }

    return (
      <PurchaseMaterialBoard defaultPNCategoryId={pnCategoryId} />
    );
  }

  render() {
    return (
      <div style={styles.wrapper}>
        <PageTitle title="訂貨" />
        <div style={styles.mainWrapper}>
          {this.categoryDecider()}
        </div>
      </div>
    );
  }
}

export default radium(PurchaseMainBoard);
