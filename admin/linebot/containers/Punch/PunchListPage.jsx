// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';

import { CACHE_LINEBOT_PUNCH_LIST_SEARCH } from '../../../actions/Search.js';
import { FORM_LINEBOT_INVENTORY_MATERIAL_FORM } from '../../../shared/form.js';
import { FETCH_PUNCH_RECORDS } from '../../../queries/Punch.js';
// components
import { wrapFormToPunchSearchForm } from '../Form/PunchSearchForm.jsx';
import PageTitle from '../../components/Global/PageTitle.jsx';
import Table, { TableField } from '../../../components/Table/Table.jsx';

const PunchSearchForm = wrapFormToPunchSearchForm(FORM_LINEBOT_INVENTORY_MATERIAL_FORM);

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    position: 'relative',
  },
  searchWrapper: {
    width: '100%',
    height: 245,
    padding: 16,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  tableWrapper: {
    width: '100%',
    height: 'auto',
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '6px 0',
  },
};

type Props = {
  punchListSearchOptions: {
    startDate: string,
    endDate: string,
    storeId: string,
    employeeId: string,
  },
  history: {
    pop: Function,
  },
};

const LIMIT_LENGTH = 10;

class PunchListPage extends PureComponent<Props> {
  render() {
    const {
      punchListSearchOptions,
    } = this.props;

    const queryOptions = {
      offset: 0,
      limit: LIMIT_LENGTH,
    };

    if (punchListSearchOptions) {
      const {
        startDate,
        endDate,
        storeId,
        employeeId,
      } = punchListSearchOptions;

      if (startDate) queryOptions.startDate = startDate;
      if (endDate) queryOptions.endDate = endDate;
      if (storeId) queryOptions.storeId = parseInt(storeId, 10);
      if (employeeId) queryOptions.employeeId = parseInt(employeeId, 10);
    }

    const wrapCacheOptions = punchListSearchOptions || {};

    return (
      <div style={styles.wrapper}>
        <PageTitle title="包膜師打卡紀錄" />
        <PunchSearchForm
          returnLabel="返回打卡頁面"
          actionType={CACHE_LINEBOT_PUNCH_LIST_SEARCH}
          initialValue={{
            ...wrapCacheOptions,
          }} />
        <Query
          query={FETCH_PUNCH_RECORDS}
          fetchPolicy="cache-and-network"
          variables={queryOptions}>
          {({
            data,
            fetchMore,
          }) => {
            const punchlist = (data && data.punchlist) || [];

            return (
              <div style={styles.tableWrapper}>
                <Table
                  dataSource={punchlist.map(i => ({
                    id: i.id,
                    punchTime: i.punchTime,
                    storeName: (i.store && i.store.name) || null,
                  }))}
                  fetchMore={() => fetchMore({
                    variables: {
                      ...queryOptions,
                      offset: 0,
                      limit: punchlist.length + LIMIT_LENGTH,
                    },
                    updateQuery: (prev, { fetchMoreResult }) => {
                      if (!fetchMoreResult) return prev;

                      return {
                        ...prev,
                        punchlist: [
                          ...fetchMoreResult.punchlist,
                        ],
                      };
                    },
                  })}
                  showPlaceholder={!punchlist.length}
                  placeholder="查無紀錄">
                  <TableField
                    name="打卡時間"
                    fieldKey="punchTime"
                    flex={2}
                    isCenter />
                  <TableField
                    name="門市"
                    fieldKey="storeName"
                    flex={1}
                    isCenter />
                </Table>
              </div>
            );
          }}
        </Query>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    punchListSearchOptions: state.Search.punchList,
  }),
);

export default reduxHook(
  radium(
    PunchListPage
  )
);
