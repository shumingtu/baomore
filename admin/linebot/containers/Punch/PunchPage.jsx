// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import {
  reduxForm,
  Fields,
  SubmissionError,
} from 'redux-form';
import { Mutation } from 'react-apollo';
import moment from 'moment';

import { PUNCH } from '../../../mutations/Punch.js';
import { FORM_LINEBOT_PUNCH_FORM } from '../../../shared/form.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import ChannelStoreSelectorBind from '../../components/Form/ChannelStoreSelectorBind.jsx';
import SubmitButton from '../../../components/Form/SubmitButton.jsx';
import Error from '../../../components/Form/Error.jsx';
import Button from '../../../components/Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  mainBoard: {
    flex: 1,
    width: '100%',
    height: 'auto',
    padding: 24,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formWrapper: {
    width: '100%',
    maxWidth: 414,
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '4px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '24px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnWrapper: {
    width: 'auto',
    height: 'auto',
    padding: 4,
  },
};

type Props = {
  handleSubmit: Function,
  history: {
    push: Function,
  },
  match: {
    url: string,
  },
};

type State = {
  latitude: number,
  longitude: number,
  error: string,
};

class PunchPage extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      latitude: null,
      longitude: null,
      error: null,
    };

    this.isMount = false;
  }

  componentDidMount() {
    this.isMount = true;
    if (navigator && navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        pos => this.positionUpdate(pos), // success callback
        err => this.locationErrorHandle(err), // error callback
      );
    }
  }

  componentWillUnmount() {
    this.isMount = false;
  }

  getFields(d) {
    const {
      cityId,
      storeId,
    } = d;

    const {
      latitude,
      longitude,
    } = this.state;

    if (!cityId || !storeId || cityId === '-1' || storeId === '-1') {
      throw new SubmissionError({
        cityId: 'required',
        storeId: 'required',
      });
    }

    if (!latitude || !longitude) {
      return this.setState({
        error: '尚未取得位置資訊',
      });
    }

    return ({
      storeId: parseInt(storeId, 10),
      latitude,
      longitude,
      punchTime: moment().format('YYYY-MM-DD HH:mm:ss'),
    });
  }

  positionUpdate(position) {
    if (position && position.coords) {
      if (this.isMount) {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null,
        });
      }
    }
  }

  locationErrorHandle(error) {
    if (this.isMount && error) {
      let errorMsg = '';
      switch (error.code) {
        case 1: // User denied
          errorMsg = '取得GPS位置失敗，請確認已同意GPS存取設定';
          break;
        default:
          errorMsg = `GPS Error: ${error.message}`;
          break;
      }

      this.setState({
        error: errorMsg,
      });
    }
  }

  render() {
    const {
      handleSubmit,
      history,
      match: {
        url,
      },
    } = this.props;

    const {
      error,
      latitude,
      longitude,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        <PageTitle title="包膜師打卡" />
        <div style={styles.mainBoard}>
          {!error && !latitude && !longitude ? (
            <span>位置資訊讀取中</span>
          ) : (
            <Mutation mutation={PUNCH}>
              {(punch, mutateProps) => (
                <form
                  style={styles.formWrapper}
                  onSubmit={handleSubmit(async (d) => {
                    const payload = this.getFields(d);

                    const {
                      data,
                    } = await punch({
                      variables: payload,
                    });

                    if (data && data.punch && data.punch.status) {
                      this.setState({
                        error: '打卡成功',
                      });
                    }
                  })}>
                  <div style={styles.fieldWrapper}>
                    <Fields
                      lat={latitude}
                      lon={longitude}
                      names={[
                        'cityId',
                        'storeId',
                      ]}
                      component={ChannelStoreSelectorBind} />
                  </div>
                  {error || mutateProps.error ? (
                    <Error
                      error={(mutateProps.error && mutateProps.error.message) || error || null} />
                  ) : null}
                  <div style={styles.functionWrapper}>
                    <div style={styles.btnWrapper}>
                      <SubmitButton
                        disabled={mutateProps.loading || false}
                        label="打卡確認" />
                    </div>
                    <div style={styles.btnWrapper}>
                      <Button
                        isWhiteBg
                        label="查看紀錄"
                        onClick={() => history.push(`${url}/list`)} />
                    </div>
                  </div>
                </form>
              )}
            </Mutation>
          )}
        </div>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_LINEBOT_PUNCH_FORM,
});

export default formHook(
  radium(
    PunchPage
  )
);
