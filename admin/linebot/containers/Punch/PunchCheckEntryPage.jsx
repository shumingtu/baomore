// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import {
  reduxForm,
  Fields,
  SubmissionError,
} from 'redux-form';

import { FORM_LINEBOT_MANAGER_PUNCH_STORE_FORM } from '../../../shared/form.js';
import { isPermissionAllowed } from '../../../helper/roles.js';
import { LINEBOT_PUNCH_CHECK_PAGE_KEYS } from '../../../../shared/selectorActionKeys.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import ChannelStoreSelectorBind from '../../components/Form/ChannelStoreSelectorBind.jsx';
import SubmitButton from '../../../components/Form/SubmitButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  mainBoard: {
    flex: 1,
    width: '100%',
    height: 'auto',
    padding: 24,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  fieldWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '4px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '24px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnWrapper: {
    width: 'auto',
    height: 'auto',
    padding: 4,
  },
};

type Props = {
  handleSubmit: Function,
  history: {
    push: Function,
  },
  match: {
    url: string,
  },
};

class PunchCheckEntryPage extends PureComponent<Props> {
  constructor(props) {
    super(props);

    this.state = {
      latitude: null,
      longitude: null,
      error: null,
    };

    this.isMount = false;
  }

  componentDidMount() {
    this.isMount = true;
    if (navigator && navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        pos => this.positionUpdate(pos), // success callback
        err => this.locationErrorHandle(err), // error callback
      );
    }
  }

  componentWillUnmount() {
    this.isMount = false;
  }

  getFields(d) {
    const {
      cityId,
      storeId,
    } = d;

    if (!cityId || !storeId || cityId === '-1' || storeId === '-1') {
      throw new SubmissionError({
        cityId: 'required',
        storeId: 'required',
      });
    }
    return ({
      cityId: parseInt(cityId, 10),
      storeId: parseInt(storeId, 10),
    });
  }

  positionUpdate(position) {
    if (position && position.coords) {
      if (this.isMount) {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null,
        });
      }
    }
  }

  locationErrorHandle(error) {
    if (this.isMount && error) {
      let errorMsg = '';
      switch (error.code) {
        case 1: // User denied
          errorMsg = '取得GPS位置失敗，請確認已同意GPS存取設定';
          break;
        default:
          errorMsg = `GPS Error: ${error.message}`;
          break;
      }

      this.setState({
        error: errorMsg,
      });
    }
  }

  render() {
    const {
      handleSubmit,
      history,
      match: {
        url,
      },
    } = this.props;

    const {
      error,
      latitude,
      longitude,
    } = this.state;


    if (!isPermissionAllowed(LINEBOT_PUNCH_CHECK_PAGE_KEYS)) {
      history.replace('/linebot/login');
      return null;
    }

    return (
      <div style={styles.wrapper}>
        <PageTitle title="巡檢打卡" />
        <form
          style={styles.mainBoard}
          onSubmit={handleSubmit((d) => {
            const fields = this.getFields(d);

            history.push(`${url}/store/${fields.storeId}`);
          })}>
          {!error && !latitude && !longitude ? (
            <span>位置資訊讀取中</span>
          ) : (
            <>
              <div style={styles.fieldWrapper}>
                <Fields
                  lat={latitude}
                  lon={longitude}
                  names={[
                    'cityId',
                    'storeId',
                  ]}
                  component={ChannelStoreSelectorBind} />
              </div>
              <div style={styles.functionWrapper}>
                <div style={styles.btnWrapper}>
                  <SubmitButton label="開始巡檢" />
                </div>
              </div>
            </>
          )}
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_LINEBOT_MANAGER_PUNCH_STORE_FORM,
});

export default formHook(
  radium(
    PunchCheckEntryPage
  )
);
