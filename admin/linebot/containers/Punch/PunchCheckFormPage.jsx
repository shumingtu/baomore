// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import {
  reduxForm,
  Fields,
  Field,
  SubmissionError,
} from 'redux-form';
import moment from 'moment';
import { Mutation } from 'react-apollo';

import { MANAGER_PUNCH } from '../../../mutations/Punch.js';
import { FORM_LINEBOT_MANAGER_PUNCH_CHECK_FORM } from '../../../shared/form.js';
import { validatePunchCheckForm } from '../../../helper/validator/PunchCheckForm.js';
import { isPermissionAllowed } from '../../../helper/roles.js';
import { LINEBOT_PUNCH_CHECK_PAGE_KEYS } from '../../../../shared/selectorActionKeys.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import EmployeeSelectorBind from '../../components/Form/EmployeeSelectorBind.jsx';
import DatetimeRangeSelectorBind from '../../components/Form/DatetimeRangeSelectorBind.jsx';
import Input from '../../../components/Form/Input.jsx';
import SubmitButton from '../../../components/Form/SubmitButton.jsx';
import Error from '../../../components/Form/Error.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  mainBoard: {
    flex: 1,
    width: '100%',
    height: 'auto',
    padding: 24,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formWrapper: {
    width: '100%',
    maxWidth: 414,
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '4px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '24px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnWrapper: {
    width: 'auto',
    height: 'auto',
    padding: 4,
  },
};

type Props = {
  handleSubmit: Function,
  history: {
    push: Function,
  },
  match: {
    params: {
      storeId: string,
    },
  },
};

type State = {
  message: string,
};

class PunchCheckFormPage extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      message: null,
    };
  }

  getFields(d) {
    const {
      employeeId,
      startDate,
      endDate,
      grade,
      remark,
    } = d;

    const {
      match: {
        params: {
          storeId,
        },
      },
    } = this.props;

    if (!storeId || storeId === 'undefined') {
      this.setState({
        message: '查無門市，請確認是否已選取門市',
      });
      return null;
    }

    const errors = validatePunchCheckForm(d);

    if (errors) {
      throw new SubmissionError(errors);
    }

    return ({
      punchStartTime: `${moment().format('YYYY-MM-DD')} ${moment(startDate).format('HH:mm:ss')}`,
      punchEndTime: `${moment().format('YYYY-MM-DD')} ${moment(endDate).format('HH:mm:ss')}`,
      grade: parseFloat(grade),
      employeeId: parseInt(employeeId, 10),
      storeId: parseInt(storeId, 10),
      remark: remark || null,
    });
  }

  render() {
    const {
      handleSubmit,
      history,
    } = this.props;

    const {
      message,
    } = this.state;

    if (!isPermissionAllowed(LINEBOT_PUNCH_CHECK_PAGE_KEYS)) {
      history.replace('/linebot/login');
      return null;
    }

    return (
      <div style={styles.wrapper}>
        <PageTitle title="巡檢打卡" />
        <div style={styles.mainBoard}>
          <Mutation mutation={MANAGER_PUNCH}>
            {(managerPunch, mutateProps) => (
              <form
                style={styles.formWrapper}
                onSubmit={handleSubmit(async (d) => {
                  const payload = this.getFields(d);

                  const {
                    data,
                  } = await managerPunch({
                    variables: payload,
                  });

                  if (data && data.managerPunch && data.managerPunch.status) {
                    alert('巡檢成功！');
                    history.replace('/linebot/punch/check');
                  }
                })}>
                <div style={styles.fieldWrapper}>
                  <EmployeeSelectorBind />
                </div>
                <div style={styles.fieldWrapper}>
                  <Fields
                    disableDate
                    names={[
                      'startDate',
                      'endDate',
                    ]}
                    startDateLabel="到店時間："
                    endDateLabel="離店時間："
                    component={DatetimeRangeSelectorBind} />
                </div>
                <div style={styles.fieldWrapper}>
                  <Field
                    type="number"
                    name="grade"
                    label="檢核分數："
                    placeholder="請輸入數字"
                    component={Input} />
                </div>
                <div style={styles.fieldWrapper}>
                  <Field
                    type="textarea"
                    name="remark"
                    label="備註："
                    placeholder="請輸入備註"
                    component={Input} />
                </div>
                {message || mutateProps.error ? (
                  <Error
                    error={message || (mutateProps.error && mutateProps.error.message) || null} />
                ) : null}
                <div style={styles.functionWrapper}>
                  <div style={styles.btnWrapper}>
                    <SubmitButton
                      disabled={mutateProps.loading || false}
                      label="巡檢完成" />
                  </div>
                </div>
              </form>
            )}
          </Mutation>
        </div>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_LINEBOT_MANAGER_PUNCH_CHECK_FORM,
});

export default formHook(
  radium(
    PunchCheckFormPage
  )
);
