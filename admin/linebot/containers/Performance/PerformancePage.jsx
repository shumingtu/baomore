// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';

import { CACHE_LINEBOT_PERFORMANCE_SEARCH } from '../../../actions/Search.js';
import { FORM_LINEBOT_PERFORMANCE_FORM } from '../../../shared/form.js';
import { FETCH_PERFORMANCE } from '../../../queries/Performance.js';
import { performanceTableDataFormatted } from '../../../helper/linebot.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import { wrapFormToPerformanceForm } from '../Form/PerformanceSearchForm.jsx';
import Table, { TableField } from '../../../components/Table/Table.jsx';
import YellowField from '../../../components/Table/Custom/YellowField.jsx';
import PerformanceLabel from '../../../components/Table/Custom/PerformanceLabel.jsx';
import Button from '../../../components/Global/Button.jsx';

const PerformanceSearchForm = wrapFormToPerformanceForm(FORM_LINEBOT_PERFORMANCE_FORM);

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    position: 'relative',
  },
  searchWrapper: {
    width: '100%',
    height: 245,
    padding: 16,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  tableWrapper: {
    width: '100%',
    height: 'auto',
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    overflowX: 'auto',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '6px 0',
  },
  collapseBtn: {
    position: 'absolute',
    top: 4,
    right: 10,
  },
};

type Props = {
  performanceOptions: {
    startDate: string,
    endDate: string,
    employeeId: string,
  },
};

class PerformancePage extends PureComponent<Props> {
  state = {
    hideSearchForm: false,
  }

  render() {
    const {
      performanceOptions,
    } = this.props;

    const {
      hideSearchForm,
    } = this.state;

    const queryOptions = {};

    if (performanceOptions) {
      const {
        startDate,
        endDate,
        employeeId,
      } = performanceOptions;

      if (startDate) queryOptions.startDate = startDate;
      if (endDate) queryOptions.endDate = endDate;
      if (employeeId) queryOptions.employeeId = parseInt(employeeId, 10);
    }

    const wrapCacheOptions = performanceOptions || {};

    return (
      <div style={styles.wrapper}>
        <div style={styles.collapseBtn}>
          <Button
            onClick={() => this.setState({ hideSearchForm: !hideSearchForm })}
            label={hideSearchForm ? '展開' : '收合'} />
        </div>
        <PageTitle title="查看業績" />
        <PerformanceSearchForm
          hide={hideSearchForm}
          actionType={CACHE_LINEBOT_PERFORMANCE_SEARCH}
          initialValue={{
            ...wrapCacheOptions,
          }} />
        <Query
          query={FETCH_PERFORMANCE}
          variables={queryOptions}>
          {({
            data,
          }) => {
            const performance = (data && data.performance) || null;
            const performanceList = performanceTableDataFormatted(performance);

            return (
              <div style={styles.tableWrapper}>
                <Table
                  dataSource={performanceList}
                  showPlaceholder={!performanceList.length}
                  placeholder="查無業績資料">
                  <TableField
                    name="欄位"
                    fieldKey="fieldName"
                    flex={1}
                    Component={YellowField} />
                  <TableField
                    name="包膜"
                    fieldKey="baomore"
                    flex={1}
                    Component={PerformanceLabel} />
                  <TableField
                    name="保貼"
                    fieldKey="protector"
                    flex={1}
                    Component={PerformanceLabel} />
                  <TableField
                    name="小計"
                    fieldKey="total"
                    flex={1}
                    Component={PerformanceLabel} />
                </Table>
              </div>
            );
          }}
        </Query>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    performanceOptions: state.Search.performance,
  }),
);

export default reduxHook(
  radium(
    PerformancePage
  )
);
