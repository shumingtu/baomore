// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import {
  Switch,
  Route,
  withRouter,
} from 'react-router-dom';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';

import { CACHE_LINEBOT_RETURN_MATERIAL_SEARCH } from '../../../actions/Search.js';
import { FORM_LINEBOT_RETURN_MATERIAL_FORM } from '../../../shared/form.js';
import { FETCH_INVENTORIES } from '../../../queries/Inventory.js';
// components
import { wrapFormToMaterialSearchForm } from '../Form/MaterialSearchForm.jsx';
import Table, { TableField } from '../../../components/Table/Table.jsx';
import ReturnManageActions from '../../../components/Table/Actions/linebot/ReturnManageActions.jsx';
import InventoryDetailPopup from '../InventoryDetailPopup.jsx';

const MaterialSearchForm = wrapFormToMaterialSearchForm(
  FORM_LINEBOT_RETURN_MATERIAL_FORM
);

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    position: 'relative',
  },
  searchWrapper: {
    width: '100%',
    height: 245,
    padding: 16,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  tableWrapper: {
    width: '100%',
    height: 'auto',
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '6px 0',
  },
};

type Props = {
  returnMaterialOptions: {
    pnCategoryId: string,
    bigSubCategoryId: string,
    smallSubCategoryId: string,
  },
  defaultPNCategoryId: string,
  match: {
    url: string,
  },
  history: {
    push: Function,
  },
};

const LIMIT_LENGTH = 10;

class ReturnMaterialBoard extends PureComponent<Props> {
  render() {
    const {
      returnMaterialOptions,
      defaultPNCategoryId,
      match: {
        url,
      },
      history,
    } = this.props;

    const wrapPNCategoryId = (returnMaterialOptions && returnMaterialOptions.pnCategoryId)
      || defaultPNCategoryId
      || null;

    const queryOptions = {
      pnCategoryId: wrapPNCategoryId ? parseInt(wrapPNCategoryId, 10) : null,
      offset: 0,
      limit: LIMIT_LENGTH,
    };

    if (returnMaterialOptions) {
      const {
        bigSubCategoryId,
        smallSubCategoryId,
        keyword,
      } = returnMaterialOptions;

      if (bigSubCategoryId) queryOptions.bigSubCategoryId = parseInt(bigSubCategoryId, 10);
      if (smallSubCategoryId) queryOptions.smallSubCategoryId = parseInt(smallSubCategoryId, 10);
      if (keyword) queryOptions.keyword = keyword;
    }

    const wrapCacheOptions = returnMaterialOptions || {};

    return (
      <div style={styles.wrapper}>
        <MaterialSearchForm
          actionType={CACHE_LINEBOT_RETURN_MATERIAL_SEARCH}
          initialValue={{
            pnCategoryId: wrapPNCategoryId,
            ...wrapCacheOptions,
          }} />
        <Query
          query={FETCH_INVENTORIES}
          variables={queryOptions}>
          {({
            data,
            fetchMore,
          }) => {
            const inventories = (data && data.inventories) || [];

            return (
              <div style={styles.tableWrapper}>
                <Table
                  dataSource={inventories.map(i => ({
                    id: i.id,
                    name: (i.pnTable && i.pnTable.name) || null,
                    amount: i.amount || 0,
                  }))}
                  fetchMore={() => fetchMore({
                    variables: {
                      ...queryOptions,
                      offset: 0,
                      limit: inventories.length + LIMIT_LENGTH,
                    },
                    updateQuery: (prev, { fetchMoreResult }) => {
                      if (!fetchMoreResult) return prev;

                      return {
                        ...prev,
                        inventories: [
                          ...fetchMoreResult.inventories,
                        ],
                      };
                    },
                  })}
                  getActions={() => [
                    <ReturnManageActions />,
                  ]}
                  actionTitles={['操作']}
                  showPlaceholder={!inventories.length}
                  placeholder="查無存貨">
                  <TableField
                    name="品名"
                    fieldKey="name"
                    flex={2}
                    isCenter />
                  <TableField
                    name="庫存數量"
                    fieldKey="amount"
                    flex={1}
                    isCenter />
                </Table>
              </div>
            );
          }}
        </Query>
        <Switch>
          <Route
            path={`${url}/detail/:pnTableId`}
            component={props => (
              <InventoryDetailPopup
                {...props}
                onClose={() => history.push(url)} />
            )} />
        </Switch>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    returnMaterialOptions: state.Search.returnMaterial,
  }),
);

export default withRouter(
  reduxHook(
    radium(
      ReturnMaterialBoard
    )
  )
);
