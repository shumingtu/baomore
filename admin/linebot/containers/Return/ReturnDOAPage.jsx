// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  Fields,
  SubmissionError,
  initialize,
} from 'redux-form';
import {
  Mutation,
} from 'react-apollo';

import Theme from '../../../styles/Theme.js';
import { validateDOAForm } from '../../../helper/validator/ReturnForm.js';
import { INVENTORY_ROLLBACK } from '../../../mutations/Rollback.js';
import { FORM_LINEBOT_RETURN_DOA_FORM } from '../../../shared/form.js';
import { inventoryRollbackTypeSpec } from '../../../shared/linebot.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import ReturnPNInfo from './ReturnPNInfo.jsx';
import Error from '../../../components/Form/Error.jsx';
import Input from '../../../components/Form/Input.jsx';
import FileInput from '../../../components/Form/FileInput.jsx';
import ErrorTypeSelectorBind from '../../components/Form/ErrorTypeSelectorBind.jsx';
import SubmitButton from '../../../components/Form/SubmitButton.jsx';
import Selector from '../../../components/Form/Selector.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
  mainWrapper: {
    width: '100%',
    height: 'calc(100vh - 90px)',
    overflow: 'auto',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  formWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0 24px 0',
  },
  formTitleWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottom: `2px solid ${Theme.SECONDARY_THEME_COLOR}`,
  },
  formTitle: {
    fontSize: 15,
    fontWeight: 500,
    letterSpacing: 1,
    color: Theme.BLACK_COLOR,
  },
  mainFormWrapper: {
    width: '100%',
    height: 'auto',
    padding: 12,
  },
  fileWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '6px 0',
  },
  fileInput: {
    width: '100%',
    height: 130,
    maxWidth: 260,
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

type Props = {
  pnTableId?: string,
  inventoryInfo?: {
    id: number,
    amount: number,
    pnTable: {
      id: number,
      code: string,
      name: string,
      picture: string,
      pnCategory: {
        id: number,
        name: string,
      },
      bigSubCategory: {
        id: number,
        name: string,
      },
      smallSubCategory: {
        id: number,
        name: string,
      },
    },
  },
  error?: {
    message: string,
  },
  itemId?: string,
  handleSubmit: Function,
  initializeForm: Function,
  productType: string,
};

type State = {
  isComplete: boolean,
};

class ReturnDOAPage extends PureComponent<Props, State> {
  static defaultProps = {
    inventoryInfo: null,
    error: null,
    itemId: null,
    pnTableId: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isComplete: false,
    };
  }

  componentDidMount() {
    const {
      itemId,
      initializeForm,
    } = this.props;

    initializeForm({
      qrcode: itemId || null,
      picture: '',
      errorType: null,
      errorDesc: null,
      remark: null,
    });
  }

  getFields(d) {
    const {
      pnTableId,
    } = this.props;

    const {
      picture,
      errorType,
      errorDesc,
      qrcode,
      remark,
      productType,
    } = d;

    const errors = validateDOAForm(d);

    if (errors) {
      throw new SubmissionError(errors);
    }

    const errorStr = errorType ? `異常類型：${errorType}` : null;
    const errorDescStr = errorDesc ? `異常說明：${errorDesc}` : null;
    const remarkStr = remark ? `備註：${remark}` : null;
    const remarkFinalStr = [errorStr, errorDescStr, remarkStr]
      .filter(s => s)
      .join('***');

    const payload = {
      qrcode,
      picture: picture || null,
      storeId: null,
      remark: remarkFinalStr || null,
      rollbackType: inventoryRollbackTypeSpec.DOA,
      productType,
    };

    if (pnTableId) {
      payload.pnTableId = parseInt(pnTableId, 10);
    }

    return payload;
  }

  render() {
    const {
      handleSubmit,
      error,
      inventoryInfo,
    } = this.props;

    const {
      isComplete,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        <PageTitle title="未銷貨退回" />
        <div style={styles.mainWrapper}>
          {inventoryInfo ? (
            <ReturnPNInfo data={inventoryInfo} />
          ) : null}
          {error ? (
            <Error error={error.message || null} />
          ) : (
            <Mutation mutation={INVENTORY_ROLLBACK}>
              {(inventoryRollback, mutateProps) => (
                <form
                  style={styles.formWrapper}
                  onSubmit={handleSubmit(async (d) => {
                    const payload = this.getFields(d);

                    const res = await inventoryRollback({
                      variables: payload,
                    });

                    if (res
                      && res.data
                      && res.data.inventoryRollback
                      && res.data.inventoryRollback.status
                    ) {
                      this.setState({
                        isComplete: true,
                      });
                    }
                  })}>
                  <div style={styles.formTitleWrapper}>
                    <span style={styles.formTitle}>
                      填寫退回資訊
                    </span>
                  </div>
                  <div style={styles.mainFormWrapper}>
                    <div style={styles.fileWrapper}>
                      <Field
                        name="picture"
                        annotation="上傳異常圖片"
                        component={FileInput}
                        style={styles.fileInput} />
                    </div>
                    <div style={styles.fieldWrapper}>
                      <Field
                        nullable
                        name="productType"
                        label="產品類型："
                        options={[{
                          id: 'PROTECTOR',
                          name: '保貼',
                        }, {
                          id: 'MATERIAL',
                          name: '膜料',
                        }]}
                        placeholder="請輸入產品類型"
                        component={Selector} />
                    </div>
                    <Fields
                      type="employee"
                      names={[
                        'errorType',
                        'errorDesc',
                      ]}
                      component={ErrorTypeSelectorBind} />
                    <div style={styles.fieldWrapper}>
                      <Field
                        required
                        name="qrcode"
                        label="商品序號："
                        placeholder="請輸入商品序號"
                        component={Input} />
                    </div>
                    <div style={styles.fieldWrapper}>
                      <Field
                        type="textarea"
                        name="remark"
                        label="備註："
                        placeholder=""
                        component={Input} />
                    </div>
                    {isComplete || mutateProps.error ? (
                      <Error
                        error={(mutateProps.error && mutateProps.error.message)
                          || (isComplete && '退回成功')
                        || null} />
                    ) : null}
                    <div style={styles.functionWrapper}>
                      <SubmitButton
                        disabled={mutateProps.loading || false}
                        label="未銷貨確認" />
                    </div>
                  </div>
                </form>
              )}
            </Mutation>
          )}
        </div>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_LINEBOT_RETURN_DOA_FORM,
});

const reduxHook = connect(
  () => ({
  }),
  dispatch => bindActionCreators({
    initializeForm: v => initialize(FORM_LINEBOT_RETURN_DOA_FORM, v),
  }, dispatch),
);

export default reduxHook(
  formHook(
    radium(
      ReturnDOAPage
    )
  )
);
