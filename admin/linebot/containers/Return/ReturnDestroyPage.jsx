// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  Fields,
  initialize,
  SubmissionError,
} from 'redux-form';
import {
  Mutation,
} from 'react-apollo';

import { INVENTORY_ROLLBACK } from '../../../mutations/Rollback.js';
import { FORM_LINEBOT_RETURN_DESTROY_FORM } from '../../../shared/form.js';
import { inventoryRollbackTypeSpec } from '../../../shared/linebot.js';
import { validateDestroyForm } from '../../../helper/validator/ReturnForm.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import ReturnPNInfo from './ReturnPNInfo.jsx';
import Input from '../../../components/Form/Input.jsx';
import ErrorTypeSelectorBind from '../../components/Form/ErrorTypeSelectorBind.jsx';
import Error from '../../../components/Form/Error.jsx';
import SubmitButton from '../../../components/Form/SubmitButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
  mainWrapper: {
    width: '100%',
    height: 'calc(100vh - 90px)',
    overflow: 'auto',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  formWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0 24px 0',
  },
  mainFormWrapper: {
    width: '100%',
    height: 'auto',
    padding: 12,
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

type Props = {
  pnTableId?: string,
  itemId?: string,
  inventoryInfo?: {
    id: number,
    amount: number,
    pnTable: {
      id: number,
      code: string,
      name: string,
      picture: string,
      pnCategory: {
        id: number,
        name: string,
      },
      bigSubCategory: {
        id: number,
        name: string,
      },
      smallSubCategory: {
        id: number,
        name: string,
      },
    },
  },
  error?: {
    message: string,
  },
  handleSubmit: Function,
  initializeForm: Function,
};

type State = {
  isComplete: boolean,
};

class ReturnDestroy extends PureComponent<Props, State> {
  static defaultProps = {
    inventoryInfo: null,
    error: null,
    pnTableId: null,
    itemId: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isComplete: false,
    };
  }

  componentDidMount() {
    const {
      itemId,
      initializeForm,
    } = this.props;

    initializeForm({
      qrcode: itemId || null,
      errorType: null,
      errorDesc: null,
      remark: null,
    });
  }

  getFields(d) {
    const {
      pnTableId,
    } = this.props;

    const {
      qrcode,
      errorType,
      errorDesc,
      remark,
    } = d;

    const errors = validateDestroyForm(d);

    if (errors) {
      throw new SubmissionError(errors);
    }

    const errorStr = errorType ? `異常類型：${errorType}` : null;
    const errorDescStr = errorDesc ? `異常說明：${errorDesc}` : null;
    const remarkStr = remark ? `備註：${remark}` : null;
    const remarkFinalStr = [errorStr, errorDescStr, remarkStr]
      .filter(s => s)
      .join('***');

    const payload = {
      qrcode,
      remark: remarkFinalStr || null,
      rollbackType: inventoryRollbackTypeSpec.RETURN,
    };

    if (pnTableId) {
      payload.pnTableId = parseInt(pnTableId, 10);
    }

    return payload;
  }

  render() {
    const {
      inventoryInfo,
      error,
      handleSubmit,
    } = this.props;

    const {
      isComplete,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        <PageTitle title="未銷貨退回-下架退回" />
        <div style={styles.mainWrapper}>
          {inventoryInfo ? (
            <ReturnPNInfo
              needAmount
              data={inventoryInfo} />
          ) : null}
          <Mutation mutation={INVENTORY_ROLLBACK}>
            {(inventoryRollback, mutateProps) => (
              <form
                style={styles.formWrapper}
                onSubmit={handleSubmit(async (d) => {
                  const payload = this.getFields(d);

                  const res = await inventoryRollback({
                    variables: payload,
                  });

                  if (res
                    && res.data
                    && res.data.inventoryRollback
                    && res.data.inventoryRollback.status
                  ) {
                    this.setState({
                      isComplete: true,
                    });
                  }
                })}>
                <div style={styles.mainFormWrapper}>
                  <div style={styles.fieldWrapper}>
                    <Field
                      required
                      name="qrcode"
                      label="商品序號："
                      placeholder="請輸入商品序號"
                      component={Input} />
                  </div>
                  <Fields
                    type="pullof"
                    names={[
                      'errorType',
                      'errorDesc',
                    ]}
                    label="退回類型"
                    component={ErrorTypeSelectorBind} />
                  <div style={styles.fieldWrapper}>
                    <Field
                      type="textarea"
                      name="remark"
                      label="備註："
                      placeholder=""
                      component={Input} />
                  </div>
                  {isComplete || mutateProps.error || error ? (
                    <Error
                      error={(mutateProps.error && mutateProps.error.message)
                        || error
                        || (isComplete && '退回成功')
                      || null} />
                  ) : null}
                  <div style={styles.functionWrapper}>
                    <SubmitButton
                      disabled={mutateProps.loading || false}
                      label="確認下架退回" />
                  </div>
                </div>
              </form>
            )}
          </Mutation>
        </div>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_LINEBOT_RETURN_DESTROY_FORM,
});

const reduxHook = connect(
  () => ({}),
  dispatch => bindActionCreators({
    initializeForm: v => initialize(FORM_LINEBOT_RETURN_DESTROY_FORM, v),
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      ReturnDestroy
    )
  )
);
