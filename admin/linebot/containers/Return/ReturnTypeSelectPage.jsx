// @flow
import React from 'react';
import radium from 'radium';

// config
import { returnCategoryTypes } from '../../../shared/linebot.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import PageLink from '../../components/Global/PageLink.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
  mainPlacement: {
    width: '100%',
    height: 'auto',
    minHeight: 'calc(100vh - 90px)',
    padding: 12,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainWrapper: {
    width: '100%',
    maxWidth: 320,
    height: 450,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

type Props = {
  match: {
    url: string,
  },
};

function ReturnTypeSelectPage({
  match: {
    url,
  },
}: Props) {
  return (
    <div style={styles.wrapper}>
      <PageTitle title="未銷貨退回" />
      <div style={styles.mainPlacement}>
        <div style={styles.mainWrapper}>
          {returnCategoryTypes.map(rc => (
            <PageLink
              key={rc.id}
              name={rc.name}
              path={`${url}/type/${rc.id}`} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default radium(ReturnTypeSelectPage);
