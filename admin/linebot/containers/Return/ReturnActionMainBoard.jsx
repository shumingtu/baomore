// @flow
import React, { PureComponent } from 'react';
import { Query } from 'react-apollo';

import { FETCH_MY_INVENTORY_DETAIL } from '../../../queries/Inventory.js';
// components
import ReturnDOAPage from './ReturnDOAPage.jsx';
import ReturnDestroyPage from './ReturnDestroyPage.jsx';
import ReturnFreePage from './ReturnFreePage.jsx';

type Props = {
  errorType: 'doa' | 'destroy' | 'free',
  match: {
    params: {
      pnTableId: string,
      itemId: string,
    },
  },
};

class ReturnActionMainBoard extends PureComponent<Props> {
  renderMainComponent({
    inventoryInfo,
    error,
  }) {
    const {
      errorType,
      match: {
        params: {
          pnTableId,
          itemId,
        },
      },
    } = this.props;

    switch (errorType) {
      case 'doa': // DOA
        return (
          <ReturnDOAPage
            pnTableId={pnTableId || null}
            itemId={itemId || null}
            inventoryInfo={inventoryInfo}
            error={error} />
        );

      case 'destroy': // 下架退回
        return (
          <ReturnDestroyPage
            pnTableId={pnTableId || null}
            itemId={itemId || null}
            inventoryInfo={inventoryInfo}
            error={error} />
        );

      case 'free': // 公關銷帳
        return (
          <ReturnFreePage
            pnTableId={pnTableId || null}
            itemId={itemId || null}
            inventoryInfo={inventoryInfo}
            error={error} />
        );

      default:
        return null;
    }
  }

  render() {
    const {
      match: {
        params: {
          pnTableId,
        },
      },
    } = this.props;

    if (pnTableId) {
      return (
        <Query
          query={FETCH_MY_INVENTORY_DETAIL}
          variables={{
            pnTableId: parseInt(pnTableId, 10),
          }}>
          {({
            data,
            error,
          }) => {
            const meInventory = (data && data.meInventory) || {};

            return this.renderMainComponent({
              inventoryInfo: meInventory,
              error,
            });
          }}
        </Query>
      );
    }

    return this.renderMainComponent({});
  }
}

export default ReturnActionMainBoard;
