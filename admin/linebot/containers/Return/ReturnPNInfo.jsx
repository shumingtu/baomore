// @flow
import React, { memo } from 'react';

import Theme from '../../../styles/Theme.js';

const styles = {
  infoWrapper: {
    width: '100%',
    height: 'auto',
    padding: 12,
    overflowY: 'auto',
    overflowX: 'hidden',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  label: {
    width: 65,
    fontSize: 13,
    fontWeight: 500,
    color: Theme.BLACK_COLOR,
  },
  text: {
    flex: 1,
    fontSize: 13,
    color: Theme.BLACK_COLOR,
    lineHeight: 1,
    whiteSpace: 'pre-line',
    wordBreak: 'break-word',
  },
};

type Props = {
  data: {
    id: number,
    amount: number,
    pnTable: {
      id: number,
      code: string,
      name: string,
      picture: string,
      pnCategory: {
        id: number,
        name: string,
      },
      bigSubCategory: {
        id: number,
        name: string,
      },
      smallSubCategory: {
        id: number,
        name: string,
      },
    },
  },
  needAmount: boolean,
};

function ReturnPNInfo({
  data,
  needAmount,
}: Props) {
  if (!data) return null;

  const {
    pnTable,
  } = data;

  return (
    <div style={styles.infoWrapper}>
      <div style={styles.fieldWrapper}>
        <span style={styles.label}>
          料號編號：
        </span>
        {pnTable && pnTable.code ? (
          <span style={styles.text}>
            {pnTable.code}
          </span>
        ) : null}
      </div>
      <div style={styles.fieldWrapper}>
        <span style={styles.label}>
          料號類別：
        </span>
        {pnTable && pnTable.pnCategory ? (
          <span style={styles.text}>
            {pnTable.pnCategory.name || null}
          </span>
        ) : null}
      </div>
      <div style={styles.fieldWrapper}>
        <span style={styles.label}>
          大類：
        </span>
        {pnTable && pnTable.bigSubCategory ? (
          <span style={styles.text}>
            {pnTable.bigSubCategory.name || null}
          </span>
        ) : null}
      </div>
      <div style={styles.fieldWrapper}>
        <span style={styles.label}>
          類型：
        </span>
        {pnTable && pnTable.smallSubCategory ? (
          <span style={styles.text}>
            {pnTable.smallSubCategory.name || null}
          </span>
        ) : null}
      </div>
      <div style={styles.fieldWrapper}>
        <span style={styles.label}>
          品名：
        </span>
        {pnTable && pnTable.name ? (
          <span style={styles.text}>
            {pnTable.name}
          </span>
        ) : null}
      </div>
      {needAmount ? (
        <div style={styles.fieldWrapper}>
          <span style={styles.label}>
            數量：
          </span>
          <span style={styles.text}>
            {data.amount || 0}
          </span>
        </div>
      ) : null}
    </div>
  );
}

export default memo(ReturnPNInfo);
