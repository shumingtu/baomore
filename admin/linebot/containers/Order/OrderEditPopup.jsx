// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import qs from 'qs';
import {
  Mutation,
} from 'react-apollo';
import {
  reduxForm,
  Fields,
  Field,
  initialize,
  SubmissionError,
} from 'redux-form';

// static, config
import Theme from '../../../styles/Theme.js';
import closeIcon from '../../../static/images/close-icon.png';
import { GET_MY_ORDER_LIST } from '../../../queries/Order.js';
import {
  EDIT_ORDER_BY_LINEBOT,
  DELETE_ORDER_BY_LINEBOT,
} from '../../../mutations/Order.js';
import { FORM_LINEBOT_ORDER_EDIT_FORM } from '../../../shared/form.js';
// components
import ChannelStoreSelectorBind from '../../components/Form/ChannelStoreSelectorBind.jsx';
import Input from '../../../components/Form/Input.jsx';
import SubmitButton from '../../../components/Form/SubmitButton.jsx';
import Error from '../../../components/Form/Error.jsx';
import Button from '../../../components/Global/Button.jsx';

const styles = {
  wrapper: {
    position: 'fixed',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    padding: 12,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 999,
  },
  mainWrapper: {
    width: '100%',
    maxWidth: 414,
    height: 'auto',
    padding: '12px 24px',
    backgroundColor: '#fff',
    borderRadius: 3,
    border: `1px solid ${Theme.BLACK_COLOR}`,
    position: 'relative',
  },
  titleWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 15,
    fontWeight: 500,
    letterSpacing: 1,
    color: Theme.BLACK_COLOR,
  },
  closeBtn: {
    position: 'absolute',
    top: 12,
    right: 12,
    width: 24,
    height: 24,
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundImage: `url(${closeIcon})`,
    cursor: 'pointer',
  },
  formWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '4px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  label: {
    width: 65,
    fontSize: 13,
    fontWeight: 500,
    color: Theme.BLACK_COLOR,
  },
  text: {
    flex: 1,
    fontSize: 13,
    color: Theme.BLACK_COLOR,
    lineHeight: 1,
    whiteSpace: 'pre-line',
    wordBreak: 'break-word',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 4px',
  },
};

type Props = {
  location: {
    search: string,
  },
  match: {
    params: {
      orderId: string,
    },
  },
  orderOptions: {
    status: string,
    keyword: string,
  },
  handleSubmit: Function,
  onClose: Function,
  initializeForm: Function,
};

class OrderEditPopup extends PureComponent<Props> {
  componentDidMount() {
    const {
      initializeForm,
      location: {
        search,
      },
    } = this.props;

    const qsString = qs.parse(search, { ignoreQueryPrefix: true });

    initializeForm({
      channelId: (qsString && qsString.channelId) || '-1',
      cityId: (qsString && qsString.cityId) || '-1',
      storeId: (qsString && qsString.storeId) || '-1',
      quantity: (qsString && qsString.qty) || null,
    });
  }

  getFields(d) {
    const {
      match: {
        params: {
          orderId,
        },
      },
    } = this.props;

    const {
      storeId,
      quantity,
    } = d;

    if (!orderId || orderId === 'undefined') {
      throw new SubmissionError({
        _error: '系統發生錯誤',
      });
    }

    if (!storeId || storeId === '-1') {
      throw new SubmissionError({
        storeId: 'required',
      });
    }

    if (!quantity || parseInt(quantity, 10) <= 0) {
      throw new SubmissionError({
        quantity: 'failed',
      });
    }

    return ({
      orderId: parseInt(orderId, 10),
      storeId: parseInt(storeId, 10),
      quantity: parseInt(quantity, 10),
    });
  }

  render() {
    const {
      match: {
        params: {
          orderId,
        },
      },
      onClose,
      handleSubmit,
      location: {
        search,
      },
      orderOptions,
    } = this.props;

    const qsString = qs.parse(search, { ignoreQueryPrefix: true });
    const queryOptions = {
      offset: 0,
      limit: 10,
    };

    if (orderOptions) {
      const {
        status,
        keyword,
      } = orderOptions;

      if (status) queryOptions.status = status;
      if (keyword) queryOptions.keyword = keyword;
    }

    return (
      <div style={styles.wrapper}>
        <div style={styles.mainWrapper}>
          <button
            type="button"
            onClick={onClose || null}
            style={styles.closeBtn} />
          <div style={styles.titleWrapper}>
            <span style={styles.title}>
              訂單修改
            </span>
          </div>
          <div style={styles.fieldWrapper}>
            <span style={styles.label}>
              品名：
            </span>
            {qsString && qsString.name ? (
              <span style={styles.text}>
                {qsString.name}
              </span>
            ) : null}
          </div>
          <Mutation mutation={EDIT_ORDER_BY_LINEBOT}>
            {(editOrderByLineBot, editMutateProps) => (
              <form
                style={styles.formWrapper}
                onSubmit={handleSubmit(async (d) => {
                  const payload = this.getFields(d);

                  const {
                    data,
                  } = await editOrderByLineBot({
                    variables: payload,
                  });

                  if (data && data.editOrderByLineBot && data.editOrderByLineBot.status) {
                    if (onClose) onClose();
                  }
                })}>
                <Fields
                  names={[
                    'channelId',
                    'cityId',
                    'storeId',
                  ]}
                  component={ChannelStoreSelectorBind} />
                <div style={styles.fieldWrapper}>
                  <Field
                    required
                    type="number"
                    name="quantity"
                    label="訂貨數量："
                    placeholder="數量"
                    component={Input} />
                </div>
                {editMutateProps.error ? (
                  <Error
                    error={editMutateProps.error.message || null} />
                ) : null}
                <div style={styles.functionWrapper}>
                  <div style={styles.btnWrapper}>
                    <SubmitButton
                      disabled={editMutateProps.loading || false}
                      label="確認修改" />
                  </div>
                  <Mutation
                    mutation={DELETE_ORDER_BY_LINEBOT}
                    update={(cache, { data: { deleteOrderByLineBot } }) => {
                      const {
                        orderListByEmployee,
                      } = cache.readQuery({
                        query: GET_MY_ORDER_LIST,
                        variables: queryOptions,
                      });

                      const dIdx = orderListByEmployee
                        .findIndex(l => l.id === deleteOrderByLineBot.id);

                      if (~dIdx) {
                        cache.writeQuery({
                          query: GET_MY_ORDER_LIST,
                          variables: queryOptions,
                          data: {
                            orderListByEmployee: [
                              ...orderListByEmployee.slice(0, dIdx),
                              ...orderListByEmployee.slice(dIdx + 1),
                            ],
                          },
                        });
                      }
                    }}>
                    {(deleteOrderByLineBot, deleteMutateProps) => (
                      <div style={styles.btnWrapper}>
                        <Button
                          disabled={deleteMutateProps.loading || false}
                          label="刪除訂單"
                          onClick={async () => {
                            const {
                              data,
                            } = await deleteOrderByLineBot({
                              variables: {
                                orderId: parseInt(orderId, 10),
                              },
                            });

                            if (data
                              && data.deleteOrderByLineBot
                              && data.deleteOrderByLineBot.status
                            ) {
                              onClose();
                            }
                          }}
                          style={{ backgroundColor: Theme.ERROR_COLOR }} />
                      </div>
                    )}
                  </Mutation>
                </div>
              </form>
            )}
          </Mutation>
        </div>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_LINEBOT_ORDER_EDIT_FORM,
});

const reduxHook = connect(
  state => ({
    orderOptions: state.Search.order,
  }),
  dispatch => bindActionCreators({
    initializeForm: v => initialize(FORM_LINEBOT_ORDER_EDIT_FORM, v),
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      OrderEditPopup
    )
  )
);
