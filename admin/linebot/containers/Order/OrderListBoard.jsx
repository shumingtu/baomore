// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import {
  Switch,
  Route,
  withRouter,
} from 'react-router-dom';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';

import { SHIPPING_STATUS_SPEC } from '../../../shared/Global.js';
import { CACHE_LINEBOT_ORDER_SEARCH } from '../../../actions/Search.js';
import { FORM_LINEBOT_ORDER_FORM } from '../../../shared/form.js';
import { GET_MY_ORDER_LIST } from '../../../queries/Order.js';
// components
import { wrapFormToOrderSearchForm } from '../Form/OrderSearchForm.jsx';
import Table, { TableField } from '../../../components/Table/Table.jsx';
import InventoryDetailPopup from '../InventoryDetailPopup.jsx';
import OrderManageActions from '../../../components/Table/Actions/linebot/OrderManageActions.jsx';
import OrderEditPopup from './OrderEditPopup.jsx';
import OrderQRCodePopup from './OrderQRCodePopup.jsx';

const OrderSearchForm = wrapFormToOrderSearchForm(FORM_LINEBOT_ORDER_FORM);

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    position: 'relative',
  },
  searchWrapper: {
    width: '100%',
    height: 245,
    padding: 16,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  tableWrapper: {
    width: '100%',
    height: '100%',
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    overflow: 'auto',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '6px 0',
  },
};

type Props = {
  orderOptions: {
    status: string,
    keyword: string,
  },
  match: {
    url: string,
  },
  history: {
    push: Function,
  },
  hideSearchFrom?: boolean,
};

const LIMIT_LENGTH = 10;

class OrderListBoard extends PureComponent<Props> {
  static defaultProps = {
    hideSearchFrom: false,
  };

  render() {
    const {
      orderOptions,
      match: {
        url,
      },
      history,
      hideSearchFrom,
    } = this.props;

    const {
      cacheCloseDate,
    } = this.state;

    const queryOptions = {
      offset: 0,
      limit: LIMIT_LENGTH,
    };

    if (orderOptions) {
      const {
        status,
        keyword,
        orderSN,
        closeDate,
        qrcode,
      } = orderOptions;

      if (status) queryOptions.status = status;
      if (keyword) queryOptions.keyword = keyword;
      if (orderSN) queryOptions.orderSN = orderSN;
      if (closeDate) queryOptions.closeDate = closeDate;
      if (qrcode) queryOptions.qrcode = qrcode;
    }

    const wrapCacheOptions = orderOptions || {};

    return (
      <div style={styles.wrapper}>
        <OrderSearchForm
          hide={hideSearchFrom}
          actionType={CACHE_LINEBOT_ORDER_SEARCH}
          initialValue={{
            ...wrapCacheOptions,
          }} />
        <Query
          query={GET_MY_ORDER_LIST}
          fetchPolicy="cache-and-network"
          variables={queryOptions}>
          {({
            data,
            fetchMore,
          }) => {
            const orderListByEmployee = (data && data.orderListByEmployee) || [];

            return (
              <div style={styles.tableWrapper}>
                <Table
                  fixedItemWidth={100}
                  dataSource={orderListByEmployee.map(i => ({
                    id: i.id,
                    pnTableId: (i.pnTable && i.pnTable.id) || null,
                    name: (i.pnTable && i.pnTable.name) || null,
                    quantity: i.quantity,
                    status: SHIPPING_STATUS_SPEC[i.status] || '狀態異常',
                    store: i.store || null,
                    orderSN: i.orderSN || '無',
                    closeDate: (i.orderGropShipment && i.orderGropShipment.createdAt) || '尚未結單',
                  }))}
                  fetchMore={() => fetchMore({
                    variables: {
                      ...queryOptions,
                      offset: 0,
                      limit: orderListByEmployee.length + LIMIT_LENGTH,
                    },
                    updateQuery: (prev, { fetchMoreResult }) => {
                      if (!fetchMoreResult) return prev;

                      return {
                        ...prev,
                        orderListByEmployee: [
                          ...fetchMoreResult.orderListByEmployee,
                        ],
                      };
                    },
                  })}
                  getActions={() => [
                    <OrderManageActions
                      cacheCloseDate={v => this.setState({ cacheCloseDate: v || null })} />,
                  ]}
                  actionTitles={['操作']}
                  showPlaceholder={!orderListByEmployee.length}
                  placeholder="查無訂單">
                  <TableField
                    name="訂單編號"
                    fieldKey="orderSN"
                    flex={2}
                    isCenter />
                  <TableField
                    name="品名"
                    fieldKey="name"
                    flex={2}
                    isCenter />
                  <TableField
                    name="數量"
                    fieldKey="quantity"
                    flex={1}
                    isCenter />
                  <TableField
                    name="狀態"
                    fieldKey="status"
                    flex={1}
                    isCenter />
                </Table>
              </div>
            );
          }}
        </Query>
        <Switch>
          <Route
            path={`${url}/qrcodes/:orderId`}
            component={props => (
              <OrderQRCodePopup
                {...props}
                onClose={() => history.push(url)} />
            )} />
          <Route
            path={`${url}/detail/:pnTableId`}
            component={props => (
              <InventoryDetailPopup
                {...props}
                showCloseDate
                closeDate={cacheCloseDate}
                onClose={() => history.push(url)} />
            )} />
          <Route
            path={`${url}/action/edit/:orderId`}
            component={props => (
              <OrderEditPopup
                {...props}
                onClose={() => history.push(url)} />
            )} />
        </Switch>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    orderOptions: state.Search.order,
  }),
);

export default withRouter(
  reduxHook(
    radium(
      OrderListBoard
    )
  )
);
