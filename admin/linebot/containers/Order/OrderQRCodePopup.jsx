// @flow

import React from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';

import Theme from '../../../styles/Theme.js';
import closeIcon from '../../../static/images/close-icon.png';

import { FETCH_ORDER_QRCODES } from '../../../queries/Order.js';

const styles = {
  wrapper: {
    position: 'fixed',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    padding: 12,
    zIndex: 999,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainPlacement: {
    width: 'auto',
    height: 'auto',
    minWidth: 270,
    maxHeight: 'calc(100vh - 170px)',
    overflow: 'auto',
  },
  mainWrapper: {
    width: '100%',
    height: 'auto',
    maxWidth: 414,
    padding: '12px 24px',
    backgroundColor: '#fff',
    borderRadius: 3,
    border: `1px solid ${Theme.BLACK_COLOR}`,
    position: 'relative',
  },
  closeBtn: {
    position: 'absolute',
    top: 12,
    right: 12,
    width: 24,
    height: 24,
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundImage: `url(${closeIcon})`,
    cursor: 'pointer',
  },
  qrcodeWrap: {
    width: '100%',
    textAlign: 'center',
    margin: '5px 0',
  },
};

type Props = {
  onClose: Function,
  match: object,
};

function OrderQRCodePopup({
  onClose,
  match: {
    params: {
      orderId,
    },
  },
}: Props) {
  return (
    <div style={styles.wrapper}>
      <div style={styles.mainPlacement}>
        <Query
          query={FETCH_ORDER_QRCODES}
          variables={{
            orderId: orderId ? parseInt(orderId, 10) : -1,
          }}>
          {({
            data,
          }) => {
            const qrcodes = (data && data.orderQRCodeList) || [];

            return (
              <div style={styles.mainWrapper}>
                <button
                  type="button"
                  onClick={onClose || null}
                  style={styles.closeBtn} />
                {qrcodes.map(q => (
                  <div key={q.id} style={styles.qrcodeWrap}>
                    {q.qrcode}
                  </div>
                ))}
              </div>
            );
          }}
        </Query>
      </div>
    </div>
  );
}

export default radium(OrderQRCodePopup);
