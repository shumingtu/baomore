// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';
import {
  Switch,
  Route,
  withRouter,
} from 'react-router-dom';

import { CACHE_LINEBOT_INVENTORY_PROTECTOR_SEARCH } from '../../../actions/Search.js';
import { FORM_LINEBOT_INVENTORY_PROTECTOR_FORM } from '../../../shared/form.js';
import { FETCH_INVENTORIES } from '../../../queries/Inventory.js';
// components
import { wrapFormToProtectorSearchForm } from '../Form/ProtectorSearchForm.jsx';
import Table, { TableField } from '../../../components/Table/Table.jsx';
import InventoryManageActions from '../../../components/Table/Actions/linebot/InventoryManageActions.jsx';
import InventoryDetailPopup from '../InventoryDetailPopup.jsx';
import Button from '../../../components/Global/Button.jsx';

const ProtectorSearchForm = wrapFormToProtectorSearchForm(FORM_LINEBOT_INVENTORY_PROTECTOR_FORM);

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    position: 'relative',
  },
  searchWrapper: {
    width: '100%',
    height: 245,
    padding: 16,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 277px)',
    flex: 1,
    // overflow: 'auto',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '6px 0',
  },
  collapseBtn: {
    position: 'absolute',
    top: -36,
    right: 10,
  },
};

type Props = {
  inventoryProtectorOptions: {
    employeeId?: string,
    brandId: string,
    modelId: string,
    keyword: string,
  },
  pnCategoryId: string,
  match: {
    url: string,
  },
  history: {
    push: Function,
  },
};

const LIMIT_LENGTH = 10;

class InventoryProtectorBoard extends PureComponent<Props> {
  state = {
    hideSearchForm: false,
  }

  render() {
    const {
      inventoryProtectorOptions,
      pnCategoryId,
      match: {
        url,
      },
      history,
    } = this.props;

    const {
      hideSearchForm,
    } = this.state;

    const queryOptions = {
      pnCategoryId: pnCategoryId ? parseInt(pnCategoryId, 10) : null,
      offset: 0,
      limit: LIMIT_LENGTH,
    };

    if (inventoryProtectorOptions) {
      const {
        brandId,
        modelId,
        keyword,
        employeeId,
      } = inventoryProtectorOptions;

      if (brandId) queryOptions.brandId = parseInt(brandId, 10);
      if (modelId) queryOptions.brandModelId = parseInt(modelId, 10);
      if (keyword) queryOptions.keyword = keyword;
      if (employeeId) queryOptions.employeeId = parseInt(employeeId, 10);
    }

    return (
      <div style={styles.wrapper}>
        <div style={styles.collapseBtn}>
          <Button
            onClick={() => this.setState({ hideSearchForm: !hideSearchForm })}
            label={hideSearchForm ? '展開' : '收合'} />
        </div>
        <ProtectorSearchForm
          hide={hideSearchForm}
          actionType={CACHE_LINEBOT_INVENTORY_PROTECTOR_SEARCH}
          initialValue={inventoryProtectorOptions} />
        <Query
          query={FETCH_INVENTORIES}
          fetchPolicy="cache-and-network"
          variables={{
            ...queryOptions,
          }}>
          {({
            data,
            fetchMore,
          }) => {
            const inventories = (data && data.inventories) || [];

            return (
              <div style={styles.tableWrapper}>
                <Table
                  dataSource={inventories.map(i => ({
                    id: i.id,
                    name: (i.pnTable && i.pnTable.name) || null,
                    amount: i.amount || 0,
                  }))}
                  fetchMore={() => fetchMore({
                    variables: {
                      ...queryOptions,
                      offset: 0,
                      limit: inventories.length + LIMIT_LENGTH,
                    },
                    updateQuery: (prev, { fetchMoreResult }) => {
                      if (!fetchMoreResult) return prev;

                      return {
                        ...prev,
                        inventories: [
                          ...fetchMoreResult.inventories,
                        ],
                      };
                    },
                  })}
                  getActions={() => [
                    <InventoryManageActions />,
                  ]}
                  actionTitles={['操作']}
                  showPlaceholder={!inventories.length}
                  placeholder="查無存貨">
                  <TableField
                    name="品名"
                    fieldKey="name"
                    flex={2}
                    isCenter />
                  <TableField
                    name="庫存數量"
                    fieldKey="amount"
                    flex={1}
                    isCenter />
                </Table>
              </div>
            );
          }}
        </Query>
        <Switch>
          <Route
            path={`${url}/detail/:pnTableId`}
            component={props => (
              <InventoryDetailPopup
                {...props}
                queryOptions={queryOptions}
                onClose={() => history.push(url)} />
            )} />
        </Switch>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    inventoryProtectorOptions: state.Search.inventoryProtector,
  }),
);

export default withRouter(
  reduxHook(
    radium(
      InventoryProtectorBoard
    )
  )
);
