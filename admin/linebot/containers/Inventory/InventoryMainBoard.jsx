// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

import { pnCategoryTypeIsProtector } from '../../../helper/linebot.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import InventoryProtectorBoard from './InventoryProtectorBoard.jsx';
import InventoryMaterialBoard from './InventoryMaterialBoard.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
  mainWrapper: {
    width: '100%',
    height: 'calc(100vh - 90px)',
  },
};

type Props = {
  match: {
    params: {
      pnCategoryType: string,
    },
  },
};

class InventoryMainBoard extends PureComponent<Props> {
  categoryDecider() {
    const {
      match: {
        params: {
          pnCategoryType,
          pnCategoryId,
        },
      },
    } = this.props;

    const isProtector = pnCategoryTypeIsProtector(pnCategoryType);

    if (isProtector) {
      return (
        <InventoryProtectorBoard pnCategoryId={pnCategoryId} />
      );
    }

    return (
      <InventoryMaterialBoard defaultPNCategoryId={pnCategoryId} />
    );
  }

  render() {
    return (
      <div style={styles.wrapper}>
        <PageTitle title="存貨" />
        <div style={styles.mainWrapper}>
          {this.categoryDecider()}
        </div>
      </div>
    );
  }
}

export default radium(InventoryMainBoard);
