// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';

import { CACHE_LINEBOT_EMPLOYEE_LIST_SEARCH } from '../../../actions/Search.js';
import { FORM_LINEBOT_EMPLOYEE_FORM } from '../../../shared/form.js';
import { GET_EMPLOYEE_LIST } from '../../../queries/Member.js';
import { isPermissionAllowed } from '../../../helper/roles.js';
import { LINEBOT_EMPLOYEE_INFO_AREA_SELECTOR_KEYS } from '../../../../shared/selectorActionKeys.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import { wrapFormToEmployeeSearchForm } from '../Form/EmployeeSearchForm.jsx';
import Table, { TableField } from '../../../components/Table/Table.jsx';
import EmployeeManageActions from '../../../components/Table/Actions/linebot/EmployeeManageActions.jsx';
import EmployeePopup from './EmployeePopup.jsx';
import Button from '../../../components/Global/Button.jsx';

const EmployeeSearchForm = wrapFormToEmployeeSearchForm(FORM_LINEBOT_EMPLOYEE_FORM);

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    position: 'relative',
  },
  searchWrapper: {
    width: '100%',
    height: 245,
    padding: 16,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 176px)',
    flex: 1,
    paddingTop: 12,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '6px 0',
  },
  collapseBtn: {
    position: 'absolute',
    top: 4,
    right: 10,
  },
};

type Props = {
  employeeListOptions: {
    areaId: string,
    keyword: string,
  },
  memberAreaId: number,
};

type State = {
  currentEmployee: Object,
};

const LIMIT_LENGTH = 10;

class EmployeeInfoList extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      currentEmployee: null,
      hideSearchForm: false,
    };
  }

  render() {
    const {
      employeeListOptions,
      memberAreaId,
    } = this.props;

    const {
      currentEmployee,
      hideSearchForm,
    } = this.state;

    const queryOptions = {
      isForClient: true,
    };

    const allowSearch = isPermissionAllowed(LINEBOT_EMPLOYEE_INFO_AREA_SELECTOR_KEYS);

    if (employeeListOptions) {
      const {
        keyword,
      } = employeeListOptions;

      if (keyword) queryOptions.keyword = keyword;
    }

    // 最後更改為不管是主任、包膜師 都只能看到自己區域的包膜師
    queryOptions.areaId = memberAreaId;

    const wrapCacheOptions = employeeListOptions || {};

    return (
      <div style={styles.wrapper}>
        <PageTitle title="包膜師資訊" />
        {allowSearch ? (
          <>
            <div style={styles.collapseBtn}>
              <Button
                onClick={() => this.setState({ hideSearchForm: !hideSearchForm })}
                label={hideSearchForm ? '展開' : '收合'} />
            </div>
            <EmployeeSearchForm
              hide={hideSearchForm}
              actionType={CACHE_LINEBOT_EMPLOYEE_LIST_SEARCH}
              initialValue={{
                ...wrapCacheOptions,
              }} />
          </>
        ) : null}
        <Query
          query={GET_EMPLOYEE_LIST}
          variables={{
            ...queryOptions,
            limit: LIMIT_LENGTH,
            offset: 0,
          }}>
          {({
            data,
            fetchMore,
          }) => {
            const employeeMemberlist = (data && data.employeeMemberlist) || [];

            return (
              <div style={styles.tableWrapper}>
                <Table
                  dataSource={employeeMemberlist.map(i => ({
                    id: i.id,
                    serialNumber: i.serialNumber || null,
                    name: i.name || null,
                    phone: i.phone || null,
                    areaName: (i.area && i.area.name) || null,
                    lineAccount: i.lineAccount || null,
                  }))}
                  fetchMore={() => fetchMore({
                    variables: {
                      ...queryOptions,
                      offset: 0,
                      limit: employeeMemberlist.length + LIMIT_LENGTH,
                    },
                    updateQuery: (prev, { fetchMoreResult }) => {
                      if (!fetchMoreResult) return prev;

                      return {
                        ...prev,
                        employeeMemberlist: [
                          ...fetchMoreResult.employeeMemberlist,
                        ],
                      };
                    },
                  })}
                  getActions={() => [
                    <EmployeeManageActions
                      onClick={v => this.setState({
                        currentEmployee: {
                          id: v.id,
                          name: v.name,
                          serialNumber: v.serialNumber,
                          phone: v.phone,
                          areaName: v.areaName,
                          lineAccount: v.lineAccount,
                        },
                      })} />,
                  ]}
                  actionTitles={['操作']}
                  showPlaceholder={!employeeMemberlist.length}
                  placeholder="查無包膜師">
                  <TableField
                    name="包膜師名稱"
                    fieldKey="name"
                    flex={1}
                    isCenter />
                  <TableField
                    name="電話"
                    fieldKey="phone"
                    flex={1}
                    isCenter />
                </Table>
              </div>
            );
          }}
        </Query>
        {currentEmployee ? (
          <EmployeePopup
            employee={currentEmployee}
            onClose={() => this.setState({ currentEmployee: null })} />
        ) : null}
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    employeeListOptions: state.Search.employeeList,
    memberAreaId: state.Member.memberAreaId,
  }),
);

export default reduxHook(
  radium(
    EmployeeInfoList
  )
);
