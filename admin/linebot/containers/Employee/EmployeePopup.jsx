// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

// static, config
import Theme from '../../../styles/Theme.js';
import closeIcon from '../../../static/images/close-icon.png';

const styles = {
  wrapper: {
    position: 'fixed',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    padding: 12,
    zIndex: 999,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainPlacement: {
    width: 'auto',
    height: 'auto',
    minWidth: 270,
    maxHeight: 'calc(100vh - 24px)',
    overflow: 'auto',
  },
  mainWrapper: {
    width: '100%',
    height: 'auto',
    maxWidth: 414,
    padding: '36px 24px',
    backgroundColor: '#fff',
    borderRadius: 3,
    border: `1px solid ${Theme.BLACK_COLOR}`,
    position: 'relative',
  },
  closeBtn: {
    position: 'absolute',
    top: 12,
    right: 12,
    width: 24,
    height: 24,
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundImage: `url(${closeIcon})`,
    cursor: 'pointer',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '4px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  label: {
    width: 65,
    fontSize: 13,
    fontWeight: 500,
    color: Theme.BLACK_COLOR,
  },
  text: {
    flex: 1,
    fontSize: 13,
    color: Theme.BLACK_COLOR,
    lineHeight: 1,
    whiteSpace: 'pre-line',
    wordBreak: 'break-word',
  },
};

type Props = {
  onClose: Function,
  employee: {
    id: number,
    name: string,
    serialNumber: string,
    phone: string,
    areaName: string,
    lineAccount: string,
  },
};

class EmployeePopup extends PureComponent<Props> {
  render() {
    const {
      onClose,
      employee,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.mainPlacement}>
          <div style={styles.mainWrapper}>
            <button
              type="button"
              onClick={onClose || null}
              style={styles.closeBtn} />
            <div style={styles.fieldWrapper}>
              <span style={styles.label}>
                區域：
              </span>
              {employee.areaName ? (
                <span style={styles.text}>
                  {employee.areaName}
                </span>
              ) : null}
            </div>
            <div style={styles.fieldWrapper}>
              <span style={styles.label}>
                工號：
              </span>
              {employee.serialNumber ? (
                <span style={styles.text}>
                  {employee.serialNumber || null}
                </span>
              ) : null}
            </div>
            <div style={styles.fieldWrapper}>
              <span style={styles.label}>
                姓名：
              </span>
              {employee.name ? (
                <span style={styles.text}>
                  {employee.name || null}
                </span>
              ) : null}
            </div>
            <div style={styles.fieldWrapper}>
              <span style={styles.label}>
                電話：
              </span>
              {employee.phone ? (
                <span style={styles.text}>
                  {employee.phone || null}
                </span>
              ) : null}
            </div>
            <div style={styles.fieldWrapper}>
              <span style={styles.label}>
                line帳號：
              </span>
              {employee.lineAccount ? (
                <span style={styles.text}>
                  {employee.lineAccount}
                </span>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default radium(EmployeePopup);
