// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

// static, config
import { shipStatus } from '../../../shared/order.js';
import Theme from '../../../styles/Theme.js';
import closeIcon from '../../../static/images/close-icon.png';

const styles = {
  wrapper: {
    position: 'fixed',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    padding: 12,
    zIndex: 999,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainPlacement: {
    width: 'auto',
    height: 'auto',
    minWidth: 270,
    maxHeight: 'calc(100vh - 24px)',
    overflow: 'auto',
  },
  mainWrapper: {
    width: '100%',
    height: 'auto',
    maxWidth: 414,
    padding: '36px 24px',
    backgroundColor: '#fff',
    borderRadius: 3,
    border: `1px solid ${Theme.BLACK_COLOR}`,
    position: 'relative',
  },
  closeBtn: {
    position: 'absolute',
    top: 12,
    right: 12,
    width: 24,
    height: 24,
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundImage: `url(${closeIcon})`,
    cursor: 'pointer',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '4px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  label: {
    width: 90,
    fontSize: 13,
    fontWeight: 500,
    color: Theme.BLACK_COLOR,
  },
  text: {
    flex: 1,
    fontSize: 13,
    color: Theme.BLACK_COLOR,
    lineHeight: 1,
    whiteSpace: 'pre-line',
    wordBreak: 'break-word',
  },
};

type Props = {
  onClose: Function,
  order: {
    id: number,
    name: string,
    phone: string,
    email: string,
    store: {
      id: number,
      name: string,
    },
    date: string,
    time: string,
    shippingStatus: string,
  },
};

class ScheduleDetailPopup extends PureComponent<Props> {
  render() {
    const {
      onClose,
      order,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.mainPlacement}>
          <div style={styles.mainWrapper}>
            <button
              type="button"
              onClick={onClose || null}
              style={styles.closeBtn} />
            <div style={styles.fieldWrapper}>
              <span style={styles.label}>
                訂購人姓名：
              </span>
              {order.name ? (
                <span style={styles.text}>
                  {order.name}
                </span>
              ) : null}
            </div>
            <div style={styles.fieldWrapper}>
              <span style={styles.label}>
                訂購人電話：
              </span>
              {order.phone ? (
                <span style={styles.text}>
                  {order.phone || null}
                </span>
              ) : null}
            </div>
            <div style={styles.fieldWrapper}>
              <span style={styles.label}>
                訂購人Email：
              </span>
              {order.email ? (
                <span style={styles.text}>
                  {order.email || null}
                </span>
              ) : null}
            </div>
            <div style={styles.fieldWrapper}>
              <span style={styles.label}>
                預約門市：
              </span>
              {order.store ? (
                <span style={styles.text}>
                  {order.store.name || null}
                </span>
              ) : null}
            </div>
            <div style={styles.fieldWrapper}>
              <span style={styles.label}>
                預約時間：
              </span>
              {order.date ? (
                <span style={styles.text}>
                  {order.date ? `${order.date} ` : null}
                  {order.time || null}
                </span>
              ) : null}
            </div>
            <div style={styles.fieldWrapper}>
              <span style={styles.label}>
                出貨狀態：
              </span>
              {order.shippingStatus ? (
                <span style={styles.text}>
                  {shipStatus.find(s => s.id === order.shippingStatus)
                    ? shipStatus.find(s => s.id === order.shippingStatus).name
                    : null}
                </span>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default radium(ScheduleDetailPopup);
