// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import moment from 'moment';
import { Query } from 'react-apollo';

import {
  FETCH_SCHEDULE_DATE_ASSIGNMENT,
} from '../../../queries/Schedule.js';
import { FETCH_BOOK_TOME_SLOT_LIST } from '../../../queries/Order.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import Table, { TableField } from '../../../components/Table/Table.jsx';
import ScheduleManageActions from '../../../components/Table/Actions/linebot/ScheduleManageActions.jsx';
import YellowField from '../../../components/Table/Custom/YellowField.jsx';
import ScheduleCalendar from '../../components/Schedule/ScheduleCalendar.jsx';
import ScheduleDetailPopup from './ScheduleDetailPopup.jsx';
import Error from '../../../components/Form/Error.jsx';
import Button from '../../../components/Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    position: 'relative',
  },
  searchWrapper: {
    width: '100%',
    height: 245,
    padding: 16,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  tableWrapper: {
    width: '100%',
    height: 'auto',
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '6px 0',
  },
  collapseBtn: {
    position: 'absolute',
    top: 4,
    right: 10,
  },
};

type Props = {

};

type State = {
  currentOrder: Object,
  currentSelectedDate: string,
};

class SchedulePage extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      currentOrder: null,
      currentSelectedDate: moment().format('YYYY-MM-DD'),
      hideCalendar: false,
    };
  }

  render() {
    const {
      currentOrder,
      currentSelectedDate,
      hideCalendar,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        <div style={styles.collapseBtn}>
          <Button
            onClick={() => this.setState({ hideCalendar: !hideCalendar })}
            label={hideCalendar ? '展開' : '收合'} />
        </div>
        <PageTitle title="查看行程" />
        <ScheduleCalendar
          hide={hideCalendar}
          onChange={d => this.setState({ currentSelectedDate: d })}
          value={currentSelectedDate} />
        <Query query={FETCH_BOOK_TOME_SLOT_LIST}>
          {slotProps => (
            <Query
              query={FETCH_SCHEDULE_DATE_ASSIGNMENT}
              fetchPolicy="cache-and-network"
              skip={!currentSelectedDate}
              variables={{
                date: currentSelectedDate,
              }}>
              {({
                data,
                error,
              }) => {
                const slotData = (slotProps.data && slotProps.data.bookTimeSlotList) || [];
                const meScheduleDateAssignments = (data && data.meScheduleDateAssignments) || [];
                const scheduleSrc = slotData.map((s, idx) => {
                  const assign = meScheduleDateAssignments.find(a => a.time === s.time);

                  return ({
                    id: `${idx}-${s.time}`,
                    time: s.time,
                    order: (assign && assign.order) || null,
                  });
                });

                return (
                  <div style={styles.tableWrapper}>
                    {error ? (
                      <Error error={error.message || null} />
                    ) : null}
                    <Table
                      dataSource={scheduleSrc}
                      getActions={() => [
                        <ScheduleManageActions
                          onClick={v => this.setState({
                            currentOrder: v,
                          })} />,
                      ]}
                      actionTitles={['預約狀態']}
                      showPlaceholder={!currentSelectedDate || !slotData.length}
                      placeholder="未選擇日期/查無行程">
                      <TableField
                        name="欄位"
                        fieldKey="time"
                        flex={1}
                        Component={YellowField} />
                    </Table>
                  </div>
                );
              }}
            </Query>
          )}
        </Query>
        {currentOrder ? (
          <ScheduleDetailPopup
            order={currentOrder}
            onClose={() => this.setState({ currentOrder: null })} />
        ) : null}
      </div>
    );
  }
}

export default radium(
  SchedulePage
);
