// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

// static, config
import Theme from '../../../styles/Theme.js';
import closeIcon from '../../../static/images/close-icon.png';

const styles = {
  wrapper: {
    position: 'fixed',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    padding: 12,
    zIndex: 999,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainPlacement: {
    width: 'auto',
    height: 'auto',
    minWidth: 270,
    maxHeight: 'calc(100vh - 24px)',
    overflow: 'auto',
  },
  mainWrapper: {
    width: '100%',
    height: 'auto',
    maxWidth: 414,
    padding: '36px 24px 12px',
    backgroundColor: '#fff',
    borderRadius: 3,
    border: `1px solid ${Theme.BLACK_COLOR}`,
    position: 'relative',
  },
  closeBtn: {
    position: 'absolute',
    top: 12,
    right: 12,
    width: 24,
    height: 24,
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundImage: `url(${closeIcon})`,
    cursor: 'pointer',
  },
  textWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  title: {
    fontSize: 15,
    fontWeight: 700,
    color: Theme.BLACK_COLOR,
    whiteSpace: 'pre-line',
    wordBreak: 'break-word',
  },
  content: {
    fontSize: 13,
    fontWeight: 400,
    color: Theme.BLACK_COLOR,
    lineHeight: 1.6,
    whiteSpace: 'pre-line',
    wordBreak: 'break-word',
  },
  dateWrapper: {
    width: '100%',
    height: 'auto',
    padding: '4px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  date: {
    fontSize: 12,
    fontWeight: 400,
    color: 'rgba(0, 0, 0, 0.5)',
  },
};

type Props = {
  onClose: Function,
  announcement: {
    id: number,
    title: string,
    content: string,
    createdAt: string,
  },
};

class AnnouncementPopup extends PureComponent<Props> {
  render() {
    const {
      onClose,
      announcement,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.mainPlacement}>
          <div style={styles.mainWrapper}>
            <button
              type="button"
              onClick={onClose || null}
              style={styles.closeBtn} />
            <div style={styles.textWrapper}>
              {announcement.title ? (
                <span style={styles.title}>
                  {announcement.title}
                </span>
              ) : null}
            </div>
            <div style={styles.textWrapper}>
              {announcement.content ? (
                <span style={styles.content}>
                  {announcement.content}
                </span>
              ) : null}
            </div>
            <div style={styles.dateWrapper}>
              {announcement.createdAt ? (
                <span style={styles.date}>
                  {announcement.createdAt}
                </span>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default radium(AnnouncementPopup);
