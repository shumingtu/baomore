// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';

import { CACHE_LINEBOT_ANNOUNCEMENT_SEARCH } from '../../../actions/Search.js';
import { FORM_LINEBOT_ANNOUNCEMENT_FORM } from '../../../shared/form.js';
import { FETCH_ANNOUNCEMENT_LIST } from '../../../queries/Announcement.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import { wrapFormToAnnoucementSearchForm } from '../Form/AnnouncementSearchForm.jsx';
import Table, { TableField } from '../../../components/Table/Table.jsx';
import EmployeeManageActions from '../../../components/Table/Actions/linebot/EmployeeManageActions.jsx';
import AnnouncementPopup from './AnnouncementPopup.jsx';

const AnnouncementSearchForm = wrapFormToAnnoucementSearchForm(FORM_LINEBOT_ANNOUNCEMENT_FORM);

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    position: 'relative',
  },
  searchWrapper: {
    width: '100%',
    height: 245,
    padding: 16,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  tableWrapper: {
    width: '100%',
    height: 'auto',
    flex: 1,
    paddingTop: 12,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '6px 0',
  },
};

type Props = {
  announcementOptions: {
    keyword: string,
  },
};

type State = {
  currentAnnouncement: Object,
};

const LIMIT_LENGTH = 10;

class AnnouncementList extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      currentAnnouncement: null,
    };
  }

  render() {
    const {
      announcementOptions,
    } = this.props;

    const {
      currentAnnouncement,
    } = this.state;

    const queryOptions = {
      offset: 0,
      limit: LIMIT_LENGTH,
    };

    if (announcementOptions) {
      const {
        keyword,
      } = announcementOptions;

      if (keyword) queryOptions.keyword = keyword;
    }

    const wrapCacheOptions = announcementOptions || {};

    return (
      <div style={styles.wrapper}>
        <PageTitle title="公告" />
        <AnnouncementSearchForm
          actionType={CACHE_LINEBOT_ANNOUNCEMENT_SEARCH}
          initialValue={{
            ...wrapCacheOptions,
          }} />
        <Query
          query={FETCH_ANNOUNCEMENT_LIST}
          variables={queryOptions}>
          {({
            data,
            fetchMore,
          }) => {
            const announcements = (data && data.announcements) || [];

            return (
              <div style={styles.tableWrapper}>
                <Table
                  dataSource={announcements.map(i => ({
                    id: i.id,
                    title: i.title || null,
                    content: i.content || null,
                    createdAt: i.createdAt || null,
                  }))}
                  fetchMore={() => fetchMore({
                    variables: {
                      ...queryOptions,
                      offset: 0,
                      limit: announcements.length + LIMIT_LENGTH,
                    },
                    updateQuery: (prev, { fetchMoreResult }) => {
                      if (!fetchMoreResult) return prev;

                      return {
                        ...prev,
                        announcements: [
                          ...fetchMoreResult.announcements,
                        ],
                      };
                    },
                  })}
                  getActions={() => [
                    <EmployeeManageActions
                      onClick={v => this.setState({
                        currentAnnouncement: {
                          id: v.id,
                          title: v.title,
                          content: v.content,
                          createdAt: v.createdAt,
                        },
                      })} />,
                  ]}
                  actionTitles={['操作']}
                  showPlaceholder={!announcements.length}
                  placeholder="查無公告">
                  <TableField
                    name="標題"
                    fieldKey="title"
                    flex={2}
                    isCenter />
                  <TableField
                    name="發布日期"
                    fieldKey="createdAt"
                    flex={1}
                    isCenter />
                </Table>
              </div>
            );
          }}
        </Query>
        {currentAnnouncement ? (
          <AnnouncementPopup
            announcement={currentAnnouncement}
            onClose={() => this.setState({ currentAnnouncement: null })} />
        ) : null}
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    announcementOptions: state.Search.announcements,
  }),
);

export default reduxHook(
  radium(
    AnnouncementList
  )
);
