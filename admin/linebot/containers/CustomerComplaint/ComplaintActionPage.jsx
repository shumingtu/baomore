// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import {
  reduxForm,
  Field,
  Fields,
  SubmissionError,
} from 'redux-form';
import {
  Query,
  Mutation,
} from 'react-apollo';

import Theme from '../../../styles/Theme.js';
import { validateComplaintForm } from '../../../helper/validator/ComplaintForm.js';
import { FETCH_MY_INVENTORY_DETAIL } from '../../../queries/Inventory.js';
import { ROLLBACK_FOR_CUSTOMER } from '../../../mutations/Rollback.js';
import { FORM_LINEBOT_CUSTOMER_COMPLAINT_CONFIRM_FORM } from '../../../shared/form.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import Error from '../../../components/Form/Error.jsx';
import Input from '../../../components/Form/Input.jsx';
import FileInput from '../../../components/Form/FileInput.jsx';
import ErrorTypeSelectorBind from '../../components/Form/ErrorTypeSelectorBind.jsx';
import SubmitButton from '../../../components/Form/SubmitButton.jsx';
import ChannelStoreSelectorBind from '../../components/Form/ChannelStoreSelectorBind.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
  },
  mainWrapper: {
    width: '100%',
    height: 'calc(100vh - 90px)',
    overflow: 'auto',
  },
  infoWrapper: {
    width: '100%',
    height: 'auto',
    padding: 12,
    overflowY: 'auto',
    overflowX: 'hidden',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  label: {
    width: 65,
    fontSize: 13,
    fontWeight: 500,
    color: Theme.BLACK_COLOR,
  },
  text: {
    flex: 1,
    fontSize: 13,
    color: Theme.BLACK_COLOR,
    lineHeight: 1,
    whiteSpace: 'pre-line',
    wordBreak: 'break-word',
  },
  formWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0 24px 0',
  },
  formTitleWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottom: `2px solid ${Theme.SECONDARY_THEME_COLOR}`,
  },
  formTitle: {
    fontSize: 15,
    fontWeight: 500,
    letterSpacing: 1,
    color: Theme.BLACK_COLOR,
  },
  mainFormWrapper: {
    width: '100%',
    height: 'auto',
    padding: 12,
  },
  fileWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '6px 0',
  },
  fileInput: {
    width: '100%',
    height: 130,
    maxWidth: 260,
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

type Props = {
  match: {
    params: {
      pnTableId: string,
    },
  },
  handleSubmit: Function,
};

type State = {
  isComplete: boolean,
};

class ComplaintActionPage extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      isComplete: false,
    };
  }

  getFields(d) {
    const {
      quantity,
      picture,
      vat,
      customerName,
      customerPhone,
      errorType,
      errorDesc,
      storeId,
      remark,
    } = d;

    const errors = validateComplaintForm(d);

    if (errors) {
      throw new SubmissionError(errors);
    }

    const vatStr = vat ? `發票號碼：${vat}` : null;
    const nameStr = customerName ? `客戶姓名：${customerName}` : null;
    const phoneStr = customerPhone ? `手機號碼：${customerPhone}` : null;
    const errorStr = errorType ? `異常類型：${errorType}` : null;
    const errorDescStr = errorDesc ? `異常說明：${errorDesc}` : null;
    const remarkStr = remark ? `備註：${remark}` : null;
    const remarkFinalStr = [vatStr, nameStr, phoneStr, errorStr, errorDescStr, remarkStr]
      .filter(s => s)
      .join('***');

    return ({
      quantity: parseInt(quantity, 10),
      picture: picture || null,
      storeId: storeId ? parseInt(storeId, 10) : null,
      remark: remarkFinalStr || null,
    });
  }

  render() {
    const {
      match: {
        params: {
          pnTableId,
        },
      },
      handleSubmit,
    } = this.props;

    const {
      isComplete,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        <PageTitle title="客訴退回" />
        <Query
          query={FETCH_MY_INVENTORY_DETAIL}
          variables={{
            pnTableId: pnTableId ? parseInt(pnTableId, 10) : -1,
          }}>
          {({
            data,
            error,
          }) => {
            const meInventory = (data && data.meInventory) || {};
            const {
              pnTable,
            } = meInventory;

            return (
              <div style={styles.mainWrapper}>
                <div style={styles.infoWrapper}>
                  <div style={styles.fieldWrapper}>
                    <span style={styles.label}>
                      料號編號：
                    </span>
                    {pnTable && pnTable.code ? (
                      <span style={styles.text}>
                        {pnTable.code}
                      </span>
                    ) : null}
                  </div>
                  <div style={styles.fieldWrapper}>
                    <span style={styles.label}>
                      料號類別：
                    </span>
                    {pnTable && pnTable.pnCategory ? (
                      <span style={styles.text}>
                        {pnTable.pnCategory.name || null}
                      </span>
                    ) : null}
                  </div>
                  <div style={styles.fieldWrapper}>
                    <span style={styles.label}>
                      大類：
                    </span>
                    {pnTable && pnTable.bigSubCategory ? (
                      <span style={styles.text}>
                        {pnTable.bigSubCategory.name || null}
                      </span>
                    ) : null}
                  </div>
                  <div style={styles.fieldWrapper}>
                    <span style={styles.label}>
                      類型：
                    </span>
                    {pnTable && pnTable.smallSubCategory ? (
                      <span style={styles.text}>
                        {pnTable.smallSubCategory.name || null}
                      </span>
                    ) : null}
                  </div>
                  <div style={styles.fieldWrapper}>
                    <span style={styles.label}>
                      品名：
                    </span>
                    {pnTable && pnTable.name ? (
                      <span style={styles.text}>
                        {pnTable.name}
                      </span>
                    ) : null}
                  </div>
                </div>
                {error ? (
                  <Error error={error.message || null} />
                ) : (
                  <Mutation mutation={ROLLBACK_FOR_CUSTOMER}>
                    {(rollBackForCustomer, mutateProps) => (
                      <form
                        style={styles.formWrapper}
                        onSubmit={handleSubmit(async (d) => {
                          const payload = this.getFields(d);

                          const res = await rollBackForCustomer({
                            variables: {
                              ...payload,
                              pnTableId: pnTable.id,
                            },
                          });

                          if (res
                            && res.data
                            && res.data.rollBackForCustomer
                            && res.data.rollBackForCustomer.status
                          ) {
                            this.setState({
                              isComplete: true,
                            });
                          }
                        })}>
                        <div style={styles.formTitleWrapper}>
                          <span style={styles.formTitle}>
                            填寫退回資訊
                          </span>
                        </div>
                        <div style={styles.mainFormWrapper}>
                          <div style={styles.fileWrapper}>
                            <Field
                              name="picture"
                              annotation="上傳異常圖片"
                              component={FileInput}
                              style={styles.fileInput} />
                          </div>
                          <div style={styles.fieldWrapper}>
                            <Field
                              required
                              name="quantity"
                              label="數量："
                              type="number"
                              placeholder="請輸入數量"
                              component={Input} />
                          </div>
                          <div style={styles.fieldWrapper}>
                            <Field
                              name="vat"
                              label="發票號碼："
                              placeholder="請輸入發票號碼"
                              component={Input} />
                          </div>
                          <div style={styles.fieldWrapper}>
                            <Field
                              name="customerName"
                              label="客戶姓名："
                              placeholder="請輸入客戶姓名"
                              component={Input} />
                          </div>
                          <div style={styles.fieldWrapper}>
                            <Field
                              name="customerPhone"
                              label="手機號碼："
                              placeholder="請輸入客戶手機號碼"
                              component={Input} />
                          </div>
                          <Fields
                            type="customer"
                            names={[
                              'errorType',
                              'errorDesc',
                            ]}
                            component={ErrorTypeSelectorBind} />
                          <div style={[styles.fieldWrapper, { flexDirection: 'column' }]}>
                            <Fields
                              names={[
                                'channelId',
                                'cityId',
                                'storeId',
                              ]}
                              component={ChannelStoreSelectorBind} />
                          </div>
                          <div style={styles.fieldWrapper}>
                            <Field
                              type="textarea"
                              name="remark"
                              label="備註："
                              placeholder=""
                              component={Input} />
                          </div>
                          {isComplete || mutateProps.error ? (
                            <Error
                              error={(isComplete && '退回成功')
                                || (mutateProps.error && mutateProps.error.message)
                              || null} />
                          ) : null}
                          <div style={styles.functionWrapper}>
                            <SubmitButton
                              disabled={mutateProps.loading || false}
                              label="銷貨退回確認" />
                          </div>
                        </div>
                      </form>
                    )}
                  </Mutation>
                )}
              </div>
            );
          }}
        </Query>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_LINEBOT_CUSTOMER_COMPLAINT_CONFIRM_FORM,
});

export default formHook(
  radium(
    ComplaintActionPage
  )
);
