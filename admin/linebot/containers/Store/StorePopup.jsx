// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

// static, config
import Theme from '../../../styles/Theme.js';
import closeIcon from '../../../static/images/close-icon.png';

const styles = {
  wrapper: {
    position: 'fixed',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    padding: 12,
    zIndex: 999,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainPlacement: {
    width: 'auto',
    height: 'auto',
    minWidth: 270,
    maxHeight: 'calc(100vh - 24px)',
    overflow: 'auto',
  },
  mainWrapper: {
    width: '100%',
    height: 'auto',
    maxWidth: 414,
    padding: '36px 24px',
    backgroundColor: '#fff',
    borderRadius: 3,
    border: `1px solid ${Theme.BLACK_COLOR}`,
    position: 'relative',
  },
  closeBtn: {
    position: 'absolute',
    top: 12,
    right: 12,
    width: 24,
    height: 24,
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundImage: `url(${closeIcon})`,
    cursor: 'pointer',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '4px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  label: {
    width: 65,
    fontSize: 13,
    fontWeight: 500,
    color: Theme.BLACK_COLOR,
  },
  text: {
    flex: 1,
    fontSize: 13,
    color: Theme.BLACK_COLOR,
    lineHeight: 1,
    whiteSpace: 'pre-line',
    wordBreak: 'break-word',
  },
};

type Props = {
  onClose: Function,
  store: {
    id: number,
    name: string,
    phone: string,
    address: string,
    latitude: number,
    longitude: number,
    manager: string,
    areaName: string,
    channelName: string,
    cityName: string,
    employees: Array<string>,
  },
};

class StorePopup extends PureComponent<Props> {
  render() {
    const {
      onClose,
      store,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.mainPlacement}>
          <div style={styles.mainWrapper}>
            <button
              type="button"
              onClick={onClose || null}
              style={styles.closeBtn} />
            <div style={styles.fieldWrapper}>
              <span style={styles.label}>
                門市名稱：
              </span>
              {store.name ? (
                <span style={styles.text}>
                  {store.name}
                </span>
              ) : null}
            </div>
            <div style={styles.fieldWrapper}>
              <span style={styles.label}>
                門市電話：
              </span>
              {store.phone ? (
                <span style={styles.text}>
                  {store.phone || null}
                </span>
              ) : null}
            </div>
            <div style={styles.fieldWrapper}>
              <span style={styles.label}>
                門市地址：
              </span>
              {store.address ? (
                <span style={styles.text}>
                  {store.address || null}
                </span>
              ) : null}
            </div>
            <div style={styles.fieldWrapper}>
              <span style={styles.label}>
                經緯度：
              </span>
              <span style={styles.text}>
                {store.latitude ? `${store.latitude}, ` : null}
                {store.longitude || null}
              </span>
            </div>
            <div style={styles.fieldWrapper}>
              <span style={styles.label}>
                區域：
              </span>
              {store.areaName ? (
                <span style={styles.text}>
                  {store.areaName || null}
                </span>
              ) : null}
            </div>
            <div style={styles.fieldWrapper}>
              <span style={styles.label}>
                通路：
              </span>
              {store.channelName ? (
                <span style={styles.text}>
                  {store.channelName || null}
                </span>
              ) : null}
            </div>
            <div style={styles.fieldWrapper}>
              <span style={styles.label}>
                縣市：
              </span>
              {store.cityName ? (
                <span style={styles.text}>
                  {store.cityName || null}
                </span>
              ) : null}
            </div>
            <div style={styles.fieldWrapper}>
              <span style={styles.label}>
                包膜師：
              </span>
              {store.employees && Array.isArray(store.employees) ? (
                <span style={styles.text}>
                  {store.employees.join('、') || null}
                </span>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default radium(StorePopup);
