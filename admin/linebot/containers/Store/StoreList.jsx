// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';

import { CACHE_LINEBOT_STORE_SEARCH } from '../../../actions/Search.js';
import { FORM_LINEBOT_EMPLOYEE_FORM } from '../../../shared/form.js';
import { FETCH_STORES } from '../../../queries/Global.js';
import { isPermissionAllowed } from '../../../helper/roles.js';
import { LINEBOT_STORE_PLACES_PAGE_KEYS } from '../../../../shared/selectorActionKeys.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import { wrapFormToStoreSearchForm } from '../Form/StoreSearchForm.jsx';
import Table, { TableField } from '../../../components/Table/Table.jsx';
import StoreManageActions from '../../../components/Table/Actions/linebot/StoreManageActions.jsx';
import StorePopup from './StorePopup.jsx';
import Button from '../../../components/Global/Button.jsx';

const StoreSearchForm = wrapFormToStoreSearchForm(FORM_LINEBOT_EMPLOYEE_FORM);

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    position: 'relative',
  },
  searchWrapper: {
    width: '100%',
    height: 245,
    padding: 16,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  tableWrapper: {
    width: '100%',
    height: 'calc(100% - 270px)',
    flex: 1,
    paddingTop: 12,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '6px 0',
  },
  collapseBtn: {
    position: 'absolute',
    top: 4,
    right: 10,
  },
};

type Props = {
  history: {
    replace: Function,
  },
  storeOptions: {
    cityId: string,
    storeId: string,
    keyword: string,
  },
};

type State = {
  currentStore: Object,
};

const LIMIT_LENGTH = 10;

class StoreList extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      hideSearchForm: false,
      currentStore: null,
    };
  }

  render() {
    const {
      history,
      storeOptions,
    } = this.props;

    const {
      currentStore,
      hideSearchForm,
    } = this.state;

    if (!isPermissionAllowed(LINEBOT_STORE_PLACES_PAGE_KEYS)) {
      history.replace('/linebot/login');
      return null;
    }

    const queryOptions = {
      limit: LIMIT_LENGTH,
      offset: 0,
      isForClient: true,
    };

    if (storeOptions) {
      const {
        cityId,
        storeId,
        keyword,
      } = storeOptions;

      if (cityId) queryOptions.cityId = parseInt(cityId, 10);
      if (storeId) queryOptions.storeId = parseInt(storeId, 10);
      if (keyword) queryOptions.keyword = keyword;
    }

    const wrapCacheOptions = storeOptions || {};

    return (
      <div style={styles.wrapper}>
        <PageTitle title="門市店點資訊" />
        <div style={styles.collapseBtn}>
          <Button
            onClick={() => this.setState({ hideSearchForm: !hideSearchForm })}
            label={hideSearchForm ? '展開' : '收合'} />
        </div>
        <StoreSearchForm
          hide={hideSearchForm}
          actionType={CACHE_LINEBOT_STORE_SEARCH}
          initialValue={{
            ...wrapCacheOptions,
          }} />
        <Query
          query={FETCH_STORES}
          variables={queryOptions}>
          {({
            data,
            fetchMore,
          }) => {
            const stores = (data && data.stores) || [];

            return (
              <div style={styles.tableWrapper}>
                <Table
                  dataSource={stores.map((i) => {
                    const cityName = i.district && i.district.city && `${i.district.city.name}${i.district.name}`;
                    return {
                      id: i.id,
                      name: i.name || null,
                      phone: i.phone || null,
                      address: i.address || null,
                      latitude: i.latitude || null,
                      longitude: i.longitude || null,
                      manager: i.manager || null,
                      areaName: (i.area && i.area.name) || null,
                      channelName: (i.channel && i.channel.name) || null,
                      cityName: cityName || null,
                      employees: (i.members && i.members.map(m => m.name)) || [],
                    };
                  })}
                  fetchMore={() => fetchMore({
                    variables: {
                      ...queryOptions,
                      offset: 0,
                      limit: stores.length + LIMIT_LENGTH,
                    },
                    updateQuery: (prev, { fetchMoreResult }) => {
                      if (!fetchMoreResult) return prev;

                      return {
                        ...prev,
                        stores: [
                          ...fetchMoreResult.stores,
                        ],
                      };
                    },
                  })}
                  getActions={() => [
                    <StoreManageActions
                      onClick={v => this.setState({
                        currentStore: {
                          id: v.id,
                          name: v.name,
                          phone: v.phone,
                          address: v.address,
                          latitude: v.latitude,
                          longitude: v.longitude,
                          areaName: v.areaName,
                          channelName: v.channelName,
                          cityName: v.cityName,
                          employees: v.employees,
                        },
                      })} />,
                  ]}
                  actionTitles={['操作']}
                  showPlaceholder={!stores.length}
                  placeholder="查無門市">
                  <TableField
                    name="門市名稱"
                    fieldKey="name"
                    flex={1}
                    isCenter />
                  <TableField
                    name="電話"
                    fieldKey="phone"
                    flex={1}
                    isCenter />
                </Table>
              </div>
            );
          }}
        </Query>
        {currentStore ? (
          <StorePopup
            store={currentStore}
            onClose={() => this.setState({ currentStore: null })} />
        ) : null}
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    storeOptions: state.Search.store,
  }),
);

export default reduxHook(
  radium(
    StoreList
  )
);
