// @flow
import React, { Component } from 'react';
import radium from 'radium';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Mutation } from 'react-apollo';
import moment from 'moment';
import {
  reduxForm,
  Field,
  Fields,
  initialize,
  SubmissionError,
  change,
} from 'redux-form';
import qs from 'qs';
// config
import { FORM_LINEBOT_SALES_FORM } from '../../../shared/form.js';
import { SALE_START } from '../../../mutations/Sales.js';
import { validateSalesForm } from '../../../helper/validator/LinebotSalesForm.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import SubmitButton from '../../../components/Form/SubmitButton.jsx';
import Input from '../../../components/Form/Input.jsx';
import Error from '../../../components/Form/Error.jsx';
import ChannelStoreSelectorBind from '../../components/Form/ChannelStoreSelectorBind.jsx';
import WorkingPopup from './WorkingPopup.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    position: 'relative',
  },
  mainWrapper: {
    width: '100%',
    height: '100%',
    padding: '12px 24px',
  },
  formPlacement: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formWrapper: {
    width: '100%',
    maxWidth: 414,
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

type Props = {
  handleSubmit: Function,
  initializeForm: Function,
  match: {
    params: {
      itemId: string,
    },
  },
  history: {
    push: Function,
  },
  location: {
    pathname: string,
  },
  match: {
    path: string,
  },
  setItemId: Function,
};

class SalesForm extends Component<Props> {
  state = {
    oriPathName: null,
  }

  componentDidMount() {
    const {
      match: {
        params: {
          itemId,
        },
        path,
      },
      initializeForm,
    } = this.props;

    initializeForm({
      areaId: '-1',
      cityId: '-1',
      channelId: '-1',
      storeId: '-1',
      vat: '',
      saleCode: '',
      itemId: itemId || '',
    });

    this.setState({
      oriPathName: path,
    });
  }

  render() {
    const {
      handleSubmit,
      history,
      setItemId,
    } = this.props;

    const {
      oriPathName,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        <PageTitle title="銷貨" />
        <div style={styles.mainWrapper}>
          <div style={styles.formPlacement}>
            <Mutation mutation={SALE_START}>
              {(saleStart, { loading, error }) => (
                <form
                  style={styles.formWrapper}
                  onSubmit={handleSubmit(async (d) => {
                    const errors = validateSalesForm(d);

                    if (errors) {
                      throw new SubmissionError(errors);
                    }

                    const {
                      data,
                    } = await saleStart({
                      variables: {
                        startTime: moment().format('YYYY-MM-DD HH:mm:ss'),
                        channelId: d.channelId ? parseInt(d.channelId, 10) : -1,
                        storeId: d.storeId ? parseInt(d.storeId, 10) : -1,
                        vat: d.vat || null,
                        saleCode: d.saleCode || null,
                        productCode: d.itemId || null,
                      },
                    });

                    if (data && data.saleStart && data.saleStart.status) {
                      const wrapId = data.saleStart.id || null;
                      const qsString = qs.stringify({
                        vat: d.vat || null,
                        saleCode: d.saleCode || null,
                        recordId: wrapId,
                      }, {
                        skipNulls: true,
                      });

                      history.push(`${oriPathName}/start?${qsString}`);
                    }
                  })}>
                  <Fields
                    names={[
                      'cityId',
                      'channelId',
                      'storeId',
                    ]}
                    component={ChannelStoreSelectorBind} />
                  <div style={styles.fieldWrapper}>
                    <Field
                      name="vat"
                      label="發票號碼："
                      placeholder="請填寫發票號碼"
                      component={Input} />
                  </div>
                  <div style={styles.fieldWrapper}>
                    <Field
                      name="saleCode"
                      label="銷貨編號："
                      placeholder="請填寫銷貨編號"
                      component={Input} />
                  </div>
                  <div style={styles.fieldWrapper}>
                    <Field
                      required
                      name="itemId"
                      label="商品序號："
                      placeholder="請填寫商品序號"
                      component={Input} />
                  </div>
                  {error ? (
                    <Error error={error.message || null} />
                  ) : null}
                  <div style={styles.functionWrapper}>
                    <SubmitButton
                      disabled={loading}
                      label="確認銷貨" />
                  </div>
                </form>
              )}
            </Mutation>
          </div>
        </div>
        <Switch>
          <Route
            path={`${oriPathName}/start`}
            component={props => (
              <WorkingPopup
                {...props}
                onClose={() => {
                  history.push(`${oriPathName}`);
                  setItemId('');
                }} />
            )} />
        </Switch>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: FORM_LINEBOT_SALES_FORM,
});

const reduxHook = connect(
  () => ({}),
  dispatch => bindActionCreators({
    initializeForm: v => initialize(FORM_LINEBOT_SALES_FORM, v),
    setItemId: v => change(FORM_LINEBOT_SALES_FORM, 'itemId', v),
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      SalesForm
    )
  )
);
