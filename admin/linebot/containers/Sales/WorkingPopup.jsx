// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import qs from 'qs';
import moment from 'moment';
import { Mutation } from 'react-apollo';

// static, config
import Theme from '../../../styles/Theme.js';
import closeIcon from '../../../static/images/close-icon.png';
import { SALE_STOP } from '../../../mutations/Sales.js';
// components
import Button from '../../../components/Global/Button.jsx';
import Error from '../../../components/Form/Error.jsx';

const styles = {
  wrapper: {
    position: 'fixed',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    padding: 12,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 999,
  },
  mainWrapper: {
    width: '100%',
    maxWidth: 414,
    height: 'auto',
    padding: '12px 24px',
    backgroundColor: '#fff',
    borderRadius: 3,
    border: `1px solid ${Theme.BLACK_COLOR}`,
    position: 'relative',
  },
  titleWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 15,
    fontWeight: 500,
    letterSpacing: 1,
    color: Theme.BLACK_COLOR,
  },
  closeBtn: {
    position: 'absolute',
    top: 12,
    right: 12,
    width: 24,
    height: 24,
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundImage: `url(${closeIcon})`,
    cursor: 'pointer',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '4px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  text: {
    fontSize: 13,
    color: Theme.BLACK_COLOR,
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '24px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  btnWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '12px 0',
  },
  btn: {
    width: 116,
  },
};

type Props = {
  location: {
    search: string,
  },
  onClose: Function,
};

class WorkingPopup extends PureComponent<Props> {
  async handleMutate(mutate) {
    const {
      onClose,
      location: {
        search,
      },
    } = this.props;

    const info = search ? qs.parse(search, { ignoreQueryPrefix: true }) : {};

    const {
      data,
    } = await mutate({
      variables: {
        endTime: moment().format('YYYY-MM-DD HH:mm:ss'),
        recordId: info.recordId ? parseInt(info.recordId, 10) : null,
      },
    });

    if (data && data.saleStop && data.saleStop.status) {
      alert('銷貨成功！');
      if (onClose) onClose();
    }
  }

  render() {
    const {
      location: {
        search,
      },
    } = this.props;

    const info = search ? qs.parse(search, { ignoreQueryPrefix: true }) : {};

    return (
      <div style={styles.wrapper}>
        <Mutation mutation={SALE_STOP}>
          {(saleStop, { loading, error }) => (
            <div style={styles.mainWrapper}>
              <button
                type="button"
                disabled={loading}
                onClick={() => this.handleMutate(saleStop)}
                style={styles.closeBtn} />
              <div style={styles.titleWrapper}>
                <span style={styles.title}>
                  包膜師服務中
                </span>
              </div>
              <div style={styles.fieldWrapper}>
                <span style={styles.text}>
                  發票號碼：
                </span>
                {info && info.vat ? (
                  <span style={styles.text}>
                    {info.vat}
                  </span>
                ) : null}
              </div>
              <div style={styles.fieldWrapper}>
                <span style={styles.text}>
                  銷貨編號：
                </span>
                {info && info.saleCode ? (
                  <span style={styles.text}>
                    {info.saleCode}
                  </span>
                ) : null}
              </div>
              {error ? (
                <Error error={error.message || null} />
              ) : null}
              <div style={styles.functionWrapper}>
                <div style={styles.btnWrapper}>
                  <Button
                    disabled={loading}
                    label="完成銷貨服務"
                    onClick={() => this.handleMutate(saleStop)}
                    style={styles.btn} />
                </div>
                <div style={styles.btnWrapper}>
                  <Button
                    isWhiteBg
                    disabled={loading}
                    label="連續銷貨"
                    onClick={() => this.handleMutate(saleStop)}
                    style={styles.btn} />
                </div>
              </div>
            </div>
          )}
        </Mutation>
      </div>
    );
  }
}

export default radium(WorkingPopup);
