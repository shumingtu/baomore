// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import AnnouncementList from '../containers/Announcement/AnnouncementList.jsx';

function Punch({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}`} component={AnnouncementList} />
    </Switch>
  );
}

export default Punch;
