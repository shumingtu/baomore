// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import SchedulePage from '../containers/Schedule/SchedulePage.jsx';

function Schedule({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}`} component={SchedulePage} />
    </Switch>
  );
}

export default Schedule;
