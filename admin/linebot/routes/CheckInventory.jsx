// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import CheckInventoryForm from '../containers/CheckInventory/CheckInventoryForm.jsx';
import CheckInventoryDetail from '../containers/CheckInventory/CheckInventoryDetail.jsx';

function CheckInventory({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}/detail/:itemId`} component={CheckInventoryDetail} />
      <Route path={`${url}/:itemId`} component={CheckInventoryForm} />
      <Route path={`${url}`} component={CheckInventoryForm} />
    </Switch>
  );
}

export default CheckInventory;
