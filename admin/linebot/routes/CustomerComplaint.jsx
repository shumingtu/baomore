// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';
// components
import SelectItemTypePage from '../containers/SelectItemTypePage.jsx';
import ComplaintMainBoard from '../containers/CustomerComplaint/ComplaintMainBoard.jsx';
import ComplaintActionPage from '../containers/CustomerComplaint/ComplaintActionPage.jsx';

function CustomerComplaint({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}/:pnCategoryType/:pnCategoryId/action/:pnTableId`} component={ComplaintActionPage} />
      <Route path={`${url}/:pnCategoryType/:pnCategoryId`} component={ComplaintMainBoard} />
      <Route path={`${url}`} component={props => <SelectItemTypePage {...props} title="客訴退回" />} />
    </Switch>
  );
}

export default CustomerComplaint;
