// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';
// components
import SelectItemTypePage from '../containers/SelectItemTypePage.jsx';
import PurchaseMainBoard from '../containers/Purchase/PurchaseMainBoard.jsx';

function Purchase({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}/:pnCategoryType/:pnCategoryId`} component={PurchaseMainBoard} />
      <Route path={`${url}`} component={props => <SelectItemTypePage {...props} title="訂貨查詢" />} />
    </Switch>
  );
}

export default Purchase;
