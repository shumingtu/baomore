// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import EmployeeInfoList from '../containers/Employee/EmployeeInfoList.jsx';

function Employee({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}`} component={EmployeeInfoList} />
    </Switch>
  );
}

export default Employee;
