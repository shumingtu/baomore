// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import SalesForm from '../containers/Sales/SalesForm.jsx';

function Sales({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}/:itemId`} component={SalesForm} />
      <Route path={`${url}`} component={SalesForm} />
    </Switch>
  );
}

export default Sales;
