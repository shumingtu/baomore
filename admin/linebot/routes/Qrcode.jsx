// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import QrcodeMainBoard from '../containers/Qrcode/QrcodeMainBoard.jsx';

function Qrcode({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}`} component={QrcodeMainBoard} />
    </Switch>
  );
}

export default Qrcode;
