// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';
// components
import SelectItemTypePage from '../containers/SelectItemTypePage.jsx';
import TransferMainBoard from '../containers/Transfer/TransferMainBoard.jsx';
import TransferActionWrapper from '../containers/Transfer/TransferActionWrapper.jsx';
import TransferQrcodeActionWrapper from '../containers/Transfer/TransferQrcodeActionWrapper.jsx';

function Transfer({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}/action/:qrcode`} component={TransferQrcodeActionWrapper} />
      <Route path={`${url}/:pnCategoryType/:pnCategoryId/action/:pnTableId`} component={TransferActionWrapper} />
      <Route path={`${url}/:pnCategoryType/:pnCategoryId`} component={TransferMainBoard} />
      <Route path={`${url}`} component={props => <SelectItemTypePage {...props} title="調貨" />} />
    </Switch>
  );
}

export default Transfer;
