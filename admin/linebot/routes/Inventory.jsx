// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

// components
import SelectItemTypePage from '../containers/SelectItemTypePage.jsx';
import InventoryMainBoard from '../containers/Inventory/InventoryMainBoard.jsx';

function Inventory({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}/:pnCategoryType/:pnCategoryId`} component={InventoryMainBoard} />
      <Route path={`${url}`} component={props => <SelectItemTypePage {...props} title="存貨查詢" />} />
    </Switch>
  );
}

export default Inventory;
