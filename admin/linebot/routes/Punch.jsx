// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import PunchPage from '../containers/Punch/PunchPage.jsx';
import PunchListPage from '../containers/Punch/PunchListPage.jsx';
import PunchCheckEntryPage from '../containers/Punch/PunchCheckEntryPage.jsx';
import PunchCheckFormPage from '../containers/Punch/PunchCheckFormPage.jsx';

function Punch({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}/check/store/:storeId`} component={PunchCheckFormPage} />
      <Route path={`${url}/check`} component={PunchCheckEntryPage} />
      <Route path={`${url}/list`} component={PunchListPage} />
      <Route path={`${url}`} component={PunchPage} />
    </Switch>
  );
}

export default Punch;
