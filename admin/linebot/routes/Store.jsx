// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import StoreList from '../containers/Store/StoreList.jsx';

function Store({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}`} component={StoreList} />
    </Switch>
  );
}

export default Store;
