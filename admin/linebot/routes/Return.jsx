// @flow
import React, { Fragment } from 'react';
import {
  Route,
  Switch,
} from 'react-router';
import { returnCategoryTypes } from '../../shared/linebot.js';
// components
// import SelectItemTypePage from '../containers/SelectItemTypePage.jsx';
// import ReturnMainBoard from '../containers/Return/ReturnMainBoard.jsx';
import ReturnTypeSelectPage from '../containers/Return/ReturnTypeSelectPage.jsx';
import ReturnActionMainBoard from '../containers/Return/ReturnActionMainBoard.jsx';

function Return({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      {returnCategoryTypes.map(rc => (
        <Route
          key={`${rc.id}-with-itemId`}
          path={`${url}/type/${rc.id}/:itemId`}
          component={props => (
            <ReturnActionMainBoard
              {...props}
              errorType={rc.id} />
          )} />
      ))}
      {returnCategoryTypes.map(rc => (
        <Route
          key={`${rc.id}-without-itemId`}
          path={`${url}/type/${rc.id}`}
          component={props => (
            <ReturnActionMainBoard
              {...props}
              errorType={rc.id} />
          )} />
      ))}
      {/* removed at 2019-04-02 | rollback don't need pnTable
        <Route
          path={`${url}/:pnCategoryType/:pnCategoryId/action/:pnTableId`}
          component={ReturnTypeSelectPage} />
        <Route path={`${url}/:pnCategoryType/:pnCategoryId`} component={ReturnMainBoard} />
      */}
      <Route path={`${url}`} component={ReturnTypeSelectPage} />
    </Switch>
  );
}

export default Return;
