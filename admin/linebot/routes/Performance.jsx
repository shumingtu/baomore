// @flow
import React from 'react';
import {
  Route,
  Switch,
} from 'react-router';

import PerformancePage from '../containers/Performance/PerformancePage.jsx';

function Performance({
  match: {
    url,
  },
}: {
  match: {
    url: string,
  },
}) {
  return (
    <Switch>
      <Route path={`${url}`} component={PerformancePage} />
    </Switch>
  );
}

export default Performance;
