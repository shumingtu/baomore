// @flow
import React from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as MemberActions from '../actions/Member.js';
import { history } from '../global.js';
import Theme from '../styles/Theme.js';
import logo from '../static/images/artmo-logo.png';

const styles = {
  wrapper: {
    width: '100%',
    height: 50,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Theme.MAIN_THEME_COLOR,
    position: 'relative',
  },
  headerTextWrapper: {
    width: 'auto',
    height: 'auto',
    border: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    textDecoration: 'none',
  },
  logo: {
    width: 45,
    height: 45,
    '@media (max-width: 413px)': {
      width: 30,
      height: 30,
    },
  },
  title: {
    fontSize: 15,
    fontWeight: 400,
    letterSpacing: 1,
    color: '#FFF',
    padding: '0 12px',
    '@media (max-width: 413px)': {
      fontSize: 13,
    },
  },
  logoutBtn: {
    position: 'absolute',
    right: 6,
    top: 12,
    width: 40,
    height: 26,
    textAlign: 'center',
    fontSize: 13,
    fontWeight: 300,
    color: 'rgba(255, 255, 255, 0.8)',
    lineHeight: '25px',
    display: 'block',
    backgroundColor: 'transparent',
    border: 0,
    padding: 0,
    margin: 0,
    outline: 0,
    cursor: 'pointer',
  },
};

type Props = {
  memberLogout: Function,
};

function SiteHeader({
  memberLogout,
}: Props) {
  return (
    <div style={styles.wrapper}>
      <div style={styles.headerTextWrapper}>
        <img
          alt="artmo-logo"
          src={logo}
          style={styles.logo} />
        <span style={styles.title}>
          Artmo 行動管理系統
        </span>
      </div>
      <button
        key="header-logout-btn"
        type="button"
        onClick={() => {
          memberLogout();
          history.push('/linebot/login');
        }}
        style={styles.logoutBtn}>
        登出
      </button>
    </div>
  );
}

const reduxHook = connect(
  () => ({}),
  dispatch => bindActionCreators({
    ...MemberActions,
  }, dispatch)
);

export default reduxHook(
  radium(
    SiteHeader
  )
);
