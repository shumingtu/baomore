// @flow
/* eslint no-underscore-dangle: 0, no-console: 0 */
import 'cross-fetch/polyfill';
import ApolloClient from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory/lib/inMemoryCache';
import { ApolloLink } from 'apollo-link';
import jwtDecode from 'jwt-decode';
import { HttpLink } from 'apollo-link-http';

import {
  MEMBER_LOGOUT,
} from './actions/Member.js';

const httpLink = new HttpLink({
  uri: `${API_HOST}/graphql`,
});

export function createClient(store) {
  const cache = new InMemoryCache();

  const authMiddleware = new ApolloLink((operation, forward) => {
    const headers = {};

    const token = store.getState().Member.accessToken || null;

    if (token) {
      const decodedToken = jwtDecode(token);
      const isValidToken = decodedToken.exp * 1000 > Date.now();

      headers.authorization = `Bearer ${token}`;

      if (!isValidToken) {
        return store.dispatch({
          type: MEMBER_LOGOUT,
        });
      }
    }

    operation.setContext({
      headers,
    });

    return forward(operation);
  });

  const client = new ApolloClient({
    link: ApolloLink.from([
      authMiddleware,
      httpLink,
    ]),
    cache,
    dataIdFromObject: o => `${o.id}${o.__typename}`,
  });

  return client;
}

export default createClient;
