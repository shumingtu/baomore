import gql from 'graphql-tag';

export const FETCH_MY_SCHEDULES = gql`
  query meScheduleDates {
    meScheduleDates
  }
`;

export const FETCH_SCHEDULE_DATE_ASSIGNMENT = gql`
  query meScheduleDateAssignments($date: String) {
    meScheduleDateAssignments(
      date: $date
    ) {
      time
      order {
        id
        name
        phone
        date
        time
        email
        store {
          id
          name
        }
        shippingStatus
      }
    }
  }
`;
