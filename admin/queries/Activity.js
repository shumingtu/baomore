import gql from 'graphql-tag';

export const FETCH_SOCIAL_ACTIVITY_CATEGORIES = gql`
  query socialCategories($id: Int) {
    socialCategories(id: $id) {
      id
      name
      link
    }
  }
`;

export const FETCH_ACTIVITY_CATEGORIES = gql`
  query activityCategories {
    activityCategories {
      id
      name
      desktopImg
      mobileImg
    }
  }
`;

export const FETCH_ACTIVITY_CATEGORY = gql`
  query activityCategory($categoryId: Int!) {
    activityCategory(
      categoryId: $categoryId
    ) {
      id
      name
      desktopImg
      mobileImg
    }
  }
`;

export const FETCH_ACTIVITIES = gql`
  query activities($categoryId: Int!) {
    activities(
      categoryId: $categoryId
    ) {
      id
      name
      desktopImg
      mobileImg
      description
      isActived
    }
  }
`;

export const FETCH_ACTIVITY_DETAIL = gql`
  query activity($activityId: Int!) {
    activity(
      activityId: $activityId
    ) {
      id
      name
      desktopImg
      mobileImg
      description
      fragments {
        id
        type
        title
        content
      }
    }
  }
`;
