import gql from 'graphql-tag';

export const FETCH_ANNOUNCEMENT_LIST = gql`
  query announcements(
    $keyword: String
    $startDate: String
    $endDate: String
    $limit: Int
    $offset: Int
  ) {
    announcements (
      keyword: $keyword
      startDate: $startDate
      endDate: $endDate
      limit: $limit
      offset: $offset
    ) {
      id
      title
      content
      createdAt
    }
  }
`;

export const FETCH_ANNOUNCEMENT_DETAIL = gql`
  query announcement($id: Int!) {
    announcement(
      id: $id
    ) {
      id
      title
      content
      createdAt
    }
  }
`;

export default null;
