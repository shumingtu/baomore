import gql from 'graphql-tag';

export const FETCH_ORDER_QRCODES = gql`
  query orderQRCodeList($orderId: Int!) {
    orderQRCodeList(orderId: $orderId) {
      id
      qrcode
    }
  }
`;

export const FETCH_BOOK_TOME_SLOT_LIST = gql`
  query bookTimeSlotList {
    bookTimeSlotList {
      time
    }
  }
`;

export const FETCH_SCHEDULE_LIST = gql`
  query schedulelist(
    $startDate: String,
    $endDate: String,
    $storeId: Int,
    $bookedMemberId: Int,
    $limit: Int,
    $offset: Int
  ) {
    schedulelist(
      startDate: $startDate,
      endDate: $endDate,
      storeId: $storeId,
      bookedMemberId: $bookedMemberId,
      limit: $limit,
      offset: $offset,
    ) {
      date
      assignments {
        time
        denominator
        onlineOrders {
          id
          name
          phone
          email
          store {
            id
            name
          }
          date
          time
          employee {
            id
            name
          }
        }
      }
    }
  }
`;

export const FETCH_EMPLOYEE_LIST = gql`
  query employeeMemberlist($storeId: Int, $memberId: Int, $areaId: Int) {
    employeeMemberlist(storeId: $storeId, memberId: $memberId, areaId: $areaId) {
      id
      name
      area {
        id
        name
      }
    }
  }
`;

export const FETCH_ONLINEORDER_LIST = gql`
  query onlineOrderlist(
    $keyword: String
    $startDate: String
    $endDate: String
    $payedStatus: String
    $shippingStatus: String
    $storeId: Int,
    $bookedMemberId: Int
    $limit: Int
    $offset: Int
  ) {
    onlineOrderlist(
      keyword: $keyword
      startDate: $startDate
      endDate: $endDate
      payedStatus: $payedStatus
      shippingStatus: $shippingStatus
      storeId: $storeId
      bookedMemberId: $bookedMemberId
      limit: $limit
      offset: $offset
    ) {
      id
      picture
      name
      phone
      email
      store {
        id
        name
      }
      time
      employee {
        id
        name
      }
      date
      payedStatus
      createdAt
      shippingStatus
      isShared
      order {
        id
        orderSN
      }
    }
  }
`;

export const FETCH_SINGLE_ORDER = gql`
  query onlineOrder($onlineOrderId: String) {
    onlineOrder(
      onlineOrderId: $onlineOrderId
    ) {
      id
      picture
      name
      phone
      email
      store {
        id
        name
      }
      time
      employee {
        id
        name
      }
      date
      payedStatus
      createdAt
      shippingStatus
    }
  }
`;

export const GET_ALREADY_BOOKED_DATES = gql`
  query alreadyBookedDates($storeId: Int!, $employeeId: Int) {
    alreadyBookedDates(
      storeId: $storeId
      employeeId: $employeeId
    ) {
      date
    }
  }
`;

export const GET_AVAILABLE_BOOK_TIMES = gql`
  query availableBookedTimes($storeId: Int!, $date: String!, $employeeId: Int) {
    availableBookedTimes(
      storeId: $storeId
      date: $date
      employeeId: $employeeId
    ) {
      time
    }
  }
`;

export const GET_ORDER_LIST = gql`
  query orderList(
    $memberId: Int
    $status: String
    $keyword: String
    $limit: Int
    $offset: Int
    $orderSN: String
    $closeDate: String
    $qrcode: String
  ) {
    orderList(
      memberId: $memberId
      status: $status
      keyword: $keyword
      limit: $limit
      offset: $offset
      orderSN: $orderSN
      closeDate: $closeDate
      qrcode: $qrcode
    ) {
      onlineOrder {
        id
      }
      id
      quantity
      status
      orderSN
      Member {
        id
        name
      }
      pnTable {
        id
        picture
        name
        code
        pnCategory {
          id
          name
        }
        bigSubCategory {
          id
          name
        }
        smallSubCategory {
          id
          name
        }
        name
        price
        onlinePrice
        vendor {
          id
          name
        }
        suitableDevice
        description
      }
      store {
        id
        name
        channel {
          id
          name
        }
        district {
          id
          name
          city {
            id
            name
          }
        }
      }
      orderGropShipment {
        id
        createdAt
      }
    }
  }
`;

export const GET_MY_ORDER_LIST = gql`
  query orderListByEmployee(
    $status: String
    $keyword: String
    $orderSN: String
    $closeDate: String,
    $limit: Int
    $offset: Int
    $qrcode: String
  ) {
    orderListByEmployee(
      status: $status
      keyword: $keyword
      orderSN: $orderSN
      closeDate: $closeDate,
      qrcode: $qrcode
      limit: $limit
      offset: $offset
    ) {
      id
      quantity
      status
      orderSN
      pnTable {
        id
        code
        name
      }
      store {
        id
        name
        channel {
          id
          name
        }
        district {
          id
          name
          city {
            id
            name
          }
        }
      }
      orderGropShipment {
        id
        createdAt
      }
    }
  }
`;
