import gql from 'graphql-tag';

export const FETCH_BEHAVIOR_LIST = gql`
  query behaviorList {
    behaviorList {
      id
      name
    }
  }
`;

export const FETCH_RECORD_LIST = gql`
  query recordList(
    $behaviorId: Int,
    $employeeId: Int,
    $startDate: String,
    $endDate: String,
    $recordStartDate: String,
    $recordEndDate: String,
    $limit: Int,
    $offset: Int,
  ) {
    recordList(
      behaviorId: $behaviorId,
      employeeId: $employeeId,
      startDate: $startDate,
      endDate: $endDate,
      recordStartDate: $recordStartDate,
      recordEndDate: $recordEndDate,
      limit: $limit,
      offset: $offset,
    ) {
      id
      behavior {
        id
        name
      }
      member {
        id
        name
      }
      remark
      qrcode
      orderSN
      startTime
      endTime
      price
      createdAt
    }
  }
`;

export default null;
