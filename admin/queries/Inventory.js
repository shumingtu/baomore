import gql from 'graphql-tag';

export const FETCH_STOCK_LIST = gql`
  query stockList(
    $memberId: Int,
    $keyword: String,
    $limit: Int,
    $offset: Int,
  ) {
    stockList(
      memberId: $memberId,
      keyword: $keyword,
      limit: $limit,
      offset: $offset,
    ) {
      id
      storageCount
      member {
        id
        name
      }
      pnTable {
        id
        picture
        code
        name
      }
    }
  }
`;

export const FETCH_INVENTORIES = gql`
  query inventories(
    $pnCategoryId: Int
    $brandId: Int
    $brandModelId: Int
    $keyword: String
    $bigSubCategoryId: Int
    $smallSubCategoryId: Int
    $limit: Int
    $offset: Int
    $employeeId: Int
    $isForPurchase: Boolean,
  ) {
    inventories(
      pnCategoryId: $pnCategoryId
      brandId: $brandId
      brandModelId: $brandModelId
      keyword: $keyword
      bigSubCategoryId: $bigSubCategoryId
      smallSubCategoryId: $smallSubCategoryId
      limit: $limit
      offset: $offset
      employeeId: $employeeId
      isForPurchase: $isForPurchase
    ) {
      id
      pnTable {
        id
        name
      }
      amount
      orderingOrder {
        id
        quantity
      }
    }
  }
`;

export const FETCH_MY_INVENTORY_DETAIL = gql`
  query meInventory($pnTableId: Int!, $employeeId: Int) {
    meInventory(
      pnTableId: $pnTableId
      employeeId: $employeeId
    ) {
      id
      pnTable {
        id
        code
        name
        picture
        suitableDevice
        description
        isOnSale
        price
        onlinePrice
        pnCategory {
          id
          name
        }
        bigSubCategory {
          id
          name
        }
        smallSubCategory {
          id
          name
        }
        vendor {
          id
          name
        }
        BrandModel {
          id
          name
        }
        Brand {
          id
          name
        }
      }
      amount
      orderingOrder {
        id
        quantity
        status
        store {
          id
          name
          channel {
            id
            name
          }
          district {
            id
            name
            city {
              id
              name
            }
          }
        }
      }
    }
  }
`;

export const FETCH_INVENTORY_ROLLBACKS = gql`
  query behaviorRecordList(
    $rollbackType: String!
    $pnCategoryId: Int
    $startDate: String
    $endDate: String
    $code: String
    $memberId: Int
    $errorType: String
    $target: String
    $storeId: Int
    $vat: String
    $customerName: String
    $customerPhone: String
    $limit: Int
    $offset: Int
  ) {
    behaviorRecordList(
      rollbackType: $rollbackType
      pnCategoryId: $pnCategoryId
      startDate: $startDate
      endDate: $endDate
      code: $code
      memberId: $memberId
      errorType: $errorType
      target: $target
      storeId: $storeId
      vat: $vat
      customerName: $customerName
      customerPhone: $customerPhone
      limit: $limit
      offset: $offset
    ) {
      id
      rollbackType
      picture
      remarks
      createdAt
      member {
        id
        name
      }
      vendor {
        id
        name
      }
      store {
        id
        name
      }
      pnTable {
        id
        code
        name
      }
      pnCategory {
        id
        name
      }
      approver {
        id
        name
      }
    }
  }
`;

export default null;
