import gql from 'graphql-tag';

export const FETCH_CLOSEDORDERSETTING_LIST = gql`
  query closedOrderSettings {
    closedOrderSettings {
      id
      closedDay
      closedTime
      PNTable {
        id
        code
      }
    }
  }
`;

export const FETCH_SINGLE_CLOSEDORDERSETTING = gql`
  query singleClosedOrderSetting($id: Int!) {
    singleClosedOrderSetting(id: $id) {
      id
      closedDay
      closedTime
      PNTable {
        id
        code
      }
    }
  }
`;

export default null;
