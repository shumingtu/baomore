import gql from 'graphql-tag';

export const FETCH_AREA_LIST = gql`
  query areaList {
    areaList {
      id
      name
    }
  }
`;

export default null;
