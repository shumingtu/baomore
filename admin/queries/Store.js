import gql from 'graphql-tag';

export const FETCH_LOCATION_INFO = gql`
  query locationInfo($address: String!) {
    locationInfo(address: $address) {
      lat
      lon
    }
  }
`;

export const FETCH_STORES_LIST = gql`
  query stores($storeId: Int, $channelId: Int, $districtId: Int, $limit: Int, $offset: Int, $isForClient: Boolean, $keyword: String) {
    stores(
      storeId: $storeId,
      channelId: $channelId,
      districtId: $districtId,
      limit: $limit,
      offset: $offset,
      isForClient: $isForClient,
      keyword: $keyword,
    ) {
      id
      name
      phone
      address
      manager
      latitude
      longitude
      archive
      area {
        id
        name
      }
      channel {
        id
        name
      }
      members {
        memberId: id
        name
        serialNumber
        memberStore {
          type
        }
      }
      district {
        id
        name
        city {
          id
          name
        }
      }
    }
  }
`;

export default null;
