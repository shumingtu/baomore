import gql from 'graphql-tag';

export const FETCH_BRAND_LIST = gql`
  query brandList($keyword: String) {
    brandList(keyword: $keyword) {
      id
      name
    }
  }
`;

export const FETCH_BRAND_MODEL_LIST = gql`
  query brandModelList($brandId: Int!, $keyword: String) {
    brandModelList(
      brandId: $brandId
      keyword: $keyword
    ) {
      id
      name
    }
  }
`;

export const FETCH_MODEL_COLOR_LIST = gql`
  query brandModelColorList($modelId: Int!, $keyword: String) {
    brandModelColorList(
      modelId: $modelId
      keyword: $keyword
    ) {
      id
      name
      phoneBg
      phoneMask
      phoneCover
    }
  }
`;

export const FETCH_ADMIN_PHONE_LIST = gql`
  query adminBrandModelColorList(
    $brandId: Int
    $modelId: Int
    $colorId: Int
    $limit: Int
    $offset: Int
  ) {
    adminBrandModelColorList(
      brandId: $brandId
      modelId: $modelId
      colorId: $colorId
      limit: $limit
      offset: $offset
    ) {
      id
      name
      brand {
        id
        name
      }
      model {
        id
        name
      }
      phoneBg
      phoneMask
      phoneCover
    }
  }
`;

export const FETCH_SINGLE_PHONE_DETAIL = gql`
  query brandModelColor($id: Int!) {
    brandModelColor(
      id: $id
    ) {
      id
      name
      phoneBg
      phoneMask
      phoneCover
      brand {
        id
        name
      }
      model {
        id
        name
      }
    }
  }
`;
