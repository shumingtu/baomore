import gql from 'graphql-tag';

export const FETCH_VENDOR_LIST = gql`
  query vendorList($id: Int, $limit: Int, $offset: Int) {
    vendorList(id: $id, limit: $limit, offset: $offset) {
      id
      name
      contactPersonName
      phone
      address
      VendorSettings {
        id
        type
        value
        MemberId
        message
      }
    }
  }
`;

export const FETCH_CITY_DISTRICTS = gql`
  query cityDistricts {
    cityDistricts {
      id
      name
      latitude
      longitude
      disticts {
        id
        name
      }
    }
  }
`;

export const FETCH_STORES = gql`
  query stores(
    $cityId: Int,
    $storeId: Int,
    $districtId: Int,
    $channelId: Int
    $keyword: String,
    $limit: Int,
    $isForClient: Boolean,
    $offset: Int,
    $lat: Float,
    $lon: Float,
  ) {
    stores(
      cityId: $cityId
      storeId: $storeId
      districtId: $districtId
      channelId: $channelId
      keyword: $keyword
      isForClient: $isForClient,
      limit: $limit,
      offset: $offset,
      lat: $lat,
      lon: $lon,
    ) {
      id
      name
      phone
      address
      latitude
      longitude
      manager
      area {
        id
        name
      }
      channel {
        id
        name
      }
      members {
        id
        name
      }
      district {
        id
        name
        city {
          id
          name
        }
      }
    }
  }
`;

export const FETCH_AREAS = gql`
  query areaList {
    areaList {
      id
      name
    }
  }
`;

export const FETCH_CHANNEL_LIST = gql`
  query channelList($isPurchaseChannel: Boolean) {
    channelList(isPurchaseChannel: $isPurchaseChannel) {
      id
      name
    }
  }
`;

export default null;
