import gql from 'graphql-tag';

export const CHECK_INFO = gql`
  query checkInfo($checkCode: String!) {
    checkInfo(
      checkCode: $checkCode
    ) {
      id
      pnTable {
        id
        code
        name
        picture
        description
        pnCategory {
          id
          name
        }
        bigSubCategory {
          id
          name
        }
        smallSubCategory {
          id
          name
        }
      }
      amount
    }
  }
`;

export default null;
