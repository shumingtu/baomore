import gql from 'graphql-tag';

export const FETCH_ACTION_LIST = gql`
  query actionList {
    actionList {
      code
      name
    }
  }
`;

export default null;
