import gql from 'graphql-tag';

export const FETCH_PUNCH_RECORDS = gql`
  query punchlist(
    $startDate: String
    $endDate: String
    $employeeId: Int
    $storeId: Int
    $limit: Int
    $offset: Int
  ) {
    punchlist(
      startDate: $startDate
      endDate: $endDate
      employeeId: $employeeId
      storeId: $storeId
      limit: $limit
      offset: $offset
    ) {
      id
      punchTime
      store {
        id
        name
        phone
        address
        latitude
        longitude
        manager
      }
    }
  }
`;

export const FETCH_ADMIN_PUNCH_RECORDS = gql`
  query adminPunchList(
    $startDate: String
    $endDate: String
    $employeeId: Int
    $limit: Int
    $offset: Int
  ) {
    adminPunchList(
      startDate: $startDate
      endDate: $endDate
      employeeId: $employeeId
      limit: $limit
      offset: $offset
    ) {
      id
      punchTime
      store {
        id
        name
      }
      type
      member {
        id
        name
      }
    }
  }
`;

export default null;
