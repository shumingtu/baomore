import gql from 'graphql-tag';

export const FETCH_LANDING_BANNERS = gql`
  query banners($id: Int) {
    banners (
      id: $id
    ) {
      id
      picture
      link
      mobileImg
    }
  }
`;

export const FETCH_LANDINGS = gql`
  query landings($id: Int) {
    landings(
      id: $id
    ) {
      id
      desktopImg
      mobileImg
      link
    }
  }
`;
