import gql from 'graphql-tag';

export const FETCH_ROLE_LIST = gql`
  query roleList($limit: Int, $offset: Int) {
    roleList(
      limit: $limit,
      offset: $offset,
    ) {
      id
      name
      actions {
        code
        name
      }
    }
  }
`;

export default null;
