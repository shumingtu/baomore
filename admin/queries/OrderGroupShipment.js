import gql from 'graphql-tag';

export const FETCH_ORDER_GROUP_SHIPMENT_LIST = gql`
  query orderGroupShipmentList(
    $startDate: String
    $endDate: String
    $shippingStatus: String
    $vendorId: Int
    $memberId: Int
    $areaId: Int
    $code: String,
    $orderSN: String,
    $limit: Int
    $offset: Int
  ) {
    orderGroupShipmentList(
      startDate: $startDate
      endDate: $endDate
      shippingStatus: $shippingStatus
      vendorId: $vendorId
      memberId: $memberId
      areaId: $areaId
      code: $code
      orderSN: $orderSN
      limit: $limit
      offset: $offset
    ) {
      id
      SN
      status
      startDate
      endDate
      totalQuantity
      PNTable {
        id
        code
        name
        picture
        suitableDevice
        description
        isOnSale
        price
        onlinePrice
        pnCategory {
          id
          name
        }
        bigSubCategory {
          id
          name
        }
        smallSubCategory {
          id
          name
        }
        vendor {
          id
          name
        }
      }
      Orders {
        id
        quantity
        orderSN
        orderSource
        Member {
          id
          name
          area {
            id
            name
          }
        }
      }
    }
  }
`;

export default null;
