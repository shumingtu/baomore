import gql from 'graphql-tag';

export const FETCH_CITY_LIST = gql`
  query cityDistricts {
    cityDistricts {
      id
      name
    }
  }
`;

export const FETCH_DISTRICT_LIST = gql`
  query districts($cityId: Int) {
    districts(cityId: $cityId) {
      id
      name
    }
  }
`;
