import gql from 'graphql-tag';

export const FETCH_ALL_MEMBER_LIST = gql`
  query allMemberList($memberId: Int, $keyword: String, $limit: Int, $offset: Int, $roleId: Int) {
    allMemberList(memberId: $memberId, keyword: $keyword, limit: $limit, offset: $offset, roleId: $roleId) {
      id
      serialNumber
      name
      lineId
      email
      gender
      birthday
      lineAccount
      phone
      area {
        id
        name
      }
      director {
        id
        name
      }
      lineId
      roles {
        id
        name
      }
      archive
    }
  }
`;

export const GET_MEMBER_ME = gql`
  query me {
    me {
      id
      name
      phone
      AreaId
    }
  }
`;

export const GET_EMPLOYEE_LIST = gql`
  query employeeMemberlist(
    $memberId: Int,
    $storeId: Int,
    $areaId: Int,
    $employeeType: String,
    $limit: Int,
    $offset: Int,
    $keyword: String,
    $directorId: Int,
    $lineIdRequired: Boolean,
    $isForClient: Boolean,
  ) {
    employeeMemberlist(
      memberId: $memberId
      storeId: $storeId
      employeeType: $employeeType
      limit: $limit
      offset: $offset
      areaId: $areaId
      keyword: $keyword
      directorId: $directorId
      lineIdRequired: $lineIdRequired
      isForClient: $isForClient
    ) {
      id
      archive
      serialNumber
      lineId
      lineAccount
      birthday
      name
      serialNumber
      phone
      email
      gender
      director {
        id
        name
        serialNumber
      }
      area {
        id
        name
      }
      memberWarranties {
        id
        service
        phoneModel
        store
        VAT
        IME
        phoneLastSix
        isApproved
        employeeCode
        createdAt
      }
      onlineOrders {
        id
        picture
        name
        phone
        email
        date
        time
        price
        store {
          id
          name
          phone
          address
        }
        employee {
          id
          name
        }
        createdAt
        payedStatus
        shippingStatus
      }
    }
  }
`;

export default null;
