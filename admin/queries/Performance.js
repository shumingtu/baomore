import gql from 'graphql-tag';

export const FETCH_PERFORMANCE = gql`
  query performance(
    $startDate: String
    $endDate: String
    $employeeId: Int
  ) {
    performance(
      startDate: $startDate
      endDate: $endDate
      employeeId: $employeeId
    ) {
      id
      targets
      amounts
      sales
      achieveRate
      asp
      mom
      workHours
      expectAchieves
      expectProfits
    }
  }
`;

export const FETCH_STORE_PERFORMANCE_FOR_ADMIN = gql`
  query storePerformanceForAdmin(
    $startDate: String!
    $endDate: String!
    $storeId: Int
    $channelId: Int
  ) {
    storePerformanceForAdmin(
      startDate: $startDate
      endDate: $endDate
      storeId: $storeId
      channelId: $channelId
    ) {
      id
      name
      channel {
        id
        name
      }
      targets
      amounts
      sales
      achieveRate
      asp
      mom
      workHours
      expectAchieves
      expectProfits
      turnover
      commissions
    }
  }
`;

export const FETCH_EMPLOYEE_PERFORMANCE_FOR_ADMIN = gql`
  query employeePerformanceForAdmin(
    $startDate: String!
    $endDate: String!
    $employeeId: Int
    $areaId: Int
    $directorId: Int,
  ) {
    employeePerformanceForAdmin(
      startDate: $startDate
      endDate: $endDate
      employeeId: $employeeId
      areaId: $areaId
      directorId: $directorId
    ) {
      id
      name
      area {
        id
        name
      }
      targets
      amounts
      sales
      achieveRate
      asp
      mom
      workHours
      expectAchieves
      expectProfits
      turnover
      commissions
    }
  }
`;

export default null;
