import gql from 'graphql-tag';

export const FETCH_CHANNEL_LIST = gql`
  query channelList {
    channelList {
      id
      name
    }
  }
`;

export default null;
