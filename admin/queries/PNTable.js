import gql from 'graphql-tag';

export const FETCH_PNTABLE_CODE = gql`
  query pnTableCodeList($code: String) {
    pnTableCodeList(code: $code) {
      id
      code
    }
  }
`;

export const FETCH_PN_ITEM = gql`
  query pnTableItem($id: Int!) {
    pnTableItem(id: $id) {
      id
      code
      name
      picture
      suitableDevice
      description
      isOnSale
      price
      onlinePrice
      pnCategory {
        id
        name
      }
      bigSubCategory {
        id
        name
      }
      smallSubCategory {
        id
        name
      }
      vendor {
        id
        name
      }
      BrandModel {
        id
        name
      }
      Brand {
        id
        name
      }
    }
  }
`;

export const FETCH_PN_LIST = gql`
  query pntableList(
    $code: String
    $pnCategoryId: Int
    $bigSubCategoryId: Int
    $smallSubCategoryId: Int
    $keyword: String
    $vendorId: Int
    $brandId: Int
    $modelId: Int
    $limit: Int
    $offset: Int
  ) {
    pntableList(
      code: $code
      pnCategoryId: $pnCategoryId
      bigSubCategoryId: $bigSubCategoryId
      smallSubCategoryId: $smallSubCategoryId
      keyword: $keyword
      vendorId: $vendorId
      limit: $limit
      offset: $offset
      brandId: $brandId
      modelId: $modelId
    ) {
      picture
      id
      code
      pnCategory {
        id
        name
      }
      bigSubCategory {
        id
        name
      }
      smallSubCategory {
        id
        name
      }
      name
      price
      onlinePrice
      vendor {
        id
        name
      }
      suitableDevice
      description
      BrandModel {
        id
        name
      }
      Brand {
        id
        name
      }
    }
  }
`;

export const FETCH_SINGLE_PNSUBCATEGORY = gql`
  query pnSubCategoryItem($id: Int!) {
    pnSubCategoryItem(id: $id) {
      id
      name
      picture
      isOnSale
    }
  }
`;

export const FETCH_PNCATEGORIES = gql`
  query pnCategories {
    pnCategories {
      id
      name
    }
  }
`;

export const FETCH_BIGSUBCATEGORIES = gql`
  query subCategoriesByPNCategory($id: Int, $keyword: String) {
    subCategoriesByPNCategory(id: $id, keyword: $keyword) {
      id
      name
    }
  }
`;

export const FETCH_SMALLSUBCATEGORIES = gql`
  query subCategoriesBySubCategory($id: Int, $keyword: String) {
    subCategoriesBySubCategory(id: $id, keyword: $keyword) {
      id
      name
    }
  }
`;

export const FETCH_PNCATEGORYBYKEYWORD = gql`
  query pnCategoriesByKeyword($keyword: String) {
    pnCategoriesByKeyword(keyword: $keyword) {
      id
      name
    }
  }
`;
