# ADATA
# Author: Stanney (Rytass)

FROM node:10-alpine

MAINTAINER stanney@rytass.com

ENV HOME /root
ENV PORT 80
ENV DEBUG BAOMORE:*
ENV NODE_ENV production
ENV TZ=Asia/Taipei
ENV API_HOST http://baomore-official.rytass.com
ENV S3ACCESSKEY AKIAT4OOVQSVLN62G5FJ
ENV S3SECRETACCESSKEY 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
ENV S3BUCKETNAME artmo-tokyo

RUN apk add --update tzdata git make gcc g++ python
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN mkdir -p /var/www/BAOMORE/server
ADD /client/dist/* /var/www/BAOMORE/server/static/
ADD /server/dist/ /var/www/BAOMORE/server
RUN mv /var/www/BAOMORE/server/shared /var/www/BAOMORE
ADD package.json /var/www/BAOMORE
ADD /shared/socialCategory.json /var/www/BAOMORE/shared/socialCategory.json
RUN unlink /etc/localtime
RUN ln -s /usr/share/zoneinfo/$TZ /etc/localtime
RUN echo $TZ > /etc/timezone

WORKDIR /var/www/BAOMORE

RUN npm i

EXPOSE 80

CMD node /var/www/BAOMORE/server/server.js
