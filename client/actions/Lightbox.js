export const OPEN_LIGHTBOX = 'LIGHTBOX/OPEN_LIGHTBOX';
export const CLOSE_LIGHTBOX = 'LIGHTBOX/CLOSE_LIGHTBOX';
export const OPEN_STATUS_BOX = 'LIGHTBOX/OPEN_STATUS_BOX';
export const CLOSE_STATUS_BOX = 'LIGHTBOX/CLOSE_STATUS_BOX';

export function openLightbox(lightbox) {
  return {
    type: OPEN_LIGHTBOX,
    lightbox,
  };
}

export function closeLightbox() {
  return {
    type: CLOSE_LIGHTBOX,
  };
}

export function openStatusBox(status) {
  return {
    type: OPEN_STATUS_BOX,
    status,
  };
}

export function closeStatusBox() {
  return {
    type: CLOSE_STATUS_BOX,
  };
}
