export const CACHE_MEMBER_INFO = 'MEMBER/CACHE_MEMBER_INFO';
export const CACHE_MEMBER_TOKEN = 'MEMBER/CACHE_MEMBER_TOKEN';
export const CACHE_MEMBER_CUSTOMIZATION = 'MEMBER/CACHE_MEMBER_CUSTOMIZATION';
export const CLEAR_MEMBER_INFO = 'MEMBER/CLEAR_MEMBER_INFO';

export function cacheMemberInfo(member = {}) {
  return {
    type: CACHE_MEMBER_INFO,
    member,
  };
}

export function cacheMemberToken(token) {
  return {
    type: CACHE_MEMBER_TOKEN,
    token,
  };
}

export function cacheMemberCustomization(json) {
  return {
    type: CACHE_MEMBER_CUSTOMIZATION,
    json,
  };
}

export function clearMemberInfo() {
  return {
    type: CLEAR_MEMBER_INFO,
  };
}
