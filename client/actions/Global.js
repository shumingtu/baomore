export const OPEN_MOBILE_EXTEND_NAV = 'GLOBAL/OPEN_MOBILE_EXTEND_NAV';
export const CLOSE_MOBILE_EXTEND_NAV = 'GLOBAL/CLOSE_MOBILE_EXTEND_NAV';

export function openMobileExtendNav() {
  return {
    type: OPEN_MOBILE_EXTEND_NAV,
  };
}

export function closeMobileExtendNav() {
  return {
    type: CLOSE_MOBILE_EXTEND_NAV,
  };
}
