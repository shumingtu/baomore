export const CACHE_PHONE_SELECTION = 'CUSTOMIZE/CACHE_PHONE_SELECTION';
export const CACHE_CUSTOMIZE_CATEGORY = 'CUSTOMIZE/CACHE_CUSTOMIZE_CATEGORY';
export const CACHE_CURRENT_SELECTED_IMAGE = 'CUSTOMIZE/CACHE_CURRENT_SELECTED_IMAGE';
export const CACHE_CURRENT_SELECTED_TEXT = 'CUSTOMIZE/CACHE_CURRENT_SELECTED_TEXT';
export const CACHE_CURRENT_SELECTED_EMOJI = 'CUSTOMIZE/CACHE_CURRENT_SELECTED_EMOJI';
export const CACHE_CURRENT_SELECTED_FIXED_IMAGE = 'CUSTOMIZE/CACHE_CURRENT_SELECTED_FIXED_IMAGE';
export const CLEAR_ALL_SELECTED_CACHE = 'CUSTOMIZE/CLEAR_ALL_SELECTED_CACHE';
export const CACHE_CUSTOMIZE_MAIN_CATEGORIES = 'CUSTOMIZE/CACHE_CUSTOMZIE_MAIN_CATEGORIES';
export const CACHE_CUSTOMIZE_URL = 'CUSTOMIZE/CACHE_CUSTOMIZE_URL';
export const CACHE_CUSTOMIZE_CANVAS_CLIP_SIZE = 'CUSTOMIZE/CACHE_CUSTOMIZE_CANVAS_CLIP_SIZE';
export const CACHE_CUSTOMIZE_CATEGORY_NAME = 'CACHE_CUSTOMIZE_CATEGORY_NAME';
export const CACHE_CUSTOMIZE_PNTABLE_INFO = 'CUSTOMIZE/CACHE_CUSTOMIZE_PNTABLE_INFO';

export function savePhoneSelection(payload) {
  return {
    type: CACHE_PHONE_SELECTION,
    payload,
  };
}

export function saveCustomizeCategoryName(payload) {
  return {
    type: CACHE_CUSTOMIZE_CATEGORY_NAME,
    payload,
  };
}

export function saveCustomizeCategory(payload) {
  return {
    type: CACHE_CUSTOMIZE_CATEGORY,
    payload,
  };
}

export function setCurrentSelection(imageId) {
  return {
    type: CACHE_CURRENT_SELECTED_IMAGE,
    imageId,
  };
}

export function setCurrentSelectionOnText(textId) {
  return {
    type: CACHE_CURRENT_SELECTED_TEXT,
    textId,
  };
}

export function setCurrentSelectionEmoji(emojiId) {
  return {
    type: CACHE_CURRENT_SELECTED_EMOJI,
    emojiId,
  };
}

export function setCurrentSelectionOnFixedImage(imageId) {
  return {
    type: CACHE_CURRENT_SELECTED_FIXED_IMAGE,
    imageId,
  };
}

export function clearAllSelectedCache() {
  return {
    type: CLEAR_ALL_SELECTED_CACHE,
  };
}

export function cacheCustomizeMainCategory(list = []) {
  return {
    type: CACHE_CUSTOMIZE_MAIN_CATEGORIES,
    list,
  };
}

export function cacheCustomizeUrl(url = '') {
  return {
    type: CACHE_CUSTOMIZE_URL,
    url,
  };
}

export function cacheCustomizeClipSize(image = {}) {
  return {
    type: CACHE_CUSTOMIZE_CANVAS_CLIP_SIZE,
    image,
  };
}

export function cacheCustomizePNTableInfo(info = null) {
  return {
    type: CACHE_CUSTOMIZE_PNTABLE_INFO,
    info,
  };
}
