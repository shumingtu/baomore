import gql from 'graphql-tag';

export const CONFIRM_LINEPAY_PAYMENT = gql`
  mutation payConfirm($orderId: String!, $transactionId: String!) {
    payConfirm(
      orderId: $orderId
      transactionId: $transactionId
    ) {
      status
      message
      human
    }
  }
`;

export const MAKE_ONLINE_ORDER = gql`
  mutation makeOnlineOrder(
    $pnTableId: Int!
    $picture: String!
    $storeId: Int!
    $date: String!
    $time: String!
    $name: String!
    $email: String!
    $phone: String!
    $isShared: Boolean!
    $storedDesignJSON: String!
    $isAllCustomized: Boolean,
  ) {
    makeOnlineOrder(
      pnTableId: $pnTableId
      picture: $picture
      storeId: $storeId
      date: $date
      time: $time
      name: $name
      email: $email
      phone: $phone
      isShared: $isShared
      storedDesignJSON: $storedDesignJSON
      isAllCustomized: $isAllCustomized
    ) {
      status
      message
      human
      payToken
    }
  }
`;
