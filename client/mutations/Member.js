import gql from 'graphql-tag';

export const STORE_MEMBER_JSON = gql`
  mutation storeJSON($json: String!) {
    storeJSON(
      json: $json
    ) {
      id
      name
      avatar
      phone
      email
      gender
      birthday
      storedDesignJSON
    }
  }
`;

export const EDIT_MEMBER_ME = gql`
  mutation editMe($name: String!, $email: String, $gender: String, $birthday: String, $phone: String) {
    editMe(
      name: $name
      email: $email
      gender: $gender
      birthday: $birthday
      phone: $phone
    ) {
      id
      name
      avatar
      phone
      email
      gender
      birthday
      storedDesignJSON
    }
  }
`;

export const ADD_WARRANTY_RECORD = gql`
  mutation addWarrantyRecord(
    $service: String!
    $phoneModel: String!
    $store: String!
    $VAT: String!
    $IME: String!
    $phone: String!
    $EmployeeMemberId: Int!
  ) {
    addWarrantyRecord(
      service: $service
      phoneModel: $phoneModel
      store: $store
      VAT: $VAT
      IME: $IME
      phone: $phone
      EmployeeMemberId: $EmployeeMemberId
    ) {
      id
      service
      phoneModel
      store
      VAT
      IME
      phoneLastSix
      isApproved
      employeeCode
      createdAt
    }
  }
`;

export default null;
