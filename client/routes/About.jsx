// @flow
import React from 'react';
import {
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
// config
import navs from '../shared/About/navs.js';
// components
import AboutMainBoard from '../containers/About/AboutMainBoard.jsx';

function AboutRoutes() {
  return (
    <Switch>
      {navs.map(n => (
        <Route
          key={n.id}
          path={n.path}
          component={props => <AboutMainBoard {...props} pathId={n.id} />} />
      ))}
      <Redirect to={{ pathname: (navs && navs[0] && navs[0].path) || '/' }} />
    </Switch>
  );
}

export default AboutRoutes;
