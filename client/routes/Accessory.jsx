// @flow
import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
// config
import navs from '../shared/Accessory/navs.js';
import {
  TYPE1_TITLE,
  TYPE1_FRAGMENTS,
  TYPE2_TITLE,
  TYPE2_FRAGMENTS,
  TYPE3_TITLE,
  TYPE3_FRAGMENTS,
  TYPE4_TITLE,
  TYPE4_FRAGMENTS,
} from '../shared/Accessory/fragments.js';
// components
import AccessoryLanding from '../containers/Accessory/AccessoryLanding.jsx';
import AccessoryMainBoard from '../containers/Accessory/AccessoryMainBoard.jsx';

type Props = {
  match: {
    url: string,
  },
};

function getTypeBundle(id) {
  switch (id) {
    case 'type1':
      return {
        title: TYPE1_TITLE,
        fragments: TYPE1_FRAGMENTS,
      };
    case 'type2':
      return {
        title: TYPE2_TITLE,
        fragments: TYPE2_FRAGMENTS,
      };
    case 'type3':
      return {
        title: TYPE3_TITLE,
        fragments: TYPE3_FRAGMENTS,
      };
    case 'type4':
      return {
        title: TYPE4_TITLE,
        fragments: TYPE4_FRAGMENTS,
      };
    default:
      return {};
  }
}

function AccessoryRoutes({
  match: {
    url,
  },
}: Props) {
  return (
    <Switch>
      {navs.map(n => (
        <Route
          key={n.id}
          path={n.path}
          component={props => (
            <AccessoryMainBoard
              {...props}
              {...getTypeBundle(n.id)} />
          )} />
      ))}
      <Route path={`${url}`} component={AccessoryLanding} />
    </Switch>
  );
}

export default AccessoryRoutes;
