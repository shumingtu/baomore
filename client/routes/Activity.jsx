// @flow
import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { Query } from 'react-apollo';
// config
import { GET_ACTIVITY_CATEGORIES } from '../queries/Activity.js';
// components
import ActivityMainBoard from '../containers/Activity/ActivityMainBoard.jsx';

type Props = {
  match: {
    url: string,
  },
};

function ActivityRoutes({
  match: {
    url,
  },
}: Props) {
  return (
    <Switch>
      <Route
        path={`${url}`}
        component={props => (
          <Query query={GET_ACTIVITY_CATEGORIES}>
            {({
              data,
            }) => {
              const categories = (data && data.activityCategories) || [];

              return (
                <ActivityMainBoard
                  {...props}
                  activityCategories={categories} />
              );
            }}
          </Query>
        )} />
    </Switch>
  );
}

export default ActivityRoutes;
