// @flow
import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
// config
import navs from '../shared/Introduction/navs.js';
import {
  TYPE1_TITLE,
  TYPE1_SERIES,
  TYPE1_FRAGMENTS,
  TYPE2_TITLE,
  TYPE2_SERIES,
  TYPE2_FRAGMENTS,
  TYPE3_TITLE,
  TYPE3_SERIES,
  TYPE3_FRAGMENTS,
  TYPE4_TITLE,
  TYPE4_SERIES,
  TYPE4_FRAGMENTS,
} from '../shared/Introduction/fragments.js';
// components
import IntroLanding from '../containers/Introduction/IntroLanding.jsx';
import IntroMainBoard from '../containers/Introduction/IntroMainBoard.jsx';
import IntroAdvantagePage from '../containers/Introduction/IntroAdvantagePage.jsx';

type Props = {
  match: {
    url: string,
  },
};

function getTypeBundle(id) {
  switch (id) {
    case 'type1':
      return {
        title: TYPE1_TITLE,
        series: TYPE1_SERIES,
        fragments: TYPE1_FRAGMENTS,
      };
    case 'type2':
      return {
        title: TYPE2_TITLE,
        series: TYPE2_SERIES,
        fragments: TYPE2_FRAGMENTS,
      };
    case 'type3':
      return {
        title: TYPE3_TITLE,
        series: TYPE3_SERIES,
        fragments: TYPE3_FRAGMENTS,
      };
    case 'type4':
      return {
        title: TYPE4_TITLE,
        series: TYPE4_SERIES,
        fragments: TYPE4_FRAGMENTS,
      };
    default:
      return {};
  }
}

function IntroductionRoutes({
  match: {
    url,
  },
}: Props) {
  return (
    <Switch>
      <Route path={`${url}/advantage`} component={IntroAdvantagePage} />
      {navs.map(n => (
        <Route
          key={n.id}
          path={n.path}
          component={props => (
            <IntroMainBoard
              {...props}
              {...getTypeBundle(n.id)} />
          )} />
      ))}
      <Route path={`${url}`} component={IntroLanding} />
    </Switch>
  );
}

export default IntroductionRoutes;
