// @flow
import React, { Component } from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { graphql } from 'react-apollo';

import * as CustomizeActions from '../actions/Customize.js';
// components
import CustomizeLanding from '../containers/Customize/CustomizeLanding.jsx';
import SelectBrandPage from '../containers/Customize/SelectBrandPage.jsx';
import SelectCategoryPage from '../containers/Customize/SelectCategoryPage.jsx';
import CustomizeCreateBoard from '../containers/Customize/CustomizeCreateBoard.jsx';
import {
  GET_CUSTOMIZE_MAIN_CATEGORIES,
  GET_CUSTOMIZE_PNTABLE_INFO,
} from '../queries/Customize.js';

type Props = {
  match: {
    url: string,
  },
  categories: Array<{
    id: number,
    name: string,
    picture: string,
  }>,
  customPNTable: {
    id: number,
    code: string,
    name: string,
    description: string,
    onlinePrice: string,
  },
  cacheCustomizeMainCategory: Function,
  cacheCustomizePNTableInfo: Function,
};

class CustomizeRoutes extends Component<Props> {
  componentDidMount() {
    const {
      categories,
      customPNTable,
      cacheCustomizeMainCategory,
      cacheCustomizePNTableInfo,
    } = this.props;

    if (categories) {
      cacheCustomizeMainCategory(categories);
    }

    if (customPNTable) {
      cacheCustomizePNTableInfo(customPNTable);
    }
  }

  componentDidUpdate(prevProps) {
    const {
      categories,
      customPNTable,
      cacheCustomizeMainCategory,
      cacheCustomizePNTableInfo,
    } = this.props;

    if (categories !== prevProps.categories && categories) {
      cacheCustomizeMainCategory(categories);
    }

    if (customPNTable !== prevProps.customPNTable && customPNTable) {
      cacheCustomizePNTableInfo(customPNTable);
    }
  }

  render() {
    const {
      match: {
        url,
      },
    } = this.props;

    return (
      <Switch>
        <Route exact path={`${url}`} component={CustomizeLanding} />
        <Route exact path={`${url}/start`} component={SelectBrandPage} />
        <Route exact path={`${url}/category`} component={SelectCategoryPage} />
        <Route exact path={`${url}/create`} component={CustomizeCreateBoard} />
      </Switch>
    );
  }
}

const queryHook = graphql(GET_CUSTOMIZE_MAIN_CATEGORIES, {
  props: ({
    data: {
      subCategoriesForSale,
    },
  }) => ({
    categories: subCategoriesForSale || [],
  }),
});

const customizeQueryHook = graphql(GET_CUSTOMIZE_PNTABLE_INFO, {
  props: ({
    data: {
      customPNTable,
    },
  }) => ({
    customPNTable: customPNTable || null,
  }),
});

const reduxHook = connect(
  state => ({
    customizeCategory: state.Customize.customizeCategory,
  }),
  dispatch => bindActionCreators({
    ...CustomizeActions,
  }, dispatch)
);

export default reduxHook(
  queryHook(
    customizeQueryHook(
      CustomizeRoutes
    )
  )
);
