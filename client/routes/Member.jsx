// @flow
import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
// components
import MemberMainBoard from '../containers/Member/MemberMainBoard.jsx';

type Props = {
  match: {
    url: string,
  },
};

function MemberRoutes({
  match: {
    url,
  },
}: Props) {
  return (
    <Switch>
      <Route
        path={`${url}`}
        component={MemberMainBoard} />
    </Switch>
  );
}

export default MemberRoutes;
