// @flow
import React from 'react';
import {
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
// config
import navs from '../shared/Customer/navs.js';
// components
import CustomerMainBoard from '../containers/Customer/CustomerMainBoard.jsx';

function CustomerRoutes() {
  return (
    <Switch>
      {navs.map(n => (
        <Route
          key={n.id}
          path={n.path}
          component={props => <CustomerMainBoard {...props} pathId={n.id} />} />
      ))}
      <Redirect to={{ pathname: (navs && navs[0] && navs[0].path) || '/' }} />
    </Switch>
  );
}

export default CustomerRoutes;
