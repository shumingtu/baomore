// @flow
import React from 'react';
import {
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
// config
import navs from '../shared/Description/navs.js';
// components
import DescriptionMainBoard from '../containers/Description/DescriptionMainBoard.jsx';

function DescriptionRoutes() {
  return (
    <Switch>
      {navs.map(n => (
        <Route
          key={n.id}
          path={n.path}
          component={props => <DescriptionMainBoard {...props} pathId={n.id} />} />
      ))}
      <Redirect to={{ pathname: (navs && navs[0] && navs[0].path) || '/' }} />
    </Switch>
  );
}

export default DescriptionRoutes;
