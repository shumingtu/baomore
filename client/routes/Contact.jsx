// @flow
import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
// components
import ContactMainBoard from '../containers/Contact/ContactMainBoard.jsx';
import ContactStorePage from '../containers/Contact/ContactStorePage.jsx';

type Props = {
  match: {
    url: string,
  },
};

function ContactRoutes({
  match: {
    url,
  },
}: Props) {
  return (
    <Switch>
      <Route path={`${url}/stores`} component={ContactStorePage} />
      <Route path={`${url}`} component={ContactMainBoard} />
    </Switch>
  );
}

export default ContactRoutes;
