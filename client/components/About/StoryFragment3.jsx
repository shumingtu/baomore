// @flow
import React, { Fragment } from 'react';
import radium from 'radium';
// config
import Theme from '../../styles/Theme.js';
// components
import ResizableImage from '../Fragment/ResizableImage.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '4px 12px',
  },
  desktopPlacement: {
    width: '100%',
    height: 'auto',
    display: 'block',
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
  mobilePlacement: {
    display: 'none',
    '@media (max-width: 767px)': {
      width: '100%',
      height: 'auto',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'flex-start',
    },
  },
  titlePlacement: {
    width: '100%',
    height: 'auto',
    padding: '48px 24px 62px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  titleWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '6px 0',
    borderBottom: `1px solid ${Theme.UNDERLINE_COLOR}`,
  },
  title: {
    fontSize: 32,
    fontWeight: 700,
    letterSpacing: 3,
    lineHeight: 1.4,
    textAlign: 'center',
    color: Theme.BLACK_COLOR,
    whiteSpace: 'pre-line',
  },
  imagesPlacement: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  imageWrapper: {
    width: '100%',
    height: 'auto',
    margin: '0 0 24px 0',
    '@media (max-width: 767px)': {
      margin: '16px 0',
    },
  },
};

type Props = {
  fragment: Array<{
    title: string,
    desktopImg: string,
    mobileImg: string,
  }>
};

function StoryFragment3({
  fragment,
}: Props) {
  if (!fragment) return null;

  return (
    <div style={styles.wrapper}>
      <div style={styles.desktopPlacement}>
        <div style={styles.titlePlacement}>
          {fragment.map(f => (
            <div key={f.title} style={styles.titleWrapper}>
              <span style={styles.title}>
                {f.title}
              </span>
            </div>
          ))}
        </div>
        <div style={styles.imagesPlacement}>
          {fragment.map((f) => {
            if (!f.desktopImg) return null;

            return (
              <div key={f.desktopImg} style={styles.imageWrapper}>
                <ResizableImage
                  desktopRatio={4.255}
                  mobileRatio={18.644}
                  desktopImg={f.desktopImg}
                  mobileImg={f.mobileImg} />
              </div>
            );
          })}
        </div>
      </div>
      <div style={styles.mobilePlacement}>
        {fragment.map((mf) => {
          if (!mf.desktopImg) return null;

          return (
            <Fragment key={mf.title}>
              {/* <div style={styles.titleWrapper}>
                <span style={styles.title}>
                  {mf.title}
                </span>
              </div> */}
              <div style={styles.imageWrapper}>
                <ResizableImage
                  desktopRatio={4.255}
                  mobileRatio={18.308}
                  desktopImg={mf.desktopImg}
                  mobileImg={mf.mobileImg} />
              </div>
            </Fragment>
          );
        })}
      </div>
    </div>
  );
}

export default radium(StoryFragment3);
