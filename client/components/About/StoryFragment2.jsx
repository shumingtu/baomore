// @flow
import React from 'react';
import radium from 'radium';
// config
import Theme from '../../styles/Theme.js';
// components
import ResizableImage from '../Fragment/ResizableImage.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '4px 12px',
  },
  textWrapper: {
    width: '100%',
    height: 'auto',
    padding: '24px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      padding: '0 12px',
    },
  },
  desc: {
    fontSize: 24,
    fontWeight: 700,
    letterSpacing: 1,
    lineHeight: 1.4,
    textAlign: 'center',
    color: Theme.BLACK_COLOR,
    whiteSpace: 'pre-line',
    '@media (max-width: 767px)': {
      whiteSpace: 'normal',
    },
  },
};

type Props = {
  fragment: {
    title: {
      desktopImg: string,
      mobileImg: string,
    },
    desc: Array<string>,
  },
};

function StoryFragment2({
  fragment,
}: Props) {
  if (!fragment) return null;

  return (
    <div style={styles.wrapper}>
      {fragment.title ? (
        <ResizableImage
          desktopRatio={0.08}
          mobileRatio={0.427}
          desktopImg={fragment.title.desktopImg}
          mobileImg={fragment.title.mobileImg} />
      ) : null}
      {fragment.desc && Array.isArray(fragment.desc) ? (
        <div style={styles.textWrapper}>
          {fragment.desc.map(d => (
            <p
              key={d}
              style={styles.desc}>
              {d}
            </p>
          ))}
        </div>
      ) : null}
    </div>
  );
}

export default radium(StoryFragment2);
