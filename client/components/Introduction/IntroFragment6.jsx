// @flow
import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
// static
import Theme from '../../styles/Theme.js';
// components
import ResizableImage from '../Fragment/ResizableImage.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: 48,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#fff',
    '@media (max-width: 767px)': {
      padding: '48px 0',
    },
  },
  groupWrapper: {
    width: '100%',
    maxWidth: 1000,
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: '6px 0',
  },
  imageWrapper: {
    flex: 2,
    height: 'auto',
    '@media (max-width: 767px)': {
      width: '100%',
      height: 'auto',
    },
  },
  imageTextWrapper: {
    flex: 1,
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    padding: '0 32px',
    '@media (max-width: 767px)': {
      width: '100%',
      alignItems: 'center',
      padding: '24px 0',
    },
  },
  title: {
    fontSize: 16,
    fontWeight: 700,
    color: Theme.YELLOW_COLOR,
    letterSpacing: 1.5,
  },
  desc: {
    fontSize: 16,
    fontWeight: 400,
    color: Theme.GRAY_TEXT_COLOR,
    letterSpacing: 1.5,
  },
};

type Props = {
  fragments: Array<{
    title: string,
    desc: string,
    desktopImg: string,
    mobileImg: string,
  }>,
};

type State = {
  showImages: boolean,
};

class IntroFragment6 extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      showImages: false,
    };

    this.imageRef = React.createRef();
  }

  componentDidMount() {
    this.setState({
      showImages: true,
    });
  }

  imageRef: { current: null | HTMLDivElement }

  render() {
    const {
      fragments = [],
    } = this.props;

    const {
      showImages,
    } = this.state;

    if (!fragments.length) return null;

    return (
      <div style={styles.wrapper}>
        {fragments.map((f, idx) => (
          <Fragment key={`${f.desktopImg}-${f.mobileImg}`}>
            {idx % 2 === 0 ? (
              <div
                style={[
                  styles.groupWrapper,
                  {
                    '@media (max-width: 767px)': {
                      flexDirection: 'column-reverse',
                      alignItems: 'flex-start',
                      justifyContent: 'flex-start',
                    },
                  },
                ]}>
                <div
                  ref={this.imageRef}
                  style={styles.imageWrapper}>
                  {showImages ? (
                    <ResizableImage
                      widthRef={this.imageRef}
                      maxWidth={594}
                      desktopRatio={0.671}
                      mobileRatio={1.5}
                      desktopImg={f.desktopImg}
                      mobileImg={f.mobileImg} />
                  ) : null}
                </div>
                <div style={styles.imageTextWrapper}>
                  <span style={styles.title}>
                    {f.title || null}
                  </span>
                  <span style={styles.desc}>
                    {f.desc || null}
                  </span>
                </div>
              </div>
            ) : (
              <div
                style={[
                  styles.groupWrapper,
                  {
                    '@media (max-width: 767px)': {
                      flexDirection: 'column',
                      alignItems: 'flex-start',
                      justifyContent: 'flex-start',
                    },
                  },
                ]}>
                <div style={styles.imageTextWrapper}>
                  <span style={styles.title}>
                    {f.title || null}
                  </span>
                  <span style={styles.desc}>
                    {f.desc || null}
                  </span>
                </div>
                <div
                  ref={this.imageRef}
                  style={styles.imageWrapper}>
                  {showImages ? (
                    <ResizableImage
                      widthRef={this.imageRef}
                      maxWidth={594}
                      desktopRatio={0.671}
                      mobileRatio={1.5}
                      desktopImg={f.desktopImg}
                      mobileImg={f.mobileImg} />
                  ) : null}
                </div>
              </div>
            )}
          </Fragment>
        ))}
      </div>
    );
  }
}

export default radium(IntroFragment6);
