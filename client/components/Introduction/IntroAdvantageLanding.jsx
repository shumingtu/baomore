import React from 'react';
import radium from 'radium';
// static
import landing5 from '../../static/images/introduction/desktop/introduction-desktop-landing-5.jpg';
import mLanding5 from '../../static/images/introduction/mobile/introduction-mobile-landing-5.jpg';
// components
import LinkButton from '../Fragment/Element/LinkButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: 48,
    backgroundColor: '#fff',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      padding: '24px 0',
    },
  },
  titleWrapper: {
    width: '100%',
    height: 'auto',
    padding: '16px 24px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      padding: '36px 48px',
    },
  },
  title: {
    fontSize: 36,
    fontWeight: 700,
    color: '#000',
    letterSpacing: 3,
    textAlign: 'center',
  },
  imgWrapper: {
    width: '100%',
    height: 533,
    backgroundImage: `url(${landing5})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: 48,
    '@media (max-width: 1023px)': {
      backgroundSize: 'contain',
    },
    '@media (max-width: 767px)': {
      height: 800,
      alignItems: 'flex-start',
      justifyContent: 'center',
      padding: '48px 12px',
      backgroundImage: `url(${mLanding5})`,
    },
  },
  infoWrapper: {
    width: 'auto',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      alignItems: 'center',
    },
  },
  textWrapper: {
    width: 'auto',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  mobileColumnText: {
    width: 'auto',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'center',
    },
  },
  inlineText: {
    width: 'auto',
    height: 'auto',
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    padding: '12px 0',
  },
  text: {
    fontSize: 36,
    letterSpacing: 3,
    lineHeight: 1.3,
    color: '#fff',
    textShadow: '3px 2px 4px rgba(0, 0, 0, 0.7)',
    whiteSpace: 'nowrap',
    '@media (max-width: 767px)': {
      whiteSpace: 'pre-line',
    },
  },
  bigText: {
    fontSize: 82,
    lineHeight: 0.88,
    letterSpacing: 1.5,
  },
};

function IntroAdvantageLanding() {
  return (
    <div style={styles.wrapper}>
      <div style={styles.titleWrapper}>
        <span style={styles.title}>
          將你的手機，放心給artmo保護
        </span>
      </div>
      <div style={styles.imgWrapper}>
        <div style={styles.infoWrapper}>
          <div style={styles.textWrapper}>
            <div style={styles.mobileColumnText}>
              <span style={styles.text}>
                包膜不可
              </span>
              <span style={styles.text}>
                不知的
              </span>
            </div>
            <div style={styles.inlineText}>
              <span style={[styles.text, styles.bigText]}>
                6
              </span>
              <span style={styles.text}>
                大功能
              </span>
            </div>
          </div>
          <LinkButton
            filledColor
            path="/introduction/advantage"
            text="了解更多" />
        </div>
      </div>
    </div>
  );
}

export default radium(IntroAdvantageLanding);
