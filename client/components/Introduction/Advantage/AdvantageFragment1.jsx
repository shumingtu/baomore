// @flow
import React from 'react';
import radium from 'radium';
// config
import Theme from '../../../styles/Theme.js';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '32px 24px',
  },
  textWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 700,
    letterSpacing: 2,
    color: Theme.PURPLE_COLOR,
    '@media (max-width: 767px)': {
      fontSize: 32,
    },
  },
  desc: {
    fontSize: 18,
    fontWeight: 300,
    textAlign: 'center',
    letterSpacing: 2,
    color: Theme.BLACK_COLOR,
    whiteSpace: 'pre-line',
  },
  cardsWrapper: {
    width: '100%',
    height: 'auto',
    padding: '24px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cardsPlacement: {
    width: '100%',
    maxWidth: 700,
    height: 'auto',
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    '@media (max-width: 767px)': {
      justifyContent: 'center',
    },
  },
  card: {
    width: 180,
    height: 290,
    margin: 12,
    padding: 12,
    backgroundColor: '#fff',
    border: 0,
    borderRadius: 12,
    boxShadow: Theme.CLIENT_BLOCK_SHADOW,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  cardTitleWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '18px 0',
  },
  cardTitle: {
    fontSize: 24,
    fontWeight: 700,
    color: Theme.BLACK_COLOR,
    letterSpacing: 2,
  },
  cardDescWrapper: {
    width: '100%',
    height: 'auto',
    flex: 1,
    padding: 4,
    textAlign: 'left',
  },
  cardDesc: {
    fontSize: 15,
    color: Theme.BLACK_COLOR,
    fontWeight: 300,
    lineHeight: 1.5,
    letterSpacing: 1,
  },
};

type Props = {
  fragment: {
    title: string,
    desc: string,
    cards: Array<{
      title: string,
      desc: string,
    }>,
  },
};

function AdvantageFragment1({
  fragment,
}: Props) {
  if (!fragment) return null;

  const {
    title,
    desc,
    cards,
  } = fragment;

  return (
    <div style={styles.wrapper}>
      {title ? (
        <div style={styles.textWrapper}>
          <span style={styles.title}>
            {title}
          </span>
        </div>
      ) : null}
      {desc ? (
        <div style={styles.textWrapper}>
          <span style={styles.desc}>
            {desc}
          </span>
        </div>
      ) : null}
      {cards && Array.isArray(cards) ? (
        <div style={styles.cardsWrapper}>
          <div style={styles.cardsPlacement}>
            {cards.map(c => (
              <div
                key={`${c.title}-${c.desc}`}
                style={styles.card}>
                <div style={styles.cardTitleWrapper}>
                  <span style={styles.cardTitle}>
                    {c.title || null}
                  </span>
                </div>
                <div style={styles.cardDescWrapper}>
                  <span style={styles.cardDesc}>
                    {c.desc || null}
                  </span>
                </div>
              </div>
            ))}
          </div>
        </div>
      ) : null}
    </div>
  );
}

export default radium(AdvantageFragment1);
