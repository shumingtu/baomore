// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
// config
import Theme from '../../../styles/Theme.js';
// components
import ResizableImage from '../../Fragment/ResizableImage.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
  },
  textWrapper: {
    display: 'none',
    width: '100%',
    height: 'auto',
    '@media (max-width: 767px)': {
      padding: '24px 48px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  },
  title: {
    fontSize: 24,
    fontWeight: 700,
    letterSpacing: 2,
    textAlign: 'center',
    color: Theme.PURPLE_COLOR,
    '@media (max-width: 767px)': {
      fontSize: 32,
    },
  },
  desc: {
    fontSize: 18,
    fontWeight: 300,
    textAlign: 'center',
    letterSpacing: 2,
    color: Theme.BLACK_COLOR,
    whiteSpace: 'pre-line',
  },
  imagesWrapper: {
    width: '100%',
    height: 'auto',
    padding: '24px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  imageWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    '@media (max-width: 767px)': {
      padding: '6px 0',
    },
  },
};

type Props = {
  fragment: {
    title: string,
    desc: string,
    images: Array<{
      desktopImg: string,
      mobileImg: string,
    }>,
  },
};

type State = {
  showImages: boolean,
};

class AdvantageFragment2 extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      showImages: false,
    };

    this.imageRef = React.createRef();
  }

  componentDidMount() {
    this.setState({
      showImages: true,
    });
  }

  imageRef: { current: null | HTMLDivElement }

  render() {
    const {
      fragment,
    } = this.props;

    if (!fragment) return null;

    const {
      title,
      desc,
      images,
    } = fragment;

    const {
      showImages,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        {title ? (
          <div style={styles.textWrapper}>
            <span style={styles.title}>
              {title}
            </span>
          </div>
        ) : null}
        {desc ? (
          <div style={styles.textWrapper}>
            <span style={styles.desc}>
              {desc}
            </span>
          </div>
        ) : null}
        <div ref={this.imageRef} style={styles.imagesWrapper}>
          {showImages && Array.isArray(images) ? images.map(i => (
            <div key={i.desktopImg} style={styles.imageWrapper}>
              <ResizableImage
                widthRef={this.imageRef}
                desktopRatio={0.418}
                mobileRatio={1.5}
                desktopImg={i.desktopImg}
                mobileImg={i.mobileImg} />
            </div>
          )) : null}
        </div>
      </div>
    );
  }
}

export default radium(AdvantageFragment2);
