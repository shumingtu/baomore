// @flow
import React from 'react';
// components
import ResizableImage from '../Fragment/ResizableImage.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    backgroundColor: '#fff',
    padding: 0,
  },
  img: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

type Props = {
  fragment: {
    desktopImg: string,
    mobileImg: string,
  },
};

function IntroFragment7({
  fragment,
}: Props) {
  if (!fragment) return null;

  return (
    <div style={styles.wrapper}>
      {fragment.desktopImg ? (
        <div style={styles.img}>
          <ResizableImage
            desktopRatio={0.556}
            mobileRatio={3}
            desktopImg={fragment.desktopImg}
            mobileImg={fragment.mobileImg} />
        </div>
      ) : null}
    </div>
  );
}

export default IntroFragment7;
