// @flow
import React from 'react';
import radium from 'radium';

import Theme from '../../styles/Theme.js';
// components
import ResizableImage from '../Fragment/ResizableImage.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    backgroundColor: '#fff',
    padding: 0,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      marginBottom: 8,
    },
  },
  img: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textWrapper: {
    width: 'auto',
    height: 'auto',
    padding: 12,
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
  text: {
    fontSize: 16,
    fontWeight: 400,
    letterSpacing: 1.5,
    color: Theme.DEFAULT_TEXT_COLOR,
    lineHeight: 1.3,
    textAlign: 'center',
    whiteSpace: 'pre-line',
  },
};

type Props = {
  fragment: {
    desktopImg: string,
    mobileImg: string,
    desc: string,
  },
};

function IntroFragment9({
  fragment,
}: Props) {
  if (!fragment) return null;

  return (
    <div style={styles.wrapper}>
      {fragment.desktopImg ? (
        <div style={styles.img}>
          <ResizableImage
            desktopRatio={0.552}
            mobileRatio={2}
            desktopImg={fragment.desktopImg}
            mobileImg={fragment.mobileImg} />
        </div>
      ) : null}
      {fragment.desc ? (
        <div style={styles.textWrapper}>
          <span style={styles.text}>
            {fragment.desc}
          </span>
        </div>
      ) : null}
    </div>
  );
}

export default radium(IntroFragment9);
