// @flow
import React from 'react';
import radium from 'radium';
// components
import MultiImageViewer from './MultiImageViewer.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '36px 12px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
  },
  textWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      padding: 12,
    },
  },
  title: {
    fontSize: 24,
    fontWeight: 700,
    color: '#fff',
    letterSpacing: 3,
    textAlign: 'center',
    whiteSpace: 'pre-line',
    '@media (max-width: 767px)': {
      fontSize: 32,
    },
  },
  desc: {
    fontSize: 18,
    fontWeight: 400,
    color: '#fff',
    letterSpacing: 1,
    lineHeight: 1.3,
    textAlign: 'center',
    whiteSpace: 'pre-line',
    '@media (max-width: 767px)': {
      fontWeight: 300,
    },
  },
};

type Props = {
  mainBackgroundColor: string,
  fragment: {
    title: {
      color: string,
      name: string,
    },
    desc: {
      color: string,
      name: string,
    },
    images: Array<{
      desc: string,
      src1: {
        desktopImg: string,
        mobileImg: string,
      },
      src2: {
        desktopImg: string,
        mobileImg: string,
      },
      src3: {
        desktopImg: string,
        mobileImg: string,
      },
    }>,
    button: {
      backgroundColor: string,
      color: string,
    },
  },
};

function SeriesFragment2({
  mainBackgroundColor,
  fragment,
}: Props) {
  return (
    <div
      style={[
        styles.wrapper,
        {
          backgroundColor: mainBackgroundColor || 'transparent',
        },
      ]}>
      {fragment.title ? (
        <div style={styles.textWrapper}>
          <span
            style={[
              styles.title,
              {
                color: fragment.title.color || '#fff',
              },
            ]}>
            {fragment.title.name || null}
          </span>
        </div>
      ) : null}
      {fragment.desc ? (
        <div style={styles.textWrapper}>
          <span
            style={[
              styles.desc,
              {
                color: fragment.desc.color || '#fff',
              },
            ]}>
            {fragment.desc.name || null}
          </span>
        </div>
      ) : null}
      {fragment.images && Array.isArray(fragment.images) ? (
        <MultiImageViewer
          button={fragment.button}
          images={fragment.images}
          textColor={fragment.desc.color || '#fff'} />
      ) : null}
    </div>
  );
}

export default radium(SeriesFragment2);
