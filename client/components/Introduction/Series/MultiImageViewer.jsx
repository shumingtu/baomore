// @flow
import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
import {
  Link as l,
} from 'react-router-dom';
// components
import ResizableImage from '../../Fragment/ResizableImage.jsx';

const Link = radium(l);

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '18px 36px',
  },
  mainImageWrapper: {
    width: '100%',
    height: 'auto',
  },
  groupImageWrapper: {
    width: '100%',
    height: 'auto',
    margin: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'flex-start',
      margin: 0,
    },
  },
  groupImage: {
    flex: 1,
    height: 'auto',
    '@media (max-width: 767px)': {
      width: '100%',
      height: 'auto',
      marginTop: 8,
    },
  },
  thumbWrapper: {
    width: '100%',
    height: 'auto',
    padding: 24,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      padding: '24px 0 12px 0',
    },
  },
  thumb: {
    width: 80,
    height: 80,
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    margin: '0 6px',
    opacity: 0.6,
    cursor: 'pointer',
    '@media (max-width: 767px)': {
      width: 50,
      height: 50,
    },
  },
  thumbActive: {
    opacity: 1,
    border: '2px solid #fff',
  },
  descWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  descPlacement: {
    width: 'auto',
    maxWidth: 500,
    height: 'auto',
    padding: '16px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  desc: {
    fontSize: 16,
    fontWeight: 400,
    color: '#fff',
    letterSpacing: 1.6,
    lineHeight: 1.4,
    textAlign: 'center',
    whiteSpace: 'pre-line',
  },
  buyBtnWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buyBtn: {
    width: 'auto',
    height: 'auto',
    padding: '4px 24px',
    backgroundColor: 'transparent',
    borderRadius: 16,
    border: 0,
    outline: 0,
    fontSize: 13,
    letterSpacing: 4,
    color: '#fff',
    textAlign: 'center',
    textDecoration: 'none',
    cursor: 'pointer',
    ':hover': {
      opacity: 0.9,
    },
  },
};

type Props = {
  textColor: string,
  images: Array<{
    desc: string,
    src1: {
      desktopImg: string,
      mobileImg: string,
    },
    src2: {
      desktopImg: string,
      mobileImg: string,
    },
    src3: {
      desktopImg: string,
      mobileImg: string,
    },
  }>,
  button: {
    backgroundColor: string,
    color: string,
  },
};

type State = {
  currentSelectedIdx: number,
  showImages: boolean,
};

class MultiImageViewer extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      currentSelectedIdx: 0,
      showImages: false,
    };

    this.mainImageWrapper = React.createRef();
    this.subImage1Wrapper = React.createRef();
    this.subImage2Wrapper = React.createRef();
  }

  componentDidMount() {
    this.setState({
      showImages: true,
    });
  }

  mainImageWrapper: { current: null | HTMLDivElement }

  subImage1Wrapper: { current: null | HTMLDivElement }

  subImage2Wrapper: { current: null | HTMLDivElement }

  render() {
    const {
      images,
      button,
      textColor,
    } = this.props;

    const {
      currentSelectedIdx,
      showImages,
    } = this.state;

    const currentImage = images.find((_, idx) => idx === currentSelectedIdx);

    return (
      <div style={styles.wrapper}>
        {currentImage ? (
          <Fragment>
            <div
              ref={this.mainImageWrapper}
              style={styles.mainImageWrapper}>
              {showImages ? (
                <ResizableImage
                  widthRef={this.mainImageWrapper}
                  desktopRatio={0.669}
                  mobileRatio={1.331}
                  desktopImg={(currentImage.src1 && currentImage.src1.desktopImg) || null}
                  mobileImg={(currentImage.src1 && currentImage.src1.mobileImg) || null} />
              ) : null}
            </div>
            <div style={styles.groupImageWrapper}>
              <div
                ref={this.subImage1Wrapper}
                style={[
                  styles.groupImage,
                  {
                    marginRight: 12,
                    '@media (max-width: 767px)': {
                      marginRight: 0,
                    },
                  },
                ]}>
                {showImages ? (
                  <ResizableImage
                    widthRef={this.subImage1Wrapper}
                    desktopRatio={1.01}
                    mobileRatio={1.01}
                    desktopImg={(currentImage.src2 && currentImage.src2.desktopImg) || null}
                    mobileImg={(currentImage.src2 && currentImage.src2.mobileImg) || null} />
                ) : null}
              </div>
              <div
                ref={this.subImage2Wrapper}
                style={styles.groupImage}>
                {showImages ? (
                  <ResizableImage
                    widthRef={this.subImage2Wrapper}
                    desktopRatio={1.01}
                    mobileRatio={1.01}
                    desktopImg={(currentImage.src3 && currentImage.src3.desktopImg) || null}
                    mobileImg={(currentImage.src3 && currentImage.src3.mobileImg) || null} />
                ) : null}
              </div>
            </div>
          </Fragment>
        ) : null}
        <div style={styles.thumbWrapper}>
          {images.map((i, idx) => (
            <button
              key={`${idx + 1}-btn`}
              type="button"
              onClick={() => this.setState({ currentSelectedIdx: idx })}
              style={[
                styles.thumb,
                currentSelectedIdx === idx && {
                  opacity: 1,
                  border: button && button.backgroundColor
                    ? `2px solid ${button.backgroundColor}`
                    : '2px solid #fff',
                },
                {
                  backgroundImage: i.src1 && i.src1.desktopImg
                    ? `url(${i.src1.desktopImg})`
                    : null,
                },
              ]} />
          ))}
        </div>
        <div style={styles.descWrapper}>
          <div
            style={[
              styles.descPlacement,
              {
                borderBottom: button && button.backgroundColor
                  ? `2px solid ${button.backgroundColor}`
                  : 0,
              },
            ]}>
            <span
              style={[
                styles.desc,
                {
                  color: textColor,
                },
              ]}>
              {(currentImage && currentImage.desc) || null}
            </span>
          </div>
        </div>
        {button ? (
          <div style={styles.buyBtnWrapper}>
            <Link
              to={{ pathname: '/customize/create' }}
              style={[
                styles.buyBtn,
                {
                  backgroundColor: button.backgroundColor || 'transparent',
                  color: button.color || '#fff',
                },
              ]}>
              購買
            </Link>
          </div>
        ) : null}
      </div>
    );
  }
}

export default radium(MultiImageViewer);
