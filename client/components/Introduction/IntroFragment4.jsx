// @flow
import React from 'react';
import radium from 'radium';
// config
import Theme from '../../styles/Theme.js';
// components
import ResizableImage from '../Fragment/ResizableImage.jsx';
import CenterCoverImage from '../Fragment/CenterCoverImage.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    backgroundColor: '#fff',
  },
  textWrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textPlacement: {
    width: 600,
    height: 'auto',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'space-around',
    '@media (max-width: 767px)': {
      width: 'auto',
      flexDirection: 'column',
      marginTop: 12,
    },
  },
  textBlock: {
    width: 'auto',
    height: 'auto',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      margin: '16px 0',
    },
  },
  highlight: {
    color: Theme.BLACK_COLOR,
  },
  text: {
    fontSize: 16,
    fontWeight: 400,
    color: Theme.GRAY_COLOR,
    letterSpacing: 1.1,
    whiteSpace: 'pre-line',
  },
};

type Props = {
  fragment: {
    desktopImg: string,
    mobileImg: string,
    descTitle1: string,
    desc1: string,
    descTitle2: string,
    desc2: string,
  },
};

function IntroFragment4({
  fragment,
}: Props) {
  if (!fragment) return null;

  return (
    <div style={styles.wrapper}>
      {fragment.desktopImg ? (
        <ResizableImage
          desktopImg={fragment.desktopImg}
          mobileImg={fragment.mobileImg} />
          // <CenterCoverImage
          //   width="100%"
          //   height={711}
          //   mobileWidth="100%"
          //   mobileHeight={655}
          //   desktopImg={fragment.desktopImg}
          //   mobileImg={fragment.mobileImg} />
      ) : null}
      {fragment.desc1 || fragment.desc2 ? (
        <div style={styles.textWrapper}>
          <div style={styles.textPlacement}>
            {fragment.desc1 ? (
              <div style={styles.textBlock}>
                <span style={styles.text}>
                  {fragment.descTitle1
                    ? <b style={styles.highlight}>{fragment.descTitle1}</b>
                    : null}
                  {fragment.desc1}
                </span>
              </div>
            ) : null}
            {fragment.desc2 ? (
              <div style={styles.textBlock}>
                <span style={styles.text}>
                  {fragment.descTitle2
                    ? <b style={styles.highlight}>{fragment.descTitle2}</b>
                    : null}
                  {fragment.desc2}
                </span>
              </div>
            ) : null}
          </div>
        </div>
      ) : null}
    </div>
  );
}

export default radium(IntroFragment4);
