// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

import Theme from '../../styles/Theme.js';
import ResizableImage from '../Fragment/ResizableImage.jsx';
import LinkButton from '../Fragment/Element/LinkButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#fff',
    paddingBottom: 48,
  },
  textWrapper: {
    width: '100%',
    height: 'auto',
    padding: '24px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      padding: '24px 18px',
    },
  },
  desc: {
    fontSize: 18,
    fontWeight: 700,
    letterSpacing: 1.3,
    lineHeight: 1.4,
    textAlign: 'center',
    whiteSpace: 'pre-line',
    '@media (max-width: 767px)': {
      fontSize: 24,
    },
  },
  title: {
    fontSize: 36,
    fontWeight: 700,
    letterSpacing: 4,
    color: Theme.PURPLE_COLOR,
    textAlign: 'center',
    whiteSpace: 'pre-line',
  },
  examplesWrapper: {
    width: '100%',
    height: 'auto',
    padding: '24px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
    },
  },
  exampleWrapper: {
    width: 350,
    height: 'auto',
    margin: '0 8px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      width: '100%',
      maxWidth: 350,
    },
  },
  exampleLabelWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: '8px 0',
    borderBottom: '1px solid rgba(0, 0, 0, 0.1)',
    '@media (max-width: 767px)': {
      width: '70%',
    },
  },
  labelTitle: {
    fontSize: 16,
    fontWeight: 700,
    letterSpacing: 1.4,
    color: Theme.YELLOW_COLOR,
    textAlign: 'center',
  },
  labelDesc: {
    fontSize: 16,
    fontWeight: 400,
    letterSpacing: 1.4,
    color: Theme.DEFAULT_TEXT_COLOR,
    textAlign: 'center',
  },
  exampleImg: {
    width: '100%',
    height: 'auto',
  },
  buttonWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

type Props = {
  fragment: {
    desc: string,
    title: string,
    path: string,
    examples: Array<{
      title: string,
      desc: string,
      desktopImg: string,
      mobileImg: string,
    }>,
  },
};

type State = {
  showImages: boolean,
};

class IntroFragment11 extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      showImages: false,
    };

    this.exampleImg = React.createRef();
  }

  componentDidMount() {
    this.setState({
      showImages: true,
    });
  }

  exampleImg: { current: null | HTMLDivElement }

  render() {
    const {
      fragment,
    } = this.props;

    const {
      showImages,
    } = this.state;

    if (!fragment) return null;

    return (
      <div style={styles.wrapper}>
        {fragment.desc ? (
          <div style={styles.textWrapper}>
            <span style={styles.desc}>
              {fragment.desc}
            </span>
          </div>
        ) : null}
        {fragment.title ? (
          <div style={styles.textWrapper}>
            <span style={styles.title}>
              {fragment.title}
            </span>
          </div>
        ) : null}
        {fragment.examples && Array.isArray(fragment.examples) ? (
          <div style={styles.examplesWrapper}>
            {fragment.examples.map(ex => (
              <div key={`${ex.title}-${ex.desc}`} style={styles.exampleWrapper}>
                {ex.title || ex.desc ? (
                  <div style={styles.exampleLabelWrapper}>
                    {ex.title ? (
                      <span style={styles.labelTitle}>
                        {ex.title}
                      </span>
                    ) : null}
                    {ex.desc ? (
                      <span style={styles.labelDesc}>
                        {ex.desc}
                      </span>
                    ) : null}
                  </div>
                ) : null}
                {ex.desktopImg ? (
                  <div ref={this.exampleImg} style={styles.exampleImg}>
                    {showImages ? (
                      <ResizableImage
                        widthRef={this.exampleImg}
                        maxWidth={350}
                        desktopRatio={1.484}
                        mobileRatio={1.574}
                        desktopImg={ex.desktopImg}
                        mobileImg={ex.mobileImg} />
                    ) : null}
                  </div>
                ) : null}
              </div>
            ))}
          </div>
        ) : null}
        {fragment.path ? (
          <div style={styles.buttonWrapper}>
            <LinkButton
              text="更多樣式"
              path={fragment.path} />
          </div>
        ) : null}
      </div>
    );
  }
}

export default radium(IntroFragment11);
