// @flow
import React from 'react';
import radium from 'radium';
// components
import ResizableImage from '../Fragment/ResizableImage.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#fff',
    padding: '48px 0 0 0',
  },
  textWrapper: {
    fontSize: 22,
    fontWeight: 400,
    color: '#000',
    letterSpacing: 1,
    lineHeight: 1.6,
    textAlign: 'center',
    whiteSpace: 'pre-line',
    '@media (max-width: 767px)': {
      padding: '0 12px',
    },
  },
  img: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

type Props = {
  fragment: {
    title: string,
    desktopImg: string,
    mobileImg: string,
  },
};

function IntroFragment3({
  fragment,
}: Props) {
  if (!fragment) return null;

  return (
    <div style={styles.wrapper}>
      {fragment.title ? (
        <span style={styles.textWrapper}>
          {fragment.title}
        </span>
      ) : null}
      {fragment.desktopImg ? (
        <div style={styles.img}>
          <ResizableImage
            desktopRatio={0.51}
            mobileRatio={2.05}
            desktopImg={fragment.desktopImg}
            mobileImg={fragment.mobileImg} />
        </div>
      ) : null}
    </div>
  );
}

export default radium(IntroFragment3);
