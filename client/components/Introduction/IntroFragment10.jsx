// @flow
import React from 'react';
import radium from 'radium';
// components
import ResizableImage from '../Fragment/ResizableImage.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    backgroundColor: '#fff',
    padding: 0,
    '@media (max-width: 767px)': {
      marginBottom: 8,
    },
  },
  img: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

type Props = {
  fragment: {
    desktopImg: string,
    mobileImg: string,
  },
};

function IntroFragment10({
  fragment,
}: Props) {
  if (!fragment) return null;

  return (
    <div style={styles.wrapper}>
      {fragment.desktopImg ? (
        <div style={styles.img}>
          <ResizableImage
            desktopRatio={0.62}
            mobileRatio={2}
            desktopImg={fragment.desktopImg}
            mobileImg={fragment.mobileImg} />
        </div>
      ) : null}
    </div>
  );
}

export default radium(IntroFragment10);
