// @flow
import React from 'react';
import radium from 'radium';
import { Link as link } from 'react-router-dom';
// config
import Theme from '../../styles/Theme.js';

const Link = radium(link);

const styles = {
  wrapper: {
    width: 'auto',
    height: 'auto',
    padding: '4px 12px',
    backgroundColor: Theme.GRAY_COLOR,
    border: 0,
    borderRadius: 12,
    marginRight: 12,
    outline: 0,
    fontSize: 13,
    color: '#fff',
    letterSpacing: 1.5,
    textAlign: 'center',
    textDecoration: 'none',
    cursor: 'pointer',
    ':hover': {
      border: `1px solid ${Theme.GRAY_COLOR}`,
      backgroundColor: 'transparent',
      color: Theme.GRAY_COLOR,
    },
  },
};

type Props = {
  nav: {
    id: string,
    path: string,
    name: string,
  },
};

function IntroLandingNav({
  nav,
}: Props) {
  if (!nav) return null;

  return (
    <Link
      to={{ pathname: nav.path }}
      style={styles.wrapper}>
      {nav.name || null}
    </Link>
  );
}

export default radium(
  IntroLandingNav
);
