// @flow
import React from 'react';
import radium from 'radium';
import { Link as link } from 'react-router-dom';
// config
import Theme from '../../styles/Theme.js';

const Link = radium(link);

const styles = {
  wrapper: {
    width: 120,
    height: 'auto',
    border: 0,
    borderRadius: 12,
    marginRight: 12,
    outline: 0,
    textDecoration: 'none',
    cursor: 'pointer',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    ':hover': {
      opacity: 0.8,
    },
  },
  iconWrapper: {
    width: 60,
    height: 60,
    marginBottom: 8,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  },
  textWrapper: {
    width: '100%',
    height: 'auto',
    padding: '2px 6px',
    border: 0,
    borderRadius: 12,
    backgroundColor: Theme.GRAY_COLOR,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 15,
    fontWeight: 400,
    color: '#fff',
    letterSpacing: 1.5,
    textAlign: 'center',
  },
};

type Props = {
  nav: {
    id: string,
    path: string,
    icon: string,
    name: string,
  },
};

function AccessoryLandingNav({
  nav,
}: Props) {
  if (!nav) return null;

  return (
    <Link
      to={{ pathname: nav.path }}
      style={styles.wrapper}>
      <div
        style={[
          styles.iconWrapper,
          {
            backgroundImage: nav.icon ? `url(${nav.icon})` : null,
          },
        ]} />
      <div style={styles.textWrapper}>
        <span style={styles.text}>
          {nav.name}
        </span>
      </div>
    </Link>
  );
}

export default radium(
  AccessoryLandingNav
);
