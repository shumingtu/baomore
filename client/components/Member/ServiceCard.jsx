// @flow
import React from 'react';
import radium from 'radium';
// static
import Theme from '../../styles/Theme.js';
import artmoLogo from '../../static/images/artmo-logo-top.png';
// components


const styles = {
  wrapper: {
    width: 483,
    height: 360,
    backgroundColor: '#fff',
    borderRadius: 12,
    boxShadow: Theme.CLIENT_BLOCK_SHADOW,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      width: 300,
      height: 300,
    },
  },
  logo: {
    width: 135,
    height: 52,
    margin: '4px 0',
    '@media (max-width: 767px)': {
      width: 100,
      height: 38.5,
    },
  },
  linePlacement: {
    width: '100%',
    height: 15,
    margin: '4px 0',
    borderTop: `2px solid ${Theme.YELLOW_COLOR}`,
    borderBottom: `2px solid ${Theme.PURPLE_COLOR}`,
    position: 'relative',
    overflow: 'hidden',
  },
  heavyLine: {
    position: 'absolute',
    zIndex: 3,
    width: 250,
    height: 8,
    backgroundColor: Theme.YELLOW_COLOR,
    '@media (max-width: 767px)': {
      width: 160,
    },
  },
  triangle: {
    position: 'absolute',
    zIndex: 3,
    width: 20,
    height: 20,
    transform: 'rotate(45deg)',
    backgroundColor: Theme.YELLOW_COLOR,
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '8px 24px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      padding: '6px 12px',
    },
  },
  label: {
    width: 100,
    height: 'auto',
    fontSize: 16,
    fontWeight: 400,
    letterSpacing: 1,
    color: Theme.BLACK_COLOR,
    '@media (max-width: 767px)': {
      width: 80,
      fontSize: 14,
    },
  },
  text: {
    fontSize: 16,
    fontWeight: 300,
    letterSpacing: 1,
    color: Theme.BLACK_COLOR,
    padding: '0 4px',
    '@media (max-width: 767px)': {
      fontSize: 14,
    },
  },
};

type Props = {
  info: {
    id: number,
    name: string,
    phoneModel: string,
    IME: string,
    phoneLastSix: string,
    store: string,
    employeeCode: string,
  },
};

function ServiceCard({
  info,
}: Props) {
  return (
    <div style={styles.wrapper}>
      <img
        alt="Artmo Logo"
        src={artmoLogo}
        style={styles.logo} />
      <div style={styles.linePlacement}>
        <div
          style={[
            styles.heavyLine,
            {
              left: -15,
              top: 0,
              backgroundColor: Theme.YELLOW_COLOR,
            },
          ]} />
        <div
          style={[
            styles.triangle,
            {
              left: 225,
              top: -16,
              backgroundColor: Theme.YELLOW_COLOR,
              '@media (max-width: 767px)': {
                left: 135,
              },
            },
          ]} />
        <div
          style={[
            styles.heavyLine,
            {
              right: -15,
              bottom: 0,
              backgroundColor: Theme.PURPLE_COLOR,
            },
          ]} />
        <div
          style={[
            styles.triangle,
            {
              right: 225,
              bottom: -16,
              backgroundColor: Theme.PURPLE_COLOR,
              '@media (max-width: 767px)': {
                right: 135,
              },
            },
          ]} />
      </div>
      {info.service ? (
        <div style={styles.fieldWrapper}>
          <span style={styles.label}>
            服務項目
          </span>
          ：
          <span style={styles.text}>
            {info.service}
          </span>
        </div>
      ) : null}
      {info.phoneModel ? (
        <div style={styles.fieldWrapper}>
          <span style={styles.label}>
            手機型號
          </span>
          ：
          <span style={styles.text}>
            {info.phoneModel}
          </span>
        </div>
      ) : null}
      {info.IME ? (
        <div style={styles.fieldWrapper}>
          <span style={styles.label}>
            IME後五碼
          </span>
          ：
          <span style={styles.text}>
            {info.IME}
          </span>
        </div>
      ) : null}
      {info.phoneLastSix ? (
        <div style={styles.fieldWrapper}>
          <span style={styles.label}>
            手機末六碼
          </span>
          ：
          <span style={styles.text}>
            {info.phoneLastSix}
          </span>
        </div>
      ) : null}
      {info.store ? (
        <div style={styles.fieldWrapper}>
          <span style={styles.label}>
            門市名稱
          </span>
          ：
          <span style={styles.text}>
            {info.store}
          </span>
        </div>
      ) : null}
      {info.employeeCode ? (
        <div style={styles.fieldWrapper}>
          <span style={styles.label}>
            包膜師工號
          </span>
          ：
          <span style={styles.text}>
            {info.employeeCode}
          </span>
        </div>
      ) : null}
      <div style={styles.fieldWrapper}>
        <span style={styles.label}>
          審核狀態
        </span>
        ：
        <span style={styles.text}>
          {info.isApproved ? '已確認生效' : '尚未生效'}
        </span>
      </div>
    </div>
  );
}

export default radium(ServiceCard);
