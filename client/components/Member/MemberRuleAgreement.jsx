// @flow
/* eslint no-undef: 0 */
import React, { PureComponent } from 'react';
import radium from 'radium';
import { Field } from 'redux-form';
// config
import Theme from '../../styles/Theme.js';
// components
import Checkbox from '../Form/Checkbox.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '0 4px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: 'auto',
    height: 'auto',
  },
  checkbox: {
    width: 32,
    height: 32,
  },
  labelWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '0 12px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  label: {
    fontSize: 16,
    letterSpacing: 1,
    color: Theme.DEFAULT_TEXT_COLOR,
    padding: '0 4px',
  },
  link: {
    width: 'auto',
    height: 'auto',
    padding: '0 6px',
    fontSize: 16,
    color: Theme.PURPLE_COLOR,
    letterSpacing: 1,
    border: 0,
    outline: 0,
    textDecoration: 'none',
    backgroundColor: 'transparent',
    cursor: 'pointer',
    ':hover': {
      opacity: 0.9,
    },
  },
};

type Props = {

};

class MemberRuleAgreement extends PureComponent<Props> {
  render() {
    return (
      <div style={styles.wrapper}>
        <div style={styles.fieldWrapper}>
          <Field
            name="isAgree"
            component={Checkbox}
            style={styles.checkbox} />
        </div>
        <div style={styles.labelWrapper}>
          <span style={styles.label}>
            已閱讀並同意
          </span>
          <a
            rel="noopener noreferrer"
            href={`${HOST}/customer/rights`}
            target="_blank"
            style={styles.link}>
            artmo會員權益說明
          </a>
        </div>
      </div>
    );
  }
}

export default radium(MemberRuleAgreement);
