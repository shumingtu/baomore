// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
// config
import {
  PAYED_STATUS_SPEC,
  SHIPPING_STATUS_SPEC,
} from '../../shared/Global.js';
import Theme from '../../styles/Theme.js';
import arrowDown from '../../static/images/arrow-down-icon.png';
import OrderItemField from './OrderItemField.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: 12,
    borderRadius: 12,
    marginBottom: 16,
    backgroundColor: '#fff',
    boxShadow: Theme.CLIENT_BLOCK_SHADOW,
  },
  collapseWrapper: {
    width: '100%',
    height: 'auto',
    padding: '0 4px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
    border: 0,
    outline: 0,
    margin: 0,
    cursor: 'pointer',
  },
  collapseLabel: {
    fontSize: 16,
    fontWeight: 400,
    letterSpacing: 1,
    color: Theme.BLACK_COLOR,
  },
  collapseOrderNameWrapper: {
    flex: 1,
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  collapseOrderName: {
    fontSize: 16,
    fontWeight: 400,
    letterSpacing: 1,
    color: Theme.BLACK_COLOR,
  },
  collapseIcon: {
    width: 16,
    height: 16,
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundImage: `url(${arrowDown})`,
    transition: 'transform 0.2s ease-in',
  },
  detailWrapper: {
    width: '100%',
    height: 'auto',
    padding: 24,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  groupImageWrapper: {
    width: '100%',
    height: 'auto',
    padding: '8px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  imageWrapper: {
    width: '100%',
    height: 'auto',
    padding: '8px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  groupLabel: {
    width: 'auto',
    minWidth: 90,
    fontSize: 16,
    fontWeight: 400,
    letterSpacing: 1,
    color: Theme.BLACK_COLOR,
  },
  groupImage: {
    width: 200,
    height: 280,
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
  separateLine: {
    width: '100%',
    height: 1,
    backgroundColor: Theme.BLACK_COLOR,
    margin: '24px 0',
  },
};

type Props = {
  order: {
    id: string,
    picture: string,
    name: string,
    phone: string,
    email: string,
    date: string,
    time: string,
    store: {
      id: number,
      name: string,
      phone: string,
      address: string,
    },
    employee: {
      id: number,
      name: string,
    },
    createdAt: string,
    payedStatus: string,
    shippingStatus: string,
  },
};

type State = {
  showDetail: boolean,
};

class MemberOrderItem extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      showDetail: false,
    };
  }

  render() {
    const {
      order,
    } = this.props;

    const {
      showDetail,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        <button
          type="button"
          onClick={() => this.setState({ showDetail: !showDetail })}
          style={styles.collapseWrapper}>
          <span style={styles.collapseLabel}>
            訂單編號：
          </span>
          <div style={styles.collapseOrderNameWrapper}>
            <span style={styles.collapseOrderName}>
              {order.id || null}
            </span>
          </div>
          <div
            style={[
              styles.collapseIcon,
              showDetail && {
                transform: 'rotate(180deg)',
              },
            ]} />
        </button>
        {showDetail ? (
          <div style={styles.detailWrapper}>
            <OrderItemField
              label="付款狀態："
              value={PAYED_STATUS_SPEC[order.payedStatus] || null} />
            <OrderItemField
              label="送貨狀態："
              value={SHIPPING_STATUS_SPEC[order.shippingStatus] || null} />
            <div style={styles.separateLine} />
            <OrderItemField
              label="訂購會員："
              value={order.name || null} />
            <OrderItemField
              label="訂購日期："
              value={order.createdAt || null} />
            <OrderItemField
              label="聯絡電話："
              value={order.phone || null} />
            <OrderItemField
              label="預約門市："
              value={order.store ? order.store.name : null} />
            <OrderItemField
              label="預約日期："
              value={order.date || null} />
            <OrderItemField
              label="預約時段："
              value={order.time || null} />
            <div style={styles.groupImageWrapper}>
              <span style={styles.groupLabel}>
                訂購樣式：
              </span>
              <div style={styles.imageWrapper}>
                <div
                  style={[
                    styles.groupImage,
                    {
                      backgroundImage: order.picture ? `url(${order.picture})` : null,
                    },
                  ]} />
              </div>
            </div>
            <OrderItemField
              label="訂購金額："
              value={order.price ? `$${order.price}` : null} />
          </div>
        ) : null}
      </div>
    );
  }
}

export default radium(MemberOrderItem);
