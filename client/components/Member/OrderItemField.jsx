// @flow
import React, { memo } from 'react';

import Theme from '../../styles/Theme.js';

const styles = {
  detailGroupWrapper: {
    width: '100%',
    height: 'auto',
    padding: '8px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  groupLabel: {
    width: 'auto',
    minWidth: 90,
    fontSize: 16,
    fontWeight: 400,
    letterSpacing: 1,
    color: Theme.BLACK_COLOR,
  },
  groupText: {
    fontSize: 16,
    fontWeight: 500,
    letterSpacing: 1,
    padding: '0 4px',
    color: Theme.BLACK_COLOR,
  },
};

type Props = {
  label: string,
  value: string,
};

function OrderItemField({
  label,
  value,
}: Props) {
  return (
    <div style={styles.detailGroupWrapper}>
      <span style={styles.groupLabel}>
        {label || null}
      </span>
      <span style={styles.groupText}>
        {value || null}
      </span>
    </div>
  );
}

export default memo(OrderItemField);
