// @flow
import * as React from 'react';
import radium from 'radium';
import { Link as link } from 'react-router-dom';

import Theme from '../../styles/Theme.js';

const Link = radium(link);

const styles = {
  placement: {
    width: '100%',
    maxWidth: 480,
    height: 'auto',
    padding: 12,
  },
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: 16,
    borderRadius: 12,
    backgroundColor: '#fff',
    boxShadow: Theme.CLIENT_BLOCK_SHADOW,
  },
  paragraph: {
    fontSize: 16,
    fontWeight: 400,
    color: Theme.BLACK_COLOR,
    letterSpacing: 1,
    lineHeight: 1.6,
    textAlign: 'center',
    whiteSpace: 'pre-line',
    '@media (max-width: 767px)': {
      whiteSpace: 'normal',
    },
  },
  link: {
    color: Theme.PURPLE_COLOR,
    textDecoration: 'none',
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    cursor: 'pointer',
  },
};

type Props = {
  type: 'noToken' | 'noService' | 'noOrder',
};

function componentRenderer(type) {
  switch (type) {
    case 'noToken':
      return (
        <p style={styles.paragraph}>
          您尚未登入本網站，請至
          <Link
            to={{ pathname: '/member/login' }}
            style={styles.link}>
            會員註冊/登入
          </Link>
          {`
            登入後，即可使用本功能
          `}
        </p>
      );
    case 'noService':
      return (
        <p style={styles.paragraph}>
          {`親愛的會員您好，您尚未登入服務資料，
            請洽詢您的服務人員以確保您的權利。
            尚未體驗artmo服務之會員，請至`}
          <Link
            to={{ pathname: '/customize/start' }}
            style={styles.link}>
            客製服務
          </Link>
          {`，
            依您的喜好挑選商品，artmo將為您服務！`}
        </p>
      );
    case 'noOrder':
      return (
        <p style={styles.paragraph}>
          {`親愛的會員您好，您尚未擁有訂單資料，
            請洽詢您的服務人員以確保您的權利。
            尚未體驗artmo服務之會員，請至`}
          <Link
            to={{ pathname: '/customize/start' }}
            style={styles.link}>
            客製服務
          </Link>
          {`，
            依您的喜好挑選商品，artmo將為您服務！`}
        </p>
      );
    default:
      return null;
  }
}

function NoDataPlaceholder({
  type,
}: Props) {
  return (
    <div style={styles.placement}>
      <div style={styles.wrapper}>
        {componentRenderer(type)}
      </div>
    </div>
  );
}

export default radium(NoDataPlaceholder);
