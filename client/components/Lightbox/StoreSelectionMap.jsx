// @flow
/* eslint react/no-did-update-set-state: 0 */
import * as React from 'react';
import radium from 'radium';
import { graphql } from 'react-apollo';
// config
import Theme from '../../styles/Theme.js';
import {
  GET_STORES,
} from '../../queries/Customize.js';
// static
import deleteIcon from '../../static/images/delete-icon.png';
// component
import FormLabel from '../Form/FormLabel.jsx';
import CustomInfoWindow from './Components/CustomInfoWindow.jsx';

const styles = {
  wrapper: {
    width: 640,
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      width: '80vw',
    },
  },
  mapPlacement: {
    width: '100%',
    height: 'auto',
    marginTop: 12,
    position: 'relative',
  },
  map: {
    width: '100%',
    height: '60vh',
    '@media (max-width: 767px)': {
      height: '65vh',
    },
  },
  infoWindowWrapper: {
    position: 'absolute',
    zIndex: 1,
    left: 40,
    top: '50%',
    width: 'auto',
    height: 'auto',
    padding: 12,
    backgroundColor: '#fff',
    borderRadius: 8,
    boxShadow: Theme.BLOCK_SHADOW,
    '@media (max-width: 767px)': {
      left: '5%',
      top: '30%',
    },
  },
  infoInner: {
    width: '100%',
    height: '100%',
    position: 'relative',
  },
  closeWrapper: {
    position: 'absolute',
    top: 4,
    right: 4,
    width: 16,
    height: 16,
    outline: 0,
    border: 0,
    padding: 0,
    backgroundColor: 'transparent',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundImage: `url(${deleteIcon})`,
    cursor: 'pointer',
    ':hover': {
      opacity: 0.87,
    },
  },
};

type Props = {
  lat: number,
  lng: number,
  zoom: number,
  cityId: number | string | null,
  storeId: number | string | null,
  channelId: number | string | null,
  districtId: number | string | null,
  stores: Array<{
    id: number,
    name: string,
    address: string,
    latitude: string,
    longitude: string,
  }>,
  onClose: Function,
  onSelected: Function,
  wrapperStyle?: Object,
  mapStyle?: Object,
  infoStyle?: Object,
  hideLabel?: boolean,
  shouldClearInfoWindowWhenAnyUpdate?: boolean,
  shouldUpdateCenterPosition?: boolean,
};

type State = {
  currentInfoWindow: React.Node,
};

let storeMap = null;
let markersCache = [];

const DEFAULT_OPTION = {
  lat: 25.042708,
  lng: 121.546707,
  zoom: 14,
};

class StoreSelectionMap extends React.PureComponent<Props, State> {
  static defaultProps = {
    wrapperStyle: null,
    mapStyle: null,
    infoStyle: null,
    hideLabel: false,
    shouldUpdateCenterPosition: false,
    shouldClearInfoWindowWhenAnyUpdate: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      currentInfoWindow: null,
    };
  }

  componentDidMount() {
    const {
      lat,
      lng,
      zoom,
      stores,
    } = this.props;

    storeMap = new google.maps.Map(document.getElementById('map-container'), {
      center: {
        lat: lat || DEFAULT_OPTION.lat,
        lng: lng || DEFAULT_OPTION.lng,
      },
      zoom: zoom || DEFAULT_OPTION.zoom,
      fullscreenControl: false,
    });

    this.mapInit(stores);
  }

  componentDidUpdate(prevProps) {
    const {
      stores,
      shouldUpdateCenterPosition,
      shouldClearInfoWindowWhenAnyUpdate,
      lat,
      lng,
      zoom,
      cityId,
      districtId,
      channelId,
      storeId,
    } = this.props;

    if (shouldUpdateCenterPosition) {
      if (lat !== prevProps.lat || lng !== prevProps.lng || zoom !== prevProps.zoom) {
        const newPos = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
        storeMap.setCenter(newPos);
        storeMap.setZoom(zoom);
      }
    }

    if (shouldClearInfoWindowWhenAnyUpdate) {
      if (cityId !== prevProps.cityId
        || districtId !== prevProps.districtId
        || channelId !== prevProps.channelId
        || storeId !== prevProps.storeId
      ) {
        this.setState({
          currentInfoWindow: null,
        });
      }
    }

    if (stores !== prevProps.stores && stores) {
      this.mapInit(stores);
    }
  }

  mapInit(markers = []) {
    if (markersCache.length) {
      markersCache.map(m => m.setMap(null));
    }

    markersCache = markers.map(m => new google.maps.Marker({
      id: m.id,
      name: m.name || null,
      description: m.address || null,
      position: new google.maps.LatLng(parseFloat(m.latitude), parseFloat(m.longitude)),
      map: storeMap,
    }));

    markersCache.forEach(m => m.addListener('click', () => {
      storeMap.setCenter(m.getPosition());

      this.setState({
        currentInfoWindow: this.renderInfoWindow(markers.find(mark => mark.id === m.id)),
      });
    }));
  }

  renderInfoWindow(marker) {
    const {
      onClose,
      onSelected,
    } = this.props;

    if (marker) {
      return (
        <CustomInfoWindow
          info={marker}
          onChange={onSelected ? () => {
            onSelected(marker.id);

            if (onClose) {
              onClose();
            }
          } : null} />
      );
    }

    return null;
  }

  render() {
    const {
      wrapperStyle,
      mapStyle,
      infoStyle,
      hideLabel,
    } = this.props;

    const {
      currentInfoWindow,
    } = this.state;

    return (
      <div
        style={[
          styles.wrapper,
          wrapperStyle,
        ]}>
        {!hideLabel ? (
          <FormLabel label="請選擇鄰近的特約門市" />
        ) : null}
        <div
          style={[
            styles.mapPlacement,
            hideLabel && {
              marginTop: 0,
            },
          ]}>
          <div
            id="map-container"
            style={[
              styles.map,
              mapStyle,
            ]} />
          {currentInfoWindow ? (
            <div
              style={[
                styles.infoWindowWrapper,
                infoStyle,
              ]}>
              <button
                key="info-window-close"
                type="button"
                onClick={() => this.setState({ currentInfoWindow: null })}
                style={styles.closeWrapper} />
              <div style={styles.infoInner}>
                {currentInfoWindow}
              </div>
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}

const queryHook = graphql(GET_STORES, {
  options: ({
    cityId,
    districtId,
    storeId,
    channelId,
  }) => ({
    variables: {
      cityId: cityId ? parseInt(cityId, 10) : null,
      districtId: districtId ? parseInt(districtId, 10) : null,
      channelId: channelId ? parseInt(channelId, 10) : null,
      storeId: storeId ? parseInt(storeId, 10) : null,
      isForClient: true,
    },
  }),
  props: ({
    data: {
      stores,
    },
  }) => ({
    stores: stores || [],
  }),
});

export default queryHook(
  radium(
    StoreSelectionMap
  )
);
