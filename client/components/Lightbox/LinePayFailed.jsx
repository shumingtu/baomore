// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { withRouter } from 'react-router-dom';
// config
import Theme from '../../styles/Theme.js';
import failedIcon from '../../static/images/fail-icon.png';
// component
import Button from '../Global/Button.jsx';

const styles = {
  wrapper: {
    width: 400,
    height: '100%',
    textAlign: 'center',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: 24,
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      width: '85vw',
      maxWidth: 400,
    },
  },
  title: {
    width: 'auto',
    height: 'auto',
    fontSize: 18,
    fontWeight: 600,
    letterSpacing: 1.8,
    color: Theme.DEFAULT_TEXT_COLOR,
  },
  failedIcon: {
    width: 180,
    height: 180,
    margin: '36px 0',
    backgroundImage: `url(${failedIcon})`,
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
  },
  descWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  desc: {
    width: 'auto',
    height: 'auto',
    fontSize: 15,
    letterSpacing: 1,
    color: Theme.DEFAULT_TEXT_COLOR,
  },
  myLink: {
    width: 'auto',
    height: 'auto',
    fontSize: 15,
    letterSpacing: 1,
    padding: '0 4px',
    backgroundColor: 'transparent',
    border: 0,
    outline: 0,
    textDecoration: 'underline',
    color: Theme.BUTTON_BACKGROUND_COLOR,
    cursor: 'pointer',
  },
  button: {
    width: '100%',
    height: 48,
  },
};

type Props = {
  onClose: Function,
  errorCode: string,
  history: {
    push: Function,
  },
};

class LinePayFailed extends PureComponent<Props> {
  render() {
    const {
      onClose,
      history,
      errorCode,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <span style={styles.title}>
          付款失敗
        </span>
        <div style={styles.failedIcon} />
        <div style={styles.descWrapper}>
          <span style={styles.desc}>
            {errorCode ? `錯誤代碼：${errorCode}, ` : null}
            請重新嘗試付款
          </span>
        </div>
        <div style={styles.button}>
          <Button
            label="重新付款"
            onClick={() => {
              history.push('/payment');
              onClose();
            }} />
        </div>
        <div style={styles.descWrapper}>
          <span style={styles.desc}>至</span>
          <button
            type="button"
            onClick={() => {
              history.push('/payment');
              onClose();
            }}
            style={styles.myLink}>
            客製化產品
          </button>
          <span style={styles.desc}>
            查看您的作品
          </span>
        </div>
      </div>
    );
  }
}

export default withRouter(
  radium(
    LinePayFailed
  )
);
