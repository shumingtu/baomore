// @flow
import React from 'react';
import radium from 'radium';
// config
import Theme from '../../../styles/Theme.js';
// components
import Button from '../../Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    maxWidth: 260,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      maxWidth: 200,
    },
  },
  name: {
    fontSize: 18,
    fontWeight: 600,
    color: Theme.DEFAULT_TEXT_COLOR,
    letterSpacing: 1.8,
    paddingBottom: 4,
  },
  description: {
    fontSize: 13,
    lineHeight: 1.618,
    letterSpacing: 1,
    padding: '4px 0',
    color: Theme.GRAY_TEXT_COLOR,
    wordBreak: 'break-word',
    whiteSpace: 'pre-line',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnWrapper: {
    width: 200,
    height: 40,
  },
};

type Props = {
  onChange: Function,
  info: {
    id: number | string,
    name: string,
    phone: string,
    address: string,
  },
};

function CustomInfoWindow({
  onChange,
  info,
}: Props) {
  return (
    <div style={styles.wrapper}>
      {info && info.name ? (
        <span style={styles.name}>
          {info.name}
        </span>
      ) : null}
      {info && info.phone ? (
        <span style={styles.description}>
          {info.phone}
        </span>
      ) : null}
      {info && info.address ? (
        <span style={styles.description}>
          {info.address}
        </span>
      ) : null}
      {onChange ? (
        <div style={styles.functionWrapper}>
          <div style={styles.btnWrapper}>
            <Button
              type="button"
              label="選擇門市"
              onClick={onChange} />
          </div>
        </div>
      ) : null}
    </div>
  );
}

export default radium(CustomInfoWindow);
