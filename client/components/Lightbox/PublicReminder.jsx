// @flow
import React from 'react';
import radium from 'radium';
import {
  reduxForm,
  Field,
} from 'redux-form';
import { Link } from 'react-router-dom';

// components
import Checkbox from '../Form/Checkbox.jsx';

const FORM_NAME = 'CUSTOMIZE_PUBLIC_FORM';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '0 24px 35px',
    maxWidth: 680,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      padding: '0 16px',
    },
  },
  title: {
    fontSize: 45,
    fontWeight: 700,
    margin: '39px 0',
    textAlign: 'center',
    color: 'rgb(173, 0, 129)',
    '@media (max-width: 767px)': {
      fontSize: 36,
      margin: '16px 0',
    },
  },
  mainScroll: {
    width: '100%',
    height: 'auto',
    maxHeight: '50vh',
    overflowX: 'hidden',
    overflowY: 'auto',
  },
  p: {
    fontSize: 17,
    fontWeight: 400,
    margin: '12px 0',
    color: 'rgb(34, 23, 20)',
    wordBreak: 'break-word',
    whiteSpace: 'pre-line',
  },
  highlight: {
    fontSize: 17,
    fontWeight: 700,
    color: 'rgb(173, 0, 129)',
  },
  link: {
    fontSize: 17,
    fontWeight: 700,
    border: 0,
    color: 'rgb(173, 0, 129)',
    textDecoration: 'underline',
    backgroundColor: 'transparent',
    cursor: 'pointer',
  },
  inputGroup: {
    width: '100%',
    height: 'auto',
    padding: '4px 0',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  checkbox: {
    width: 30,
    height: 30,
    minWidth: 30,
    border: '2px solid rgb(89, 87, 87)',
    '@media (max-width: 767px)': {
      width: 20,
      height: 20,
      minWidth: 20,
    },
  },
  checkboxLabel: {
    fontSize: 17,
    color: 'rgb(89, 87, 87)',
  },
  remind: {
    fontSize: 12.5,
    color: 'rgb(113, 112, 113)',
  },
};

type Props = {
  onClose: Function,
  onConfirm: Function,
  onDeny: Function,
};

function PublicReminder({
  onClose,
  onConfirm,
  onDeny,
}: Props) {
  return (
    <div style={styles.wrapper}>
      <h2 style={styles.title}>
        客製化包膜授權說明
      </h2>
      <div style={styles.mainScroll}>
        <p style={styles.p}>
          使用者同意將其所著作並享有著作財產權之製作資料授權artmo行使。
          同意聲明請詳閱
          <Link
            target="_blank"
            to={{ pathname: '/customer/disclaim' }}
            style={styles.link}>
            【artmo個人化使用條款及免責條款】
          </Link>
          ，同意後本公司將於本站開放此次下單之圖片供其他使用者做素材選擇及產品製作。
        </p>
        <p style={styles.p}>
          使用者在本網站填寫之物件訊息、相關資料、上傳圖片等行為，純屬用戶個人行
          為，本網站對其內容之真實性或完整性不負有任何責任。
        </p>
        <div style={styles.inputGroup}>
          <Field
            name="isAgree"
            label="本人已詳閱【artmo個人化使用條款及免責條款】並同意"
            onClick={() => {
              if (onConfirm) {
                onConfirm();
              }
              onClose();
            }}
            component={Checkbox}
            style={styles.checkbox}
            labelStyle={styles.checkboxLabel} />
        </div>
        <div
          style={[
            styles.inputGroup,
            {
              padding: '4px 0 4px 38px',
              '@media (max-width: 767px)': {
                padding: '4px 0 4px 27px',
              },
            },
          ]}>
          <span style={styles.remind}>
            再次提醒您於訂購前務必確認您對相關商品聲明已充分事前審閱並瞭解因本商品性質特殊，可能會因此而影響您契約解除權利之行使。
          </span>
        </div>
        <div style={styles.inputGroup}>
          <Field
            name="isDisagree"
            label="不同意"
            onClick={() => {
              if (onDeny) {
                onDeny();
              }
              onClose();
            }}
            component={Checkbox}
            style={styles.checkbox}
            labelStyle={styles.checkboxLabel} />
        </div>
      </div>
    </div>
  );
}

const formHook = reduxForm({
  form: FORM_NAME,
});

export default formHook(
  radium(
    PublicReminder
  )
);
