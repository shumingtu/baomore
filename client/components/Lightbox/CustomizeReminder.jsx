// @flow
import React from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import {
  reduxForm,
  Field,
  formValueSelector,
} from 'redux-form';
import { Link } from 'react-router-dom';

// components
import Checkbox from '../Form/Checkbox.jsx';

const FORM_NAME = 'CUSTOMIZE_REMINDER_FORM';
const selector = formValueSelector(FORM_NAME);

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '0 24px',
    maxWidth: 680,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      padding: '0 16px',
    },
  },
  title: {
    fontSize: 45,
    fontWeight: 700,
    margin: '39px 0',
    color: 'rgb(173, 0, 129)',
    '@media (max-width: 767px)': {
      margin: '16px 0',
    },
  },
  mainScroll: {
    width: '100%',
    height: 'auto',
    maxHeight: '50vh',
    overflowX: 'hidden',
    overflowY: 'auto',
  },
  p: {
    fontSize: 17,
    fontWeight: 400,
    margin: '12px 0',
    color: 'rgb(34, 23, 20)',
    wordBreak: 'break-word',
    whiteSpace: 'pre-line',
  },
  highlight: {
    fontSize: 17,
    fontWeight: 700,
    color: 'rgb(173, 0, 129)',
  },
  link: {
    fontSize: 17,
    fontWeight: 700,
    border: 0,
    color: 'rgb(173, 0, 129)',
    textDecoration: 'underline',
    backgroundColor: 'transparent',
    cursor: 'pointer',
  },
  inputGroup: {
    width: '100%',
    height: 'auto',
    padding: '4px 0',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  checkbox: {
    width: 30,
    height: 30,
    minWidth: 30,
    border: '2px solid rgb(89, 87, 87)',
    '@media (max-width: 767px)': {
      width: 20,
      height: 20,
      minWidth: 20,
    },
  },
  checkboxLabel: {
    fontSize: 17,
    color: 'rgb(89, 87, 87)',
  },
  remind: {
    fontSize: 12.5,
    color: 'rgb(113, 112, 113)',
  },
  nextStep: {
    width: 'auto',
    height: 'auto',
    margin: '35px 0',
    padding: '6px 16px',
    textAlign: 'center',
    fontSize: 17,
    fontWeight: 700,
    color: '#fff',
    backgroundColor: 'rgb(113, 112, 113)',
    border: '1px solid rgb(113, 112, 113)',
    borderRadius: 12,
    outline: 0,
    opacity: 0.6,
    '@media (max-width: 767px)': {
      margin: '16px 0',
    },
  },
  nextStepActived: {
    backgroundColor: 'transparent',
    border: '1px solid rgb(173, 0, 129)',
    color: 'rgb(173, 0, 129)',
    cursor: 'pointer',
    opacity: 1,
  },
};

type Props = {
  onClose: Function,
  isAgree: boolean,
};

function CustomizeReminder({
  onClose,
  isAgree,
}: Props) {
  return (
    <div style={styles.wrapper}>
      <h2 style={styles.title}>
        注意事項
      </h2>
      <div className="hide-scrollbar" style={styles.mainScroll}>
        <p style={styles.p}>
          包膜產品除artmo設計款、授權款有浮雕效果外，
          <span style={styles.highlight}>
            客製化包膜皆無浮雕效果
          </span>
          ，客製化包膜請詳閱
          <Link
            target="_blank"
            to={{ pathname: '/customer/disclaim' }}
            style={styles.link}>
            【artmo個人化使用條款及免責條款】
          </Link>
          及
          <Link
            target="_blank"
            to={{ pathname: '/description/transport' }}
            style={styles.link}>
            【購物說明】
          </Link>
          ，同意後進入製作頁面。
        </p>
        <p style={styles.p}>
          1.請於購買前確定您使用的圖片已被授權使用，若遭第三人聲明有侵犯他人利
          益時，或致使本公司蒙受損失，您將需要賠償本公司一切損失。
          2.使用與製作之圖片請您自行事前檢視該圖之檔案尺寸大小、格式、解析度相
          關資訊（上傳檔案接受JPG、PNG，建議解析度300dpi以上），產品不因用戶
          上傳之圖檔模糊而視為瑕疵或損壞，要求完美者請審慎下單。
        </p>
        <div style={styles.inputGroup}>
          <Field
            name="isAgree"
            label="本人已詳閱【artmo個人化使用條款及免責條款】及【購物說明】並同意"
            component={Checkbox}
            style={styles.checkbox}
            labelStyle={styles.checkboxLabel} />
        </div>
        <div
          style={[
            styles.inputGroup,
            {
              padding: '4px 0 4px 38px',
              '@media (max-width: 767px)': {
                padding: '4px 0 4px 27px',
              },
            },
          ]}>
          <span style={styles.remind}>
            再次提醒您於訂購前務必確認您對相關商品聲明已充分事前審閱並瞭解因本商品性質特殊，可能會因此而影響您契約解除權利之行使。
          </span>
        </div>
      </div>
      <button
        key="next-step-btn"
        type="button"
        disabled={!isAgree}
        onClick={() => onClose()}
        style={[
          styles.nextStep,
          isAgree && styles.nextStepActived,
        ]}>
        下一步
      </button>
    </div>
  );
}

const formHook = reduxForm({
  form: FORM_NAME,
});

const reduxHook = connect(
  state => ({
    isAgree: selector(state, 'isAgree'),
  }),
);

export default reduxHook(
  formHook(
    radium(
      CustomizeReminder
    )
  )
);
