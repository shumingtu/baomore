// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import qs from 'qs';
// config
import Theme from '../../styles/Theme.js';
import linePay from '../../static/images/linepay-logo.png';
import { LINE_PAY_BASE_LINK } from '../../shared/Global.js';

const styles = {
  wrapper: {
    width: 300,
    height: '100%',
    textAlign: 'center',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: 24,
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      width: '85vw',
      maxWidth: 300,
    },
  },
  title: {
    width: 'auto',
    height: 'auto',
    fontSize: 18,
    fontWeight: 600,
    letterSpacing: 1.8,
    wordBreak: 'break-word',
    color: Theme.DEFAULT_TEXT_COLOR,
  },
  mainWrapper: {
    width: '100%',
    height: 100,
    padding: '8px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  lineLink: {
    width: 120,
    height: 34,
    textDecoration: 'none',
  },
  linepay: {
    width: '100%',
    height: '100%',
    backgroundImage: `url(${linePay})`,
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
};

type Props = {
  onClose: Function,
  human: string,
  status: boolean,
  token: string,
};

class LinePayBox extends PureComponent<Props> {
  render() {
    const {
      status = false,
      human,
      token,
    } = this.props;

    const qsString = qs.stringify({
      token: token || null,
    }, {
      skipNulls: true,
    });

    return (
      <div style={styles.wrapper}>
        <span style={styles.title}>
          {human}
        </span>
        {status ? (
          <div style={styles.mainWrapper}>
            <a
              target="_self"
              href={`${LINE_PAY_BASE_LINK}?${qsString}`}
              style={styles.lineLink}>
              <div style={styles.linepay} />
            </a>
          </div>
        ) : null}
      </div>
    );
  }
}

export default radium(
  LinePayBox
);
