// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
// config
import Theme from '../../styles/Theme.js';
import lineLogin from '../../static/images/line-login-button.png';
import { LINE_LOGIN_CUSTOMIZE_PATH } from '../../shared/Global.js';

const styles = {
  wrapper: {
    width: 300,
    height: '100%',
    textAlign: 'center',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: 24,
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      width: '85vw',
      maxWidth: 300,
    },
  },
  title: {
    width: 'auto',
    height: 'auto',
    fontSize: 18,
    fontWeight: 600,
    letterSpacing: 1.8,
    color: Theme.DEFAULT_TEXT_COLOR,
  },
  mainWrapper: {
    width: '100%',
    height: 100,
    padding: '8px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  lineLink: {
    width: 152,
    height: 44,
    textDecoration: 'none',
  },
  lineLogin: {
    width: '100%',
    height: '100%',
    backgroundImage: `url(${lineLogin})`,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  },
};

type Props = {
  onClose: Function,
};

class LineLogin extends PureComponent<Props> {
  render() {
    return (
      <div style={styles.wrapper}>
        <span style={styles.title}>
          請先登入才能完成此操作
        </span>
        <div style={styles.mainWrapper}>
          <a
            target="_self"
            href={LINE_LOGIN_CUSTOMIZE_PATH}
            style={styles.lineLink}>
            <div style={styles.lineLogin} />
          </a>
        </div>
      </div>
    );
  }
}

export default radium(
  LineLogin
);
