// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { history } from '../../global.js';
// config
import Theme from '../../styles/Theme.js';
import { LINE_FRIEND_ID } from '../../shared/Global.js';
import successIcon from '../../static/images/success-icon.png';
// component
import Button from '../Global/Button.jsx';

const styles = {
  wrapper: {
    width: 400,
    height: '100%',
    textAlign: 'center',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: 24,
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      width: '85vw',
      maxWidth: 400,
    },
  },
  title: {
    width: 'auto',
    height: 'auto',
    fontSize: 18,
    fontWeight: 600,
    letterSpacing: 1.8,
    color: Theme.DEFAULT_TEXT_COLOR,
  },
  successIcon: {
    width: 180,
    height: 180,
    margin: '36px 0',
    backgroundImage: `url(${successIcon})`,
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
  },
  descWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  desc: {
    width: 'auto',
    height: 'auto',
    fontSize: 15,
    letterSpacing: 1,
    color: Theme.DEFAULT_TEXT_COLOR,
  },
  lineLink: {
    fontSize: 15,
    letterSpacing: 1,
    textDecoration: 'underline',
    padding: '0 4px',
    color: Theme.LINE_COLOR,
  },
  button: {
    width: '100%',
    height: 48,
  },
};

type Props = {
  onClose: Function,
  payConfirm: Function,
};

class LinePaySuccess extends PureComponent<Props> {
  componentDidMount() {
    const {
      payConfirm,
    } = this.props;

    payConfirm();
  }

  render() {
    const {
      onClose,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <span style={styles.title}>
          付款成功
        </span>
        <div style={styles.successIcon} />
        <div style={styles.descWrapper}>
          <span style={styles.desc}>加</span>
          <a
            rel="noopener noreferrer"
            href={`https://line.me/R/ti/p/${LINE_FRIEND_ID}`}
            target="_blank"
            style={styles.lineLink}>
            Line@
          </a>
          <span style={styles.desc}>
            好友接收行程提醒通知
          </span>
        </div>
        <div style={styles.button}>
          <Button
            label="查看訂單"
            onClick={() => {
              if (history) {
                history.push('/member/order');
              }
              onClose();
            }} />
        </div>
      </div>
    );
  }
}

export default radium(
  LinePaySuccess
);
