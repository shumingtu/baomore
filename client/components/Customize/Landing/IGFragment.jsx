// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import chunk from 'lodash/chunk';
// config
import Theme from '../../../styles/Theme.js';
// components
import ResizableImage from '../../Fragment/ResizableImage.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 24px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  titleWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottom: `2px solid ${Theme.YELLOW_COLOR}`,
    '@media (max-width: 767px)': {
      width: '75%',
    },
  },
  title: {
    fontSize: 48,
    fontWeight: 700,
    letterSpacing: 3,
    color: Theme.YELLOW_COLOR,
    '@media (max-width: 767px)': {
      fontSize: 26,
    },
  },
  imagesWrapper: {
    width: '100%',
    height: 'auto',
    padding: '32px 0',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    '@media (max-width: 767px)': {
      flexDirection: 'column-reverse',
      alignItems: 'center',
      justifyContent: 'flex-start',
    },
  },
  imagesPlacement: {
    flex: 1,
    height: '100%',
    margin: '0 4px',
  },
  groupWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  groupImgWrapper: {
    flex: 1,
    height: 'auto',
    margin: '0 4px',
  },
};

type Props = {
  fragment: {
    mainImage: {
      desktopImg: string,
      mobileImg: string,
    },
    groupImages: Array<{
      desktopImg: string,
      mobileImg: string,
    }>,
  },
};

type State = {
  showImages: boolean,
};

class IGFragment extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      showImages: false,
    };

    this.groupImgRef = React.createRef();
    this.mainImgRef = React.createRef();
  }

  componentDidMount() {
    this.setState({
      showImages: true,
    });
  }

  groupImgRef: { current: null | HTMLDivElement }

  mainImgRef: { current: null | HTMLDivElement }

  render() {
    const {
      fragment,
    } = this.props;

    const {
      showImages,
    } = this.state;

    if (!fragment) return null;

    const {
      mainImage,
      groupImages = [],
    } = fragment;

    const chunkGroupImages = chunk(groupImages, 2);

    return (
      <div style={styles.wrapper}>
        <div style={styles.titleWrapper}>
          <span style={styles.title}>
            SHOW ON IG
          </span>
        </div>
        <div style={styles.imagesWrapper}>
          <div style={styles.imagesPlacement}>
            {chunkGroupImages.map((c, idx) => (
              <div key={`${idx + 1}-c`} style={styles.groupWrapper}>
                {c.map(img => (
                  <div
                    key={img.desktopImg}
                    ref={this.groupImgRef}
                    style={styles.groupImgWrapper}>
                    {showImages ? (
                      <ResizableImage
                        widthRef={this.groupImgRef}
                        desktopRatio={1}
                        mobileRatio={1}
                        desktopImg={img.desktopImg}
                        mobileImg={img.mobileImg} />
                    ) : null}
                  </div>
                ))}
              </div>
            ))}
          </div>
          <div ref={this.mainImgRef} style={styles.imagesPlacement}>
            {showImages ? (
              <ResizableImage
                widthRef={this.mainImgRef}
                desktopRatio={1}
                mobileRatio={1}
                desktopImg={mainImage && mainImage.desktopImg}
                mobileImg={mainImage && mainImage.mobileImg} />
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}

export default radium(IGFragment);
