// @flow
import React, { Component } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Stage,
  Layer,
  Rect,
} from 'react-konva';
import {
  formValueSelector,
  change,
} from 'redux-form';
import { Query } from 'react-apollo';
// actions
import * as CustomizeActions from '../../actions/Customize.js';
// config
import {
  CANVAS_WIDTH,
  CANVAS_HEIGHT,
  MOBILE_CANVAS_WIDTH,
  MOBILE_CANVAS_HEIGHT,
} from '../../shared/PhoneConfig.js';
import { PHONE_CUSTOMIZE_FORM } from '../../shared/Form.js';
import { FETCH_MODEL_COLOR_LIST } from '../../queries/Brand.js';
import {
  isMobileMode,
  getCanvasHeight,
} from '../../helper/canvasHelper.js';
// phone component
import PhoneElement from './Phone/PhoneElement.jsx';
import PhoneCustomImage from './Phone/PhoneCustomImage.jsx';
import PhoneCustomText from './Phone/PhoneCustomText.jsx';

const styles = {
  wrapper: {
    width: CANVAS_WIDTH + 20,
    height: CANVAS_HEIGHT + 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  konvaWrapper: {
    backgroundColor: '#fff',
  },
};

const selector = formValueSelector(PHONE_CUSTOMIZE_FORM);

type Props = {
  images: Array<{
    id: number | string,
    type: string,
    src: string,
    x: number,
    y: number,
  }>,
  needWhiteBg: boolean,
  setImageAttr: Function,
  locked: boolean,
  phoneSelection: {
    brandId: number,
    modelId: number,
    colorId: number,
  },
  createRef: Function,
  cacheCustomizeClipSize: Function,
  clipBundle: {
    x: number,
    y: number,
    width: number,
    height: number,
  },
  canvasWidth: number,
};

type State = {

};

class PhoneKonva extends Component<Props, State> {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {
      locked,
      images = [],
      setImageAttr,
      needWhiteBg,
      phoneSelection,
      createRef,
      cacheCustomizeClipSize,
      clipBundle,
      canvasWidth,
    } = this.props;

    if (!phoneSelection) return null;

    const wrapImages = images && Array.isArray(images) ? images : [images];
    const mobileCanvasWidth = canvasWidth || MOBILE_CANVAS_WIDTH;
    const mobileCanvasHeight = canvasWidth ? getCanvasHeight(canvasWidth) : MOBILE_CANVAS_HEIGHT;
    const canvasSizeBundle = isMobileMode() ? {
      width: mobileCanvasWidth,
      height: mobileCanvasHeight,
    } : {
      width: CANVAS_WIDTH,
      height: CANVAS_HEIGHT,
    };

    const {
      modelId,
      colorId,
    } = phoneSelection;

    return (
      <div
        style={[
          styles.wrapper,
          {
            '@media (max-width: 767px)': {
              width: mobileCanvasWidth + 4,
              height: mobileCanvasHeight + 4,
            },
          },
        ]}>
        <Query
          query={FETCH_MODEL_COLOR_LIST}
          variables={{
            modelId: parseInt(modelId, 10),
          }}>
          {({
            data: {
              brandModelColorList = [],
            },
          }) => {
            const myColor = brandModelColorList.find(c => c.id === parseInt(colorId, 10));

            if (!myColor) return null;

            return (
              <Stage
                name="PhoneKonva"
                listening={!locked}
                width={canvasSizeBundle.width}
                height={canvasSizeBundle.height}
                style={styles.konvaWrapper}>
                <Layer
                  clipX={clipBundle.x}
                  clipY={clipBundle.y}
                  clipWidth={clipBundle.width}
                  clipHeight={clipBundle.height}>
                  <PhoneElement
                    type="background"
                    src={myColor.phoneBg}
                    width={canvasSizeBundle.width}
                    height={canvasSizeBundle.height}
                    onSetClip={({
                      clipX,
                      clipY,
                      clipWidth,
                      clipHeight,
                    }) => cacheCustomizeClipSize({
                      x: clipX || 0,
                      y: clipY || 0,
                      width: clipWidth || canvasSizeBundle.width,
                      height: clipHeight || canvasSizeBundle.height,
                    })} />
                </Layer>
                <Layer
                  clipX={clipBundle.x}
                  clipY={clipBundle.y}
                  clipWidth={clipBundle.width}
                  clipHeight={clipBundle.height}
                  ref={(r) => {
                    if (createRef) {
                      // output layer
                      createRef(r);
                    }
                  }}>
                  {needWhiteBg ? (
                    <Rect
                      fill="#fff"
                      width={canvasSizeBundle.width}
                      height={canvasSizeBundle.height} />
                  ) : null}
                  {wrapImages.map((i, idx) => {
                    switch (i.type) {
                      case 'image':
                      case 'emoji':
                        return (
                          <PhoneCustomImage
                            key={i.id}
                            allowMultiTouchEvent
                            image={i}
                            canvasSize={canvasSizeBundle}
                            onChange={v => setImageAttr([
                              ...images.slice(0, idx),
                              v,
                              ...images.slice(idx + 1),
                            ])} />
                        );
                      case 'text':
                        return (
                          <PhoneCustomText
                            key={i.id}
                            text={i}
                            canvasSize={canvasSizeBundle}
                            onChange={v => setImageAttr([
                              ...images.slice(0, idx),
                              v,
                              ...images.slice(idx + 1),
                            ])} />
                        );
                      case 'fixed':
                        return (
                          <PhoneCustomImage
                            key={i.id}
                            allowMultiTouchEvent={false}
                            image={i}
                            canvasSize={canvasSizeBundle}
                            onChange={v => setImageAttr({
                              ...images,
                              v,
                            })} />
                        );
                      default:
                        return null;
                    }
                  })}
                </Layer>
                <Layer>
                  <PhoneElement
                    type="mask"
                    src={myColor.phoneMask}
                    width={canvasSizeBundle.width}
                    height={canvasSizeBundle.height} />
                  <PhoneElement
                    type="cover"
                    src={myColor.phoneCover}
                    width={canvasSizeBundle.width}
                    height={canvasSizeBundle.height} />
                </Layer>
              </Stage>
            );
          }}
        </Query>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    images: selector(state, 'Main.Customize.images'),
    needWhiteBg: selector(state, 'Main.Customize.needWhiteBg'),
    phoneSelection: selector(state, 'Main.phoneSelection'),
    clipBundle: state.Customize.clipBundle,
  }),
  dispatch => bindActionCreators({
    ...CustomizeActions,
    setImageAttr: v => change(PHONE_CUSTOMIZE_FORM, 'Main.Customize.images', v),
  }, dispatch)
);

export default reduxHook(
  radium(
    PhoneKonva
  )
);
