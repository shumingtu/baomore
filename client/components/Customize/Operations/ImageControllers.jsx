// @flow
import React, {
  PureComponent,
  Fragment,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import {
  Field,
} from 'redux-form';

// config
import Theme from '../../../styles/Theme.js';
// static
import mouseIcon from '../../../static/images/mouse-pointer-icon.png';
// components
import FormLabel from '../../Form/FormLabel.jsx';
import RangeSlider from '../../Form/RangeSlider.jsx';
import LayerController from './LayerController.jsx';
import MobileWhiteBgSelectionBlock from './MobileWhiteBgSelectionBlock.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: 24,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#fff',
    boxShadow: Theme.BLOCK_SHADOW,
    '@media (max-width: 767px)': {
      width: '90%',
      padding: 12,
    },
  },
  mouseIcon: {
    width: 16,
    height: 16,
    backgroundImage: `url(${mouseIcon})`,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
  annotation: {
    fontSize: 14,
    textAlign: 'center',
    color: Theme.DEFAULT_TEXT_COLOR,
    padding: '16px 0',
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
  rangeSlideWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      flexDirection: 'row',
      alignItems: 'center',
      padding: '4px 0',
    },
  },
  groupWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
};

type Props = {
  currentSelectedImageId: number | string,
  currentSelectedTextId: number | string,
  currentSelectedEmojiId: number | string,
  fields: Object,
  type: 'image' | 'text' | 'emoji',
  scalable?: boolean,
  rotatable?: boolean,
  isSingleSource?: boolean,
};

class ImageController extends PureComponent<Props> {
  static defaultProps = {
    scalable: true,
    rotatable: true,
    isSingleSource: false,
  };

  render() {
    const {
      currentSelectedImageId,
      currentSelectedTextId,
      currentSelectedEmojiId,
      fields,
      type,
      scalable,
      rotatable,
      isSingleSource,
    } = this.props;

    if (type === 'image' && !currentSelectedImageId) return null;
    if (type === 'text' && !currentSelectedTextId) return null;
    if (type === 'emoji' && !currentSelectedEmojiId) return null;

    const currentSelectedId = () => {
      switch (type) {
        case 'image':
          return currentSelectedImageId;
        case 'text':
          return currentSelectedTextId;
        case 'emoji':
          return currentSelectedEmojiId;
        default:
          return null;
      }
    };

    const wrapFields = fields.getAll() || [];
    const currentFieldIdx = wrapFields.findIndex(f => f.id === currentSelectedId());

    return (
      <div style={styles.wrapper}>
        <Fragment>
          <div style={styles.mouseIcon} />
          <span style={styles.annotation}>
            請拖動手機上的圖片進行移動到理想的位置
          </span>
        </Fragment>
        {fields.map((image, index) => {
          if (index !== currentFieldIdx) return null;

          const label = type === 'text' ? '文字' : '圖片';

          return (
            <Fragment key={`${image}-scale-and-rotate`}>
              {scalable ? (
                <div style={styles.rangeSlideWrapper}>
                  <FormLabel label={`縮放${label}`} />
                  <Field
                    key={`${image}-scale`}
                    name={`${image}.scale`}
                    type="percentage"
                    min={0.1}
                    max={2}
                    step={0.1}
                    component={RangeSlider} />
                </div>
              ) : null}
              {rotatable ? (
                <div style={styles.rangeSlideWrapper}>
                  <FormLabel label={`旋轉${label}`} />
                  <Field
                    key={`${image}-rotate`}
                    name={`${image}.rotate`}
                    type="degree"
                    min={-179}
                    max={179}
                    step={1}
                    component={RangeSlider} />
                </div>
              ) : null}
              <div style={styles.groupWrapper}>
                <Field
                  name="needWhiteBg"
                  component={MobileWhiteBgSelectionBlock} />
                {!isSingleSource ? (
                  <LayerController
                    fields={fields}
                    index={index} />
                ) : null}
              </div>
            </Fragment>
          );
        })}
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    currentSelectedImageId: state.Customize.currentSelectedImageId,
    currentSelectedTextId: state.Customize.currentSelectedTextId,
    currentSelectedEmojiId: state.Customize.currentSelectedEmojiId,
  }),
);

export default reduxHook(
  radium(
    ImageController
  )
);
