// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

// themes
import Theme from '../../../styles/Theme.js';

const styles = {
  selectionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '4px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  selection: {
    flex: 1,
    height: 'auto',
    fontSize: 14,
    fontWeight: 500,
    letterSpacing: 1,
    marginRight: 12,
    padding: 12,
    textAlign: 'center',
    color: Theme.GRAY_TEXT_COLOR,
    border: `1px solid ${Theme.GRAY_TEXT_COLOR}`,
    borderRadius: 5,
    outline: 0,
    backgroundColor: 'transparent',
    cursor: 'pointer',
    opacity: 0.5,
    ':hover': {
      opacity: 0.88,
    },
    '@media (max-width: 767px)': {
      padding: '6px 8px',
      marginRight: 6,
    },
  },
  actived: {
    opacity: 1,
    border: `1px solid ${Theme.BUTTON_BACKGROUND_COLOR}`,
    color: Theme.BUTTON_BACKGROUND_COLOR,
  },
};

type Props = {
  input: {
    value: boolean,
    onChange: Function,
  },
  options: Array<{
    name: string,
    value: string | number,
  }>
};

class MultiSelectionBlock extends PureComponent<Props> {
  render() {
    const {
      input: {
        value = false,
        onChange,
      },
      options = [],
    } = this.props;

    return (
      <div style={styles.selectionWrapper}>
        {options.map(option => (
          <button
            key={`${option.value}-btn`}
            type="button"
            onClick={() => onChange(option.value)}
            style={[
              styles.selection,
              value === option.value && styles.actived,
            ]}>
            {option.name || '選項'}
          </button>
        ))}
      </div>
    );
  }
}

export default radium(MultiSelectionBlock);
