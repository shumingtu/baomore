// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

// config
import Theme from '../../../styles/Theme.js';
// components
import FormLabel from '../../Form/FormLabel.jsx';
// static
import layerTopIcon from '../../../static/images/layer-top-icon.png';
import layerBottomIcon from '../../../static/images/layer-bottom-icon.png';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '8px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      width: 100,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
    },
  },
  controllerWrapper: {
    width: '100%',
    height: 94,
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      width: 'auto',
      flex: 1,
      height: 32,
      padding: '0 6px',
    },
  },
  controllBtn: {
    width: 112,
    height: 69,
    padding: 8,
    margin: '0 24px 0 0',
    borderRadius: 5,
    border: `1px solid ${Theme.GRAY_TEXT_COLOR}`,
    outline: 0,
    backgroundColor: 'transparent',
    cursor: 'pointer',
    ':hover': {
      opacity: 0.8,
    },
    '@media (max-width: 767px)': {
      width: 'auto',
      height: '100%',
      flex: 1,
      margin: '0 6px',
    },
  },
  btnInnerWrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-around',
    '@media (max-width: 767px)': {
      flexDirection: 'row',
      justifyContent: 'flex-start',
    },
  },
  icon: {
    width: 18,
    height: 20,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  },
  desktopLabelWrapper: {
    width: 'auto',
    height: 'auto',
    display: 'block',
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
  mobileLabelWrapper: {
    display: 'none',
    '@media (max-width: 767px)': {
      display: 'block',
      width: 'auto',
      height: 'auto',
      paddingLeft: 12,
    },
  },
  label: {
    fontSize: 12,
    fontWeight: 500,
    letterSpacing: 1,
    color: Theme.GRAY_TEXT_COLOR,
    textAlign: 'center',
  },
};

type Props = {
  fields: Object,
  index: Number,
};

class LayerController extends PureComponent<Props> {
  goTop() {
    const {
      fields,
      index,
    } = this.props;

    fields.move(index, fields.length - 1);
  }

  goBottom() {
    const {
      fields,
      index,
    } = this.props;

    fields.move(index, 0);
  }

  render() {
    return (
      <div style={styles.wrapper}>
        <div style={styles.desktopLabelWrapper}>
          <FormLabel label="圖層排序" />
        </div>
        <div style={styles.controllerWrapper}>
          <button
            key="layer-top-button"
            type="button"
            onClick={() => this.goTop()}
            style={styles.controllBtn}>
            <div style={styles.btnInnerWrapper}>
              <div
                style={[
                  styles.icon,
                  {
                    backgroundImage: `url(${layerTopIcon})`,
                  },
                ]} />
              <div style={styles.desktopLabelWrapper}>
                <span style={styles.label}>
                  移到最上層
                </span>
              </div>
            </div>
          </button>
          <button
            key="layer-bottom-button"
            type="button"
            onClick={() => this.goBottom()}
            style={styles.controllBtn}>
            <div style={styles.btnInnerWrapper}>
              <div
                style={[
                  styles.icon,
                  {
                    backgroundImage: `url(${layerBottomIcon})`,
                  },
                ]} />
              <div style={styles.desktopLabelWrapper}>
                <span style={styles.label}>
                  移到最下層
                </span>
              </div>
            </div>
          </button>
        </div>
      </div>
    );
  }
}

export default radium(LayerController);
