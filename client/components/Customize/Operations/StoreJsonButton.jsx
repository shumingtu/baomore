// @flow
import React, { Component } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  getFormValues,
} from 'redux-form';
import { Mutation } from 'react-apollo';

// actions
import * as LightboxActions from '../../../actions/Lightbox.js';
import * as MemberActions from '../../../actions/Member.js';
// config
import Theme from '../../../styles/Theme.js';
import { PHONE_CUSTOMIZE_FORM } from '../../../shared/Form.js';
import { STORE_MEMBER_JSON } from '../../../mutations/Member.js';
import {
  STATUS_SUCCESS,
  STATUS_FAILED,
} from '../../../shared/StatusTypes.js';
import {
  LINE_LOGIN_LIGHTBOX,
} from '../../../shared/LightboxTypes.js';
// helper
import { canvasJsonToString } from '../../../helper/canvasHelper.js';
// static
import saveIcon from '../../../static/images/save-icon.png';

const styles = {
  storeJsonWrapper: {
    width: 70,
    height: 30,
    padding: 4,
    margin: 0,
    border: 0,
    backgroundColor: 'transparent',
    outline: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
    '@media (max-width: 1023px)': {
      width: 18,
      height: 20,
      backgroundImage: `url(${saveIcon})`,
      backgroundSize: 'contain',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
    },
  },
  storeText: {
    fontSize: 13,
    color: Theme.BUTTON_BACKGROUND_COLOR,
    '@media (max-width: 1023px)': {
      display: 'none',
    },
  },
};

type Props = {
  openStatusBox: Function,
  openLightbox: Function,
  cacheMemberCustomization: Function,
  formValues: Object,
  accessToken: string,
  memberStoredJSON: string,
};

class StoreJsonButton extends Component<Props> {
  render() {
    const {
      formValues,
      accessToken,
      openStatusBox,
      openLightbox,
      memberStoredJSON,
      cacheMemberCustomization,
    } = this.props;

    if (!formValues) return null;

    return (
      <Mutation mutation={STORE_MEMBER_JSON}>
        {storeJSON => (
          <button
            type="button"
            onClick={async () => {
              const newData = canvasJsonToString(formValues);

              cacheMemberCustomization(newData);

              if (accessToken) {
                const {
                  data,
                } = await storeJSON({
                  variables: {
                    json: newData,
                  },
                });

                if (data) {
                  openStatusBox({
                    type: STATUS_SUCCESS,
                    label: memberStoredJSON ? '已覆蓋上一次暫存作品' : '已暫存此作品',
                  });
                } else {
                  openStatusBox({
                    type: STATUS_FAILED,
                    label: '暫存失敗',
                  });
                }
              } else {
                openLightbox({
                  type: LINE_LOGIN_LIGHTBOX,
                });
              }
            }}
            style={styles.storeJsonWrapper}>
            <span style={styles.storeText}>
              暫存設計
            </span>
          </button>
        )}
      </Mutation>
    );
  }
}

const reduxHook = connect(
  state => ({
    accessToken: state.Member.accessToken,
    memberStoredJSON: state.Member.memberStoredJSON,
    formValues: getFormValues(PHONE_CUSTOMIZE_FORM)(state),
  }),
  dispatch => bindActionCreators({
    ...LightboxActions,
    ...MemberActions,
  }, dispatch)
);

export default reduxHook(
  radium(
    StoreJsonButton
  )
);
