// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Field,
} from 'redux-form';

// actions
import {
  setCurrentSelectionEmoji as setCurrentSelectionEmojiAction,
} from '../../../../actions/Customize.js';
// config
import Theme from '../../../../styles/Theme.js';
import SliderStyles from '../../../../styles/OperationSlider.js';
// context
import CustomizeEmojiContext from '../../../../context/CustomizeEmojiContext.js';
// helper
import { isMobileMode } from '../../../../helper/canvasHelper.js';
// static
import addIcon from '../../../../static/images/add-icon.png';

const styles = {
  ...SliderStyles,
  emojiWrapper: {
    width: '100%',
    height: '100%',
    outline: 0,
    border: 0,
    backgroundColor: 'transparent',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    cursor: 'pointer',
  },
  addAnnotation: {
    fontSize: 13,
    color: Theme.DEFAULT_TEXT_COLOR,
    opacity: 0.38,
    fontWeight: 600,
    paddingBottom: 6,
  },
};

type Props = {
  fields: Object,
  currentIndex: number,
  setCurrentSelectionEmoji: Function,
  currentSelectedEmojiId: number | string,
};

function EmojiRender({
  input: {
    value,
  },
  onClick,
  disabled,
}: {
  input: {
    value: string,
  },
  onClick: Function,
  disabled: boolean,
}) {
  return (
    <button
      type="button"
      onClick={onClick}
      disabled={disabled}
      style={[
        styles.emojiWrapper,
        {
          backgroundImage: value ? `url(${value})` : null,
        },
      ]} />
  );
}

const renderEmoji = radium(EmojiRender);

class EmojiSlider extends PureComponent<Props> {
  render() {
    const {
      fields,
      currentSelectedEmojiId,
      setCurrentSelectionEmoji,
      currentIndex,
    } = this.props;

    const wrapFields = (fields && fields.getAll()) || [];
    const emojiFields = wrapFields.filter(f => f.type === 'emoji');

    return (
      <div style={styles.slideWrapper}>
        <div style={styles.adderPlacement}>
          {/* desktop adder here */}
          <div style={styles.adderWrapper}>
            <div style={styles.addIconWrapper}>
              <img
                alt="新增表情"
                src={addIcon}
                style={styles.addIcon} />
            </div>
            <div style={styles.adderAnnotationWrapper}>
              <span style={styles.dragAnnotation}>
                新增表情
              </span>
            </div>
          </div>
          {/* mobile adder here */}
          <CustomizeEmojiContext.Consumer>
            {({
              onOpenAdderModule,
            }: {
              onOpenAdderModule: Function,
            }) => (
              <button
                key="open-emoji-adder-module"
                type="button"
                disabled={!onOpenAdderModule}
                onClick={onOpenAdderModule ? () => onOpenAdderModule() : null}
                style={[styles.adderWrapper, styles.mobileAdderWrapper]}>
                <div style={styles.addIconWrapper}>
                  <img
                    alt="新增表情"
                    src={addIcon}
                    style={styles.addIcon} />
                </div>
              </button>
            )}
          </CustomizeEmojiContext.Consumer>
        </div>
        <div className="hide-scrollbar" style={styles.mainSlidePlacement}>
          {fields.map((emoji, index) => {
            const wrapFieldValue = fields.get(index) || {};
            if (wrapFieldValue.type !== 'emoji') return null;

            const myEmojiId = wrapFieldValue.id || '';
            const myIndex = emojiFields.findIndex(t => t.id === myEmojiId);
            const isCurrentlySelected = currentSelectedEmojiId === myEmojiId;

            return (
              <div
                key={`emoji-${myEmojiId}-${index + 1}`}
                style={[
                  styles.slideItem,
                  isCurrentlySelected && styles.activedItem,
                  !isMobileMode() ? {
                    left: 5 + ((myIndex - currentIndex) * 130),
                  } : {
                    top: 5 + ((myIndex - currentIndex) * 70),
                  },
                ]}>
                <div style={styles.slideItemInner}>
                  <button
                    key={`emoji-${myEmojiId}-${index + 1}-delete`}
                    type="button"
                    onClick={() => {
                      fields.remove(index);
                      if (isCurrentlySelected) {
                        setCurrentSelectionEmoji(null);
                      }
                    }}
                    style={styles.deleteWrapper} />
                  <Field
                    name={`${emoji}.src`}
                    disabled={isCurrentlySelected}
                    onClick={() => setCurrentSelectionEmoji(myEmojiId)}
                    component={renderEmoji} />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    currentSelectedEmojiId: state.Customize.currentSelectedEmojiId,
  }),
  dispatch => bindActionCreators({
    setCurrentSelectionEmoji: setCurrentSelectionEmojiAction,
  }, dispatch)
);

export default reduxHook(
  radium(
    EmojiSlider
  )
);
