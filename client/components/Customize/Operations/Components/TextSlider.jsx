// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Field,
} from 'redux-form';

// actions
import {
  setCurrentSelectionOnText as setCurrentSelectionOnTextAction,
} from '../../../../actions/Customize.js';
// context
import CustomizeTextContext from '../../../../context/CustomizeTextContext.js';
// helper
import { isMobileMode } from '../../../../helper/canvasHelper.js';
// config
import SliderStyles from '../../../../styles/OperationSlider.js';
// static
import addIcon from '../../../../static/images/add-icon.png';

const styles = {
  ...SliderStyles,
  textWrapper: {
    width: '100%',
    height: '100%',
    outline: 0,
    border: 0,
    backgroundColor: 'transparent',
    cursor: 'pointer',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 14,
    color: '#000',
    textAlign: 'center',
  },
};

type Props = {
  currentIndex: number,
  fields: Object,
  setCurrentSelectionOnText: Function,
  currentSelectedTextId: number | string,
};

function TextRender({
  input: {
    value,
  },
  onClick,
  disabled,
}: {
  input: {
    value: string,
  },
  onClick: Function,
  disabled: boolean,
}) {
  return (
    <button
      type="button"
      onClick={onClick}
      disabled={disabled}
      style={styles.textWrapper}>
      <span style={styles.text}>
        {value || null}
      </span>
    </button>
  );
}

const renderText = radium(TextRender);

class TextSlider extends PureComponent<Props> {
  render() {
    const {
      fields,
      currentIndex,
      currentSelectedTextId,
      setCurrentSelectionOnText,
    } = this.props;

    const wrapFields = (fields && fields.getAll()) || [];
    const textFields = wrapFields.filter(f => f.type === 'text');

    return (
      <div style={styles.slideWrapper}>
        <div style={styles.adderPlacement}>
          {/* desktop adder here */}
          <div style={styles.adderWrapper}>
            <div style={styles.addIconWrapper}>
              <img
                alt="新增文字"
                src={addIcon}
                style={styles.addIcon} />
            </div>
            <div style={styles.adderAnnotationWrapper}>
              <span style={styles.dragAnnotation}>
                新增文字
              </span>
            </div>
          </div>
          {/* mobile adder here */}
          <CustomizeTextContext.Consumer>
            {({
              onOpenAdderModule,
            }: {
              onOpenAdderModule: Function,
            }) => (
              <button
                key="open-text-adder-module"
                type="button"
                disabled={!onOpenAdderModule}
                onClick={onOpenAdderModule ? () => onOpenAdderModule() : null}
                style={[styles.adderWrapper, styles.mobileAdderWrapper]}>
                <div style={styles.addIconWrapper}>
                  <img
                    alt="新增文字"
                    src={addIcon}
                    style={styles.addIcon} />
                </div>
              </button>
            )}
          </CustomizeTextContext.Consumer>
        </div>
        <div className="hide-scrollbar" style={styles.mainSlidePlacement}>
          {fields.map((text, index) => {
            const wrapFieldValue = fields.get(index) || {};
            if (wrapFieldValue.type !== 'text') return null;

            const myTextId = wrapFieldValue.id || '';
            const myIndex = textFields.findIndex(t => t.id === myTextId);
            const isCurrentlySelected = currentSelectedTextId === myTextId;

            return (
              <div
                key={`text-${myTextId}`}
                style={[
                  styles.slideItem,
                  isCurrentlySelected && styles.activedItem,
                  !isMobileMode() ? {
                    left: 5 + ((myIndex - currentIndex) * 130),
                  } : {
                    top: 5 + ((myIndex - currentIndex) * 70),
                  },
                ]}>
                <div style={styles.slideItemInner}>
                  <button
                    key={`text-${myTextId}-delete`}
                    type="button"
                    onClick={() => {
                      fields.remove(index);
                      if (isCurrentlySelected) {
                        setCurrentSelectionOnText(null);
                      }
                    }}
                    style={styles.deleteWrapper} />
                  <Field
                    name={`${text}.text`}
                    disabled={isCurrentlySelected}
                    onClick={() => setCurrentSelectionOnText(myTextId)}
                    component={renderText} />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    currentSelectedTextId: state.Customize.currentSelectedTextId,
  }),
  dispatch => bindActionCreators({
    setCurrentSelectionOnText: setCurrentSelectionOnTextAction,
  }, dispatch)
);

export default reduxHook(
  radium(
    TextSlider
  )
);
