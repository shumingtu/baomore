// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
// config
import Theme from '../../../../styles/Theme.js';
// helper
import { isMobileMode } from '../../../../helper/canvasHelper.js';

const styles = {
  slideWrapper: {
    width: '100%',
    height: 110,
    overflowY: 'hidden',
    overflowX: 'hidden',
    position: 'relative',
    '@media (max-width: 767px)': {
      height: '40vh',
      overflowY: 'auto',
      overflowX: 'hidden',
      backgroundColor: 'transparent',
    },
  },
  fixedWrapper: {
    height: 150,
    '@media (max-width: 767px)': {
      alignItems: 'flex-start',
    },
  },
  slideItem: {
    position: 'absolute',
    top: 5,
    left: 0,
    width: 100,
    height: 100,
    backgroundColor: '#fff',
    border: 0,
    borderRadius: 8,
    boxShadow: Theme.BLOCK_SHADOW,
    outline: 0,
    cursor: 'pointer',
    transition: 'left 0.2s ease-in',
  },
  fixedItem: {
    width: 100,
    height: 140,
    '@media (max-width: 767px)': {
      width: 60,
      height: 60,
      boxShadow: Theme.MOBILE_BLOCK_SHADOW,
    },
  },
  activedItem: {
    boxShadow: Theme.BUTTON_SHADOW,
    '@media (max-width: 767px)': {
      boxShadow: Theme.MOBILE_BUTTON_SHADOW,
    },
  },
  slideItemInner: {
    width: '100%',
    height: '100%',
    padding: 10,
    position: 'relative',
    '@media (max-width: 767px)': {
      padding: 6,
    },
  },
  fixedImageWrapper: {
    width: '100%',
    height: '100%',
    outline: 0,
    border: 0,
    backgroundColor: 'transparent',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    cursor: 'pointer',
  },
};

type Props = {
  type: 'fixed',
  images: Array<Object>,
  selectedId: string | number,
  onChange: Function,
  currentIndex: number,
};

class FixedImageSlider extends PureComponent<Props> {
  render() {
    const {
      type,
      onChange,
      images = [],
      selectedId,
      currentIndex,
    } = this.props;

    if (!onChange) return null;

    return (
      <div
        className="hide-scrollbar"
        style={[
          styles.slideWrapper,
          type === 'fixed' && styles.fixedWrapper,
        ]}>
        {images.map((i, idx) => (
          <div
            key={`emoji-${i.id}`}
            style={[
              styles.slideItem,
              type === 'fixed' && styles.fixedItem,
              selectedId && selectedId === i.id && styles.activedItem,
              !isMobileMode() ? {
                left: 5 + ((idx - currentIndex) * 130),
              } : {
                top: 5 + ((idx - currentIndex) * 70),
              },
            ]}>
            <div style={styles.slideItemInner}>
              <button
                type="button"
                onClick={() => onChange(i)}
                style={[
                  styles.fixedImageWrapper,
                  {
                    backgroundImage: i.src ? `url(${i.src})` : null,
                  },
                ]} />
            </div>
          </div>
        ))}
      </div>
    );
  }
}

export default radium(
  FixedImageSlider
);
