// @flow
import React, { Component } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Field,
} from 'redux-form';

// actions
import {
  setCurrentSelection as setCurrentSelectionAction,
} from '../../../../actions/Customize.js';
// config
import Theme from '../../../../styles/Theme.js';
import SliderStyles from '../../../../styles/OperationSlider.js';
// helper
import { uploadFile } from '../../../../helper/uploader.js';
import {
  canvasImageAttrBinder,
  isMobileMode,
  isBase64,
} from '../../../../helper/canvasHelper.js';
// static
import addIcon from '../../../../static/images/add-icon.png';
// component
import Loading from '../../../Global/Loading.jsx';

const styles = {
  ...SliderStyles,
  imageWrapper: {
    width: '100%',
    height: '100%',
    outline: 0,
    border: 0,
    backgroundColor: 'transparent',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    cursor: 'pointer',
    position: 'relative',
  },
  dragAnnotation: {
    fontSize: 13,
    color: Theme.DEFAULT_TEXT_COLOR,
    opacity: 0.38,
    fontWeight: 600,
    paddingBottom: 6,
  },
  fileAnnotationWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  fileAnnotation: {
    fontSize: 13,
    color: Theme.DEFAULT_TEXT_COLOR,
    fontWeight: 600,
  },
  inputFile: {
    textDecoration: 'underline',
    fontSize: 13,
    color: Theme.BUTTON_BACKGROUND_COLOR,
    cursor: 'pointer',
    textAlign: 'center',
  },
  hideInput: {
    width: 0,
    height: 0,
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    WebkitAppearance: 'none',
    display: 'none',
  },
  loadingMask: {
    position: 'absolute',
    left: 0,
    top: 0,
    zIndex: 1,
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.3)',
  },
};

type Props = {
  currentIndex: number,
  fields: Object,
  setCurrentSelection: Function,
  currentSelectedImageId: number | string,
};

function ImageRender({
  input: {
    value,
  },
  onClick,
  disabled,
}: {
  input: {
    value: string,
  },
  onClick: Function,
  disabled: boolean,
}) {
  const imageIsBase64 = isBase64(value);

  return (
    <button
      type="button"
      onClick={onClick}
      disabled={disabled || imageIsBase64}
      style={[
        styles.imageWrapper,
        {
          backgroundImage: value ? `url(${value})` : null,
        },
      ]}>
      {imageIsBase64 ? (
        <div style={styles.loadingMask}>
          <Loading />
        </div>
      ) : null}
    </button>
  );
}

const renderImage = radium(ImageRender);

class ImageSlider extends Component<Props> {
  fileDropped(files, length) {
    const {
      fields,
      setCurrentSelection,
    } = this.props;

    const wrapOneFile = (files && files[0]) || null;

    uploadFile(wrapOneFile, ({
      src,
      isPreview,
    }) => {
      const generatedData = canvasImageAttrBinder(src);

      if (isPreview) {
        fields.push({
          ...generatedData,
        });

        return null;
      }

      const wrapValue = fields.get(length);

      if (wrapValue) {
        fields.remove(length);
        fields.insert(length, {
          ...generatedData,
          src,
        });
      }

      return setCurrentSelection(generatedData.id);
    }, true);
  }

  render() {
    const {
      fields,
      currentIndex,
      currentSelectedImageId,
      setCurrentSelection,
    } = this.props;

    const wrapFields = (fields && fields.getAll()) || [];
    const imageFields = Array.isArray(wrapFields) ? wrapFields.filter(f => f.type === 'image') : [];

    return (
      <div style={styles.slideWrapper}>
        <div style={styles.adderPlacement}>
          {/* Desktop adder here */}
          <div
            onDrop={(e) => {
              e.preventDefault();
              e.stopPropagation();

              this.fileDropped(e.dataTransfer.files, fields.length);
            }}
            style={styles.adderWrapper}>
            <div style={styles.addIconWrapper}>
              <img
                alt="新增圖片"
                src={addIcon}
                style={styles.addIcon} />
            </div>
            <div style={styles.adderAnnotationWrapper}>
              <span style={styles.dragAnnotation}>
                拖曳照片至此
              </span>
              <div style={styles.fileAnnotationWrapper}>
                <span style={styles.fileAnnotation}>
                  或
                </span>
                <label htmlFor="fileInput" style={styles.inputFile}>
                  選擇檔案
                  <input
                    id="fileInput"
                    type="file"
                    accept="image/*"
                    value=""
                    onChange={e => this.fileDropped(e.target.files, fields.length)}
                    style={styles.hideInput} />
                </label>
              </div>
            </div>
          </div>
          {/* mobile adder here */}
          <label htmlFor="mobileFileInput" style={[styles.adderWrapper, styles.mobileAdderWrapper]}>
            <div style={styles.addIconWrapper}>
              <img
                alt="新增圖片"
                src={addIcon}
                style={styles.addIcon} />
            </div>
            <input
              id="mobileFileInput"
              type="file"
              accept="image/*"
              value=""
              onChange={e => this.fileDropped(e.target.files, fields.length)}
              style={styles.hideInput} />
          </label>
        </div>
        <div className="hide-scrollbar" style={styles.mainSlidePlacement}>
          {fields.map((image, index) => {
            const wrapFieldValue = fields.get(index) || {};
            if (wrapFieldValue.type !== 'image') return null;

            const myImageId = wrapFieldValue.id || null;
            const myIndex = imageFields.findIndex(t => t.id === myImageId);
            const isCurrentlySelected = currentSelectedImageId === myImageId;
            const imageIsBase64 = isBase64(wrapFieldValue.src || null);

            return (
              <div
                key={`image-${index + 1}`}
                style={[
                  styles.slideItem,
                  isCurrentlySelected && styles.activedItem,
                  !isMobileMode() ? {
                    left: 5 + ((myIndex - currentIndex) * 130),
                  } : {
                    top: 5 + ((myIndex - currentIndex) * 70),
                  },
                ]}>
                <div style={styles.slideItemInner}>
                  {!imageIsBase64 ? (
                    <button
                      key={`image-${index + 1}-delete`}
                      type="button"
                      onClick={() => {
                        fields.remove(index);
                        if (isCurrentlySelected) {
                          setCurrentSelection(null);
                        }
                      }}
                      style={styles.deleteWrapper} />
                  ) : null}
                  <Field
                    name={`${image}.src`}
                    disabled={isCurrentlySelected}
                    onClick={() => setCurrentSelection(myImageId)}
                    component={renderImage} />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    currentSelectedImageId: state.Customize.currentSelectedImageId,
  }),
  dispatch => bindActionCreators({
    setCurrentSelection: setCurrentSelectionAction,
  }, dispatch)
);

export default reduxHook(
  radium(
    ImageSlider
  )
);
