// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';

import {
  isMobileMode,
} from '../../../helper/canvasHelper';
// themes
import Theme from '../../../styles/Theme.js';

const styles = {
  placement: {
    width: '100%',
    height: 'auto',
    padding: '8px 0',
    '@media (max-width: 767px)': {
      width: '90%',
    },
  },
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '18px 32px',
    backgroundColor: '#fff',
    borderRadius: 8,
    boxShadow: Theme.BLOCK_SHADOW,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      padding: '18px 12px',
      flexDirection: 'row',
      alignItems: 'center',
    },
  },
  label: {
    fontSize: 12,
    fontWeight: 500,
    color: Theme.GRAY_TEXT_COLOR,
    letterSpacing: 1,
    padding: '0 0 13px 0',
    '@media (max-width: 767px)': {
      padding: '0 8px 0 0',
    },
  },
  selectionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      width: 'auto',
      flex: 1,
    },
  },
  selection: {
    width: 112,
    height: 'auto',
    padding: 12,
    textAlign: 'center',
    color: Theme.GRAY_TEXT_COLOR,
    border: `1px solid ${Theme.GRAY_TEXT_COLOR}`,
    borderRadius: 5,
    outline: 0,
    backgroundColor: 'transparent',
    cursor: 'pointer',
    marginRight: 12,
    ':hover': {
      opacity: 0.88,
    },
    '@media (max-width: 767px)': {
      width: 'auto',
      flex: 1,
      padding: '6px 12px',
    },
  },
  actived: {
    border: `1px solid ${Theme.BUTTON_BACKGROUND_COLOR}`,
    color: Theme.BUTTON_BACKGROUND_COLOR,
  },
};

type Props = {
  label: String,
  input: {
    value: Boolean,
    onChange: Function,
  },
  type: 'image' | 'text' | 'emoji' | 'fixed',
  currentSelectedImageId: number | string,
  currentSelectedTextId: number | string,
  currentSelectedEmojiId: number | string,
  currentSelectedFixedImageId: number | string,
};

class BooleanSelectionBlock extends PureComponent<Props> {
  render() {
    const {
      input: {
        value = false,
        onChange,
      },
      label,
      type,
      currentSelectedImageId,
      currentSelectedTextId,
      currentSelectedEmojiId,
      currentSelectedFixedImageId,
    } = this.props;

    if (isMobileMode()) {
      if (type === 'photo' && currentSelectedImageId) return null;
      if (type === 'text' && currentSelectedTextId) return null;
      if (type === 'emoji' && currentSelectedEmojiId) return null;
      if (type === 'fixed' && currentSelectedFixedImageId) return null;
    }

    return (
      <div style={styles.placement}>
        <div style={styles.wrapper}>
          <span style={styles.label}>
            {label || null}
          </span>
          <div style={styles.selectionWrapper}>
            <button
              key="yes-btn"
              type="button"
              onClick={() => onChange(true)}
              style={[
                styles.selection,
                value && styles.actived,
              ]}>
              是
            </button>
            <button
              key="false-btn"
              type="button"
              onClick={() => onChange(false)}
              style={[
                styles.selection,
                !value && styles.actived,
              ]}>
              否
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    currentSelectedImageId: state.Customize.currentSelectedImageId,
    currentSelectedTextId: state.Customize.currentSelectedTextId,
    currentSelectedEmojiId: state.Customize.currentSelectedEmojiId,
    currentSelectedFixedImageId: state.Customize.currentSelectedFixedImageId,
  }),
);

export default reduxHook(
  radium(
    BooleanSelectionBlock
  )
);
