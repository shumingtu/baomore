// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

// themes
import Theme from '../../../styles/Theme.js';

const styles = {
  placement: {
    display: 'none',
    '@media (max-width: 767px)': {
      flex: 1,
      width: 'auto',
      height: 'auto',
      padding: '8px 0',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-start',
    },
  },
  selectionWrapper: {
    display: 'none',
    '@media (max-width: 767px)': {
      width: 'auto',
      height: 'auto',
      flex: 1,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-start',
    },
  },
  selection: {
    display: 'none',
    flex: 1,
    width: 'auto',
    height: 'auto',
    padding: '6px 12px',
    textAlign: 'center',
    color: Theme.GRAY_TEXT_COLOR,
    border: `1px solid ${Theme.GRAY_TEXT_COLOR}`,
    borderRadius: 5,
    outline: 0,
    backgroundColor: 'transparent',
    cursor: 'pointer',
    marginRight: 12,
    ':hover': {
      opacity: 0.88,
    },
    '@media (max-width: 767px)': {
      display: 'block',
    },
  },
  actived: {
    border: `1px solid ${Theme.BUTTON_BACKGROUND_COLOR}`,
    color: Theme.BUTTON_BACKGROUND_COLOR,
  },
};

type Props = {
  label: String,
  input: {
    value: Boolean,
    onChange: Function,
  },
};

class MobileWhiteBgSelectionBlock extends PureComponent<Props> {
  render() {
    const {
      input: {
        value = false,
        onChange,
      },
    } = this.props;

    return (
      <div style={styles.placement}>
        <div style={styles.selectionWrapper}>
          <button
            key="yes-btn"
            type="button"
            onClick={() => onChange(true)}
            style={[
              styles.selection,
              value && styles.actived,
            ]}>
            白底
          </button>
          <button
            key="false-btn"
            type="button"
            onClick={() => onChange(false)}
            style={[
              styles.selection,
              !value && styles.actived,
            ]}>
            透明
          </button>
        </div>
      </div>
    );
  }
}

export default radium(MobileWhiteBgSelectionBlock);
