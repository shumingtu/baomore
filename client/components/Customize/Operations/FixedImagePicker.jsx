// @flow
/* eslint react/no-did-update-set-state: 0 */
import React, { PureComponent } from 'react';
import radium from 'radium';
// config
import Theme from '../../../styles/Theme.js';
// components
import ArrowButton from '../../Global/ArrowButton.jsx';
import FixedImageSlider from './Components/FixedImageSlider.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '8xp 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      height: '100%',
    },
  },
  functionWrapper: {
    width: '100%',
    height: 48,
    padding: '8px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
  indexCounterBarWrapper: {
    flex: 1,
    height: '100%',
    padding: '0 16px',
  },
  indexCounterBar: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fff',
    borderRadius: 8,
    boxShadow: Theme.BLOCK_SHADOW,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  indexCounter: {
    fontSize: 14,
    color: Theme.DEFAULT_TEXT_COLOR,
    textAlign: 'center',
  },
};

type Props = {
  type: 'fixed',
  libraryType: string,
  images: Array<Object>,
  selectedId: string | number,
  onChange: Function,
};

type State = {
  currentIndex: number,
};

class FixedImagePicker extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      currentIndex: 0,
    };
  }

  componentDidUpdate({
    libraryType: prevLibraryType,
  }) {
    const {
      libraryType,
    } = this.props;

    if (libraryType !== prevLibraryType && libraryType) {
      this.setState({
        currentIndex: 0,
      });
    }
  }

  onNextImage() {
    const {
      images = [],
    } = this.props;

    const {
      currentIndex,
    } = this.state;

    if (currentIndex + 1 >= images.length) return null;

    return this.setState({
      currentIndex: currentIndex + 1,
    });
  }

  onPrevImage() {
    const {
      currentIndex,
    } = this.state;

    if (currentIndex <= 0) return null;

    return this.setState({
      currentIndex: currentIndex - 1,
    });
  }

  render() {
    const {
      type,
      onChange,
      images = [],
      selectedId,
    } = this.props;

    const {
      currentIndex,
    } = this.state;

    if (!onChange) return null;

    return (
      <div style={styles.wrapper}>
        <FixedImageSlider
          type={type}
          images={images}
          onChange={onChange}
          selectedId={selectedId}
          currentIndex={currentIndex} />
        <div style={styles.functionWrapper}>
          <ArrowButton
            type="left"
            onClick={() => this.onPrevImage()} />
          <div style={styles.indexCounterBarWrapper}>
            <div style={styles.indexCounterBar}>
              <span style={styles.indexCounter}>
                {!images.length ? '0/0' : `${currentIndex + 1}/${images.length}`}
              </span>
            </div>
          </div>
          <ArrowButton
            type="right"
            onClick={() => this.onNextImage()} />
        </div>
      </div>
    );
  }
}

export default radium(
  FixedImagePicker
);
