// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
// actions
import * as CustomizeActions from '../../../actions/Customize.js';
// config
import Theme from '../../../styles/Theme.js';
// helper
import { canvasEmojiAttrBinder } from '../../../helper/canvasHelper.js';
// components
import ArrowButton from '../../Global/ArrowButton.jsx';
import Button from '../../Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '8xp 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  slideWrapper: {
    width: '100%',
    height: 70,
    overflow: 'hidden',
    position: 'relative',
    '@media (max-width: 767px)': {
      overflowX: 'auto',
      overflowY: 'hidden',
    },
  },
  slideItem: {
    position: 'absolute',
    top: 10,
    left: 0,
    width: 32,
    height: 50,
    backgroundColor: '#fff',
    border: 0,
    borderRadius: 8,
    outline: 0,
    cursor: 'pointer',
    transition: 'left 0.2s ease-in',
  },
  slideItemInner: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    position: 'relative',
  },
  emojiWrapper: {
    width: '100%',
    flex: 1,
    outline: 0,
    border: 0,
    padding: 0,
    backgroundColor: 'transparent',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    cursor: 'pointer',
  },
  dotWrapper: {
    width: '100%',
    height: 12,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  dot: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: Theme.BUTTON_BACKGROUND_COLOR,
  },
  swipeArrowWrapper: {
    width: '100%',
    height: 20,
    padding: '8px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
  indexCounterBarWrapper: {
    flex: 1,
    height: '100%',
  },
  indexCounterBar: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  indexCounter: {
    fontSize: 14,
    color: Theme.DEFAULT_TEXT_COLOR,
    textAlign: 'center',
  },
  customArrow: {
    width: 12,
    height: 12,
    boxShadow: 'none',
  },
  addWrapper: {
    width: '100%',
    height: 56,
    paddingTop: 16,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      height: 40,
      paddingTop: 8,
    },
  },
  btnWrapper: {
    width: 240,
    height: '100%',
  },
};

type Props = {
  type: 'emoji',
  fields: Object,
  images: Array<{
    id: number | string,
    src: string,
  }>,
  setCurrentSelectionEmoji: Function,
};

type State = {
  currentIndex: number,
  currentSelectedSrc: ?Object,
};

class ImagePicker extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      currentIndex: 0,
      currentSelectedSrc: null,
    };
  }

  onNextImage() {
    const {
      images = [],
    } = this.props;

    const {
      currentIndex,
    } = this.state;

    if (currentIndex + 1 >= images.length) return null;

    return this.setState({
      currentIndex: currentIndex + 1,
    });
  }

  onPrevImage() {
    const {
      currentIndex,
    } = this.state;

    if (currentIndex <= 0) return null;

    return this.setState({
      currentIndex: currentIndex - 1,
    });
  }

  render() {
    const {
      type,
      fields,
      images = [],
      setCurrentSelectionEmoji,
    } = this.props;

    const {
      currentIndex,
      currentSelectedSrc,
    } = this.state;

    if (!fields) return null;

    return (
      <div style={styles.wrapper}>
        <div className="hide-scrollbar" style={styles.slideWrapper}>
          {images.map((i, idx) => (
            <div
              key={`emoji-${i.id}`}
              style={[
                styles.slideItem,
                {
                  left: (idx - currentIndex) * 50,
                },
              ]}>
              <div style={styles.slideItemInner}>
                <button
                  key={`emoji-${i.id}-button`}
                  type="button"
                  onClick={() => this.setState({
                    currentSelectedSrc: i,
                  })}
                  style={[
                    styles.emojiWrapper,
                    {
                      backgroundImage: i.src ? `url(${i.src})` : null,
                    },
                  ]} />
                <div style={styles.dotWrapper}>
                  {currentSelectedSrc && currentSelectedSrc.id === i.id ? (
                    <div style={styles.dot} />
                  ) : null}
                </div>
              </div>
            </div>
          ))}
        </div>
        <div style={styles.swipeArrowWrapper}>
          <ArrowButton
            type="left"
            onClick={() => this.onPrevImage()}
            style={styles.customArrow} />
          <div style={styles.indexCounterBarWrapper}>
            <div style={styles.indexCounterBar}>
              <span style={styles.indexCounter}>
                {!images.length ? '0/0' : `${currentIndex + 1}/${images.length}`}
              </span>
            </div>
          </div>
          <ArrowButton
            type="right"
            onClick={() => this.onNextImage()}
            style={styles.customArrow} />
        </div>
        <div style={styles.addWrapper}>
          <div style={styles.btnWrapper}>
            <Button
              hollow
              disabled={!currentSelectedSrc}
              label="加入選擇"
              onClick={() => {
                if (currentSelectedSrc) {
                  if (type === 'emoji') {
                    const generatedEmoji = canvasEmojiAttrBinder(currentSelectedSrc.src);

                    fields.push(generatedEmoji);

                    return setCurrentSelectionEmoji(generatedEmoji.id);
                  }

                  return fields.push(currentSelectedSrc);
                }

                return null;
              }} />
          </div>
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  () => ({}),
  dispatch => bindActionCreators({
    ...CustomizeActions,
  }, dispatch)
);

export default reduxHook(
  radium(
    ImagePicker
  )
);
