// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  initialize,
  SubmissionError,
  reset,
} from 'redux-form';
// actions
import {
  setCurrentSelectionOnText as setCurrentSelectionOnTextAction,
} from '../../../actions/Customize.js';
// config
import Theme from '../../../styles/Theme.js';
import { PHONE_TEXT_ADDER_TEMP_FORM } from '../../../shared/Form.js';
import {
  NONE_BORDER,
  SOLID_BORDER,
  SHADOW_BORDER,
} from '../../../shared/Global.js';
// static
import deleteIcon from '../../../static/images/delete-icon.png';
// helper
import { canvasTextAttrBinder } from '../../../helper/canvasHelper.js';
// component
import FormLabel from '../../Form/FormLabel.jsx';
import Input from '../../Form/Input.jsx';
import Selector from '../../Form/Selector.jsx';
import ColorPicker from '../../Form/ColorPicker.jsx';
import MultiSelectionBlock from './MultiSelectionBlock.jsx';
import Button from '../../Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '8px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    position: 'relative',
    '@media (max-width: 767px)': {
      padding: 0,
      justifyContent: 'flex-end',
    },
  },
  styleForm: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    backgroundColor: '#fff',
    borderRadius: 8,
    boxShadow: Theme.BLOCK_SHADOW,
    padding: 24,
    marginBottom: 16,
    position: 'relative',
    '@media (max-width: 767px)': {
      padding: '12px 24px',
      marginBottom: 8,
    },
  },
  inputWrapper: {
    width: '100%',
    height: 'auto',
    padding: '8px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  row: {
    flexDirection: 'row',
  },
  flexWrapper: {
    flex: 1,
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonWrapper: {
    width: 240,
    height: 40,
  },
  closeWrapper: {
    position: 'absolute',
    top: 4,
    right: 4,
    width: 24,
    height: 24,
    outline: 0,
    border: 0,
    backgroundColor: 'transparent',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundImage: `url(${deleteIcon})`,
    cursor: 'pointer',
    ':hover': {
      opacity: 0.87,
    },
  },
};

type Props = {
  fields: Object,
  handleSubmit: Function,
  initializeForm: Function,
  setCurrentSelectionOnText: Function,
  resetCreateForm: Function,
  onClose: Function,
};

class TextAdderForm extends PureComponent<Props> {
  componentDidMount() {
    const {
      initializeForm,
    } = this.props;

    initializeForm({
      text: '',
      fontFamily: '微軟正黑體',
      fontWeight: 'normal',
      color: {
        color: '#000000',
        alpha: 100,
      },
      strokeType: NONE_BORDER,
      strokeColor: {
        color: '#000000',
        alpha: 100,
      },
    });
  }

  submit(d) {
    const {
      text,
      fontFamily,
      fontWeight,
      color,
      strokeType,
      strokeColor,
    } = d;

    if (!text) throw new SubmissionError({ text: '請輸入文字' });
    if (!fontFamily || fontFamily === '-1') throw new SubmissionError({ fontFamily: '請選擇字型' });
    if (!fontWeight || fontWeight === '-1') throw new SubmissionError({ fontWeight: '請選擇粗細' });
    if (!color) throw new SubmissionError({ color: '請選擇顏色' });

    const {
      fields,
      setCurrentSelectionOnText,
      resetCreateForm,
      onClose,
    } = this.props;

    const textInCanvas = canvasTextAttrBinder({
      text,
      fontFamily,
      fontWeight,
      color,
      strokeType,
      strokeColor,
    });

    fields.push(textInCanvas); // push value into canvas stack
    setCurrentSelectionOnText(textInCanvas.id); // set current selection
    resetCreateForm(); // reset form

    if (onClose) {
      onClose();
    }
  }

  render() {
    const {
      handleSubmit,
      onClose,
    } = this.props;

    return (
      <form
        id="text-adder-popup"
        style={styles.wrapper}
        onSubmit={handleSubmit(d => this.submit(d))}>
        <div style={styles.styleForm}>
          {onClose ? (
            <button
              key="adder-form-close"
              type="button"
              onClick={() => onClose()}
              style={styles.closeWrapper} />
          ) : null}
          <div style={styles.inputWrapper}>
            <FormLabel label="輸入個性文字" />
            <Field
              type="text"
              name="text"
              placeholder="請輸入想貼上的文字"
              component={Input} />
          </div>
          <div style={styles.inputWrapper}>
            <FormLabel label="選擇字型" />
            <Field
              name="fontFamily"
              options={[{
                id: '微軟正黑體',
                name: '微軟正黑體',
              }]}
              placeholder="請選擇字型"
              component={Selector} />
          </div>
          <div style={styles.inputWrapper}>
            <FormLabel label="選擇粗細" />
            <Field
              name="fontWeight"
              options={[{
                id: 'normal',
                name: '一般',
              }, {
                id: 'bold',
                name: '粗體',
              }]}
              placeholder="請選擇字體粗細"
              component={Selector} />
          </div>
          <div style={styles.inputWrapper}>
            <FormLabel label="字體顏色" />
            <Field
              name="color"
              renderOnTargetContainer={document.getElementById('text-adder-popup')}
              component={ColorPicker} />
          </div>
        </div>
        <div style={styles.styleForm}>
          <div style={styles.inputWrapper}>
            <FormLabel label="選擇邊框" />
            <Field
              name="strokeType"
              options={[{
                name: '無邊框',
                value: NONE_BORDER,
              }, {
                name: '硬邊框',
                value: SOLID_BORDER,
              }, {
                name: '外光暈',
                value: SHADOW_BORDER,
              }]}
              component={MultiSelectionBlock} />
          </div>
          <div style={[styles.inputWrapper, styles.row]}>
            <FormLabel label="選擇顏色" />
            <div style={styles.flexWrapper}>
              <Field
                name="strokeColor"
                renderOnTargetContainer={document.getElementById('text-adder-popup')}
                component={ColorPicker} />
            </div>
          </div>
          <div style={styles.functionWrapper}>
            <div style={styles.buttonWrapper}>
              <Button
                hollow
                type="submit"
                label="預覽效果" />
            </div>
          </div>
        </div>
      </form>
    );
  }
}

const formHook = reduxForm({
  form: PHONE_TEXT_ADDER_TEMP_FORM,
});

const reduxHook = connect(
  () => ({}),
  dispatch => bindActionCreators({
    setCurrentSelectionOnText: setCurrentSelectionOnTextAction,
    initializeForm: v => initialize(PHONE_TEXT_ADDER_TEMP_FORM, v),
    resetCreateForm: () => reset(PHONE_TEXT_ADDER_TEMP_FORM),
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      TextAdderForm
    )
  )
);
