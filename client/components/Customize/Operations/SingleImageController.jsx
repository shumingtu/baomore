// @flow
import React, {
  Component,
  Fragment,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import {
  Field,
} from 'redux-form';

// config
import Theme from '../../../styles/Theme.js';
// static
import mouseIcon from '../../../static/images/mouse-pointer-icon.png';
// components
import FormLabel from '../../Form/FormLabel.jsx';
import RangeSlider from '../../Form/RangeSlider.jsx';
import MobileWhiteBgSelectionBlock from './MobileWhiteBgSelectionBlock.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: 24,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#fff',
    boxShadow: Theme.BLOCK_SHADOW,
    '@media (max-width: 767px)': {
      width: '90%',
    },
  },
  mouseIcon: {
    width: 16,
    height: 16,
    backgroundImage: `url(${mouseIcon})`,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
  annotation: {
    fontSize: 14,
    color: Theme.DEFAULT_TEXT_COLOR,
    padding: '16px 0',
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
  rangeSlideWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      flexDirection: 'row',
      alignItems: 'center',
    },
  },
  groupWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
};

type Props = {
  images: {
    rotate: Object,
    scale: Object,
  },
  names: Array<String>,
  currentSelectedFixedImageId: number | string,
  scalable?: boolean,
  rotatable?: boolean,
  type: string,
};

class SingleImageController extends Component<Props> {
  static defaultProps = {
    scalable: true,
    rotatable: true,
  };

  render() {
    const {
      type,
      images,
      currentSelectedFixedImageId,
      scalable,
      rotatable,
    } = this.props;

    if (type === 'fixed' && !currentSelectedFixedImageId) return null;

    return (
      <div style={styles.wrapper}>
        <Fragment>
          <div style={styles.mouseIcon} />
          <span style={styles.annotation}>
            請拖動手機上的圖片進行移動到理想的位置
          </span>
        </Fragment>
        {scalable ? (
          <div style={styles.rangeSlideWrapper}>
            <FormLabel label="縮放圖片" />
            <RangeSlider
              type="percentage"
              min={0.1}
              max={2}
              step={0.1}
              input={images.scale.input} />
          </div>
        ) : null}
        {rotatable ? (
          <div style={styles.rangeSlideWrapper}>
            <FormLabel label="旋轉圖片" />
            <RangeSlider
              type="degree"
              min={-179}
              max={179}
              step={1}
              input={images.rotate.input} />
          </div>
        ) : null}
        <div style={styles.groupWrapper}>
          <Field
            name="needWhiteBg"
            component={MobileWhiteBgSelectionBlock} />
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    currentSelectedFixedImageId: state.Customize.currentSelectedFixedImageId,
  }),
);

export default reduxHook(
  radium(
    SingleImageController
  )
);
