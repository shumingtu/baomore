// @flow
import React, { Component } from 'react';
import radium from 'radium';
// config
import Theme from '../../../styles/Theme.js';
// components
import ArrowButton from '../../Global/ArrowButton.jsx';
import TextSlider from './Components/TextSlider.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '8px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      height: '100%',
    },
  },
  functionWrapper: {
    width: '100%',
    height: 48,
    padding: '8px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
  indexCounterBarWrapper: {
    flex: 1,
    height: '100%',
    padding: '0 16px',
  },
  indexCounterBar: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fff',
    borderRadius: 8,
    boxShadow: Theme.BLOCK_SHADOW,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  indexCounter: {
    fontSize: 14,
    color: Theme.DEFAULT_TEXT_COLOR,
    textAlign: 'center',
  },
};

type Props = {
  fields: Object,
};

type State = {
  currentIndex: number,
};

class SwipeText extends Component<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      currentIndex: 0,
    };
  }

  onNextImage() {
    const {
      fields,
    } = this.props;

    const wrapFields = (fields && fields.getAll()) || [];
    const textFieldLength = wrapFields.filter(f => f.type === 'text').length;

    const {
      currentIndex,
    } = this.state;

    if (currentIndex + 1 >= textFieldLength) return null;

    return this.setState({
      currentIndex: currentIndex + 1,
    });
  }

  onPrevImage() {
    const {
      currentIndex,
    } = this.state;

    if (currentIndex <= 0) return null;

    return this.setState({
      currentIndex: currentIndex - 1,
    });
  }

  render() {
    const {
      fields,
    } = this.props;

    const {
      currentIndex,
    } = this.state;

    const wrapFields = (fields && fields.getAll()) || [];
    const textFields = wrapFields.filter(f => f.type === 'text');

    return (
      <div style={styles.wrapper}>
        <TextSlider
          fields={fields}
          currentIndex={currentIndex} />
        <div style={styles.functionWrapper}>
          <ArrowButton
            type="left"
            onClick={() => this.onPrevImage()} />
          <div style={styles.indexCounterBarWrapper}>
            <div style={styles.indexCounterBar}>
              <span style={styles.indexCounter}>
                {!textFields.length ? '0/0' : `${currentIndex + 1}/${textFields.length}`}
              </span>
            </div>
          </div>
          <ArrowButton
            type="right"
            onClick={() => this.onNextImage()} />
        </div>
      </div>
    );
  }
}

export default radium(
  SwipeText
);
