// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import {
  formValueSelector,
} from 'redux-form';

// config
import { PHONE_CUSTOMIZE_FORM } from '../../../shared/Form.js';
// helper
import {
  checkCustomizeIsDone,
  checkPayCheckIsDone,
} from '../../../helper/canvasHelper.js';
// components
import Button from '../../Global/Button.jsx';

const styles = {
  submitWrapper: {
    width: '100%',
    height: 'auto',
    paddingTop: 48,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      height: '100%',
      padding: 0,
    },
  },
  submit: {
    width: 240,
    height: 40,
    display: 'block',
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
  mobileSubmit: {
    width: 90,
    height: 40,
    display: 'none',
    '@media (max-width: 767px)': {
      display: 'block',
    },
  },
};

const selector = formValueSelector(PHONE_CUSTOMIZE_FORM);

type Props = {
  needCheckPaycheckInfo: boolean,
  images: Array<Object>,
  onClick: Function,
  payCheckInfo: Object,
  forceDisabled: boolean,
};

class CheckoutButton extends PureComponent<Props> {
  render() {
    const {
      needCheckPaycheckInfo = false,
      images,
      payCheckInfo = null,
      onClick,
      forceDisabled = false,
    } = this.props;

    const isDone = !needCheckPaycheckInfo
      ? checkCustomizeIsDone(images)
      : checkPayCheckIsDone(images, payCheckInfo);

    return (
      <div style={styles.submitWrapper}>
        <div style={styles.submit}>
          <Button
            withShadow
            type="button"
            disabled={forceDisabled || !isDone}
            label="確定並前往付款"
            onClick={onClick} />
        </div>
        <div style={styles.mobileSubmit}>
          <Button
            withShadow
            type="button"
            disabled={forceDisabled || !isDone}
            label="前往付款"
            onClick={onClick} />
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    images: selector(state, 'Main.Customize.images'),
    payCheckInfo: selector(state, 'Main.Customize.Pay'),
  }),
);

export default reduxHook(
  radium(
    CheckoutButton
  )
);
