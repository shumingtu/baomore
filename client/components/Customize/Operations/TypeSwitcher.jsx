// @flow
import React from 'react';
import radium from 'radium';

// config
import Theme from '../../../styles/Theme.js';
// components
import FormLabel from '../../Form/FormLabel.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '8px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  formLabelWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '6px 8px 6px 0',
    display: 'block',
    '@media (max-width: 767px)': {
      display: 'none',
      padding: 0,
    },
  },
  mainWrapper: {
    width: '100%',
    height: 'auto',
    padding: 16,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: '#fff',
    boxShadow: Theme.BLOCK_SHADOW,
    '@media (max-width: 767px)': {
      padding: '6px 16px',
    },
  },
  typeBtn: {
    width: 75,
    height: 'auto',
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    cursor: 'pointer',
    '@media (max-width: 767px)': {
      width: 'auto',
      flex: 1,
    },
  },
  inactive: {
    opacity: 0.3,
  },
  typeWrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  typeIcon: {
    width: 32,
    height: 32,
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    '@media (max-width: 767px)': {
      width: 24,
      height: 24,
    },
  },
  typeLabel: {
    fontSize: 12,
    fontWeight: 500,
    color: '#000',
    letterSpacing: 1,
    paddingTop: 12,
    textAlign: 'center',
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
};

type Props = {
  currentType: string,
  onChange: Function,
  types: Array<{
    type: string,
    icon: string,
    label: string,
  }>
};

function TypeSwitcher({
  types = [],
  currentType,
  onChange,
}: Props) {
  return (
    <div style={styles.wrapper}>
      <div style={styles.formLabelWrapper}>
        <FormLabel label="選擇服務項目" />
      </div>
      <div style={styles.mainWrapper}>
        {types.map(t => (
          <button
            key={t.type}
            type="button"
            onClick={() => onChange(t.type)}
            style={[
              styles.typeBtn,
              currentType !== t.type && styles.inactive,
            ]}>
            <div style={styles.typeWrapper}>
              <div
                style={[
                  styles.typeIcon,
                  {
                    backgroundImage: t.icon ? `url(${t.icon})` : null,
                  },
                ]} />
              <span style={styles.typeLabel}>
                {t.label || null}
              </span>
            </div>
          </button>
        ))}
      </div>
    </div>
  );
}

export default radium(TypeSwitcher);
