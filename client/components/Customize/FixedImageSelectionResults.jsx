// @flow
import React, {
  PureComponent,
  lazy,
  Suspense,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  formValueSelector,
  change,
} from 'redux-form';
import { Query } from 'react-apollo';
// actions
import * as CustomizeActions from '../../actions/Customize.js';
// config
import {
  PHONE_FIXED_IMAGE_TEMP_FORM,
  PHONE_CUSTOMIZE_FORM,
} from '../../shared/Form.js';
import {
  GET_SUBCATEGORY_PNTABLE_LIST,
} from '../../queries/Customize.js';
import { canvasFixedImageAttrBinder } from '../../helper/canvasHelper.js';
// components
import Loading from '../Global/Loading.jsx';
// lazily component
const FixedImagePicker = lazy(() => import('./Operations/FixedImagePicker.jsx'));
const selector = formValueSelector(PHONE_FIXED_IMAGE_TEMP_FORM);

type Props = {
  input?: {
    onChange: Function,
  },
  onChange?: Function,
  libraryType: string | number,
  currentSelectedFixedImageId: string | number,
  setCurrentSelectionOnFixedImage: Function,
  setPrice: Function,
  setDescription: Function,
};

class FixedImageSelection extends PureComponent<Props> {
  static defaultProps = {
    onChange: null,
    input: null,
  };

  render() {
    const {
      input,
      onChange,
      libraryType,
      currentSelectedFixedImageId,
      setCurrentSelectionOnFixedImage,
      setPrice,
      setDescription,
    } = this.props;

    const fieldOnChange = input ? input.onChange : onChange;

    if (libraryType && libraryType !== '-1') {
      return (
        <Query
          query={GET_SUBCATEGORY_PNTABLE_LIST}
          variables={{
            subCategoryId: parseInt(libraryType, 10) || -1,
          }}>
          {({
            data: {
              pntablelistBySubcategory = [],
            },
          }) => (
            <Suspense fallback={<Loading />}>
              <FixedImagePicker
                type="fixed"
                selectedId={currentSelectedFixedImageId}
                libraryType={libraryType}
                onChange={(i) => {
                  fieldOnChange(canvasFixedImageAttrBinder(i));
                  setCurrentSelectionOnFixedImage(i.id);
                  setPrice(i.onlinePrice ? `$${i.onlinePrice}` : null);
                  setDescription(i.description || null);
                }}
                images={pntablelistBySubcategory.map(pn => ({
                  id: pn.id,
                  src: pn.picture,
                  description: pn.description || null,
                  onlinePrice: pn.onlinePrice || null,
                }))} />
            </Suspense>
          )}
        </Query>
      );
    }

    return null;
  }
}

const reduxHook = connect(
  state => ({
    libraryType: selector(state, 'libraryType'),
    currentSelectedFixedImageId: state.Customize.currentSelectedFixedImageId,
  }),
  dispatch => bindActionCreators({
    ...CustomizeActions,
    setPrice: v => change(PHONE_CUSTOMIZE_FORM, 'Main.Customize.price', v),
    setDescription: v => change(PHONE_CUSTOMIZE_FORM, 'Main.Customize.description', v),
  }, dispatch)
);

export default reduxHook(
  radium(
    FixedImageSelection
  )
);
