// @flow
import React, {
  PureComponent,
  lazy,
  Suspense,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import {
  reduxForm,
  Field,
  formValueSelector,
} from 'redux-form';
import { Query } from 'react-apollo';
// static
import deleteIcon from '../../static/images/delete-icon.png';
// config
import Theme from '../../styles/Theme.js';
import { PHONE_EMOJI_TEMP_FORM } from '../../shared/Form.js';
import {
  GET_EMOJI_CATEGORIES,
  GET_EMOJI_LIST,
} from '../../queries/Customize.js';
// components
import FormLabel from '../Form/FormLabel.jsx';
import Selector from '../Form/Selector.jsx';
import Loading from '../Global/Loading.jsx';
// lazily component
const ImagesPicker = lazy(() => import('./Operations/ImagesPicker.jsx'));

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '8px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  formBlock: {
    width: '100%',
    height: 'auto',
    padding: '24px 32px',
    borderRadius: 8,
    boxShadow: Theme.BLOCK_SHADOW,
    backgroundColor: '#fff',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    position: 'relative',
    '@media (max-width: 767px)': {
      padding: '16px 24px',
    },
  },
  selectorWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      flexDirection: 'row',
      alignItems: 'center',
    },
  },
  selector: {
    width: '100%',
    height: 'auto',
    '@media (max-width: 767px)': {
      flex: 1,
      width: 'auto',
    },
  },
  closeWrapper: {
    position: 'absolute',
    top: 4,
    right: 4,
    width: 24,
    height: 24,
    outline: 0,
    border: 0,
    backgroundColor: 'transparent',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundImage: `url(${deleteIcon})`,
    cursor: 'pointer',
    ':hover': {
      opacity: 0.87,
    },
  },
};

const selector = formValueSelector(PHONE_EMOJI_TEMP_FORM);

type Props = {
  fields: Object,
  emojiType: String,
  onClose: Function,
};

class EmojiSelections extends PureComponent<Props> {
  render() {
    const {
      fields,
      emojiType,
      onClose,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.formBlock}>
          {onClose ? (
            <button
              key="adder-form-close"
              type="button"
              onClick={() => onClose()}
              style={styles.closeWrapper} />
          ) : null}
          <div style={styles.selectorWrapper}>
            <FormLabel label="選擇表情圖庫" />
            <div style={styles.selector}>
              <Query query={GET_EMOJI_CATEGORIES}>
                {({
                  data: {
                    emojiCategories = [],
                  },
                }) => (
                  <Field
                    name="emojiType"
                    options={emojiCategories}
                    placeholder="請選擇表情圖庫"
                    component={Selector} />
                )}
              </Query>
            </div>
          </div>
          {emojiType && emojiType !== '-1' ? (
            <Query
              query={GET_EMOJI_LIST}
              variables={{
                categoryId: parseInt(emojiType, 10),
              }}>
              {({
                data: {
                  emoji = [],
                },
              }) => (
                <Suspense fallback={<Loading />}>
                  <ImagesPicker
                    type="emoji"
                    fields={fields}
                    images={emoji.map(e => ({
                      id: e.id,
                      src: e.picture,
                    }))} />
                </Suspense>
              )}
            </Query>
          ) : null}
        </div>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: PHONE_EMOJI_TEMP_FORM,
});

const reduxHook = connect(
  state => ({
    emojiType: selector(state, 'emojiType'),
  }),
);

export default reduxHook(
  formHook(
    radium(
      EmojiSelections
    )
  )
);
