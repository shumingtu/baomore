// @flow
/* eslint react/no-did-update-set-state: 0 */
import React, { Component } from 'react';
import radium from 'radium';
import { Image } from 'react-konva';

type Props = {
  type: string,
  src: string,
  width: number,
  height: number,
  onSetClip?: Function,
};

type State = {
  image: ?Object,
  imageBundle: {
    width: number,
    height: number,
    x: number,
    y: number,
  },
};

class PhoneElement extends Component<Props, State> {
  static defaultProps = {
    onSetClip: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      image: null,
      imageBundle: {
        width: 1,
        height: 1,
        x: 0,
        y: 0,
      },
    };
  }

  componentDidMount() {
    /* use online element source */
    const {
      src,
      width,
      height,
      onSetClip,
      type,
    } = this.props;

    const image = new window.Image();
    image.setAttribute('crossOrigin', 'anonymous');
    image.src = `${src}?${new Date().getTime()}`;

    image.onload = () => {
      // const zoomRatioInHeight = height / image.height; // height ratio
      // const baseOnHeightIsOverflow = Math.floor(image.width * zoomRatioInHeight) > width;
      // const baseOnHeight = {
      //   width: Math.floor(image.width * zoomRatioInHeight),
      //   height,
      // };
      // const zoomRatioInWidth = width / image.width; // width ratio
      // const baseOnWidth = {
      //   width,
      //   height: Math.ceil(image.height * zoomRatioInWidth),
      // };
      //
      // const currentSizeBundle = !baseOnHeightIsOverflow ? baseOnHeight : baseOnWidth;
      //
      // const currentPosition = {
      //   x: (width - currentSizeBundle.width) / 2,
      //   y: (height - currentSizeBundle.height) / 2,
      // };
      //
      // const deviation = type === 'mask' ? 4 : 0;

      // this.setState({
      //   image,
      //   imageBundle: {
      //     width: currentSizeBundle.width + deviation,
      //     height: currentSizeBundle.height + deviation,
      //     x: currentPosition.x,
      //     y: currentPosition.y,
      //   },
      // });
      //
      // if (onSetClip) {
      //   onSetClip({
      //     clipX: currentPosition.x,
      //     clipY: currentPosition.y,
      //     clipWidth: currentSizeBundle.width,
      //     clipHeight: currentSizeBundle.height,
      //   });
      // }

      const sizeDeviation = type === 'mask' ? 8 : 0;

      this.setState({
        image,
        imageBundle: {
          width: width + sizeDeviation,
          height: height + sizeDeviation,
          x: 0 + ((sizeDeviation / 2) * -1),
          y: 0 + ((sizeDeviation / 2) * -1),
        },
      });

      if (onSetClip) {
        onSetClip({
          clipX: 0,
          clipY: 0,
          clipWidth: width,
          clipHeight: height,
        });
      }
    };
  }

  componentDidUpdate({
    width: prevWidth,
    height: prevHeight,
  }) {
    const {
      width,
      height,
      type,
      onSetClip,
    } = this.props;

    if (prevWidth !== width || prevHeight !== height) {
      const sizeDeviation = type === 'mask' ? 8 : 0;

      this.setState({
        imageBundle: {
          width: width + sizeDeviation,
          height: height + sizeDeviation,
          x: 0 + ((sizeDeviation / 2) * -1),
          y: 0 + ((sizeDeviation / 2) * -1),
        },
      });

      if (onSetClip) {
        onSetClip({
          clipX: 0,
          clipY: 0,
          clipWidth: width,
          clipHeight: height,
        });
      }
    }
  }

  render() {
    const {
      image,
      imageBundle,
    } = this.state;

    return (
      <Image
        listening={false}
        x={imageBundle.x}
        y={imageBundle.y}
        width={imageBundle.width}
        height={imageBundle.height}
        image={image} />
    );
  }
}

export default radium(PhoneElement);
