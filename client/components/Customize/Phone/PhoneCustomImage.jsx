// @flow
/* eslint func-names: 0, react/no-did-update-set-state: 0 */
import React, { Component } from 'react';
import radium from 'radium';
import Exif from 'exif-js';
import { Image } from 'react-konva';

// setting
import {
  CANVAS_WIDTH,
} from '../../../shared/PhoneConfig.js';
import {
  calculateAspectRatioFit,
  isMobileMode,
  getTouchDistance,
} from '../../../helper/canvasHelper.js';

type Props = {
  image: {
    src: string,
    x: number,
    y: number,
    scale: number,
    rotate: number,
  },
  createRef: Function,
  onChange: Function,
  allowMultiTouchEvent: boolean,
  canvasSize: {
    width: number,
    height: number,
  },
};

type State = {
  imageCanvas: ?Object,
  size: {
    width: number,
    height: number,
  },
};

let cacheTouchLastDist = 0;

class PhoneCustomImage extends Component<Props, State> {
  constructor(props) {
    super(props);

    const {
      canvasSize,
    } = props;

    this.state = {
      imageCanvas: null,
      size: {
        width: canvasSize.width,
        height: canvasSize.height,
      },
    };

    this.imageRef = null;
    this.touchMoveEvt = evt => this.handleTouchMove(evt);
    this.touchEndEvt = evt => this.handleTouchEnd(evt);
  }

  componentDidMount() {
    const {
      image,
      onChange,
      allowMultiTouchEvent,
      canvasSize,
    } = this.props;

    const i = new window.Image();

    if (allowMultiTouchEvent) {
      this.imageRef.addEventListener('touchmove', this.touchMoveEvt, false);
      this.imageRef.addEventListener('touchend', this.touchEndEvt, false);
    }

    if (image) {
      const isBase64 = this.isBase64();
      i.setAttribute('crossOrigin', 'anonymous');
      if (!isBase64) {
        i.src = `${image.src}?${new Date().getTime()}`;

        i.onload = () => {
          this.setState({
            imageCanvas: i,
            size: calculateAspectRatioFit(
              i.width,
              i.height,
              canvasSize.width,
              canvasSize.height,
            ),
          });
          // fix ios HEIF wrong orientation bug
          Exif.getData(i, function () {
            const orientation = Exif.getTag(this, 'Orientation');

            if (orientation === 6) {
              // image in wrong orientation
              onChange({
                ...image,
                rotate: 90,
              });
            }
          });
        };
      } else {
        /* Hide all base64 images */
        // i.src = image.src;
      }
    }
  }

  componentDidUpdate({
    canvasSize: {
      width: prevWidth,
      height: prevHeight,
    },
  }, {
    size,
  }) {
    const {
      canvasSize: {
        width,
        height,
      },
    } = this.props;

    if (prevWidth !== width || prevHeight !== height) {
      this.setState({
        size: {
          width: size.width + (width - prevWidth),
          height: size.height + (height - prevHeight),
        },
      });
    }
  }

  handleTouchMove(evt) {
    const [
      touch1,
      touch2,
    ] = evt.touches;

    if (touch1 && touch2) {
      const {
        image,
        onChange,
      } = this.props;

      if (image) {
        const distance = getTouchDistance({
          x: touch1.clientX,
          y: touch1.clientY,
        }, {
          x: touch2.clientX,
          y: touch2.clientY,
        });

        if (!cacheTouchLastDist) {
          cacheTouchLastDist = distance;
        } else {
          const newScale = (parseFloat(image.scale) || 1) * (distance / cacheTouchLastDist);

          onChange({
            ...image,
            scale: Math.min(2, Math.max(0.1, newScale.toFixed(2))), // scale between 0.1 ~ 2
          });

          cacheTouchLastDist = distance;
        }
      }
    }
  }

  handleTouchEnd() {
    cacheTouchLastDist = 0;
  }

  isBase64() {
    const {
      image,
    } = this.props;

    if (image) {
      return image.src.startsWith('data:image', 0);
    }

    return false;
  }

  dragEnd(evt) {
    const {
      onChange,
      image,
      canvasSize,
    } = this.props;

    const newPos = evt.target.getAbsolutePosition();
    const applyRwdRatio = isMobileMode() ? CANVAS_WIDTH / canvasSize.width : 1;

    onChange({
      ...image,
      x: newPos.x * applyRwdRatio,
      y: newPos.y * applyRwdRatio,
    });
  }

  render() {
    const {
      imageCanvas,
      size,
    } = this.state;

    const {
      image,
      createRef,
      canvasSize,
    } = this.props;

    const applyRwdRatio = isMobileMode() ? canvasSize.width / CANVAS_WIDTH : 1;

    return (
      <Image
        ref={(r) => {
          if (createRef) {
            createRef(r);
          } else {
            this.imageRef = r;
          }
        }}
        x={image.x * applyRwdRatio}
        y={image.y * applyRwdRatio}
        draggable
        rotation={parseInt(image.rotate, 10)}
        scale={{
          x: image.scale ? parseFloat(image.scale) : 1,
          y: image.scale ? parseFloat(image.scale) : 1,
        }}
        offset={{
          x: size.width / 2,
          y: size.height / 2,
        }}
        onDragEnd={evt => this.dragEnd(evt)}
        width={size.width}
        height={size.height}
        image={imageCanvas} />
    );
  }
}

export default radium(PhoneCustomImage);
