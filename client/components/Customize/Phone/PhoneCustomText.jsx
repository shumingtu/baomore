// @flow
import React, { Component } from 'react';
import radium from 'radium';
import { Text } from 'react-konva';

// setting
import {
  NONE_BORDER,
  SOLID_BORDER,
  SHADOW_BORDER,
} from '../../../shared/Global.js';
import {
  CANVAS_WIDTH,
} from '../../../shared/PhoneConfig.js';
import { isMobileMode } from '../../../helper/canvasHelper.js';

type Props = {
  text: {
    id: number,
    text: string,
    fontSize: number,
    fontWeight: string,
    fontFamily: string,
    color: string,
    strokeType: string,
    strokeColor: string,
    x: number,
    y: number,
    rotate: number,
    scale: number,
  },
  onChange: Function,
  canvasSize: {
    width: number,
    height: number,
  },
};

type State = {
  offset: {
    x: number,
    y: number,
  },
};

class PhoneCustomText extends Component<Props, State> {
  constructor() {
    super();

    this.textRef = null;

    this.state = {
      offset: {
        x: 0,
        y: 0,
      },
    };
  }

  componentDidMount() {
    if (this.textRef) {
      this.setState({
        offset: {
          x: this.textRef.getTextWidth() / 2,
          y: this.textRef.getTextHeight() / 2,
        },
      });
    }
  }

  dragEnd(evt) {
    const {
      onChange,
      text,
      canvasSize,
    } = this.props;

    const newPos = evt.target.getAbsolutePosition();
    const applyRwdRatio = isMobileMode() ? CANVAS_WIDTH / canvasSize.width : 1;

    onChange({
      ...text,
      x: newPos.x * applyRwdRatio,
      y: newPos.y * applyRwdRatio,
    });
  }

  render() {
    const {
      text = {},
      canvasSize,
    } = this.props;

    const {
      offset,
    } = this.state;

    const {
      id,
      fontSize,
      fontWeight,
      fontFamily,
      x,
      y,
      rotate,
      scale,
      color,
      strokeColor,
      strokeType,
    } = text;

    const applyRwdRatio = isMobileMode() ? canvasSize.width / CANVAS_WIDTH : 1;

    const customAttrs = () => {
      switch (strokeType) {
        case NONE_BORDER:
          return {};
        case SOLID_BORDER:
          return {
            stroke: strokeColor,
            strokeWidth: 1,
          };
        case SHADOW_BORDER:
          return {
            shadowColor: strokeColor,
            shadowOpacity: 0.6,
            shadowBlur: 24,
            shadowOffset: {
              x: 0,
              y: 5,
            },
          };
        default:
          return {};
      }
    };

    return (
      <Text
        key={id}
        ref={(r) => { this.textRef = r; }}
        {...customAttrs()}
        fontSize={fontSize}
        fontStyle={fontWeight}
        fontFamily={fontFamily}
        x={x * applyRwdRatio}
        y={y * applyRwdRatio}
        offset={offset}
        scale={{
          x: parseFloat(scale) * applyRwdRatio,
          y: parseFloat(scale) * applyRwdRatio,
        }}
        rotation={parseInt(rotate, 10)}
        text={text.text}
        fill={color}
        onDragEnd={evt => this.dragEnd(evt)}
        draggable />
    );
  }
}

export default radium(PhoneCustomText);
