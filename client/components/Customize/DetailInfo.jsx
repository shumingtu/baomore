// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

// config
import Theme from '../../styles/Theme.js';
// static
import detailIcon from '../../static/images/detail-icon.png';
import deleteIcon from '../../static/images/delete-icon.png';

const styles = {
  wrapper: {
    width: 48,
    height: 48,
    borderRadius: 8,
    boxShadow: Theme.BLOCK_SHADOW,
    backgroundColor: '#fff',
    padding: 12,
    outline: 0,
    border: 0,
    cursor: 'pointer',
    '@media (max-width: 767px)': {
      boxShadow: Theme.MOBILE_BLOCK_SHADOW,
    },
  },
  icon: {
    width: '100%',
    height: '100%',
    backgroundImage: `url(${detailIcon})`,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  },
  modalWrapper: {
    width: 'auto',
    height: 'auto',
    position: 'relative',
    maxWidth: 232,
  },
  stringWrapper: {
    fontSize: 13,
    letterSpacing: 1,
    color: Theme.DEFAULT_TEXT_COLOR,
    whiteSpace: 'pre-line',
    wordBreak: 'break-word',
  },
  closeWrapper: {
    position: 'absolute',
    top: 4,
    right: 4,
    width: 16,
    height: 16,
    outline: 0,
    border: 0,
    backgroundColor: 'transparent',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundImage: `url(${deleteIcon})`,
    cursor: 'pointer',
    zIndex: 2,
    ':hover': {
      opacity: 0.87,
    },
  },
};

type Props = {
  input: {
    value: string,
  },
};

type State = {
  openModal: boolean,
};

class DetailInfo extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      openModal: false,
    };
  }

  render() {
    const {
      input: {
        value,
      },
    } = this.props;

    const {
      openModal,
    } = this.state;

    if (!value) return null;

    if (!openModal) {
      return (
        <button
          key="open-detail-modal-btn"
          type="button"
          onClick={() => this.setState({ openModal: true })}
          style={styles.wrapper}>
          <div style={styles.icon} />
        </button>
      );
    }

    return (
      <div
        style={[
          styles.wrapper,
          styles.modalWrapper,
        ]}>
        <button
          key="detail-close-btn"
          type="button"
          onClick={() => this.setState({ openModal: false })}
          style={styles.closeWrapper} />
        <span style={styles.stringWrapper}>
          {value || null}
        </span>
      </div>
    );
  }
}

export default radium(DetailInfo);
