// @flow
import React from 'react';
import radium from 'radium';

import Theme from '../../styles/Theme.js';

const styles = {
  titleWrapper: {
    width: '100%',
    height: 'auto',
    padding: '16px 0 6px 12px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      width: 'auto',
      flex: 1,
      padding: '0 0 0 8px',
    },
  },
  title: {
    fontSize: 16,
    fontWeight: 600,
    color: Theme.DEFAULT_TEXT_COLOR,
    letterSpacing: 1,
    margin: 0,
    padding: 0,
  },
};

type Props = {
  title: String,
};

function Title({
  title,
}: Props) {
  return (
    <div style={styles.titleWrapper}>
      <h2 style={styles.title}>
        {title || null}
      </h2>
    </div>
  );
}

export default radium(Title);
