// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  formValueSelector,
} from 'redux-form';
import { Query } from 'react-apollo';
// actions
import * as CustomizeActions from '../../actions/Customize.js';
// config
import Theme from '../../styles/Theme.js';
import {
  PHONE_FIXED_IMAGE_TEMP_FORM,
  PHONE_CUSTOMIZE_FORM,
} from '../../shared/Form.js';
import {
  GET_CUSTOMIZE_CATEGORY_SUBCATEGORIES,
} from '../../queries/Customize.js';
// components
import FormLabel from '../Form/FormLabel.jsx';
import Selector from '../Form/Selector.jsx';
import FixedImageSelectionResults from './FixedImageSelectionResults.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '8px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      padding: '4px 0 12px 0',
    },
  },
  selectorBlock: {
    width: '100%',
    height: 'auto',
    padding: 32,
    borderRadius: 8,
    boxShadow: Theme.BLOCK_SHADOW,
    backgroundColor: '#fff',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      padding: '8px 24px',
      flexDirection: 'row',
      alignItems: 'center',
      boxShadow: Theme.MOBILE_BLOCK_SHADOW,
    },
  },
  selector: {
    width: '100%',
    height: 'auto',
    '@media (max-width: 767px)': {
      width: 'auto',
      flex: 1,
    },
  },
  desktopWrapper: {
    width: '100%',
    height: 'auto',
    display: 'block',
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
};

const originFormSelector = formValueSelector(PHONE_CUSTOMIZE_FORM);

type Props = {
  input: {
    onChange: Function,
  },
  selectedCategory: string,
  setCurrentSelectionOnFixedImage: Function,
};

class FixedImageSelection extends PureComponent<Props> {
  render() {
    const {
      input: {
        onChange,
      },
      selectedCategory,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <div style={styles.selectorBlock}>
          <FormLabel label="選擇圖庫" />
          <div style={styles.selector}>
            <Query
              query={GET_CUSTOMIZE_CATEGORY_SUBCATEGORIES}
              variables={{
                id: selectedCategory ? parseInt(selectedCategory, 10) : -1,
                isOnSale: true,
              }}>
              {({
                data: {
                  subCategoriesBySubCategory = [],
                },
              }) => (
                <Field
                  name="libraryType"
                  options={subCategoriesBySubCategory}
                  placeholder="請選擇圖庫"
                  component={Selector} />
              )}
            </Query>
          </div>
        </div>
        <div style={styles.desktopWrapper}>
          <FixedImageSelectionResults
            onChange={onChange} />
        </div>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: PHONE_FIXED_IMAGE_TEMP_FORM,
});

const reduxHook = connect(
  state => ({
    selectedCategory: originFormSelector(state, 'Main.customizeCategory'),
  }),
  dispatch => bindActionCreators({
    ...CustomizeActions,
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      FixedImageSelection
    )
  )
);
