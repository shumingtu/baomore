// @flow
import React, { memo } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  formValueSelector,
  change,
} from 'redux-form';
import { Query } from 'react-apollo';
// config
import Theme from '../../../styles/Theme.js';
import { STORE_MAP_LIGHTBOX } from '../../../shared/LightboxTypes.js';
import {
  GET_CITY_DISTRICTS,
  GET_STORES,
} from '../../../queries/Customize.js';
// actions
import * as LightboxActions from '../../../actions/Lightbox.js';

const styles = {
  mapOpener: {
    width: 60,
    height: 20,
    fontSize: 13,
    textAlign: 'center',
    color: Theme.BUTTON_BACKGROUND_COLOR,
    padding: 0,
    margin: 0,
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    cursor: 'pointer',
    ':hover': {
      opacity: 0.87,
    },
  },
};

type Props = {
  openLightbox: Function,
  setStoreId: Function,
  cityId: Number | String,
  storeId: Number | String,
};

function StoreMapOpener({
  openLightbox,
  cityId,
  storeId,
  setStoreId,
}: Props) {
  if (!cityId || cityId === '-1') return null;

  if (storeId && storeId !== '-1') {
    return (
      <Query
        query={GET_STORES}
        variables={{
          cityId: parseInt(cityId, 10),
          isForClient: true,
        }}>
        {({
          data: {
            stores = [],
          },
        }) => {
          const myStore = stores.find(s => s.id === parseInt(storeId, 10));

          return (
            <button
              type="button"
              onClick={() => openLightbox({
                type: STORE_MAP_LIGHTBOX,
                cityId,
                storeId: (myStore && myStore.id) || null,
                lat: (myStore && myStore.latitude) || 25.042302, // store lat
                lng: (myStore && myStore.longitude) || 121.546309, // store lng
                zoom: 16,
                onSelected: id => setStoreId(id),
              })}
              style={styles.mapOpener}>
              門市地圖
            </button>
          );
        }}
      </Query>
    );
  }

  return (
    <Query query={GET_CITY_DISTRICTS}>
      {({
        data: {
          cityDistricts = [],
        },
      }) => {
        const myCity = cityDistricts.find(c => c.id === parseInt(cityId, 10));

        return (
          <button
            type="button"
            onClick={() => openLightbox({
              type: STORE_MAP_LIGHTBOX,
              cityId,
              lat: (myCity && myCity.latitude) || 25.032308, // city lat
              lng: (myCity && myCity.longitude) || 121.534707, // city lng
              zoom: 10,
              onSelected: id => setStoreId(id),
            })}
            style={styles.mapOpener}>
            門市地圖
          </button>
        );
      }}
    </Query>
  );
}

export default StoreMapOpener;

export function wrapOpenerInForm(form: string) {
  if (!form) return null;

  const selector = formValueSelector(form);

  return memo(connect(
    state => ({
      cityId: selector(state, 'Main.Customize.Pay.cityId'),
      storeId: selector(state, 'Main.Customize.Pay.storeId'),
    }),
    dispatch => bindActionCreators({
      setStoreId: v => change(form, 'Main.Customize.Pay.storeId', v),
      ...LightboxActions,
    }, dispatch)
  )(StoreMapOpener));
}
