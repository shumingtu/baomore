// @flow
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Mutation } from 'react-apollo';
import {
  getFormValues,
} from 'redux-form';
// actions
import * as LightboxActions from '../../../actions/Lightbox.js';
// config
import { MAKE_ONLINE_ORDER } from '../../../mutations/Payment.js';
import { PHONE_CUSTOMIZE_FORM } from '../../../shared/Form.js';
import { LINE_PAY_LIGHTBOX } from '../../../shared/LightboxTypes.js';
// components
import CheckoutButton from '../Operations/CheckoutButton.jsx';

type Props = {
  formValues: {
    Main: {
      customizeCategory: string,
      Customize: {
        Pay: {
          cityId: number,
          storeId: number,
          date: string,
          time: string,
          name: string,
          email: string,
          phone: string,
        },
        images: {
          id: number,
        },
        isPublic: boolean,
      },
    },
  },
  currentCustomizeUrl: string,
  memberStoredJSON: string,
  mainCategories: Array<{
    id: number,
    name: string,
  }>,
  openLightbox: Function,
};

class MakeOrderButton extends PureComponent<Props> {
  render() {
    const {
      formValues,
      currentCustomizeUrl,
      memberStoredJSON,
      mainCategories = [],
      openLightbox,
    } = this.props;

    return (
      <Mutation mutation={MAKE_ONLINE_ORDER}>
        {(makeOnlineOrder, { loading }) => (
          <CheckoutButton
            forceDisabled={loading}
            needCheckPaycheckInfo
            onClick={async () => {
              if (formValues && formValues.Main && formValues.Main.Customize) {
                const isAllCustomized = formValues.Main.customizeCategory === '客製化包膜' || false;
                const categoryId = isAllCustomized && mainCategories.find(m => m.id === 5)
                  ? mainCategories.find(m => m.name === '客製化包膜').id
                  : parseInt(formValues.Main.customizeCategory, 10);
                const wrapPNtable = !Array.isArray(formValues.Main.Customize.images)
                  ? (formValues.Main.Customize.images.id || null)
                  : null;
                const pnTableId = isAllCustomized
                  ? categoryId
                  : wrapPNtable;
                const paymentInfo = formValues.Main.Customize.Pay || {};
                const isPublic = formValues.Main.Customize.isPublic || false;

                const {
                  data,
                } = await makeOnlineOrder({
                  variables: {
                    pnTableId,
                    picture: currentCustomizeUrl,
                    storeId: (paymentInfo.storeId && parseInt(paymentInfo.storeId, 10)) || null,
                    date: paymentInfo.date || null,
                    time: paymentInfo.time || null,
                    name: paymentInfo.name || null,
                    email: paymentInfo.email || null,
                    phone: paymentInfo.phone || null,
                    isShared: isPublic,
                    storedDesignJSON: memberStoredJSON,
                    isAllCustomized,
                  },
                });

                if (data && data.makeOnlineOrder) {
                  openLightbox({
                    type: LINE_PAY_LIGHTBOX,
                    human: data.makeOnlineOrder.human || null,
                    status: data.makeOnlineOrder.status || false,
                    token: data.makeOnlineOrder.payToken || null,
                  });
                }
              }
            }} />
        )}
      </Mutation>
    );
  }
}

const reduxHook = connect(
  state => ({
    formValues: getFormValues(PHONE_CUSTOMIZE_FORM)(state),
    currentCustomizeUrl: state.Customize.currentCustomizeUrl,
    memberStoredJSON: state.Member.memberStoredJSON,
    mainCategories: state.Customize.mainCategories,
  }),
  dispatch => bindActionCreators({
    ...LightboxActions,
  }, dispatch)
);

export default reduxHook(
  MakeOrderButton
);
