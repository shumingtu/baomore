// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Field,
  formValueSelector,
  change,
} from 'redux-form';
import {
  Query,
} from 'react-apollo';
// config
import Theme from '../../../styles/Theme.js';
import { PHONE_CUSTOMIZE_FORM } from '../../../shared/Form.js';
import {
  GET_CITY_DISTRICTS,
  GET_STORES,
  GET_ALREADY_BOOKED_DATES,
  GET_AVAILABLE_BOOK_TIMES,
} from '../../../queries/Customize.js';
// components
import FormLabel from '../../Form/FormLabel.jsx';
import Selector from '../../Form/Selector.jsx';
import Input from '../../Form/Input.jsx';
import CalendarBoard from '../../Form/Calendar/CalendarBoard.jsx';
import { wrapOpenerInForm } from './StoreMapOpener.jsx';

const StoreMapOpener = wrapOpenerInForm(PHONE_CUSTOMIZE_FORM);
const selector = formValueSelector(PHONE_CUSTOMIZE_FORM);

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    position: 'relative',
  },
  formWrapper: {
    width: '100%',
    height: 'auto',
    backgroundColor: '#fff',
    borderRadius: 8,
    boxShadow: Theme.BLOCK_SHADOW,
    padding: 24,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  inputWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    padding: '12px 0',
    position: 'relative',
  },
  decoratorWrapper: {
    position: 'absolute',
    right: 0,
    top: 15,
    width: 'auto',
    height: 'auto',
  },
};

type Props = {
  cityId: number,
  storeId: number,
  selectedDate: string,
  resetField: Function,
  resetDate: Function,
  customizeCategoryName: string,
};

class PayCheckForm extends PureComponent<Props> {
  componentDidUpdate(prevProps) {
    const {
      cityId,
      storeId,
      selectedDate,
      resetField,
      resetDate,
    } = this.props;

    if (cityId !== prevProps.cityId) {
      resetField('Main.Customize.Pay.storeId');
    }

    if (storeId !== prevProps.storeId) {
      resetDate();
    }

    if (selectedDate !== prevProps.selectedDate) {
      resetField('Main.Customize.Pay.time');
    }
  }

  render() {
    const {
      cityId,
      storeId,
      selectedDate,
      customizeCategoryName,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <FormLabel label="填寫取貨資訊" />
        <div style={styles.formWrapper}>
          <div style={styles.inputWrapper}>
            <FormLabel label="所在縣市" />
            <Query query={GET_CITY_DISTRICTS}>
              {({
                data: {
                  cityDistricts = [],
                },
              }) => (
                <Field
                  name="cityId"
                  options={cityDistricts}
                  placeholder="選擇所在縣市"
                  component={Selector} />
              )}
            </Query>
          </div>
          <div style={styles.inputWrapper}>
            <FormLabel label="預約門市" />
            <Query
              skip={!cityId || cityId === '-1'}
              query={GET_STORES}
              variables={{
                cityId: cityId ? parseInt(cityId, 10) : null,
                isForClient: true,
                isForCustomize: true,
              }}>
              {({
                data,
              }) => {
                const stores = (data && data.stores) || [];
                return (
                  <Field
                    name="storeId"
                    options={stores}
                    placeholder="選擇取貨門市"
                    component={Selector} />
                );
              }}
            </Query>
            <div style={styles.decoratorWrapper}>
              <StoreMapOpener />
            </div>
          </div>
          <div style={styles.inputWrapper}>
            <FormLabel label="預計可預約日" />
            <Query
              skip={!storeId || storeId === '-1'}
              query={GET_ALREADY_BOOKED_DATES}
              fetchPolicy="network-only"
              variables={{
                storeId: storeId ? parseInt(storeId, 10) : -1,
                categoryName: customizeCategoryName,
              }}>
              {({
                data,
              }) => {
                const alreadyBookedDates = (data && data.alreadyBookedDates) || [];

                return (
                  <Field
                    name="date"
                    isNotEditable={!storeId}
                    notAvailableDateList={alreadyBookedDates.map(d => ({
                      id: d.date || null,
                      name: d.date || null,
                    }))}
                    placeholder="選擇預約日期"
                    component={CalendarBoard} />
                );
              }}
            </Query>
          </div>
          <div style={styles.inputWrapper}>
            <FormLabel label="預約時間" />
            <Query
              skip={!storeId || !selectedDate}
              query={GET_AVAILABLE_BOOK_TIMES}
              fetchPolicy="network-only"
              variables={{
                storeId: storeId ? parseInt(storeId, 10) : 1,
                date: selectedDate || '',
              }}>
              {({
                data,
              }) => {
                const availableBookedTimes = (data && data.availableBookedTimes) || [];

                return (
                  <Field
                    name="time"
                    options={availableBookedTimes.map(t => ({
                      id: t.time || null,
                      name: t.time || null,
                    }))}
                    placeholder="選擇取貨時間"
                    component={Selector} />
                );
              }}
            </Query>
          </div>
          <div style={styles.inputWrapper}>
            <FormLabel label="個人資料" />
            <Field
              name="name"
              placeholder="姓名"
              component={Input} />
          </div>
          <div style={styles.inputWrapper}>
            <Field
              type="email"
              name="email"
              placeholder="電子郵件"
              component={Input} />
          </div>
          <div style={styles.inputWrapper}>
            <Field
              type="tel"
              name="phone"
              placeholder="手機號碼"
              component={Input} />
          </div>
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    cityId: selector(state, 'Main.Customize.Pay.cityId'),
    storeId: selector(state, 'Main.Customize.Pay.storeId'),
    selectedDate: selector(state, 'Main.Customize.Pay.date'),
    customizeCategoryName: selector(state, 'Main.customizeCategoryName'),
  }),
  dispatch => bindActionCreators({
    resetField: f => change(PHONE_CUSTOMIZE_FORM, f, '-1'),
    resetDate: () => change(PHONE_CUSTOMIZE_FORM, 'Main.Customize.Pay.date', null),
  }, dispatch)
);

export default reduxHook(
  radium(
    PayCheckForm
  )
);
