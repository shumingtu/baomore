// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// config
import Theme from '../../styles/Theme.js';
// actions
import * as CustomizeActions from '../../actions/Customize.js';

const styles = {
  categoryWrapper: {
    width: 168,
    height: 'auto',
    border: 0,
    borderRadius: 8,
    padding: 16,
    backgroundColor: '#fff',
    outline: 0,
    boxShadow: Theme.BLOCK_SHADOW,
    cursor: 'pointer',
    ':hover': {
      opacity: 0.9,
    },
    '@media (max-width: 767px)': {
      width: 150,
    },
  },
  categoryInnerWrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  categoryImage: {
    width: 136,
    height: 107,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    '@media (max-width: 767px)': {
      width: 118,
      height: 93,
    },
  },
  categoryLabel: {
    fontSize: 14,
    fontWeight: 500,
    paddingTop: 9,
    letterSpacing: 0.9,
    color: Theme.GRAY_TEXT_COLOR,
    '@media (max-width: 767px)': {
      fontSize: 13,
    },
  },
};

type Props = {
  history: {
    push: Function,
  },
  id: number,
  image: string,
  label: string,
  saveCustomizeCategory: Function,
  saveCustomizeCategoryName: Function,
};

class Category extends PureComponent<Props> {
  setCategory() {
    const {
      history,
      id,
      label,
      saveCustomizeCategory,
      saveCustomizeCategoryName,
    } = this.props;

    if (label === '客製化包膜') {
      saveCustomizeCategory(`${label}`);
    } else {
      saveCustomizeCategory(id);
    }

    saveCustomizeCategoryName(`${label}`);

    history.push('/customize/create');
  }

  render() {
    const {
      image,
      label,
    } = this.props;

    return (
      <button
        type="button"
        onClick={() => this.setCategory()}
        style={styles.categoryWrapper}>
        <div style={styles.categoryInnerWrapper}>
          <div
            style={[
              styles.categoryImage,
              {
                backgroundImage: image ? `url(${image})` : null,
              },
            ]} />
          <span style={styles.categoryLabel}>
            {label || null}
          </span>
        </div>
      </button>
    );
  }
}

const reduxHook = connect(
  () => ({}),
  dispatch => bindActionCreators({
    ...CustomizeActions,
  }, dispatch)
);

export default withRouter(
  reduxHook(
    radium(
      Category
    )
  )
);
