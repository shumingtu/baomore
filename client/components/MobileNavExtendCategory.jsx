// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import {
  withRouter,
} from 'react-router-dom';

// static
import arrowRight from '../static/images/arrow-right-icon.png';
import arrowDown from '../static/images/arrow-down-icon.png';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  itemGroup: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  button: {
    padding: 0,
    margin: 0,
    borderTop: 0,
    borderRight: 0,
    borderLeft: 0,
    borderBottom: 0,
    textDecoration: 'none',
    backgroundColor: 'transparent',
    outline: 0,
    cursor: 'pointer',
  },
  item: {
    fontSize: 16,
    textAlign: 'left',
    color: 'rgba(0, 0, 0, 0.8)',
    letterSpacing: 1,
    padding: '12px 0',
  },
  itemParent: {
    flex: 1,
    maxWidth: 160,
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  itemChild: {
    flex: 1.2,
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  icon: {
    width: 16,
    height: 16,
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
};

type Props = {
  nav: {
    title: string,
    categories: Array<{
      path: string,
      name: string,
    }>,
  },
  isLastCategory: boolean,
  history: {
    push: Function,
  },
  onClose: Function,
};

type State = {
  showChildren: boolean,
};

class MobileNavExtendCategory extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      showChildren: false,
    };
  }

  render() {
    const {
      nav,
      isLastCategory,
      history,
      onClose,
    } = this.props;

    if (!nav) return null;

    const {
      showChildren,
    } = this.state;

    const {
      categories = [],
    } = nav;

    return (
      <div style={styles.wrapper}>
        <button
          type="button"
          onClick={() => this.setState({ showChildren: !showChildren })}
          style={[
            styles.itemGroup,
            styles.button,
            {
              borderBottom: '1px solid rgba(0, 0, 0, 0.2)',
            },
          ]}>
          <div
            style={[
              styles.item,
              styles.itemParent,
            ]}>
            {nav.title}
            <div
              style={[
                styles.icon,
                {
                  backgroundImage: showChildren ? `url(${arrowDown})` : `url(${arrowRight})`,
                },
              ]} />
          </div>
          <div style={styles.itemChild} />
        </button>
        {showChildren ? categories.map((c, idx) => (
          <div
            key={c.name}
            style={[
              styles.itemGroup,
              idx === categories.length - 1 && !isLastCategory && {
                borderBottom: '1px solid rgba(0, 0, 0, 0.2)',
              },
            ]}>
            <div
              style={[
                styles.item,
                styles.itemParent,
              ]} />
            {c.link ? (
              <a
                target="_blank"
                rel="noreferrer noopener"
                key={`${c.link}-${c.name}`}
                href={c.link}
                style={[
                  styles.itemChild,
                  styles.button,
                  styles.item,
                  idx !== categories.length - 1 && {
                    borderBottom: '1px solid rgba(0, 0, 0, 0.2)',
                  },
                ]}>
                {c.name}
              </a>
            ) : (
              <button
                type="button"
                onClick={() => {
                  history.push(c.path);
                  if (onClose) {
                    onClose();
                  }
                }}
                style={[
                  styles.itemChild,
                  styles.button,
                  styles.item,
                  idx !== categories.length - 1 && {
                    borderBottom: '1px solid rgba(0, 0, 0, 0.2)',
                  },
                ]}>
                {c.name}
              </button>
            )}
          </div>
        )) : null}
      </div>
    );
  }
}

export default withRouter(
  radium(
    MobileNavExtendCategory
  )
);
