// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import {
  Field,
} from 'redux-form';
// components
import Selector from '../Form/Selector.jsx';

const styles = {
  selectionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '16px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
      justifyContent: 'flex-start',
    },
  },
  selection: {
    width: 220,
    height: 'auto',
    margin: 12,
  },
  selector: {
    padding: '7px 16px',
    borderTop: '1px solid rgba(153, 153, 153, 0.6)',
    borderLeft: '1px solid rgba(153, 153, 153, 0.6)',
    borderRight: '1px solid rgba(153, 153, 153, 0.6)',
    borderBottom: '1px solid rgba(153, 153, 153, 0.6)',
    borderRadius: 12,
  },
};

type Props = {
  currentCityId: string,
  cityDistricts: Array<{
    id: number,
    name: string,
    disticts: Array<{
      id: number,
      name: string,
    }>,
  }>,
  stores: Array<{
    id: number,
    name: string,
  }>,
  channelList: Array<{
    id: number,
    name: string,
  }>,
};

class ContactMapSelectors extends PureComponent<Props> {
  render() {
    const {
      stores,
      currentCityId,
      cityDistricts,
      channelList,
    } = this.props;

    const currentCity = currentCityId && ~parseInt(currentCityId, 10)
      ? cityDistricts.find(c => c.id === parseInt(currentCityId, 10))
      : null;
    const currentDistrictList = (currentCity && currentCity.disticts) || [];

    return (
      <div style={styles.selectionWrapper}>
        <div style={styles.selection}>
          <Field
            name="cityId"
            options={cityDistricts.map(c => ({
              id: c.id,
              name: c.name,
            }))}
            placeholder="選擇縣市"
            component={Selector}
            customStyle={styles.selector} />
        </div>
        <div style={styles.selection}>
          <Field
            name="districtId"
            options={currentDistrictList}
            placeholder="選擇區域"
            component={Selector}
            customStyle={styles.selector} />
        </div>
        <div style={styles.selection}>
          <Field
            name="channelId"
            options={channelList}
            placeholder="選擇電信商"
            component={Selector}
            customStyle={styles.selector} />
        </div>
        <div style={styles.selection}>
          <Field
            name="storeId"
            options={stores}
            placeholder="選擇門市據點"
            component={Selector}
            customStyle={styles.selector} />
        </div>
      </div>
    );
  }
}

export default radium(
  ContactMapSelectors
);
