// @flow
import React from 'react';
import radium from 'radium';
import { Link as l } from 'react-router-dom';

import Theme from '../../styles/Theme.js';

const Link = radium(l);

const styles = {
  wrapper: {
    width: 270,
    height: 500,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  iconWrapper: {
    width: 200,
    height: 200,
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
  },
  titleWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottom: `1px solid ${Theme.UNDERLINE_COLOR}`,
  },
  title: {
    fontSize: 26,
    fontWeight: 700,
    letterSpacing: 2,
    color: Theme.BLACK_COLOR,
  },
  descWrapper: {
    width: '100%',
    height: 'auto',
    flex: 1,
    padding: '24px 12px',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  desc: {
    fontSize: 16,
    fontWeight: 400,
    color: 'rgba(0, 0, 0, 0.6)',
    letterSpacing: 2,
    lineHeight: 1.4,
    whiteSpace: 'pre-line',
  },
  buttonWrapper: {
    width: '100%',
    height: 'auto',
    padding: '4px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    width: 'auto',
    height: 'auto',
    margin: 8,
    padding: '4px 16px',
    backgroundColor: 'transparent',
    borderRadius: 16,
    border: '1px solid rgb(173, 0, 129)',
    outline: 0,
    fontSize: 16,
    color: 'rgb(173, 0, 129)',
    textAlign: 'center',
    textDecoration: 'none',
    cursor: 'pointer',
    ':hover': {
      opacity: 0.8,
    },
  },
};

type Props = {
  category: {
    icon: string,
    title: string,
    desc: string,
    button: {
      isLink: boolean,
      path?: string,
      onClick?: Function,
      name: string,
    },
  },
};

function ContactCategory({
  category,
}: Props) {
  if (!category) return null;

  const {
    icon,
    title,
    desc,
    button,
  } = category;

  return (
    <div style={styles.wrapper}>
      <div
        style={[
          styles.iconWrapper,
          {
            backgroundImage: icon ? `url(${icon})` : null,
          },
        ]} />
      <div style={styles.titleWrapper}>
        <span style={styles.title}>
          {title || null}
        </span>
      </div>
      <div style={styles.descWrapper}>
        <span style={styles.desc}>
          {desc || null}
        </span>
      </div>
      {button ? (
        <div style={styles.buttonWrapper}>
          {button.isLink ? (
            <Link
              to={{ pathname: button.path }}
              style={styles.button}>
              {button.name || null}
            </Link>
          ) : (
            <button
              key="link-button"
              type="button"
              onClick={button.onClick || null}
              style={styles.button}>
              {button.name || null}
            </button>
          )}
        </div>
      ) : null}
    </div>
  );
}

export default radium(ContactCategory);
