// @flow
import React from 'react';
import radium from 'radium';

import Theme from '../../styles/Theme.js';
import closeIcon from '../../static/images/contact/close-icon.png';
import northQrcode from '../../static/images/contact/north-qrcode.png';
import midQrcode from '../../static/images/contact/mid-qrcode.png';
import southQrcode from '../../static/images/contact/south-qrcode.png';

const styles = {
  wrapper: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    padding: 24,
  },
  placement: {
    width: '100%',
    height: 'auto',
    borderRadius: 12,
    backgroundColor: '#fff',
    boxShadow: Theme.CLIENT_BLOCK_SHADOW,
    padding: '48px 24px',
    position: 'relative',
  },
  closeBtn: {
    position: 'absolute',
    right: 16,
    top: 16,
    width: 24,
    height: 24,
    border: 0,
    backgroundColor: 'transparent',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundImage: `url(${closeIcon})`,
    outline: 0,
    cursor: 'pointer',
  },
  titleWrapper: {
    width: '100%',
    height: 'auto',
    padding: '24px 12px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
    },
  },
  title: {
    fontSize: 32,
    fontWeight: 700,
    letterSpacing: 2,
    color: Theme.BLACK_COLOR,
  },
  qrcodesPlacement: {
    width: '100%',
    height: 'auto',
    padding: 12,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  qrcodeInnerPlacement: {
    width: '100%',
    height: 'auto',
    maxWidth: 800,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
    },
  },
  qrcodeWrapper: {
    width: 200,
    height: 250,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  qrcodeLink: {
    width: 200,
    height: 200,
    border: 0,
    textDecoration: 'none',
    backgroundColor: 'transparent',
    outline: 0,
    padding: 0,
    margin: 0,
    cursor: 'pointer',
  },
  qrcode: {
    width: 200,
    height: 200,
  },
  text: {
    fontSize: 16,
    fontWeight: 400,
    textAlign: 'center',
    letterSpacing: 1,
    padding: '12px 0',
    color: 'rgba(0, 0, 0, 0.6)',
  },
  qrcodeDesc: {
    fontSize: 12,
    fontWeight: 400,
    padding: '12px 0',
    color: 'rgba(0, 0, 0, 0.5)',
  },
  annotationWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      padding: 12,
    },
  },
};

type Props = {
  onClose: Function,
};

const QRCODES = [{
  src: northQrcode,
  title: null,
  desc: '(新竹以北、花蓮、台東)',
  link: 'line://ti/p/@cmm7559c',
}, {
  src: midQrcode,
  title: null,
  desc: '(苗栗、台中、彰化、南投)',
  link: 'line://ti/p/@own0842p',
}, {
  src: southQrcode,
  title: null,
  desc: '(雲林以南、金門、澎湖)',
  link: 'line://ti/p/@bfd7249y',
}];

function ContactPopup({
  onClose,
}: Props) {
  return (
    <div style={styles.wrapper}>
      <div style={styles.placement}>
        <button
          type="button"
          onClick={onClose}
          style={styles.closeBtn} />
        <div style={styles.titleWrapper}>
          <span style={styles.title}>
            artmo
          </span>
          <span style={styles.title}>
            線上預約
          </span>
        </div>
        <div style={styles.qrcodesPlacement}>
          <div style={styles.qrcodeInnerPlacement}>
            {QRCODES.map(qrcode => (
              <div key={qrcode.src} style={styles.qrcodeWrapper}>
                {qrcode.link ? (
                  <a
                    rel="noopener noreferrer"
                    href={qrcode.link}
                    target="_blank"
                    style={styles.qrcodeLink}>
                    <img
                      alt="QRCODE"
                      src={qrcode.src}
                      style={styles.qrcode} />
                  </a>
                ) : (
                  <img
                    alt="QRCODE"
                    src={qrcode.src}
                    style={styles.qrcode} />
                )}
                {qrcode.title ? (
                  <span style={styles.text}>
                    {qrcode.title}
                  </span>
                ) : null}
                {qrcode.desc ? (
                  <span style={styles.qrcodeDesc}>
                    {qrcode.desc}
                  </span>
                ) : null}
              </div>
            ))}
          </div>
        </div>
        <div style={styles.annotationWrapper}>
          <span style={styles.text}>
            選擇區域圖示點擊加入artmo好友，即可開始預約。
          </span>
        </div>
      </div>
    </div>
  );
}

export default radium(ContactPopup);
