// @flow
/* eslint jsx-a11y/no-static-element-interactions: 0 */

import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import moment from 'moment';

import arrowDown from '../../../static/images/arrow-down-icon.png';
import CalendarDatePicker from './CalendarDatePicker.jsx';
import CalendarSelector from './CalendarSelector.jsx';

import Theme from '../../../styles/Theme.js';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    borderRadius: 0,
    borderLeft: 0,
    borderTop: 0,
    borderRight: 0,
    borderBottom: '1px solid #999999',
    padding: '7px 4px',
    cursor: 'pointer',
    outline: 'none',
    backgroundColor: 'transparent',
    textAlign: 'left',
    position: 'relative',
  },
  fieldText: {
    fontSize: 14,
    color: Theme.DEFAULT_TEXT_COLOR,
  },
  error: {
    position: 'absolute',
    right: 55,
    top: 'calc(50% - 8px)',
    fontSize: 13,
    color: '#f01a66',
    lineHeight: 1,
    letterSpacing: '1px',
    pointerEvents: 'none',
  },
  selectSvgWrap: {
    position: 'absolute',
    right: 22,
    top: 'calc(50% - 3px)',
    width: 10,
    height: 6,
    pointerEvents: 'none',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundImage: `url(${arrowDown})`,
  },
  calendarWrapper: {
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1000,
    backgroundColor: '#fff',
    padding: 4,
    borderRadius: 2,
    border: '1px solid #f2f2f2',
    boxShadow: '2px 10px 20px rgba(0, 0, 0, 0.24)',
    '@media (max-width: 767px)': {
      width: '100%',
    },
  },
  clickableBackground: {
    position: 'fixed',
    zIndex: 999,
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    cursor: 'pointer',
  },
  disable: {
    cursor: 'default',
  },
};

type Props = {
  input: {
    name: string,
    value: string,
    onChange: Function,
  },
  placeholder?: string,
  notAvailableDateList: Array<{
    id: string,
    name: string,
  }>,
  meta: {
    error: string,
  },
  isNotEditable: boolean,
};

type State = {
  showSchedule: boolean,
  currentYear: number,
  currentMonth: number,
};

const defaultDateBound = {
  minDate: moment().format('YYYY-MM-DD'),
  maxDate: moment().add(2, 'month').format('YYYY-MM-DD'),
};

class CalendarBoard extends PureComponent<Props, State> {
  static defaultProps = {
    placeholder: '選擇日期',
  };

  constructor(props) {
    super(props);

    this.state = {
      currentMonth: moment().month() + 1,
      currentYear: moment().year(),
      showSchedule: false,
    };
  }

  render() {
    const {
      isNotEditable,
      input: {
        value,
        onChange,
      },
      placeholder,
      meta: {
        error,
      },
      notAvailableDateList,
    } = this.props;

    const {
      currentYear,
      currentMonth,
      showSchedule,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        {!showSchedule ? (
          <button
            type="button"
            onClick={!isNotEditable ? () => this.setState({ showSchedule: true }) : null}
            style={styles.fieldWrapper}>
            {error ? (
              <span style={styles.error}>
                {error}
              </span>
            ) : null}
            <span style={styles.fieldText}>
              {value || placeholder || null}
            </span>
            <div style={styles.selectSvgWrap} />
          </button>
        ) : (
          <div style={styles.calendarWrapper}>
            <CalendarSelector
              defaultDateBound={defaultDateBound}
              currentYear={currentYear}
              currentMonth={currentMonth}
              onChange={month => this.setState({ currentMonth: month })}
              yearOnChange={(year, month) => this.setState({
                currentYear: year,
                currentMonth: month,
              })} />
            <CalendarDatePicker
              notAvailableDateList={notAvailableDateList}
              defaultDateBound={defaultDateBound}
              year={currentYear}
              month={currentMonth}
              onClick={(date) => {
                onChange(moment(date).format('YYYY-MM-DD'));
                this.setState({
                  showSchedule: false,
                });
              }} />
          </div>
        )}
        {showSchedule ? (
          <div
            style={styles.clickableBackground}
            onKeyDown={() => {}}
            onClick={() => this.setState({
              showSchedule: false,
            })} />
        ) : null}
      </div>
    );
  }
}

export default radium(CalendarBoard);
