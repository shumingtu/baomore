// @flow

import React, {
  PureComponent,
  Fragment,
} from 'react';
import moment from 'moment';
import radium from 'radium';
import getWeeksInMonth from '../../../helper/getWeeksInMonth.js';
import CalendarTheme from '../../../styles/Calendar.js';

const styles = {
  header: {
    width: '100%',
    height: 30,
    backgroundColor: CalendarTheme.CALENDAR_HEADER_BACKGROUND,
    border: `1px solid ${CalendarTheme.CALENDAR_HEADER_BORDER_COLOR}`,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerFields: {
    flex: '1 0 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0px 2px',
  },
  calendarWrap: {
    margin: 0,
  },
  weekWrap: {
    display: 'flex',
    flexWrap: 'no-wrap',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '2px 0',
  },
  dayblock: {
    flex: '1 0 0',
    height: 35,
    margin: 2,
    backgroundColor: CalendarTheme.CALENDAR_DAYBLOCK_BACKGROUND,
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
    ':hover': {
      border: `1px solid ${CalendarTheme.CALENDAR_DAYBLOCK_HOVER_BORDER_COLOR}`,
    },
  },
  disabled: {
    cursor: 'default',
    ':hover': {
      border: 0,
    },
  },
  whiteCalendar: {
    headerFields: {
      color: CalendarTheme.CALENDAR_HEADER_TEXT_COLOR,
    },
    dayNumber: {
      fontSize: 13,
      color: CalendarTheme.CALENDAR_DAYBLOCK_TEXT_COLOR,
      fontWeight: 500,
    },
    daysNotThisMonth: {
      color: CalendarTheme.CALENDAR_DAYBLOCK_DISABLED_TEXT_COLOR,
      opacity: 0.64,
    },
    disabled: {
      color: CalendarTheme.CALENDAR_DAYBLOCK_DISABLED_TEXT_COLOR,
      opacity: 0.3,
    },
  },
};

type Props = {
  defaultDateBound: {
    minDate: string,
    maxDate: string,
  },
  notAvailableDateList: Array<{
    id: string,
    name: string,
  }>,
  month: number,
  year: number,
  onClick: Function,
};

const daysInWeek = moment.weekdaysShort();

class CalendarDatePicker extends PureComponent<Props> {
  render() {
    const {
      notAvailableDateList,
      defaultDateBound,
      month,
      year,
      onClick,
    } = this.props;

    const monthWeeks = getWeeksInMonth(month, year);

    return (
      <Fragment>
        <header style={styles.header}>
          {daysInWeek.map(day => (
            <span
              key={day}
              style={[
                styles.headerFields,
                styles.whiteCalendar.headerFields,
              ]}>
              {day}
            </span>
          ))}
        </header>
        <div style={styles.calendarWrap}>
          {monthWeeks.map(week => (
            <div key={week} style={styles.weekWrap}>
              {week.map((date, idx) => {
                const isDisabled = defaultDateBound
                  ? (
                    moment(defaultDateBound.maxDate).diff(date, 'day') < 0
                    || date.diff(moment(defaultDateBound.minDate), 'day') < 0
                    || notAvailableDateList.find(l => l.id === date.format('YYYY-MM-DD'))
                  )
                  : false;

                return (
                  <div
                    key={date}
                    role="button"
                    tabIndex={idx}
                    onKeyDown={() => {}}
                    onClick={!isDisabled ? () => onClick(date) : null}
                    style={[
                      styles.dayblock,
                      isDisabled && styles.disabled,
                    ]}>
                    <span
                      style={[
                        styles.whiteCalendar.dayNumber,
                        date.month() + 1 !== month && styles.whiteCalendar.daysNotThisMonth,
                        isDisabled && styles.whiteCalendar.disabled,
                      ]}>
                      {date.format('DD')}
                    </span>
                  </div>
                );
              })}
            </div>
          ))}
        </div>
      </Fragment>
    );
  }
}

export default radium(
  CalendarDatePicker
);
