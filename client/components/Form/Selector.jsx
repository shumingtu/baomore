// @flow

import React from 'react';
import radium from 'radium';

import arrowDown from '../../static/images/arrow-down-icon.png';

const styles = {
  selectGroup: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  selector: {
    width: '100%',
    height: 'auto',
    borderRadius: 0,
    borderLeft: 0,
    borderTop: 0,
    borderRight: 0,
    borderBottom: '1px solid #999999',
    padding: '7px 4px',
    fontSize: 14,
    color: 'rgba(0, 0, 0, 0.87)',
    cursor: 'pointer',
    outline: 'none',
    appearance: 'none',
    WebkitAppearance: 'none',
    MozAppearance: 'none',
    backgroundColor: 'transparent',
  },
  error: {
    position: 'absolute',
    right: 55,
    top: 'calc(50% - 8px)',
    fontSize: 13,
    color: '#f01a66',
    lineHeight: 1,
    letterSpacing: '1px',
    pointerEvents: 'none',
  },
  selectSvgWrap: {
    position: 'absolute',
    right: 22,
    top: 'calc(50% - 3px)',
    width: 10,
    height: 6,
    pointerEvents: 'none',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundImage: `url(${arrowDown})`,
  },
};

type Props = {
  options: Array<{
    id: number | string,
    name: string,
  }>,
  input: {
    name: string,
    value: string | number,
    onChange: Function,
  },
  placeholder?: string,
  meta: {
    error: string,
  },
  customStyle?: Object,
  customOnChange?: Function,
};

function Selector({
  options,
  input: {
    name,
    value,
    onChange,
  },
  placeholder,
  meta: {
    error,
  },
  customStyle,
  customOnChange,
}: Props) {
  return (
    <div style={styles.selectGroup}>
      {error ? (
        <span style={styles.error}>
          {error}
        </span>
      ) : null}
      <select
        name={name}
        style={[
          styles.selector,
          customStyle,
        ]}
        value={value}
        onChange={(e) => {
          onChange(e.target.value);

          if (customOnChange) {
            customOnChange(options.find(o => o.id === parseInt(e.target.value, 10)) || null);
          }
        }}>
        {placeholder ? (
          <option key="-1" value="-1">{placeholder}</option>
        ) : null}
        {options ? options.map(t => (
          <option
            key={t.id}
            value={t.id}>
            {t.name}
          </option>
        )) : null}
      </select>
      <div style={styles.selectSvgWrap} />
    </div>
  );
}

Selector.defaultProps = {
  placeholder: null,
  customStyle: null,
  customOnChange: null,
};

export default radium(Selector);
