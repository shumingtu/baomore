// @flow
import React from 'react';
import radium from 'radium';
// config
import Theme from '../../styles/Theme.js';
// component
import FormLabel from './FormLabel.jsx';

const styles = {
  placement: {
    width: '100%',
    height: 'auto',
    padding: '8px 0',
  },
  wrapper: {
    width: '100%',
    height: '100%',
    padding: '16px 32px',
    backgroundColor: '#fff',
    borderRadius: 8,
    boxShadow: Theme.BLOCK_SHADOW,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    '@media (max-width: 767px)': {
      padding: '8px 16px',
    },
  },
  value: {
    fontSize: 20,
    color: Theme.BUTTON_BACKGROUND_COLOR,
    '@media (max-width: 767px)': {
      fontSize: 16,
    },
  },
};

type Props = {
  label: string,
  input: {
    value: string | number,
  },
};

function SingleInfo({
  label = '',
  input: {
    value,
  },
}: Props) {
  return (
    <div style={styles.placement}>
      <div style={styles.wrapper}>
        <FormLabel label={label} />
        <span style={styles.value}>
          {value}
        </span>
      </div>
    </div>
  );
}

export default radium(SingleInfo);
