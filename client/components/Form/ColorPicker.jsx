// @flow
import 'rc-color-picker/assets/index.css';
import React from 'react';
import radium from 'radium';
import ColorPickerComponent from 'rc-color-picker';

const styles = {
  wrapper: {
    width: 'auto',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: '6px 0',
  },
};

type Props = {
  input: {
    value: {
      color: string,
    },
    onChange: Function,
  },
  onOpen: Function,
  onClose: Function,
  renderOnTargetContainer: Element,
};

function ColorPicker({
  input: {
    value,
    onChange,
  },
  onOpen,
  onClose,
  renderOnTargetContainer,
}: Props) {
  return (
    <div style={styles.wrapper}>
      <ColorPickerComponent
        enableAlpha={false}
        animation="slide-up"
        color={value ? value.color : '#000'}
        getCalendarContainer={() => {
          if (renderOnTargetContainer) {
            return renderOnTargetContainer;
          }

          return document.body;
        }}
        onOpen={() => {
          if (onOpen) {
            onOpen();
          }
        }}
        onClose={() => {
          if (onClose) {
            onClose();
          }
        }}
        onChange={onChange} />
    </div>
  );
}

export default radium(ColorPicker);
