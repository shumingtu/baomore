// @flow
import React, { memo } from 'react';

const styles = {
  wrapper: {
    fontSize: 12,
    fontWeight: 500,
    color: 'rgba(102, 102, 102, 0.92)',
    letterSpacing: 1,
    padding: '6px 8px 6px 0',
  },
};

function FormLabel({
  label,
}: {
  label: string,
}) {
  return (
    <span style={styles.wrapper}>
      {label || null}
    </span>
  );
}

export default memo(FormLabel);
