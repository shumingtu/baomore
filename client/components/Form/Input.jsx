// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

// config
import Theme from '../../styles/Theme.js';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    fontSize: 14,
    fontWeight: 600,
    outline: 0,
    borderLeft: 0,
    borderTop: 0,
    borderRight: 0,
    borderBottom: '1px solid #999999',
    borderRadius: 0,
    color: Theme.DEFAULT_TEXT_COLOR,
    '::-webkit-input-placeholder': {
      color: 'rgba(0, 0, 0, 0.28)',
    },
  },
  error: {
    borderBottom: '1px solid rgb(238, 96, 82)',
  },
};

type Props = {
  input: {
    value: string,
    name: string,
    onChange: Function,
  },
  placeholder?: string,
  type?: string,
  style?: Object,
  errorStyle?: Object,
  disabled?: boolean,
  meta: {
    error: string,
  },
};

class Input extends PureComponent<Props> {
  static defaultProps = {
    type: 'text',
    style: {},
    errorStyle: {},
    placeholder: '',
    disabled: false,
  };

  render() {
    const {
      input: {
        value,
        onChange,
        name,
      },
      placeholder,
      type,
      style,
      errorStyle,
      disabled,
      meta: {
        error,
      },
    } = this.props;

    const customStyles = Array.isArray(style) ? style : [style];

    return (
      <input
        id={name}
        key={`${name}-input`}
        type={type}
        value={value}
        onChange={e => onChange(e)}
        onKeyPress={(e) => {
          if (e.key === 'Enter') e.preventDefault();
        }}
        name={name}
        placeholder={placeholder}
        disabled={disabled}
        style={[
          styles.wrapper,
          ...customStyles,
          error && styles.error,
          error && errorStyle,
        ]} />
    );
  }
}

export default radium(Input);
