// @flow
import React from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
// config
import Theme from '../../styles/Theme.js';
import { openLightbox } from '../../actions/Lightbox.js';
import { CUSTOMIZE_PUBLIC_LIGHTBOX } from '../../shared/LightboxTypes.js';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  checkArea: {
    width: 16,
    height: 16,
    border: `2px solid ${Theme.BUTTON_BACKGROUND_COLOR}`,
    borderRadius: 4,
    backgroundColor: '#fff',
    padding: 2,
    marginRight: 8,
    outline: 0,
    cursor: 'pointer',
  },
  checked: {
    width: '100%',
    height: '100%',
    backgroundColor: Theme.BUTTON_BACKGROUND_COLOR,
  },
  label: {
    fontSize: 13,
    color: Theme.DEFAULT_TEXT_COLOR,
  },
};

type Props = {
  label: String,
  input: {
    value: Boolean,
    onChange: Function,
  },
  meta: {
    error: string,
  },
  style?: Object,
  labelStyle?: Object,
  showReminderWhenChecked?: boolean,
  onClick?: Function,
  dispatch: Function,
};


let isReminderShown = false;

function Checkbox({
  label,
  input: {
    value,
    onChange,
  },
  meta: {
    error,
  },
  onClick,
  showReminderWhenChecked,
  style,
  labelStyle,
  dispatch,
}: Props) {
  return (
    <div style={styles.wrapper}>
      {error ? (
        <span style={{ display: 'block', color: 'rgb(149, 28, 28)', fontSize: 16 }}>
          !
        </span>
      ) : null}
      <button
        type="button"
        onClick={() => {
          if (showReminderWhenChecked) {
            if (!value && !isReminderShown) {
              isReminderShown = true;
              dispatch(openLightbox({
                type: CUSTOMIZE_PUBLIC_LIGHTBOX,
                backgroundClickableDisabled: true,
                onConfirm: () => {
                  onChange(!value);
                  isReminderShown = true;
                },
                onDeny: () => {
                  onChange(false);
                  isReminderShown = false;
                },
              }));
            } else {
              onChange(!value);
            }

            return;
          }

          onChange(!value);

          if (onClick) {
            onClick();
          }
        }}
        style={[
          styles.checkArea,
          style,
        ]}>
        {value ? (
          <div style={styles.checked} />
        ) : null}
      </button>
      <span
        style={[
          styles.label,
          labelStyle,
        ]}>
        {label || null}
      </span>
    </div>
  );
}

Checkbox.defaultProps = {
  style: null,
  labelStyle: null,
  showReminderWhenChecked: false,
  onClick: null,
};

const reduxHook = connect(() => ({}));

export default reduxHook(radium(Checkbox));
