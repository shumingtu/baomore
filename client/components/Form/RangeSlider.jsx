// @flow
import React from 'react';
import radium from 'radium';

import Theme from '../../styles/Theme.js';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '8px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      width: 'auto',
      flex: 1,
    },
  },
  sliderWrapper: {
    flex: 1,
    height: '100%',
    padding: '0 12px',
    position: 'relative',
  },
  valueBlock: {
    width: 60,
    height: 40,
    borderRadius: 8,
    backgroundColor: '#f5f5f5',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
  value: {
    fontSize: 14,
    fontWeight: 600,
    color: Theme.GRAY_TEXT_COLOR,
    textAlign: 'center',
  },
};

type Props = {
  input: {
    value: number,
    onChange: Function,
  },
  min: number,
  max: number,
  step: number,
  type: 'percentage' | 'degree' | 'value',
};

function getUnit(type, value = 0) {
  switch (type) {
    case 'percentage':
      return ({
        value: parseInt(value * 100, 10),
        symbol: '%',
      });
    case 'degree':
      return ({
        value,
        symbol: 'º',
      });
    case 'value':
    default:
      return ({
        value,
        symbol: null,
      });
  }
}

function RangeSlider({
  input: {
    value,
    onChange,
  },
  min,
  max,
  step,
  type = 'value',
}: Props) {
  const wrapValue = getUnit(type, value);

  return (
    <div style={styles.wrapper}>
      <div style={styles.sliderWrapper}>
        <input
          className="range-slider"
          type="range"
          min={min}
          max={max}
          step={step}
          value={value}
          onChange={onChange} />
      </div>
      <div style={styles.valueBlock}>
        <span style={styles.value}>
          {wrapValue && wrapValue.value ? wrapValue.value : 0}
          {(wrapValue && wrapValue.symbol) || null}
        </span>
      </div>
    </div>
  );
}

export default radium(RangeSlider);
