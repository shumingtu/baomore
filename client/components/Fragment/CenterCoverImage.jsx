// @flow
import * as React from 'react';
import radium from 'radium';
// elements
import MainTitle from './Element/MainTitle.jsx';
import Slogan from './Element/Slogan.jsx';
import LinkButton from './Element/LinkButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    position: 'relative',
  },
};

type Props = {
  children: React.Node,
  width: number | string,
  height: number | string,
  mobileWidth: number | string,
  mobileHeight: number | string,
  desktopImg: string,
  mobileImg: string,
  containMode: boolean,
};

function CenterCoverImage({
  width,
  height,
  mobileWidth,
  mobileHeight,
  desktopImg,
  mobileImg,
  containMode,
  children,
}: Props) {
  return (
    <div
      style={[
        styles.wrapper,
        {
          width: width || '100%',
          height: height || '100%',
          backgroundSize: containMode ? 'contain' : 'cover',
          backgroundImage: desktopImg ? `url(${desktopImg})` : null,
          backgroundColor: '#fff',
          '@media (max-width: 767px)': {
            width: mobileWidth || '100%',
            height: mobileHeight || '100%',
            backgroundImage: mobileImg ? `url(${mobileImg})` : null,
          },
        },
      ]}>
      {children || null}
    </div>
  );
}

export default radium(CenterCoverImage);

export function CenterTextBlock({
  fragment,
}: {
  fragment: {
    title: {
      name: string,
      color: string,
    },
    slogan: {
      name: string,
      color: string,
    },
    button?: {
      filledColor: boolean,
      title: string,
      path: string,
    },
  },
}) {
  return (
    <div
      style={{
        width: '100%',
        height: '100%',
        padding: '64px 12px 12px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      {fragment.title ? (
        <MainTitle title={fragment.title} />
      ) : null}
      {fragment.slogan ? (
        <Slogan slogan={fragment.slogan} />
      ) : null}
      {fragment.button ? (
        <LinkButton
          filledColor={fragment.button.filledColor}
          text={fragment.button.title}
          path={fragment.button.path} />
      ) : null}
    </div>
  );
}
