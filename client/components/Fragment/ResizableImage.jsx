// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

const MAX_WIDTH = 'auto';
const DESKTOP_RATIO = 0.40905;
const MOBILE_RATIO = 1.46384;

const styles = {
  placement: {
    width: '100%',
    height: 'auto',
    borderLeft: 0,
    borderRight: 0,
    borderTop: 0,
    borderBottom: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wrapper: {
    width: 'auto',
    maxWidth: MAX_WIDTH,
    height: 'auto',
    backgroundColor: 'transparent',
  },
  image: {
    width: '100%',
    height: 'auto',
    // height: '100%',
  },
};

type Props = {
  maxWidth?: number | null,
  desktopRatio?: number | null,
  mobileRatio?: number | null,
  style: Object,
  desktopImg: string,
  mobileImg: string,
  widthRef: {
    current: null | {
      getBoundingClientRect: Function,
    },
  },
};

type State = {
  windowWidth: number,
  currentWidth: number | string,
};

class ResizableImage extends PureComponent<Props, State> {
  static defaultProps = {
    maxWidth: null,
    desktopRatio: null,
    mobileRatio: null,
  };

  constructor(props) {
    super(props);

    const {
      widthRef,
      maxWidth,
    } = props;

    const width = window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;

    const myMaxWidth = maxWidth || MAX_WIDTH;
    const useParentWidth = (widthRef && widthRef.current) || false;
    const myCurrentWidth = width > myMaxWidth ? myMaxWidth : width;

    this.state = {
      windowWidth: width,
      currentWidth: useParentWidth ? '100%' : myCurrentWidth,
    };

    this.resizeHandler = () => this.resizing();
  }

  static getDerivedStateFromProps({
    widthRef,
  }, state) {
    if (widthRef && widthRef.current) {
      return {
        ...state,
        currentWidth: '100%',
      };
    }

    return state;
  }

  componentDidMount() {
    window.addEventListener('resize', this.resizeHandler);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeHandler);
  }

  resizing() {
    const {
      maxWidth,
      widthRef,
    } = this.props;

    const width = window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;
    const myMaxWidth = maxWidth || MAX_WIDTH;
    const useParentWidth = (widthRef && widthRef.current) || false;

    if (useParentWidth || (myMaxWidth !== 'auto' && width > myMaxWidth)) {
      return this.setState({
        windowWidth: width,
        currentWidth: myMaxWidth,
      });
    }

    return this.setState({
      windowWidth: width,
      currentWidth: width,
    });
  }

  resizeHandler: Function

  render() {
    const {
      widthRef,
      maxWidth,
      style = {},
      desktopImg,
      mobileImg,
      desktopRatio,
      mobileRatio,
    } = this.props;

    const {
      windowWidth,
      currentWidth,
    } = this.state;

    const isMobileMode = windowWidth <= 767;
    const myDesktopRatio = desktopRatio || DESKTOP_RATIO;
    const myMobileRatio = mobileRatio || MOBILE_RATIO;
    const wrapperStyle = {
      maxWidth: maxWidth || MAX_WIDTH,
      width: widthRef && widthRef.current
        ? '100%'
        : windowWidth, // 原本是 currentWidth
      height: widthRef && widthRef.current
        ? '100%'
        : windowWidth * (isMobileMode ? myMobileRatio : myDesktopRatio),
    };

    return (
      <div
        style={[
          styles.placement,
          style || null,
        ]}>
        <img
          alt="圖片"
          src={isMobileMode ? mobileImg : desktopImg}
          style={styles.image} />
        {/* <div
          style={[
            styles.wrapper,
            wrapperStyle,
          ]}>
          <img
            alt="圖片"
            src={isMobileMode ? mobileImg : desktopImg}
            style={styles.image} />
        </div> */}
      </div>
    );
  }
}

export default radium(ResizableImage);
