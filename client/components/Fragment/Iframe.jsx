// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

const MAX_WIDTH = 'auto';
const YT_RATIO = 0.563;

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
  },
};

type Props = {
  src: string,
  playVideoOnInit: boolean,
};

type State = {
  currentWidth: number,
  play: boolean,
};

class Iframe extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    const {
      playVideoOnInit,
    } = props;

    const width = window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;

    const myCurrentWidth = (MAX_WIDTH !== 'auto' && width > MAX_WIDTH) ? MAX_WIDTH : width;

    this.state = {
      currentWidth: myCurrentWidth,
      play: playVideoOnInit || false,
    };

    this.elementRef = React.createRef();
    this.scrollHandler = () => this.scrolling();
    this.resizeHandler = () => this.resizing();
  }

  componentDidMount() {
    window.addEventListener('scroll', this.scrollHandler);
    window.addEventListener('resize', this.resizeHandler);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.scrollHandler);
    window.removeEventListener('resize', this.resizeHandler);
  }

  resizing() {
    const width = window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;

    if (MAX_WIDTH !== 'auto' && width > MAX_WIDTH) {
      return this.setState({
        currentWidth: MAX_WIDTH,
      });
    }

    return this.setState({
      currentWidth: width,
    });
  }

  scrolling() {
    const {
      currentWidth,
      play,
    } = this.state;

    if (this.elementRef.current) {
      const elementOffset = this.elementRef.current.offsetTop || 0;
      const elementHeight = currentWidth * YT_RATIO;
      const scrollY = window.scrollY || 0;
      const visibleHeight = elementHeight < 500 ? 500 : elementHeight;
      const videoVisible = (scrollY > (elementOffset - (visibleHeight / 2)))
        && (scrollY < (elementOffset + visibleHeight));
      /* safari may causing browser crash */
      // if (!play && videoVisible) {
      //   this.setState({
      //     play: true,
      //   });
      // }
      //
      // if (play && !videoVisible) {
      //   this.setState({
      //     play: false,
      //   });
      // }
    }
  }

  resizeHandler: Function

  scrollHandler: Function

  elementRef: { current: null | HTMLDivElement }

  render() {
    const {
      src,
    } = this.props;

    const {
      currentWidth,
      play,
    } = this.state;

    if (!src) return null;

    const source = play ? `${src}?autoplay=1` : src;

    return (
      <div
        ref={this.elementRef}
        style={[
          styles.wrapper,
          {
            width: currentWidth,
            height: currentWidth * YT_RATIO,
          },
        ]}>
        <iframe
          title="Artmo 宣傳影片"
          width={currentWidth}
          height={currentWidth * YT_RATIO}
          src={source}
          frameBorder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen />
      </div>
    );
  }
}

export default radium(Iframe);
