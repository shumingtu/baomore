// @flow
import React from 'react';
import radium from 'radium';

const styles = {
  text: {
    fontSize: 36,
    fontWeight: 700,
    color: '#000',
    letterSpacing: 3,
    textAlign: 'center',
    padding: '4px 0',
    '@media (max-width: 767px)': {
      whiteSpace: 'pre-line',
    },
  },
};

type Props = {
  slogan: {
    name: string,
    color: string,
  },
};

function Slogan({
  slogan,
}: Props) {
  return (
    <span
      style={[
        styles.text,
        {
          color: slogan.color || '#000',
        },
      ]}>
      {slogan.name || null}
    </span>
  );
}

export default radium(Slogan);
