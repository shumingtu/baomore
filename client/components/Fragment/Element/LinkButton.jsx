// @flow
import React from 'react';
import radium from 'radium';
import { Link as l } from 'react-router-dom';

import Theme from '../../../styles/Theme.js';

const Link = radium(l);

const styles = {
  wrapper: {
    width: 'auto',
    height: 'auto',
    margin: '8px 0',
    padding: '4px 16px',
    backgroundColor: 'transparent',
    borderRadius: 16,
    border: `1px solid ${Theme.PURPLE_COLOR}`,
    outline: 0,
    fontSize: 16,
    color: Theme.PURPLE_COLOR,
    textAlign: 'center',
    textDecoration: 'none',
    cursor: 'pointer',
    ':hover': {
      backgroundColor: Theme.PURPLE_COLOR,
      color: '#fff',
    },
  },
  filled: {
    color: '#fff',
    backgroundColor: Theme.PURPLE_COLOR,
    ':hover': {
      opacity: 0.8,
    },
  },
};

type Props = {
  text: string,
  path: string,
  filledColor?: boolean,
};

function LinkButton({
  text,
  path,
  filledColor = false,
}: Props) {
  return (
    <Link
      to={{ pathname: path || '/' }}
      style={[
        styles.wrapper,
        filledColor && styles.filled,
      ]}>
      {text || null}
    </Link>
  );
}

LinkButton.defaultProps = {
  filledColor: false,
};

export default LinkButton;
