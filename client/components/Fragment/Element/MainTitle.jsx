// @flow
import React from 'react';
import radium from 'radium';

const styles = {
  text: {
    fontSize: 22,
    fontWeight: 400,
    color: '#000',
    letterSpacing: 1.6,
    textAlign: 'center',
    whiteSpace: 'pre-line',
    padding: '4px 0',
  },
};

type Props = {
  title: {
    name: string,
    color: string,
  },
};

function MainTitle({
  title,
}: Props) {
  return (
    <span
      style={[
        styles.text,
        {
          color: title.color || '#000',
        },
      ]}>
      {title.name || null}
    </span>
  );
}

export default radium(MainTitle);
