// @flow
import React, { memo } from 'react';
import radium from 'radium';

import Theme from '../../styles/Theme.js';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    borderRadius: 16,
    padding: 12,
    backgroundColor: 'rgba(0, 0, 0, 0.15)',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  title: {
    fontSize: 18,
    fontWeight: 700,
    color: Theme.BLACK_COLOR,
    letterSpacing: 3,
    '@media (max-width: 767px)': {
      fontSize: 16,
    },
  },
};

function BlockTitle({
  title,
}: {
  title: string,
}) {
  return (
    <div style={styles.wrapper}>
      <span style={styles.title}>
        {title || null}
      </span>
    </div>
  );
}

export default memo(radium(BlockTitle));
