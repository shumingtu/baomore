// @flow
import React from 'react';
import radium from 'radium';
// config
import Theme from '../../styles/Theme.js';

const styles = {
  placement: {
    width: 'auto',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    margin: 12,
  },
  wrapper: {
    width: 160,
    height: 160,
    padding: 12,
    borderRadius: 12,
    boxShadow: Theme.CLIENT_BLOCK_SHADOW,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      width: 140,
      height: 140,
    },
  },
  icon: {
    width: 120,
    height: 100,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    '@media (max-width: 767px)': {
      width: 100,
      height: 80,
    },
  },
  label: {
    fontSize: 18,
    fontWeight: 700,
    color: Theme.BLACK_COLOR,
    letterSpacing: 2,
  },
  annotation: {
    fontSize: 12,
    fontWeight: 400,
    letterSpacing: 1,
    padding: '12px 0',
    color: 'rgba(0, 0, 0, 0.6)',
  },
};

type Props = {
  icon: string,
  label: string,
  annotation: string,
};

function StepBlock({
  icon,
  label,
  annotation,
}: Props) {
  return (
    <div style={styles.placement}>
      <div style={styles.wrapper}>
        <div
          style={[
            styles.icon,
            {
              backgroundImage: icon ? `url(${icon})` : null,
            },
          ]} />
        <span style={styles.label}>
          {label || null}
        </span>
      </div>
      <span style={styles.annotation}>
        {annotation || null}
      </span>
    </div>
  );
}

export default radium(StepBlock);
