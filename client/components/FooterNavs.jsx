// @flow
import React from 'react';
import radium from 'radium';
import { Link as link } from 'react-router-dom';
// config
import { BLACK_COLOR } from '../styles/Landing.js';
import Theme from '../styles/Theme.js';

const Link = radium(link);

const styles = {
  wrapper: {
    width: 140,
    height: 100,
    margin: '0 12px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  titleWrapper: {
    width: 120,
    height: 22,
    borderRadius: 3,
    backgroundColor: 'rgba(77, 77, 77, 0.7)',
    padding: '0 6px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  title: {
    fontSize: 12,
    fontWeight: 300,
    color: '#fff',
    letterSpacing: 1,
    cursor: 'default',
  },
  navPlacement: {
    flex: 1,
    width: '100%',
    padding: 4,
  },
  navWrapper: {
    width: '100%',
    height: '100%',
    paddingLeft: 16,
    borderLeft: '1px solid rgba(77, 77, 77, 0.7)',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  nav: {
    fontSize: 12,
    color: BLACK_COLOR,
    letterSpacing: 1.6,
    lineHeight: 1.6,
    textDecoration: 'none',
    border: 0,
    outline: 0,
    cursor: 'pointer',
    ':hover': {
      color: Theme.PURPLE_COLOR,
    },
  },
};

type Props = {
  nav: {
    title: string,
    categories: Array<{
      path: string,
      name: string,
    }>,
  },
};

function FooterNavs({
  nav,
}: Props) {
  return (
    <div style={styles.wrapper}>
      <div style={styles.titleWrapper}>
        <span style={styles.title}>
          {nav.title || null}
        </span>
      </div>
      <div style={styles.navPlacement}>
        <div style={styles.navWrapper}>
          {(nav.categories && nav.categories.map((c) => {
            if (c.link) {
              return (
                <a
                  target="_blank"
                  rel="noreferrer noopener"
                  key={`${c.link}-${c.name}`}
                  href={c.link}
                  style={styles.nav}>
                  {c.name || null}
                </a>
              );
            }

            return (
              <Link
                key={`${c.path}-${c.name}`}
                to={{ pathname: c.path || '/' }}
                style={styles.nav}>
                {c.name || null}
              </Link>
            );
          })) || null}
        </div>
      </div>
    </div>
  );
}

export default radium(FooterNavs);
