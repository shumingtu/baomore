// @flow

import React from 'react';
import radium from 'radium';

import rightArrow from '../../static/images/arrow-right-icon.png';

const styles = {
  naviBtn: {
    width: 12,
    height: 12,
    cursor: 'pointer',
    outline: 'none',
    border: 'none',
    backgroundImage: `url(${rightArrow})`,
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundColor: 'transparent',
  },
};

function NextButton({
  onClick,
  style = {},
}: {
  onClick: Function,
  style: {},
}) {
  return (
    <button
      type="button"
      style={[
        styles.naviBtn,
        style,
      ]}
      onClick={onClick} />
  );
}

export default radium(NextButton);
