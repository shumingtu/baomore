// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
// styles
import {
  BLACK_COLOR,
} from '../../styles/Landing.js';

const styles = {
  placement: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    padding: '24px 0',
    margin: 0,
    position: 'relative',
  },
  imageSlidePlacement: {
    position: 'absolute',
    zIndex: 1,
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    overflow: 'hidden',
  },
  wrapper: {
    width: '100%',
    height: '100%',
    position: 'relative',
    overflow: 'hidden',
  },
  useScrollInMobile: {
    '@media (max-width: 1023px)': {
      overflow: 'auto',
    },
  },
  image: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    transition: 'left 0.2s ease-in-out',
    cursor: 'pointer',
  },
  dotWrapper: {
    flex: 1,
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    zIndex: 2,
  },
  dot: {
    width: 10,
    height: 10,
    padding: 0,
    border: 0,
    outline: 0,
    margin: '0 6px',
    borderRadius: '50%',
    backgroundColor: '#fff',
    cursor: 'pointer',
  },
  activedDot: {
    backgroundColor: BLACK_COLOR,
  },
};

type Props = {
  images: Array<string>,
  clickable: boolean,
  useScrollInMobile: boolean,
};

type State = {
  currentIndex: number,
  currentWidth: number | null,
};

let timer = null;

class ImageSlider extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    const width = window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;

    this.state = {
      windowWidth: width,
      currentIndex: 0,
      currentWidth: 0,
    };

    this.placement = React.createRef();
    this.resizeHandler = () => this.getContainerWidth();
  }

  componentDidMount() {
    this.getContainerWidth();
    this.startAutoplay();

    window.addEventListener('resize', this.resizeHandler);
  }

  componentDidUpdate(prevProps) {
    const {
      images,
    } = this.props;

    if (images !== prevProps.images && images) {
      this.stopAutoplay();
      this.startAutoplay();
    }
  }

  componentWillUnmount() {
    this.stopAutoplay();
    window.removeEventListener('resize', this.resizeHandler);
  }

  getContainerWidth() {
    const {
      currentWidth,
    } = this.state;

    const width = window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;

    const containerWidth = (this.placement.current
    && this.placement.current.clientWidth) || null;

    this.setState({
      windowWidth: width,
    });

    if (currentWidth === containerWidth) return null;

    return this.setState({
      currentWidth: containerWidth,
    });
  }

  startAutoplay() {
    const {
      images,
    } = this.props;

    if (images.length) {
      timer = setInterval(() => {
        const {
          currentIndex,
        } = this.state;

        if (currentIndex >= images.length - 1) {
          return this.setState({
            currentIndex: 0,
          });
        }

        return this.setState({
          currentIndex: currentIndex + 1,
        });
      }, 3000);
    }
  }

  stopAutoplay() {
    if (timer) {
      clearInterval(timer);
    }
  }

  placement: { current: null | HTMLDivElement }

  resizeHandler: Function

  render() {
    const {
      images = [],
      clickable,
      useScrollInMobile,
    } = this.props;

    const {
      currentIndex,
      currentWidth,
      windowWidth,
    } = this.state;

    const isMobileMode = windowWidth <= 767;

    return (
      <div
        ref={this.placement}
        style={[
          styles.placement,
          {
            width: windowWidth,
            height: windowWidth * 0.508,
            '@media (max-width: 767px)': {
              width: windowWidth,
              height: windowWidth * 1.4625,
            },
          },
        ]}>
        <div style={styles.imageSlidePlacement}>
          <div
            style={[
              styles.wrapper,
              useScrollInMobile && styles.useScrollInMobile,
            ]}>
            {images.map((i, idx) => (
              <button
                key={`${i.id}-image`}
                type="button"
                onClick={i.link && clickable ? () => {
                  window.open(i.link, '_blank');
                } : null}
                style={[
                  styles.image,
                  {
                    left: currentWidth
                      ? parseInt(`${(idx - currentIndex) * currentWidth}`, 10)
                      : (idx - currentIndex) * 1280, // push image far away
                    backgroundImage: i.mobileImg && isMobileMode ? `url(${i.mobileImg})` : `url(${i.picture})`,
                    cursor: clickable && i.link ? 'pointer' : 'default',
                  },
                ]} />
            ))}
          </div>
        </div>
        <div style={styles.dotWrapper}>
          {images.map((i, idx) => (
            <button
              key={`${i.id}-dot`}
              type="button"
              onClick={currentIndex !== idx
                ? () => {
                  this.stopAutoplay();
                  this.setState({ currentIndex: idx });
                  this.startAutoplay();
                } : null}
              style={[
                styles.dot,
                currentIndex === idx && styles.activedDot,
                useScrollInMobile && {
                  '@media (max-width: 1023px)': {
                    display: 'none',
                  },
                },
              ]} />
          ))}
        </div>
      </div>
    );
  }
}

export default radium(
  ImageSlider
);
