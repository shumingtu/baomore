import React from 'react';
import radium from 'radium';

import fbIcon from '../../static/images/artmo-icon-fb.png';
import igIcon from '../../static/images/artmo-icon-ig.png';
import lineIcon from '../../static/images/artmo-icon-line@.png';
import youtubeIcon from '../../static/images/artmo-icon-youtube.png';

const styles = {
  floatingButtonWrap: {
    position: 'fixed',
    zIndex: 999,
    right: 20,
    top: 80,
    width: 'auto',
    height: 'auto',
    padding: 6.7,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    backgroundColor: 'white',
    boxShadow: '4px 3px 10px #5f5858',
  },
  itemWrap: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    cursor: 'pointer',
    margin: '2px 0',
    textDecoration: 'none',
  },
  itemIcon: {
    width: 45,
    height: 45,
    '@media (max-width: 767px)': {
      width: 31.5,
      height: 31.5,
    },
  },
  itemText: {
    fontSize: 12,
    color: '#757474',
    letterSpacing: 2,
  },
};

function FloatingButtons() {
  return (
    <div style={styles.floatingButtonWrap}>
      <a
        href="https://line.me/R/ti/p/%40gwk6718t"
        target="_blank"
        rel="noopener noreferrer"
        style={styles.itemWrap}>
        <img alt="LINE" src={lineIcon} style={styles.itemIcon} />
        {/* <span style={styles.itemText}>LINE@</span> */}
      </a>
      <a
        href="https://www.facebook.com/artmoinc/"
        target="_blank"
        rel="noopener noreferrer"
        style={styles.itemWrap}>
        <img alt="FB" src={fbIcon} style={styles.itemIcon} />
        {/* <span style={styles.itemText}>FB</span> */}
      </a>
      <a
        href="https://www.instagram.com/artmoinc/"
        target="_blank"
        rel="noopener noreferrer"
        style={styles.itemWrap}>
        <img alt="IG" src={igIcon} style={styles.itemIcon} />
        {/* <span style={styles.itemText}>IG</span> */}
      </a>
      <a
        href="https://www.youtube.com/channel/UC8ygda_uIC5qpfz_K2XVNRw"
        target="_blank"
        rel="noopener noreferrer"
        style={styles.itemWrap}>
        <img alt="YOUTUBE" src={youtubeIcon} style={styles.itemIcon} />
        {/* <span style={styles.itemText}>Youtube</span> */}
      </a>
    </div>
  );
}

export default radium(FloatingButtons);
