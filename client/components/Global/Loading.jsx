import React from 'react';

import loadingGIF from '../../static/images/loading-spinner.gif';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loading: {
    width: 32,
    height: 32,
  },
};

function Loading() {
  return (
    <div style={styles.wrapper}>
      <img
        alt="讀取中"
        src={loadingGIF}
        style={styles.loading} />
    </div>
  );
}

export default Loading;
