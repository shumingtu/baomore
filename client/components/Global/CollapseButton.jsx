// @flow
import React from 'react';
import radium from 'radium';
// static
import collapseIcon from '../../static/images/collapse-icon.png';
import expandIcon from '../../static/images/expand-icon.png';

const styles = {
  wrapper: {
    width: 16,
    height: 16,
    outline: 0,
    border: 0,
    backgroundColor: 'transparent',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    cursor: 'pointer',
  },
  collapse: {
    backgroundImage: `url(${collapseIcon})`,
  },
  expand: {
    backgroundImage: `url(${expandIcon})`,
  },
};

function CollapseButton({
  isCollapse,
  onClick,
}: {
  isCollapse: boolean,
  onClick: Function,
}) {
  return (
    <button
      type="button"
      onClick={onClick}
      style={[
        styles.wrapper,
        isCollapse ? styles.expand : styles.collapse,
      ]} />
  );
}

export default radium(CollapseButton);
