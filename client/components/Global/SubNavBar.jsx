// @flow
import * as React from 'react';
import radium from 'radium';
import {
  NavLink as l,
  Link,
  withRouter,
} from 'react-router-dom';
import {
  reduxForm,
} from 'redux-form';
// config
import {
  SUBNAV_SELECTION_FORM,
} from '../../shared/Form.js';
import Theme from '../../styles/Theme.js';
import downArrow from '../../static/images/down-arrow.png';

const NavLink = radium(l);

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '24px 18px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    position: 'relative',
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
  },
  desktopPlacement: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
  desktopWrapper: {
    width: '100%',
    height: 'auto',
    maxWidth: 1024,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  titleLink: {
    width: 'auto',
    height: 'auto',
    padding: '0 4px',
    fontSize: 18,
    fontWeight: 700,
    color: '#000',
    letterSpacing: 2,
    textDecoration: 'none',
    border: 0,
    backgroundColor: 'transparent',
    cursor: 'pointer',
  },
  disabledLink: {
    cursor: 'default',
  },
  navsWrapper: {
    flex: 1,
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  nav: {
    width: 'auto',
    height: 'auto',
    padding: '0 4px',
    fontSize: 14,
    color: '#000',
    fontWeight: 700,
    letterSpacing: 1.1,
    textDecoration: 'none',
    border: 0,
    backgroundColor: 'transparent',
    margin: '0 12px 0 0',
    cursor: 'pointer',
  },
  activeNav: {
    color: Theme.PURPLE_COLOR,
  },
  mobilePlacement: {
    display: 'none',
    '@media (max-width: 767px)': {
      width: '100%',
      height: 'auto',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-start',
    },
  },
  selectionBtnPlacement: {
    flex: 1,
    height: '100%',
  },
  selectionBtn: {
    width: 'calc(50% + 6px)', // 220px
    height: '100%',
    padding: 0,
    margin: 0,
    border: 0,
    outline: 0,
    backgroundColor: 'transparent',
    cursor: 'pointer',
  },
  withHookBtn: {
    width: 'calc(50% + 38px)',
  },
  currentSelectionWrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  selectedLabel: {
    fontSize: 18,
    fontWeight: 700,
    color: '#000',
    letterSpacing: 2,
  },
  selectedArrow: {
    width: 16,
    height: 16,
    backgroundImage: `url(${downArrow})`,
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    transition: 'transform 0.1s ease-in',
  },
  mobileExtendNav: {
    position: 'absolute',
    zIndex: 99997,
    left: 0,
    top: 73,
    width: '100%',
    height: '100vh',
    backgroundColor: '#fff',
  },
  mobileNavWrapper: {
    width: '100%',
    height: 'auto',
    padding: '24px 12px',
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
  },
  mobileNav: {
    width: '100%',
    height: 'auto',
    padding: '8px 0',
    textAlign: 'left',
    fontSize: 16,
    color: Theme.GRAY_COLOR,
    letterSpacing: 1,
    backgroundColor: 'transparent',
    borderTop: 0,
    borderLeft: 0,
    borderRight: 0,
    borderBottom: '1px solid rgba(0, 0, 0, 0.15)',
    cursor: 'pointer',
  },
  noBorder: {
    borderBottom: 0,
  },
  hookWrapper: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 8px',
  },
};

type Props = {
  title: {
    path: string,
    name: string,
  },
  navs: Array<{
    id: string | number,
    path: string,
    name: string,
  }>,
  hookButton?: null | React.Node,
  history: {
    push: Function,
  },
  location: {
    pathname: string,
  },
  disabledTitleLink?: boolean,
};

type State = {
  showMobileExtend: boolean,
};

class SubNavBar extends React.PureComponent<Props, State> {
  static defaultProps = {
    hookButton: null,
    disabledTitleLink: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      showMobileExtend: false,
    };
  }

  findCurrentPathname(navs) {
    const {
      location: {
        pathname,
      },
    } = this.props;

    const currentNav = navs.find(n => n.path === pathname) || null;

    if (currentNav) return currentNav.name;

    const splitPath = pathname.split('/').filter(p => p) || [];

    if (splitPath.length > 0) {
      const parentPath = [
        ...splitPath.slice(0, splitPath.length - 1),
      ].join('/');

      const parentNav = navs.find(n => n.path === `/${parentPath}`) || null;

      if (parentNav) return parentNav.name;
    }

    return null;
  }

  render() {
    const {
      title,
      navs = [],
      history,
      hookButton,
      disabledTitleLink,
    } = this.props;

    const {
      showMobileExtend,
    } = this.state;

    const navsWithLanding = !disabledTitleLink ? [{
      id: 'landing',
      path: (title && title.path) || '/',
      name: (title && title.name) || null,
    }].concat(navs) : navs;

    return (
      <div style={styles.wrapper}>
        <div style={styles.desktopPlacement}>
          <div style={styles.desktopWrapper}>
            {!disabledTitleLink ? (
              <Link
                to={{ pathname: (title && title.path) || null }}
                style={styles.titleLink}>
                {(title && title.name) || null}
              </Link>
            ) : (
              <span style={[styles.titleLink, styles.disabledLink]}>
                {(title && title.name) || null}
              </span>
            )}
            <div style={styles.navsWrapper}>
              {Array.isArray(navs) ? navs.map(nav => (
                <NavLink
                  key={nav.id}
                  className="category-nav-link"
                  to={{ pathname: nav.path || null }}
                  style={styles.nav}
                  activeStyle={styles.activeNav}>
                  {nav.name || null}
                </NavLink>
              )) : null}
            </div>
            {hookButton ? (
              <div style={styles.hookWrapper}>
                {hookButton}
              </div>
            ) : null}
          </div>
        </div>
        <div style={styles.mobilePlacement}>
          <div style={styles.selectionBtnPlacement}>
            <button
              type="button"
              onClick={() => this.setState({ showMobileExtend: !showMobileExtend })}
              style={[
                styles.selectionBtn,
                hookButton && styles.withHookBtn,
              ]}>
              <div style={styles.currentSelectionWrapper}>
                <span style={styles.selectedLabel}>
                  {this.findCurrentPathname(navsWithLanding)}
                </span>
                <div
                  style={[
                    styles.selectedArrow,
                    showMobileExtend && {
                      transform: 'rotate(180deg)',
                    },
                  ]} />
              </div>
            </button>
          </div>
          {hookButton ? (
            <div style={styles.hookWrapper}>
              {hookButton}
            </div>
          ) : null}
          {showMobileExtend ? (
            <div style={styles.mobileExtendNav}>
              <div style={styles.mobileNavWrapper}>
                {Array.isArray(navsWithLanding) ? navsWithLanding.map((nav, idx) => (
                  <button
                    key={nav.id}
                    type="button"
                    onClick={() => {
                      history.push(nav.path);
                      this.setState({
                        showMobileExtend: false,
                      });
                    }}
                    style={[
                      styles.mobileNav,
                      idx === (navsWithLanding.length - 1) && styles.noBorder,
                    ]}>
                    {nav.name || null}
                  </button>
                )) : null}
              </div>
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: SUBNAV_SELECTION_FORM,
});

export default withRouter(
  formHook(
    radium(
      SubNavBar
    )
  )
);
