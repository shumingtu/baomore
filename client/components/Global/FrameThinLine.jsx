// @flow
import React from 'react';
import radium from 'radium';

import {
  YELLOW_COLOR,
} from '../../styles/Landing.js';

const styles = {
  heavyLine: {
    position: 'absolute',
    zIndex: 3,
    width: 768,
    height: 1,
    backgroundColor: YELLOW_COLOR,
    '@media (max-width: 767px)': {
      width: '100%',
    },
  },
};

type Props = {
  color: string,
  type: 'leftTop' | 'rightBottom',
};

function FrameThinLine({
  color,
  type = 'leftTop',
}: Props) {
  return (
    <div
      style={[
        styles.heavyLine,
        { backgroundColor: color },
        type === 'leftTop' && {
          left: 0,
          top: 0,
        },
        type === 'rightBottom' && {
          right: 0,
          bottom: 0,
        },
      ]} />
  );
}

export default radium(FrameThinLine);
