// @flow
import React from 'react';
import radium from 'radium';

import Theme from '../../styles/Theme.js';
import arrowLeftIcon from '../../static/images/arrow-left-icon.png';
import arrowRightIcon from '../../static/images/arrow-right-icon.png';

const styles = {
  wrapper: {
    width: 32,
    height: 32,
    boxShadow: Theme.BLOCK_SHADOW,
    borderRadius: 8,
    backgroundColor: '#fff',
    outline: 0,
    border: 0,
    cursor: 'pointer',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    ':hover': {
      opacity: 0.9,
    },
  },
  arrow: {
    width: 6,
    height: 10,
  },
};

type Props = {
  type: 'left' | 'right',
  onClick: Function,
  style: Object,
};

function ArrowButton({
  type,
  onClick,
  style = {},
}: Props) {
  return (
    <button
      type="button"
      onClick={onClick}
      style={[styles.wrapper, style]}>
      <img
        alt="箭頭"
        src={type === 'left' ? arrowLeftIcon : arrowRightIcon}
        style={styles.arrow} />
    </button>
  );
}

export default radium(ArrowButton);
