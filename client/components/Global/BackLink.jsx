// @flow
import React from 'react';
import radium from 'radium';
import {
  Link as link,
} from 'react-router-dom';

// config
import Theme from '../../styles/Theme.js';
// static
import leftArrow from '../../static/images/arrow-left-icon.png';

const Link = radium(link);

const styles = {
  wrapper: {
    width: 'auto',
    height: 'auto',
    paddingRight: 12,
    textDecoration: 'none',
    outline: 0,
    border: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  arrow: {
    width: 8,
    height: 12,
    marginRight: 6,
    backgroundImage: `url(${leftArrow})`,
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
  text: {
    fontSize: 14,
    color: Theme.DEFAULT_TEXT_COLOR,
    letterSpacing: 1,
  },
};

type Props = {
  label: string,
  path: string,
};

function BackLink({
  label,
  path,
}: Props) {
  return (
    <Link
      to={{ pathname: path }}
      style={styles.wrapper}>
      <div style={styles.arrow} />
      <span style={styles.text}>
        {label || null}
      </span>
    </Link>
  );
}

export default radium(BackLink);
