// @flow
/* eslint react/button-has-type: 0 */
import React from 'react';
import radium from 'radium';

// config
import Theme from '../../styles/Theme.js';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    borderRadius: 8,
    backgroundColor: Theme.BUTTON_BACKGROUND_COLOR,
    textAlign: 'center',
    outline: 0,
    border: 0,
    fontSize: 14,
    fontWeight: 600,
    color: '#fff',
    letterSpacing: 1.4,
    cursor: 'pointer',
    ':hover': {
      opacity: 0.9,
    },
  },
  withShadow: {
    boxShadow: Theme.BUTTON_SHADOW,
  },
  hollow: {
    border: `1px solid ${Theme.BUTTON_BACKGROUND_COLOR}`,
    backgroundColor: 'transparent',
    color: Theme.BUTTON_BACKGROUND_COLOR,
  },
  disabled: {
    opacity: 0.3,
    pointerEvents: 'none',
  },
};

type Props = {
  type: string,
  withShadow: boolean,
  hollow: boolean,
  label: string,
  onClick: Function,
  disabled: boolean,
};

function Button({
  withShadow,
  hollow,
  label,
  type,
  onClick,
  disabled = false,
}: Props) {
  return (
    <button
      type={type || 'button'}
      onClick={onClick || null}
      disabled={disabled}
      style={[
        styles.wrapper,
        hollow && styles.hollow,
        withShadow && styles.withShadow,
        disabled && styles.disabled,
      ]}>
      {label || null}
    </button>
  );
}

export default radium(Button);
