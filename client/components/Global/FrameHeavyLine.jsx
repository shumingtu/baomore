// @flow
import React, { Fragment } from 'react';
import radium from 'radium';

import {
  YELLOW_COLOR,
} from '../../styles/Landing.js';

const styles = {
  heavyLine: {
    position: 'absolute',
    zIndex: 3,
    width: 250,
    height: 10,
    backgroundColor: YELLOW_COLOR,
    '@media (max-width: 1023px)': {
      width: 200,
    },
  },
  triangle: {
    position: 'absolute',
    zIndex: 3,
    width: 20,
    height: 20,
    transform: 'rotate(45deg)',
    backgroundColor: YELLOW_COLOR,
  },
};

type Props = {
  color: string,
  type: 'leftTop' | 'rightBottom',
};

function FrameHeavyLine({
  color,
  type = 'leftTop',
}: Props) {
  return (
    <Fragment>
      <div
        style={[
          styles.heavyLine,
          { backgroundColor: color },
          type === 'leftTop' && {
            left: -15,
            top: 0,
          },
          type === 'rightBottom' && {
            right: -15,
            bottom: 0,
          },
        ]} />
      <div
        style={[
          styles.triangle,
          { backgroundColor: color },
          type === 'leftTop' && {
            left: 225,
            top: -14,
            '@media (max-width: 1023px)': {
              left: 175,
            },
          },
          type === 'rightBottom' && {
            right: 225,
            bottom: -14,
            '@media (max-width: 1023px)': {
              right: 175,
            },
          },
        ]} />
    </Fragment>
  );
}

export default radium(FrameHeavyLine);
