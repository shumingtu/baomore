// @flow
import React from 'react';
import radium from 'radium';
// config
import Theme from '../../styles/Theme.js';

const styles = {
  titleWrapper: {
    width: '100%',
    height: 'auto',
    padding: '36px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
    },
  },
  title: {
    fontSize: 32,
    fontWeight: 700,
    color: Theme.BLACK_COLOR,
    letterSpacing: 3,
    textAlign: 'center',
    '@media (max-width: 767px)': {
      fontSize: 26,
    },
  },
};

type Props = {
  title: string | Array<string>,
};

function PageTitle({
  title,
}: Props) {
  if (!title) return null;

  const wrapTitle = Array.isArray(title) ? title : [title];

  return (
    <div style={styles.titleWrapper}>
      {wrapTitle.map(s => (
        <span key={s} style={styles.title}>
          {s}
        </span>
      ))}
    </div>
  );
}

export default radium(PageTitle);
