// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import {
  Link as link,
} from 'react-router-dom';

const Link = radium(link);
const MAX_WIDTH = 1280;
const DESKTOP_RATIO = 0.40905;
const MOBILE_RATIO = 1.46384;

const styles = {
  placement: {
    width: '100%',
    height: 'auto',
    borderLeft: 0,
    borderRight: 0,
    borderTop: 0,
    borderBottom: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wrapper: {
    width: 'auto',
    maxWidth: MAX_WIDTH,
    height: 'auto',
    backgroundColor: 'transparent',
    textDecoration: 'none',
    border: 0,
    outline: 0,
    cursor: 'pointer',
  },
  image: {
    width: '100%',
    height: '100%',
  },
};

type Props = {
  path: string,
  useURL?: boolean,
  maxWidth?: number | null,
  desktopRatio?: number | null,
  mobileRatio?: number | null,
  style: Object,
  desktopImg: string,
  mobileImg: string,
  widthRef: {
    current: null | {
      getBoundingClientRect: Function,
    },
  },
};

type State = {
  windowWidth: number,
  currentWidth: number | string,
};

class LinkImage extends PureComponent<Props, State> {
  static defaultProps = {
    maxWidth: null,
    desktopRatio: null,
    mobileRatio: null,
    useURL: false,
  };

  constructor(props) {
    super(props);

    const {
      widthRef,
      maxWidth,
    } = props;

    const width = window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;

    const myMaxWidth = maxWidth || MAX_WIDTH;
    const useParentWidth = (widthRef && widthRef.current) || false;
    const myCurrentWidth = width > myMaxWidth ? myMaxWidth : width;

    this.state = {
      windowWidth: width,
      currentWidth: useParentWidth ? '100%' : myCurrentWidth,
    };

    this.resizeHandler = () => this.resizing();
  }

  static getDerivedStateFromProps({
    widthRef,
  }, state) {
    if (widthRef && widthRef.current) {
      return {
        ...state,
        currentWidth: '100%',
      };
    }

    return state;
  }

  componentDidMount() {
    window.addEventListener('resize', this.resizeHandler);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeHandler);
  }

  resizing() {
    const {
      maxWidth,
      widthRef,
    } = this.props;

    const width = window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;
    const myMaxWidth = maxWidth || MAX_WIDTH;
    const useParentWidth = (widthRef && widthRef.current) || false;

    if (useParentWidth || width > myMaxWidth) {
      return this.setState({
        windowWidth: width,
        currentWidth: myMaxWidth,
      });
    }

    return this.setState({
      windowWidth: width,
      currentWidth: width,
    });
  }

  resizeHandler: Function

  render() {
    const {
      widthRef,
      useURL,
      path,
      maxWidth,
      style = {},
      desktopImg,
      mobileImg,
      desktopRatio,
      mobileRatio,
    } = this.props;

    const {
      windowWidth,
      currentWidth,
    } = this.state;

    const isMobileMode = windowWidth <= 767;
    const myDesktopRatio = desktopRatio || DESKTOP_RATIO;
    const myMobileRatio = mobileRatio || MOBILE_RATIO;
    const linkStyle = {
      maxWidth: maxWidth || MAX_WIDTH,
      width: widthRef && widthRef.current
        ? '100%'
        : currentWidth,
      height: widthRef && widthRef.current
        ? '100%'
        : currentWidth * (isMobileMode ? myMobileRatio : myDesktopRatio),
    };

    return (
      <div
        style={[
          styles.placement,
          style || null,
        ]}>
        {useURL ? (
          <a
            rel="noopener noreferrer"
            href={path}
            target="_blank"
            style={[
              styles.wrapper,
              linkStyle,
            ]}>
            <img
              alt="圖片"
              src={isMobileMode ? mobileImg : desktopImg}
              style={styles.image} />
          </a>
        ) : (
          <Link
            to={{ pathname: path }}
            style={[
              styles.wrapper,
              linkStyle,
            ]}>
            <img
              alt="圖片"
              src={isMobileMode ? mobileImg : desktopImg}
              style={styles.image} />
          </Link>
        )}
      </div>
    );
  }
}

export default radium(LinkImage);
