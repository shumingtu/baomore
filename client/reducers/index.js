// @flow

import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { routerReducer } from 'react-router-redux';
// reducers
import Customize from './Customize.js';
import Lightbox from './Lightbox.js';
import Member from './Member.js';
import Global from './Global.js';

export default combineReducers({
  Customize,
  Lightbox,
  Member,
  Global,
  form: formReducer,
  routing: routerReducer,
});
