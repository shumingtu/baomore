import {
  CACHE_PHONE_SELECTION,
  CACHE_CUSTOMIZE_CATEGORY,
  CACHE_CURRENT_SELECTED_IMAGE,
  CACHE_CURRENT_SELECTED_TEXT,
  CACHE_CURRENT_SELECTED_EMOJI,
  CACHE_CURRENT_SELECTED_FIXED_IMAGE,
  CLEAR_ALL_SELECTED_CACHE,
  CACHE_CUSTOMIZE_MAIN_CATEGORIES,
  CACHE_CUSTOMIZE_URL,
  CACHE_CUSTOMIZE_CANVAS_CLIP_SIZE,
  CACHE_CUSTOMIZE_CATEGORY_NAME,
  CACHE_CUSTOMIZE_PNTABLE_INFO,
} from '../actions/Customize.js';
import {
  CANVAS_WIDTH,
  CANVAS_HEIGHT,
  MOBILE_CANVAS_WIDTH,
  MOBILE_CANVAS_HEIGHT,
} from '../shared/PhoneConfig.js';
import { isMobileMode } from '../helper/canvasHelper.js';

export default (state = {
  phoneSelection: null,
  customizeCategory: null,
  customizeCategoryName: null,
  currentSelectedImageId: null,
  currentSelectedTextId: null,
  currentSelectedEmojiId: null,
  currentSelectedFixedImageId: null,
  currentCustomizeUrl: '',
  mainCategories: [],
  customizePNTableInfo: null,
  clipBundle: {
    x: 0,
    y: 0,
    width: isMobileMode ? MOBILE_CANVAS_WIDTH : CANVAS_WIDTH,
    height: isMobileMode ? MOBILE_CANVAS_HEIGHT : CANVAS_HEIGHT,
  },
}, action) => {
  switch (action.type) {
    case CACHE_PHONE_SELECTION:
      return {
        ...state,
        phoneSelection: action.payload,
      };

    case CACHE_CUSTOMIZE_CATEGORY_NAME:
      return {
        ...state,
        customizeCategoryName: action.payload,
      };

    case CACHE_CUSTOMIZE_CATEGORY:
      return {
        ...state,
        customizeCategory: action.payload,
      };

    case CACHE_CURRENT_SELECTED_IMAGE:
      return {
        ...state,
        currentSelectedImageId: action.imageId,
      };

    case CACHE_CURRENT_SELECTED_TEXT:
      return {
        ...state,
        currentSelectedTextId: action.textId,
      };

    case CACHE_CURRENT_SELECTED_EMOJI:
      return {
        ...state,
        currentSelectedEmojiId: action.emojiId,
      };

    case CACHE_CURRENT_SELECTED_FIXED_IMAGE:
      return {
        ...state,
        currentSelectedFixedImageId: action.imageId,
      };

    case CLEAR_ALL_SELECTED_CACHE:
      return {
        ...state,
        currentSelectedImageId: null,
        currentSelectedTextId: null,
        currentSelectedEmojiId: null,
        currentSelectedFixedImageId: null,
        clipBundle: {
          x: 0,
          y: 0,
          width: isMobileMode ? MOBILE_CANVAS_WIDTH : CANVAS_WIDTH,
          height: isMobileMode ? MOBILE_CANVAS_HEIGHT : CANVAS_HEIGHT,
        },
      };

    case CACHE_CUSTOMIZE_MAIN_CATEGORIES:
      return {
        ...state,
        mainCategories: action.list,
      };

    case CACHE_CUSTOMIZE_URL:
      return {
        ...state,
        currentCustomizeUrl: action.url || '',
      };

    case CACHE_CUSTOMIZE_CANVAS_CLIP_SIZE:
      return {
        ...state,
        clipBundle: {
          x: (action.image && action.image.x) || state.clipBundle.x,
          y: (action.image && action.image.y) || state.clipBundle.y,
          width: (action.image && action.image.width) || state.clipBundle.width,
          height: (action.image && action.image.height) || state.clipBundle.height,
        },
      };

    case CACHE_CUSTOMIZE_PNTABLE_INFO:
      return {
        ...state,
        customizePNTableInfo: action.info,
      };

    default:
      return state;
  }
};
