import {
  OPEN_LIGHTBOX,
  CLOSE_LIGHTBOX,
  OPEN_STATUS_BOX,
  CLOSE_STATUS_BOX,
} from '../actions/Lightbox.js';

export default (state = {
  currentLightbox: null,
  currentStatusBox: null,
}, action) => {
  switch (action.type) {
    case OPEN_LIGHTBOX:
      return {
        ...state,
        currentLightbox: action.lightbox,
      };

    case CLOSE_LIGHTBOX:
      return {
        ...state,
        currentLightbox: null,
      };

    case OPEN_STATUS_BOX:
      return {
        ...state,
        currentStatusBox: action.status,
      };

    case CLOSE_STATUS_BOX:
      return {
        ...state,
        currentStatusBox: null,
      };

    default:
      return state;
  }
};
