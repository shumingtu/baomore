import {
  CACHE_MEMBER_INFO,
  CACHE_MEMBER_TOKEN,
  CACHE_MEMBER_CUSTOMIZATION,
  CLEAR_MEMBER_INFO,
} from '../actions/Member.js';

export default (state = {
  memberId: null,
  memberName: null,
  memberAvatar: null,
  memberEmail: null,
  memberPhone: null,
  memberGender: null,
  memberBirthday: null,
  memberStoredJSON: localStorage.getItem('customizeJson') || null,
  accessToken: localStorage.getItem('accessToken') || null,
}, action) => {
  switch (action.type) {
    case CACHE_MEMBER_INFO:
      return {
        ...state,
        memberId: action.member.id || state.memberId || null,
        memberName: action.member.name || state.memberName || null,
        memberAvatar: action.member.avatar || state.memberAvatar || null,
        memberEmail: action.member.email || state.memberEmail || null,
        memberPhone: action.member.phone || state.memberPhone || null,
        memberGender: action.member.gender || state.memberGender || null,
        memberBirthday: action.member.birthday || state.memberBirthday || null,
        memberStoredJSON: action.member.storedDesignJSON || localStorage.getItem('customizeJson') || null,
        accessToken: action.member.accessToken || localStorage.getItem('accessToken') || null,
      };

    case CACHE_MEMBER_TOKEN: {
      if (action.token) {
        localStorage.setItem('accessToken', action.token);
      }

      return {
        ...state,
        accessToken: action.token || null,
      };
    }

    case CACHE_MEMBER_CUSTOMIZATION: {
      if (action.json) {
        localStorage.setItem('customizeJson', action.json);
      }

      return {
        ...state,
        memberStoredJSON: action.json || localStorage.getItem('customizeJson') || null,
      };
    }

    case CLEAR_MEMBER_INFO: {
      localStorage.removeItem('accessToken');

      return {
        ...state,
        memberId: null,
        memberName: null,
        memberAvatar: null,
        memberEmail: null,
        memberPhone: null,
        memberGender: null,
        memberBirthday: null,
        accessToken: null,
        memberStoredJSON: localStorage.getItem('customizeJson') || null,
      };
    }

    default:
      return state;
  }
};
