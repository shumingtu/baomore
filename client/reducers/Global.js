import {
  OPEN_MOBILE_EXTEND_NAV,
  CLOSE_MOBILE_EXTEND_NAV,
} from '../actions/Global.js';

export default (state = {
  extendNavIsOpened: false,
}, action) => {
  switch (action.type) {
    case OPEN_MOBILE_EXTEND_NAV:
      return {
        ...state,
        extendNavIsOpened: true,
      };

    case CLOSE_MOBILE_EXTEND_NAV:
      return {
        ...state,
        extendNavIsOpened: false,
      };

    default:
      return state;
  }
};
