// spec
export const IMAGE_WIDTH_SPEC = 1418; // 1418
export const IMAGE_HEIGHT_SPEC = 2362; // 2362
const RATIO = IMAGE_HEIGHT_SPEC / IMAGE_WIDTH_SPEC;
// edit mode
export const CANVAS_WIDTH = 300;
export const CANVAS_HEIGHT = Math.ceil(CANVAS_WIDTH * RATIO); // 300 x ratio
export const CANVAS_MAX_WIDTH = 400;
export const MOBILE_CANVAS_WIDTH = 200;
export const MOBILE_CANVAS_HEIGHT = Math.ceil(MOBILE_CANVAS_WIDTH * RATIO); // 200 x ratio
export const CANVAS_DEFAULT_TEXT_COLOR = '#000';
export const CANVAS_TEXT_STYLE = 'sans-serif';
// thumbnail mode
export const CANVAS_THUMBNAIL_WIDTH = 150;
export const CANVAS_THUMBNAIL_HEIGHT = CANVAS_THUMBNAIL_WIDTH * RATIO; // 150 x ratio
// ratio
export const CANVAS_EDIT_MODE_RATIO = CANVAS_WIDTH / IMAGE_WIDTH_SPEC;
export const CANVAS_THUMBNAIL_MODE_RATIO = CANVAS_THUMBNAIL_WIDTH / IMAGE_WIDTH_SPEC;
