export const PHONE_SELECTION_FORM = 'FORM/PHONE_SELECTION_FORM';
export const PHONE_CUSTOMIZE_FORM = 'FORM/PHONE_CUSTOMIZE_FORM';
export const PHONE_TEXT_ADDER_TEMP_FORM = 'FORM/PHONE_TEXT_ADDER_TEMP_FORM';
export const PHONE_EMOJI_TEMP_FORM = 'FORM/PHONE_EMOJI_TEMP_FORM';
export const PHONE_FIXED_IMAGE_TEMP_FORM = 'FORM/PHONE_FIXED_IMAGE_TEMP_FORM';
// website form
export const SUBNAV_SELECTION_FORM = 'FORM/SUBNAV_SELECTION_FORM';
export const CONTACT_MAP_FORM = 'FORM/CONTACT_MAP_FORM';
export const MEMBER_EDIT_FORM = 'FORM/MEMBER_EDIT_FORM';
export const WARRANTY_CREATE_FORM = 'FORM/WARRANTY_CREATE_FORM';

export default null;
