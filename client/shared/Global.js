// static
import photoIcon from '../static/images/photo-icon.png';
import textIcon from '../static/images/text-icon.png';
import emojiIcon from '../static/images/emoji-icon.png';
// some variables
export const LINE_PAY_BASE_LINK = `${API_HOST}/line/pay/reserve`;
export const LINE_FRIEND_ID = '%40gwk6718t'; // @ = %40
export const LINE_LOGIN_PATH = `https://access.line.me/oauth2/v2.1/authorize?response_type=code&client_id=1599855723&redirect_uri=${API_HOST}/auth/lineLogin&state=test&scope=openid%20profile`; // localhost:2113
export const LINE_LOGIN_CUSTOMIZE_PATH = `https://access.line.me/oauth2/v2.1/authorize?response_type=code&client_id=1599855723&redirect_uri=${API_HOST}/auth/customize/lineLogin&state=test&scope=openid%20profile`;
// customize text stroke types
export const NONE_BORDER = 'none';
export const SOLID_BORDER = 'stroke';
export const SHADOW_BORDER = 'shadow';
// customize type spec
export const TYPES_SPEC = [{
  type: 'photo',
  icon: photoIcon,
  label: '新增照片',
}, {
  type: 'text',
  icon: textIcon,
  label: '新增文字',
}, {
  type: 'emoji',
  icon: emojiIcon,
  label: '新增表情',
}];
// customize detail mock desc
export const CUSTOMIZE_DEFAULT_DESCRIPTION = '圖面尺寸：143.6 x 70.9 mm\n製作方式：3D熱轉印\n材質：亮殼\n\n自由打造獨特個人風格客製化手機殼、最具意義的紀念品讓精彩感動、隨時在你身邊';
export const CUSTOMIZE_DEFAULT_PRICE = '$1680';
// order status spec
export const PAYED_STATUS_SPEC = {
  UNPAID: '未付款',
  PAIDNOTCONFIRMED: '已付款(處理中)',
  PAIDANDCONFIRMED: '已付款',
  CANCELED: '已取消',
  ROLLBACKED: '已退款',
};
export const SHIPPING_STATUS_SPEC = {
  UNDISPATCHED: '未出貨',
  DISPATCHED: '已出貨',
  DISPATCHEDANDCHECKED: '已到貨',
  SOLDED: '已售出',
};
// warranty service spec
export const WARRANTY_SERVICE_LIST = [{
  id: '包膜+保護貼',
  name: '包膜+保護貼',
}, {
  id: '單背面包膜',
  name: '單背面包膜',
}, {
  id: '單貼保貼/諮詢包膜商品',
  name: '單貼保貼/諮詢包膜商品',
}];
// site header NAVS

/* v1 隱藏項目 */
const accessoriesRoute = {
  path: '/accessories',
  name: '時尚配件',
};
/* v1隱藏項目 */

export const SITE_HEADER_NAVS = [{
  path: '/about',
  name: '品牌故事',
}, {
  path: '/introduction',
  name: '包膜服務',
}, {
  path: '/customize',
  name: '創意客製',
}, {
  path: '/activities',
  name: '最新消息',
}, {
  path: '/member',
  name: '會員中心',
}];
// site footer NAVS
export const SITE_FOOTER_NAVS = [{
  title: '關於 artmo',
  categories: [{
    path: '/about/story',
    name: '品牌故事',
  }, {
    link: 'https://www.104.com.tw/jobbank/custjob/index.php?r=cust&j=5f3b436c35373f6831333b64393f371a72a2a2a2a43474b2674j02',
    name: '加入我們',
  }, {
    path: '/about/privacy',
    name: '隱私權條款',
  }],
}, {
  title: '購物說明',
  categories: [{
    path: '/description/payment',
    name: '付款方式',
  }, {
    path: '/description/transport',
    name: '運送方式',
  }, {
    path: '/description/return',
    name: '退換貨方式',
  }],
}, {
  title: '客服資訊',
  categories: [{
    path: '/customer/qa',
    name: '常見問題',
  }, {
    path: '/customer/rights',
    name: '會員權益聲明',
  }, {
    path: '/customer/disclaim',
    name: '使用與免責條款',
  }],
}, {
  title: '聯絡我們',
  categories: [{
    path: '/contact',
    name: '服務資訊',
  }, {
    path: '/contact',
    name: '預約方式',
  }],
}];
