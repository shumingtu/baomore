import categoryMocks from '../../mocks/activityCategories.json';
import activitiesMocks from '../../mocks/activities.json';
import activityMock from '../../mocks/activity.json';
// categories
import landing1 from '../../static/images/activity/desktop/activity-mock-landing-1.jpg';
import landing2 from '../../static/images/activity/desktop/activity-mock-landing-2.jpg';
import mLanding1 from '../../static/images/activity/mobile/activity-mock-landing-1.jpg';
import mLanding2 from '../../static/images/activity/mobile/activity-mock-landing-2.jpg';
// activity list
import list1 from '../../static/images/activity/desktop/activity-mock-list-1.jpg';
// activity detail
import detail1 from '../../static/images/activity/desktop/activity-mock-detail-1.jpg';
import mDetail1 from '../../static/images/activity/mobile/activity-mock-detail-1.jpg';

export const CATEGORY_MOCKS = categoryMocks.data.activityCategories.map((c, idx) => ({
  id: c.id,
  name: c.name,
  desktopImg: idx % 2 === 0 ? landing1 : landing2,
  mobileImg: idx % 2 === 0 ? mLanding1 : mLanding2,
}));

export const OTHER_ACTIVITIES = [{
  id: 'facebook',
  name: 'FACEBOOK活動',
  link: 'https://www.facebook.com/artmoinc/',
}, {
  id: 'line',
  name: 'LINE＠活動',
  link: 'https://line.me/zh-hant/',
}];

export const ACTIVITIES_MOCKS = activitiesMocks.data.activities.map(a => ({
  id: a.id,
  name: a.name,
  description: a.description,
  desktopImg: detail1,
  mobileImg: mDetail1,
}));

export const ACTIVITY_MOCK = {
  ...activityMock.data.activity,
  desktopImg: detail1,
  mobileImg: mDetail1,
};

export default null;
