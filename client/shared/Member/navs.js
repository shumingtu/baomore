import editIcon from '../../static/images/member/member-edit-icon.png';
import customizeIcon from '../../static/images/member/member-customize-icon.png';
import orderIcon from '../../static/images/member/member-order-icon.png';
import serviceIcon from '../../static/images/member/member-service-icon.png';

export const LOGIN_NAV = [{
  id: 'login',
  name: '會員註冊/登入',
  path: '/member/login',
  icon: null,
}];

export const LOGOUT_NAV = [{
  id: 'logout',
  name: '會員登出',
  path: '/member/logout',
  icon: null,
}];

export const MEMBER_NAVS = [{
  id: 'edit',
  name: '個人資料',
  path: '/member/edit',
  icon: editIcon,
}, {
  id: 'customize',
  name: '客製暫存',
  path: '/payment',
  icon: customizeIcon,
}, {
  id: 'order',
  name: '訂單查詢',
  path: '/member/order',
  icon: orderIcon,
}, {
  id: 'service',
  name: '售後服務',
  path: '/member/service',
  icon: serviceIcon,
}];
