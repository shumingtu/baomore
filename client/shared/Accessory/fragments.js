/* desktop */
// landing
import landing1 from '../../static/images/accessory/desktop/accessory-desktop-landing-1.jpg';
import landing2 from '../../static/images/accessory/desktop/accessory-desktop-landing-2.jpg';
import landing3 from '../../static/images/accessory/desktop/accessory-desktop-landing-3.jpg';
import landing4 from '../../static/images/accessory/desktop/accessory-desktop-landing-4.jpg';
/* mobile */
// landing
import mLanding1 from '../../static/images/accessory/mobile/accessory-mobile-landing-1.jpg';
import mLanding2 from '../../static/images/accessory/mobile/accessory-mobile-landing-2.jpg';
import mLanding3 from '../../static/images/accessory/mobile/accessory-mobile-landing-3.jpg';
import mLanding4 from '../../static/images/accessory/mobile/accessory-mobile-landing-4.jpg';

import {
  ACCESSORY_TYPE1_TITLE,
  ACCESSORY_TYPE1_FRAGMENTS,
} from './type1/index.js';
import {
  ACCESSORY_TYPE2_TITLE,
  ACCESSORY_TYPE2_FRAGMENTS,
} from './type2/index.js';
import {
  ACCESSORY_TYPE3_TITLE,
  ACCESSORY_TYPE3_FRAGMENTS,
} from './type3/index.js';
import {
  ACCESSORY_TYPE4_TITLE,
  ACCESSORY_TYPE4_FRAGMENTS,
} from './type4/index.js';

export const TYPE1_TITLE = ACCESSORY_TYPE1_TITLE;
export const TYPE1_FRAGMENTS = ACCESSORY_TYPE1_FRAGMENTS;
export const TYPE2_TITLE = ACCESSORY_TYPE2_TITLE;
export const TYPE2_FRAGMENTS = ACCESSORY_TYPE2_FRAGMENTS;
export const TYPE3_TITLE = ACCESSORY_TYPE3_TITLE;
export const TYPE3_FRAGMENTS = ACCESSORY_TYPE3_FRAGMENTS;
export const TYPE4_TITLE = ACCESSORY_TYPE4_TITLE;
export const TYPE4_FRAGMENTS = ACCESSORY_TYPE4_FRAGMENTS;

export const LANDING_FRAGMENTS = [{
  title: {
    name: '極淨沉穩',
    color: '#000',
  },
  slogan: {
    name: '就是要保護你',
    color: '#000',
  },
  button: {
    filledColor: false,
    title: '了解更多',
    path: '/accessories/type1',
  },
  desktopImg: landing1,
  mobileImg: mLanding1,
}, {
  title: {
    name: '真實立體聲',
    color: '#fff',
  },
  slogan: {
    name: '沉醉你愛的音樂',
    color: '#fff',
  },
  button: {
    filledColor: true,
    title: '了解更多',
    path: '/accessories/type2',
  },
  desktopImg: landing2,
  mobileImg: mLanding2,
}, {
  title: {
    name: '智能快沖不傷機',
    color: '#000',
  },
  slogan: {
    name: '無線，能量無限',
    color: '#000',
  },
  button: {
    filledColor: true,
    title: '了解更多',
    path: '/accessories/type3',
  },
  desktopImg: landing3,
  mobileImg: mLanding3,
}, {
  title: {
    name: '傳輸/充電',
    color: '#000',
  },
  slogan: {
    name: '堅韌高效率',
    color: '#000',
  },
  button: {
    filledColor: false,
    title: '了解更多',
    path: '/accessories/type4',
  },
  desktopImg: landing4,
  mobileImg: mLanding4,
}];

export default null;
