import nav from '../navs.js';
/* desktop */
import introA1 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-1.jpg';
import introA2 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-2.jpg';
import introA3 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-3.jpg';
import introA4 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-4.jpg';
import introA5 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-5.jpg';
import introA6 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-6.jpg';
import introA7 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-7.jpg';
import introA8 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-8.jpg';
import introA9 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-9.jpg';
import introA10 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-10.jpg';
import introA11 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-11.jpg';
import introA12 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-12.jpg';
import introA13 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-13.jpg';
/* mobile */
import mIntroA1 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-1.jpg';
import mIntroA2 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-2.jpg';
import mIntroA3 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-3.jpg';
import mIntroA4 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-4.jpg';
import mIntroA5 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-5.jpg';
import mIntroA6 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-6.jpg';
import mIntroA7 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-7.jpg';
import mIntroA8 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-8.jpg';
import mIntroA9 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-9.jpg';
import mIntroA10 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-10.jpg';
import mIntroA11 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-11.jpg';
import mIntroA12 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-12.jpg';
import mIntroA13 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-13.jpg';

export const ACCESSORY_TYPE4_TITLE = {
  path: '/accessories/type4',
  name: (nav[3] && nav[3].name) || null,
};

export const ACCESSORY_TYPE4_FRAGMENTS = {
  fragment1: {
    title: '',
    slogan: '',
    desktopImg: null,
    mobileImg: null,
  },
  fragment2: {
    src: 'https://www.youtube.com/embed/6DuZfNrboso',
  },
  fragment3: {
    title: '',
    desktopImg: null,
    mobileImg: null,
  },
  fragment4: {
    desktopImg: null,
    mobileImg: null,
    descTitle1: '',
    desc1: '',
    descTitle2: '',
    desc2: '',
  },
  fragment5: {
    desktopImg: null,
    mobileImg: null,
  },
  fragment6: [{
    desktopImg: null,
    mobileImg: null,
    title: '',
    desc: '',
  }, {
    desktopImg: null,
    mobileImg: null,
    title: '',
    desc: '',
  }],
  fragment7: {
    desktopImg: null,
    mobileImg: null,
  },
  fragment8: {
    desktopImg: null,
    mobileImg: null,
  },
  fragment9: {
    desktopImg: null,
    mobileImg: null,
    desc: '',
  },
  fragment10: {
    desktopImg: null,
    mobileImg: null,
  },
  fragment11: {
    desc: '',
    title: '',
    path: null,
    examples: [{
      title: '',
      desc: '',
      desktopImg: null,
      mobileImg: null,
    }, {
      title: '',
      desc: '',
      desktopImg: null,
      mobileImg: null,
    }, {
      title: '',
      desc: '',
      desktopImg: null,
      mobileImg: null,
    }],
  },
};

export default null;
