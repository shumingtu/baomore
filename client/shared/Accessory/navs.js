import protectIcon from '../../static/images/accessory/desktop/accessory-protect-icon.png';
import bluetoothIcon from '../../static/images/accessory/desktop/accessory-bluetooth-icon.png';
import chargeIcon from '../../static/images/accessory/desktop/accessory-charge-icon.png';
import lineIcon from '../../static/images/accessory/desktop/accessory-line-icon.png';

export default [{
  id: 'type1',
  path: '/accessories/type1',
  icon: protectIcon,
  name: '保護類',
}, {
  id: 'type2',
  path: '/accessories/type2',
  icon: bluetoothIcon,
  name: '藍芽類',
}, {
  id: 'type3',
  path: '/accessories/type3',
  icon: chargeIcon,
  name: '充電類',
}, {
  id: 'type4',
  path: '/accessories/type4',
  icon: lineIcon,
  name: '線材類',
}];
