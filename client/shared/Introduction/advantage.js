/* desktop */
import advantage1 from '../../static/images/introduction/desktop/advantage/introduction-desktop-advantage-1.png';
import advantage2 from '../../static/images/introduction/desktop/advantage/introduction-desktop-advantage-2.jpg';
import advantage3 from '../../static/images/introduction/desktop/advantage/introduction-desktop-advantage-3.jpg';
import advantage4 from '../../static/images/introduction/desktop/advantage/introduction-desktop-advantage-4.jpg';
/* mobile */
import mAdvantage1 from '../../static/images/introduction/mobile/advantage/introduction-mobile-advantage-1.jpg';
import mAdvantage2 from '../../static/images/introduction/mobile/advantage/introduction-mobile-advantage-2.jpg';
import mAdvantage3 from '../../static/images/introduction/mobile/advantage/introduction-mobile-advantage-3.jpg';
import mAdvantage4 from '../../static/images/introduction/mobile/advantage/introduction-mobile-advantage-4.jpg';

export default {
  videoSrc: 'https://www.youtube.com/embed/-cmjvo4TURo',
  fragment1: {
    title: '包膜6大優勢',
    desc: '獨家技術讓你擁有最有感的浮雕膜，\n比市售浮雕膜厚2倍！是各地裸機愛好者最佳首選，\n開啟包膜新時代',
    cards: [{
      title: '防止刮傷',
      desc: 'artmo專業包膜極度耐刮，完整防護手機外觀，artmo的研發只希望守護您最尊貴的手機。我們的團隊時時刻刻為您準備就緒，立即預約保護您的愛機吧！',
    }, {
      title: '完全包覆',
      desc: '最強大的防塵效果在artmo，全機包覆再加上邊條強化！使用手機殼容易卡灰塵，歷久難清理，包膜能無縫包覆避免進塵的，愛護手機的您能不包膜嗎？',
    }, {
      title: '低調奢華',
      desc: '精緻療癒，超立體浮雕彩膜讓你展現獨特魅力！不僅絕美體現且手握有感，artmo細膩工法讓你實際體驗非凡氣場！',
    }, {
      title: '裸機質感',
      desc: '包膜絕對是裸機愛好者的救星！我們的膜料表面光滑透亮，隱形清澈的透氣孔，質感一流，手機已全然晉升不同境界，給你更舒適、更輕盈的體驗。',
    }, {
      title: '完美防護',
      desc: '包過artmo的手機都說讚！換活時就會發現拆除我們的包膜後仍然如包膜前的機況！高品質的防護給品味超群的你。',
    }, {
      title: '超強耐磨',
      desc: '包膜本身耐磨，我們專為您守護，邊條修復與加強享有五年超長保固！用時間證明artmo溫暖的服務。',
    }],
  },
  fragment2: {
    title: '細心工法服服貼貼',
    desc: 'artmo，不同凡響。設計層次豐富、創新領先的技術，\n打造每一視覺與觸覺的極致享受，處處細節讓你「更有感」。\n生活每時刻，感受artmo貼進心坎。',
    images: [{
      desktopImg: advantage1,
      mobileImg: mAdvantage1,
    }, {
      desktopImg: advantage2,
      mobileImg: mAdvantage2,
    }, {
      desktopImg: advantage3,
      mobileImg: mAdvantage3,
    }, {
      desktopImg: advantage4,
      mobileImg: mAdvantage4,
    }],
  },
  slogan: '將你的手機，放心給artmo保護',
};
