/* eslint camelcase: 0 */
/* desktop */
import series1 from '../../../static/images/introduction/desktop/type4/series1/introduction-desktop-series1-1.jpg';
import series2_1_1 from '../../../static/images/introduction/desktop/type4/series1/introduction-desktop-series1-2-1-1.jpg';
import series2_1_2 from '../../../static/images/introduction/desktop/type4/series1/introduction-desktop-series1-2-1-2.jpg';
import series2_1_3 from '../../../static/images/introduction/desktop/type4/series1/introduction-desktop-series1-2-1-3.jpg';
/* mobile */
import mSeries1 from '../../../static/images/introduction/mobile/type4/series1/introduction-mobile-series1-1.jpg';
import mSeries2_1_1 from '../../../static/images/introduction/mobile/type4/series1/introduction-mobile-series1-2-1-1.jpg';
import mSeries2_1_2 from '../../../static/images/introduction/mobile/type4/series1/introduction-mobile-series1-2-1-2.jpg';
import mSeries2_1_3 from '../../../static/images/introduction/mobile/type4/series1/introduction-mobile-series1-2-1-3.jpg';

export const TYPE4_SERIES1_FRAGMENTS = {
  mainBackgroundColor: 'rgb(255, 255, 255)',
  fragment1: {
    desktopImg: series1,
    mobileImg: mSeries1,
  },
  fragment2: {
    title: {
      color: 'rgb(242, 150, 0)',
      name: '時尚不敗幾何圖',
    },
    desc: {
      color: 'rgb(0, 0, 0)',
      name: '閃爍的星，一直是經典中的經典，它帶著青春氣息。',
    },
    images: [{
      desc: '五芒星帶給人無限希望，發揮無限的可能性！',
      src1: {
        desktopImg: series2_1_1,
        mobileImg: mSeries2_1_1,
      },
      src2: {
        desktopImg: series2_1_2,
        mobileImg: mSeries2_1_2,
      },
      src3: {
        desktopImg: series2_1_3,
        mobileImg: mSeries2_1_3,
      },
    }],
    button: {
      backgroundColor: 'rgb(242, 150, 0)',
      color: 'rgb(255, 255, 255)',
    },
  },
};

export default null;
