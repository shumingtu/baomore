import nav from '../navs.js';
/* desktop */
import introA1 from '../../../static/images/introduction/desktop/type4/introduction-desktop-intro-4-1.jpg';
import introA2 from '../../../static/images/introduction/desktop/type4/introduction-desktop-intro-4-2.jpg';
import introA3 from '../../../static/images/introduction/desktop/type4/introduction-desktop-intro-4-3.jpg';
// import introA4 from '../../../static/images/introduction/desktop/type4/introduction-desktop-intro-4-4.jpg';
import introA5 from '../../../static/images/introduction/desktop/type4/introduction-desktop-intro-4-5.jpg';
import introA6 from '../../../static/images/introduction/desktop/type4/introduction-desktop-intro-4-6.jpg';
import introA7 from '../../../static/images/introduction/desktop/type4/introduction-desktop-intro-4-7.jpg';
// import introA8 from '../../../static/images/introduction/desktop/type4/introduction-desktop-intro-4-8.jpg';
// import introA9 from '../../../static/images/introduction/desktop/type4/introduction-desktop-intro-4-9.jpg';
// import introA10 from '../../../static/images/introduction/desktop/type4/introduction-desktop-intro-4-10.jpg';
import introA11 from '../../../static/images/introduction/desktop/type4/introduction-desktop-intro-4-11.jpg';
import introA12 from '../../../static/images/introduction/desktop/type4/introduction-desktop-intro-4-12.jpg';
import introA13 from '../../../static/images/introduction/desktop/type4/introduction-desktop-intro-4-13.jpg';
/* mobile */
import mIntroA1 from '../../../static/images/introduction/mobile/type4/introduction-mobile-intro-4-1.jpg';
import mIntroA2 from '../../../static/images/introduction/mobile/type4/introduction-mobile-intro-4-2.jpg';
import mIntroA3 from '../../../static/images/introduction/mobile/type4/introduction-mobile-intro-4-3.jpg';
// import mIntroA4 from '../../../static/images/introduction/mobile/type4/introduction-mobile-intro-4-4.jpg';
import mIntroA5 from '../../../static/images/introduction/mobile/type4/introduction-mobile-intro-4-5.jpg';
import mIntroA6 from '../../../static/images/introduction/mobile/type4/introduction-mobile-intro-4-6.jpg';
import mIntroA7 from '../../../static/images/introduction/mobile/type4/introduction-mobile-intro-4-7.jpg';
// import mIntroA8 from '../../../static/images/introduction/mobile/type4/introduction-mobile-intro-4-8.jpg';
// import mIntroA9 from '../../../static/images/introduction/mobile/type4/introduction-mobile-intro-4-9.jpg';
// import mIntroA10 from '../../../static/images/introduction/mobile/type4/introduction-mobile-intro-4-10.jpg';
import mIntroA11 from '../../../static/images/introduction/mobile/type4/introduction-mobile-intro-4-11.jpg';
import mIntroA12 from '../../../static/images/introduction/mobile/type4/introduction-mobile-intro-4-12.jpg';
import mIntroA13 from '../../../static/images/introduction/mobile/type4/introduction-mobile-intro-4-13.jpg';

// series data
import {
  TYPE4_SERIES1_FRAGMENTS,
} from './series1.js';

export const SERIES1_FRAGMENTS = TYPE4_SERIES1_FRAGMENTS;
// 主標題 - 基礎壓紋膜
export const INTRO_TYPE4_TITLE = {
  path: '/introduction/type4',
  name: (nav[3] && nav[3].name) || null,
};
// 基礎壓紋膜 - 系列群
export const INTRO_TYPE4_SERIES = [{
  id: 'series1',
  path: '/introduction/type4/series1',
  name: '基本款',
  data: SERIES1_FRAGMENTS,
}];
// 基礎壓紋膜 - 主文章
export const INTRO_TYPE4_FRAGMENTS = {
  fragment1: {
    title: {
      color: '#000',
      name: '透明壓紋系列',
    },
    slogan: {
      color: '#000',
      name: '低調中帶點神秘色彩',
    },
    desktopImg: introA1,
    mobileImg: mIntroA1,
  },
  fragment2: {
    src: 'https://www.youtube.com/embed/eAPcdwmZQv0',
  },
  fragment3: {
    title: '基礎壓紋最經典之處在於簡單。\n但越是簡單的東西，越有不平凡的靈魂！',
    desktopImg: introA2,
    mobileImg: mIntroA2,
  },
  fragment4: {
    desktopImg: introA3,
    mobileImg: mIntroA3,
    descTitle1: null,
    desc1: null,
    descTitle2: null,
    desc2: null,
  },
  // fragment5: {
  //   desktopImg: introA4,
  //   mobileImg: mIntroA4,
  // },
  fragment6: [{
    desktopImg: introA5,
    mobileImg: mIntroA5,
    title: '若隱若現的奢華。',
    desc: '細節看得見，手感零障礙。',
  }, {
    desktopImg: introA6,
    mobileImg: mIntroA6,
    title: '平凡出眾成為焦點。',
    desc: '自然光照耀就能浮現！',
  }],
  fragment7: {
    desktopImg: introA7,
    mobileImg: mIntroA7,
  },
  // fragment8: {
  //   desktopImg: introA8,
  //   mobileImg: mIntroA8,
  // },
  // fragment9: {
  //   desktopImg: introA9,
  //   mobileImg: mIntroA9,
  //   desc: '領先業界超立體浮雕膜，擁有最強大技術！\n表面光滑透亮，清澈透氣孔，不限制機型，\n無論從哪一個角度看，觸感立體，登峰造極。',
  // },
  // fragment10: {
  //   desktopImg: introA10,
  //   mobileImg: mIntroA10,
  // },
  fragment11: {
    desc: null,
    title: '尋找屬於你的守護者',
    path: INTRO_TYPE4_SERIES[0].path,
    examples: [{
      title: '透明壓紋系列',
      desc: '卡夢',
      desktopImg: introA11,
      mobileImg: mIntroA11,
    }, {
      title: '透明壓紋系列',
      desc: '蝴蝶結',
      desktopImg: introA12,
      mobileImg: mIntroA12,
    }, {
      title: '透明壓紋系列',
      desc: '交叉髮絲',
      desktopImg: introA13,
      mobileImg: mIntroA13,
    }],
  },
};

export default null;
