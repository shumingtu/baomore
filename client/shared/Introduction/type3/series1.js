/* eslint camelcase: 0 */
/* desktop */
import series1 from '../../../static/images/introduction/desktop/type3/series1/introduction-desktop-series1-1.jpg';
import series2_1_1 from '../../../static/images/introduction/desktop/type3/series1/introduction-desktop-series1-2-1-1.jpg';
import series2_1_2 from '../../../static/images/introduction/desktop/type3/series1/introduction-desktop-series1-2-1-2.jpg';
import series2_1_3 from '../../../static/images/introduction/desktop/type3/series1/introduction-desktop-series1-2-1-3.jpg';
import series2_2_1 from '../../../static/images/introduction/desktop/type3/series1/introduction-desktop-series1-2-2-1.jpg';
import series2_2_2 from '../../../static/images/introduction/desktop/type3/series1/introduction-desktop-series1-2-2-2.jpg';
import series2_2_3 from '../../../static/images/introduction/desktop/type3/series1/introduction-desktop-series1-2-2-3.jpg';
import series2_3_1 from '../../../static/images/introduction/desktop/type3/series1/introduction-desktop-series1-2-3-1.jpg';
import series2_3_2 from '../../../static/images/introduction/desktop/type3/series1/introduction-desktop-series1-2-3-2.jpg';
import series2_3_3 from '../../../static/images/introduction/desktop/type3/series1/introduction-desktop-series1-2-3-3.jpg';
/* mobile */
import mSeries1 from '../../../static/images/introduction/mobile/type3/series1/introduction-mobile-series1-1.jpg';
import mSeries2_1_1 from '../../../static/images/introduction/mobile/type3/series1/introduction-mobile-series1-2-1-1.jpg';
import mSeries2_1_2 from '../../../static/images/introduction/mobile/type3/series1/introduction-mobile-series1-2-1-2.jpg';
import mSeries2_1_3 from '../../../static/images/introduction/mobile/type3/series1/introduction-mobile-series1-2-1-3.jpg';
import mSeries2_2_1 from '../../../static/images/introduction/mobile/type3/series1/introduction-mobile-series1-2-2-1.jpg';
import mSeries2_2_2 from '../../../static/images/introduction/mobile/type3/series1/introduction-mobile-series1-2-2-2.jpg';
import mSeries2_2_3 from '../../../static/images/introduction/mobile/type3/series1/introduction-mobile-series1-2-2-3.jpg';
import mSeries2_3_1 from '../../../static/images/introduction/mobile/type3/series1/introduction-mobile-series1-2-3-1.jpg';
import mSeries2_3_2 from '../../../static/images/introduction/mobile/type3/series1/introduction-mobile-series1-2-3-2.jpg';
import mSeries2_3_3 from '../../../static/images/introduction/mobile/type3/series1/introduction-mobile-series1-2-3-3.jpg';

export const TYPE3_SERIES1_FRAGMENTS = {
  mainBackgroundColor: 'rgb(0, 0, 0)',
  fragment1: {
    desktopImg: series1,
    mobileImg: mSeries1,
  },
  fragment2: {
    title: {
      color: 'rgb(242, 150, 0)',
      name: '復仇者聯盟',
    },
    desc: {
      color: 'rgb(255, 255, 255)',
      name: '漫威宇宙中耳熟能詳的英雄們，隱匿在日常生活，\n在需要時即刻顯現，默默守護你的世界。',
    },
    images: [{
      desc: '象徵美國隊長的盾牌，擁有一顆超級堅毅、單純善良的心，\n以及永不放棄的精神，保護自己所愛。',
      src1: {
        desktopImg: series2_1_1,
        mobileImg: mSeries2_1_1,
      },
      src2: {
        desktopImg: series2_1_2,
        mobileImg: mSeries2_1_2,
      },
      src3: {
        desktopImg: series2_1_3,
        mobileImg: mSeries2_1_3,
      },
    }, {
      desc: '英雄icon大集結！完全激起心底的英雄魂\n精心設計的漫威元素與精神巧妙交織，熱愛Marvel的你勢必要包起來！',
      src1: {
        desktopImg: series2_2_1,
        mobileImg: mSeries2_2_1,
      },
      src2: {
        desktopImg: series2_2_2,
        mobileImg: mSeries2_2_2,
      },
      src3: {
        desktopImg: series2_2_3,
        mobileImg: mSeries2_2_3,
      },
    }, {
      desc: '不知道每一次奮力一搏是否能拯救世界？\n重要的是，我們的心永遠在一起！',
      src1: {
        desktopImg: series2_3_1,
        mobileImg: mSeries2_3_1,
      },
      src2: {
        desktopImg: series2_3_2,
        mobileImg: mSeries2_3_2,
      },
      src3: {
        desktopImg: series2_3_3,
        mobileImg: mSeries2_3_3,
      },
    }],
    button: {
      backgroundColor: 'rgb(242, 150, 0)',
      color: 'rgb(255, 255, 255)',
    },
  },
};

export default null;
