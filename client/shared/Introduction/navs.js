export default [{
  id: 'type1',
  path: '/introduction/type1',
  name: '授權款浮雕膜',
}, {
  id: 'type2',
  path: '/introduction/type2',
  name: '設計款浮雕膜',
}, {
  id: 'type3',
  path: '/introduction/type3',
  name: '授權款壓紋膜',
}, {
  id: 'type4',
  path: '/introduction/type4',
  name: '基礎壓紋膜',
}];
