/* eslint camelcase: 0 */
/* desktop */
import series1 from '../../../static/images/introduction/desktop/type2/series1/introduction-desktop-series1-1.jpg';
import series2_1_1 from '../../../static/images/introduction/desktop/type2/series1/introduction-desktop-series1-2-1-1.jpg';
import series2_1_2 from '../../../static/images/introduction/desktop/type2/series1/introduction-desktop-series1-2-1-2.jpg';
import series2_1_3 from '../../../static/images/introduction/desktop/type2/series1/introduction-desktop-series1-2-1-3.jpg';
import series2_2_1 from '../../../static/images/introduction/desktop/type2/series1/introduction-desktop-series1-2-2-1.jpg';
import series2_2_2 from '../../../static/images/introduction/desktop/type2/series1/introduction-desktop-series1-2-2-2.jpg';
import series2_2_3 from '../../../static/images/introduction/desktop/type2/series1/introduction-desktop-series1-2-2-3.jpg';
/* mobile */
import mSeries1 from '../../../static/images/introduction/mobile/type2/series1/introduction-mobile-series1-1.jpg';
import mSeries2_1_1 from '../../../static/images/introduction/mobile/type2/series1/introduction-mobile-series1-2-1-1.jpg';
import mSeries2_1_2 from '../../../static/images/introduction/mobile/type2/series1/introduction-mobile-series1-2-1-2.jpg';
import mSeries2_1_3 from '../../../static/images/introduction/mobile/type2/series1/introduction-mobile-series1-2-1-3.jpg';
import mSeries2_2_1 from '../../../static/images/introduction/mobile/type2/series1/introduction-mobile-series1-2-2-1.jpg';
import mSeries2_2_2 from '../../../static/images/introduction/mobile/type2/series1/introduction-mobile-series1-2-2-2.jpg';
import mSeries2_2_3 from '../../../static/images/introduction/mobile/type2/series1/introduction-mobile-series1-2-2-3.jpg';

export const TYPE2_SERIES1_FRAGMENTS = {
  mainBackgroundColor: 'rgb(255, 255, 255)',
  fragment1: {
    desktopImg: series1,
    mobileImg: mSeries1,
  },
  fragment2: {
    title: {
      color: 'rgb(242, 150, 0)',
      name: '設計插畫',
    },
    desc: {
      color: 'rgb(0, 0, 0)',
      name: '從街頭潮流到時尚精品樣樣具備，且不斷碰撞將流行推向浮雕的更高層次，\n使包膜逐漸成為流行文化與品味展現的獨特細節。',
    },
    images: [{
      desc: '復古元素近來躍升主流，怎樣都好搭，令人愛不釋手！\n包上復古風的膜後，手機完全完美詮釋個人獨一無二的性格。',
      src1: {
        desktopImg: series2_1_1,
        mobileImg: mSeries2_1_1,
      },
      src2: {
        desktopImg: series2_1_2,
        mobileImg: mSeries2_1_2,
      },
      src3: {
        desktopImg: series2_1_3,
        mobileImg: mSeries2_1_3,
      },
    }, {
      desc: '神秘高貴的金色玫瑰花，是等待，抑或是愛情即將燃起的起點。',
      src1: {
        desktopImg: series2_2_1,
        mobileImg: mSeries2_2_1,
      },
      src2: {
        desktopImg: series2_2_2,
        mobileImg: mSeries2_2_2,
      },
      src3: {
        desktopImg: series2_2_3,
        mobileImg: mSeries2_2_3,
      },
    }],
    button: {
      backgroundColor: 'rgb(242, 150, 0)',
      color: 'rgb(255, 255, 255)',
    },
  },
};

export default null;
