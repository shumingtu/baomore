import nav from '../navs.js';
/* desktop */
import introA1 from '../../../static/images/introduction/desktop/type2/introduction-desktop-intro-2-1.jpg';
import introA2 from '../../../static/images/introduction/desktop/type2/introduction-desktop-intro-2-2.jpg';
import introA3 from '../../../static/images/introduction/desktop/type2/introduction-desktop-intro-2-3.jpg';
import introA4 from '../../../static/images/introduction/desktop/type2/introduction-desktop-intro-2-4.jpg';
import introA5 from '../../../static/images/introduction/desktop/type2/introduction-desktop-intro-2-5.jpg';
import introA6 from '../../../static/images/introduction/desktop/type2/introduction-desktop-intro-2-6.jpg';
// import introA7 from '../../../static/images/introduction/desktop/type2/introduction-desktop-intro-2-7.jpg';
// import introA8 from '../../../static/images/introduction/desktop/type2/introduction-desktop-intro-2-8.jpg';
// import introA9 from '../../../static/images/introduction/desktop/type2/introduction-desktop-intro-2-9.jpg';
// import introA10 from '../../../static/images/introduction/desktop/type2/introduction-desktop-intro-2-10.jpg';
import introA11 from '../../../static/images/introduction/desktop/type2/introduction-desktop-intro-2-11.jpg';
import introA12 from '../../../static/images/introduction/desktop/type2/introduction-desktop-intro-2-12.jpg';
import introA13 from '../../../static/images/introduction/desktop/type2/introduction-desktop-intro-2-13.jpg';
/* mobile */
import mIntroA1 from '../../../static/images/introduction/mobile/type2/introduction-mobile-intro-2-1.jpg';
import mIntroA2 from '../../../static/images/introduction/mobile/type2/introduction-mobile-intro-2-2.jpg';
import mIntroA3 from '../../../static/images/introduction/mobile/type2/introduction-mobile-intro-2-3.jpg';
import mIntroA4 from '../../../static/images/introduction/mobile/type2/introduction-mobile-intro-2-4.jpg';
import mIntroA5 from '../../../static/images/introduction/mobile/type2/introduction-mobile-intro-2-5.jpg';
import mIntroA6 from '../../../static/images/introduction/mobile/type2/introduction-mobile-intro-2-6.jpg';
// import mIntroA7 from '../../../static/images/introduction/mobile/type2/introduction-mobile-intro-2-7.jpg';
// import mIntroA8 from '../../../static/images/introduction/mobile/type2/introduction-mobile-intro-2-8.jpg';
// import mIntroA9 from '../../../static/images/introduction/mobile/type2/introduction-mobile-intro-2-9.jpg';
// import mIntroA10 from '../../../static/images/introduction/mobile/type2/introduction-mobile-intro-2-10.jpg';
import mIntroA11 from '../../../static/images/introduction/mobile/type2/introduction-mobile-intro-2-11.jpg';
import mIntroA12 from '../../../static/images/introduction/mobile/type2/introduction-mobile-intro-2-12.jpg';
import mIntroA13 from '../../../static/images/introduction/mobile/type2/introduction-mobile-intro-2-13.jpg';

// series data
import {
  TYPE2_SERIES1_FRAGMENTS,
} from './series1.js';

export const SERIES1_FRAGMENTS = TYPE2_SERIES1_FRAGMENTS;
// 主標題 - 設計款浮雕膜
export const INTRO_TYPE2_TITLE = {
  path: '/introduction/type2',
  name: (nav[1] && nav[1].name) || null,
};
// 設計款浮雕膜 - 系列群
export const INTRO_TYPE2_SERIES = [{
  id: 'series1',
  path: '/introduction/type2/series1',
  name: '設計款系列',
  data: SERIES1_FRAGMENTS,
}];
// 設計款浮雕膜 - 主文章
export const INTRO_TYPE2_FRAGMENTS = {
  fragment1: {
    title: {
      color: '#000',
      name: '設計款包膜',
    },
    slogan: {
      color: '#000',
      name: '時尚流行設計自我風格',
    },
    desktopImg: introA1,
    mobileImg: mIntroA1,
  },
  fragment2: {
    src: 'https://www.youtube.com/embed/DA37oiwDdMg',
  },
  fragment3: {
    title: '時代飛快進步著，這短短五十年來，\n手機已經從高級昂貴的巨大機器變成人手數支的生活必需品！',
    desktopImg: introA2,
    mobileImg: mIntroA2,
  },
  fragment4: {
    desktopImg: introA3,
    mobileImg: mIntroA3,
    descTitle1: null,
    desc1: null,
    descTitle2: null,
    desc2: null,
  },
  fragment5: {
    desktopImg: introA4,
    mobileImg: mIntroA4,
  },
  fragment6: [{
    desktopImg: introA5,
    mobileImg: mIntroA5,
    title: null,
    desc: null,
  }, {
    desktopImg: introA6,
    mobileImg: mIntroA6,
    title: null,
    desc: null,
  }],
  // fragment7: {
  //   desktopImg: introA7,
  //   mobileImg: mIntroA7,
  // },
  // fragment8: {
  //   desktopImg: introA8,
  //   mobileImg: mIntroA8,
  // },
  // fragment9: {
  //   desktopImg: introA9,
  //   mobileImg: mIntroA9,
  //   desc: '領先業界超立體浮雕膜，擁有最強大技術！\n表面光滑透亮，清澈透氣孔，不限制機型，\n無論從哪一個角度看，觸感立體，登峰造極。',
  // },
  // fragment10: {
  //   desktopImg: introA10,
  //   mobileImg: mIntroA10,
  // },
  fragment11: {
    desc: null,
    title: '尋找屬於你的守護者',
    path: INTRO_TYPE2_SERIES[0].path,
    examples: [{
      title: '春桐飄沂',
      desc: 'SNA19',
      desktopImg: introA11,
      mobileImg: mIntroA11,
    }, {
      title: '燦玩潏潏',
      desc: 'SNA20',
      desktopImg: introA12,
      mobileImg: mIntroA12,
    }, {
      title: '蔻鈺出萃',
      desc: 'SNA21',
      desktopImg: introA13,
      mobileImg: mIntroA13,
    }],
  },
};

export default null;
