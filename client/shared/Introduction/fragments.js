/* desktop */
// landing
import landing1 from '../../static/images/introduction/desktop/introduction-desktop-landing-1.jpg';
import landing2 from '../../static/images/introduction/desktop/introduction-desktop-landing-2.jpg';
import landing3 from '../../static/images/introduction/desktop/introduction-desktop-landing-3.jpg';
import landing4 from '../../static/images/introduction/desktop/introduction-desktop-landing-4.jpg';
/* mobile */
// landing
import mLanding1 from '../../static/images/introduction/mobile/introduction-mobile-landing-1.jpg';
import mLanding2 from '../../static/images/introduction/mobile/introduction-mobile-landing-2.jpg';
import mLanding3 from '../../static/images/introduction/mobile/introduction-mobile-landing-3.jpg';
import mLanding4 from '../../static/images/introduction/mobile/introduction-mobile-landing-4.jpg';

import {
  INTRO_TYPE1_TITLE,
  INTRO_TYPE1_SERIES,
  INTRO_TYPE1_FRAGMENTS,
} from './type1/index.js';
import {
  INTRO_TYPE2_TITLE,
  INTRO_TYPE2_SERIES,
  INTRO_TYPE2_FRAGMENTS,
} from './type2/index.js';
import {
  INTRO_TYPE3_TITLE,
  INTRO_TYPE3_SERIES,
  INTRO_TYPE3_FRAGMENTS,
} from './type3/index.js';
import {
  INTRO_TYPE4_TITLE,
  INTRO_TYPE4_SERIES,
  INTRO_TYPE4_FRAGMENTS,
} from './type4/index.js';

export const TYPE1_TITLE = INTRO_TYPE1_TITLE;
export const TYPE1_FRAGMENTS = INTRO_TYPE1_FRAGMENTS;
export const TYPE1_SERIES = INTRO_TYPE1_SERIES;
export const TYPE2_TITLE = INTRO_TYPE2_TITLE;
export const TYPE2_FRAGMENTS = INTRO_TYPE2_FRAGMENTS;
export const TYPE2_SERIES = INTRO_TYPE2_SERIES;
export const TYPE3_TITLE = INTRO_TYPE3_TITLE;
export const TYPE3_FRAGMENTS = INTRO_TYPE3_FRAGMENTS;
export const TYPE3_SERIES = INTRO_TYPE3_SERIES;
export const TYPE4_TITLE = INTRO_TYPE4_TITLE;
export const TYPE4_FRAGMENTS = INTRO_TYPE4_FRAGMENTS;
export const TYPE4_SERIES = INTRO_TYPE4_SERIES;

export const LANDING_FRAGMENTS = [{
  title: {
    name: '超獨特手感',
    color: '#000',
  },
  slogan: {
    name: '挑戰立體感官',
    color: '#000',
  },
  button: {
    filledColor: false,
    title: '了解更多',
    path: '/introduction/type1',
  },
  desktopImg: landing1,
  mobileImg: mLanding1,
}, {
  title: {
    name: '真個性風格',
    color: '#000',
  },
  slogan: {
    name: '展現完美自我',
    color: '#000',
  },
  button: {
    filledColor: false,
    title: '了解更多',
    path: '/introduction/type2',
  },
  desktopImg: landing2,
  mobileImg: mLanding2,
}, {
  title: {
    name: '有型的低調',
    color: '#000',
  },
  slogan: {
    name: '主流簡單帥氣',
    color: '#000',
  },
  button: {
    filledColor: false,
    title: '了解更多',
    path: '/introduction/type3',
  },
  desktopImg: landing3,
  mobileImg: mLanding3,
}, {
  title: {
    name: '無負擔細緻',
    color: '#000',
  },
  slogan: {
    name: '綻放絢麗風采',
    color: '#000',
  },
  button: {
    filledColor: false,
    title: '了解更多',
    path: '/introduction/type4',
  },
  desktopImg: landing4,
  mobileImg: mLanding4,
}];

export default null;
