import nav from '../navs.js';
/* desktop */
import introA1 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-1.jpg';
import introA2 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-2.jpg';
import introA3 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-3.jpg';
import introA4 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-4.jpg';
import introA5 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-5.jpg';
import introA6 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-6.jpg';
import introA7 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-7.jpg';
import introA8 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-8.jpg';
import introA9 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-9.jpg';
import introA10 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-10.jpg';
import introA11 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-11.png';
import introA12 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-12.png';
import introA13 from '../../../static/images/introduction/desktop/type1/introduction-desktop-intro-1-13.png';
/* mobile */
import mIntroA1 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-1.jpg';
import mIntroA2 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-2.jpg';
import mIntroA3 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-3.jpg';
import mIntroA4 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-4.jpg';
import mIntroA5 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-5.jpg';
import mIntroA6 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-6.jpg';
import mIntroA7 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-7.jpg';
import mIntroA8 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-8.jpg';
import mIntroA9 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-9.jpg';
import mIntroA10 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-10.jpg';
import mIntroA11 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-11.png';
import mIntroA12 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-12.png';
import mIntroA13 from '../../../static/images/introduction/mobile/type1/introduction-mobile-intro-1-13.png';
// series data
import {
  TYPE1_SERIES1_FRAGMENTS,
} from './series1.js';
import {
  TYPE1_SERIES2_FRAGMENTS,
} from './series2.js';
import {
  TYPE1_SERIES3_FRAGMENTS,
} from './series3.js';

export const SERIES1_FRAGMENTS = TYPE1_SERIES1_FRAGMENTS;
export const SERIES2_FRAGMENTS = TYPE1_SERIES2_FRAGMENTS;
export const SERIES3_FRAGMENTS = TYPE1_SERIES3_FRAGMENTS;
// landing data
export const INTRO_TYPE1_TITLE = {
  path: '/introduction/type1',
  name: (nav[0] && nav[0].name) || null,
};

/* V1 隱藏項目 */
const DC_SERIES = {
  id: 'series4',
  path: '/introduction/type1/series4',
  name: 'DC HERO系列',
  data: SERIES1_FRAGMENTS,
};
/* V1 隱藏項目 */

export const INTRO_TYPE1_SERIES = [{
  id: 'series1',
  path: '/introduction/type1/series1',
  name: '三麗鷗系列',
  data: SERIES1_FRAGMENTS,
}, {
  id: 'series2',
  path: '/introduction/type1/series2',
  name: 'MARVEL系列',
  data: SERIES2_FRAGMENTS,
}, {
  id: 'series3',
  path: '/introduction/type1/series3',
  name: '小熊維尼系列',
  data: SERIES3_FRAGMENTS,
}];

export const INTRO_TYPE1_FRAGMENTS = {
  fragment1: {
    title: {
      color: '#000',
      name: '5D立體浮雕膜',
    },
    slogan: {
      color: '#000',
      name: '怎麼摸，都有感',
    },
    desktopImg: introA1,
    mobileImg: mIntroA1,
  },
  fragment2: {
    src: 'https://www.youtube.com/embed/47rHEF75I0U',
  },
  fragment3: {
    title: '獨家技術讓你擁有最有感的浮雕膜，\n比市售浮雕膜厚2倍！是各地裸機愛好者最佳首選，\n開啟包膜新時代。',
    desktopImg: introA2,
    mobileImg: mIntroA2,
  },
  fragment4: {
    desktopImg: introA3,
    mobileImg: mIntroA3,
    descTitle1: null,
    desc1: null,
    descTitle2: null,
    desc2: null,
  },
  fragment5: {
    desktopImg: introA4,
    mobileImg: mIntroA4,
  },
  fragment6: [{
    desktopImg: introA5,
    mobileImg: mIntroA5,
    title: '真實立體感。',
    desc: '肉眼看不到的，也摸得到。',
  }, {
    desktopImg: introA6,
    mobileImg: mIntroA6,
    title: '在乎所有小細節。',
    desc: '用心設計每一塊浮雕',
  }],
  fragment7: {
    desktopImg: introA7,
    mobileImg: mIntroA7,
  },
  fragment8: {
    desktopImg: introA8,
    mobileImg: mIntroA8,
  },
  fragment9: {
    desktopImg: introA9,
    mobileImg: mIntroA9,
    desc: '領先業界超立體浮雕膜，擁有最強大技術！\n表面光滑透亮，清澈透氣孔，不限制機型，\n無論從哪一個角度看，觸感立體，登峰造極。',
  },
  fragment10: {
    desktopImg: introA10,
    mobileImg: mIntroA10,
  },
  fragment11: {
    desc: '創新的技術搭配漫威等帥氣授權圖案，\n讓你享有前所未有的生活體驗。',
    title: '尋找屬於你的守護者',
    path: INTRO_TYPE1_SERIES[0].path,
    examples: [{
      title: 'MARVEL系列',
      desc: '死侍：惡棍英雄',
      desktopImg: introA11,
      mobileImg: mIntroA11,
    }, {
      title: 'DISNEY系列',
      desc: '小熊維尼：溫暖粉紅心',
      desktopImg: introA12,
      mobileImg: mIntroA12,
    }, {
      title: 'MARVEL系列',
      desc: '復仇者聯盟：無限之戰',
      desktopImg: introA13,
      mobileImg: mIntroA13,
    }],
  },
};

export default null;
