/* eslint camelcase: 0 */
/* desktop */
import series1 from '../../../static/images/introduction/desktop/type1/series1/introduction-desktop-series1-1.jpg';
import series2_1_1 from '../../../static/images/introduction/desktop/type1/series1/introduction-desktop-series1-2-1-1.jpg';
import series2_1_2 from '../../../static/images/introduction/desktop/type1/series1/introduction-desktop-series1-2-1-2.jpg';
import series2_1_3 from '../../../static/images/introduction/desktop/type1/series1/introduction-desktop-series1-2-1-3.jpg';
import series2_2_1 from '../../../static/images/introduction/desktop/type1/series1/introduction-desktop-series1-2-2-1.jpg';
import series2_2_2 from '../../../static/images/introduction/desktop/type1/series1/introduction-desktop-series1-2-2-2.jpg';
import series2_2_3 from '../../../static/images/introduction/desktop/type1/series1/introduction-desktop-series1-2-2-3.jpg';
import series2_3_1 from '../../../static/images/introduction/desktop/type1/series1/introduction-desktop-series1-2-3-1.jpg';
import series2_3_2 from '../../../static/images/introduction/desktop/type1/series1/introduction-desktop-series1-2-3-2.jpg';
import series2_3_3 from '../../../static/images/introduction/desktop/type1/series1/introduction-desktop-series1-2-3-3.jpg';
import series2_4_1 from '../../../static/images/introduction/desktop/type1/series1/introduction-desktop-series1-2-4-1.jpg';
import series2_4_2 from '../../../static/images/introduction/desktop/type1/series1/introduction-desktop-series1-2-4-2.jpg';
import series2_4_3 from '../../../static/images/introduction/desktop/type1/series1/introduction-desktop-series1-2-4-3.jpg';
import series3 from '../../../static/images/introduction/desktop/type1/series1/introduction-desktop-series1-3.jpg';
import series4_1_1 from '../../../static/images/introduction/desktop/type1/series1/introduction-desktop-series1-4-1-1.jpg';
import series4_1_2 from '../../../static/images/introduction/desktop/type1/series1/introduction-desktop-series1-4-1-2.jpg';
import series4_1_3 from '../../../static/images/introduction/desktop/type1/series1/introduction-desktop-series1-4-1-3.jpg';
import series4_2_1 from '../../../static/images/introduction/desktop/type1/series1/introduction-desktop-series1-4-2-1.jpg';
import series4_2_2 from '../../../static/images/introduction/desktop/type1/series1/introduction-desktop-series1-4-2-2.jpg';
import series4_2_3 from '../../../static/images/introduction/desktop/type1/series1/introduction-desktop-series1-4-2-3.jpg';
/* mobile */
import mSeries1 from '../../../static/images/introduction/mobile/type1/series1/introduction-mobile-series1-1.jpg';
import mSeries2_1_1 from '../../../static/images/introduction/mobile/type1/series1/introduction-mobile-series1-2-1-1.jpg';
import mSeries2_1_2 from '../../../static/images/introduction/mobile/type1/series1/introduction-mobile-series1-2-1-2.jpg';
import mSeries2_1_3 from '../../../static/images/introduction/mobile/type1/series1/introduction-mobile-series1-2-1-3.jpg';
import mSeries2_2_1 from '../../../static/images/introduction/mobile/type1/series1/introduction-mobile-series1-2-2-1.jpg';
import mSeries2_2_2 from '../../../static/images/introduction/mobile/type1/series1/introduction-mobile-series1-2-2-2.jpg';
import mSeries2_2_3 from '../../../static/images/introduction/mobile/type1/series1/introduction-mobile-series1-2-2-3.jpg';
import mSeries2_3_1 from '../../../static/images/introduction/mobile/type1/series1/introduction-mobile-series1-2-3-1.jpg';
import mSeries2_3_2 from '../../../static/images/introduction/mobile/type1/series1/introduction-mobile-series1-2-3-2.jpg';
import mSeries2_3_3 from '../../../static/images/introduction/mobile/type1/series1/introduction-mobile-series1-2-3-3.jpg';
import mSeries2_4_1 from '../../../static/images/introduction/mobile/type1/series1/introduction-mobile-series1-2-4-1.jpg';
import mSeries2_4_2 from '../../../static/images/introduction/mobile/type1/series1/introduction-mobile-series1-2-4-2.jpg';
import mSeries2_4_3 from '../../../static/images/introduction/mobile/type1/series1/introduction-mobile-series1-2-4-3.jpg';
import mSeries3 from '../../../static/images/introduction/mobile/type1/series1/introduction-mobile-series1-3.jpg';
import mSeries4_1_1 from '../../../static/images/introduction/mobile/type1/series1/introduction-mobile-series1-4-1-1.jpg';
import mSeries4_1_2 from '../../../static/images/introduction/mobile/type1/series1/introduction-mobile-series1-4-1-2.jpg';
import mSeries4_1_3 from '../../../static/images/introduction/mobile/type1/series1/introduction-mobile-series1-4-1-3.jpg';
import mSeries4_2_1 from '../../../static/images/introduction/mobile/type1/series1/introduction-mobile-series1-4-2-1.jpg';
import mSeries4_2_2 from '../../../static/images/introduction/mobile/type1/series1/introduction-mobile-series1-4-2-2.jpg';
import mSeries4_2_3 from '../../../static/images/introduction/mobile/type1/series1/introduction-mobile-series1-4-2-3.jpg';

export const TYPE1_SERIES1_FRAGMENTS = {
  mainBackgroundColor: 'rgb(255, 255, 255)',
  fragment1: {
    desktopImg: series1,
    mobileImg: mSeries1,
  },
  fragment2: {
    title: {
      color: 'rgb(242, 150, 0)',
      name: '凱蒂貓',
    },
    desc: {
      color: 'rgb(0, 0, 0)',
      name: 'KITTY迷注意！artmo出過最多代的當紅明星非KITTY莫屬啦\n還沒收集全款立體包膜的朋友，千萬別錯過呀 ~\n雖然KITTY不會笑，但擁有他的女孩都會大大微笑！\n不管哪個年齡層，都會被收服，得到滿滿幸福感！',
    },
    images: [{
      desc: 'Kitty萌到我完全融化~想直接幫手機包膜包回家\n經典看不膩，買起來收一波！！',
      src1: {
        desktopImg: series2_1_1,
        mobileImg: mSeries2_1_1,
      },
      src2: {
        desktopImg: series2_1_2,
        mobileImg: mSeries2_1_2,
      },
      src3: {
        desktopImg: series2_1_3,
        mobileImg: mSeries2_1_3,
      },
    }, {
      desc: '可愛無比的Hello Kitty無疑是每個女孩心裡溫柔的一塊\n浪漫又療癒的畫面好像把煩惱都趕走了',
      src1: {
        desktopImg: series2_2_1,
        mobileImg: mSeries2_2_1,
      },
      src2: {
        desktopImg: series2_2_2,
        mobileImg: mSeries2_2_2,
      },
      src3: {
        desktopImg: series2_2_3,
        mobileImg: mSeries2_2_3,
      },
    }, {
      desc: '少女心大爆發！好想要擁抱滿滿的Kitty漂浮在粉紅色泡泡中\n啊~~~~實在太幸福啦',
      src1: {
        desktopImg: series2_3_1,
        mobileImg: mSeries2_3_1,
      },
      src2: {
        desktopImg: series2_3_2,
        mobileImg: mSeries2_3_2,
      },
      src3: {
        desktopImg: series2_3_3,
        mobileImg: mSeries2_3_3,
      },
    }, {
      desc: 'Kitty身高是五顆蘋果，萌萌的好可愛\n希望有天能跟他一起吃餅乾、喝下午茶\n享受像公主的每一天!',
      src1: {
        desktopImg: series2_4_1,
        mobileImg: mSeries2_4_1,
      },
      src2: {
        desktopImg: series2_4_2,
        mobileImg: mSeries2_4_2,
      },
      src3: {
        desktopImg: series2_4_3,
        mobileImg: mSeries2_4_3,
      },
    }],
    button: {
      backgroundColor: 'rgb(242, 150, 0)',
      color: 'rgb(255, 255, 255)',
    },
  },
  fragment3: {
    desktopImg: series3,
    mobileImg: mSeries3,
  },
  fragment4: { // same as fragment2
    title: {
      color: 'rgb(242, 150, 0)',
      name: '雙子星&美樂蒂',
    },
    desc: {
      color: 'rgb(0, 0, 0)',
      name: '他們都是KITTY家族可愛的好朋友們，兩位任性又愛撒嬌的雙子星姊弟\nKIKI跟LALA與樂天派的MELODY小白兔繽紛登場！\n',
    },
    images: [{
      desc: 'KikiLala真是療癒單品呀！有了它不用粉嫩濾鏡也覺得整個人粉粉滴好浪漫\n兩姐弟乘著雲朵在花雨之下享受這歡樂時光，下一站會去哪呢？',
      src1: {
        desktopImg: series4_1_1,
        mobileImg: mSeries4_1_1,
      },
      src2: {
        desktopImg: series4_1_2,
        mobileImg: mSeries4_1_2,
      },
      src3: {
        desktopImg: series4_1_3,
        mobileImg: mSeries4_1_3,
      },
    }, {
      desc: '甜美是Melody的代名詞，擁有Melody的女孩絕對是公主！\n甜美系的你輕易駕馭，在一起整個超有愛！',
      src1: {
        desktopImg: series4_2_1,
        mobileImg: mSeries4_2_1,
      },
      src2: {
        desktopImg: series4_2_2,
        mobileImg: mSeries4_2_2,
      },
      src3: {
        desktopImg: series4_2_3,
        mobileImg: mSeries4_2_3,
      },
    }],
    button: {
      backgroundColor: 'rgb(242, 150, 0)',
      color: 'rgb(255, 255, 255)',
    },
  },
};

export default null;
