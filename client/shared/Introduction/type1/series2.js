/* eslint camelcase: 0 */
/* desktop */
import series1 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-1.jpg';
import series2_1_1 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-2-1-1.jpg';
import series2_1_2 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-2-1-2.jpg';
import series2_1_3 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-2-1-3.jpg';
import series2_2_1 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-2-2-1.jpg';
import series2_2_2 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-2-2-2.jpg';
import series2_2_3 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-2-2-3.jpg';
import series2_3_1 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-2-3-1.jpg';
import series2_3_2 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-2-3-2.jpg';
import series2_3_3 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-2-3-3.jpg';
import series3 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-3.jpg';
import series4_1_1 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-4-1-1.jpg';
import series4_1_2 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-4-1-2.jpg';
import series4_1_3 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-4-1-3.jpg';
import series4_2_1 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-4-2-1.jpg';
import series4_2_2 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-4-2-2.jpg';
import series4_2_3 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-4-2-3.jpg';
import series4_3_1 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-4-3-1.jpg';
import series4_3_2 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-4-3-2.jpg';
import series4_3_3 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-4-3-3.jpg';
import series4_4_1 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-4-4-1.jpg';
import series4_4_2 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-4-4-2.jpg';
import series4_4_3 from '../../../static/images/introduction/desktop/type1/series2/introduction-desktop-series2-4-4-3.jpg';
/* mobile */
import mSeries1 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-1.jpg';
import mSeries2_1_1 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-2-1-1.jpg';
import mSeries2_1_2 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-2-1-2.jpg';
import mSeries2_1_3 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-2-1-3.jpg';
import mSeries2_2_1 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-2-2-1.jpg';
import mSeries2_2_2 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-2-2-2.jpg';
import mSeries2_2_3 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-2-2-3.jpg';
import mSeries2_3_1 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-2-3-1.jpg';
import mSeries2_3_2 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-2-3-2.jpg';
import mSeries2_3_3 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-2-3-3.jpg';
import mSeries3 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-3.jpg';
import mSeries4_1_1 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-4-1-1.jpg';
import mSeries4_1_2 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-4-1-2.jpg';
import mSeries4_1_3 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-4-1-3.jpg';
import mSeries4_2_1 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-4-2-1.jpg';
import mSeries4_2_2 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-4-2-2.jpg';
import mSeries4_2_3 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-4-2-3.jpg';
import mSeries4_3_1 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-4-3-1.jpg';
import mSeries4_3_2 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-4-3-2.jpg';
import mSeries4_3_3 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-4-3-3.jpg';
import mSeries4_4_1 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-4-4-1.jpg';
import mSeries4_4_2 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-4-4-2.jpg';
import mSeries4_4_3 from '../../../static/images/introduction/mobile/type1/series2/introduction-mobile-series2-4-4-3.jpg';

export const TYPE1_SERIES2_FRAGMENTS = {
  mainBackgroundColor: 'rgb(0, 0, 0)',
  fragment1: {
    desktopImg: series3,
    mobileImg: mSeries3,
  },
  fragment2: { // same as fragment2
    title: {
      color: 'rgb(242, 150, 0)',
      name: '復仇者聯盟：終局之戰',
    },
    desc: {
      color: 'rgb(255, 255, 255)',
      name: '復仇者聯盟系列故事中英雄們一同經歷了紐約大戰、奧創逆襲、英雄內戰，以及毀滅半數宇宙生命的無限之戰，在最終章，僅存的復仇者們要如何重整旗鼓，背水一戰，為僅存的信念而戰。當英雄齊聚，在他們的眼神中仍看見一線希望。',
    },
    images: [{
      desc: '復仇聯盟者Avengers LOGO本身就是最帥的英雄元素\n保護世界沒有設限，背水一戰的精神令人熱血沸騰！為僅存的信念，戰鬥吧！',
      src1: {
        desktopImg: series4_1_1,
        mobileImg: mSeries4_1_1,
      },
      src2: {
        desktopImg: series4_1_2,
        mobileImg: mSeries4_1_2,
      },
      src3: {
        desktopImg: series4_1_3,
        mobileImg: mSeries4_1_3,
      },
    }, {
      desc: '「我們試著完成不可能的事情，若能保住小命更好。」\n這種熱血，就是我們十年來魂牽夢縈的漫威魂啊',
      src1: {
        desktopImg: series4_2_1,
        mobileImg: mSeries4_2_1,
      },
      src2: {
        desktopImg: series4_2_2,
        mobileImg: mSeries4_2_2,
      },
      src3: {
        desktopImg: series4_2_3,
        mobileImg: mSeries4_2_3,
      },
    }, {
      desc: '灰飛煙毀後的世界該如何走下去，是否還有逆轉結局的機會？\n英雄集結一處，同心合力！深信信念將帶領我們重返榮耀',
      src1: {
        desktopImg: series4_3_1,
        mobileImg: mSeries4_3_1,
      },
      src2: {
        desktopImg: series4_3_2,
        mobileImg: mSeries4_3_2,
      },
      src3: {
        desktopImg: series4_3_3,
        mobileImg: mSeries4_3_3,
      },
    }, {
      desc: '是鷹眼?還是浪人?，在他背後的故事會是我們想像中那個樣子嗎?\n煥然一新帥氣的造型，更讓這位英雄角色更有個性！',
      src1: {
        desktopImg: series4_4_1,
        mobileImg: mSeries4_4_1,
      },
      src2: {
        desktopImg: series4_4_2,
        mobileImg: mSeries4_4_2,
      },
      src3: {
        desktopImg: series4_4_3,
        mobileImg: mSeries4_4_3,
      },
    }],
    button: {
      backgroundColor: 'rgb(242, 150, 0)',
      color: 'rgb(255, 255, 255)',
    },
  },
  fragment3: {
    desktopImg: series1,
    mobileImg: mSeries1,
  },
  fragment4: {
    title: {
      color: 'rgb(242, 150, 0)',
      name: '驚奇隊長',
    },
    desc: {
      color: 'rgb(255, 255, 255)',
      name: '前美國空軍飛行員，現為克里軍隊星際部隊的成員之一。\n在一次事故中卡蘿丹佛斯，在克里星球甦醒並失去了過往所有的記憶，\n也意外獲得了光速飛行、能量吸收、量子光波等超人能力。\n因任務而意外到訪C-53星球的地球，與神盾局長尼克福瑞\n還有超萌的外星貓呆頭鵝，一起聯手尋找羅森博士的下落。',
    },
    images: [{
      desc: '驚奇隊長LOGO經典款，光子衝擊的正義光芒無非是代表！\n漫威十週年一定要走在時尚前端，經典品味沒話說！',
      src1: {
        desktopImg: series2_1_1,
        mobileImg: mSeries2_1_1,
      },
      src2: {
        desktopImg: series2_1_2,
        mobileImg: mSeries2_1_2,
      },
      src3: {
        desktopImg: series2_1_3,
        mobileImg: mSeries2_1_3,
      },
    }, {
      desc: '擁有克里星菁英軍事部隊訓練的卡蘿，原是空軍飛行員，\n在找回記憶後的她，將帶著她的義氣與堅強，面對邪惡勢力！',
      src1: {
        desktopImg: series2_2_1,
        mobileImg: mSeries2_2_1,
      },
      src2: {
        desktopImg: series2_2_2,
        mobileImg: mSeries2_2_2,
      },
      src3: {
        desktopImg: series2_2_3,
        mobileImg: mSeries2_2_3,
      },
    }, {
      desc: '萌死人不償命，卻又能從口中噴出魔性觸角、\n還自帶百寶袋功能的外星貓，呆頭鵝在故事中也是相當重要的角色！',
      src1: {
        desktopImg: series2_3_1,
        mobileImg: mSeries2_3_1,
      },
      src2: {
        desktopImg: series2_3_2,
        mobileImg: mSeries2_3_2,
      },
      src3: {
        desktopImg: series2_3_3,
        mobileImg: mSeries2_3_3,
      },
    }],
    button: {
      backgroundColor: 'rgb(242, 150, 0)',
      color: 'rgb(255, 255, 255)',
    },
  },
};

export default null;
