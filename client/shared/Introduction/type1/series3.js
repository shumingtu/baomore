/* eslint camelcase: 0 */
/* desktop */
import series1 from '../../../static/images/introduction/desktop/type1/series3/introduction-desktop-series3-1.jpg';
import series2_1_1 from '../../../static/images/introduction/desktop/type1/series3/introduction-desktop-series3-2-1-1.jpg';
import series2_1_2 from '../../../static/images/introduction/desktop/type1/series3/introduction-desktop-series3-2-1-2.jpg';
import series2_1_3 from '../../../static/images/introduction/desktop/type1/series3/introduction-desktop-series3-2-1-3.jpg';
import series2_2_1 from '../../../static/images/introduction/desktop/type1/series3/introduction-desktop-series3-2-2-1.jpg';
import series2_2_2 from '../../../static/images/introduction/desktop/type1/series3/introduction-desktop-series3-2-2-2.jpg';
import series2_2_3 from '../../../static/images/introduction/desktop/type1/series3/introduction-desktop-series3-2-2-3.jpg';
import series2_3_1 from '../../../static/images/introduction/desktop/type1/series3/introduction-desktop-series3-2-3-1.jpg';
import series2_3_2 from '../../../static/images/introduction/desktop/type1/series3/introduction-desktop-series3-2-3-2.jpg';
import series2_3_3 from '../../../static/images/introduction/desktop/type1/series3/introduction-desktop-series3-2-3-3.jpg';
import series2_4_1 from '../../../static/images/introduction/desktop/type1/series3/introduction-desktop-series3-2-4-1.jpg';
import series2_4_2 from '../../../static/images/introduction/desktop/type1/series3/introduction-desktop-series3-2-4-2.jpg';
import series2_4_3 from '../../../static/images/introduction/desktop/type1/series3/introduction-desktop-series3-2-4-3.jpg';
import series2_5_1 from '../../../static/images/introduction/desktop/type1/series3/introduction-desktop-series3-2-5-1.jpg';
import series2_5_2 from '../../../static/images/introduction/desktop/type1/series3/introduction-desktop-series3-2-5-2.jpg';
import series2_5_3 from '../../../static/images/introduction/desktop/type1/series3/introduction-desktop-series3-2-5-3.jpg';
/* mobile */
import mSeries1 from '../../../static/images/introduction/mobile/type1/series3/introduction-mobile-series3-1.jpg';
import mSeries2_1_1 from '../../../static/images/introduction/mobile/type1/series3/introduction-mobile-series3-2-1-1.jpg';
import mSeries2_1_2 from '../../../static/images/introduction/mobile/type1/series3/introduction-mobile-series3-2-1-2.jpg';
import mSeries2_1_3 from '../../../static/images/introduction/mobile/type1/series3/introduction-mobile-series3-2-1-3.jpg';
import mSeries2_2_1 from '../../../static/images/introduction/mobile/type1/series3/introduction-mobile-series3-2-2-1.jpg';
import mSeries2_2_2 from '../../../static/images/introduction/mobile/type1/series3/introduction-mobile-series3-2-2-2.jpg';
import mSeries2_2_3 from '../../../static/images/introduction/mobile/type1/series3/introduction-mobile-series3-2-2-3.jpg';
import mSeries2_3_1 from '../../../static/images/introduction/mobile/type1/series3/introduction-mobile-series3-2-3-1.jpg';
import mSeries2_3_2 from '../../../static/images/introduction/mobile/type1/series3/introduction-mobile-series3-2-3-2.jpg';
import mSeries2_3_3 from '../../../static/images/introduction/mobile/type1/series3/introduction-mobile-series3-2-3-3.jpg';
import mSeries2_4_1 from '../../../static/images/introduction/mobile/type1/series3/introduction-mobile-series3-2-4-1.jpg';
import mSeries2_4_2 from '../../../static/images/introduction/mobile/type1/series3/introduction-mobile-series3-2-4-2.jpg';
import mSeries2_4_3 from '../../../static/images/introduction/mobile/type1/series3/introduction-mobile-series3-2-4-3.jpg';
import mSeries2_5_1 from '../../../static/images/introduction/mobile/type1/series3/introduction-mobile-series3-2-5-1.jpg';
import mSeries2_5_2 from '../../../static/images/introduction/mobile/type1/series3/introduction-mobile-series3-2-5-2.jpg';
import mSeries2_5_3 from '../../../static/images/introduction/mobile/type1/series3/introduction-mobile-series3-2-5-3.jpg';

export const TYPE1_SERIES3_FRAGMENTS = {
  mainBackgroundColor: 'rgb(255, 255, 255)',
  fragment1: {
    desktopImg: series1,
    mobileImg: mSeries1,
  },
  fragment2: {
    title: {
      color: 'rgb(242, 150, 0)',
      name: '小熊維尼',
    },
    desc: {
      color: 'rgb(0, 0, 0)',
      name: '1926 年誕生，他是陪伴我們童年一起長大的小熊維尼。\n維尼包膜太犯規了！那顆圓滾滾的肚子做成浮雕叫人怎麼受的了啊？\n每天都拿著維尼滑滑滑，實在太療癒！',
    },
    images: [{
      desc: '看到維尼的萌樣就先融化了！溫暖的色調，讓你隨時記錄著\n維尼與你的幸福日常！天天被維尼可愛的圍繞著。',
      src1: {
        desktopImg: series2_1_1,
        mobileImg: mSeries2_1_1,
      },
      src2: {
        desktopImg: series2_1_2,
        mobileImg: mSeries2_1_2,
      },
      src3: {
        desktopImg: series2_1_3,
        mobileImg: mSeries2_1_3,
      },
    }, {
      desc: '救命啊好萌！我們的心已經被粉嫩維尼燒炸了\n這麼可愛一定要放在心裡收藏，維尼粉千萬不能錯過！',
      src1: {
        desktopImg: series2_2_1,
        mobileImg: mSeries2_2_1,
      },
      src2: {
        desktopImg: series2_2_2,
        mobileImg: mSeries2_2_2,
      },
      src3: {
        desktopImg: series2_2_3,
        mobileImg: mSeries2_2_3,
      },
    }, {
      desc: '黃色與蜂蜜絕對是小熊維尼經典元素\n維尼控出列！一起收服可愛的維尼吧～',
      src1: {
        desktopImg: series2_3_1,
        mobileImg: mSeries2_3_1,
      },
      src2: {
        desktopImg: series2_3_2,
        mobileImg: mSeries2_3_2,
      },
      src3: {
        desktopImg: series2_3_3,
        mobileImg: mSeries2_3_3,
      },
    }, {
      desc: '你有幻想過與小熊維尼一起躺在森林嗎？\n光是想像就覺得幸福又療癒！夢幻場景 夢幻登場！',
      src1: {
        desktopImg: series2_4_1,
        mobileImg: mSeries2_4_1,
      },
      src2: {
        desktopImg: series2_4_2,
        mobileImg: mSeries2_4_2,
      },
      src3: {
        desktopImg: series2_4_3,
        mobileImg: mSeries2_4_3,
      },
    }, {
      desc: '暖色系組成質感滿分的插畫版維尼好Q喔\n維尼與朋友們可愛俏皮的小日常隨時隨地融化你心!',
      src1: {
        desktopImg: series2_5_1,
        mobileImg: mSeries2_5_1,
      },
      src2: {
        desktopImg: series2_5_2,
        mobileImg: mSeries2_5_2,
      },
      src3: {
        desktopImg: series2_5_3,
        mobileImg: mSeries2_5_3,
      },
    }],
    button: {
      backgroundColor: 'rgb(242, 150, 0)',
      color: 'rgb(255, 255, 255)',
    },
  },
};

export default null;
