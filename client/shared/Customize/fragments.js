/* desktop */
// landing
import landing1 from '../../static/images/customize/desktop/customize-desktop-landing-1.jpg';
import landing2 from '../../static/images/customize/desktop/customize-desktop-landing-2.jpg';
import landing3 from '../../static/images/customize/desktop/customize-desktop-landing-3.jpg';
import landingIG1 from '../../static/images/customize/desktop/customize-desktop-landing-4-1.jpg';
import landingIG2 from '../../static/images/customize/desktop/customize-desktop-landing-4-2.jpg';
import landingIG3 from '../../static/images/customize/desktop/customize-desktop-landing-4-3.jpg';
import landingIG4 from '../../static/images/customize/desktop/customize-desktop-landing-4-4.jpg';
import landingIG5 from '../../static/images/customize/desktop/customize-desktop-landing-4-5.jpg';
/* mobile */
// landing
import mLanding1 from '../../static/images/customize/mobile/customize-mobile-landing-1.jpg';
import mLanding2 from '../../static/images/customize/mobile/customize-mobile-landing-2.jpg';
import mLanding3 from '../../static/images/customize/mobile/customize-mobile-landing-3.jpg';
import mLandingIG1 from '../../static/images/customize/mobile/customize-mobile-landing-4-1.jpg';
import mLandingIG2 from '../../static/images/customize/mobile/customize-mobile-landing-4-2.jpg';
import mLandingIG3 from '../../static/images/customize/mobile/customize-mobile-landing-4-3.jpg';
import mLandingIG4 from '../../static/images/customize/mobile/customize-mobile-landing-4-4.jpg';
import mLandingIG5 from '../../static/images/customize/mobile/customize-mobile-landing-4-5.jpg';

export default {
  fragment1: {
    desktopImg: landing1,
    mobileImg: mLanding1,
  },
  fragment2: {
    src: 'https://www.youtube.com/embed/UVsGBCBXXIU',
  },
  fragment3: {
    desktopImg: landing2,
    mobileImg: mLanding2,
  },
  fragment4: {
    desktopImg: landing3,
    mobileImg: mLanding3,
  },
  fragment5: {
    mainImage: {
      desktopImg: landingIG5,
      mobileImg: mLandingIG5,
    },
    groupImages: [{
      desktopImg: landingIG1,
      mobileImg: mLandingIG1,
    }, {
      desktopImg: landingIG2,
      mobileImg: mLandingIG2,
    }, {
      desktopImg: landingIG4,
      mobileImg: mLandingIG4,
    }, {
      desktopImg: landingIG3,
      mobileImg: mLandingIG3,
    }],
  },
};
