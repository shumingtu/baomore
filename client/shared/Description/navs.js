export default [{
  id: 'payment',
  path: '/description/payment',
  name: '付款方式',
}, {
  id: 'transport',
  path: '/description/transport',
  name: '運送方式',
}, {
  id: 'return',
  path: '/description/return',
  name: '退換貨方式',
}];
