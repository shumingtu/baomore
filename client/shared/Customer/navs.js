export default [{
  id: 'qa',
  path: '/customer/qa',
  name: '常見問題',
}, {
  id: 'rights',
  path: '/customer/rights',
  name: '會員權益說明',
}, {
  id: 'disclaim',
  path: '/customer/disclaim',
  name: '個人化使用及免責條款',
}];
