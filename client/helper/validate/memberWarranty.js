export function validateMemberWarranty(data) {
  if (!data) return null;

  const {
    service,
    brandId,
    brandModelName,
    storeId,
    storeName,
    VAT,
    IME,
    phone,
    memberId,
  } = data;

  const errors = {};

  if (!service) errors.service = 'required';
  if (!brandId) errors.brandId = 'required';
  if (!brandModelName) errors.brandModelName = 'required';
  if (!storeId) errors.storeId = 'required';
  if (!storeName) errors.storeId = 'required';
  if (!VAT) errors.VAT = 'required';
  if (!IME) errors.IME = 'required';
  if (!service) errors.service = 'required';
  if (!phone) errors.phone = 'required';
  if (!memberId) errors.memberId = 'required';

  if (brandId === '-1') errors.brandId = 'required';
  if (brandModelName === '-1') errors.brandModelName = 'required';
  if (storeId === '-1') errors.storeId = 'required';
  if (memberId === '-1') errors.memberId = 'required';
  if (service === '-1') errors.service = 'required';

  if (Object.keys(errors).length === 0) return null;

  return errors;
}

export default null;
