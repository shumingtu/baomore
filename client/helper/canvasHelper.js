import {
  CANVAS_WIDTH,
  CANVAS_HEIGHT,
  IMAGE_HEIGHT_SPEC,
  IMAGE_WIDTH_SPEC,
} from '../shared/PhoneConfig.js';
import {
  NONE_BORDER,
} from '../shared/Global.js';

export function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {
  /* in fit ratio */
  // const ratio = Math.min(Math.min(maxWidth / srcWidth, maxHeight / srcHeight), 1);
  //
  // return { width: srcWidth * ratio, height: srcHeight * ratio };

  /* in 100% height ratio */
  const ratio = maxHeight / srcHeight;

  return { width: srcWidth * ratio, height: maxHeight };
}

export function getTouchDistance(p1, p2) {
  return Math.sqrt(((p2.x - p1.x) ** 2) + ((p2.y - p1.y) ** 2));
}

export function getCanvasHeight(width) {
  if (!width) return 0;

  return Math.ceil(width * (IMAGE_HEIGHT_SPEC / IMAGE_WIDTH_SPEC));
}

export function isMobileMode() {
  const width = window.innerWidth
    || document.documentElement.clientWidth
    || document.body.clientWidth;

  if (width < 768) return true;

  return false;
}

export function isBase64(value) {
  if (value) {
    return value.startsWith('data:image', 0);
  }

  return false;
}

export function canvasJsonToString(json) {
  if (!json) return null;

  const stringifyJson = JSON.stringify(json);

  return stringifyJson;
}

export function canvasStringToJson(string) {
  if (!string) return {};

  const json = JSON.parse(string);

  return json;
}

export function canvasImageAttrBinder(src) {
  if (!src) return null;

  const positionInit = {
    x: CANVAS_WIDTH / 2,
    y: CANVAS_HEIGHT / 2,
  };

  return {
    id: Math.random(),
    type: 'image',
    src,
    x: positionInit.x,
    y: positionInit.y,
    scale: 1,
    rotate: 0,
  };
}

export function canvasTextAttrBinder({
  text,
  fontFamily,
  fontWeight,
  color,
  strokeType = NONE_BORDER,
  strokeColor,
}) {
  if (!text || !fontFamily || !fontWeight || !color) return null;

  const wrapColor = (color && color.color) || null;
  const wrapStrokeColor = (strokeColor && strokeColor.color) || null;
  const positionInit = {
    x: (CANVAS_WIDTH / 3),
    y: (CANVAS_HEIGHT / 2) - 16,
  };

  return {
    id: Math.random(),
    type: 'text',
    text,
    fontSize: 32,
    fontFamily,
    fontWeight,
    color: wrapColor,
    strokeType,
    strokeColor: strokeType === NONE_BORDER ? null : wrapStrokeColor,
    x: positionInit.x,
    y: positionInit.y,
    scale: 1,
    rotate: 0,
  };
}

export function canvasEmojiAttrBinder(src) {
  if (!src) return null;
  // const emojiMockSize = { width: 64, height: 64 }; // using offset, so remove mock size adjust
  const positionInit = {
    x: CANVAS_WIDTH / 2,
    y: CANVAS_HEIGHT / 2,
  };

  return {
    id: Math.random(), // emoji id will deprecated
    type: 'emoji',
    src,
    x: positionInit.x,
    y: positionInit.y,
    scale: 1,
    rotate: 0,
  };
}

export function canvasFixedImageAttrBinder({
  id,
  src,
}) {
  if (!src || !id) return null;

  const positionInit = {
    x: CANVAS_WIDTH / 2,
    y: CANVAS_HEIGHT / 2,
  };

  return {
    type: 'fixed',
    id,
    src,
    x: positionInit.x,
    y: positionInit.y,
    rotate: 0,
  };
}

export function checkCustomizeIsDone(images) {
  if (!images) return false;

  if (Array.isArray(images)) {
    if (images.length) {
      const hasBase64Image = images.filter(i => isBase64(i.src)).length;

      if (hasBase64Image > 0) return false;

      return true;
    }

    return false;
  }

  if (typeof images === 'object') {
    // is object
    if (Object.keys(images).length) return true;

    return false;
  }

  return false;
}

export function checkPayCheckIsDone(images, checkInfo) {
  if (!images || !checkInfo) return false;

  const customizeIsDone = checkCustomizeIsDone(images);

  if (customizeIsDone) {
    const {
      cityId,
      storeId,
      date,
      time,
      name,
      email,
      phone,
    } = checkInfo;

    if (!cityId
      || !storeId
      || !date
      || !time
      || time === '-1'
      || !name
      || !email
      || !phone
    ) {
      return false;
    }

    return true;
  }

  return false;
}

/* currently unused */
export function imageResolver(imageBundle) {
  if (!imageBundle) return null;
  if (!imageBundle.src) return null;

  const {
    src,
    x,
    y,
  } = imageBundle;

  return new Promise((resolve) => {
    const img = new Image();
    img.onload = () => {
      const {
        width,
        height,
      } = calculateAspectRatioFit(
        img.width,
        img.height,
        CANVAS_WIDTH,
        CANVAS_HEIGHT,
      );

      resolve({
        img,
        x,
        y,
        width,
        height,
        status: 'ok',
      });
    };

    img.onerror = () => resolve({
      img,
      x,
      y,
      width: 0,
      height: 0,
      status: 'ok',
    });

    img.src = src;
  });
}

export default null;
