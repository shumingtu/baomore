import { store } from '../global.js';
import {
  IMAGE_WIDTH_SPEC,
  IMAGE_HEIGHT_SPEC,
} from '../shared/PhoneConfig.js';

function upload(file) {
  return new Promise(async (resolve, reject) => {
    const fd = new FormData();

    fd.append('file', file);

    try {
      const {
        url,
      } = await fetch(`${API_HOST}/upload`, {
        method: 'POST',
        body: fd,
      }).then(res => res.json());

      resolve(url);
    } catch (ex) {
      reject(ex);
    }
  });
}

export async function uploadFile(file, cb, imageOnly) {
  if (imageOnly && !(/\.(jpe?g|png|gif)$/i.test(file.name))) {
    return;
  }

  // preview section
  const reader = new FileReader();

  reader.addEventListener('load', () => cb({
    src: reader.result,
    isPreview: true,
  }), false);

  if (file) {
    reader.readAsDataURL(file);
  }
  // upload section
  const fileURL = await upload(file);

  cb({
    src: fileURL,
    isPreview: false,
  });
}

function getZoomedSize() {
  const clipBundle = store.getState().Customize.clipBundle || {};
  const zoomRatioInHeight = IMAGE_HEIGHT_SPEC / clipBundle.height; // height ratio

  const baseOnHeightIsOverflow = clipBundle.width * zoomRatioInHeight > IMAGE_WIDTH_SPEC;
  const baseOnHeight = {
    width: Math.ceil(clipBundle.width * zoomRatioInHeight),
    height: IMAGE_HEIGHT_SPEC,
    x: (clipBundle.x * zoomRatioInHeight) || 0,
    y: clipBundle.y || 0,
  };
  const zoomRatioInWidth = IMAGE_WIDTH_SPEC / clipBundle.width; // width ratio

  const baseOnWidth = {
    width: IMAGE_WIDTH_SPEC,
    height: clipBundle.height * zoomRatioInWidth,
    x: clipBundle.x || 0,
    y: (clipBundle.y * zoomRatioInWidth) || 0,
  };

  const currentSizeBundle = !baseOnHeightIsOverflow ? baseOnHeight : baseOnWidth;

  return {
    ...currentSizeBundle,
  };
}

export async function uploadCanvas(file, cb) {
  const reader = new FileReader();

  reader.addEventListener('load', () => {
    const img = new Image();
    img.src = reader.result;

    img.onload = () => {
      const currentSizeBundle = getZoomedSize();
      const canvas = document.createElement('canvas');
      canvas.width = IMAGE_WIDTH_SPEC;
      canvas.height = IMAGE_HEIGHT_SPEC;
      const ctx = canvas.getContext('2d');

      ctx.drawImage(
        img,
        currentSizeBundle.x,
        currentSizeBundle.y,
        currentSizeBundle.width,
        currentSizeBundle.height
      );

      ctx.canvas.toBlob(async (blob) => {
        const fileURL = await upload(blob);

        cb({
          src: fileURL,
          isPreview: false,
        });
      }, 'image/png');
    };
  }, false);

  if (file) {
    reader.readAsDataURL(file);
  }
}

export default uploadFile;
