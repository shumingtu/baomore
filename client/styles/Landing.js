export const BLACK_COLOR = 'rgb(114, 113, 113)';
export const PURPLE_COLOR = 'rgb(174, 0, 130)';
export const YELLOW_COLOR = 'rgb(243, 152, 0)';

export default {
  navText: {
    fontSize: 16,
    fontWeight: 400,
  },
  mainTitle: {
    fontSize: 46,
    fontWeight: 700,
    letterSpacing: 1.04,
  },
  subTitle: {
    fontSize: 16,
    fontWeight: 400,
    letterSpacing: 2,
  },
  entryBtnText: {
    fontSize: 16,
    letterSpacing: 2,
  },
};
