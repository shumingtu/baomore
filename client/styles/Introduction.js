export const PURPLE_COLOR = 'rgb(173, 0, 129)';
export const YELLOW_COLOR = 'rgb(242, 150, 0)';
export const GRAY_COLOR = 'rgb(114, 113, 113)';
export const BLACK_COLOR = 'rgb(0, 0, 0)';
export const RED_COLOR = 'rgb(195, 13, 35)';
