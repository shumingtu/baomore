import gql from 'graphql-tag';

export const GET_MEMBER_ME = gql`
  query me {
    me {
      id
      name
      avatar
      phone
      email
      gender
      birthday
      storedDesignJSON
    }
  }
`;

export const GET_MY_ORDERS = gql`
  query myOrderList {
    myOrderList {
      id
      picture
      name
      phone
      email
      date
      time
      price
      store {
        id
        name
        phone
        address
      }
      employee {
        id
        name
      }
      createdAt
      payedStatus
      shippingStatus
    }
  }
`;

export const GET_MY_WARRANTIES = gql`
  query myWarranties($limit: Int, $offset: Int) {
    myWarranties(
      limit: $limit
      offset: $offset
    ) {
      id
      service
      phoneModel
      store
      VAT
      IME
      phoneLastSix
      isApproved
      employeeCode
      createdAt
    }
  }
`;

export const GET_EMPLOYEE_LIST = gql`
  query employeeMemberlist(
    $storeId: Int
    $memberId: Int
    $areaId: Int
    $keyword: String
    $isForClient: Boolean
  ) {
    employeeMemberlist(
      storeId: $storeId
      memberId: $memberId
      areaId: $areaId
      keyword: $keyword
      isForClient: $isForClient
    ) {
      id
      serialNumber
      name
      phone
      email
      area {
        id
        name
      }
    }
  }
`;

export default null;
