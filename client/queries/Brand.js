import gql from 'graphql-tag';

export const FETCH_BRAND_LIST = gql`
  query brandList {
    brandList {
      id
      name
    }
  }
`;

export const FETCH_BRAND_MODEL_LIST = gql`
  query brandModelList($brandId: Int!) {
    brandModelList(
      brandId: $brandId
    ) {
      id
      name

    }
  }
`;

export const FETCH_MODEL_COLOR_LIST = gql`
  query brandModelColorList($modelId: Int!) {
    brandModelColorList(
      modelId: $modelId
    ) {
      id
      name
      phoneBg,
      phoneMask
      phoneCover
    }
  }
`;
