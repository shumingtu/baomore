import gql from 'graphql-tag';

export const FETCH_SOCIAL_ACTIVITY_CATEGORIES = gql`
  query socialCategories($id: Int) {
    socialCategories(id: $id) {
      id
      name
      link
    }
  }
`;

export const GET_ACTIVITY_CATEGORIES = gql`
  query activityCategories {
    activityCategories {
      id
      name
      desktopImg
      mobileImg
    }
  }
`;

export const GET_ACTIVITY_LIST = gql`
  query activities($categoryId: Int!, $status: Boolean) {
    activities (
      categoryId: $categoryId
      status: $status
    ) {
      id
      name
      desktopImg
      mobileImg
      description
    }
  }
`;

export const GET_ACTIVITY_DETAIL = gql`
  query activity($activityId: Int!) {
    activity(
      activityId: $activityId
    ) {
      id
      name
      desktopImg
      mobileImg
      fragments {
        id
        type
        title
        content
      }
    }
  }
`;
