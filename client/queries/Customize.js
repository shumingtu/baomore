import gql from 'graphql-tag';

export const GET_CUSTOMIZE_MAIN_CATEGORIES = gql`
  query subCategoriesForSale {
    subCategoriesForSale {
      id
      name
      picture
    }
  }
`;

export const GET_CUSTOMIZE_CATEGORY_SUBCATEGORIES = gql`
  query subCategoriesBySubCategory($id: Int!, $isOnSale: Boolean) {
    subCategoriesBySubCategory(
      id: $id
      isOnSale: $isOnSale
    ) {
      id
      name
    }
  }
`;

export const GET_SUBCATEGORY_PNTABLE_LIST = gql`
  query pntablelistBySubcategory($subCategoryId: Int!) {
    pntablelistBySubcategory(
      subCategoryId: $subCategoryId
    ) {
      id
      code
      name
      picture
      suitableDevice
      description
      isOnSale
      onlinePrice
    }
  }
`;

export const GET_EMOJI_CATEGORIES = gql`
  query emojiCategories {
    emojiCategories {
      id
      name
    }
  }
`;

export const GET_EMOJI_LIST = gql`
  query emoji($categoryId: Int) {
    emoji(
      categoryId: $categoryId
    ) {
      id
      picture
    }
  }
`;

export const GET_CITY_DISTRICTS = gql`
  query cityDistricts {
    cityDistricts {
      id
      name
      latitude
      longitude
      disticts {
        id
        name
      }
    }
  }
`;

export const GET_CHANNELS = gql`
  query channelList {
    channelList {
      id
      name
    }
  }
`;

export const GET_STORES = gql`
  query stores(
    $cityId: Int
    $districtId: Int
    $storeId: Int
    $channelId: Int
    $isForClient: Boolean,
    $isForCustomize: Boolean,
  ) {
    stores(
      cityId: $cityId
      districtId: $districtId
      storeId: $storeId
      channelId: $channelId
      isForClient: $isForClient
      isForCustomize: $isForCustomize
    ) {
      id
      name
      phone
      address
      latitude
      longitude
      area {
        id
        name
      }
    }
  }
`;

export const GET_ALREADY_BOOKED_DATES = gql`
  query alreadyBookedDates($storeId: Int!, $categoryName: String) {
    alreadyBookedDates(
      storeId: $storeId
      categoryName: $categoryName
    ) {
      date
    }
  }
`;

export const GET_AVAILABLE_BOOK_TIMES = gql`
  query availableBookedTimes($storeId: Int!, $date: String!) {
    availableBookedTimes(
      storeId: $storeId
      date: $date
    ) {
      time
    }
  }
`;

export const GET_CUSTOMIZE_PNTABLE_INFO = gql`
  query customPNTable {
    customPNTable {
      id
      code
      name
      description
      onlinePrice
    }
  }
`;
