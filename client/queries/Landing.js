import gql from 'graphql-tag';

export const GET_LANDING_BANNERS = gql`
  query banners {
    banners {
      id
      picture
      link
      mobileImg
    }
  }
`;

export const GET_LANDINGS = gql`
  query landings {
    landings {
      id
      desktopImg
      mobileImg
      link
    }
  }
`;
