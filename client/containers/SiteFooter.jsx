// @flow
import React from 'react';
import radium from 'radium';

// components
import FooterNavs from '../components/FooterNavs.jsx';
// theme
import Theme from '../styles/Theme.js';
// static
import fbIcon from '../static/images/artmo-icon-fb.png';
import lineIcon from '../static/images/artmo-icon-line@.png';
import igIcon from '../static/images/artmo-icon-ig.png';
import youtubeIcon from '../static/images/artmo-icon-youtube.png';
import logoFooter from '../static/images/landing/logo-footer.png';
import {
  SITE_FOOTER_NAVS,
} from '../shared/Global.js';

const styles = {
  wrapper: {
    width: '100%',
    maxWidth: 1200,
    height: 'auto',
    padding: '24px 0 12px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hideInMobile: {
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
  mainWrapper: {
    width: '100%',
    height: 'auto',
    flex: 1,
    padding: '0 0 12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 1023px)': {
      justifyContent: 'center',
    },
  },
  footerLogo: {
    width: 80,
    height: 80,
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundImage: `url(${logoFooter})`,
    '@media (max-width: 1023px)': {
      width: 60,
      height: 60,
    },
  },
  contactWrapper: {
    width: 220,
    height: '100%',
    padding: '0 16px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  contactLabel: {
    fontSize: 14,
    color: Theme.SITE_FOOTER_COPYRIGHT_COLOR,
    lineHeight: 1.6,
    paddingLeft: 6,
    letterSpacing: 2,
    '@media (max-width: 1023px)': {
      display: 'none',
    },
  },
  shareWrapper: {
    width: '100%',
    height: 40,
    marginTop: 12,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  itemWrap: {
    width: 'auto',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
    margin: '0 5px',
    border: 0,
    backgroundColor: 'transparent',
    textDecoration: 'none',
  },
  itemIcon: {
    width: 40,
    height: 40,
    '@media (max-width: 767px)': {
      width: 31.5,
      height: 31.5,
    },
  },
  navWrapper: {
    flex: 1,
    width: 'auto',
    height: '100%',
    padding: '0 12px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 1023px)': {
      display: 'none',
    },
  },
  copyrightWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  onlyDesktop: {
    '@media (max-width: 1023px)': {
      display: 'none',
    },
  },
  copyright: {
    fontSize: 12,
    color: Theme.SITE_FOOTER_COPYRIGHT_COLOR,
    lineHeight: 1.83,
    letterSpacing: 1,
  },
};

type Props = {
  hide: Boolean,
  hideInMobile: Boolean,
};

function SiteFooter({
  hide,
  hideInMobile,
}: Props) {
  if (hide) return null;

  return (
    <div style={[styles.wrapper, hideInMobile && styles.hideInMobile]}>
      <div style={styles.mainWrapper}>
        <div style={styles.footerLogo} />
        <div style={styles.contactWrapper}>
          <span style={styles.contactLabel}>
            關注我們
          </span>
          <div style={styles.shareWrapper}>
            <a
              href="https://line.me/R/ti/p/%40gwk6718t"
              target="_blank"
              rel="noopener noreferrer"
              style={styles.itemWrap}>
              <img alt="LINE" src={lineIcon} style={styles.itemIcon} />
            </a>
            <a
              href="https://www.facebook.com/artmoinc/"
              target="_blank"
              rel="noopener noreferrer"
              style={styles.itemWrap}>
              <img alt="FB" src={fbIcon} style={styles.itemIcon} />
            </a>
            <a
              href="https://www.instagram.com/artmoinc/"
              target="_blank"
              rel="noopener noreferrer"
              style={styles.itemWrap}>
              <img alt="IG" src={igIcon} style={styles.itemIcon} />
            </a>
            <a
              href="https://www.youtube.com/channel/UC8ygda_uIC5qpfz_K2XVNRw"
              target="_blank"
              rel="noopener noreferrer"
              style={styles.itemWrap}>
              <img alt="YOUTUBE" src={youtubeIcon} style={styles.itemIcon} />
            </a>
          </div>
        </div>
        <div style={styles.navWrapper}>
          {SITE_FOOTER_NAVS.map(nav => (
            <FooterNavs
              key={nav.title}
              nav={nav} />
          ))}
        </div>
      </div>
      <div style={styles.copyrightWrapper}>
        <span style={[styles.copyright, styles.onlyDesktop]}>
          本網站瀏覽以 Google Chrome、Firefox 或 IE11 以上版本及較新版本支瀏覽器效果最佳。
        </span>
        <span style={styles.copyright}>
          Copyright artmo,2018,All Rights Reserved.
        </span>
      </div>
    </div>
  );
}

export default radium(SiteFooter);
