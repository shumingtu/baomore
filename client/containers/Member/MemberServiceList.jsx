// @flow
import React from 'react';
import radium from 'radium';
import {
  Link as link,
} from 'react-router-dom';

import Theme from '../../styles/Theme.js';

const Link = radium(link);

const styles = {
  wrapper: {
    width: '100%',
    maxWidth: 480,
    padding: 12,
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  item: {
    width: '100%',
    height: 'auto',
    margin: '12px 0',
    padding: '12px 24px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
    borderRadius: 16,
    border: '1px solid rgba(0, 0, 0, 0.4)',
    outline: 0,
    cursor: 'pointer',
    textDecoration: 'none',
    ':hover': {
      opacity: 0.8,
    },
    '@media (max-width: 767px)': {
      flexDirection: 'column',
    },
  },
  itemLabel: {
    fontSize: 18,
    fontWeight: 400,
    letterSpacing: 1.5,
    color: Theme.BLACK_COLOR,
  },
};

type Props = {
  list: Array<{
    id: number,
    createdAt: string,
  }>,
  match: {
    url: string,
  },
};

function MemberServiceList({
  list = [],
  match: {
    url,
  },
}: Props) {
  return (
    <div style={styles.wrapper}>
      {list.map(l => (
        <Link
          key={l.id}
          to={{ pathname: `${url}/${l.id}` }}
          style={styles.item}>
          <span style={styles.itemLabel}>
            包膜產品售後服務卡：
          </span>
          <span style={styles.itemLabel}>
            {l.createdAt ? `${l.createdAt}` : null}
          </span>
        </Link>
      ))}
    </div>
  );
}

export default radium(MemberServiceList);
