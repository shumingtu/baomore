// @flow
/* eslint prefer-spread: 0 react/no-did-update-set-state: 0 */
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Mutation } from 'react-apollo';
import moment from 'moment';
import {
  reduxForm,
  initialize,
  Field,
  formValueSelector,
  change,
  SubmissionError,
} from 'redux-form';
import qs from 'qs';
// config
import Theme from '../../styles/Theme.js';
import { MEMBER_EDIT_FORM } from '../../shared/Form.js';
import * as MemberActions from '../../actions/Member.js';
import { EDIT_MEMBER_ME } from '../../mutations/Member.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import Input from '../../components/Form/Input.jsx';
import Selector from '../../components/Form/Selector.jsx';
import MemberRuleAgreement from '../../components/Member/MemberRuleAgreement.jsx';
import NoDataPlaceholder from '../../components/Member/NoDataPlaceholder.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  formWrapper: {
    width: '100%',
    maxWidth: 600,
    padding: '0 12px',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
  },
  groupWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
      padding: 0,
    },
  },
  groupFieldWrapper: {
    flex: 1,
    height: 'auto',
    padding: '0 4px 0 0',
    '@media (max-width: 767px)': {
      width: '100%',
      padding: '4px 0',
    },
  },
  input: {
    padding: '12px 16px',
    borderRadius: 16,
    borderBottom: '1px solid #999999',
    borderLeft: '1px solid #999999',
    borderTop: '1px solid #999999',
    borderRight: '1px solid #999999',
  },
  inputError: {
    borderBottom: '1px solid rgb(238, 96, 82)',
    borderLeft: '1px solid rgb(238, 96, 82)',
    borderTop: '1px solid rgb(238, 96, 82)',
    borderRight: '1px solid rgb(238, 96, 82)',
  },
  button: {
    width: 'auto',
    height: 'auto',
    margin: '12px 0',
    padding: '6px 12px',
    borderRadius: 12,
    border: `1px solid ${Theme.PURPLE_COLOR}`,
    backgroundColor: 'transparent',
    fontSize: 15,
    fontWeight: 400,
    color: Theme.PURPLE_COLOR,
    letterSpacing: 1,
    textAlign: 'center',
    outline: 0,
    cursor: 'pointer',
    ':hover': {
      opacity: 0.8,
    },
  },
  disabled: {
    opacity: 0.4,
  },
};

type Props = {
  initializeForm: Function,
  accessToken: string,
  handleSubmit: Function,
  yearString: string,
  monthString: string,
  memberName: string,
  memberEmail: string,
  memberPhone: string,
  memberBirthday: string,
  memberGender: 'male' | 'female',
  history: {
    push: Function,
  },
  location: {
    search: string,
  },
  resetDay: Function,
  cacheMemberInfo: Function,
};

type State = {
  daysList: Array<{
    id: string,
    name: number,
  }>,
};

const selector = formValueSelector(MEMBER_EDIT_FORM);
const yearsLength = moment().diff('1911-01-01', 'years');
const years = Array.apply(null, { length: yearsLength }).map((_, idx) => {
  const currentYear = moment().year();

  return ({
    id: (currentYear - idx).toString(),
    name: currentYear - idx,
  });
});
const months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(v => ({
  id: v.toString(),
  name: v,
}));

class MemberEditPage extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      daysList: [],
    };
  }

  static getDerivedStateFromProps({
    yearString,
    monthString,
  }, state) {
    if (!yearString || !monthString || yearString === '-1' || monthString === '-1') {
      return {
        ...state,
        daysList: [],
      };
    }

    return state;
  }

  componentDidMount() {
    const {
      memberName,
      memberEmail,
      memberPhone,
      memberGender,
      memberBirthday,
    } = this.props;

    this.initializing({
      name: memberName,
      email: memberEmail,
      phone: memberPhone,
      gender: memberGender,
      birthday: memberBirthday,
    });
  }

  componentDidUpdate(prevProps) {
    const {
      yearString,
      monthString,
      resetDay,
      memberName,
      memberEmail,
      memberPhone,
      memberGender,
      memberBirthday,
    } = this.props;

    if (memberName !== prevProps.memberName && memberName) {
      this.initializing({
        name: memberName,
        email: memberEmail,
        phone: memberPhone,
        gender: memberGender,
        birthday: memberBirthday,
      });
    }

    if ((yearString !== prevProps.yearString && yearString === '-1')
    || (monthString !== prevProps.monthString && monthString === '-1')
    ) {
      resetDay();
    }

    if (yearString && monthString && yearString !== '-1' && monthString !== '-1') {
      if (yearString !== prevProps.yearString || monthString !== prevProps.monthString) {
        const daysLength = moment(`${yearString}-${monthString}`, 'YYYY-MM').daysInMonth();
        const days = Array.apply(null, { length: daysLength }).map((_, idx) => ({
          id: (idx + 1).toString(),
          name: (idx + 1),
        }));

        this.setState({
          daysList: days,
        });
      }
    }
  }

  getEditPayload(d) {
    const {
      email,
      name,
      gender,
      phone,
      year,
      month,
      day,
    } = d;

    let date = null;

    if (year && year !== '-1'
      && month && month !== '-1'
      && day && day !== '-1'
    ) {
      const wrapMonth = parseInt(month, 10) < 10 ? `0${month}` : month;
      const wrapDay = parseInt(day, 10) < 10 ? `0${day}` : day;
      date = `${year}-${wrapMonth}-${wrapDay}`;
    }

    const payload = {
      email: email || null,
      name: name || null,
      gender: gender && gender !== '-1' ? gender : null,
      birthday: date || null,
      phone: phone || null,
    };

    return payload;
  }

  initializing(obj: {
    email?: string,
    name?: string,
    gender?: string,
    birthday?: string,
    phone?: string,
  }) {
    const {
      initializeForm,
    } = this.props;

    let year;
    let month;
    let day;

    if (obj && obj.birthday) {
      const splitDate = obj.birthday.split('-').filter(d => d);
      year = splitDate[0] ? `${parseInt(splitDate[0], 10)}` : null;
      month = splitDate[1] ? `${parseInt(splitDate[1], 10)}` : null;
      day = splitDate[2] ? `${parseInt(splitDate[2], 10)}` : null;
    }

    return initializeForm({
      email: (obj && obj.email) || '',
      name: (obj && obj.name) || '',
      gender: (obj && obj.gender) || '-1',
      year: year || '-1',
      month: month || '-1',
      day: day || '-1',
      phone: (obj && obj.phone) || '',
    });
  }

  render() {
    const {
      handleSubmit,
      accessToken,
      history,
      cacheMemberInfo,
      location: {
        search,
      },
    } = this.props;

    const {
      daysList,
    } = this.state;

    const qsString = search ? qs.parse(search, { ignoreQueryPrefix: true }) : {};
    const isNewMember = (qsString.new && qsString.new === 'true') || false;

    return (
      <div style={styles.wrapper}>
        <PageTitle
          title={isNewMember ? '成為 artmo 會員' : '修改會員資料'} />
        {!accessToken ? (
          <NoDataPlaceholder type="noToken" />
        ) : (
          <Mutation mutation={EDIT_MEMBER_ME}>
            {(editMe, { loading }) => (
              <form
                style={styles.formWrapper}
                onSubmit={handleSubmit(async (d) => {
                  if (isNewMember) {
                    const {
                      isAgree,
                    } = d;

                    if (!isAgree) {
                      throw new SubmissionError({
                        isAgree: 'Required',
                      });
                    }
                  }

                  const payload = this.getEditPayload(d);

                  const {
                    data,
                  } = await editMe({
                    variables: payload,
                  });

                  if (data && data.editMe && data.editMe.id) {
                    cacheMemberInfo(payload);

                    history.push('/member');
                  }
                })}>
                <div style={styles.fieldWrapper}>
                  <Field
                    type="email"
                    name="email"
                    placeholder="電子信箱"
                    component={Input}
                    style={styles.input}
                    errorStyle={styles.inputError} />
                </div>
                <div style={styles.fieldWrapper}>
                  <Field
                    name="name"
                    placeholder="姓名"
                    component={Input}
                    style={styles.input}
                    errorStyle={styles.inputError} />
                </div>
                <div style={styles.fieldWrapper}>
                  <Field
                    name="gender"
                    placeholder="性別"
                    options={[{
                      id: 'male',
                      name: '男',
                    }, {
                      id: 'female',
                      name: '女',
                    }]}
                    component={Selector}
                    customStyle={styles.input} />
                </div>
                <div style={styles.groupWrapper}>
                  <div style={styles.groupFieldWrapper}>
                    <Field
                      name="year"
                      placeholder="生日/西元年"
                      options={years}
                      component={Selector}
                      customStyle={styles.input} />
                  </div>
                  <div style={styles.groupFieldWrapper}>
                    <Field
                      name="month"
                      placeholder="月份"
                      options={months}
                      component={Selector}
                      customStyle={styles.input} />
                  </div>
                  <div style={styles.groupFieldWrapper}>
                    <Field
                      name="day"
                      placeholder="日期"
                      options={daysList}
                      component={Selector}
                      customStyle={styles.input} />
                  </div>
                </div>
                <div style={styles.fieldWrapper}>
                  <Field
                    type="tel"
                    name="phone"
                    placeholder="手機號碼"
                    component={Input}
                    style={styles.input}
                    errorStyle={styles.inputError} />
                </div>
                {isNewMember ? (
                  <div style={styles.fieldWrapper}>
                    <MemberRuleAgreement />
                  </div>
                ) : null}
                <button
                  type="submit"
                  disabled={loading}
                  style={[
                    styles.button,
                    loading && styles.disabled,
                  ]}>
                  確認送出
                </button>
              </form>
            )}
          </Mutation>
        )}
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  if (!values.name) {
    errors.name = 'Required';
  }

  return errors;
}

const formHook = reduxForm({
  form: MEMBER_EDIT_FORM,
  validate,
});

const reduxHook = connect(
  state => ({
    accessToken: state.Member.accessToken,
    memberName: state.Member.memberName,
    memberEmail: state.Member.memberEmail,
    memberPhone: state.Member.memberPhone,
    memberGender: state.Member.memberGender,
    memberBirthday: state.Member.memberBirthday,
    yearString: selector(state, 'year'),
    monthString: selector(state, 'month'),
  }),
  dispatch => bindActionCreators({
    ...MemberActions,
    initializeForm: v => initialize(MEMBER_EDIT_FORM, v),
    resetDay: () => change(MEMBER_EDIT_FORM, 'day', '-1'),
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      MemberEditPage
    )
  )
);
