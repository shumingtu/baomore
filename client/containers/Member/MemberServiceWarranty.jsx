// @flow
import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { Query } from 'react-apollo';

import { GET_MY_WARRANTIES } from '../../queries/Member.js';
// components
import NoDataPlaceholder from '../../components/Member/NoDataPlaceholder.jsx';
import MemberServiceList from './MemberServiceList.jsx';
import MemberServiceDetail from './MemberServiceDetail.jsx';

type Props = {
  match: {
    url: string,
  },
};

class MemberServiceWarranty extends PureComponent<Props> {
  placeholderRenderer(myWarranties) {
    if (myWarranties && myWarranties.length) {
      return null;
    }

    return (
      <NoDataPlaceholder type="noService" />
    );
  }

  render() {
    const {
      match: {
        url,
      },
    } = this.props;

    return (
      <Query query={GET_MY_WARRANTIES}>
        {({
          data,
        }) => {
          const myWarranties = (data && data.myWarranties) || [];

          return (
            <Fragment>
              {this.placeholderRenderer(myWarranties) || (
                <Switch>
                  {myWarranties.map(l => (
                    <Route
                      key={`${l.id}-detail`}
                      path={`${url}/${l.id}`}
                      component={props => (
                        <MemberServiceDetail
                          {...props}
                          service={l} />
                      )} />
                  ))}
                  <Route
                    path={url}
                    component={props => (
                      <MemberServiceList
                        {...props}
                        list={myWarranties} />
                    )} />
                </Switch>
              )}
            </Fragment>
          );
        }}
      </Query>
    );
  }
}

export default radium(MemberServiceWarranty);
