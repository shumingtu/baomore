// @flow
import React, { memo } from 'react';
import radium from 'radium';
import {
  Link as link,
} from 'react-router-dom';
// config
import Theme from '../../styles/Theme.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';

const Link = radium(link);

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  navsPlacement: {
    width: '100%',
    maxWidth: 860,
    height: 'auto',
    padding: '24px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
    },
  },
  navWrapper: {
    width: 160,
    height: 160,
    padding: 12,
    margin: 12,
    border: 0,
    borderRadius: 12,
    backgroundColor: '#fff',
    boxShadow: Theme.CLIENT_BLOCK_SHADOW,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    textDecoration: 'none',
    cursor: 'pointer',
    ':hover': {
      opacity: 0.9,
    },
  },
  navIconPlacement: {
    width: '100%',
    height: 'auto',
    flex: 2,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  navIcon: {
    width: 80,
    height: 80,
  },
  navLabelPlacement: {
    width: '100%',
    height: 'auto',
    flex: 1,
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  navLabel: {
    fontSize: 18,
    fontWeight: 700,
    letterSpacing: 2,
    paddingLeft: 4,
    color: Theme.BLACK_COLOR,
    textAlign: 'center',
  },
};

type Props = {
  navs: Array<{
    id: string,
    name: string,
    icon: string,
    path: string,
  }>,
};

function MemberLanding({
  navs,
}: Props) {
  return (
    <div style={styles.wrapper}>
      <PageTitle
        title={[
          'artmo',
          '會員中心',
        ]} />
      <div style={styles.navsPlacement}>
        {navs.map(nav => (
          <Link
            key={nav.id}
            to={{ pathname: nav.path }}
            style={styles.navWrapper}>
            <div style={styles.navIconPlacement}>
              <img
                alt="icon"
                src={nav.icon || ''}
                style={styles.navIcon} />
            </div>
            <div style={styles.navLabelPlacement}>
              <span style={styles.navLabel}>
                {nav.name || null}
              </span>
            </div>
          </Link>
        ))}
      </div>
    </div>
  );
}

export default memo(
  radium(
    MemberLanding
  )
);
