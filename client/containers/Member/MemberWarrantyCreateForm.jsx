// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Mutation } from 'react-apollo';
import {
  reduxForm,
  initialize,
  Field,
  Fields,
  SubmissionError,
} from 'redux-form';
// config
import Theme from '../../styles/Theme.js';
import { WARRANTY_CREATE_FORM } from '../../shared/Form.js';
import * as MemberActions from '../../actions/Member.js';
import { GET_MY_WARRANTIES } from '../../queries/Member.js';
import { ADD_WARRANTY_RECORD } from '../../mutations/Member.js';
import {
  WARRANTY_SERVICE_LIST,
} from '../../shared/Global.js';
import { validateMemberWarranty } from '../../helper/validate/memberWarranty.js';
// components
import Input from '../../components/Form/Input.jsx';
import Selector from '../../components/Form/Selector.jsx';
import StoreEmployeeSelectorBind from '../Form/StoreEmployeeSelectorBind.jsx';
import BrandModelSelectorBind from '../Form/BrandModelSelectorBind.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  formWrapper: {
    width: '100%',
    maxWidth: 600,
    padding: '0 12px',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  fieldWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
  },
  groupWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
      padding: 0,
    },
  },
  groupFieldWrapper: {
    flex: 1,
    height: 'auto',
    padding: '0 4px 0 0',
    '@media (max-width: 767px)': {
      width: '100%',
      padding: '4px 0',
    },
  },
  input: {
    padding: '12px 16px',
    borderRadius: 16,
    borderBottom: '1px solid #999999',
    borderLeft: '1px solid #999999',
    borderTop: '1px solid #999999',
    borderRight: '1px solid #999999',
  },
  inputError: {
    borderBottom: '1px solid rgb(238, 96, 82)',
    borderLeft: '1px solid rgb(238, 96, 82)',
    borderTop: '1px solid rgb(238, 96, 82)',
    borderRight: '1px solid rgb(238, 96, 82)',
  },
  button: {
    width: 'auto',
    height: 'auto',
    margin: '12px 0',
    padding: '6px 12px',
    borderRadius: 12,
    border: `1px solid ${Theme.PURPLE_COLOR}`,
    backgroundColor: 'transparent',
    fontSize: 15,
    fontWeight: 400,
    color: Theme.PURPLE_COLOR,
    letterSpacing: 1,
    textAlign: 'center',
    outline: 0,
    cursor: 'pointer',
    ':hover': {
      opacity: 0.8,
    },
  },
  disabled: {
    opacity: 0.4,
  },
  hintWrapper: {
    width: '100%',
    height: 'auto',
    padding: '4px 24px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  hint: {
    fontSize: 13,
    fontWeight: 400,
    color: '#9b9b9b',
  },
};

type Props = {
  initializeForm: Function,
  accessToken: string,
  handleSubmit: Function,
  memberPhone: string,
  history: {
    push: Function,
    replace: Function,
  },
};

class MemberWarrantyCreateForm extends PureComponent<Props> {
  componentDidMount() {
    const {
      memberPhone,
      accessToken,
      history,
    } = this.props;

    if (!accessToken) {
      history.replace('/member/service');
    } else {
      this.initializing({
        phone: memberPhone,
      });
    }
  }

  getEditPayload(d) {
    const {
      service,
      brandModelName,
      storeName,
      VAT,
      IME,
      phone,
      memberId,
    } = d;

    const errors = validateMemberWarranty(d);

    if (errors) {
      throw new SubmissionError(errors);
    }

    return {
      service,
      phoneModel: brandModelName,
      store: storeName,
      VAT,
      IME,
      phone,
      EmployeeMemberId: parseInt(memberId, 10),
    };
  }

  initializing(obj: {
    phone?: string,
  }) {
    const {
      initializeForm,
    } = this.props;

    return initializeForm({
      service: '-1',
      brandModelName: '',
      storeId: '-1',
      storeName: null,
      memberId: '-1',
      VAT: '',
      IME: '',
      phone: (obj && obj.phone) || '',
    });
  }

  render() {
    const {
      handleSubmit,
      history,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <Mutation
          mutation={ADD_WARRANTY_RECORD}
          update={(store, { data: { addWarrantyRecord } }) => {
            const data = store.readQuery({
              query: GET_MY_WARRANTIES,
            });

            const {
              myWarranties,
            } = data;

            if (addWarrantyRecord) {
              store.writeQuery({
                query: GET_MY_WARRANTIES,
                data: {
                  myWarranties: [
                    ...myWarranties,
                    {
                      ...addWarrantyRecord,
                    },
                  ],
                },
              });
            }
          }}>
          {(addWarrantyRecord, { loading }) => (
            <form
              style={styles.formWrapper}
              onSubmit={handleSubmit(async (d) => {
                const payload = this.getEditPayload(d);

                const {
                  data,
                } = await addWarrantyRecord({
                  variables: payload,
                });

                if (data && data.addWarrantyRecord && data.addWarrantyRecord.id) {
                  history.push('/member/service');
                }
              })}>
              <div style={styles.fieldWrapper}>
                <Field
                  name="service"
                  placeholder="服務項目"
                  options={WARRANTY_SERVICE_LIST}
                  component={Selector}
                  customStyle={styles.input} />
              </div>
              <div style={styles.groupWrapper}>
                <Fields
                  names={[
                    'brandId',
                    'brandModelName',
                  ]}
                  component={BrandModelSelectorBind}
                  style={styles.input} />
              </div>
              <div style={styles.fieldWrapper}>
                <Field
                  name="VAT"
                  placeholder="統一發票"
                  component={Input}
                  style={styles.input}
                  errorStyle={styles.inputError} />
              </div>
              <div style={styles.fieldWrapper}>
                <Field
                  name="IME"
                  placeholder="IME後五碼"
                  component={Input}
                  style={styles.input}
                  errorStyle={styles.inputError} />
              </div>
              <div style={styles.hintWrapper}>
                <span style={styles.hint}>
                  ※手機輸入 *#06# 查詢IME碼
                </span>
              </div>
              <div style={styles.fieldWrapper}>
                <Field
                  type="tel"
                  name="phone"
                  placeholder="手機號碼"
                  component={Input}
                  style={styles.input}
                  errorStyle={styles.inputError} />
              </div>
              <div style={styles.groupWrapper}>
                <Fields
                  names={[
                    'storeId',
                    'storeName',
                    'memberId',
                    'areaId',
                  ]}
                  component={StoreEmployeeSelectorBind}
                  style={styles.input} />
              </div>
              <button
                type="submit"
                disabled={loading}
                style={[
                  styles.button,
                  loading && styles.disabled,
                ]}>
                確認送出
              </button>
            </form>
          )}
        </Mutation>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: WARRANTY_CREATE_FORM,
});

const reduxHook = connect(
  state => ({
    accessToken: state.Member.accessToken,
    memberPhone: state.Member.memberPhone,
  }),
  dispatch => bindActionCreators({
    ...MemberActions,
    initializeForm: v => initialize(WARRANTY_CREATE_FORM, v),
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      MemberWarrantyCreateForm
    )
  )
);
