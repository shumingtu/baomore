// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
// config
import Theme from '../../styles/Theme.js';
import { LINE_LOGIN_PATH } from '../../shared/Global.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  lineLogin: {
    width: 'auto',
    height: 'auto',
    padding: 12,
    border: `1px solid ${Theme.PURPLE_COLOR}`,
    borderRadius: 12,
    fontSize: 16,
    fontWeight: 400,
    letterSpacing: 1,
    color: Theme.PURPLE_COLOR,
    textAlign: 'center',
    backgroundColor: 'transparent',
    textDecoration: 'none',
    outline: 0,
    cursor: 'pointer',
    ':hover': {
      opacity: 0.9,
    },
  },
};

type Props = {
  accessToken: string,
  history: {
    replace: Function,
  },
};

class MemberLoginPage extends PureComponent<Props> {
  componentDidMount() {
    const {
      accessToken,
      history,
    } = this.props;

    if (accessToken) {
      history.replace('/member');
    }
  }

  render() {
    return (
      <div style={styles.wrapper}>
        <PageTitle title="artmo註冊" />
        <a
          target="_self"
          href={LINE_LOGIN_PATH}
          style={styles.lineLogin}>
          使用Line＠登入
        </a>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    accessToken: state.Member.accessToken,
  }),
);

export default reduxHook(
  radium(
    MemberLoginPage
  )
);
