// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

// config
import Theme from '../../styles/Theme.js';
import { SERVICE_INFO } from '../../shared/Member/fragments.js';
// components
import ServiceCard from '../../components/Member/ServiceCard.jsx';
import BlockTitle from '../../components/Description/BlockTitle.jsx';

const styles = {
  wrapper: {
    width: '100%',
    maxWidth: 768,
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      padding: '0 12px',
    },
  },
  activeDateWrapper: {
    width: '100%',
    height: 'auto',
    padding: '48px 12px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  activeDate: {
    fontSize: 16,
    fontWeight: 400,
    color: Theme.BLACK_COLOR,
    letterSpacing: 1,
  },
  categoryWrapper: {
    width: '100%',
    height: 'auto',
    marginTop: 24,
    borderTop: `1px solid ${Theme.BLACK_COLOR}`,
    padding: '6px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  descWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 6px',
  },
  desc: {
    fontSize: 16,
    fontWeight: 400,
    color: Theme.BLACK_COLOR,
    letterSpacing: 1,
    lineHeight: 1.6,
    whiteSpace: 'pre-line',
  },
};

type Props = {
  service: {
    id: number,
    name: string,
    phoneType: string,
    ime: string,
    phoneNumber: string,
    store: {
      id: number,
      name: string,
    },
    manager: string,
    createdAt: string,
  },
};

class MemberServiceDetail extends PureComponent<Props> {
  render() {
    const {
      service,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <ServiceCard info={service} />
        <div style={styles.activeDateWrapper}>
          <span style={styles.activeDate}>
            {`消費日期(登入日期)：${service.createdAt}`}
          </span>
        </div>
        {SERVICE_INFO.map(info => (
          <div key={info.title} style={styles.categoryWrapper}>
            <BlockTitle title={`【${info.title}】`} />
            <div style={styles.descWrapper}>
              <span style={styles.desc}>
                {info.desc}
              </span>
            </div>
          </div>
        ))}
      </div>
    );
  }
}

export default radium(MemberServiceDetail);
