// @flow
import { PureComponent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as MemberActions from '../../actions/Member.js';

type Props = {
  clearMemberInfo: Function,
  history: {
    goBack: Function,
  },
};

class MemberLogoutPage extends PureComponent<Props> {
  componentDidMount() {
    const {
      clearMemberInfo,
      history,
    } = this.props;

    clearMemberInfo();
    history.goBack();
  }

  render() {
    return null;
  }
}

const reduxHook = connect(
  () => ({}),
  dispatch => bindActionCreators({
    ...MemberActions,
  }, dispatch)
);

export default reduxHook(MemberLogoutPage);
