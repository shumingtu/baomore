// @flow
import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import {
  Switch,
  Route,
} from 'react-router-dom';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import NoDataPlaceholder from '../../components/Member/NoDataPlaceholder.jsx';
import MemberServiceWarranty from './MemberServiceWarranty.jsx';
import MemberWarrantyCreateForm from './MemberWarrantyCreateForm.jsx';
import Button from '../../components/Global/Button.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonWrapper: {
    width: 200,
    height: 35,
  },
};

type Props = {
  match: {
    url: string,
  },
  history: {
    push: Function,
  },
  accessToken: string,
};

class MemberServiceListMainPage extends PureComponent<Props> {
  placeholderRenderer() {
    const {
      accessToken,
    } = this.props;

    if (!accessToken) {
      return (
        <NoDataPlaceholder type="noToken" />
      );
    }

    return null;
  }

  render() {
    const {
      match: {
        url,
      },
      history,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <PageTitle
          title={[
            'artmo',
            '售後服務',
          ]} />
        {this.placeholderRenderer() || (
          <Fragment>
            <Switch>
              <Route
                exact
                path={url}
                component={() => (
                  <div style={styles.functionWrapper}>
                    <div style={styles.buttonWrapper}>
                      <Button
                        withShadow
                        type="button"
                        label="新增保卡"
                        onClick={() => history.push(`${url}/create`)} />
                    </div>
                  </div>
                )} />
            </Switch>
            <Switch>
              <Route
                path={`${url}/create`}
                component={MemberWarrantyCreateForm} />
              <Route
                path={url}
                component={MemberServiceWarranty} />
            </Switch>
          </Fragment>
        )}
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    accessToken: state.Member.accessToken,
  }),
);

export default reduxHook(
  radium(
    MemberServiceListMainPage
  )
);
