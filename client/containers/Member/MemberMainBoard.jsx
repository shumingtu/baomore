// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { graphql } from 'react-apollo';
// static
import {
  LOGIN_NAV,
  LOGOUT_NAV,
  MEMBER_NAVS,
} from '../../shared/Member/navs.js';
import {
  GET_MEMBER_ME,
} from '../../queries/Member.js';
import * as MemberActions from '../../actions/Member.js';
// components
import SubNavBar from '../../components/Global/SubNavBar.jsx';
import MemberLanding from './MemberLanding.jsx';
import MemberLoginPage from './MemberLoginPage.jsx';
import MemberEditPage from './MemberEditPage.jsx';
import MemberServiceListMainPage from './MemberServiceListMainPage.jsx';
import MemberOrderPage from './MemberOrderPage.jsx';
import MemberLogoutPage from './MemberLogoutPage.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  mainPlacement: {
    width: '100%',
    height: 'auto',
    paddingBottom: 64,
  },
};

type Props = {
  match: {
    url: string,
  },
  accessToken: string,
  me: {
    id: number,
    name: string,
    phone: string,
    email: string,
    gender: string,
  },
  cacheMemberInfo: Function,
};

class MemberMainBoard extends PureComponent<Props> {
  componentDidMount() {
    const {
      me,
      cacheMemberInfo,
    } = this.props;

    if (me) {
      cacheMemberInfo(me);
    }
  }

  componentDidUpdate(prevProps) {
    const {
      me,
      cacheMemberInfo,
    } = this.props;

    if (me !== prevProps.me && me) {
      cacheMemberInfo(me);
    }
  }

  render() {
    const {
      match: {
        url,
      },
      accessToken,
    } = this.props;

    const currentNavs = accessToken
      ? LOGOUT_NAV.concat(MEMBER_NAVS)
      : LOGIN_NAV.concat(MEMBER_NAVS);

    return (
      <div style={styles.wrapper}>
        <SubNavBar
          title={{
            name: '會員中心',
            path: url,
          }}
          navs={currentNavs} />
        <div style={styles.mainPlacement}>
          <Switch>
            <Route
              path={`${url}/logout`}
              component={MemberLogoutPage} />
            <Route
              path={`${url}/order`}
              component={MemberOrderPage} />
            <Route
              path={`${url}/service`}
              component={MemberServiceListMainPage} />
            <Route
              path={`${url}/edit`}
              component={MemberEditPage} />
            <Route
              path={`${url}/login`}
              component={MemberLoginPage} />
            <Route
              path={url}
              component={props => (
                <MemberLanding
                  {...props}
                  navs={MEMBER_NAVS} />
              )} />
          </Switch>
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    accessToken: state.Member.accessToken,
  }),
  dispatch => bindActionCreators({
    ...MemberActions,
  }, dispatch)
);

const queryHook = graphql(GET_MEMBER_ME, {
  options: ({
    accessToken,
  }) => ({
    skip: !accessToken,
  }),
  props: ({
    data,
  }) => ({
    me: (data && data.me) || null,
  }),
});

export default reduxHook(
  queryHook(
    radium(
      MemberMainBoard
    )
  )
);
