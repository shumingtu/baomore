// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';
// config
import Theme from '../../styles/Theme.js';
import { GET_MY_ORDERS } from '../../queries/Member.js';
// components
import PageTitle from '../../components/Global/PageTitle.jsx';
import NoDataPlaceholder from '../../components/Member/NoDataPlaceholder.jsx';
import MemberOrderItem from '../../components/Member/MemberOrderItem.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  listWrapper: {
    width: '100%',
    maxWidth: 768,
    padding: 12,
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  thankyou: {
    fontSize: 16,
    fontWeight: 700,
    color: Theme.PURPLE_COLOR,
    letterSpacing: 1.5,
    padding: '0 0 12px 0',
  },
};

type Props = {
  accessToken: string,
};

class MemberOrderPage extends PureComponent<Props> {
  placeholderRenderer(orders) {
    const {
      accessToken,
    } = this.props;

    if (!accessToken) {
      return (
        <NoDataPlaceholder type="noToken" />
      );
    }

    if (orders && orders.length) {
      return null;
    }

    return (
      <NoDataPlaceholder type="noOrder" />
    );
  }

  render() {
    return (
      <Query
        query={GET_MY_ORDERS}
        fetchPolicy="cache-and-network">
        {({
          data,
        }) => {
          const myOrderList = (data && data.myOrderList) || [];

          return (
            <div style={styles.wrapper}>
              <PageTitle title="訂單查詢" />
              {this.placeholderRenderer(myOrderList) || (
                <div style={styles.listWrapper}>
                  <span style={styles.thankyou}>
                    感謝您的訂購！
                  </span>
                  {myOrderList.map(order => (
                    <MemberOrderItem
                      key={order.id}
                      order={order} />
                  ))}
                </div>
              )}
            </div>
          );
        }}
      </Query>
    );
  }
}

const reduxHook = connect(
  state => ({
    accessToken: state.Member.accessToken,
  }),
);

export default reduxHook(
  radium(
    MemberOrderPage
  )
);
