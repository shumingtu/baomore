// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';
import { Link as link } from 'react-router-dom';
// config
import {
  YELLOW_COLOR,
  PURPLE_COLOR,
} from '../../styles/Landing.js';
import {
  GET_LANDINGS,
  GET_LANDING_BANNERS,
} from '../../queries/Landing.js';
// components
import LinkImage from '../../components/Global/LinkImage.jsx';
import FrameHeavyLine from '../../components/Global/FrameHeavyLine.jsx';
import ImageSlider from '../../components/Global/ImageSlider.jsx';
import ResizableImage from '../../components/Fragment/ResizableImage.jsx';
import FloatingButtons from '../../components/Global/FloatingButtons.jsx';
// imgs
import landing4 from '../../static/images/landing/landing-4.jpg';
import landing5 from '../../static/images/landing/landing-5.jpg';
import landing6 from '../../static/images/landing/landing-6.jpg';
// mobile imgs
import mLanding4 from '../../static/images/landing/mobile-landing-4.jpg';
import mLanding5 from '../../static/images/landing/mobile-landing-5.jpg';
import mLanding6 from '../../static/images/landing/mobile-landing-6.jpg';

const Link = radium(link);

const styles = {
  wrapper: {
    width: '100vw',
    height: '100%',
  },
  sliderWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  slidePlacement: {
    width: '100%',
    height: 'auto',
    borderTop: `1px solid ${YELLOW_COLOR}`,
    borderBottom: `1px solid ${PURPLE_COLOR}`,
    position: 'relative',
    overflow: 'hidden',
    '@media (max-width: 1023px)': {
      height: 'auto',
    },
  },
  imageWrapper: {
    width: '100%',
    height: 'auto',
    margin: '12px 0',
    textDecoration: 'none',
    border: 0,
    backgroundColor: 'transparent',
    outline: 0,
  },
  imgGroupWrapper: {
    width: '100%',
    height: 'auto',
    margin: '12px 0',
    borderTop: `2px solid ${YELLOW_COLOR}`,
    borderBottom: `2px solid ${YELLOW_COLOR}`,
    padding: '24px 12px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  placement: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
    },
  },
  groupImg: {
    flex: 1,
    height: 'auto',
    padding: '0 4px',
    '@media (max-width: 767px)': {
      width: '100%',
    },
  },
};

type Props = {

};

class Landing extends PureComponent<Props> {
  constructor(props) {
    super(props);

    this.groupRef = React.createRef();
  }

  groupRef: { current: null | HTMLDivElement }

  render() {
    return (
      <div style={styles.wrapper}>
        <FloatingButtons />
        <div style={styles.sliderWrapper}>
          <div style={styles.slidePlacement}>
            <FrameHeavyLine
              type="leftTop"
              color={YELLOW_COLOR} />
            <Query query={GET_LANDING_BANNERS}>
              {({
                data,
              }) => {
                const banners = (data && data.banners) || [];

                return (
                  <ImageSlider
                    clickable
                    useScrollInMobile
                    images={banners} />
                );
              }}
            </Query>
            <FrameHeavyLine
              type="rightBottom"
              color={PURPLE_COLOR} />
          </div>
        </div>
        <Query query={GET_LANDINGS}>
          {({
            data,
          }) => {
            const landings = (data && data.landings) || [];

            return landings.map((l) => {
              if (l.link) {
                return (
                  <Link
                    key={l.id}
                    to={{ pathname: l.link }}
                    style={[
                      styles.imageWrapper,
                      {
                        cursor: 'pointer',
                      },
                    ]}>
                    <ResizableImage
                      desktopRatio={0.40905}
                      mobileRatio={1.46384}
                      desktopImg={l.desktopImg || null}
                      mobileImg={l.mobileImg || null} />
                  </Link>
                );
              }

              return (
                <div key={l.id} style={styles.imageWrapper}>
                  <ResizableImage
                    desktopRatio={0.40905}
                    mobileRatio={1.46384}
                    desktopImg={l.desktopImg || null}
                    mobileImg={l.mobileImg || null} />
                </div>
              );
            });
          }}
        </Query>
      </div>
    );
  }
}

export default radium(Landing);
