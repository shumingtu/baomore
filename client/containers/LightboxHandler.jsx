// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Mutation } from 'react-apollo';
// config
import Theme from '../styles/Theme.js';
import {
  STORE_MAP_LIGHTBOX,
  LINE_PAY_LIGHTBOX,
  LINE_PAY_SUCCESS_LIGHTBOX,
  LINE_PAY_FAILED_LIGHTBOX,
  LINE_LOGIN_LIGHTBOX,
  CUSTOMIZE_REMINDER_LIGHTBOX,
  CUSTOMIZE_PUBLIC_LIGHTBOX,
} from '../shared/LightboxTypes.js';
import {
  CONFIRM_LINEPAY_PAYMENT,
} from '../mutations/Payment.js';
// actions
import * as LightboxActions from '../actions/Lightbox.js';
// components
import StoreSelectionMap from '../components/Lightbox/StoreSelectionMap.jsx';
import LinePaySuccess from '../components/Lightbox/LinePaySuccess.jsx';
import LinePayFailed from '../components/Lightbox/LinePayFailed.jsx';
import LineLogin from '../components/Lightbox/LineLogin.jsx';
import LinePayBox from '../components/Lightbox/LinePayBox.jsx';
import CustomizeReminder from '../components/Lightbox/CustomizeReminder.jsx';
import PublicReminder from '../components/Lightbox/PublicReminder.jsx';

const styles = {
  wrapper: {
    position: 'fixed',
    left: 0,
    top: 0,
    zIndex: 99999,
    width: '100vw',
    height: '100vh',
    backgroundColor: 'rgba(230, 230, 230, 0.7)',
  },
  placement: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    '@media (max-width: 767px)': {
      alignItems: 'flex-start',
      paddingTop: '10%',
    },
  },
  placementPadding: {
    paddingLeft: 30,
    paddingRight: 30,
  },
  mainLightbox: {
    width: 'auto',
    height: 'auto',
    padding: 16,
    borderRadius: 8,
    boxShadow: Theme.BLOCK_SHADOW,
    backgroundColor: '#fff',
    position: 'relative',
    zIndex: 2,
  },
  clickableBackground: {
    position: 'absolute',
    zIndex: 1,
    left: 0,
    top: 0,
    width: '100vw',
    height: '100vh',
    outline: 0,
    border: 0,
    backgroundColor: 'transparent',
  },
};

type Props = {
  currentLightbox: {
    type: string,
    orderId?: string,
    transactionId?: string,
    errorCode?: string,
    backgroundClickableDisabled?: boolean,
  },
  closeLightbox: Function,
};

class LightboxHandler extends PureComponent<Props> {
  lightboxRenderer() {
    const {
      currentLightbox,
      closeLightbox,
    } = this.props;

    switch (currentLightbox.type) {
      case STORE_MAP_LIGHTBOX:
        return (
          <StoreSelectionMap
            {...currentLightbox}
            onClose={() => closeLightbox()} />
        );
      case LINE_PAY_SUCCESS_LIGHTBOX:
        return (
          <Mutation mutation={CONFIRM_LINEPAY_PAYMENT}>
            {payConfirm => (
              <LinePaySuccess
                {...currentLightbox}
                payConfirm={async () => {
                  if (currentLightbox.orderId && currentLightbox.transactionId) {
                    await payConfirm({
                      variables: {
                        orderId: currentLightbox.orderId,
                        transactionId: currentLightbox.transactionId,
                      },
                    });
                  }
                }}
                onClose={() => closeLightbox()} />
            )}
          </Mutation>
        );
      case LINE_PAY_FAILED_LIGHTBOX:
        return (
          <LinePayFailed
            {...currentLightbox}
            errorCode={currentLightbox.errorCode || null}
            onClose={() => closeLightbox()} />
        );
      case LINE_LOGIN_LIGHTBOX:
        return (
          <LineLogin
            {...currentLightbox}
            onClose={() => closeLightbox()} />
        );
      case LINE_PAY_LIGHTBOX:
        return (
          <LinePayBox
            {...currentLightbox}
            onClose={() => closeLightbox()} />
        );
      case CUSTOMIZE_REMINDER_LIGHTBOX:
        return (
          <CustomizeReminder
            {...currentLightbox}
            onClose={() => closeLightbox()} />
        );
      case CUSTOMIZE_PUBLIC_LIGHTBOX:
        return (
          <PublicReminder
            {...currentLightbox}
            onClose={() => closeLightbox()} />
        );
      default:
        return null;
    }
  }

  render() {
    const {
      currentLightbox,
      closeLightbox,
    } = this.props;

    if (!currentLightbox) return null;

    const placementStyle = () => {
      switch (currentLightbox.type) {
        case CUSTOMIZE_REMINDER_LIGHTBOX:
        case CUSTOMIZE_PUBLIC_LIGHTBOX:
          return styles.placementPadding;
        default:
          return null;
      }
    };

    return (
      <div style={styles.wrapper}>
        <div
          style={[
            styles.placement,
            placementStyle(),
          ]}>
          <button
            key="close-lightbox-bg"
            type="button"
            onClick={() => {
              if (currentLightbox.backgroundClickableDisabled) {
                return;
              }

              closeLightbox();
            }}
            style={styles.clickableBackground} />
          <div style={styles.mainLightbox}>
            {this.lightboxRenderer()}
          </div>
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    currentLightbox: state.Lightbox.currentLightbox,
  }),
  dispatch => bindActionCreators({
    ...LightboxActions,
  }, dispatch)
);

export default reduxHook(
  radium(
    LightboxHandler
  )
);
