// @flow
import React from 'react';
import radium from 'radium';
// config
import Theme from '../styles/Theme.js';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '0 48px 48px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      padding: '0 12px',
    },
  },
  titleWrapper: {
    width: '100%',
    height: 'auto',
    padding: '36px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
    },
  },
  title: {
    fontSize: 32,
    fontWeight: 700,
    color: Theme.BLACK_COLOR,
    letterSpacing: 3,
    textAlign: 'center',
    '@media (max-width: 767px)': {
      fontSize: 26,
    },
  },
  contentWrapper: {
    width: '100%',
    height: 'auto',
    padding: 36,
    backgroundColor: '#fff',
    borderRadius: 12,
    boxShadow: Theme.CLIENT_BLOCK_SHADOW,
  },
  content: {
    fontSize: 16,
    fontWeight: 400,
    lineHeight: 1.6,
    letterSpacing: 1,
    color: Theme.BLACK_COLOR,
    whiteSpace: 'pre-line',
    wordBreak: 'break-all',
  },
};

function RulePage({
  titles,
  rule,
}: {
  titles: Array<string>,
  rule: string,
}) {
  return (
    <div style={styles.wrapper}>
      <div style={styles.titleWrapper}>
        {Array.isArray(titles) ? titles.map(t => (
          <span key={t} style={styles.title}>
            {t}
          </span>
        )) : null}
      </div>
      <div style={styles.contentWrapper}>
        <span style={styles.content}>
          {rule || null}
        </span>
      </div>
    </div>
  );
}

export default radium(RulePage);
