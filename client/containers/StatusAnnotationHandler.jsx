// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
// config
import Theme from '../styles/Theme.js';
import {
  STATUS_SUCCESS,
  STATUS_FAILED,
} from '../shared/StatusTypes.js';
// actions
import * as LightboxActions from '../actions/Lightbox.js';

const styles = {
  wrapper: {
    position: 'fixed',
    right: -110,
    top: 60,
    zIndex: 99999,
    width: 'auto',
    minWidth: 100,
    height: 'auto',
    minHeight: 30,
    padding: 6,
    borderRadius: 3,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    boxShadow: Theme.BLOCK_SHADOW,
    transition: 'right 0.2s ease-in',
  },
  show: {
    right: 20,
    top: 60,
  },
  success: {
    backgroundColor: '#4BB543',
  },
  failed: {
    backgroundColor: '#FF0000',
  },
  annotation: {
    fontSize: 12,
    letterSpacing: 1,
    color: 'rgb(4, 4, 4)',
  },
};

type Props = {
  currentStatusBox: {
    type: string,
    label?: string,
  },
  closeStatusBox: Function,
};

class StatusAnnotationHandler extends PureComponent<Props> {
  componentDidMount() {
    const {
      currentStatusBox,
      closeStatusBox,
    } = this.props;

    if (currentStatusBox) {
      closeStatusBox();
    }
  }

  componentDidUpdate(prevProps) {
    const {
      currentStatusBox,
      closeStatusBox,
    } = this.props;

    if (currentStatusBox !== prevProps.currentStatusBox && currentStatusBox) {
      setTimeout(() => closeStatusBox(), 3000);
    }
  }

  annotationRenderer() {
    const {
      currentStatusBox,
    } = this.props;

    if (currentStatusBox) {
      switch (currentStatusBox.type) {
        case STATUS_SUCCESS:
          return ({
            style: styles.success,
            label: currentStatusBox.label || 'Success',
          });
        case STATUS_FAILED:
          return ({
            style: styles.failed,
            label: currentStatusBox.label || 'Failed',
          });
        default:
          return {};
      }
    }

    return {};
  }

  render() {
    const {
      currentStatusBox,
    } = this.props;

    const wrapBundle = this.annotationRenderer();

    return (
      <div
        style={[
          styles.wrapper,
          wrapBundle.style || null,
          currentStatusBox && styles.show,
        ]}>
        <span style={styles.annotation}>
          {wrapBundle.label || null}
        </span>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    currentStatusBox: state.Lightbox.currentStatusBox,
  }),
  dispatch => bindActionCreators({
    ...LightboxActions,
  }, dispatch)
);

export default reduxHook(
  radium(
    StatusAnnotationHandler
  )
);
