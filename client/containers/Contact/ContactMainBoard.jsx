// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

// config
import Theme from '../../styles/Theme.js';
import placeIcon from '../../static/images/contact/contact-place-icon.png';
import bookingIcon from '../../static/images/contact/contact-booking-icon.png';
// components
import FrameHeavyLine from '../../components/Global/FrameHeavyLine.jsx';
import FrameThinLine from '../../components/Global/FrameThinLine.jsx';
import ContactCategory from '../../components/Contact/ContactCategory.jsx';
import ContactPopup from '../../components/Contact/ContactPopup.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  sloganWrapper: {
    width: '100%',
    height: 'auto',
    padding: '32px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
    position: 'relative',
    '@media (max-width: 767px)': {
      padding: '32px 12px',
      margin: 0,
    },
  },
  slogan: {
    fontSize: 32,
    fontWeight: 700,
    letterSpacing: 4,
    textAlign: 'center',
    color: Theme.BLACK_COLOR,
    whiteSpace: 'pre-line',
    '@media (max-width: 767px)': {
      fontSize: 26,
    },
  },
  mainWrapper: {
    width: '100%',
    height: 'auto',
    padding: 64,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    position: 'relative',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
      padding: '32px 12px',
    },
  },
};

type Props = {

};

type State = {
  openBookingModal: boolean,
};

class ContactMainBoard extends PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      openBookingModal: false,
    };
  }

  render() {
    const categories = [{
      icon: placeIcon,
      title: '服務據點',
      desc: 'artmo 於北中南區皆有門市據點，查詢與您鄰近的門市，讓artmo服務您。',
      button: {
        isLink: true,
        path: '/contact/stores',
        name: '查詢據點',
      },
    }, {
      icon: bookingIcon,
      title: '線上預約',
      desc: '您也可以在線上與artmo溝通，歡迎加入LINE＠，不論您身在何處，歡迎聯繫我們，我們將為您服務。',
      button: {
        isLink: false,
        onClick: () => this.setState({ openBookingModal: true }),
        name: '加入LINE＠',
      },
    }];

    const {
      openBookingModal,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        <div style={styles.sloganWrapper}>
          <FrameThinLine
            type="leftTop"
            color={Theme.YELLOW_COLOR} />
          <FrameHeavyLine
            type="leftTop"
            color={Theme.YELLOW_COLOR} />
          <span style={styles.slogan}>
            artmo 時刻守護你
          </span>
          <FrameThinLine
            type="rightBottom"
            color={Theme.PURPLE_COLOR} />
          <FrameHeavyLine
            type="rightBottom"
            color={Theme.PURPLE_COLOR} />
        </div>
        <div style={styles.mainWrapper}>
          {categories.map(c => (
            <ContactCategory
              key={c.title}
              category={c} />
          ))}
          {openBookingModal ? (
            <ContactPopup
              onClose={() => this.setState({ openBookingModal: false })} />
          ) : null}
        </div>
      </div>
    );
  }
}

export default radium(ContactMainBoard);
