// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  FormSection,
  formValueSelector,
  change,
  initialize,
} from 'redux-form';
import { Query } from 'react-apollo';
// config
import Theme from '../../styles/Theme.js';
import {
  CONTACT_MAP_FORM,
} from '../../shared/Form.js';
import {
  GET_CITY_DISTRICTS,
  GET_STORES,
  GET_CHANNELS,
} from '../../queries/Customize.js';
// components
import StoreSelectionMap from '../../components/Lightbox/StoreSelectionMap.jsx';
import FrameHeavyLine from '../../components/Global/FrameHeavyLine.jsx';
import ContactMapSelectors from '../../components/Contact/ContactMapSelectors.jsx';

const selector = formValueSelector(CONTACT_MAP_FORM);

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    maxWidth: 1280,
    padding: '0 0 48px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  titleWrapper: {
    width: '100%',
    height: 'auto',
    padding: '36px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
    },
  },
  title: {
    fontSize: 32,
    fontWeight: 700,
    color: Theme.BLACK_COLOR,
    letterSpacing: 3,
    textAlign: 'center',
    '@media (max-width: 767px)': {
      fontSize: 26,
    },
  },
  mapWrapper: {
    width: '100%',
    height: 500,
    borderRight: 0,
    borderLeft: 0,
    borderTop: `3px solid ${Theme.YELLOW_COLOR}`,
    borderBottom: `3px solid ${Theme.PURPLE_COLOR}`,
    overflow: 'hidden',
    position: 'relative',
    '@media (max-width: 767px)': {
      height: 400,
    },
  },
  customMapWrapper: {
    width: '100%',
    '@media (max-width: 767px)': {
      width: '100%',
    },
  },
  customMap: {
    width: '100%',
    height: 500,
    '@media (max-width: 767px)': {
      width: '100%',
      height: 400,
    },
  },
  customInfoPlace: {
    left: '30%',
    top: '40%',
  },
  selectionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '16px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
      justifyContent: 'flex-start',
    },
  },
  selection: {
    width: 220,
    height: 'auto',
    margin: 12,
  },
  selector: {
    padding: '7px 16px',
    borderTop: '1px solid rgba(153, 153, 153, 0.6)',
    borderLeft: '1px solid rgba(153, 153, 153, 0.6)',
    borderRight: '1px solid rgba(153, 153, 153, 0.6)',
    borderBottom: '1px solid rgba(153, 153, 153, 0.6)',
    borderRadius: 12,
  },
};

type Props = {
  currentCityId: string,
  currentDistrictId: string,
  currentStoreId: string,
  currentChannelId: string,
  initializeForm: Function,
  resetDistrict: Function,
  resetStore: Function,
};

class ContactStorePage extends PureComponent<Props> {
  componentDidMount() {
    const {
      initializeForm,
    } = this.props;

    initializeForm({
      Selections: {
        cityId: '-1',
        districtId: '-1',
        channelId: '-1',
        storeId: '-1',
      },
    });
  }

  componentDidUpdate(prevProps) {
    const {
      currentCityId,
      currentDistrictId,
      resetDistrict,
      resetStore,
    } = this.props;

    if (currentCityId !== prevProps.currentCityId) {
      resetDistrict();
    }

    if (currentDistrictId !== prevProps.currentDistrictId) {
      resetStore();
    }
  }

  render() {
    const {
      currentCityId,
      currentDistrictId,
      currentStoreId,
      currentChannelId,
    } = this.props;

    return (
      <Query query={GET_CITY_DISTRICTS}>
        {({
          data: {
            cityDistricts = [],
          },
        }) => (
          <Query query={GET_CHANNELS}>
            {({
              data: {
                channelList = [],
              },
            }) => (
              <Query
                query={GET_STORES}
                variables={{
                  cityId: currentCityId && ~parseInt(currentCityId, 10)
                    ? parseInt(currentCityId, 10)
                    : null,
                  districtId: currentDistrictId && ~parseInt(currentDistrictId, 10)
                    ? parseInt(currentDistrictId, 10)
                    : null,
                  channelId: currentChannelId && ~parseInt(currentChannelId, 10)
                    ? parseInt(currentChannelId, 10)
                    : null,
                  isForCustomize: true,
                  isForClient: true,
                }}>
                {({
                  data,
                }) => {
                  let lat = null;
                  let lng = null;
                  let zoom = 10;

                  if (currentCityId) {
                    const wrapCity = cityDistricts.find(c => c.id === parseInt(currentCityId, 10));

                    if (wrapCity) {
                      lat = wrapCity.latitude;
                      lng = wrapCity.longitude;
                      zoom = 10;
                    }
                  }

                  if (currentStoreId) {
                    const storeSource = (data && data.stores) || [];
                    const wrapStore = storeSource.find(s => s.id === parseInt(currentStoreId, 10));

                    if (wrapStore) {
                      lat = wrapStore.latitude;
                      lng = wrapStore.longitude;
                      zoom = 17;
                    }
                  }

                  return (
                    <div style={styles.wrapper}>
                      <div style={styles.titleWrapper}>
                        <span style={styles.title}>
                          artmo
                        </span>
                        <span style={styles.title}>
                          服務據點
                        </span>
                      </div>
                      <div style={styles.mapWrapper}>
                        <FrameHeavyLine
                          type="leftTop"
                          color={Theme.YELLOW_COLOR} />
                        <StoreSelectionMap
                          hideLabel
                          shouldUpdateCenterPosition
                          shouldClearInfoWindowWhenAnyUpdate
                          cityId={currentCityId !== '-1' ? parseInt(currentCityId, 10) : null}
                          districtId={currentDistrictId !== '-1' ? parseInt(currentDistrictId, 10) : null}
                          channelId={currentChannelId !== '-1' ? parseInt(currentChannelId, 10) : null}
                          storeId={currentStoreId !== '-1' ? parseInt(currentStoreId, 10) : null}
                          lat={lat}
                          lng={lng}
                          zoom={zoom}
                          wrapperStyle={styles.customMapWrapper}
                          mapStyle={styles.customMap}
                          infoStyle={styles.customInfoPlace} />
                        <FrameHeavyLine
                          type="rightBottom"
                          color={Theme.PURPLE_COLOR} />
                      </div>
                      <FormSection name="Selections">
                        <ContactMapSelectors
                          cityDistricts={cityDistricts}
                          channelList={channelList}
                          stores={(data && data.stores) || []}
                          currentCityId={currentCityId}
                          currentDistrictId={currentDistrictId} />
                      </FormSection>
                    </div>
                  );
                }}
              </Query>
            )}
          </Query>
        )}
      </Query>
    );
  }
}

const formHook = reduxForm({
  form: CONTACT_MAP_FORM,
});

const reduxHook = connect(
  state => ({
    currentCityId: selector(state, 'Selections.cityId'),
    currentDistrictId: selector(state, 'Selections.districtId'),
    currentStoreId: selector(state, 'Selections.storeId'),
    currentChannelId: selector(state, 'Selections.channelId'),
  }),
  dispatch => bindActionCreators({
    initializeForm: v => initialize(CONTACT_MAP_FORM, v),
    resetDistrict: () => change(CONTACT_MAP_FORM, 'Selections.districtId', '-1'),
    resetStore: () => change(CONTACT_MAP_FORM, 'Selections.storeId', '-1'),
  }, dispatch)
);

export default formHook(
  reduxHook(
    radium(
      ContactStorePage
    )
  )
);
