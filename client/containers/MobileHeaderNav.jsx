// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';

// static
import {
  SITE_HEADER_NAVS,
  SITE_FOOTER_NAVS,
} from '../shared/Global.js';
import * as GlobalActions from '../actions/Global.js';
// components
import SiteHeader from './SiteHeader.jsx';
import MobileNavExtendCategory from '../components/MobileNavExtendCategory.jsx';

const styles = {
  placement: {
    display: 'none',
    '@media (max-width: 1023px)': {
      width: '100vw',
      height: '80vh',
      position: 'absolute',
      zIndex: 99998,
      left: 0,
      top: 0,
      backgroundColor: '#fff',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'flex-start',
    },
  },
  navListWrapper: {
    flex: 1,
    width: '100%',
    height: 'auto',
    padding: '16px 12px',
    borderTop: '1px solid rgba(0, 0, 0, 0.2)',
    overflowX: 'hidden',
    overflowY: 'auto',
  },
  directNav: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    fontSize: 16,
    textAlign: 'left',
    color: 'rgba(0, 0, 0, 0.8)',
    letterSpacing: 1,
    borderTop: 0,
    borderRight: 0,
    borderLeft: 0,
    borderBottom: '1px solid rgba(0, 0, 0, 0.2)',
    backgroundColor: 'transparent',
    outline: 0,
    cursor: 'pointer',
  },
};

type Props = {
  history: {
    push: Function,
  },
  memberAvatar: string,
  navIsOpened: boolean,
  closeMobileExtendNav: Function,
};

class MobileHeaderNav extends PureComponent<Props> {
  componentDidMount() {
    const {
      navIsOpened,
    } = this.props;

    if (navIsOpened) {
      const staticView = document.getElementById('main-area');

      if (staticView) {
        staticView.style.height = 0;
        staticView.style.minHeight = 'auto';
        staticView.style.overflow = 'hidden';
      }
    }
  }

  componentDidUpdate({
    navIsOpened: prevNavIsOpened,
  }) {
    const {
      navIsOpened,
    } = this.props;

    if (!prevNavIsOpened && navIsOpened) {
      const staticView = document.getElementById('main-area');

      if (staticView) {
        staticView.style.height = 0;
        staticView.style.minHeight = 'auto';
        staticView.style.overflow = 'hidden';
      }
    }

    if (prevNavIsOpened && !navIsOpened) {
      const staticView = document.getElementById('main-area');

      if (staticView) {
        staticView.style.height = 'auto';
        staticView.style.minHeight = '100vh';
        staticView.style.overflow = 'auto';
      }
    }
  }

  render() {
    const {
      history,
      navIsOpened,
      closeMobileExtendNav,
      memberAvatar,
    } = this.props;

    if (!navIsOpened) return null;

    const headerNavs = SITE_HEADER_NAVS.filter(s => s.name !== '品牌故事');

    return (
      <div style={styles.placement}>
        <SiteHeader
          isOpened
          history={history}
          onClose={() => closeMobileExtendNav()} />
        <div style={styles.navListWrapper}>
          {headerNavs.map(n => (
            <button
              key={n.path}
              type="button"
              onClick={() => {
                const overridePath = n.path === '/member' && !memberAvatar ? '/member/login' : n.path;
                history.push(overridePath);
                closeMobileExtendNav();
              }}
              style={styles.directNav}>
              {n.name}
            </button>
          ))}
          {SITE_FOOTER_NAVS.map((n, idx) => (
            <MobileNavExtendCategory
              key={n.title}
              isLastCategory={idx === SITE_FOOTER_NAVS.length - 1}
              nav={n}
              onClose={() => closeMobileExtendNav()} />
          ))}
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    navIsOpened: state.Global.extendNavIsOpened,
    memberAvatar: state.Member.memberAvatar,
  }),
  dispatch => bindActionCreators({
    ...GlobalActions,
  }, dispatch)
);

export default withRouter(
  reduxHook(
    radium(
      MobileHeaderNav
    )
  )
);
