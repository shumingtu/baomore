// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
// config
import Theme from '../../styles/Theme.js';
// components
import SeriesFragment1 from '../../components/Introduction/Series/SeriesFragment1.jsx';
import SeriesFragment2 from '../../components/Introduction/Series/SeriesFragment2.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    maxWidth: 1280,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  fragmentsWrapper: {
    width: '100%',
    height: 'auto',
    marginBottom: 24,
    backgroundColor: Theme.GRAY_COLOR,
  },
  fragmentWrapper: {
    width: '100%',
    height: 'auto',
    marginBottom: 8,
  },
};

type ImageOnly = {
  desktopImg: string,
  mobileImg: string,
};

type Fragment = {
  title: {
    color: string,
    name: string,
  },
  desc: {
    color: string,
    name: string,
  },
  images: Array<{
    desc: string,
    src1: {
      desktopImg: string,
      mobileImg: string,
    },
    src2: {
      desktopImg: string,
      mobileImg: string,
    },
    src3: {
      desktopImg: string,
      mobileImg: string,
    },
  }>,
  button: {
    backgroundColor: string,
    color: string,
  },
};

type Props = {
  fragments: {
    mainBackgroundColor: string,
    fragment1: ImageOnly,
    fragment2: Fragment,
    fragment3: ImageOnly,
    fragment4: Fragment,
  },
};

class IntroSeriesPage extends PureComponent<Props> {
  render() {
    const {
      fragments,
    } = this.props;

    return (
      <div style={styles.fragmentsWrapper}>
        {fragments.fragment1 ? (
          <div style={styles.fragmentWrapper}>
            <SeriesFragment1 fragment={fragments.fragment1} />
          </div>
        ) : null}
        {fragments.fragment2 ? (
          <div style={styles.fragmentWrapper}>
            <SeriesFragment2
              mainBackgroundColor={fragments.mainBackgroundColor}
              fragment={fragments.fragment2} />
          </div>
        ) : null}
        {fragments.fragment3 ? (
          <div style={styles.fragmentWrapper}>
            <SeriesFragment1 fragment={fragments.fragment3} />
          </div>
        ) : null}
        {fragments.fragment4 ? (
          <SeriesFragment2
            mainBackgroundColor={fragments.mainBackgroundColor}
            fragment={fragments.fragment4} />
        ) : null}
      </div>
    );
  }
}

export default radium(IntroSeriesPage);
