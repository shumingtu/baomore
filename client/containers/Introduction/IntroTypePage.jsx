// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
// config
import Theme from '../../styles/Theme.js';
// components
import CenterCoverImage, {
  CenterTextBlock,
} from '../../components/Fragment/CenterCoverImage.jsx';
import Iframe from '../../components/Fragment/Iframe.jsx';
import IntroFragment3 from '../../components/Introduction/IntroFragment3.jsx';
import IntroFragment4 from '../../components/Introduction/IntroFragment4.jsx';
import IntroFragment5 from '../../components/Introduction/IntroFragment5.jsx';
import IntroFragment6 from '../../components/Introduction/IntroFragment6.jsx';
import IntroFragment7 from '../../components/Introduction/IntroFragment7.jsx';
import IntroFragment8 from '../../components/Introduction/IntroFragment8.jsx';
import IntroFragment9 from '../../components/Introduction/IntroFragment9.jsx';
import IntroFragment10 from '../../components/Introduction/IntroFragment10.jsx';
import IntroFragment11 from '../../components/Introduction/IntroFragment11.jsx';
import ResizableImage from '../../components/Fragment/ResizableImage.jsx';

const styles = {
  fragmentsWrapper: {
    width: '100%',
    height: 'auto',
    backgroundColor: Theme.GRAY_COLOR,
  },
  fragmentWrapper: {
    width: '100%',
    height: 'auto',
    marginBottom: 8,
  },
};

type Props = {
  fragments: {
    fragment1: Object,
    fragment2: Object,
    fragment3: Object,
    fragment4: Object,
    fragment5: Object,
    fragment6: Array<Object>,
    fragment7: Object,
    fragment8: Object,
    fragment9: Object,
    fragment10: Object,
    fragment11: Object,
  },
};

class IntroTypePage extends PureComponent<Props> {
  render() {
    const {
      fragments,
    } = this.props;

    return (
      <div style={styles.fragmentsWrapper}>
        {fragments.fragment1 ? (
          <div style={styles.fragmentWrapper}>
            <ResizableImage
              desktopImg={fragments.fragment1.desktopImg}
              mobileImg={fragments.fragment1.mobileImg} />
            {/* <CenterCoverImage
              width="100%"
              height={712}
              mobileWidth="100%"
              mobileHeight={800}
              desktopImg={fragments.fragment1.desktopImg}
              mobileImg={fragments.fragment1.mobileImg}>
              <CenterTextBlock
                fragment={{
              title: fragments.fragment1.title || '',
              slogan: fragments.fragment1.slogan || '',
                }} />
            </CenterCoverImage> */}
          </div>
        ) : null}
        {fragments.fragment2 ? (
          <div style={styles.fragmentWrapper}>
            <Iframe src={fragments.fragment2.src} />
          </div>
        ) : null}
        {fragments.fragment3 ? (
          <IntroFragment3 fragment={fragments.fragment3} />
        ) : null}
        {fragments.fragment4 ? (
          <IntroFragment4 fragment={fragments.fragment4} />
        ) : null}
        {fragments.fragment5 ? (
          <IntroFragment5 fragment={fragments.fragment5} />
        ) : null}
        {fragments.fragment6 ? (
          <IntroFragment6 fragments={fragments.fragment6} />
        ) : null}
        {fragments.fragment7 ? (
          <IntroFragment7 fragment={fragments.fragment7} />
        ) : null}
        {fragments.fragment8 ? (
          <IntroFragment8 fragment={fragments.fragment8} />
        ) : null}
        {fragments.fragment9 ? (
          <IntroFragment9 fragment={fragments.fragment9} />
        ) : null}
        {fragments.fragment10 ? (
          <IntroFragment10 fragment={fragments.fragment10} />
        ) : null}
        {fragments.fragment11 ? (
          <IntroFragment11 fragment={fragments.fragment11} />
        ) : null}
      </div>
    );
  }
}

export default radium(IntroTypePage);
