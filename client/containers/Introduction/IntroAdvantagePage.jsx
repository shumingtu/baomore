// @flow
import React from 'react';
import radium from 'radium';
// config
import Theme from '../../styles/Theme.js';
import advantageMocks from '../../shared/Introduction/advantage.js';
// components
import Iframe from '../../components/Fragment/Iframe.jsx';
import AdvantageFragment1 from '../../components/Introduction/Advantage/AdvantageFragment1.jsx';
import AdvantageFragment2 from '../../components/Introduction/Advantage/AdvantageFragment2.jsx';
import FrameHeavyLine from '../../components/Global/FrameHeavyLine.jsx';
import FrameThinLine from '../../components/Global/FrameThinLine.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  mainWrapper: {
    width: '100%',
    height: 'auto',
    maxWidth: 1280,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  fragmentsPlacement: {
    width: '100%',
    height: 'auto',
    maxWidth: 768,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  sloganWrapper: {
    width: '100%',
    height: 'auto',
    padding: '32px 0',
    margin: '64px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
    position: 'relative',
    '@media (max-width: 767px)': {
      padding: '64px 12px',
      margin: 0,
    },
  },
  slogan: {
    fontSize: 32,
    fontWeight: 700,
    letterSpacing: 4,
    textAlign: 'center',
    color: Theme.BLACK_COLOR,
    whiteSpace: 'pre-line',
  },
};

function IntroAdvantagePage() {
  const {
    videoSrc,
    fragment1,
    fragment2,
    slogan,
  } = advantageMocks;

  return (
    <div style={styles.wrapper}>
      <div style={styles.mainWrapper}>
        {videoSrc ? (
          <Iframe
            playVideoOnInit
            src={videoSrc} />
        ) : null}
        <div style={styles.fragmentsPlacement}>
          {fragment1 ? (
            <AdvantageFragment1 fragment={fragment1} />
          ) : null}
          {fragment2 ? (
            <AdvantageFragment2 fragment={fragment2} />
          ) : null}
        </div>
      </div>
      {slogan ? (
        <div style={styles.sloganWrapper}>
          <FrameThinLine
            type="leftTop"
            color={Theme.YELLOW_COLOR} />
          <FrameHeavyLine
            type="leftTop"
            color={Theme.YELLOW_COLOR} />
          <span style={styles.slogan}>
            {slogan}
          </span>
          <FrameThinLine
            type="rightBottom"
            color={Theme.PURPLE_COLOR} />
          <FrameHeavyLine
            type="rightBottom"
            color={Theme.PURPLE_COLOR} />
        </div>
      ) : null}
    </div>
  );
}

export default radium(IntroAdvantagePage);
