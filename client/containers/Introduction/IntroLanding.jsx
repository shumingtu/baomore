import React, { PureComponent } from 'react';
import radium from 'radium';
// config
import Theme from '../../styles/Theme.js';
import navs from '../../shared/Introduction/navs.js';
import { LANDING_FRAGMENTS } from '../../shared/Introduction/fragments.js';
// components
import IntroLandingNav from '../../components/Introduction/IntroLandingNav.jsx';
import CenterCoverImage, {
  CenterTextBlock,
} from '../../components/Fragment/CenterCoverImage.jsx';
import IntroAdvantageLanding from '../../components/Introduction/IntroAdvantageLanding.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  introNavWrapper: {
    width: '100%',
    height: 80,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
  fragmentsWrapper: {
    width: '100%',
    height: 'auto',
    backgroundColor: Theme.GRAY_COLOR,
  },
  fragmentWrapper: {
    width: '100%',
    height: 'auto',
    marginBottom: 8,
  },
};

class IntroLanding extends PureComponent {
  render() {
    return (
      <div style={styles.wrapper}>
        <div style={styles.introNavWrapper}>
          {navs.map(nav => (
            <IntroLandingNav
              key={nav.id}
              nav={nav} />
          ))}
        </div>
        <div style={styles.fragmentsWrapper}>
          {LANDING_FRAGMENTS.map(frag => (
            <div key={frag.title.name} style={styles.fragmentWrapper}>
              <CenterCoverImage
                key={`${frag.title.name}-image`}
                containMode
                width="100vw"
                height="calc(100vw * 0.508)"
                mobileWidth="100vw"
                mobileHeight="calc(100vw * 1.499)"
                desktopImg={frag.desktopImg}
                mobileImg={frag.mobileImg}>
                <CenterTextBlock fragment={frag} />
              </CenterCoverImage>
            </div>
          ))}
        </div>
        <IntroAdvantageLanding />
      </div>
    );
  }
}

export default radium(IntroLanding);
