// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import {
  Switch,
  Route,
  Link,
} from 'react-router-dom';

import Theme from '../../styles/Theme.js';
// components
import IntroSeriesPage from './IntroSeriesPage.jsx';
import IntroTypePage from './IntroTypePage.jsx';
import SubNavBar from '../../components/Global/SubNavBar.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  buyBtn: {
    width: 'auto',
    height: 'auto',
    padding: '6px 12px',
    border: 0,
    borderRadius: 4,
    backgroundColor: Theme.PURPLE_COLOR,
    textAlign: 'center',
    textDecoration: 'none',
    fontSize: 12,
    color: '#fff',
    cursor: 'pointer',
  },
};

type Props = {
  title: {
    path: string,
    name: string,
  },
  series: Array<{
    id: string,
    path: string,
    name: string,
    data: Object,
  }>,
  fragments: {
    fragment1: Object,
    fragment2: Object,
    fragment3: Object,
    fragment4: Object,
    fragment5: Object,
    fragment6: Array<Object>,
    fragment7: Object,
    fragment8: Object,
    fragment9: Object,
    fragment10: Object,
    fragment11: Object,
  },
  match: {
    url: string,
  },
};

class IntroMainBoard extends PureComponent<Props> {
  render() {
    const {
      title,
      series,
      fragments,
      match: {
        url,
      },
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <SubNavBar
          title={title}
          navs={series}
          hookButton={(
            <Link
              to={{ pathname: '/customize/create' }}
              style={styles.buyBtn}>
              購買
            </Link>
          )} />
        <Switch>
          {series.map(s => (
            <Route
              key={s.path}
              path={s.path}
              component={props => (
                <IntroSeriesPage
                  {...props}
                  fragments={s.data || null} />
              )} />
          ))}
          <Route
            path={`${url}`}
            component={props => (
              <IntroTypePage
                {...props}
                fragments={fragments} />
            )} />
        </Switch>
      </div>
    );
  }
}

export default radium(IntroMainBoard);
