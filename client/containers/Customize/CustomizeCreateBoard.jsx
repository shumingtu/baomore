// @flow
import React, {
  Component,
  lazy,
  Suspense,
} from 'react';
import radium from 'radium';
import qs from 'qs';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  FormSection,
  initialize,
} from 'redux-form';
// actions
import * as LightboxActions from '../../actions/Lightbox.js';
// config
import { PHONE_CUSTOMIZE_FORM } from '../../shared/Form.js';
import {
  LINE_PAY_SUCCESS_LIGHTBOX,
  LINE_PAY_FAILED_LIGHTBOX,
} from '../../shared/LightboxTypes.js';
import {
  CANVAS_WIDTH,
  CANVAS_MAX_WIDTH,
} from '../../shared/PhoneConfig.js';
// helper
import {
  canvasStringToJson,
} from '../../helper/canvasHelper.js';
// components
import Loading from '../../components/Global/Loading.jsx';
import NoDataPlaceholder from '../../components/Member/NoDataPlaceholder.jsx';
// lazily components
const MainCustomizeDesktopBoard = lazy(() => import('./Desktop/MainCustomizeBoard.jsx'));
const MainCustomizeMobileBoard = lazy(() => import('./Mobile/MainCustomizeBoard.jsx'));

const styles = {
  wrapper: {
    width: '100%',
    maxWidth: 1200,
    height: '100%',
    overflowY: 'visible',
    overflowX: 'hidden',
    position: 'relative',
  },
};

type Props = {
  customizeCategoryName: string,
  phoneSelection: Object,
  customizeCategory: string,
  customizePNTableInfo: {
    id: number,
    description: string,
    onlinePrice: string,
  },
  initializeForm: Function,
  isEditMode: boolean,
  history: {
    replace: Function,
  },
  location: {
    pathname: string,
    search: string,
  },
  openLightbox: Function,
  memberStoredJSON: string,
  memberName: string,
  memberEmail: string,
  memberPhone: string,
  memberId: string,
  mainCategories: Array<{
    id: number,
    name: string,
    picture: string,
  }>,
};

type State = {
  isMobileMode: boolean,
  previewAreaWidth: number,
  innerHeight: number,
};

class CustomizeCreateBoard extends Component<Props, State> {
  constructor(props) {
    super(props);

    const width = window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;

    const height = window.innerHeight
      || document.documentElement.clientHeight
      || document.body.clientHeight;

    this.state = {
      isMobileMode: width < 768 || false,
      previewAreaWidth: width >= 768 ? CANVAS_WIDTH : Math.min(width * 0.8, CANVAS_MAX_WIDTH),
      innerHeight: height,
    };

    this.resizeHandler = () => this.resizing();
  }

  componentDidMount() {
    window.addEventListener('resize', this.resizeHandler);

    const {
      phoneSelection,
      customizeCategory,
      customizeCategoryName,
      history,
      isEditMode,
      location: {
        pathname,
        search,
      },
      openLightbox,
    } = this.props;

    const splitPathname = pathname.split('/').filter(p => p) || [];
    const parseQs = search ? qs.parse(search, { ignoreQueryPrefix: true }) : null;

    if (splitPathname[0] && splitPathname[1] === 'success' && parseQs) {
      openLightbox({
        type: LINE_PAY_SUCCESS_LIGHTBOX,
        orderId: (parseQs && parseQs.d) || null,
        transactionId: (parseQs && parseQs.transactionId) || null,
      });
    } else if (splitPathname[0] && splitPathname[1] === 'failed' && parseQs) {
      openLightbox({
        type: LINE_PAY_FAILED_LIGHTBOX,
        errorCode: (parseQs && parseQs.errorCode) || null,
      });
    }

    // edit mode
    if (isEditMode) {
      return this.initializing();
    }
    // create mode
    if (!phoneSelection || !customizeCategory || !customizeCategoryName) {
      return history.replace('/customize/start');
    }

    return this.initializing();
  }

  componentDidUpdate(prevProps) {
    const {
      memberId,
    } = this.props;

    if (memberId !== prevProps.memberId && memberId) {
      this.initializing();
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeHandler);
  }

  getJSONData() {
    const {
      isEditMode,
      memberStoredJSON,
      location: {
        search,
      },
    } = this.props;

    const parseQs = search ? qs.parse(search, { ignoreQueryPrefix: true }) : null;

    if (isEditMode) {
      if (parseQs && parseQs.local) {
        const localData = localStorage.getItem('customizeJson') || null;
        return localData ? canvasStringToJson(localData) : null;
      }

      return canvasStringToJson(memberStoredJSON);
    }

    return null;
  }

  getCustomizeBundlePack() {
    const {
      customizeCategory,
      mainCategories,
    } = this.props;

    const jsonData = this.getJSONData();
    const wrapCategory = (jsonData && jsonData.Main
      && jsonData.Main.customizeCategory) || customizeCategory;

    if (wrapCategory === '客製化包膜') {
      const category = mainCategories.find(c => c.id === 5);

      return {
        categoryId: (category && category.id) || null,
        title: (category && category.name) || null,
        isFixedFromApi: false,
      };
    }

    const myCategory = mainCategories.find(c => c.id === parseInt(wrapCategory, 10));

    if (myCategory) {
      return {
        categoryId: myCategory.id,
        title: myCategory.name,
        isFixedFromApi: true,
      };
    }

    return {
      categoryId: null,
      title: null,
      isFixedFromApi: true,
    };
  }

  resizeHandler: Function

  resizing() {
    const {
      isMobileMode,
    } = this.state;

    const width = window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;

    const height = window.innerHeight
      || document.documentElement.clientHeight
      || document.body.clientHeight;

    if (isMobileMode && width > 767) {
      return this.setState({
        isMobileMode: false,
        previewAreaWidth: CANVAS_WIDTH,
        innerHeight: height,
      });
    }

    if (width < 768) {
      return this.setState({
        isMobileMode: true,
        previewAreaWidth: Math.min(width * 0.8, CANVAS_MAX_WIDTH),
        innerHeight: height,
      });
    }

    return null;
  }

  initializing() {
    const {
      isEditMode,
      initializeForm,
      phoneSelection,
      customizeCategory,
      customizeCategoryName,
      customizePNTableInfo,
      memberId,
      memberName,
      memberEmail,
      memberPhone,
      history,
    } = this.props;

    const customizePack = this.getCustomizeBundlePack() || {};
    const emptyImageInit = customizePack.isFixedFromApi ? {} : [];
    const data = this.getJSONData();

    if (isEditMode) {
      if (!data) return history.replace('/customize/start');
      if (!memberId) return null;

      const isCustomizeMode = (data.Main && data.Main.customizeCategory === '客製化包膜') || false;

      return initializeForm({
        Main: {
          ...data.Main,
          Customize: {
            ...data.Main.Customize,
            price: isCustomizeMode && customizePNTableInfo && customizePNTableInfo.onlinePrice
              ? `$${customizePNTableInfo.onlinePrice}` : data.Main.Customize.price,
            description: isCustomizeMode && customizePNTableInfo && customizePNTableInfo.description
              ? customizePNTableInfo.description : data.Main.Customize.description,
            Pay: {
              ...data.Main.Customize.Pay,
              name: data.Main.Customize.Pay.name || memberName || '',
              email: data.Main.Customize.Pay.email || memberEmail || '',
              phone: data.Main.Customize.Pay.phone || memberPhone || '',
            },
          },
        },
      });
    }

    return initializeForm({
      Main: {
        customizeCategoryName: customizeCategoryName || null,
        customizeCategory: customizeCategory || null,
        phoneSelection: phoneSelection || null,
        Customize: {
          needWhiteBg: false,
          images: emptyImageInit,
          price: customizeCategory === '客製化包膜' && customizePNTableInfo
            ? `$${customizePNTableInfo.onlinePrice}` : null,
          description: customizeCategory === '客製化包膜' && customizePNTableInfo
            ? customizePNTableInfo.description : null,
          isPublic: false,
          Pay: { // pay check info
            name: memberName || '',
            email: memberEmail || '',
            phone: memberPhone || '',
          },
        },
      },
    });
  }

  render() {
    const {
      isEditMode,
      memberId,
    } = this.props;

    const {
      isMobileMode,
      previewAreaWidth,
      innerHeight,
    } = this.state;

    if (isEditMode && !memberId) {
      return (
        <div
          style={{
            width: '100%',
            height: '60vh',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <NoDataPlaceholder type="noToken" />
        </div>
      );
    }
    // Do not render two konva at same time (include display: none) (causing bug)
    const customizePack = this.getCustomizeBundlePack();

    return (
      <FormSection name="Main">
        <div
          style={[
            styles.wrapper,
            {
              height: innerHeight,
            },
          ]}>
          <Suspense fallback={<Loading />}>
            {!isMobileMode ? (
              <MainCustomizeDesktopBoard
                {...customizePack}
                isEditMode={isEditMode} />
            ) : (
              <MainCustomizeMobileBoard
                {...customizePack}
                previewAreaWidth={previewAreaWidth}
                isEditMode={isEditMode} />
            )}
          </Suspense>
        </div>
      </FormSection>
    );
  }
}

const formHook = reduxForm({
  form: PHONE_CUSTOMIZE_FORM,
});

const reduxHook = connect(
  state => ({
    phoneSelection: state.Customize.phoneSelection,
    customizeCategory: state.Customize.customizeCategory,
    customizeCategoryName: state.Customize.customizeCategoryName,
    customizePNTableInfo: state.Customize.customizePNTableInfo,
    mainCategories: state.Customize.mainCategories,
    memberStoredJSON: state.Member.memberStoredJSON,
    memberName: state.Member.memberName,
    memberEmail: state.Member.memberEmail,
    memberPhone: state.Member.memberPhone,
    memberId: state.Member.memberId,
  }),
  dispatch => bindActionCreators({
    ...LightboxActions,
    initializeForm: v => initialize(PHONE_CUSTOMIZE_FORM, v),
  }, dispatch)
);

export default reduxHook(
  formHook(
    radium(
      CustomizeCreateBoard
    )
  )
);
