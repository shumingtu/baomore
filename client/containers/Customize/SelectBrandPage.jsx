// @flow
import React, {
  PureComponent,
  lazy,
  Suspense,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  formValueSelector,
  change,
  initialize,
  SubmissionError,
} from 'redux-form';
import {
  Query,
} from 'react-apollo';

// config
import Theme from '../../styles/Theme.js';
import { PHONE_SELECTION_FORM } from '../../shared/Form.js';
// action
import * as CustomizeActions from '../../actions/Customize.js';
// queries
import {
  FETCH_BRAND_LIST,
  FETCH_BRAND_MODEL_LIST,
  FETCH_MODEL_COLOR_LIST,
} from '../../queries/Brand.js';
// components
import FormLabel from '../../components/Form/FormLabel.jsx';
import Loading from '../../components/Global/Loading.jsx';
import Selector from '../../components/Form/Selector.jsx';
// lazy import components
const SubmitButton = lazy(() => import('../../components/Global/Button.jsx'));

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    padding: '40px 0',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  formWrapper: {
    width: '100%',
    maxWidth: 360,
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  formTitle: {
    fontSize: 12,
    margin: 0,
    padding: '12px 0',
    letterSpacing: 1,
    color: 'rgba(102, 102, 102, 0.92)',
  },
  formBlock: {
    width: '100%',
    height: 'auto',
    padding: 20,
    marginBottom: 16,
    borderRadius: 8,
    backgroundColor: '#fff',
    boxShadow: Theme.BLOCK_SHADOW,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    padding: '24px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonWrapper: {
    width: 240,
    height: 40,
  },
};

const selector = formValueSelector(PHONE_SELECTION_FORM);

type Props = {
  initializeForm: Function,
  handleSubmit: Function,
  resetField: Function,
  savePhoneSelection: Function,
  history: {
    push: Function,
  },
  brandId: String,
  brandModelId: String,
};

class SelectBrandPage extends PureComponent<Props> {
  componentDidMount() {
    const {
      initializeForm,
    } = this.props;

    initializeForm({
      brand: '-1',
      brandModel: '-1',
      brandModelColor: '-1',
    });
  }

  componentDidUpdate(prevProps) {
    const {
      brandId,
      brandModelId,
      resetField,
    } = this.props;

    if (brandId !== prevProps.brandId) {
      resetField('brandModel');
    }

    if (brandModelId !== prevProps.brandModelId) {
      resetField('brandModelColor');
    }
  }

  submit(d) {
    const {
      brand,
      brandModel,
      brandModelColor,
    } = d;

    if (!brand || brand === '-1') {
      throw new SubmissionError({
        brand: '請選擇品牌',
      });
    }

    if (!brandModel || brandModel === '-1') {
      throw new SubmissionError({
        brandModel: '請選擇型號',
      });
    }

    if (!brandModelColor || brandModelColor === '-1') {
      throw new SubmissionError({
        brandModelColor: '請選擇顏色',
      });
    }

    const {
      history,
      savePhoneSelection,
    } = this.props;

    const payload = {
      brandId: parseInt(brand, 10),
      modelId: parseInt(brandModel, 10),
      colorId: parseInt(brandModelColor, 10),
    };

    savePhoneSelection(payload);
    history.push('/customize/category');
  }

  render() {
    const {
      handleSubmit,
      brandId,
      brandModelId,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <form style={styles.formWrapper} onSubmit={handleSubmit(d => this.submit(d))}>
          <h2 style={styles.formTitle}>
            選擇想客製化的手機型號
          </h2>
          <div style={styles.formBlock}>
            <FormLabel label="品牌" />
            <Suspense fallback={<Loading />}>
              <Query query={FETCH_BRAND_LIST}>
                {({
                  data,
                }) => (
                  <Field
                    name="brand"
                    options={(data && data.brandList) || []}
                    placeholder="請選擇手機品牌"
                    component={Selector} />
                )}
              </Query>
            </Suspense>
          </div>
          <div style={styles.formBlock}>
            <FormLabel label="型號" />
            <Suspense fallback={<Loading />}>
              <Query
                query={FETCH_BRAND_MODEL_LIST}
                variables={{ brandId: brandId ? parseInt(brandId, 10) : null }}
                skip={!brandId}>
                {({
                  data,
                }) => (
                  <Field
                    name="brandModel"
                    options={(data && data.brandModelList) || []}
                    placeholder="請選擇手機型號"
                    component={Selector} />
                )}
              </Query>
            </Suspense>
          </div>
          <div style={styles.formBlock}>
            <FormLabel label="顏色" />
            <Suspense fallback={<Loading />}>
              <Query
                query={FETCH_MODEL_COLOR_LIST}
                variables={{ modelId: brandModelId ? parseInt(brandModelId, 10) : null }}
                skip={!brandModelId}>
                {({
                  data,
                }) => (
                  <Field
                    name="brandModelColor"
                    options={(data && data.brandModelColorList) || []}
                    placeholder="請選擇手機顏色"
                    component={Selector} />
                )}
              </Query>
            </Suspense>
          </div>
          <div style={styles.functionWrapper}>
            <div style={styles.buttonWrapper}>
              <Suspense fallback={<Loading />}>
                <SubmitButton
                  withShadow
                  type="submit"
                  label="下一步" />
              </Suspense>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const formHook = reduxForm({
  form: PHONE_SELECTION_FORM,
});

const reduxHook = connect(
  state => ({
    brandId: selector(state, 'brand'),
    brandModelId: selector(state, 'brandModel'),
  }),
  dispatch => bindActionCreators({
    ...CustomizeActions,
    initializeForm: v => initialize(PHONE_SELECTION_FORM, v),
    resetField: f => change(PHONE_SELECTION_FORM, f, '-1'),
  }, dispatch),
);

export default reduxHook(
  formHook(
    radium(
      SelectBrandPage
    )
  )
);
