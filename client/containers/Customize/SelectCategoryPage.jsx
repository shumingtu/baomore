// @flow
import React, {
  PureComponent,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import chunk from 'lodash/chunk';

import * as LightboxActions from '../../actions/Lightbox.js';
import { CUSTOMIZE_REMINDER_LIGHTBOX } from '../../shared/LightboxTypes.js';
// components
import BackLink from '../../components/Global/BackLink.jsx';
import Category from '../../components/Customize/Category.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    maxWidth: 1200,
    padding: 12,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  barWrapper: {
    width: '100%',
    height: 'auto',
    padding: '6px 4px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
  mainWrapper: {
    width: '100%',
    maxWidth: 360,
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      maxWidth: 320,
      padding: '0 2px',
    },
  },
  mainTitle: {
    fontSize: 12,
    margin: 0,
    padding: '12px 0',
    letterSpacing: 1,
    color: 'rgba(102, 102, 102, 0.92)',
  },
  categoryWrapper: {
    width: '100%',
    height: '100%',
  },
  categoryGroup: {
    width: '100%',
    height: 192,
    padding: '12px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
};

type Props = {
  history: {
    push: Function,
    replace: Function,
  },
  phoneSelection: Object,
  mainCategories: Array<{
    id: number,
    name: string,
    picture: string,
  }>,
  openLightbox: Function,
};

class SelectCategoryPage extends PureComponent<Props> {
  componentDidMount() {
    const {
      phoneSelection,
      history,
      openLightbox,
    } = this.props;

    if (!phoneSelection) {
      history.replace('/customize/start');
    } else {
      openLightbox({
        type: CUSTOMIZE_REMINDER_LIGHTBOX,
        backgroundClickableDisabled: true,
      });
    }
  }

  render() {
    const {
      mainCategories,
    } = this.props;

    const categoryChunks = chunk(mainCategories, 2);

    return (
      <div style={styles.wrapper}>
        <div style={styles.barWrapper}>
          <BackLink
            label="選擇手機型號"
            path="/customize/start" />
        </div>
        <div style={styles.mainWrapper}>
          <h2 style={styles.mainTitle}>
            選擇包膜產品
          </h2>
          <div style={styles.categoryWrapper}>
            {categoryChunks.map(chunkGroup => (
              <div key={Math.random()} style={styles.categoryGroup}>
                {chunkGroup.map(category => (
                  <Category
                    key={category.id}
                    id={category.id}
                    label={category.name}
                    image={category.picture} />
                ))}
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  state => ({
    phoneSelection: state.Customize.phoneSelection,
    mainCategories: state.Customize.mainCategories,
  }),
  dispatch => bindActionCreators({
    ...LightboxActions,
  }, dispatch)
);

export default reduxHook(
  radium(
    SelectCategoryPage
  )
);
