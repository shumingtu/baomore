import React from 'react';
import radium from 'radium';
import { Link } from 'react-router-dom';
// config
import {
  YELLOW_COLOR,
  PURPLE_COLOR,
} from '../../styles/Landing.js';
import Theme from '../../styles/Theme.js';
import customizeMocks from '../../shared/Customize/fragments.js';
// components
import ResizableImage from '../../components/Fragment/ResizableImage.jsx';
import Iframe from '../../components/Fragment/Iframe.jsx';
import LinkButton from '../../components/Fragment/Element/LinkButton.jsx';
import IGFragment from '../../components/Customize/Landing/IGFragment.jsx';
import FrameHeavyLine from '../../components/Global/FrameHeavyLine.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  fragmentsWrapper: {
    width: '100%',
    height: 'auto',
    backgroundColor: Theme.GRAY_COLOR,
  },
  fragmentWrapper: {
    width: '100%',
    height: 'auto',
    marginBottom: 8,
  },
  firstFragmentWrapper: {
    width: '100%',
    height: 'auto',
    overflow: 'hidden',
    borderTop: `1px solid ${YELLOW_COLOR}`,
    borderBottom: `1px solid ${PURPLE_COLOR}`,
    position: 'relative',
  },
  link: {
    width: '100%',
    height: 'auto',
    textDecoration: 'none',
    padding: 0,
    margin: 0,
    border: 0,
    backgroundColor: 'transparent',
    display: 'block',
    cursor: 'pointer',
  },
  buttonWrapper: {
    width: '100%',
    height: 'auto',
    padding: '36px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

function CustomizeLanding() {
  const {
    fragment1,
    fragment2,
    fragment3,
    fragment4,
    fragment5,
  } = customizeMocks;

  return (
    <div style={styles.wrapper}>
      <div style={styles.fragmentsWrapper}>
        {fragment1 ? (
          <div style={styles.firstFragmentWrapper}>
            <FrameHeavyLine
              type="leftTop"
              color={YELLOW_COLOR} />
            <Link
              to={{ pathname: '/customize/start' }}
              style={styles.link}>
              <ResizableImage
                desktopRatio={0.556}
                mobileRatio={1.5}
                desktopImg={fragment1.desktopImg}
                mobileImg={fragment1.mobileImg} />
            </Link>
            <FrameHeavyLine
              type="rightBottom"
              color={PURPLE_COLOR} />
          </div>
        ) : null}
        {fragment2 ? (
          <Iframe src={fragment2.src} />
        ) : null}
        {fragment3 ? (
          <div style={styles.fragmentWrapper}>
            <ResizableImage
              desktopRatio={0.556}
              mobileRatio={1.5}
              desktopImg={fragment3.desktopImg}
              mobileImg={fragment3.mobileImg} />
          </div>
        ) : null}
        {fragment4 ? (
          <ResizableImage
            desktopRatio={0.556}
            mobileRatio={1.5}
            desktopImg={fragment4.desktopImg}
            mobileImg={fragment4.mobileImg} />
        ) : null}
      </div>
      {/* <div style={styles.buttonWrapper}>
        <LinkButton
          path="/customize/start"
          text="開始創造" />
      </div> */}
      {fragment5 ? (
        <IGFragment fragment={fragment5} />
      ) : null}
    </div>
  );
}

export default radium(CustomizeLanding);
