// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import {
  FieldArray,
} from 'redux-form';

// config
import { TYPES_SPEC } from '../../../shared/Global.js';
// components
import SwipeImages from '../../../components/Customize/Operations/SwipeImages.jsx';
import SwipeText from '../../../components/Customize/Operations/SwipeText.jsx';
import SwipeEmoji from '../../../components/Customize/Operations/SwipeEmoji.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
};

type Props = {
  currentType: String,
};

class SwipeController extends PureComponent<Props> {
  typeComponentRenderer() {
    const {
      currentType,
    } = this.props;

    switch (currentType) {
      case TYPES_SPEC[0].type:
        return (
          <FieldArray
            name="images"
            component={SwipeImages} />
        );

      case TYPES_SPEC[1].type:
        return (
          <FieldArray
            name="images"
            component={SwipeText} />
        );

      case TYPES_SPEC[2].type:
        return (
          <FieldArray
            name="images"
            component={SwipeEmoji} />
        );
      default:
        return null;
    }
  }

  render() {
    return (
      <div style={styles.wrapper}>
        {this.typeComponentRenderer()}
      </div>
    );
  }
}

export default radium(SwipeController);
