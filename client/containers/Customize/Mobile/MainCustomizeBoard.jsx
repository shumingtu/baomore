// @flow
import React, {
  Component,
  Fragment,
  lazy,
  Suspense,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  FormSection,
  Field,
  FieldArray,
  getFormValues,
} from 'redux-form';
import { Mutation } from 'react-apollo';

// config
import { TYPES_SPEC } from '../../../shared/Global.js';
import { PHONE_CUSTOMIZE_FORM } from '../../../shared/Form.js';
import {
  CANVAS_WIDTH,
  MOBILE_CANVAS_WIDTH,
  IMAGE_WIDTH_SPEC,
} from '../../../shared/PhoneConfig.js';
import {
  canvasJsonToString,
  isMobileMode,
} from '../../../helper/canvasHelper.js';
import { uploadCanvas } from '../../../helper/uploader.js';
import { STORE_MEMBER_JSON } from '../../../mutations/Member.js';
import {
  STATUS_FAILED,
} from '../../../shared/StatusTypes.js';
import {
  LINE_LOGIN_LIGHTBOX,
} from '../../../shared/LightboxTypes.js';
// actions
import * as CustomizeActions from '../../../actions/Customize.js';
import * as LightboxActions from '../../../actions/Lightbox.js';
import * as MemberActions from '../../../actions/Member.js';
// context
import CustomizeTextContext from '../../../context/CustomizeTextContext.js';
import CustomizeEmojiContext from '../../../context/CustomizeEmojiContext.js';
// components
import Loading from '../../../components/Global/Loading.jsx';
import Title from '../../../components/Customize/Title.jsx';
import TypeSwitcher from '../../../components/Customize/Operations/TypeSwitcher.jsx';
import MakeOrderButton from '../../../components/Customize/Pay/MakeOrderButton.jsx';
import CheckoutButton from '../../../components/Customize/Operations/CheckoutButton.jsx';
import SwipeController from './SwipeController.jsx';
import OperationBoard from './OperationBoard.jsx';
import AdderModulePopup from './AdderModulePopup.jsx';
import FixedImageSelection from '../../../components/Customize/FixedImageSelection.jsx';
import FixedImageSelectionResults from '../../../components/Customize/FixedImageSelectionResults.jsx';
import SingleInfo from '../../../components/Form/SingleInfo.jsx';
import StoreJsonButton from '../../../components/Customize/Operations/StoreJsonButton.jsx';

// lazily components
const PhoneKonva = lazy(() => import('../../../components/Customize/PhoneKonva.jsx'));
const Checkbox = lazy(() => import('../../../components/Form/Checkbox.jsx'));
const PayCheckBoard = lazy(() => import('./PayCheckBoard.jsx'));

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  autoWrapper: {
    height: 'auto',
    alignItems: 'center',
  },
  titleWrapper: {
    width: '100%',
    height: 'auto',
    padding: 12,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  publicWrapper: {
    width: 140,
    height: '100%',
  },
  storeJsonWrapper: {
    width: 'auto',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '0 6px',
  },
  switchBarWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: '4px 12px',
  },
  barWrapper: {
    flex: 1,
    height: '100%',
  },
  checkoutBtnWrapper: {
    width: 'auto',
    height: '100%',
    paddingLeft: 4,
  },
  mainBoard: {
    width: '100%',
    height: 'auto',
    flex: 2,
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    position: 'relative',
  },
  fixedImageMainBoard: {
    flexDirection: 'column',
  },
  swipeWrapper: {
    width: 89,
    height: '55vh',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: '0 12px',
  },
  fixedImageSwipeWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  konvaWrapper: {
    flex: 1,
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    overflowX: 'auto',
    overflowY: 'auto',
    padding: '8px 0',
    '@media (max-width: 767px)': {
      overflowX: 'hidden',
      overflowY: 'auto',
    },
  },
  fixedImageKonvaWrapper: {
    width: '100%',
    height: 'auto',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  fixedImageResultWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  operationBoardWrapper: {
    width: '100%',
    height: 'auto',
    overflow: 'visible',
    position: 'relative',
  },
  adderModuleWrapper: {
    position: 'fixed',
    zIndex: 999,
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    width: '100%',
    height: '100%',
    outline: 0,
    border: 0,
    backgroundColor: 'rgba(230, 230, 230, 0.7)',
    overflowX: 'hidden',
    overflowY: 'auto',
  },
  hide: {
    display: 'none',
  },
};

type Props = {
  title: string,
  isFixedFromApi: boolean,
  formValues: Object,
  clearAllSelectedCache: Function,
  accessToken: string,
  openStatusBox: Function,
  openLightbox: Function,
  cacheMemberCustomization: Function,
  cacheCustomizeUrl: Function,
  previewAreaWidth: number,
};

type State = {
  currentStep: number,
  currentType: string,
  openAdderModule: string,
};

class MainCustomizeBoard extends Component<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      currentStep: 1,
      currentType: TYPES_SPEC[0].type,
      openAdderModule: '',
    };

    this.isMount = false;
    this.canvasLayer = React.createRef();
  }

  componentDidMount() {
    this.isMount = true;
  }

  componentWillUnmount() {
    const {
      clearAllSelectedCache,
    } = this.props;

    this.isMount = false;
    clearAllSelectedCache();
  }

  canvasLayer: { current: null | HTMLDivElement }

  isMount: boolean

  goNextStep() {
    const {
      currentStep,
    } = this.state;

    this.setState({
      currentStep: currentStep + 1,
    });
  }

  uploadMyCanvas() {
    const {
      cacheCustomizeUrl,
      previewAreaWidth,
    } = this.props;

    const mobileCanvasWidth = previewAreaWidth || MOBILE_CANVAS_WIDTH;
    const canvasWidth = isMobileMode() ? mobileCanvasWidth : CANVAS_WIDTH;
    const pixelRatio = IMAGE_WIDTH_SPEC / canvasWidth;

    if (this.canvasLayer) {
      const myCanvas = this.canvasLayer.getLayer().toCanvas({
        pixelRatio,
      });
      myCanvas.toBlob(blob => uploadCanvas(blob, (obj) => {
        if (!obj.isPreview) {
          cacheCustomizeUrl(obj.src);
        }
      }), 'image/png');
    }
  }

  topBarRenderer() {
    const {
      isFixedFromApi,
      formValues,
      accessToken,
      openStatusBox,
      openLightbox,
      cacheMemberCustomization,
    } = this.props;

    const {
      currentStep,
      currentType,
    } = this.state;

    switch (currentStep) {
      case 1:
        return (
          <Fragment>
            <div style={styles.barWrapper}>
              <TypeSwitcher
                types={!isFixedFromApi
                  ? TYPES_SPEC
                  : [TYPES_SPEC[0]]}
                currentType={currentType}
                onChange={t => this.setState({ currentType: t })} />
            </div>
            <div style={styles.checkoutBtnWrapper}>
              <Mutation mutation={STORE_MEMBER_JSON}>
                {storeJSON => (
                  <CheckoutButton
                    onClick={async () => {
                      this.uploadMyCanvas();

                      const newData = canvasJsonToString(formValues);

                      cacheMemberCustomization(newData);

                      if (accessToken) {
                        const {
                          data,
                        } = await storeJSON({
                          variables: {
                            json: newData,
                          },
                        });

                        if (data) {
                          if (this.isMount) {
                            this.goNextStep();
                          }
                        } else {
                          openStatusBox({
                            type: STATUS_FAILED,
                            label: '操作失敗',
                          });
                        }
                      } else {
                        openLightbox({
                          type: LINE_LOGIN_LIGHTBOX,
                        });
                      }
                    }} />
                )}
              </Mutation>
            </div>
          </Fragment>
        );
      case 2:
        return (
          <Fragment>
            <div style={styles.barWrapper}>
              <Field
                name="price"
                label="售價"
                component={SingleInfo} />
            </div>
            <div style={styles.checkoutBtnWrapper}>
              <MakeOrderButton />
            </div>
          </Fragment>
        );
      default:
        return null;
    }
  }

  operationRenderer() {
    const {
      isFixedFromApi,
    } = this.props;

    const {
      currentStep,
      currentType,
    } = this.state;

    switch (currentStep) {
      case 1:
        return (
          <OperationBoard
            isFixedFromApi={isFixedFromApi}
            currentType={currentType} />
        );
      case 2:
        return (
          <Suspense fallback={<Loading />}>
            <PayCheckBoard />
          </Suspense>
        );
      default:
        return null;
    }
  }

  render() {
    const {
      isFixedFromApi = false,
      title = null,
      formValues,
      previewAreaWidth,
    } = this.props;

    const {
      currentStep,
      currentType,
      openAdderModule,
    } = this.state;

    const canUsePublicBtn = formValues && formValues.Main
      ? formValues.Main.customizeCategory === '客製化包膜'
      : false;

    return (
      <FormSection name="Customize">
        <div
          id="customize-mobile-board"
          style={[
            styles.wrapper,
            currentStep !== 1 && styles.autoWrapper,
          ]}>
          <div style={styles.titleWrapper}>
            <Title title={title} />
            <div style={styles.publicWrapper}>
              {canUsePublicBtn ? (
                <Field
                  showReminderWhenChecked
                  name="isPublic"
                  label="同意公開上架使用"
                  component={Checkbox} />
              ) : null}
            </div>
            <div style={styles.storeJsonWrapper}>
              <StoreJsonButton />
            </div>
          </div>
          <div style={styles.switchBarWrapper}>
            {this.topBarRenderer()}
          </div>
          <div
            style={[
              styles.mainBoard,
              isFixedFromApi && styles.fixedImageMainBoard,
            ]}>
            <div
              style={[
                styles.swipeWrapper,
                isFixedFromApi && styles.fixedImageSwipeWrapper,
                currentStep !== 1 && styles.hide,
              ]}>
              {!isFixedFromApi ? (
                <CustomizeTextContext.Provider
                  value={{
                    onOpenAdderModule: () => this.setState({
                      openAdderModule: TYPES_SPEC[1].type,
                    }),
                  }}>
                  <CustomizeEmojiContext.Provider
                    value={{
                      onOpenAdderModule: () => this.setState({
                        openAdderModule: TYPES_SPEC[2].type,
                      }),
                    }}>
                    <SwipeController currentType={currentType} />
                  </CustomizeEmojiContext.Provider>
                </CustomizeTextContext.Provider>
              ) : (
                <Field
                  name="images"
                  component={FixedImageSelection} />
              )}
            </div>
            <div
              style={[
                styles.konvaWrapper,
                isFixedFromApi && styles.fixedImageKonvaWrapper,
                currentStep !== 1 && {
                  justifyContent: 'center',
                },
              ]}>
              {isFixedFromApi ? (
                <div
                  style={[
                    styles.swipeWrapper,
                    styles.fixedImageResultWrapper,
                  ]}>
                  <Field
                    name="images"
                    component={FixedImageSelectionResults} />
                </div>
              ) : null}
              <Suspense fallback={<Loading />}>
                <PhoneKonva
                  createRef={(r) => { this.canvasLayer = r; }}
                  locked={currentStep !== 1}
                  canvasWidth={previewAreaWidth} />
              </Suspense>
            </div>
          </div>
          <div style={styles.operationBoardWrapper}>
            {this.operationRenderer()}
          </div>
          {openAdderModule ? (
            <div style={styles.adderModuleWrapper}>
              <FieldArray
                name="images"
                type={openAdderModule}
                onClose={() => this.setState({ openAdderModule: '' })}
                component={AdderModulePopup} />
            </div>
          ) : null}
        </div>
      </FormSection>
    );
  }
}

const reduxHook = connect(
  state => ({
    formValues: getFormValues(PHONE_CUSTOMIZE_FORM)(state),
    accessToken: state.Member.accessToken,
  }),
  dispatch => bindActionCreators({
    ...CustomizeActions,
    ...LightboxActions,
    ...MemberActions,
  }, dispatch)
);

export default reduxHook(
  radium(
    MainCustomizeBoard
  )
);
