// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
// config
import { TYPES_SPEC } from '../../../shared/Global.js';
// component
import TextAdderForm from '../../../components/Customize/Operations/TextAdderForm.jsx';
import EmojiSelections from '../../../components/Customize/EmojiSelections.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    padding: 12,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-end',
    position: 'relative',
  },
};

type Props = {
  fields: Object,
  type: String,
  onClose: Function,
};

class AdderModulePopup extends PureComponent<Props> {
  typeRenderer() {
    const {
      type,
      fields,
      onClose,
    } = this.props;

    switch (type) {
      case TYPES_SPEC[1].type: // text
        return ({
          style: {
            height: '100%',
            minHeight: 600,
            alignItems: 'center',
            justifyContent: 'center',
          },
          component: <TextAdderForm fields={fields} onClose={onClose} />,
        });
      case TYPES_SPEC[0].type:
      case TYPES_SPEC[2].type:
        return ({
          style: {
            height: '100%',
            minHeight: '100%',
            alignItems: 'center',
            justifyContent: 'flex-end',
          },
          component: <EmojiSelections fields={fields} onClose={onClose} />,
        });
      default:
        return ({
          style: null,
          component: null,
        });
    }
  }

  render() {
    const target = this.typeRenderer();

    return (
      <div
        style={[
          styles.wrapper,
          target.style || null,
        ]}>
        {target.component || null}
      </div>
    );
  }
}

export default radium(AdderModulePopup);
