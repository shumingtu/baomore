// @flow
/* eslint react/no-did-update-set-state: 0 */
import React, { Component, Fragment } from 'react';
import radium from 'radium';
import {
  FieldArray,
  Field,
  Fields,
} from 'redux-form';

// config
import Theme from '../../../styles/Theme.js';
import { TYPES_SPEC } from '../../../shared/Global.js';
// components
import BooleanSelectionBlock from '../../../components/Customize/Operations/BooleanSelectionBlock.jsx';
import ImageControllers from '../../../components/Customize/Operations/ImageControllers.jsx';
import SingleImageController from '../../../components/Customize/Operations/SingleImageController.jsx';
import DetailInfo from '../../../components/Customize/DetailInfo.jsx';
import CollapseButton from '../../../components/Global/CollapseButton.jsx';

const styles = {
  wrapper: {
    position: 'fixed',
    zIndex: 2,
    left: 0,
    bottom: 12,
    width: '100%',
    height: 'auto',
    transition: 'bottom 0.2s ease-in',
  },
  container: {
    width: '100%',
    height: '100%',
    position: 'relative',
  },
  decoratorWrapper: {
    width: '100%',
    height: 'auto',
    padding: '0 5% 3px 5%',
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    position: 'relative',
  },
  detailBtnWrapper: {
    position: 'absolute',
    left: '5%',
    bottom: 4,
    width: 'auto',
    height: 'auto',
    zIndex: 4,
  },
  annotationWrapper: {
    flex: 1,
    paddingLeft: 20,
    textAlign: 'center',
    '@media (max-width: 413px)': {
      paddingLeft: 45,
    },
  },
  annotation: {
    fontSize: 12,
    padding: '0 6px',
    letterSpacing: 0.1,
    color: Theme.DEFAULT_TEXT_COLOR,
    opacity: 0,
    transition: 'opacity 0.2s ease-in',
  },
  showAnnotation: {
    opacity: 1,
  },
  mainContainer: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
};

type Props = {
  currentType: string,
  isFixedFromApi: boolean,
};

type State = {
  isCollapse: boolean,
};

class OperationBoard extends Component<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      isCollapse: false,
    };

    this.operationBoardRef = React.createRef();
  }

  componentDidUpdate(prevProps) {
    const {
      currentType,
    } = this.props;

    if (currentType !== prevProps.currentType && currentType) {
      this.setState({
        isCollapse: false,
      });
    }
  }

  operationBoardRef: { current: null | HTMLDivElement }

  typeComponentRenderer() {
    const {
      currentType,
    } = this.props;

    switch (currentType) {
      case TYPES_SPEC[0].type:
        return (
          <Fragment>
            <FieldArray
              name="images"
              type="image"
              component={ImageControllers} />
          </Fragment>
        );

      case TYPES_SPEC[1].type:
        return (
          <Fragment>
            <FieldArray
              name="images"
              type="text"
              component={ImageControllers} />
          </Fragment>
        );

      case TYPES_SPEC[2].type:
        return (
          <Fragment>
            <FieldArray
              name="images"
              type="emoji"
              component={ImageControllers} />
          </Fragment>
        );
      default:
        return null;
    }
  }

  render() {
    const {
      currentType,
      isFixedFromApi,
    } = this.props;

    const {
      isCollapse,
    } = this.state;

    const wrapBoardHeight = (this.operationBoardRef && this.operationBoardRef.current
      && this.operationBoardRef.current.clientHeight) || 0;

    return (
      <div
        style={[
          styles.wrapper,
          isCollapse && wrapBoardHeight ? {
            bottom: ((-1) * wrapBoardHeight) + 21,
          } : null,
        ]}>
        <div style={styles.container}>
          <div style={styles.decoratorWrapper}>
            <div style={styles.detailBtnWrapper}>
              <Field
                name="description"
                component={DetailInfo} />
            </div>
            <div style={styles.annotationWrapper}>
              <span
                style={[
                  styles.annotation,
                  isCollapse && styles.showAnnotation,
                ]}>
                請拖動手機上的圖片移動到理想位置
              </span>
            </div>
            <CollapseButton
              isCollapse={isCollapse}
              onClick={() => this.setState({ isCollapse: !isCollapse })} />
          </div>
          <div
            ref={this.operationBoardRef}
            style={styles.mainContainer}>
            {!isFixedFromApi ? (
              <Fragment>
                {this.typeComponentRenderer()}
              </Fragment>
            ) : (
              <Fields
                scalable={false}
                names={[
                  'images.rotate',
                  'images.scale',
                ]}
                type="fixed"
                component={SingleImageController} />
            )}
            <Field
              type={isFixedFromApi ? 'fixed' : currentType}
              name="needWhiteBg"
              label="是否使用白底"
              component={BooleanSelectionBlock} />
          </div>
        </div>
      </div>
    );
  }
}

export default radium(
  OperationBoard
);
