import React, {
  PureComponent,
  lazy,
  Suspense,
} from 'react';
import radium from 'radium';
import {
  FormSection,
  Field,
} from 'redux-form';
// components
import Loading from '../../../components/Global/Loading.jsx';
import DetailInfo from '../../../components/Customize/DetailInfo.jsx';
// lazily component
const PayCheckForm = lazy(() => import('../../../components/Customize/Pay/PayCheckForm.jsx'));

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: 12,
    position: 'relative',
  },
  detailBtnPlacement: {
    width: '100%',
    height: 1,
    position: 'relative',
  },
  detailBtnWrapper: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    zIndex: 4,
  },
};

class PayCheckBoard extends PureComponent {
  render() {
    return (
      <div style={styles.wrapper}>
        <div style={styles.detailBtnPlacement}>
          <div style={styles.detailBtnWrapper}>
            <Field
              name="description"
              component={DetailInfo} />
          </div>
        </div>
        <FormSection name="Pay">
          <Suspense fallback={<Loading />}>
            <PayCheckForm />
          </Suspense>
        </FormSection>
      </div>
    );
  }
}

export default radium(PayCheckBoard);
