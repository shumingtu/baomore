// @flow
import React, { Component, Fragment } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  FieldArray,
  Field,
  Fields,
} from 'redux-form';

// actions
import * as CustomizeActions from '../../../actions/Customize.js';
// config
import { TYPES_SPEC } from '../../../shared/Global.js';
// components
import TypeSwitcher from '../../../components/Customize/Operations/TypeSwitcher.jsx';
import SwipeImages from '../../../components/Customize/Operations/SwipeImages.jsx';
import BooleanSelectionBlock from '../../../components/Customize/Operations/BooleanSelectionBlock.jsx';
import ImageControllers from '../../../components/Customize/Operations/ImageControllers.jsx';
import SingleImageController from '../../../components/Customize/Operations/SingleImageController.jsx';
import TextAdderForm from '../../../components/Customize/Operations/TextAdderForm.jsx';
import SwipeText from '../../../components/Customize/Operations/SwipeText.jsx';
import SwipeEmoji from '../../../components/Customize/Operations/SwipeEmoji.jsx';
import EmojiSelections from '../../../components/Customize/EmojiSelections.jsx';
import FixedImageSelection from '../../../components/Customize/FixedImageSelection.jsx';
import SingleInfo from '../../../components/Form/SingleInfo.jsx';
import CheckoutButton from '../../../components/Customize/Operations/CheckoutButton.jsx';
import Checkbox from '../../../components/Form/Checkbox.jsx';
import StoreJsonButton from '../../../components/Customize/Operations/StoreJsonButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  submitWrapper: {
    width: '100%',
    height: 'auto',
    paddingTop: 48,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  submit: {
    width: 240,
    height: 40,
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    paddingTop: 32,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  storeJsonWrapper: {
    width: 'auto',
    height: 'auto',
    '@media (max-width: 1023px)': {
      display: 'none',
    },
  },
  publicWrapper: {
    width: 140,
    height: '100%',
    marginLeft: 12,
  },
};

type Props = {
  initializeForm: Function,
  setCurrentSelection: Function,
  onComplete: Function,
  isFixedFromApi: boolean,
  canUsePublicBtn: boolean,
};

type State = {
  currentType: string,
};

class OperationBoard extends Component<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      currentType: TYPES_SPEC[0].type,
    };
  }

  typeComponentRenderer() {
    const {
      currentType,
    } = this.state;

    switch (currentType) {
      case TYPES_SPEC[0].type:
        return (
          <Fragment>
            <FieldArray
              name="images"
              component={SwipeImages} />
            <FieldArray
              name="images"
              type="image"
              component={ImageControllers} />
          </Fragment>
        );

      case TYPES_SPEC[1].type:
        return (
          <Fragment>
            <FieldArray
              name="images"
              component={SwipeText} />
            <FieldArray
              name="images"
              type="text"
              component={ImageControllers} />
            <FieldArray
              name="images"
              component={TextAdderForm} />
          </Fragment>
        );

      case TYPES_SPEC[2].type:
        return (
          <Fragment>
            <FieldArray
              name="images"
              component={SwipeEmoji} />
            <FieldArray
              name="images"
              type="emoji"
              component={ImageControllers} />
            <FieldArray
              name="images"
              component={EmojiSelections} />
          </Fragment>
        );
      default:
        return null;
    }
  }

  render() {
    const {
      isFixedFromApi,
      onComplete,
      canUsePublicBtn,
    } = this.props;

    const {
      currentType,
    } = this.state;

    return (
      <div style={styles.wrapper}>
        {!isFixedFromApi ? (
          <Fragment>
            <TypeSwitcher
              types={TYPES_SPEC}
              currentType={currentType}
              onChange={t => this.setState({ currentType: t })} />
            {this.typeComponentRenderer()}
          </Fragment>
        ) : (
          <Fragment>
            <Field
              name="images"
              component={FixedImageSelection} />
            <Fields
              scalable={false}
              names={[
                'images.rotate',
                'images.scale',
              ]}
              type="fixed"
              component={SingleImageController} />
          </Fragment>
        )}
        <Field
          name="needWhiteBg"
          label="是否使用白底"
          component={BooleanSelectionBlock} />
        <Field
          name="price"
          label="售價"
          component={SingleInfo} />
        <CheckoutButton onClick={onComplete} />
        <div style={styles.functionWrapper}>
          <div style={styles.storeJsonWrapper}>
            <StoreJsonButton />
          </div>
          {canUsePublicBtn ? (
            <div style={styles.publicWrapper}>
              <Field
                showReminderWhenChecked
                name="isPublic"
                label="同意公開上架使用"
                component={Checkbox} />
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}

const reduxHook = connect(
  () => ({}),
  dispatch => bindActionCreators({
    ...CustomizeActions,
  }, dispatch)
);

export default reduxHook(
  radium(
    OperationBoard
  )
);
