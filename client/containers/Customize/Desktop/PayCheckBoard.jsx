// @flow
import React, {
  PureComponent,
  lazy,
  Suspense,
} from 'react';
import radium from 'radium';
import {
  Field,
  FormSection,
} from 'redux-form';
// components
import Loading from '../../../components/Global/Loading.jsx';
import SingleInfo from '../../../components/Form/SingleInfo.jsx';
import MakeOrderButton from '../../../components/Customize/Pay/MakeOrderButton.jsx';
import Checkbox from '../../../components/Form/Checkbox.jsx';
// lazily component
const PayCheckForm = lazy(() => import('../../../components/Customize/Pay/PayCheckForm.jsx'));

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingBottom: 24,
  },
  functionWrapper: {
    width: '100%',
    height: 'auto',
    paddingTop: 32,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  publicWrapper: {
    width: 140,
    height: '100%',
    marginLeft: 12,
  },
};

type Props = {
  canUsePublicBtn: boolean,
};

class PayCheckBoard extends PureComponent<Props> {
  render() {
    const {
      canUsePublicBtn,
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <FormSection name="Pay">
          <Suspense fallback={<Loading />}>
            <PayCheckForm />
          </Suspense>
        </FormSection>
        <Field
          name="price"
          label="售價"
          component={SingleInfo} />
        <MakeOrderButton />
        <div style={styles.functionWrapper}>
          {canUsePublicBtn ? (
            <div style={styles.publicWrapper}>
              <Field
                showReminderWhenChecked
                name="isPublic"
                label="同意公開上架使用"
                component={Checkbox} />
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}

export default radium(PayCheckBoard);
