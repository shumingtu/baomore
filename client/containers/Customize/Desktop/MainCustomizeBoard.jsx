// @flow
import React, {
  Component,
  lazy,
  Suspense,
} from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Field,
  FormSection,
  getFormValues,
} from 'redux-form';
import { Mutation } from 'react-apollo';

import { PHONE_CUSTOMIZE_FORM } from '../../../shared/Form.js';
import {
  canvasJsonToString,
  isMobileMode,
} from '../../../helper/canvasHelper.js';
import { uploadCanvas } from '../../../helper/uploader.js';
import { STORE_MEMBER_JSON } from '../../../mutations/Member.js';
import {
  STATUS_FAILED,
} from '../../../shared/StatusTypes.js';
import {
  LINE_LOGIN_LIGHTBOX,
} from '../../../shared/LightboxTypes.js';
// config
import {
  CANVAS_WIDTH,
  MOBILE_CANVAS_WIDTH,
  IMAGE_WIDTH_SPEC,
} from '../../../shared/PhoneConfig.js';
// actions
import * as CustomizeActions from '../../../actions/Customize.js';
import * as LightboxActions from '../../../actions/Lightbox.js';
import * as MemberActions from '../../../actions/Member.js';
// components
import Loading from '../../../components/Global/Loading.jsx';
import Title from '../../../components/Customize/Title.jsx';
import BackLink from '../../../components/Global/BackLink.jsx';
import DetailInfo from '../../../components/Customize/DetailInfo.jsx';
// lazily components
const PhoneKonva = lazy(() => import('../../../components/Customize/PhoneKonva.jsx'));
const OperationBoard = lazy(() => import('./OperationBoard.jsx'));
const PayCheckBoard = lazy(() => import('./PayCheckBoard.jsx'));


const styles = {
  wrapper: {
    width: '100%',
    minWidth: 768,
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  mainBoard: {
    width: '100%',
    flex: 1,
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  phoneModuleBlock: {
    flex: 1,
    height: '100%',
    position: 'relative',
    padding: '0 0 0 4px',
  },
  backLinkWrapper: {
    width: '100%',
    height: 'auto',
    padding: '4px 6px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  konvaWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '20px 0',
    position: 'relative',
  },
  detailBtnWrapper: {
    position: 'absolute',
    left: 0,
    top: 0,
    zIndex: 4,
    width: 'auto',
    height: 'auto',
  },
  operationBlock: {
    width: 380,
    height: 'auto',
    maxHeight: 'calc(100vh - 300px)',
    padding: '0 16px',
    overflow: 'auto',
    position: 'relative',
  },
};

type Props = {
  title: string,
  isFixedFromApi: boolean,
  clearAllSelectedCache: Function,
  formValues: Object,
  isEditMode: boolean,
  accessToken: string,
  openStatusBox: Function,
  openLightbox: Function,
  cacheMemberCustomization: Function,
  cacheCustomizeUrl: Function,
};

type State = {
  currentStep: number,
};

class MainCustomizeBoard extends Component<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      currentStep: 1,
    };
    this.isMount = false;
    this.canvasLayer = React.createRef();
  }

  componentDidMount() {
    this.isMount = true;
  }

  componentWillUnmount() {
    const {
      clearAllSelectedCache,
    } = this.props;

    this.isMount = false;
    clearAllSelectedCache();
  }

  canvasLayer: { current: null | HTMLDivElement }

  isMount: boolean

  goNextStep() {
    const {
      currentStep,
    } = this.state;

    this.setState({
      currentStep: currentStep + 1,
    });
  }

  uploadMyCanvas() {
    const {
      cacheCustomizeUrl,
    } = this.props;

    const canvasWidth = isMobileMode() ? MOBILE_CANVAS_WIDTH : CANVAS_WIDTH;
    const pixelRatio = IMAGE_WIDTH_SPEC / canvasWidth;

    if (this.canvasLayer) {
      const myCanvas = this.canvasLayer.getLayer().toCanvas({
        pixelRatio,
      });

      myCanvas.toBlob(blob => uploadCanvas(blob, (obj) => {
        if (!obj.isPreview) {
          cacheCustomizeUrl(obj.src);
        }
      }), 'image/png');
    }
  }

  stepComponentRenderer() {
    const {
      isFixedFromApi,
      formValues,
      accessToken,
      openStatusBox,
      openLightbox,
      cacheMemberCustomization,
    } = this.props;

    const {
      currentStep,
    } = this.state;

    const canUsePublicBtn = formValues && formValues.Main
      ? formValues.Main.customizeCategory === '客製化包膜'
      : false;

    switch (currentStep) {
      case 1:
        return (
          <Mutation mutation={STORE_MEMBER_JSON}>
            {storeJSON => (
              <Suspense fallback={<Loading />}>
                <OperationBoard
                  isFixedFromApi={isFixedFromApi}
                  canUsePublicBtn={canUsePublicBtn}
                  onComplete={async () => {
                    this.uploadMyCanvas();

                    const newData = canvasJsonToString(formValues);

                    cacheMemberCustomization(newData);

                    if (accessToken) {
                      const {
                        data,
                      } = await storeJSON({
                        variables: {
                          json: newData,
                        },
                      });

                      if (data) {
                        if (this.isMount) {
                          this.goNextStep();
                        }
                      } else {
                        openStatusBox({
                          type: STATUS_FAILED,
                          label: '操作失敗',
                        });
                      }
                    } else {
                      openLightbox({
                        type: LINE_LOGIN_LIGHTBOX,
                      });
                    }
                  }} />
              </Suspense>
            )}
          </Mutation>
        );
      case 2:
        return (
          <Suspense fallback={<Loading />}>
            <PayCheckBoard
              canUsePublicBtn={canUsePublicBtn} />
          </Suspense>
        );
      default:
        return null;
    }
  }

  render() {
    const {
      title = null,
      isEditMode,
    } = this.props;

    const {
      currentStep,
    } = this.state;

    return (
      <FormSection name="Customize">
        <div style={styles.wrapper}>
          <Title title={title} />
          <div style={styles.mainBoard}>
            <div style={styles.phoneModuleBlock}>
              <div style={styles.backLinkWrapper}>
                {!isEditMode ? (
                  <BackLink
                    label="選擇包膜產品"
                    path="/customize/category" />
                ) : null}
              </div>
              <div style={styles.konvaWrapper}>
                <div style={styles.detailBtnWrapper}>
                  <Field
                    name="description"
                    component={DetailInfo} />
                </div>
                <Suspense fallback={<Loading />}>
                  <PhoneKonva
                    createRef={(r) => { this.canvasLayer = r; }}
                    locked={currentStep !== 1} />
                </Suspense>
              </div>
            </div>
            <div
              className="hide-scrollbar"
              style={styles.operationBlock}>
              {this.stepComponentRenderer()}
            </div>
          </div>
        </div>
      </FormSection>
    );
  }
}

const reduxHook = connect(
  state => ({
    formValues: getFormValues(PHONE_CUSTOMIZE_FORM)(state),
    accessToken: state.Member.accessToken,
  }),
  dispatch => bindActionCreators({
    ...CustomizeActions,
    ...LightboxActions,
    ...MemberActions,
  }, dispatch)
);

export default reduxHook(
  radium(
    MainCustomizeBoard
  )
);
