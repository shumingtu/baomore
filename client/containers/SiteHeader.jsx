// @flow
import React, { memo, Fragment } from 'react';
import radium from 'radium';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  NavLink,
} from 'react-router-dom';

// theme
import Theme from '../styles/Theme.js';
// static
import artmoLogo from '../static/images/landing/logo.jpg';
import menuIcon from '../static/images/menu-icon.png';
import closeIcon from '../static/images/close-purple-icon.png';
import memberIcon from '../static/images/landing/member-icon.png';
import {
  SITE_HEADER_NAVS,
} from '../shared/Global.js';
import * as GlobalActions from '../actions/Global.js';
// components
import StoreJsonButton from '../components/Customize/Operations/StoreJsonButton.jsx';

const Link = radium(NavLink);

const styles = {
  wrapper: {
    width: '100%',
    height: 60,
    padding: '8px 18px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 1023px)': {
      padding: '8px 18px',
    },
  },
  mainPlacement: {
    width: 'auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 1023px)': {
      width: '100%',
    },
  },
  logoPlacement: {
    width: 36,
    height: 36,
    '@media (max-width: 1023px)': {
      flex: 1,
      width: 'auto',
      height: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  },
  logoWrapper: {
    width: 36,
    height: 36,
    textDecoration: 'none',
    outline: 0,
    border: 0,
    cursor: 'pointer',
  },
  logo: {
    width: 36,
    height: 36,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundImage: `url(${artmoLogo})`,
  },
  navPlacement: {
    flex: 1,
    height: '100%',
    padding: '0 36px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 1023px)': {
      display: 'none',
    },
  },
  navWrapper: {
    width: 600,
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  nav: {
    width: 'auto',
    height: 'auto',
    padding: '8px 6px',
    fontSize: 16,
    color: Theme.SITE_HEADER_NAV_COLOR,
    textDecoration: 'none',
    border: 0,
    outline: 0,
    cursor: 'pointer',
  },
  navActive: {
    color: Theme.SITE_HEADER_NAV_ACTIVE,
  },
  iconWrapper: {
    width: 18,
    height: 18,
    margin: '0 12px 0 0',
    outline: 0,
    padding: 0,
    border: 0,
    backgroundColor: 'transparent',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    cursor: 'pointer',
    ':hover': {
      opacity: 0.88,
    },
  },
  showInDesktop: {
    display: 'block',
    '@media (max-width: 1023px)': {
      display: 'none',
    },
  },
  showInMobile: {
    display: 'none',
    '@media (max-width: 1023px)': {
      display: 'block',
    },
  },
  functionWrapper: {
    width: 36,
    height: 36,
    border: 0,
    borderRadius: 18,
    outline: 0,
    padding: 0,
    backgroundColor: 'transparent',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    cursor: 'pointer',
    ':hover': {
      opacity: 0.8,
    },
  },
  mobileFunctionWrapper: {
    display: 'none',
    '@media (max-width: 1023px)': {
      width: 'auto',
      height: 'auto',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      margin: '0 12px 0 0',
    },
  },
  hideInMobileCustomize: {
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
};

type Props = {
  isCustomizing: boolean,
  isOpened: boolean,
  onClose: Function,
  openMobileExtendNav: Function,
  memberAvatar: string,
  accessToken: string,
  history: {
    push: Function,
  },
};

function SiteHeader({
  isCustomizing,
  isOpened,
  onClose,
  openMobileExtendNav,
  history,
  memberAvatar,
}: Props) {
  return (
    <div
      style={[
        styles.wrapper,
        isCustomizing && styles.hideInMobileCustomize,
      ]}>
      <div style={styles.mainPlacement}>
        {isOpened ? (
          <button
            key="mobile-close-button"
            type="button"
            onClick={onClose || null}
            style={[
              styles.iconWrapper,
              styles.showInMobile,
              {
                backgroundImage: `url(${closeIcon})`,
              },
            ]} />
        ) : (
          <button
            key="mobile-menu-button"
            type="button"
            onClick={() => openMobileExtendNav()}
            style={[
              styles.iconWrapper,
              styles.showInMobile,
              {
                backgroundImage: `url(${menuIcon})`,
              },
            ]} />
        )}
        <div style={styles.logoPlacement}>
          <Link
            to={{ pathname: '/' }}
            style={styles.logoWrapper}>
            <div style={styles.logo} />
          </Link>
        </div>
        <div style={styles.navPlacement}>
          <div style={styles.navWrapper}>
            {SITE_HEADER_NAVS.map((nav) => {
              const overridePath = nav.path === '/member' && !memberAvatar ? '/member/login' : nav.path;

              return (
                <Link
                  key={nav.path}
                  className="header-nav-link"
                  to={{ pathname: overridePath }}
                  style={styles.nav}
                  activeStyle={styles.navActive}>
                  {nav.name}
                </Link>
              );
            })}
          </div>
        </div>
        {isCustomizing ? (
          <Fragment>
            <div style={styles.mobileFunctionWrapper}>
              <StoreJsonButton />
            </div>
          </Fragment>
        ) : null}
        <button
          key="member-button"
          type="button"
          onClick={() => {
            if (!memberAvatar) {
              return history.push('/member/login');
            }

            return history.push('/member');
          }}
          style={[
            styles.functionWrapper,
            {
              backgroundImage: memberAvatar ? `url(${memberAvatar})` : `url(${memberIcon})`,
            },
          ]} />
      </div>
    </div>
  );
}

const reduxHook = connect(
  state => ({
    memberAvatar: state.Member.memberAvatar,
  }),
  dispatch => bindActionCreators({
    ...GlobalActions,
  }, dispatch),
);

export default memo(
  reduxHook(
    radium(
      SiteHeader
    )
  )
);
