// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

// config
import navs from '../../shared/Customer/navs.js';
import {
  CUSTOMER_DISCLAIM,
  CUSTOMER_RIGHTS,
} from '../../shared/Customer/fragments.js';
// components
import SubNavBar from '../../components/Global/SubNavBar.jsx';
import RulePage from '../RulePage.jsx';
import QAPage from './QAPage.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
};

type Props = {
  pathId: string,
};

class CustomerMainBoard extends PureComponent<Props> {
  componentRenderer() {
    const {
      pathId,
    } = this.props;

    switch (pathId) {
      case 'qa':
        return (
          <QAPage />
        );
      case 'rights':
        return (
          <RulePage
            titles={[
              'artmo會員權益聲明',
            ]}
            rule={CUSTOMER_RIGHTS} />
        );
      case 'disclaim':
        return (
          <RulePage
            titles={[
              'artmo個人化',
              '使用條款及免責條款',
            ]}
            rule={CUSTOMER_DISCLAIM} />
        );
      default:
        return null;
    }
  }

  render() {
    return (
      <div style={styles.wrapper}>
        <SubNavBar
          disabledTitleLink
          title={{
            name: '客服資訊',
            path: null,
          }}
          navs={navs} />
        {this.componentRenderer()}
      </div>
    );
  }
}

export default radium(CustomerMainBoard);
