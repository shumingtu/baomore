// @flow
import React from 'react';
import radium from 'radium';
// config
import Theme from '../../styles/Theme.js';
import {
  CUSTOMER_QA,
} from '../../shared/Customer/fragments.js';
// components
import BlockTitle from '../../components/Description/BlockTitle.jsx';
import LinkButton from '../../components/Fragment/Element/LinkButton.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 48px 36px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      padding: '0 12px',
    },
  },
  titleWrapper: {
    width: '100%',
    height: 'auto',
    padding: '36px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
    },
  },
  title: {
    fontSize: 32,
    fontWeight: 700,
    color: Theme.BLACK_COLOR,
    letterSpacing: 3,
    textAlign: 'center',
    '@media (max-width: 767px)': {
      fontSize: 26,
    },
  },
  contentPlacement: {
    width: '100%',
    height: 'auto',
    padding: 48,
    backgroundColor: '#fff',
    borderRadius: 12,
    boxShadow: Theme.CLIENT_BLOCK_SHADOW,
    '@media (max-width: 767px)': {
      padding: '12px 0',
    },
  },
  contentWrapper: {
    width: '100%',
    height: 'auto',
    padding: '0 24px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  text: {
    fontSize: 18,
    fontWeight: 400,
    color: Theme.BLACK_COLOR,
    letterSpacing: 2,
    lineHeight: 1.4,
    whiteSpace: 'pre-line',
    '@media (max-width: 767px)': {
      fontSize: 16,
    },
  },
  categoryWrapper: {
    width: '100%',
    height: 'auto',
    marginTop: 24,
    borderTop: `1px solid ${Theme.BLACK_COLOR}`,
    padding: '6px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  questionWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  answerText: {
    fontSize: 16,
    fontWeight: 400,
    color: Theme.PURPLE_COLOR,
    letterSpacing: 2,
    lineHeight: 1.4,
    whiteSpace: 'pre-line',
    '@media (max-width: 767px)': {
      fontSize: 13,
    },
  },
  annotationWrapper: {
    width: '100%',
    height: 'auto',
    padding: 24,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
      padding: '48px 12px',
    },
  },
};

function QAPage() {
  return (
    <div style={styles.wrapper}>
      <div style={styles.titleWrapper}>
        <span style={styles.title}>
          artmo協助您解決疑問
        </span>
      </div>
      <div style={styles.contentPlacement}>
        <div style={styles.contentWrapper}>
          {CUSTOMER_QA.map(category => (
            <div key={category.title} style={styles.categoryWrapper}>
              <BlockTitle title={`【${category.title}】`} />
              {category.questions.map(q => (
                <div key={q.title} style={styles.questionWrapper}>
                  {q.title ? (
                    <span style={[styles.text, { margin: '6px 0' }]}>
                      {q.title}
                    </span>
                  ) : null}
                  {q.answer ? (
                    <p style={styles.answerText}>
                      {q.answer}
                    </p>
                  ) : null}
                </div>
              ))}
            </div>
          ))}
        </div>
      </div>
      <div style={styles.annotationWrapper}>
        <span style={styles.text}>
          沒有解決到您的問題嗎？
        </span>
        <LinkButton
          path="/contact"
          text="聯絡客服" />
      </div>
    </div>
  );
}

export default radium(QAPage);
