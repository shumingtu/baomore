// @flow

import React, { Component } from 'react';
import radium from 'radium';
import {
  Switch,
  Route,
} from 'react-router-dom';
// global
import SiteHeader from './SiteHeader.jsx';
import SiteFooter from './SiteFooter.jsx';
import MobileHeaderNav from './MobileHeaderNav.jsx';
import AuthorizedBrandList from './AuthorizedBrandList.jsx';
// routes
import Landing from './Landing/Landing.jsx';
import CustomizeRoutes from '../routes/Customize.jsx';
import PaymentRoutes from '../routes/Payment.jsx';
import IntroRoutes from '../routes/Introduction.jsx';
import AccessoryRoutes from '../routes/Accessory.jsx';
import AboutRoutes from '../routes/About.jsx';
import DescriptionRoutes from '../routes/Description.jsx';
import CustomerRoutes from '../routes/Customer.jsx';
import ContactRoutes from '../routes/Contact.jsx';
import ActivityRoutes from '../routes/Activity.jsx';
import MemberRoutes from '../routes/Member.jsx';
// global component
import MemberMeBoard from './MemberMeBoard.jsx';
import LightboxHandler from './LightboxHandler.jsx';
import StatusAnnotationHandler from './StatusAnnotationHandler.jsx';

const styles = {
  placement: {
    width: '100%',
    height: '100vh', // non support fallback
    height: 'calc(var(--vh, 1vh) * 100)', // fix ios fullscreen bug
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    position: 'relative',
  },
  areaWrapper: {
    width: '100%',
    height: 'auto',
    minHeight: '100vh', // non support fallback
    minHeight: 'calc(var(--vh, 1vh) * 100)', // fix ios fullscreen bug
    position: 'relative',
    overflow: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      padding: 0,
    },
  },
  mainWrapper: {
    width: '100%',
    flex: 1,
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
};

type Props = {

};

class MainBoard extends Component<Props> {
  componentDidUpdate() {
    const r = document.getElementById('root');

    if (r) {
      r.scrollTop = 0;
    }
  }

  render() {
    return (
      <div style={styles.placement}>
        <MemberMeBoard />
        <div id="main-area" style={styles.areaWrapper}>
          <Switch>
            <Route exact path="/customize/create" component={props => <SiteHeader {...props} isCustomizing />} />
            <Route path="/payment" component={props => <SiteHeader {...props} isCustomizing />} />
            <Route path="/" component={SiteHeader} />
          </Switch>
          <div style={styles.mainWrapper}>
            <Switch>
              <Route path="/payment" component={PaymentRoutes} />
              <Route path="/customize" component={CustomizeRoutes} />
              <Route path="/introduction" component={IntroRoutes} />
              <Route path="/accessories" component={AccessoryRoutes} />
              <Route path="/about" component={AboutRoutes} />
              <Route path="/description" component={DescriptionRoutes} />
              <Route path="/customer" component={CustomerRoutes} />
              <Route path="/contact" component={ContactRoutes} />
              <Route path="/activities" component={ActivityRoutes} />
              <Route path="/member" component={MemberRoutes} />
              <Route path="/" component={Landing} />
            </Switch>
          </div>
          <Switch>
            <Route exact path="/customize" component={AuthorizedBrandList} />
            <Route path="/payment" component={() => <AuthorizedBrandList hide />} />
            <Route path="/customize" component={() => <AuthorizedBrandList hide />} />
            <Route path="/" component={AuthorizedBrandList} />
          </Switch>
          <Switch>
            <Route exact path="/customize/create" component={() => <SiteFooter hideInMobile />} />
            <Route path="/payment" component={() => <SiteFooter hideInMobile />} />
            <Route path="/" component={SiteFooter} />
          </Switch>
        </div>
        <MobileHeaderNav />
        <LightboxHandler />
        <StatusAnnotationHandler />
      </div>
    );
  }
}

export default radium(
  MainBoard
);
