import React, { PureComponent } from 'react';
import radium from 'radium';
// config
import Theme from '../../styles/Theme.js';
import navs from '../../shared/Accessory/navs.js';
import { LANDING_FRAGMENTS } from '../../shared/Accessory/fragments.js';
// components
import AccessoryLandingNav from '../../components/Accessory/AccessoryLandingNav.jsx';
import CenterCoverImage, {
  CenterTextBlock,
} from '../../components/Fragment/CenterCoverImage.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingBottom: 48,
  },
  introNavWrapper: {
    width: '100%',
    height: 150,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
  fragmentsWrapper: {
    width: '100%',
    height: 'auto',
    backgroundColor: Theme.GRAY_COLOR,
  },
  fragmentWrapper: {
    width: '100%',
    height: 'auto',
    marginBottom: 8,
  },
};

class AccessoryLanding extends PureComponent {
  render() {
    return (
      <div style={styles.wrapper}>
        <div style={styles.introNavWrapper}>
          {navs.map(nav => (
            <AccessoryLandingNav
              key={nav.id}
              nav={nav} />
          ))}
        </div>
        <div style={styles.fragmentsWrapper}>
          {LANDING_FRAGMENTS.map(frag => (
            <div key={frag.title.name} style={styles.fragmentWrapper}>
              <CenterCoverImage
                key={`${frag.title.name}-image`}
                width="100%"
                height={712}
                mobileWidth="100%"
                mobileHeight={600}
                desktopImg={frag.desktopImg}
                mobileImg={frag.mobileImg}>
                <CenterTextBlock fragment={frag} />
              </CenterCoverImage>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default radium(AccessoryLanding);
