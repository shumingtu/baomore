// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import {
  Switch,
  Route,
} from 'react-router-dom';

// components
import IntroTypePage from '../Introduction/IntroTypePage.jsx';
import SubNavBar from '../../components/Global/SubNavBar.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
};

type Props = {
  title: {
    path: string,
    name: string,
  },
  fragments: {
    fragment1: Object,
    fragment2: Object,
    fragment3: Object,
    fragment4: Object,
    fragment5: Object,
    fragment6: Array<Object>,
    fragment7: Object,
    fragment8: Object,
    fragment9: Object,
    fragment10: Object,
    fragment11: Object,
  },
  match: {
    url: string,
  },
};

class AccessoryMainBoard extends PureComponent<Props> {
  render() {
    const {
      title,
      fragments,
      match: {
        url,
      },
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <SubNavBar
          title={title}
          navs={[]} />
        <Switch>
          <Route
            path={`${url}`}
            component={props => (
              <IntroTypePage
                {...props}
                fragments={fragments} />
            )} />
        </Switch>
      </div>
    );
  }
}

export default radium(AccessoryMainBoard);
