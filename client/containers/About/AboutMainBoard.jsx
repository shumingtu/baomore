// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

// config
import navs from '../../shared/About/navs.js';
import {
  ABOUT_TITLE,
  STORY_FRAGMENTS,
  PRIVACY_RULES,
} from '../../shared/About/fragments.js';
// components
import SubNavBar from '../../components/Global/SubNavBar.jsx';
import StoryPage from './StoryPage.jsx';
import RulePage from '../RulePage.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
};

type Props = {
  pathId: string,
};

class AboutMainBoard extends PureComponent<Props> {
  componentRenderer() {
    const {
      pathId,
    } = this.props;

    switch (pathId) {
      case 'story':
        return (
          <StoryPage
            fragments={STORY_FRAGMENTS} />
        );
      case 'privacy':
        return (
          <RulePage
            titles={[
              'artmo',
              '隱私權條款',
            ]}
            rule={PRIVACY_RULES} />
        );
      default:
        return null;
    }
  }

  render() {
    return (
      <div style={styles.wrapper}>
        <SubNavBar
          disabledTitleLink
          title={ABOUT_TITLE}
          navs={navs} />
        {this.componentRenderer()}
      </div>
    );
  }
}

export default radium(AboutMainBoard);
