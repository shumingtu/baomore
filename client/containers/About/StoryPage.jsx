// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
// components
import ResizableImage from '../../components/Fragment/ResizableImage.jsx';
import StoryFragment2 from '../../components/About/StoryFragment2.jsx';
import StoryFragment3 from '../../components/About/StoryFragment3.jsx';

const styles = {
  fragmentsWrapper: {
    width: '100%',
    height: 'auto',
  },
  fragmentWrapper: {
    width: '100%',
    height: 'auto',
    marginBottom: 16,
  },
};

type Props = {
  fragments: {
    fragment1: Object,
    fragment2: Object,
    fragment3: Array<{
      title: string,
      desktopImg: string,
      mobileImg: string,
    }>,
  },
};

class StoryPage extends PureComponent<Props> {
  render() {
    const {
      fragments,
    } = this.props;

    return (
      <div style={styles.fragmentsWrapper}>
        {fragments.fragment1 ? (
          <div style={styles.fragmentWrapper}>
            <ResizableImage
              desktopRatio={0.556}
              mobileRatio={1.5}
              desktopImg={fragments.fragment1.desktopImg}
              mobileImg={fragments.fragment1.mobileImg} />
          </div>
        ) : null}
        {fragments.fragment2 ? (
          <div style={styles.fragmentWrapper}>
            <StoryFragment2 fragment={fragments.fragment2} />
          </div>
        ) : null}
        {fragments.fragment3 ? (
          <div style={styles.fragmentWrapper}>
            <StoryFragment3 fragment={fragments.fragment3} />
          </div>
        ) : null}
      </div>
    );
  }
}

export default radium(StoryPage);
