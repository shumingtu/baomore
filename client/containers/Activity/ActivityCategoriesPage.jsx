// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';

import Theme from '../../styles/Theme.js';
import { FETCH_SOCIAL_ACTIVITY_CATEGORIES } from '../../queries/Activity.js';
// components
import CenterCoverImage from '../../components/Fragment/CenterCoverImage.jsx';
import Slogan from '../../components/Fragment/Element/Slogan.jsx';
import LinkButton from '../../components/Fragment/Element/LinkButton.jsx';
import FrameHeavyLine from '../../components/Global/FrameHeavyLine.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    paddingBottom: 48,
  },
  bannerTextWrapper: {
    width: '100%',
    height: '100%',
    padding: '24px 64px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      padding: '64px 12px 12px',
      alignItems: 'center',
      justifyContent: 'flex-start',
    },
  },
  textWrapper: {
    width: '100%',
    height: 'auto',
    padding: '48px 64px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderTop: `8px solid ${Theme.GRAY_COLOR}`,
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
  text: {
    fontSize: 22,
    fontWeight: 400,
    color: Theme.BLACK_COLOR,
    letterSpacing: 2,
    lineHeight: 1.6,
    textAlign: 'center',
    whiteSpace: 'pre-line',
  },
  otherActivitiesWrapper: {
    width: '100%',
    height: 'auto',
    padding: 48,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderTop: `2px solid ${Theme.YELLOW_COLOR}`,
    borderBottom: `2px solid ${Theme.PURPLE_COLOR}`,
    position: 'relative',
    overflow: 'hidden',
    '@media (max-width: 767px)': {
      marginTop: 12,
    },
  },
  activitiesPlacement: {
    width: '100%',
    maxWidth: 768,
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
      justifyContent: 'flex-start',
    },
  },
  activityLink: {
    width: 300,
    height: 300,
    margin: '12px 0',
    borderRadius: 12,
    boxShadow: Theme.CLIENT_BLOCK_SHADOW,
    backgroundColor: '#fff',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    textDecoration: 'none',
    textAlign: 'center',
    border: 0,
    fontSize: 22,
    fontWeight: 700,
    color: Theme.BLACK_COLOR,
    letterSpacing: 1.5,
    '@media (max-width: 767px)': {
      width: 240,
      height: 240,
    },
  },
};

type Props = {
  match: {
    url: string,
  },
  activityCategories: Array<{
    id: number,
    name: string,
    desktopImg: string,
    mobileImg: string,
  }>,
};

class ActivityCategoriesPage extends PureComponent<Props> {
  render() {
    const {
      activityCategories = [],
      match: {
        url,
      },
    } = this.props;

    return (
      <div style={styles.wrapper}>
        {activityCategories.map(c => (
          <CenterCoverImage
            key={c.id}
            width="100%"
            height={400}
            mobileWidth="100%"
            mobileHeight={582}
            desktopImg={c.desktopImg}
            mobileImg={c.mobileImg}>
            <div style={styles.bannerTextWrapper}>
              {c.name ? (
                <Slogan
                  slogan={{
                    color: '#000',
                    name: c.name,
                  }} />
              ) : null}
              <LinkButton
                text="了解更多"
                path={`${url}/${c.id}`} />
            </div>
          </CenterCoverImage>
        ))}
        <div
          style={[
            styles.textWrapper,
            !activityCategories.length && {
              borderTop: 0,
            },
          ]}>
          <span style={styles.text}>
            {`全省涵蓋，超值活動不斷，提供線上及線下專屬活動，
              依照您的需求，給您選擇`}
          </span>
        </div>
        <Query query={FETCH_SOCIAL_ACTIVITY_CATEGORIES}>
          {({ data }) => {
            if (!data) return null;

            const {
              socialCategories = [],
            } = data;

            const results = socialCategories.filter(x => x.link);

            if (!results.length) return null;

            return (
              <div style={styles.otherActivitiesWrapper}>
                <FrameHeavyLine
                  type="leftTop"
                  color={Theme.YELLOW_COLOR} />
                <div style={styles.activitiesPlacement}>
                  {results.map(x => (
                    <a
                      rel="noopener noreferrer"
                      key={x.id}
                      href={x.link}
                      style={styles.activityLink}
                      target="_blank">
                      {x.name}
                    </a>
                  ))}
                </div>
                <FrameHeavyLine
                  type="rightBottom"
                  color={Theme.PURPLE_COLOR} />
              </div>
            );
          }}
        </Query>
      </div>
    );
  }
}

export default radium(ActivityCategoriesPage);
