// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';
import {
  Switch,
  Route,
} from 'react-router-dom';
// static
// components
import ActivityCategoriesPage from './ActivityCategoriesPage.jsx';
import ActivityListMainPage from './ActivityListMainPage.jsx';
import SubNavBar from '../../components/Global/SubNavBar.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
};

type Props = {
  activityCategories: Array<{
    id: number,
    name: string,
    desktopImg: string,
    mobileImg: string,
  }>,
  match: {
    url: string,
  },
};

class ActivityMainBoard extends PureComponent<Props> {
  render() {
    const {
      activityCategories = [],
      match: {
        url,
      },
    } = this.props;

    return (
      <div style={styles.wrapper}>
        <SubNavBar
          title={{
            name: '最新消息',
            path: url,
          }}
          navs={activityCategories.map(c => ({
            id: c.id,
            name: c.name,
            path: `${url}/${c.id}`,
          }))} />
        <Switch>
          {activityCategories.map(c => (
            <Route
              key={c.id}
              path={`${url}/${c.id}`}
              component={props => (
                <ActivityListMainPage
                  {...props}
                  title={c.name}
                  categoryId={c.id} />
              )} />
          ))}
          <Route
            path={`${url}`}
            component={props => (
              <ActivityCategoriesPage
                {...props}
                activityCategories={activityCategories} />
            )} />
        </Switch>
      </div>
    );
  }
}

export default radium(ActivityMainBoard);
