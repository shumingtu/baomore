// @flow
import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { Query } from 'react-apollo';

import { GET_ACTIVITY_LIST } from '../../queries/Activity.js';
import ActivityListPage from './ActivityListPage.jsx';
import ActivityDetailPage from './ActivityDetailPage.jsx';

type Props = {
  title: string,
  categoryId: number,
  match: {
    url: string,
  },
};

function ActivityListMainPage({
  title,
  categoryId,
  match: {
    url,
  },
}: Props) {
  // query by categoryId
  return (
    <Query
      query={GET_ACTIVITY_LIST}
      variables={{
        categoryId: parseInt(categoryId, 10),
        status: true,
      }}>
      {({
        data: {
          activities = [],
        },
      }) => (
        <Switch>
          {activities.map(l => (
            <Route
              key={`${l.id}-detail`}
              path={`${url}/${l.id}`}
              component={props => (
                <ActivityDetailPage
                  {...props}
                  activityId={l.id} />
              )} />
          ))}
          <Route
            path={url}
            component={props => (
              <ActivityListPage
                {...props}
                title={title}
                list={activities} />
            )} />
        </Switch>
      )}
    </Query>
  );
}

export default ActivityListMainPage;
