// @flow
import React from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';
// static
import Theme from '../../styles/Theme.js';
import { GET_ACTIVITY_DETAIL } from '../../queries/Activity.js';
// components
import ResizableImage from '../../components/Fragment/ResizableImage.jsx';
import FrameHeavyLine from '../../components/Global/FrameHeavyLine.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    paddingBottom: 48,
  },
  infoWrapper: {
    width: '100%',
    height: 'auto',
    marginTop: 6,
    padding: '48px 64px',
    borderTop: `2px solid ${Theme.YELLOW_COLOR}`,
    borderBottom: `2px solid ${Theme.PURPLE_COLOR}`,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    position: 'relative',
    overflow: 'hidden',
    '@media (max-width: 767px)': {
      padding: '36px 16px',
      alignItems: 'center',
    },
  },
  infoPlacement: {
    width: '100%',
    maxWidth: 768,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  title: {
    fontSize: 36,
    fontWeight: 700,
    letterSpacing: 4,
    padding: '16px 0',
    color: Theme.PURPLE_COLOR,
    '@media (max-width: 767px)': {
      fontSize: 24,
      textAlign: 'center',
    },
  },
  fragmentWrapper: {
    width: 'auto',
    height: 'auto',
    minWidth: 480,
    maxWidth: 640,
    padding: '16px 0',
    borderBottom: `1px solid ${Theme.BLACK_COLOR}`,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      minWidth: '100%',
    },
  },
  noBorder: {
    borderBottom: 0,
  },
  noLimit: {
    minWidth: 'auto',
    maxWidth: 'auto',
  },
  fragmentTitle: {
    fontSize: 18,
    fontWeight: 400,
    color: Theme.BLACK_COLOR,
    letterSpacing: 1,
    '@media (max-width: 767px)': {
      fontSize: 16,
    },
  },
  fragmentDesc: {
    fontSize: 16,
    fontWeight: 400,
    color: Theme.BLACK_COLOR,
    letterSpacing: 1,
    lineHeight: 1.6,
    padding: '12px 0',
    whiteSpace: 'pre-line',
    '@media (max-width: 767px)': {
      fontSize: 13,
    },
  },
};

type Props = {
  activityId: number,
};

function ActivityDetailPage({
  activityId,
}: Props) {
  return (
    <Query
      query={GET_ACTIVITY_DETAIL}
      variables={{
        activityId: parseInt(activityId, 10),
      }}>
      {({
        data: {
          activity = {},
        },
      }) => (
        <div style={styles.wrapper}>
          <ResizableImage
            desktopRatio={0.556}
            mobileRatio={1.452}
            desktopImg={activity && activity.desktopImg}
            mobileImg={activity && activity.mobileImg} />
          <div style={styles.infoWrapper}>
            <FrameHeavyLine
              type="leftTop"
              color={Theme.YELLOW_COLOR} />
            <div style={styles.infoPlacement}>
              <span style={styles.title}>
                {(activity && activity.name) || null}
              </span>
              {activity && activity.fragments && Array.isArray(activity.fragments)
                ? activity.fragments.map((f, idx) => (
                  <div
                    key={f.id}
                    style={[
                      styles.fragmentWrapper,
                      idx === (activity.fragments.length - 1) && {
                        ...styles.noBorder,
                        ...styles.noLimit,
                      },
                    ]}>
                    {f.title ? (
                      <span style={styles.fragmentTitle}>
                        {f.title}
                      </span>
                    ) : null}
                    {f.content ? (
                      <span style={styles.fragmentDesc}>
                        {f.content}
                      </span>
                    ) : null}
                  </div>
                )) : null}
            </div>
            <FrameHeavyLine
              type="rightBottom"
              color={Theme.PURPLE_COLOR} />
          </div>
        </div>
      )}
    </Query>
  );
}

export default radium(ActivityDetailPage);
