// @flow
import React from 'react';
import radium from 'radium';
import ellipsize from 'ellipsize';
import {
  Link as link,
} from 'react-router-dom';
// config
import Theme from '../../styles/Theme.js';

const Link = radium(link);

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 48px 36px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      padding: '0 12px',
    },
  },
  titleWrapper: {
    width: '100%',
    height: 'auto',
    padding: '36px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
    },
  },
  title: {
    fontSize: 32,
    fontWeight: 700,
    color: Theme.BLACK_COLOR,
    letterSpacing: 3,
    textAlign: 'center',
    '@media (max-width: 767px)': {
      fontSize: 26,
    },
  },
  listWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  listPlacement: {
    width: '100%',
    height: 'auto',
    maxWidth: 1044,
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 1199px)': {
      flexDirection: 'column',
    },
  },
  activityCard: {
    width: 498,
    height: 450,
    margin: 12,
    padding: 24,
    borderRadius: 12,
    backgroundColor: '#fff',
    boxShadow: Theme.CLIENT_BLOCK_SHADOW,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    textDecoration: 'none',
    border: 0,
    outline: 0,
    cursor: 'pointer',
    ':hover': {
      opacity: 0.9,
    },
    '@media (max-width: 767px)': {
      width: 257,
      height: 'auto',
      padding: 16,
    },
  },
  cardImageDesktopWrapper: {
    width: '100%',
    height: 'auto',
    display: 'block',
    '@media (max-width: 767px)': {
      display: 'none',
    },
  },
  cardImageMobileWrapper: {
    display: 'none',
    '@media (max-width: 767px)': {
      width: '100%',
      height: 'auto',
      display: 'block',
    },
  },
  cardImg: {
    width: '100%',
    height: 'auto',
  },
  cardInfoWrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 0',
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  cardTitle: {
    fontSize: 18,
    fontWeight: 700,
    letterSpacing: 2,
    color: Theme.BLACK_COLOR,
    padding: '6px 0',
    '@media (max-width: 767px)': {
      fontSize: 16,
    },
  },
  cardDesc: {
    fontSize: 16,
    fontWeight: 400,
    letterSpacing: 1,
    lineHeight: 1.1,
    color: Theme.BLACK_COLOR,
    whiteSpace: 'pre-line',
    wordBreak: 'break-word',
    '@media (max-width: 767px)': {
      fontSize: 13,
    },
  },
};

type Props = {
  title: string,
  list: Array<{
    id: number,
    name: string,
    description: string,
    desktopImg: string,
    mobileImg: string,
  }>,
  match: {
    url: string,
  },
};

function ActivityListPage({
  title,
  list = [],
  match: {
    url,
  },
}: Props) {
  return (
    <div style={styles.wrapper}>
      <div style={styles.titleWrapper}>
        <span style={styles.title}>
          {title || null}
        </span>
      </div>
      <div style={styles.listWrapper}>
        <div style={styles.listPlacement}>
          {list.map(l => (
            <Link
              key={l.id}
              to={{ pathname: `${url}/${l.id}` }}
              style={styles.activityCard}>
              <div style={styles.cardImageDesktopWrapper}>
                <img
                  alt="活動圖片(桌面版)"
                  src={l.desktopImg}
                  style={styles.cardImg} />
              </div>
              <div style={styles.cardImageMobileWrapper}>
                <img
                  alt="活動圖片(手機版)"
                  src={l.mobileImg}
                  style={styles.cardImg} />
              </div>
              <div style={styles.cardInfoWrapper}>
                <span style={styles.cardTitle}>
                  {l.name ? ellipsize(l.name, 22) : null}
                </span>
                <span style={styles.cardDesc}>
                  {l.description ? ellipsize(l.description, 78) : null}
                </span>
              </div>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
}

export default radium(ActivityListPage);
