// @flow
import React from 'react';
import radium from 'radium';
import {
  Link as l,
} from 'react-router-dom';
// config
import Theme from '../../styles/Theme.js';
import step1Icon from '../../static/images/description/desc-transport-step1.png';
import step2Icon from '../../static/images/description/desc-transport-step2.png';
import step3Icon from '../../static/images/description/desc-transport-step3.png';
import step4Icon from '../../static/images/description/desc-transport-step4.png';
import step5Icon from '../../static/images/description/desc-transport-step5.png';
import step6Icon from '../../static/images/description/desc-transport-step6.png';
import step7Icon from '../../static/images/description/desc-transport-step7.png';
import step8Icon from '../../static/images/description/desc-transport-step8.png';
import step9Icon from '../../static/images/description/desc-transport-step9.png';
import example1 from '../../static/images/description/desc-transport-example-2.png';
import example2 from '../../static/images/description/desc-transport-example-1.png';
// components
import BlockTitle from '../../components/Description/BlockTitle.jsx';
import StepBlock from '../../components/Description/StepBlock.jsx';

const Link = radium(l);

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 48px 36px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      padding: '0 12px',
    },
  },
  titleWrapper: {
    width: '100%',
    height: 'auto',
    padding: '36px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
    },
  },
  title: {
    fontSize: 32,
    fontWeight: 700,
    color: Theme.BLACK_COLOR,
    letterSpacing: 3,
    textAlign: 'center',
  },
  stepPlacement: {
    width: 600,
    height: 'auto',
    padding: '48px 0',
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      width: '100%',
      maxWidth: 600,
    },
  },
  contentPlacement: {
    width: '100%',
    height: 'auto',
    padding: 48,
    backgroundColor: '#fff',
    borderRadius: 12,
    boxShadow: Theme.CLIENT_BLOCK_SHADOW,
    '@media (max-width: 767px)': {
      padding: '12px 0',
    },
  },
  contentWrapper: {
    width: '100%',
    height: 'auto',
    padding: '0 24px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  text: {
    fontSize: 18,
    fontWeight: 400,
    color: Theme.BLACK_COLOR,
    letterSpacing: 2,
    lineHeight: 1.4,
    whiteSpace: 'pre-line',
    '@media (max-width: 767px)': {
      fontSize: 16,
    },
  },
  examplesWrapper: {
    width: '100%',
    height: 'auto',
    marginTop: 24,
    borderTop: `1px solid ${Theme.BLACK_COLOR}`,
    padding: '6px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  exWrapper: {
    width: '100%',
    height: 'auto',
    padding: '16px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  srcWrapper: {
    width: 'auto',
    height: 'auto',
    padding: '12px 0',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  srcLabel: {
    width: 'auto',
    height: 'auto',
    padding: '1px 12px',
    borderRadius: 12,
    border: '1px solid rgba(0, 0, 0, 0.6)',
    fontSize: 16,
    color: 'rgba(0, 0, 0, 0.6)',
    letterSpacing: 1,
  },
  srcImg: {
    width: 120,
    height: 240,
    margin: '0 16px',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
  },
  textWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  highlight: {
    color: Theme.PURPLE_COLOR,
  },
  link: {
    fontSize: 18,
    letterSpacing: 2,
    backgroundColor: 'transparent',
    color: Theme.PURPLE_COLOR,
    textDecoration: 'none',
    outline: 0,
    border: 0,
    cursor: 'pointer',
    '@media (max-width: 767px)': {
      fontSize: 16,
    },
  },
};

const STEPS = [{
  icon: step1Icon,
  label: '加入LINE',
  annotation: '加入artmo會員',
}, {
  icon: step2Icon,
  label: '選購商品',
  annotation: '進入客製化系統選購商品',
}, {
  icon: step3Icon,
  label: '系統下單',
  annotation: '選購完成填妥資料',
}, {
  icon: step4Icon,
  label: '付款結帳',
  annotation: '使用Line Pay付款',
}, {
  icon: step5Icon,
  label: '確認訂單',
  annotation: '付款完成即成立訂單',
}, {
  icon: step6Icon,
  label: '圖樣審核',
  annotation: '客製化圖樣審稿',
}, {
  icon: step7Icon,
  label: '系統排單',
  annotation: '審核無問題即排單製作',
}, {
  icon: step8Icon,
  label: '商品製作',
  annotation: '製作時間約7~14日',
}, {
  icon: step9Icon,
  label: '檢查寄送',
  annotation: '商品完成檢核無誤即寄送',
}];

const EXAMPLES = [{
  label: '一般無透底圖（即有滿版背景圖）請使用JPG檔',
  src: example1,
}, {
  label: '透底圖（無滿版背景圖）請使用PNG檔',
  src: example2,
}];

function TransportPage() {
  return (
    <div style={styles.wrapper}>
      <div style={styles.titleWrapper}>
        <span style={styles.title}>
          運送方式
        </span>
      </div>
      <div style={styles.stepPlacement}>
        {STEPS.map(step => (
          <StepBlock
            key={step.label}
            {...step} />
        ))}
      </div>
      <div style={styles.titleWrapper}>
        <span style={styles.title}>
          購物須知
        </span>
      </div>
      <div style={styles.contentPlacement}>
        <div style={styles.contentWrapper}>
          <p style={[styles.text, styles.highlight]}>
            ※下單前請務必詳閱以下說明
          </p>
          <p style={styles.text}>
            artmo依照您專屬的個人化需求進行製作，恕不接受個人因素退換貨，例如：錯型號、有色差、跟想像不同，敬請見諒。
          </p>
          <p style={styles.text}>
            免責聲明（請詳閱
            <Link
              to={{ pathname: '/customer/disclaim' }}
              style={styles.link}>
              使用及免責條款
            </Link>
            ）
          </p>
          <p style={styles.text}>
            當您確認購買的同時請確定您使用的圖片已被授權使用，如遭第三人聲明有侵犯他人利益時，或致使本公司蒙受損失，您將需要賠償本公司一切損失。
            請您於訂購前確認您對相關商品聲明已充分事前審閱瞭解並請瞭解因本商品性質特殊，可能會因此而影響您契約解除權利之行使。
          </p>
          <p style={styles.text}>
            artmo客製化商品 所提供的服務或內容，包括所有內文、圖片、頁面設計、商標、網站架設、架構，均受智慧財產權、商標及相關法律保護，
            在未取得rtmo書面同意之前，所有使用者不可擅自使用、複製、抄襲、重製、授權或轉授權部分或全部本網站的服務內容，若有違反將依法提出刑民事訴訟。
          </p>
          <p style={styles.text}>
            artmo提供線上編輯器 客製化商品 ，當您完成設計圖案時將直接經由印刷設備進行生產工作，若您無合法權利​​得
            授權他人使用、修改、重製、公開播送、改作、散佈、發行、公開發表某資料，
            並將前述權利轉授權第三人，
            請勿擅自將該資料上載、傳送、輸入或提供artmo。任何資料一經您上載、傳送、輸入或提供至artmo的公開頁面時，視為您已允許artmo可以基於宣傳網站之目的，
            無條件使用、修改、重製、公開播送、改作、散佈、發行、公開發表該等資料，用以製作網頁、電子郵件、印刷品，
            您對此絕無異議。您並應保證ITEES使用、修改、重製、公開播送、改作、散佈、發行、公開發表、轉授權該等資料，
            不致侵害任何第三人之智慧財產權，否則應對artmo負損害賠償責任（包括但不限於訴訟費用及律師費用等）。
          </p>
          <div style={styles.examplesWrapper}>
            <BlockTitle title="【個人化圖片使用格式】" />
            {EXAMPLES.map(ex => (
              <div key={ex.label} style={styles.exWrapper}>
                <span style={styles.text}>
                  {ex.label || null}
                </span>
                <div style={styles.srcWrapper}>
                  <span style={styles.srcLabel}>
                    範例
                  </span>
                  <div
                    style={[
                      styles.srcImg,
                      {
                        backgroundImage: ex.src ? `url(${ex.src})` : null,
                      },
                    ]} />
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default radium(TransportPage);
