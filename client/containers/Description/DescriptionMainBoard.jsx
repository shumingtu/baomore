// @flow
import React, { PureComponent } from 'react';
import radium from 'radium';

// config
import navs from '../../shared/Description/navs.js';
// components
import SubNavBar from '../../components/Global/SubNavBar.jsx';
import PaymentPage from './PaymentPage.jsx';
import ReturnPage from './ReturnPage.jsx';
import TransportPage from './TransportPage.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
};

type Props = {
  pathId: string,
};

class DescriptionMainBoard extends PureComponent<Props> {
  componentRenderer() {
    const {
      pathId,
    } = this.props;

    switch (pathId) {
      case 'payment':
        return (
          <PaymentPage />
        );
      case 'transport':
        return (
          <TransportPage />
        );
      case 'return':
        return (
          <ReturnPage />
        );
      default:
        return null;
    }
  }

  render() {
    return (
      <div style={styles.wrapper}>
        <SubNavBar
          disabledTitleLink
          title={{
            name: '購物說明',
            path: null,
          }}
          navs={navs} />
        {this.componentRenderer()}
      </div>
    );
  }
}

export default radium(DescriptionMainBoard);
