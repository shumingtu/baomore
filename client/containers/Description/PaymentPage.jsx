// @flow
import React from 'react';
import radium from 'radium';
// config
import Theme from '../../styles/Theme.js';
import linePayIcon from '../../static/images/description/desc-line-icon.png';
// components
import BlockTitle from '../../components/Description/BlockTitle.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 48px 36px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      padding: '0 12px',
    },
  },
  titleWrapper: {
    width: '100%',
    height: 'auto',
    padding: '36px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
    },
  },
  title: {
    fontSize: 32,
    fontWeight: 700,
    color: Theme.BLACK_COLOR,
    letterSpacing: 3,
    textAlign: 'center',
  },
  contentPlacement: {
    width: '100%',
    height: 'auto',
    padding: 48,
    backgroundColor: '#fff',
    borderRadius: 12,
    boxShadow: Theme.CLIENT_BLOCK_SHADOW,
    '@media (max-width: 767px)': {
      padding: '12px 0',
    },
  },
  contentWrapper: {
    width: '100%',
    height: 'auto',
    padding: '0 24px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  text: {
    fontSize: 18,
    fontWeight: 400,
    color: Theme.BLACK_COLOR,
    letterSpacing: 2,
    lineHeight: 1.4,
    whiteSpace: 'pre-line',
    '@media (max-width: 767px)': {
      fontSize: 16,
    },
  },
  smText: {
    fontSize: 14,
    fontWeight: 400,
    color: Theme.BLACK_COLOR,
    letterSpacing: 2,
    lineHeight: 1.4,
    whiteSpace: 'pre-line',
  },
  blockWrapper: {
    width: 'auto',
    height: 'auto',
    borderRadius: 12,
    backgroundColor: '#fff',
    boxShadow: Theme.CLIENT_BLOCK_SHADOW,
    margin: '24px 0',
  },
  bigLinePayWrapper: {
    width: 240,
    height: 70,
    margin: '0 0 16px 0',
    backgroundImage: `url(${linePayIcon})`,
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
  },
  smallLinePayWrapper: {
    width: 80,
    height: 20,
    backgroundImage: `url(${linePayIcon})`,
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
  },
  tutorialWrapper: {
    width: '100%',
    height: 'auto',
    marginTop: 24,
    borderTop: `1px solid ${Theme.BLACK_COLOR}`,
    padding: '6px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  tutorialItem: {
    width: 280,
    height: 'auto',
    padding: '16px 12px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  linePayBottomWrapper: {
    flex: 1,
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  linePayInputWrapper: {
    width: '100%',
    height: 50,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderRadius: 2,
  },
  inputDash: {
    width: 25,
    height: 1,
    margin: '0 2px',
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
  },
};

function PaymentPage() {
  return (
    <div style={styles.wrapper}>
      <div style={styles.titleWrapper}>
        <span style={styles.title}>
          付款方式
        </span>
      </div>
      <div style={styles.contentPlacement}>
        <div style={styles.contentWrapper}>
          <span style={styles.text}>
            artmo使用LINE PAY信用卡方式付款，請安心購買。
          </span>
          <div style={styles.blockWrapper}>
            <div
              style={{
                width: '100%',
                height: 'auto',
                padding: 24,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'flex-start',
              }}>
              <div style={styles.bigLinePayWrapper} />
              <span style={styles.smText}>
                使用 Line Pay
              </span>
              <span style={styles.smText}>
                付款簡單又安全
              </span>
            </div>
          </div>
          <div style={styles.tutorialWrapper}>
            <BlockTitle title="【使用LINE PAY】" />
            <span style={[styles.text, { padding: '24px 0' }]}>
              確定購買之後選擇LINE PAY支付方式，如果是第一次使用要先綁定信用卡。
            </span>
            <div style={styles.blockWrapper}>
              <div style={[styles.tutorialItem, { justifyContent: 'space-between' }]}>
                <span style={styles.smText}>
                  信用卡付款
                </span>
                <div style={styles.smallLinePayWrapper} />
              </div>
              <div style={[styles.tutorialItem, { borderTop: '1px solid rgba(0, 0, 0, 0.1)' }]}>
                <span style={styles.smText}>
                  ATM
                </span>
              </div>
            </div>
            <span style={[styles.text, { padding: '48px 0' }]}>
              接著輸入7個數字的密碼，這個密碼在綁定信用卡的時候就會設定，請ㄧ定要記住。
            </span>
            <div
              style={[
                styles.blockWrapper,
                {
                  backgroundColor: 'rgba(0, 0, 0, 0.1)',
                  boxShadow: 'none',
                },
              ]}>
              <div
                style={{
                  width: 280,
                  height: 240,
                  padding: '24px 4px 12px',
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'flex-start',
                }}>
                <span style={[styles.text, { fontWeight: 700, margin: '0 0 12px 0' }]}>
                  LINE Pay 密碼
                </span>
                <span style={[styles.smText, { fontSize: 12, color: 'rgba(0, 0, 0, 0.6)' }]}>
                  請輸入您的 LINE Pay密碼。
                </span>
                <div style={styles.linePayBottomWrapper}>
                  <div style={styles.linePayInputWrapper}>
                    <div style={styles.inputDash} />
                    <div style={styles.inputDash} />
                    <div style={styles.inputDash} />
                    <div style={styles.inputDash} />
                    <div style={styles.inputDash} />
                    <div style={styles.inputDash} />
                    <div style={styles.inputDash} />
                  </div>
                  <span
                    style={[
                      styles.smText,
                      {
                        fontSize: 12,
                        color: 'rgba(0, 0, 0, 0.6)',
                        paddingTop: 12,
                      },
                    ]}>
                    忘記密碼？
                  </span>
                </div>
              </div>
            </div>
            <span style={[styles.text, { padding: '36px 0' }]}>
              照著預設的流程即可完成囉！
            </span>
          </div>
        </div>
      </div>
    </div>
  );
}

export default radium(PaymentPage);
