// @flow
import React from 'react';
import radium from 'radium';
// config
import Theme from '../../styles/Theme.js';
// components
import BlockTitle from '../../components/Description/BlockTitle.jsx';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '12px 48px 36px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      padding: '0 12px',
    },
  },
  titleWrapper: {
    width: '100%',
    height: 'auto',
    padding: '36px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      flexDirection: 'column',
    },
  },
  title: {
    fontSize: 32,
    fontWeight: 700,
    color: Theme.BLACK_COLOR,
    letterSpacing: 3,
    textAlign: 'center',
  },
  contentPlacement: {
    width: '100%',
    height: 'auto',
    padding: 48,
    backgroundColor: '#fff',
    borderRadius: 12,
    boxShadow: Theme.CLIENT_BLOCK_SHADOW,
    '@media (max-width: 767px)': {
      padding: '12px 0',
    },
  },
  contentWrapper: {
    width: '100%',
    height: 'auto',
    padding: '0 24px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  text: {
    fontSize: 18,
    fontWeight: 400,
    color: Theme.BLACK_COLOR,
    letterSpacing: 2,
    lineHeight: 1.4,
    whiteSpace: 'pre-line',
    '@media (max-width: 767px)': {
      fontSize: 16,
    },
  },
  descriptionWrapper: {
    width: '100%',
    height: 'auto',
    marginTop: 24,
    borderTop: `1px solid ${Theme.BLACK_COLOR}`,
    padding: '6px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  textWrapper: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  highlight: {
    color: Theme.PURPLE_COLOR,
  },
  annotationWrapper: {
    width: '100%',
    height: 'auto',
    padding: 24,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '@media (max-width: 767px)': {
      padding: '48px 12px',
    },
  },
};

function ReturnPage() {
  return (
    <div style={styles.wrapper}>
      <div style={styles.titleWrapper}>
        <span style={styles.title}>
          退換貨方式
        </span>
      </div>
      <div style={styles.contentPlacement}>
        <div style={styles.contentWrapper}>
          <div style={styles.descriptionWrapper}>
            <BlockTitle title="【瑕疵商品退換貨辦理】" />
            <div style={styles.textWrapper}>
              <p style={styles.text}>
                客製化訂製部分，屬於消費者專屬訂製商品，故無七天鑑賞期及貨到付款服務，若造成不便請見諒。
              </p>
              <p style={styles.text}>
                確認訂單之前，請詳細確認您所選擇各項文字圖案設計，會員不得以此為退貨及退款的請求，訂購一經確認，恕不退款。
                本公司不接受任何取消訂單或退款要求，請客戶謹慎下單。
              </p>
              <p style={styles.text}>
                客製化所有商品皆為先付款才寄貨，以免損失雙方權益，若造成不便請見諒。
              </p>
            </div>
          </div>
          <div style={styles.descriptionWrapper}>
            <BlockTitle title="【瑕疵損壞定義】" />
            <div style={styles.textWrapper}>
              <p style={styles.text}>
                客製商品表面如有
                <span style={[styles.text, styles.highlight]}>
                  刮傷、污漬、浮雕層剝落
                </span>
                之瑕疵，可將照片傳給我們，加入Line或是私訊臉書小幫手，並提供您的訂單號碼及會員資料，小幫手將會統一處理。
              </p>
              <p style={styles.text}>
                客製商品恕不接受個人因素退換貨，例如：訂錯型號、有色差（因每台螢幕呈現色彩方式不同，故本身會有色差問題，
                一般15-20%的色差為合理範圍）、跟想像不同、圖片模糊不清楚（依使用者上傳之圖片解析度不同而有所差異），
                若您非常在意上述問題，請您務必審慎考慮下單，敬請見諒。
              </p>
              <p style={styles.text}>
                客製商品依據客戶選擇及上傳之圖稿至工廠印刷，因工廠為大量生產之工作環境，
                故生產、印刷過程中會有細微刮痕/微小瑕疵/細微墨點與散墨現象，但不影響客製印刷與機殼裝機正常使用。
                若您是
                <span style={[styles.text, styles.highlight]}>
                  對於客製商品有完美需求之消費者，建議您謹慎考量
                </span>
                本網站商品是否符合您的需求再行下單，以避免造成雙方認知差異進而產生消費糾紛。
              </p>
            </div>
          </div>
        </div>
      </div>
      <div style={styles.annotationWrapper}>
        <span style={styles.text}>
          關於本網站之任何使用疑問，請撥打客服專線(02)8667-3456
          (週一至週五10:00-18:00)，也可加入LINE或FB訊息留言，我們將為您提供完整的說明。
        </span>
      </div>
    </div>
  );
}

export default radium(ReturnPage);
