// @flow

import React from 'react';
import radium from 'radium';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { graphql } from 'react-apollo';
import qs from 'qs';
// actions
import * as MemberActions from '../actions/Member.js';
import { GET_MEMBER_ME } from '../queries/Member.js';

type Props = {
  cacheMemberInfo: Function,
  cacheMemberToken: Function,
  clearMemberInfo: Function,
  location: {
    search: string,
    pathname: string,
  },
  history: {
    replace: Function,
  },
  accessToken: string,
  me: {
    id: number,
    name: string,
    phone: string,
    email: string,
    gender: string,
    storedDesignJSON: string,
  },
  meError: string,
  clearMemberInfo: Function,
};

class MemberMeBoard extends React.PureComponent<Props> {
  componentDidMount() {
    const {
      location: {
        search,
        pathname,
      },
      history,
      cacheMemberInfo,
      cacheMemberToken,
      clearMemberInfo,
      me,
      meError,
    } = this.props;

    if (meError) {
      clearMemberInfo();
    }

    if (search) {
      const qsString = qs.parse(search, { ignoreQueryPrefix: true }) || {};

      if (qsString.token) {
        const wrapMe = me || {};

        cacheMemberInfo(wrapMe);
        cacheMemberToken(qsString.token);

        switch (pathname) {
          case '/oAuth': { // login from Member Center
            if (qsString.state === '0') { // new member
              history.replace('/member/edit?new=true');
            } else {
              history.replace('/member');
            }
            break;
          }
          case '/customize/oAuth': // login from customize page
            history.replace('/payment?local=true');
            break;
          default:
            history.replace('/');
        }
      }
    }
  }

  componentDidUpdate(prevProps) {
    const {
      me,
      meError,
      cacheMemberInfo,
      clearMemberInfo,
    } = this.props;

    if (meError && prevProps.meError !== meError) {
      clearMemberInfo();
    }

    if (me && me !== prevProps.me) {
      cacheMemberInfo(me);
    }
  }

  render() {
    return null;
  }
}

const reduxHook = connect(
  () => ({}),
  dispatch => bindActionCreators({
    ...MemberActions,
  }, dispatch)
);

const queryHook = graphql(GET_MEMBER_ME, {
  props: ({
    data: {
      me,
      error,
    },
  }) => ({
    me: me || null,
    meError: (error && error.message) || null,
  }),
});

export default withRouter(
  reduxHook(
    queryHook(
      radium(
        MemberMeBoard
      )
    )
  )
);
