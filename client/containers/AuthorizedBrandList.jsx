// @flow
import React from 'react';
import radium from 'radium';
// static
import dcLogo from '../static/images/landing/brand-dc-logo.png';
import disneyLogo from '../static/images/landing/brand-disney-logo.png';
import marvelLogo from '../static/images/landing/brand-marvel-logo.png';
import samniLogo from '../static/images/landing/brand-samni-logo.png';
// config
import {
  PURPLE_COLOR,
} from '../styles/Landing.js';

const styles = {
  wrapper: {
    width: '100%',
    height: 'auto',
    padding: '32px 12px',
    marginTop: 16,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderTop: `2px solid ${PURPLE_COLOR}`,
    borderBottom: `2px solid ${PURPLE_COLOR}`,
    borderLeft: 0,
    borderRight: 0,
    position: 'relative',
    '@media (max-width: 1023px)': {
      display: 'none',
    },
  },
  titleWrapper: {
    position: 'absolute',
    left: 'calc(50% - 45px)',
    top: -12,
    width: 90,
    height: 24,
    backgroundColor: PURPLE_COLOR,
    borderRadius: 3,
    fontSize: 14,
    color: '#fff',
    letterSpacing: 1,
    textAlign: 'center',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 'auto',
    height: 'auto',
    marginRight: 24,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  },
};

type Props = {
  hide: boolean,
};

function AuthorizedBrandList({
  hide,
}: Props) {
  if (hide) return null;

  return (
    <div style={styles.wrapper}>
      <div style={styles.titleWrapper}>
        授權品牌
      </div>
      <div
        style={[
          styles.logo,
          {
            width: 168,
            height: 80,
            backgroundImage: `url(${disneyLogo})`,
          },
        ]} />
      <div
        style={[
          styles.logo,
          {
            width: 194,
            height: 80,
            backgroundImage: `url(${marvelLogo})`,
          },
        ]} />
      <div
        style={[
          styles.logo,
          {
            width: 86,
            height: 80,
            backgroundImage: `url(${dcLogo})`,
          },
        ]} />
      <div
        style={[
          styles.logo,
          {
            width: 192,
            height: 80,
            backgroundImage: `url(${samniLogo})`,
          },
        ]} />
    </div>
  );
}

export default radium(AuthorizedBrandList);
