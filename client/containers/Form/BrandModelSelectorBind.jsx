// @flow
import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';
import {
  Field,
} from 'redux-form';

// config
import {
  FETCH_BRAND_LIST,
  FETCH_BRAND_MODEL_LIST,
} from '../../queries/Brand.js';
// components
import Selector from '../../components/Form/Selector.jsx';

const styles = {
  fieldWrapper: {
    flex: 1,
    width: 'auto',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      width: '100%',
    },
  },
};

type Props = {
  brandId?: {
    input: Object,
    meta: Object,
  },
  brandModelName?: {
    input: Object,
    meta: Object,
  },
  style?: Object,
};

class BrandModelSelectorBind extends PureComponent<Props> {
  static defaultProps = {
    brandId: null,
    brandModelName: null,
    style: null,
  };

  render() {
    const {
      brandId,
      brandModelName,
      style,
    } = this.props;

    return (
      <Fragment>
        {brandId ? (
          <Query query={FETCH_BRAND_LIST}>
            {({
              data,
            }) => (
              <div style={styles.fieldWrapper}>
                <Field
                  name="brandId"
                  options={(data && data.brandList) || []}
                  placeholder="請選擇手機品牌"
                  component={Selector}
                  customOnChange={() => {
                    if (brandModelName && brandModelName.input) {
                      brandModelName.input.onChange('-1');
                    }
                  }}
                  customStyle={style} />
              </div>
            )}
          </Query>
        ) : null}
        {brandId && brandModelName ? (
          <Query
            skip={!brandId.input.value || brandId.input.value === '-1'}
            query={FETCH_BRAND_MODEL_LIST}
            variables={{
              brandId: brandId.input && brandId.input.value !== '-1'
                ? parseInt(brandId.input.value, 10)
                : null,
            }}>
            {({
              data,
            }) => (
              <div style={styles.fieldWrapper}>
                <Field
                  name="brandModelName"
                  options={(data && data.brandModelList)
                    ? data.brandModelList.map(m => ({
                      id: m.name,
                      name: m.name,
                    })) : []}
                  placeholder="請選擇手機型號"
                  component={Selector}
                  customStyle={style} />
              </div>
            )}
          </Query>
        ) : null}
      </Fragment>
    );
  }
}

export default radium(BrandModelSelectorBind);
