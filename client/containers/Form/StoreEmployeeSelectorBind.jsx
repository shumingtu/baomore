// @flow
import React, { PureComponent, Fragment } from 'react';
import radium from 'radium';
import { Query } from 'react-apollo';
import {
  Field,
} from 'redux-form';

// config
import { GET_EMPLOYEE_LIST } from '../../queries/Member.js';
import { GET_STORES } from '../../queries/Customize.js';
// components
import Selector from '../../components/Form/Selector.jsx';

const styles = {
  fieldWrapper: {
    flex: 1,
    width: 'auto',
    height: 'auto',
    padding: '6px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '@media (max-width: 767px)': {
      width: '100%',
    },
  },
};

type Props = {
  storeId?: {
    input: Object,
    meta: Object,
  },
  storeName?: {
    input: Object,
    meta: Object,
  },
  memberId?: {
    input: Object,
    meta: Object,
  },
  areaId?: {
    input: Object,
    meta: Object,
  },
  style?: Object,
};

class StoreEmployeeSelectorBind extends PureComponent<Props> {
  static defaultProps = {
    storeId: null,
    storeName: null,
    memberId: null,
    areaId: null,
    style: null,
  };

  render() {
    const {
      storeId,
      storeName,
      memberId,
      style,
      areaId,
    } = this.props;

    return (
      <Fragment>
        {storeId ? (
          <Query
            query={GET_STORES}
            variables={{
              isForClient: true,
            }}>
            {({
              data,
            }) => {
              const stores = (data && data.stores) || [];
              return (
                <div style={styles.fieldWrapper}>
                  <Field
                    nullable
                    name="storeId"
                    label="門市："
                    options={stores}
                    placeholder="請選擇門市"
                    component={Selector}
                    customOnChange={(store) => {
                      if (storeName && storeName.input) {
                        storeName.input.onChange(store ? store.name : null);
                      }

                      if (areaId && areaId.input) {
                        if (store && store.area && store.area.id) {
                          areaId.input.onChange(parseInt(store.area.id, 10));
                        }
                      }
                    }}
                    customStyle={style} />
                </div>
              );
            }}
          </Query>
        ) : null}
        {memberId ? (
          <Query
            query={GET_EMPLOYEE_LIST}
            skip={
              !storeId.input.value || storeId.input.value === '-1'
              || !areaId.input.value || areaId.input.value === '-1'
            }
            variables={{
              areaId: areaId.input.value && areaId.input.value !== '-1' ? parseInt(areaId.input.value, 10) : null,
              isForClient: true,
            }}>
            {({
              data,
            }) => {
              const employeeMemberlist = (data && data.employeeMemberlist) || [];

              return (
                <div style={styles.fieldWrapper}>
                  <Field
                    nullable
                    name="memberId"
                    label="包膜師："
                    options={employeeMemberlist}
                    placeholder="請選擇包膜師"
                    component={Selector}
                    customStyle={style} />
                </div>
              );
            }}
          </Query>
        ) : null}
      </Fragment>
    );
  }
}

export default radium(StoreEmployeeSelectorBind);
