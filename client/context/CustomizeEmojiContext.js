import React from 'react';

const CustomizeEmojiContext = React.createContext('CustomizeEmoji');

export default CustomizeEmojiContext;
