import React from 'react';

const CustomizeTextContext = React.createContext('CustomizeText');

export default CustomizeTextContext;
