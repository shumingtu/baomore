export default function padSN(serialNumber) {
  if (serialNumber.startsWith('A')) {
    return serialNumber;
  }
  return `C${Array(Math.max(6 - String(serialNumber).length, 0)).join(0) + serialNumber}`;
}
