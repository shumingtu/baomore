import csv from 'csvtojson';
import iconv from 'iconv-lite';
import fs from 'fs';

export default function getCsvJsonArray(pathname) {
  return new Promise(async (resolve, reject) => {
    try {
      const rawCsvString = fs.readFileSync(pathname).toString(); // 保卡正常 其他不正常

      if (rawCsvString.indexOf('�') !== -1) {
        const csvFile = fs.readFileSync(pathname);
        const csvString = iconv.decode(csvFile, 'big5');
        const convertedJsonArray = await csv().fromString(csvString);

        resolve(convertedJsonArray);
        return;
      }

      const convertedJsonArray = await csv().fromString(rawCsvString);

      resolve(convertedJsonArray);
    } catch (e) {
      reject();
    }
  });
}
