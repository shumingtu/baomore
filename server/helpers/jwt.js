// @flow

import jwt from 'jsonwebtoken';
import Promise from 'bluebird';
import debug from 'debug';
import { InvalidRefreshToken } from '../errors/Member';

const NODE_ENV = process.env.NODE_ENV || 'development';
const debugJWT = debug('PHOTO_GENERATOR:JWT');

export const JWT_TOKEN = process.env.JWT_TOKEN || '^Baomore_Rytass:Baomore-2018$';
export const JWT_REFRESH_TOKEN = process.env.JWT_REFRESH_TOKEN || `${JWT_TOKEN}:RefreshToken2OI8`;
export const TOKEN_EXPIRED_SECONDS = parseInt((process.env.TOKEN_EXPIRED_SECONDS || `${60 * 15}`), 10); // 15 minutes

export function jwtSignRefreshToken(payload) {
  return new Promise((resolve) => {
    jwt.sign(payload, JWT_REFRESH_TOKEN, (error, token) => {
      if (error) {
        debugJWT('JWT Sign Refresh Token Error', {
          error,
          payload,
        });

        resolve();
      } else {
        debugJWT('Signed JWT Refresh Token');

        resolve(token);
      }
    });
  });
}

export function jwtSign(payload) {
  return new Promise((resolve) => {
    jwt.sign(payload, JWT_TOKEN, {
      expiresIn: NODE_ENV === 'production' ? TOKEN_EXPIRED_SECONDS : 9999999999,
    }, (error, token) => {
      if (error) {
        debugJWT('JWT Sign Error', {
          error,
          payload,
        });

        resolve();
      } else {
        debugJWT('Signed JWT token', {
          token,
        });

        resolve(token);
      }
    });
  });
}

export function verifyRefreshToken(token) {
  if (!token) throw new InvalidRefreshToken();

  return new Promise((resolve) => {
    jwt.verify(token, JWT_REFRESH_TOKEN, (err, member) => {
      if (err) {
        debugJWT('JWT Parse Error', err);

        throw new InvalidRefreshToken();
      }

      resolve(member);
    });
  });
}

export function parseJWT(token) {
  if (!token) return null;

  return new Promise((resolve) => {
    jwt.verify(token.replace(/^Bearer\s/, ''), JWT_TOKEN, (err, member) => {
      if (err) {
        debugJWT('JWT Parse Error', err);

        resolve(null);
      } else {
        resolve(member);
      }
    });
  });
}
