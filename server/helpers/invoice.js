import querystring from 'querystring';
import request from 'request-promise';
import debug from 'debug';

import transformURLEncoder from './transformURLEncoder.js';
import getCheckMacValue from './getCheckMacValue.js';
import {
  INVOICING_API_HOST,
  invoiceSetting,
} from '../../shared/global.js';

const debugInvoice = debug('BAOMORE:INVOICE');
const encodeData = data => (transformURLEncoder(encodeURIComponent(data)).toLowerCase());

export default async onlineOrder => (
  new Promise(async (resolve, reject) => {
    const payload = {
      Donation: '0',
      InvType: '07',
      ItemAmount: `${onlineOrder.price}`,
      ItemCount: '1',
      ItemPrice: `${onlineOrder.price}`,
      MerchantID: invoiceSetting.MerchantID,
      Print: '0',
      RelateNumber: onlineOrder.id,
      SalesAmount: onlineOrder.price,
      TaxType: '1',
      TimeStamp: Math.floor(Date.now() / 1000),
      CustomerName: onlineOrder.name, // 客戶名稱，長度為20字元
      CustomerEmail: onlineOrder.email,
    };
    const origin = {
      ...payload,
    };

    payload.CustomerName = encodeData(payload.CustomerName);
    payload.CustomerEmail = encodeData(payload.CustomerEmail);

    const checkMacValue = getCheckMacValue(payload, invoiceSetting.HashKey, invoiceSetting.HashIV);

    origin.ItemName = onlineOrder.PNTable.name;
    origin.ItemWord = '張';
    origin.CheckMacValue = checkMacValue;

    const options = {
      url: INVOICING_API_HOST,
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: querystring.stringify(origin),
    };

    try {
      const invoicingResult = await request(options);

      const {
        RtnCode,
        RtnMsg,
      } = querystring.parse(invoicingResult);

      if (RtnCode !== '1') {
        reject(new Error(RtnMsg));
        return;
      }

      const {
        InvoiceDate,
        InvoiceNumber,
      } = querystring.parse(invoicingResult);

      resolve({
        InvoiceDate,
        InvoiceNumber,
      });
    } catch (e) {
      debugInvoice(e);
      reject(e);
    }
  })
);
