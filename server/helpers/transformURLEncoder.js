const transformURLEncoder = (data) => {
  let result = data;

  const find = ['%2d', '%5f', '%2e', '%21', '%2a', '%28', '%29', '%20'];
  const replace = ['-', '_', '.', '!', '*', '(', ')', '+'];

  find.forEach((x, i) => {
    const regex = new RegExp(find[i], 'g');
    result = result.replace(regex, replace[i]);
  });

  return result;
};

export default transformURLEncoder;
