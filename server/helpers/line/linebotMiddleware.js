// @flow

export default bot => (
  (ctx) => {
    console.log('bot');
    if (bot.options.verify && ctx.request.rawBody && !bot.verify(ctx.request.rawBody, ctx.headers['x-line-signature'])) {
      ctx.status = 400;
      return;
    }
    console.log('receive');
    bot.parse(ctx.request.body);
    ctx.body = {};
  }
);
