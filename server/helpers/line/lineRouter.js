// @flow

import debug from 'debug';
import request from 'request-promise';
import jwt from 'jsonwebtoken';
import { db } from '../../db';
import { setEmployeeRichMenu } from './linebot';
import { MemberLINELoginError } from '../../errors/Member';

const debugAuth = debug('BAOMORE:LineAuth');

const LINE_OAUTH_HOST = process.env.LINE_OAUTH_HOST || 'https://api.line.me/oauth2/v2.1/token';
const LINE_CLIENT_ID = process.env.LINE_CLIENT_ID || '1599855723';
const LINE_CLIENT_SECRET = process.env.LINE_CLIENT_SECRET || '6d97c25d2001669f87d7dd55ab4a49e1';
const API_HOST = process.env.API_HOST || 'http://localhost:2113';
const HOST = process.env.HOST || 'http://localhost:8108';
const ADMIN_HOST = process.env.ADMIN_HOST || 'http://localhost:8109';

export async function employeeLogin(member, code) {
  return new Promise(async (resolve, reject) => {
    try {
      // get line id_token
      const options = {
        uri: LINE_OAUTH_HOST,
        method: 'POST',
        form: {
          code,
          redirect_uri: `${ADMIN_HOST}/linebot/register`,
          client_id: LINE_CLIENT_ID,
          client_secret: LINE_CLIENT_SECRET,
          grant_type: 'authorization_code',
        },
      };

      try {
        const stringData = await request(options);
        const data = JSON.parse(stringData);
        // get user info by this token
        const decoded = jwt.decode(data.id_token);

        // update Member lineId binding & picture & refreshToken
        const {
          sub,
          picture,
        } = decoded;

        member.lineId = sub;
        member.avatar = picture;
        member.refreshToken = data.refreshToken;

        await member.save();
      } catch (e) {
        debugAuth(e);
        throw new MemberLINELoginError();
      }

      const accessToken = await member.accessToken();

      // check role to set rich menu
      await setEmployeeRichMenu(member);

      resolve({
        member,
        accessToken,
      });
    } catch (e) {
      debugAuth(e);
      reject(e);
    }
  });
}

export async function adminLogin(ctx) {
  const redirectUrl = `${ADMIN_HOST}/oAuth?token=`;

  try {
    // line code
    const {
      code,
    } = ctx.request.query;

    // get line id_token
    const options = {
      uri: LINE_OAUTH_HOST,
      method: 'POST',
      form: {
        code,
        redirect_uri: `${API_HOST}/auth/adminLineLogin`,
        client_id: LINE_CLIENT_ID,
        client_secret: LINE_CLIENT_SECRET,
        grant_type: 'authorization_code',
      },
    };
    const stringData = await request(options);
    const data = JSON.parse(stringData);

    // get user info by this token
    const decoded = jwt.decode(data.id_token);
    // check user is exist or not
    const result = await db.models.Member.memberoAuthLogin(decoded.sub);

    // exist
    if (result.member) {
      return ctx.redirect(`${redirectUrl}${result.accessToken}`);
    }

    // create Member
    const {
      sub,
      name,
      picture,
    } = decoded;

    const memberData = {
      lineId: sub,
      name: name || 'artmo 會員',
      avatar: picture,
      refreshToken: data.refreshToken,
    };

    const member = await db.models.Member.createoAuthMember(memberData);

    // add default roles
    const memberRole = await db.models.Role.findOne({
      where: {
        name: '一般登入用戶',
      },
    });

    await member.addRole(memberRole);

    const accessToken = await member.accessToken();

    return ctx.redirect(`${redirectUrl}${accessToken}`);
  } catch (e) {
    debugAuth(e);
    return ctx.redirect(`${redirectUrl}&error=授權失敗`);
  }
}

export async function memberLogin(ctx) {
  const redirectUrl = `${HOST}/oAuth?token=`;

  try {
    // line code
    const {
      code,
    } = ctx.request.query;

    // get line id_token
    const options = {
      uri: LINE_OAUTH_HOST,
      method: 'POST',
      form: {
        code,
        redirect_uri: `${API_HOST}/auth/lineLogin`,
        client_id: LINE_CLIENT_ID,
        client_secret: LINE_CLIENT_SECRET,
        grant_type: 'authorization_code',
      },
    };
    const stringData = await request(options);
    const data = JSON.parse(stringData);

    // get user info by this token
    const decoded = jwt.decode(data.id_token);
    // check user is exist or not
    const result = await db.models.Member.memberoAuthLogin(decoded.sub);

    // exist
    if (result.member) {
      return ctx.redirect(`${redirectUrl}${result.accessToken}&state=1`);
    }

    // create Member
    const {
      sub,
      name,
      picture,
    } = decoded;

    const memberData = {
      lineId: sub,
      name: name || 'artmo 會員',
      avatar: picture,
      refreshToken: data.refreshToken,
    };

    const member = await db.models.Member.createoAuthMember(memberData);

    // add default roles
    const memberRole = await db.models.Role.findOne({
      where: {
        name: '一般登入用戶',
      },
    });

    await member.addRole(memberRole);

    const accessToken = await member.accessToken();

    return ctx.redirect(`${redirectUrl}${accessToken}&state=0`);
  } catch (e) {
    debugAuth(e);
    return ctx.redirect(`${redirectUrl}&error=授權失敗`);
  }
}

export async function memberLoginFromCustomizePage(ctx) {
  const redirectUrl = `${HOST}/customize/oAuth?token=`;

  try {
    // line code
    const {
      code,
    } = ctx.request.query;

    // get line id_token
    const options = {
      uri: LINE_OAUTH_HOST,
      method: 'POST',
      form: {
        code,
        redirect_uri: `${API_HOST}/auth/customize/lineLogin`,
        client_id: LINE_CLIENT_ID,
        client_secret: LINE_CLIENT_SECRET,
        grant_type: 'authorization_code',
      },
    };
    const stringData = await request(options);
    const data = JSON.parse(stringData);

    // get user info by this token
    const decoded = jwt.decode(data.id_token);
    // check user is exist or not
    const result = await db.models.Member.memberoAuthLogin(decoded.sub);

    // exist
    if (result.member) {
      return ctx.redirect(`${redirectUrl}${result.accessToken}`);
    }

    // create Member
    const {
      sub,
      name,
      picture,
    } = decoded;

    const memberData = {
      lineId: sub,
      name: name || 'artmo 會員',
      avatar: picture,
      refreshToken: data.refreshToken,
    };

    const member = await db.models.Member.createoAuthMember(memberData);

    // add default roles
    const memberRole = await db.models.Role.findOne({
      where: {
        name: '一般登入用戶',
      },
    });

    await member.addRole(memberRole);

    const accessToken = await member.accessToken();

    return ctx.redirect(`${redirectUrl}${accessToken}`);
  } catch (e) {
    debugAuth(e);
    return ctx.redirect(`${redirectUrl}&error=授權失敗`);
  }
}
