import crypto from 'crypto';
import _ from 'lodash';

import transformURLEncoder from './transformURLEncoder.js';

const getCheckMacValue = (payload, hashKey, hashIV) => {
  const keys = Object.keys(payload);
  const sortedKeys = _.sortBy(keys, key => key);
  const uri = _.map(sortedKeys, key => `${key}=${payload[key]}`).join('&');

  const raw = transformURLEncoder(encodeURIComponent(`HashKey=${hashKey}&${uri}&HashIV=${hashIV}`)).toLowerCase();
  const checkMacValue = crypto.createHash('md5').update(raw).digest('hex').toUpperCase();

  return checkMacValue;
};

export default getCheckMacValue;
