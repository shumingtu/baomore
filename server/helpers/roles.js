import jwtDecode from 'jwt-decode';

import {
  actions,
} from '../../shared/roleActions.js';

export function getMyRoles(token) {
  if (token) {
    const {
      permissions,
    } = jwtDecode(token);

    const actionKeys = Object.keys(actions)
      .map(actionKey => ({
        key: actionKey,
        code: actions[actionKey].code,
      }))
      .filter(c => permissions & c.code)
      .map(action => action.key);

    return actionKeys;
  }

  return null;
}

export function isPermissionAllowed(targetRoles = [], token) {
  if (!targetRoles || !Array.isArray(targetRoles)) {
    return false;
  }

  const myRolePermission = getMyRoles(token);

  if (myRolePermission) {
    return myRolePermission.some(r => ~targetRoles.indexOf(r));
  }

  return false;
}
