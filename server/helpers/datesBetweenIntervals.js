// @flow

import moment from 'moment';

export default (start, end, interval) => {
  let startDate = start;
  const intervals = [{ date: startDate }];
  const diffUnitOfTime: string = `${interval}s`;

  while (moment(end).diff(startDate, diffUnitOfTime) > 0) {
    const currentEnd = moment(moment(startDate).add(1, diffUnitOfTime)).format('YYYY-MM-DD');
    intervals.push({date: currentEnd});

    startDate = currentEnd;
  }

  return intervals;
};
