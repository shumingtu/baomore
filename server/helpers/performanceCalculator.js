/* eslint max-len: 0 */
import lodash from 'lodash';
import moment from 'moment';
import { Op } from 'sequelize';

import {
  db,
} from '../db';

function getFixed2Rounded(value) {
  return Math.round(value * 100) / 100;
}

function getPercentageRounded(value) {
  return Math.round(value * 10000) / 100;
}

function aspCal(sales, amounts) {
  if (amounts === 0) return 0;
  return getFixed2Rounded(sales / amounts);
}

function momCal(sales, lastMonthSales) {
  if (lastMonthSales === 0) return 0;

  return getPercentageRounded((sales / lastMonthSales) - 1);
}

function averageWorkHoursCal(recordsSpendTime, amounts) {
  if (amounts === 0) return 0;
  return getFixed2Rounded(recordsSpendTime / amounts);
}

function achieveRateCal(sales, targetAmount) {
  return getPercentageRounded(sales / targetAmount);
}

// 當前額度 除上 當月天數/當前天 (暫時的商業邏輯)
function expectAchievesCal(sales) {
  const currentMonthDays = moment().daysInMonth();
  const today = moment().date();
  return getFixed2Rounded(sales * (currentMonthDays / today));
}

function expectProfitsCal(expectAchieves, amounts, commission) {
  return getFixed2Rounded((expectAchieves - amounts) * commission);
}

function turnoverCal(totalDays, amounts, storageCount) {
  if (amounts === 0) return 0;
  return getFixed2Rounded((totalDays / amounts) * storageCount);
}

export async function getRecordGroupByPNCategoryId(parentRecord, performanceSettings, lastMonthRecordGroup, totalDays, workDays, memberIds) {
  const recordGroupByPNCategoryId = await Promise.all(lodash(parentRecord).groupBy(x => x.PNTable.PNSubCategory.PNCategory.id).map(async (value) => {
    const amounts = value.length;
    const sales = lodash.sumBy(value, 'price');
    const pnCategory = value[0].PNTable.PNSubCategory.PNCategory;
    const memberStock = await db.models.MemberStock.findAll({
      where: {
        storageCount: {
          [Op.ne]: 0,
        },
        MemberId: memberIds,
        PNTableId: lodash.uniq(value.map(v => v.PNTable.id)),
      },
    });
    const storageCount = lodash.sumBy(memberStock, 'storageCount');

    const {
      targetAmount,
      commission,
    } = performanceSettings.find(x => x.PNCategoryId === pnCategory.id);

    let recordsSpendTime = 0;

    value.forEach((x) => {
      recordsSpendTime += ((moment(x.endTime).valueOf() - moment(x.startTime).valueOf()) / 3600000);
    });

    const lastMonthRecordByPNCategoryId = lastMonthRecordGroup.filter(x => x.PNTable.PNSubCategory.PNCategory.id === pnCategory.id);

    const lastMonthSales = lodash.sumBy(lastMonthRecordByPNCategoryId, 'price');

    const achieveRate = achieveRateCal(sales, targetAmount);
    const asp = aspCal(sales, amounts);
    const mom = momCal(sales, lastMonthSales);
    const workHours = averageWorkHoursCal(recordsSpendTime, amounts);
    const expectAchieves = expectAchievesCal(sales);
    const expectProfits = expectProfitsCal(expectAchieves, amounts, commission);
    const turnover = turnoverCal(totalDays, amounts, storageCount);

    return {
      pnCategory,
      recordsSpendTime,
      lastMonthSales,
      amounts,
      sales,
      achieveRate,
      asp,
      mom,
      workHours,
      expectAchieves,
      expectProfits,
      turnover,
      storageCount,
    };
  }).value());

  return recordGroupByPNCategoryId;
}

export function getClientCalResult(materialPerformance, protectorPerformance, materialSetting, protectorSetting, totalDays) {
  const totalAmounts = ((protectorPerformance && protectorPerformance.amounts) || 0) + ((materialPerformance && materialPerformance.amounts) || 0);
  const totalSales = ((protectorPerformance && protectorPerformance.sales) || 0) + ((materialPerformance && materialPerformance.sales) || 0);
  const totalLastMonthSales = ((protectorPerformance && protectorPerformance.lastMonthSales) || 0) + ((materialPerformance && materialPerformance.lastMonthSales) || 0);
  const totalRecordsSpendTime = ((protectorPerformance && protectorPerformance.recordsSpendTime) || 0) + ((materialPerformance && materialPerformance.recordsSpendTime) || 0);
  const totalStorageCount = ((protectorPerformance && protectorPerformance.storageCount) || 0) + ((materialPerformance && materialPerformance.storageCount) || 0);

  const materialExpectAchieves = (materialPerformance && materialPerformance.expectAchieves) || 0;
  const protectorExpectAchieves = (protectorPerformance && protectorPerformance.expectAchieves) || 0;
  const materialExpectProfits = (materialPerformance && materialPerformance.expectProfits) || 0;
  const protectorExpectProfits = (protectorPerformance && protectorPerformance.expectProfits) || 0;

  return {
    targets: [
      materialSetting.targetAmount,
      protectorSetting.targetAmount,
      materialSetting.targetAmount + protectorSetting.targetAmount,
    ],
    amounts: [
      (materialPerformance && materialPerformance.amounts) || 0,
      (protectorPerformance && protectorPerformance.amounts) || 0,
      totalAmounts,
    ],
    sales: [
      (materialPerformance && materialPerformance.sales) || 0,
      (protectorPerformance && protectorPerformance.sales) || 0,
      totalSales,
    ],
    achieveRate: [
      (materialPerformance && materialPerformance.achieveRate) || 0,
      (protectorPerformance && protectorPerformance.achieveRate) || 0,
      achieveRateCal(
        totalSales,
        materialSetting.targetAmount + protectorSetting.targetAmount,
      ),
    ],
    asp: [
      (materialPerformance && materialPerformance.asp) || 0,
      (protectorPerformance && protectorPerformance.asp) || 0,
      aspCal(
        totalSales,
        totalAmounts,
      ),
    ],
    mom: [
      (materialPerformance && materialPerformance.mom) || 0,
      (protectorPerformance && protectorPerformance.mom) || 0,
      momCal(
        totalSales,
        totalLastMonthSales,
      ),
    ],
    workHours: [
      (materialPerformance && materialPerformance.workHours) || 0,
      (protectorPerformance && protectorPerformance.workHours) || 0,
      averageWorkHoursCal(
        totalRecordsSpendTime,
        totalAmounts,
      ),
    ],
    expectAchieves: [
      materialExpectAchieves,
      protectorExpectAchieves,
      (materialExpectAchieves * 100 + protectorExpectAchieves * 100) / 100,
    ],
    expectProfits: [
      materialExpectProfits,
      protectorExpectProfits,
      (materialExpectProfits * 100 + protectorExpectProfits * 100) / 100,
    ],
    turnover: [
      (materialPerformance && materialPerformance.turnover) || 0,
      (protectorPerformance && protectorPerformance.turnover) || 0,
      turnoverCal(
        totalDays,
        totalAmounts,
        totalStorageCount,
      ),
    ],
    commissions: [
      materialSetting.commission,
      protectorSetting.commission,
    ],
  };
}
