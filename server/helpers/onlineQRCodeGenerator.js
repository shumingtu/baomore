import { db } from '../db';

const getRandomNum = x => Math.floor(Math.random() * x) + 1; // 1 ~ x

const englishDecider = (num) => {
  switch (num) {
    case 1:
      return 'A';

    case 2:
      return 'B';

    case 3:
      return 'C';

    case 4:
      return 'D';

    case 5:
      return 'E';

    case 6:
      return 'F';

    case 7:
      return 'G';

    case 8:
      return 'H';

    case 9:
      return 'I';

    default:
      return 'Z';
  }
};

const englishCodeGenerator = () => {
  const randomEnglish1 = englishDecider(getRandomNum(9));
  const randomEnglish2 = englishDecider(getRandomNum(9));
  const randomEnglish3 = englishDecider(getRandomNum(9));
  const randomEnglish4 = englishDecider(getRandomNum(9));
  const randomEnglish5 = englishDecider(getRandomNum(9));

  return `${randomEnglish1}${randomEnglish2}${randomEnglish3}${randomEnglish4}${randomEnglish5}`;
};

const onlineQRCodeGenerator = prefix => new Promise(async (resolve) => {
  const english5Code = englishCodeGenerator();
  const qrcode = `${prefix}${english5Code}`;

  const existQRCode = await db.models.OrderQRCode.findOne({
    where: {
      qrcode,
    },
  });

  if (existQRCode) {
    return onlineQRCodeGenerator(prefix);
  }
  return resolve(qrcode);
});

export default onlineQRCodeGenerator;
