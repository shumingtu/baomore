import jwt from 'jsonwebtoken';
import debug from 'debug';
import {
  db,
} from '../db.js';
import { JWT_TOKEN } from './jwt';

const debugAuth = debug('Baomore:Authentication');

export default async (ctx, next) => {
  const {
    token,
  } = ctx.request.query;

  if (!token) {
    ctx.req.member = null;
    return next();
  }

  return (new Promise((resolve) => {
    jwt.verify(token.replace(/^Bearer\s/, ''), JWT_TOKEN, (err, payload) => {
      if (err) {
        debugAuth(err);
        ctx.req.member = null;
        next();

        return;
      }

      ctx.req.member = payload;

      if (ctx.req.member) {
        ctx.req.member.getInstance = function getInstance() {
          return db.models.Member.findOne({
            where: {
              id: payload.id,
            },
          });
        };
      }

      resolve(next());
    });
  }));
};
