// @flow

import debug from 'debug';
import type Sequelize from 'sequelize';
import { parseAccessToken } from './jwt';

const debugAuth = debug('Baomore:Auth');

export const AUTH_METHOD_TOKEN = 'AUTH_METHOD/TOKEN';
export const AUTH_METHOD_PASSWORD = 'AUTH_METHOD/PASSWORD';

type AuthMethodType = ?('AUTH_METHOD/TOKEN' | 'AUTH_METHOD/PASSWORD');

export const Actions = {
  ADMIN_LOGIN: 1,
};

export default class Auth {
  static INVALID_METHOD_TYPE = new Error('Invalid AuthMethod Type');

  constructor(token: ?string, db: Sequelize) {
    this.token = token;
    this.db = db;
  }

  db: ?Sequelize

  token: ?string

  payload: TokenPayloadType

  method: AuthMethodType

  getMember() {
    if (!this.token || !this.db) return null;

    return this.db.models.Administrator.findOne({
      where: {
        id: this.payload.id,
      },
    });
  }

  setMethod(newMethod: AuthMethodType) {
    this.method = newMethod;
  }

  async parseToken() {
    if (this.token) {
      try {
        this.payload = await parseAccessToken(this.token.replace(/^Bearer\s/, ''));

        this.setMethod(AUTH_METHOD_TOKEN);
      } catch (ex) {
        debugAuth('Invalid Access Token');
      }
    }
  }
}
