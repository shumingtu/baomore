// @flow

/* eslint no-bitwise: 0 */

import flatten from 'lodash/flatten';
import uniq from 'lodash/uniq';
import { db } from '../db.js';

export function checkShouldHavePermission(permissionCode, actionCodes = [], orMode = false) {
  let shouldHaveActionCodes = actionCodes;

  if (!Array.isArray(actionCodes)) {
    shouldHaveActionCodes = [actionCodes];
  }

  if (orMode) {
    return shouldHaveActionCodes.some(code => (permissionCode & code) === code);
  }

  const intersectionPermission = shouldHaveActionCodes.reduce((last, next) => last | next, 0);

  return (permissionCode & intersectionPermission) === intersectionPermission;
}

export async function getMemberPermission(memberId) {
  const {
    Member,
    Role,
    Action,
  } = db.models;

  const member = await Member.findOne({
    where: {
      id: memberId,
    },
    include: [{
      model: Role,
      attributes: [
        'id',
      ],
      include: [{
        model: Action,
        attributes: [
          'code',
        ],
      }],
    }],
  });

  return uniq(flatten(member.Roles
    .map(role => role.Actions.map(action => action.code))))
    .reduce((last, next) => last | next, 0);
}

export async function getMemberPermissionFromId(memberId: number) {
  if (!memberId) return 0;

  const member = await db.models.Member.findOne({
    where: {
      id: memberId,
    },
  });

  if (!member) return 0;

  return member.getPermission();
}
