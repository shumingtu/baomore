import {
  GraphQLList,
  GraphQLInt,
} from 'graphql';
import {
  db,
} from '../../db';
import {
  listType,
} from '../../types/Banner';

export const banners = {
  type: new GraphQLList(listType),
  args: {
    id: {
      type: GraphQLInt,
    },
  },
  resolve: (_, { id }) => {
    if (id) {
      return db.models.Banner.findAll({
        where: {
          id,
        },
      });
    }

    return db.models.Banner.findAll();
  },
};

export default banners;
