import {
  GraphQLList,
  GraphQLInt,
} from 'graphql';
import {
  db,
} from '../../db';
import {
  listType,
} from '../../types/Landing';

export const landings = {
  type: new GraphQLList(listType),
  args: {
    id: {
      type: GraphQLInt,
    },
  },
  resolve: (_, { id }) => {
    if (id) {
      return db.models.Landing.findAll({
        where: {
          id,
        },
      });
    }

    return db.models.Landing.findAll();
  },
};

export default landings;
