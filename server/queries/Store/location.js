import {
  GraphQLString,
  GraphQLNonNull,
} from 'graphql';
import googleMap from '@google/maps';

import {
  locationType,
} from '../../types/Store';
import { StoreAddressError } from '../../errors/Store';
import { GOOGLE_MAP_API_KEY } from '../../../shared/env';

const googleMapClient = googleMap.createClient({
  key: GOOGLE_MAP_API_KEY,
  Promise,
});

export const locationInfo = {
  type: locationType,
  args: {
    address: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: async (_, args) => {
    const {
      address,
    } = args;

    const googleResponse = await googleMapClient.geocode({ address }).asPromise();

    if (
      !googleResponse
      || !googleResponse.json
      || !googleResponse.json.results
      || !googleResponse.json.results[0]
      || !googleResponse.json.results[0].geometry
    ) {
      throw new StoreAddressError();
    }

    const {
      location,
    } = googleResponse.json.results[0].geometry;

    return {
      lat: location.lat,
      lon: location.lng,
    };
  },
};

export default locationInfo;
