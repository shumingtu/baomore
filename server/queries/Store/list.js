import {
  GraphQLList,
  GraphQLInt,
  GraphQLString,
  GraphQLBoolean,
  GraphQLFloat,
} from 'graphql';
import sequelize, { Op } from 'sequelize';

import {
  db,
} from '../../db';
import {
  storeType,
} from '../../types/Store';

export const list = {
  type: new GraphQLList(storeType),
  args: {
    cityId: {
      type: GraphQLInt,
    },
    storeId: {
      type: GraphQLInt,
    },
    channelId: {
      type: GraphQLInt,
    },
    districtId: {
      type: GraphQLInt,
    },
    limit: {
      type: GraphQLInt,
    },
    offset: {
      type: GraphQLInt,
    },
    keyword: {
      type: GraphQLString,
    },
    isForClient: {
      type: GraphQLBoolean,
      defaultValue: false,
    },
    isForCustomize: {
      type: GraphQLBoolean,
      defaultValue: false,
    },
    lat: {
      type: GraphQLFloat,
    },
    lon: {
      type: GraphQLFloat,
    },
  },
  resolve: async (_, args) => {
    const {
      limit,
      offset,
      isForClient,
      lat,
      lon,
      isForCustomize,
    } = args;

    const attributes = Object.keys(db.models.Store.attributes);
    let whereClause = {};

    if (lat && lon) {
      const userPointLocation = sequelize.fn('POINT', lon, lat);
      const storePointLocation = sequelize.fn('POINT', sequelize.col('Store.longitude'), sequelize.col('Store.latitude'));
      const distance = sequelize.fn('ST_DISTANCE_SPHERE', storePointLocation, userPointLocation);

      attributes.push([distance, 'distance']);

      whereClause = sequelize.where(distance, {
        [Op.lte]: 1000, // 1km
      });
    }

    const storeList = await db.models.Store.scope(
      { method: ['search', args] },
      (limit && offset !== undefined) ? { method: ['pages', limit, offset] } : {},
      'area',
      { method: ['channel', isForCustomize] },
      { method: ['members', isForClient] },
      { method: ['district', args.cityId] },
    ).findAll({
      attributes,
      where: whereClause,
    });

    return storeList;
  },
};

export default list;
