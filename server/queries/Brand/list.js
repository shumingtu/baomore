import {
  GraphQLList,
  GraphQLInt,
  GraphQLString,
} from 'graphql';
import { Op } from 'sequelize';

import {
  BrandType,
  BrandModelColorType,
} from '../../types/Brand';
import { db } from '../../db';

export const adminBrandModelColorList = {
  type: new GraphQLList(BrandModelColorType),
  args: {
    brandId: {
      type: GraphQLInt,
    },
    modelId: {
      type: GraphQLInt,
    },
    colorId: {
      type: GraphQLInt,
    },
    limit: {
      type: GraphQLInt,
      defaultValue: 10,
    },
    offset: {
      type: GraphQLInt,
      defaultValue: 0,
    },
  },
  resolve: async (_, args) => db.models.BrandModelColor.scope(
    { method: ['pages', args.limit, args.offset] },
    { method: ['search', args] }
  ).findAll({
    order: [
      ['BrandModel', 'Brand', 'name', 'ASC'],
    ],
  }),
};

export const brandList = {
  type: new GraphQLList(BrandType),
  args: {
    keyword: {
      type: GraphQLString,
    },
  },
  resolve: async (_, args) => {
    const {
      keyword,
    } = args;

    const whereClause = {};

    if (keyword) {
      whereClause.name = {
        [Op.like]: `%${keyword}%`,
      };
    }

    const result = await db.models.Brand.findAll({
      where: whereClause,
      order: [
        ['name', 'ASC'],
      ],
    });

    return result;
  },
};
