// @flow

import {
  GraphQLList,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';
import { Op } from 'sequelize';

import { BrandModelColorType } from '../../types/Brand';
import { db } from '../../db';

export const brandModelColor = {
  type: BrandModelColorType,
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: async (_, { id }) => db.models.BrandModelColor.scope(
    { method: ['search', { colorId: id }] }
  ).findOne(),
};

export const brandModelColorList = {
  type: new GraphQLList(BrandModelColorType),
  args: {
    modelId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    keyword: {
      type: GraphQLString,
    },
  },
  resolve: async (_, { modelId, keyword }) => {
    const whereClause = {
      BrandModelId: modelId,
    };

    if (keyword) {
      whereClause.name = {
        [Op.like]: `%${keyword}%`,
      };
    }

    const brandColors = await db.models.BrandModelColor.findAll({
      where: whereClause,
    });

    return brandColors;
  },
};
