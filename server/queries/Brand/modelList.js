// @flow

import {
  GraphQLList,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';
import { Op } from 'sequelize';

import { BrandModelType } from '../../types/Brand';
import { db } from '../../db';

export default {
  type: new GraphQLList(BrandModelType),
  args: {
    brandId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    keyword: {
      type: GraphQLString,
    },
  },
  resolve: async (_, { brandId, keyword }) => {
    const whereClause = {
      BrandId: brandId,
    };

    if (keyword) {
      whereClause.name = {
        [Op.like]: `%${keyword}%`,
      };
    }

    const models = await db.models.BrandModel.findAll({
      where: whereClause,
      include: [{
        model: db.models.BrandModelColor,
        as: 'colors',
      }],
      order: [
        ['name', 'ASC'],
      ],
    });

    return models;
  },
};
