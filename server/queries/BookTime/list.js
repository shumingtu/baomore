import {
  GraphQLList,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLString,
} from 'graphql';
import lodash from 'lodash';
import moment from 'moment';
import {
  db,
} from '../../db';
import {
  dateType,
  timeType,
} from '../../types/BookTime';
import {
  ResourceNotFoundError,
} from '../../errors/General';

import dateBetweenIntervals from '../../helpers/datesBetweenIntervals';

export const availableBookedDates = {
  type: new GraphQLList(dateType),
  args: {
    storeId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: () => {
    const dates = dateBetweenIntervals('2018-12-01', '2018-12-30', 'day');

    return dates;
  },
};

export const availableBookedTimes = {
  type: new GraphQLList(timeType),
  args: {
    storeId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    date: {
      type: new GraphQLNonNull(GraphQLString),
    },
    employeeId: {
      type: GraphQLInt,
    },
  },
  resolve: async (_, {
    storeId,
    date,
    employeeId,
  }) => {
    const store = await db.models.Store.scope({ method: ['search', { storeId }] }).findOne();

    if (!store) {
      throw new ResourceNotFoundError('Store Resource Not Found');
    }

    const dbTimes = await db.models.BookTimeSlot.findAll();
    const times = dbTimes.map(dbTime => dbTime.time);
    const rawTimeBudgets = lodash.flatten([times, times]);
    const timeBudgets = lodash.uniq(rawTimeBudgets);

    const storeMemberWhereClause = {};

    if (employeeId) storeMemberWhereClause.id = employeeId;

    const storeMembers = await store.getMembers({
      where: storeMemberWhereClause,
    });

    const orders = await db.models.OnlineOrder.scope({
      method: ['search', { date, bookedMemberIds: storeMembers.map(m => m.id) }],
    }).findAll();

    const orderTimes = orders.map(order => order.time);
    const matchTimes = orderTimes.reduce((acc, curr) => {
      const index = acc.findIndex(i => i === curr);
      if (index > -1) {
        return [
          ...acc.slice(0, index),
          ...acc.slice(index + 1),
        ];
      }

      return acc;
    }, timeBudgets);

    return matchTimes.map(time => ({ time }));
  },
};

export const alreadyBookedDates = {
  type: new GraphQLList(dateType),
  args: {
    storeId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    employeeId: {
      type: GraphQLInt,
    },
    categoryName: {
      type: GraphQLString,
    },
  },
  resolve: async (_, args) => {
    // two months booked dates
    const dates = dateBetweenIntervals(moment(), moment().add(2, 'months'), 'day');

    const store = await db.models.Store.scope({ method: ['search', args] }).findOne();

    if (!store) {
      throw new ResourceNotFoundError('Store Resource Not Found');
    }

    const storeMemberWhereClause = {};

    if (args.employeeId) storeMemberWhereClause.id = args.employeeId;


    const storeMembers = await store.getMembers({
      where: storeMemberWhereClause,
    });

    const storeOrders = await db.models.OnlineOrder.scope({
      method: ['search', {
        bookedMemberIds: storeMembers.map(m => m.id),
        bookedDate: dates[0].date,
        bookedEndDate: dates[dates.length - 1].date,
      },
      ],
    }).findAll();

    const dayLimit = 9;

    // if (args.categoryName) {
    //   switch (args.categoryName) {
    //     case '客製化包膜':
    //     case '會員作品共享包膜':
    //       dayLimit = 7;
    //       break;
    //
    //     default:
    //       dayLimit = 3;
    //       break;
    //   }
    // }

    const now = moment(Date.now()).format('YYYY-MM-DD');

    const limitDayArray = [now];

    Array.from(Array(dayLimit)).forEach((x, idx) => {
      limitDayArray.push(moment(now).add(idx + 1, 'days').format('YYYY-MM-DD'));
    });

    const groupByDates = lodash.groupBy(storeOrders, 'date');
    const storeDateSlotNum = 4;
    const bookedDates = Object.keys(groupByDates)
      .filter(date => groupByDates[date].length >= storeDateSlotNum);

    limitDayArray.forEach((date) => {
      const isInclude = bookedDates.includes(date);

      if (!isInclude) bookedDates.push(date);
    });

    return bookedDates.map(date => ({ date }));
  },
};

export const bookTimeSlotList = {
  type: new GraphQLList(timeType),
  resolve: () => db.models.BookTimeSlot.findAll(),
};
