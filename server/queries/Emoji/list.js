import {
  GraphQLList,
  GraphQLInt,
} from 'graphql';
import {
  db,
} from '../../db';
import {
  emojiType,
  emojiCategoryType,
} from '../../types/Emoji';

export const emojiCategories = {
  type: new GraphQLList(emojiCategoryType),
  resolve: () => db.models.EmojiCategory.findAll(),
};


export const emoji = {
  type: new GraphQLList(emojiType),
  args: {
    categoryId: {
      type: GraphQLInt,
    },
  },
  resolve: (_, args) => (
    db.models.Emoji.scope({ method: ['search', args] }).findAll()
  ),
};
