import {
  GraphQLList,
  GraphQLBoolean,
} from 'graphql';
import { db } from '../../db';
import { ChannelType } from '../../types/Channel';

export const listChannel = {
  type: new GraphQLList(ChannelType),
  args: {
    isPurchaseChannel: {
      type: GraphQLBoolean,
    },
  },
  resolve: async (_, { isPurchaseChannel }) => {
    const whereClause = {};
    if (isPurchaseChannel !== undefined) whereClause.isPurchaseChannel = isPurchaseChannel;

    const channelList = await db.models.Channel.findAll({
      where: whereClause,
    });

    return channelList;
  },
};

export default null;
