import {
  GraphQLList,
  GraphQLInt,
} from 'graphql';
import { db } from '../../db';
import { VendorType } from '../../types/Vendor';

export const listVendor = {
  type: new GraphQLList(VendorType),
  args: {
    id: {
      type: GraphQLInt,
    },
    limit: {
      type: GraphQLInt,
    },
    offset: {
      type: GraphQLInt,
    },
  },
  resolve: async (_, args) => {
    const {
      limit,
      offset,
    } = args;

    const vendorList = await db.models.Vendor.scope(
      { method: ['search', args] },
      (limit && offset !== undefined) ? { method: ['pages', limit, offset] } : {},
    ).findAll({
      include: [{
        model: db.models.VendorSetting,
      }],
    });

    return vendorList;
  },
};

export default null;
