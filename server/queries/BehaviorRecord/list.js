import {
  GraphQLList,
  GraphQLInt,
  GraphQLString,
  GraphQLNonNull,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import {
  db,
} from '../../db';
import { actions } from '../../../shared/roleActions';
import {
  PermissionError,
  ResourceNotFoundError,
} from '../../errors/General';
import {
  behaviorRecordType,
  behaviorType,
} from '../../types/BehaviorRecord';

export const behaviorList = {
  type: new GraphQLList(behaviorType),
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: [actions.ADMIN_DOA_MANAGE, actions.INVOICING_MANAGE],
    orMode: true,
  })(() => db.models.Behavior.findAll()),
};

export const recordList = {
  type: new GraphQLList(behaviorRecordType),
  args: {
    behaviorId: {
      type: GraphQLInt,
    },
    employeeId: {
      type: GraphQLInt,
    },
    startDate: {
      type: GraphQLString,
    },
    endDate: {
      type: GraphQLString,
    },
    recordStartDate: {
      type: GraphQLString,
    },
    recordEndDate: {
      type: GraphQLString,
    },
    limit: {
      type: GraphQLInt,
      defaultValue: 10,
    },
    offset: {
      type: GraphQLInt,
      defaultValue: 0,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: [actions.ADMIN_DOA_MANAGE, actions.INVOICING_MANAGE],
    orMode: true,
  })((_, args) => db.models.BehaviorRecord.scope(
    { method: ['searchForPerformance', args] },
    { method: ['pages', args.limit, args.offset] },
    'member',
    'order',
    'behavior',
  ).findAll()),
};

export default {
  type: new GraphQLList(behaviorRecordType),
  args: {
    rollbackType: {
      type: new GraphQLNonNull(GraphQLString),
    },
    pnCategoryId: {
      type: GraphQLInt,
    },
    startDate: {
      type: GraphQLString,
    },
    endDate: {
      type: GraphQLString,
    },
    code: {
      type: GraphQLString,
    },
    memberId: {
      type: GraphQLInt,
    },
    errorType: {
      type: GraphQLString,
    },
    target: {
      type: GraphQLString,
    },
    storeId: {
      type: GraphQLInt,
    },
    vat: {
      type: GraphQLString,
    },
    customerName: {
      type: GraphQLString,
    },
    customerPhone: {
      type: GraphQLString,
    },
    limit: {
      type: GraphQLInt,
      defaultValue: 10,
    },
    offset: {
      type: GraphQLInt,
      defaultValue: 0,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: [actions.ADMIN_DOA_MANAGE, actions.INVOICING_MANAGE],
    orMode: true,
  })(async (_, args) => {
    const behaviorRecords = await db.models.BehaviorRecord.scope(
      { method: ['search', args] },
      { method: ['pnTable', args.code, args.pnCategoryId] },
      { method: ['pages', args.limit, args.offset] },
      'member',
      'order',
      { method: ['vendor', false] },
      { method: ['store', false] },
      { method: ['approver', false] },
    ).findAll();

    if (!behaviorRecords) throw new ResourceNotFoundError();

    return behaviorRecords.map(record => ({
      ...record.dataValues,
      remarks: (record.dataValues.remark && record.dataValues.remark.split('***')) || [],
      pnCategory: (record.PNTable
        && record.PNTable.PNSubCategory
        && record.PNTable.PNSubCategory.PNCategory) || null,
    }));
  }),
};
