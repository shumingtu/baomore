import {
  GraphQLInt,
  GraphQLString,
  GraphQLNonNull,
  GraphQLList,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import lodash from 'lodash';
import moment from 'moment';
import { Op } from 'sequelize';

import {
  db,
} from '../../db';
import {
  employeePerformanceForAdminType,
  storePerformanceForAdminType,
} from '../../types/BehaviorRecord';
import {
  PermissionError,
} from '../../errors/General';
import { actions } from '../../../shared/roleActions';

import {
  getRecordGroupByPNCategoryId,
  getClientCalResult,
} from '../../helpers/performanceCalculator.js';

export const storePerformanceForAdmin = {
  type: new GraphQLList(storePerformanceForAdminType),
  args: {
    startDate: {
      type: new GraphQLNonNull(GraphQLString),
    },
    endDate: {
      type: new GraphQLNonNull(GraphQLString),
    },
    storeId: {
      type: GraphQLInt,
    },
    channelId: {
      type: GraphQLInt,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_PERFORMANCE_MANAGE,
  })(async (_, args) => {
    const searchForPerformanceArgs = {
      ...args,
      behaviorId: 1,
    };

    const {
      startDate,
      endDate,
    } = args;

    const lastMonthStartTime = moment(startDate).subtract(1, 'months');
    const lastMonthEndTime = moment(endDate).subtract(1, 'months');

    const lastMonthArgs = {
      ...searchForPerformanceArgs,
      startDate: lastMonthStartTime,
      endDate: lastMonthEndTime,
    };

    const [
      rawResult,
      memberSchedule,
      performanceSettings,
      lastMonthRecord,
    ] = await Promise.all([
      db.models.BehaviorRecord.scope(
        { method: ['searchForPerformance', searchForPerformanceArgs] },
        { method: ['performancePNTableAssociation'] },
        { method: ['performanceStoreAssociation', args] },
      ).findAll({
        attributes: ['id', 'startTime', 'endTime', 'price', 'BehaviorId', 'MemberId', 'PNTableId', 'StoreId'],
      }),
      db.models.MemberSchedule.scope(
        { method: ['search', args] },
      ).findAll({
        attributes: ['id', 'date', 'status', 'MemberId'],
        where: {
          [Op.or]: [{
            status: '4',
          }, {
            status: '004',
          }],
        },
      }),
      db.models.PerformanceSetting.findAll({
        attributes: ['id', 'PNCategoryId', 'targetAmount', 'commission'],
      }),
      db.models.BehaviorRecord.scope(
        { method: ['searchForPerformance', lastMonthArgs] },
        { method: ['performancePNTableAssociation'] },
        { method: ['performanceStoreAssociation', args] },
      ).findAll({
        attributes: ['id', 'price', 'BehaviorId', 'MemberId', 'PNTableId', 'StoreId'],
      }),
    ]);


    const totalDays = ((moment(endDate) - moment(startDate)) / 86400000) + 1;

    const groupByStore = lodash.groupBy(rawResult, 'StoreId');

    const calculatedResult = await Promise.all(Object.keys(groupByStore).map(async (storeId) => {
      const storeRecord = groupByStore[storeId];

      const storeMemberIds = storeRecord.map(x => x.MemberId)
        .filter((element, index, arr) => arr.indexOf(element) === index);

      const lastMonthRecordByStore = lastMonthRecord
        .filter(x => x.StoreId === parseInt(storeId, 10));

      const workDays = memberSchedule.filter(x => storeMemberIds.indexOf(x.MemberId) >= 0).length;

      const recordGroupByPNCategoryId = await getRecordGroupByPNCategoryId(
        storeRecord,
        performanceSettings,
        lastMonthRecordByStore,
        totalDays,
        workDays,
        storeMemberIds,
      );

      return {
        workDays,
        store: storeRecord[0].Store,
        performance: recordGroupByPNCategoryId,
      };
    }));

    const materialSetting = performanceSettings.find(x => x.PNCategoryId === 1);
    const protectorSetting = performanceSettings.find(x => x.PNCategoryId === 2);

    const result = [];

    calculatedResult.forEach((x) => {
      const materialPerformance = x.performance.find(p => p.pnCategory.id === 1);
      const protectorPerformance = x.performance.find(p => p.pnCategory.id === 2);

      const clientCalculatedResult = getClientCalResult(
        materialPerformance,
        protectorPerformance,
        materialSetting,
        protectorSetting,
        totalDays,
      );

      result.push({
        id: x.store.id,
        name: x.store.name,
        channel: x.store.Channel,
        ...clientCalculatedResult,
      });
    });

    return result;
  }),
};

export const employeePerformanceForAdmin = {
  type: new GraphQLList(employeePerformanceForAdminType),
  args: {
    startDate: {
      type: new GraphQLNonNull(GraphQLString),
    },
    endDate: {
      type: new GraphQLNonNull(GraphQLString),
    },
    directorId: {
      type: GraphQLInt,
    },
    employeeId: {
      type: GraphQLInt,
    },
    areaId: {
      type: GraphQLInt,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_PERFORMANCE_MANAGE,
  })(async (_, args) => {
    const {
      directorId,
      areaId,
      startDate,
      endDate,
    } = args;

    let targetMemberIds = [];

    const searchForPerformanceArgs = {
      ...args,
      behaviorId: 1,
    };

    const memberScheduleArgs = {
      ...args,
    };

    // 主任底下的包膜師
    if (directorId) {
      targetMemberIds = await db.models.Member.findAll({
        where: {
          MemberId: directorId,
        },
      }).map(member => member.id);

      searchForPerformanceArgs.employeeId = targetMemberIds;
      memberScheduleArgs.employeeId = targetMemberIds;
    }

    const lastMonthStartTime = moment(startDate).subtract(1, 'months');
    const lastMonthEndTime = moment(endDate).subtract(1, 'months');

    const lastMonthArgs = {
      ...searchForPerformanceArgs,
      startDate: lastMonthStartTime,
      endDate: lastMonthEndTime,
    };

    const [
      rawResult,
      memberSchedule,
      performanceSettings,
      lastMonthRecord,
    ] = await Promise.all([
      db.models.BehaviorRecord.scope(
        { method: ['searchForPerformance', searchForPerformanceArgs] },
        { method: ['performanceMemberAssociation', { areaId: areaId || null }] },
        { method: ['performancePNTableAssociation'] }, // memberStock另外拉
      ).findAll({
        attributes: ['id', 'startTime', 'endTime', 'price', 'BehaviorId', 'MemberId', 'PNTableId'],
      }),
      db.models.MemberSchedule.scope(
        { method: ['search', memberScheduleArgs] },
      ).findAll({
        attributes: ['id', 'date', 'status', 'MemberId'],
        where: {
          [Op.or]: [{
            status: '4',
          }, {
            status: '004',
          }],
        },
      }),
      db.models.PerformanceSetting.findAll({
        attributes: ['id', 'PNCategoryId', 'targetAmount', 'commission'],
      }),
      db.models.BehaviorRecord.scope(
        { method: ['searchForPerformance', lastMonthArgs] },
        { method: ['performanceMemberAssociation', { areaId: areaId || null }] },
        { method: ['performancePNTableAssociation'] },
      ).findAll({
        attributes: ['id', 'price', 'BehaviorId', 'MemberId', 'PNTableId'],
      }),
    ]);


    const totalDays = ((moment(endDate) - moment(startDate)) / 86400000) + 1;

    const groupByMember = lodash.groupBy(rawResult, 'MemberId');

    const calculatedResult = await Promise.all(Object.keys(groupByMember).map(async (memberId) => {
      const memberRecord = groupByMember[memberId];

      const lastMonthRecordByMember = lastMonthRecord
        .filter(x => x.MemberId === parseInt(memberId, 10));

      const workDays = memberSchedule.filter(x => x.MemberId === parseInt(memberId, 10)).length;

      const recordGroupByPNCategoryId = await getRecordGroupByPNCategoryId(
        memberRecord,
        performanceSettings,
        lastMonthRecordByMember,
        totalDays,
        workDays,
        [parseInt(memberId, 10)],
      );

      return {
        workDays,
        member: memberRecord[0].Member,
        performance: recordGroupByPNCategoryId,
      };
    }));

    const materialSetting = performanceSettings.find(x => x.PNCategoryId === 1);
    const protectorSetting = performanceSettings.find(x => x.PNCategoryId === 2);

    const result = [];

    calculatedResult.forEach((x) => {
      const materialPerformance = x.performance.find(p => p.pnCategory.id === 1);
      const protectorPerformance = x.performance.find(p => p.pnCategory.id === 2);

      const clientCalculatedResult = getClientCalResult(
        materialPerformance,
        protectorPerformance,
        materialSetting,
        protectorSetting,
        totalDays,
      );

      result.push({
        id: x.member.id,
        name: x.member.name,
        area: x.member.Area,
        ...clientCalculatedResult,
      });
    });

    return result;
  }),
};

export default null;
