import {
  GraphQLList,
  GraphQLInt,
  GraphQLString,
} from 'graphql';
import l from 'lodash';
import { authKeeper } from 'graphql-auth-keeper';

import {
  db,
} from '../../db';
import {
  scheduleType,
} from '../../types/Schedule';
import { actions } from '../../../shared/roleActions';
import {
  PermissionError,
} from '../../errors/General';

export const schedulelist = {
  type: new GraphQLList(scheduleType),
  args: {
    startDate: {
      type: GraphQLString,
    },
    endDate: {
      type: GraphQLString,
    },
    storeId: {
      type: GraphQLInt,
    },
    bookedMemberId: {
      type: GraphQLInt,
    },
    limit: {
      type: GraphQLInt,
      defaultValue: 10,
    },
    offset: {
      type: GraphQLInt,
      defaultValue: 0,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ONLINE_ORDER_MANAGE,
  })(async (_, args) => {
    const filterArgs = {
      bookedDate: args.startDate || null,
      bookedEndDate: args.endDate || null,
      storeId: args.storeId || null,
      bookedMemberId: args.bookedMemberId || null,
    };

    const results = await db.models.OnlineOrder.scope(
      { method: ['search', filterArgs] },
      'store',
      'bookedMember',
      'payedAndConfirmed',
    ).findAll({
      order: [
        ['date', 'DESC'],
      ],
    });

    const dateAggregates = l.groupBy(results, 'date');
    return Object.keys(dateAggregates).map((date) => {
      const timeAggregates = l.groupBy(dateAggregates[date], 'time');

      return {
        date,
        assignments: Object.keys(timeAggregates).map(time => ({
          time,
          denominator: 4,
          onlineOrders: timeAggregates[time],
        })),
      };
    });
  }),
};

export default schedulelist;
