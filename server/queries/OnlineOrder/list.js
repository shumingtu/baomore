import {
  GraphQLList,
  GraphQLInt,
  GraphQLString,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';

import {
  db,
} from '../../db';
import {
  onlineOrderType,
} from '../../types/OnlineOrder';
import { actions } from '../../../shared/roleActions';
import {
  PermissionError,
} from '../../errors/General';

export const onlineOrderlist = {
  type: new GraphQLList(onlineOrderType),
  args: {
    keyword: {
      type: GraphQLString,
    },
    startDate: {
      type: GraphQLString,
    },
    endDate: {
      type: GraphQLString,
    },
    payedStatus: {
      type: GraphQLString,
    },
    shippingStatus: {
      type: GraphQLString,
    },
    storeId: {
      type: GraphQLInt,
    },
    bookedMemberId: {
      type: GraphQLInt,
    },
    limit: {
      type: GraphQLInt,
      defaultValue: 10,
    },
    offset: {
      type: GraphQLInt,
      defaultValue: 0,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ONLINE_ORDER_MANAGE,
  })((_, args) => db.models.OnlineOrder.scope(
    { method: ['search', args] },
    { method: ['pages', args.limit, args.offset] },
    'store',
    'order',
    'bookedMember',
  ).findAll({
    include: [{
      model: db.models.Order,
    }],
  })),
};

export const onlineOrder = {
  type: onlineOrderType,
  args: {
    onlineOrderId: {
      type: GraphQLString,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ONLINE_ORDER_MANAGE,
  })((_, args) => db.models.OnlineOrder.scope(
    { method: ['search', { orderId: args.onlineOrderId }] },
    'store',
    'order',
    'bookedMember',
  ).findOne()),
};

export default onlineOrderlist;
