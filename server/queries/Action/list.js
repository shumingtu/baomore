import {
  GraphQLList,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';

import { db } from '../../db';
import { actionType } from '../../types/Action';
import { PermissionError } from '../../errors/General';
import { actions } from '../../../shared/roleActions';

export const actionList = {
  type: new GraphQLList(actionType),
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_MEMBER_MANAGE,
  })(() => db.models.Action.findAll()),
};

export default null;
