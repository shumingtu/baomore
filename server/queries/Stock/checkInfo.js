import {
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';
import moment from 'moment';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import {
  PermissionError,
  InvalidParameter,
} from '../../errors/General';
import { StockOrderGroupNotFoundError } from '../../errors/Stock';
import {
  StockCheckResponseType,
} from '../../types/Stock';

export default {
  type: new GraphQLNonNull(StockCheckResponseType),
  args: {
    checkCode: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.STOCK_INFO,
  })(async (_, { checkCode }, { authPayload }) => {
    const {
      id,
    } = authPayload;

    const targetQRCode = await db.models.OrderQRCode.findOne({
      where: {
        qrcode: checkCode,
      },
      include: [{
        model: db.models.Order,
        where: {
          status: 'DISPATCHED',
          EmpolyedMemberId: id,
        },
        include: [{
          model: db.models.PNTable.scope('categories'),
        }],
      }],
    });

    if (!targetQRCode) {
      throw new StockOrderGroupNotFoundError();
    }

    return {
      id: targetQRCode.id,
      pnTable: targetQRCode.Order.PNTable,
      amount: targetQRCode.Order.quantity,
    };
  }),
};
