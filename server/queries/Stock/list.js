import {
  GraphQLList,
  GraphQLInt,
  GraphQLString,
  GraphQLBoolean,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import {
  db,
} from '../../db';
import { actions } from '../../../shared/roleActions';
import {
  PermissionError,
} from '../../errors/General';
import {
  StockType,
  stockListType,
} from '../../types/Stock';

export const stockList = {
  type: new GraphQLList(stockListType),
  args: {
    memberId: {
      type: GraphQLInt,
    },
    keyword: {
      type: GraphQLString,
    },
    limit: {
      type: GraphQLInt,
      defaultValue: 10,
    },
    offset: {
      type: GraphQLInt,
      defaultValue: 0,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.INVOICING_MANAGE,
  })(async (_, args) => {
    const result = await db.models.MemberStock.scope(
      { method: ['search', args] },
      { method: ['pnTable', args] },
      'member',
    ).findAll({
      limit: args.limit,
      offset: args.offset,
    });

    return result.map((r, idx) => ({
      id: idx + 1,
      ...r.dataValues,
    }));
  }),
};

export default {
  type: new GraphQLList(StockType),
  args: {
    code: {
      type: GraphQLString,
    },
    pnCategoryId: {
      type: GraphQLInt,
    },
    bigSubCategoryId: {
      type: GraphQLInt,
    },
    smallSubCategoryId: {
      type: GraphQLInt,
    },
    keyword: {
      type: GraphQLString,
    },
    employeeId: {
      type: GraphQLInt,
    },
    brandId: {
      type: GraphQLInt,
    },
    brandModelId: {
      type: GraphQLInt,
    },
    isForPurchase: {
      type: GraphQLBoolean,
      defaultValue: false,
    },
    limit: {
      type: GraphQLInt,
      defaultValue: 10,
    },
    offset: {
      type: GraphQLInt,
      defaultValue: 0,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.STOCK_INFO,
  })(async (_, args, { authPayload }) => {
    const {
      employeeId,
      isForPurchase,
    } = args;

    const pnTables = await db.models.PNTable.scope(
      { method: ['search', args] },
      { method: ['brandAndModel', args.brandId, args.brandModelId] },
      { method: ['categories', args.pnCategoryId, args.bigSubCategoryId, args.smallSubCategoryId] },
      { method: ['stocks', employeeId || authPayload.id, !isForPurchase] },
      { method: ['pages', args.limit, args.offset] },
    ).findAll({
      include: [{
        model: db.models.Order.scope('store'),
        where: {
          EmpolyedMemberId: authPayload.id,
          status: 'ORDERING',
        },
        required: false,
      }],
    });

    const result = pnTables.map(pn => ({
      id: pn.id,
      pnTable: pn,
      amount: (pn.MemberStocks[0] && pn.MemberStocks[0].storageCount) || 0,
      orderingOrder: pn.Orders[0] || null,
    }));

    return result;
  }),
};
