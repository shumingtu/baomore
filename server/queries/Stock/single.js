import {
  GraphQLInt,
  GraphQLNonNull,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import {
  db,
} from '../../db';
import { actions } from '../../../shared/roleActions';
import {
  PermissionError,
} from '../../errors/General';
import {
  StockType,
} from '../../types/Stock';
import {
  PNTableItemNotFoundError,
} from '../../errors/PNTable';

export default {
  type: StockType,
  args: {
    pnTableId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    employeeId: {
      type: GraphQLInt,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.STOCK_INFO,
  })(async (_, args, { authPayload }) => {
    const {
      employeeId,
    } = args;

    const pnTable = await db.models.PNTable.scope(
      { method: ['search', { id: args.pnTableId }] },
      { method: ['stocks', employeeId || authPayload.id] },
      'categories',
      'vendor',
      'brandAndModel',
    ).findOne();

    if (!pnTable) throw new PNTableItemNotFoundError();

    // 看是否有ORDERING狀態的訂單

    const orderingOrder = await db.models.Order.scope(
      'store',
    ).findOne({
      where: {
        EmpolyedMemberId: authPayload.id,
        status: 'ORDERING',
        PNTableId: pnTable.id,
      },
    });

    return {
      id: pnTable.id,
      pnTable,
      amount: (pnTable.MemberStocks[0] && pnTable.MemberStocks[0].storageCount) || 0,
      orderingOrder: orderingOrder || null,
    };
  }),
};
