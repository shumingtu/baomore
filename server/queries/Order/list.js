// @flow

import {
  GraphQLList,
  GraphQLInt,
  GraphQLString,
  GraphQLNonNull,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';

import { db } from '../../db';
import { PermissionError } from '../../errors/General';
import { actions } from '../../../shared/roleActions';

import { orderType, orderQRCodeType } from '../../types/Order';

export const orderQRCodeList = {
  type: new GraphQLList(orderQRCodeType),
  args: {
    orderId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: async (_, { orderId }) => {
    const qrcodes = await db.models.OrderQRCode.findAll({
      where: {
        OrderId: orderId,
      },
    });

    return qrcodes;
  },
};

export const orderListByEmployee = {
  type: new GraphQLList(orderType),
  args: {
    status: {
      type: GraphQLString,
    },
    keyword: {
      type: GraphQLString,
    },
    orderSN: {
      type: GraphQLString,
    },
    closeDate: {
      type: GraphQLString,
    },
    qrcode: {
      type: GraphQLString,
    },
    limit: {
      type: GraphQLInt,
      defaultValue: 10,
    },
    offset: {
      type: GraphQLInt,
      defaultValue: 0,
    },
  },
  resolve: async (_, args, { authPayload }) => {
    if (!authPayload) {
      throw new PermissionError();
    }

    const {
      id,
    } = authPayload;

    const {
      limit,
      offset,
    } = args;

    const result = await db.models.Order.scope(
      { method: ['search', args] },
      'store',
      { method: ['pages', limit, offset] },
      { method: ['orderGroupShipment', args] },
      { method: ['qrcodes', args] },
    ).findAll({
      where: {
        EmpolyedMemberId: id,
      },
      include: [{
        model: db.models.PNTable.scope(
          'brandAndModel',
          'categories',
          { method: ['search', args] },
        ),
      }],
      order: [
        ['createdAt', 'DESC'],
      ],
    });

    return result;
  },
};

export const orderList = {
  type: new GraphQLList(orderType),
  args: {
    memberId: {
      type: GraphQLInt,
    },
    status: {
      type: GraphQLString,
    },
    keyword: {
      type: GraphQLString,
    },
    orderSN: {
      type: GraphQLString,
    },
    closeDate: {
      type: GraphQLString,
    },
    qrcode: {
      type: GraphQLString,
    },
    limit: {
      type: GraphQLInt,
      defaultValue: 10,
    },
    offset: {
      type: GraphQLInt,
      defaultValue: 0,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.INVOICING_MANAGE,
  })(async (_, args) => {
    const {
      limit,
      offset,
      memberId,
    } = args;

    const memberArgs = {
      memberId: memberId || null,
    };

    const result = await db.models.Order.scope(
      { method: ['memberArea', memberArgs] },
      { method: ['search', args] },
      'store',
      { method: ['pages', limit, offset] },
      { method: ['orderGroupShipment', args] },
      { method: ['qrcodes', args] },
    ).findAll({
      include: [{
        model: db.models.PNTable.scope(
          'brandAndModel',
          'categories',
          'vendor',
          { method: ['search', args] },
        ),
      }, {
        model: db.models.OnlineOrder,
      }],
      order: [
        ['createdAt', 'DESC'],
      ],
    });

    return result;
  }),
};

export default null;
