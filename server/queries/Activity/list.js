import {
  GraphQLList,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLBoolean,
} from 'graphql';
import {
  db,
} from '../../db';
import {
  categoryType,
  activityListType,
} from '../../types/Activity';

export const activityCategories = {
  type: new GraphQLList(categoryType),
  resolve: () => db.models.ActivityCategory.findAll(),
};

export const activities = {
  type: new GraphQLList(activityListType),
  args: {
    categoryId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    status: {
      type: GraphQLBoolean,
    },
  },
  resolve: async (_, { categoryId, status }) => {
    const whereClause = {
      ActivityCategoryId: categoryId,
    };

    if (status !== undefined) whereClause.isActived = status;

    const result = await db.models.Activity.findAll({
      where: whereClause,
    });

    return result;
  },
};

export default null;
