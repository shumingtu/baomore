import {
  GraphQLInt,
  GraphQLNonNull,
} from 'graphql';
import {
  db,
} from '../../db';
import {
  activityType,
  categoryType,
} from '../../types/Activity';

export const activityCategory = {
  type: categoryType,
  args: {
    categoryId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: (_, { categoryId }) => db.models.ActivityCategory.findOne({
    where: {
      id: categoryId,
    },
  }),
};

export const activity = {
  type: activityType,
  args: {
    activityId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: (_, { activityId }) => db.models.Activity.scope('includeDetailFragments').findOne({
    where: {
      id: activityId,
    },
  }),
};

export default activity;
