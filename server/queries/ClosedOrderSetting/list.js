// @flow

import {
  GraphQLList,
  GraphQLInt,
  GraphQLNonNull,
} from 'graphql';
import { db } from '../../db';
import {
  ClosedOrderSettingType,
} from '../../types/ClosedOrderSetting';

export const itemClosedOrderSetting = {
  type: ClosedOrderSettingType,
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: async (_, { id }) => {
    const result = await db.models.ClosedOrderSetting.findOne({
      where: {
        id,
      },
    });

    let PNTable = null;

    if (result && result.PNTableId) {
      PNTable = await db.models.PNTable.findOne({
        where: {
          id: result.PNTableId,
        },
      });
    }

    return {
      ...result.dataValues,
      PNTable,
    };
  },
};

export const listClosedOrderSetting = {
  type: new GraphQLList(ClosedOrderSettingType),
  resolve: async () => {
    const result = await db.models.ClosedOrderSetting.scope(
      'joinPNTable',
    ).findAll();

    return result;
  },
};

export default null;
