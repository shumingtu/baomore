// @flow

import {
  GraphQLList,
  GraphQLInt,
  GraphQLString,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import sequelize, { Op } from 'sequelize';
import lodash from 'lodash';
import moment from 'moment';

import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/General';
import { OrderGroupShipmentQueryType } from '../../types/OrderGroupShipment';

export const listOrderGroupShipment = {
  type: new GraphQLList(OrderGroupShipmentQueryType),
  args: {
    startDate: {
      type: GraphQLString,
    },
    endDate: {
      type: GraphQLString,
    },
    shippingStatus: {
      type: GraphQLString,
    },
    vendorId: {
      type: GraphQLInt,
    },
    memberId: {
      type: GraphQLInt,
    },
    areaId: {
      type: GraphQLInt,
    },
    code: {
      type: GraphQLString,
    },
    orderSN: {
      type: GraphQLString,
    },
    limit: {
      type: GraphQLInt,
      defaultValue: 10,
    },
    offset: {
      type: GraphQLInt,
      defaultValue: 0,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.INVOICING_MANAGE,
  })(async (_, args) => {
    const {
      limit,
      offset,
      orderSN,
      startDate,
      endDate,
      shippingStatus,
    } = args;

    const orderSQL = orderSN ? `AND Orders.orderSN = ${orderSN}` : '';

    const whereClause = [sequelize.literal(`EXISTS (SELECT tmp.id FROM OrderGroupShipments AS tmp INNER JOIN Orders ON Orders.OrderGroupShipmentId = tmp.id WHERE tmp.id = OrderGroupShipment.id AND Orders.orderSource != 'TRANSFER' ${orderSQL})`)];

    if (shippingStatus) {
      whereClause.push({
        status: shippingStatus,
      });
    }

    if (startDate) {
      whereClause.push({
        createdAt: {
          [Op.and]: [{
            [Op.gte]: moment(startDate),
          }, {
            [Op.lt]: (endDate && moment(endDate).add(1, 'd')) || moment().add(1, 'd'),
          }],
        },
      });
    }

    if (endDate && !startDate) {
      whereClause.push({
        createdAt: {
          [Op.lt]: moment(endDate).add(1, 'd'),
        },
      });
    }

    const rawResults = await db.models.OrderGroupShipment.scope(
      { method: ['pages', limit, offset] },
      { method: ['pnTableAllAssoicateAndOrder', args] },
    ).findAll({
      where: {
        [Op.and]: whereClause,
      },
      order: [['createdAt', 'DESC']],
    });

    const results = rawResults.map((r) => {
      const Orders = lodash(r.Orders).groupBy(x => x.Member.id).map(value => ({
        id: value[0].id,
        quantity: lodash.sumBy(value, v => v.quantity),
        status: value[0].status,
        Member: value[0].Member,
        orderSN: value[0].orderSN,
        orderSource: value[0].orderSource,
      })).value();

      const totalQuantity = lodash.sumBy(Orders, o => o.quantity);

      const isHaveSmallCategory = r.PNTable.PNSubCategory.PNSubCategory;

      return {
        ...r.dataValues,
        totalQuantity,
        PNTable: {
          ...r.PNTable.dataValues,
          pnCategory: r.PNTable.PNSubCategory.PNCategory,
          bigSubCategory: isHaveSmallCategory ? r.PNTable.PNSubCategory.PNSubCategory : r.PNTable.PNSubCategory,
          smallSubCategory: isHaveSmallCategory ? r.PNTable.PNSubCategory : null,
          vendor: r.PNTable.Vendor,
        },
        Orders,
      };
    });

    return results;
  }),
};

export default null;
