import {
  GraphQLList,
  GraphQLInt,
  GraphQLNonNull,
} from 'graphql';
import {
  db,
} from '../../db';
import {
  cityType,
} from '../../types/CityDistrict';

import {
  districtType,
} from '../../types/District';

export const cityDistrictList = {
  type: new GraphQLList(cityType),
  resolve: () => db.models.City.scope('districts').findAll(),
};

export const districtList = {
  type: new GraphQLList(districtType),
  args: {
    cityId: {
      type: GraphQLInt,
    },
  },
  resolve: async (_, args) => {
    const {
      cityId,
    } = args;

    const whereClause = {};

    if (cityId) whereClause.CityId = cityId;

    return db.models.District.findAll({
      where: whereClause,
    });
  },
};
