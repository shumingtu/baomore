// @flow

import {
  GraphQLNonNull,
} from 'graphql';
import { db } from '../../db';
import { memberMeType } from '../../types/Member.js';
import {
  PermissionError,
  MemberNotFoundError,
} from '../../errors/General';

export default {
  type: new GraphQLNonNull(memberMeType),
  resolve: async (n: null, args, { authPayload }) => {
    if (!authPayload) {
      throw new PermissionError();
    }

    const {
      id,
    } = authPayload;

    const memberMe = await db.models.Member.getMe(id);

    if (!memberMe) throw new MemberNotFoundError();

    return memberMe;
  },
};
