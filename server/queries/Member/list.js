import {
  GraphQLList,
  GraphQLInt,
  GraphQLString,
  GraphQLBoolean,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';

import {
  db,
} from '../../db';
import {
  employeePunchType,
  memberType,
} from '../../types/Member';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';

export const allMemberList = {
  type: new GraphQLList(memberType),
  args: {
    memberId: {
      type: GraphQLInt,
    },
    keyword: {
      type: GraphQLString,
    },
    limit: {
      type: GraphQLInt,
    },
    offset: {
      type: GraphQLInt,
    },
    roleId: {
      type: GraphQLInt,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_MEMBER_MANAGE,
  })(async (_, args) => {
    const members = await db.models.Member.scope(
      { method: ['search', args] },
      (args.limit && args.offset !== undefined) ? { method: ['pages', args.limit, args.offset] } : {},
    ).findAll({
      include: [{
        model: db.models.Role.scope(
          { method: ['search', args] }
        ),
      }, {
        model: db.models.Area,
      }],
    });

    return members;
  }),
};

export const employeeMemberlist = {
  type: new GraphQLList(memberType),
  args: {
    memberId: {
      type: GraphQLInt,
    },
    storeId: {
      type: GraphQLInt,
    },
    areaId: {
      type: GraphQLInt,
    },
    employeeType: {
      type: GraphQLString,
    },
    limit: {
      type: GraphQLInt,
    },
    offset: {
      type: GraphQLInt,
    },
    keyword: {
      type: GraphQLString,
    },
    directorId: {
      type: GraphQLInt,
    },
    lineIdRequired: {
      type: GraphQLBoolean,
      defaultValue: false,
    },
    isForClient: {
      type: GraphQLBoolean,
      defaultValue: false,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.EMPLOYEE_INFO,
  })(async (_, args) => db.models.Member.scope(
    { method: ['employee', args] },
    { method: ['search', args] },
    (args.limit && args.offset !== undefined) ? { method: ['pages', args.limit, args.offset] } : {},
    'warranty',
    'onlineOrderStore',
  ).findAll({
    order: ['id'],
  })),
};

export const punchlist = {
  type: new GraphQLList(employeePunchType),
  args: {
    startDate: {
      type: GraphQLString,
    },
    endDate: {
      type: GraphQLString,
    },
    employeeId: {
      type: GraphQLInt,
    },
    storeId: {
      type: GraphQLInt,
    },
    limit: {
      type: GraphQLInt,
      defaultValue: 10,
    },
    offset: {
      type: GraphQLInt,
      defaultValue: 0,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.CHECKIN,
  })(async (_, {
    startDate,
    endDate,
    storeId,
    employeeId,
    limit,
    offset,
  }, { authPayload }) => db.models.MemberPunchRecord.scope(
    {
      method: ['search', {
        storeId,
        memberId: employeeId || authPayload.id,
        startDate,
        endDate,
      }],
    },
    { method: ['pages', limit, offset] },
    'store',
  ).findAll({
    order: [
      ['punchTime', 'DESC'],
    ],
  })),
};

export const adminPunchList = {
  type: new GraphQLList(employeePunchType),
  args: {
    startDate: {
      type: GraphQLString,
    },
    endDate: {
      type: GraphQLString,
    },
    employeeId: {
      type: GraphQLInt,
    },
    limit: {
      type: GraphQLInt,
      defaultValue: 10,
    },
    offset: {
      type: GraphQLInt,
      defaultValue: 0,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_MEMBER_MANAGE,
  })(async (_, {
    startDate,
    endDate,
    employeeId,
    limit,
    offset,
  }) => db.models.MemberPunchRecord.scope(
    {
      method: ['search', {
        startDate,
        endDate,
        memberId: employeeId || null,
      }],
    },
    { method: ['pages', limit, offset] },
    'store',
    'member',
  ).findAll({
    order: [
      ['punchTime', 'DESC'],
      ['MemberId', 'DESC'],
    ],
  })),
};


export default employeeMemberlist;
