// @flow

import {
  GraphQLList,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { onlineOrderType } from '../../types/OnlineOrder.js';
import {
  PermissionError,
  MemberNotFoundError,
} from '../../errors/General';

export default {
  type: new GraphQLList(onlineOrderType),
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.NORMAL_MEMBER,
  })(async (n: null, args, { authPayload }) => {
    if (!authPayload) {
      throw new PermissionError();
    }

    const memberMe = await db.models.Member.getMe(authPayload.id);

    if (!memberMe) throw new MemberNotFoundError();

    const {
      id,
    } = memberMe;

    const orders = await db.models.OnlineOrder.scope(
      'store',
      'order',
    ).findAll({
      where: {
        OwnerId: id,
      },
      order: [['createdAt', 'DESC']],
    });

    return orders;
  }),
};
