// @flow

import {
  GraphQLList,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { memberWarrantyType } from '../../types/Warranty.js';
import {
  PermissionError,
} from '../../errors/General';

export default {
  type: new GraphQLList(memberWarrantyType),
  args: {
    limit: {
      type: GraphQLInt,
      defaultValue: 10,
    },
    offset: {
      type: GraphQLInt,
      defaultValue: 0,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.NORMAL_MEMBER,
  })(async (n: null, args, { authPayload }) => {
    const {
      id,
    } = authPayload;

    const records = await db.models.MemberWarranty.scope(
      { method: ['pages', args.limit, args.offset] },
    ).findAll({
      where: {
        MemberId: id,
      },
      order: [['createdAt', 'DESC']],
    });

    return records;
  }),
};
