import {
  GraphQLList,
  GraphQLString,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';

import {
  db,
} from '../../db';
import {
  memberAssignmentType,
} from '../../types/Schedule';
import {
  PermissionError,
} from '../../errors/General';
import { actions } from '../../../shared/roleActions';

export const meScheduleDates = {
  type: new GraphQLList(GraphQLString),
  args: {
    startDate: {
      type: GraphQLString,
    },
    endDate: {
      type: GraphQLString,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.STROKE_INFO,
  })(async (_, args, { authPayload }) => {
    const results = await db.models.OnlineOrder.scope(
      { method: ['search', args] },
      { method: ['bookedMember', authPayload.id] },
      'payedAndConfirmed',
    ).findAll();
    return results.map(result => result.date);
  }),
};

export const meScheduleDateAssignments = {
  type: new GraphQLList(memberAssignmentType),
  args: {
    date: {
      type: GraphQLString,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.STROKE_INFO,
  })(async (_, args, { authPayload }) => {
    const results = await db.models.OnlineOrder.scope(
      { method: ['search', args] },
      'store',
      'order',
      { method: ['bookedMember', authPayload.id] },
      'payedAndConfirmed',
    ).findAll();

    return results.map(result => ({
      time: result.time,
      order: result,
    }));
  }),
};

export default meScheduleDates;
