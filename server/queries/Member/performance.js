import {
  GraphQLInt,
  GraphQLString,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import lodash from 'lodash';
import moment from 'moment';
import { Op } from 'sequelize';

import {
  db,
} from '../../db';
import {
  employeePerformanceType,
} from '../../types/Member';
import {
  PermissionError,
} from '../../errors/General';
import { actions } from '../../../shared/roleActions';

import {
  getRecordGroupByPNCategoryId,
  getClientCalResult,
} from '../../helpers/performanceCalculator.js';

export const performance = {
  type: employeePerformanceType,
  args: {
    startDate: {
      type: GraphQLString,
    },
    endDate: {
      type: GraphQLString,
    },
    employeeId: {
      type: GraphQLInt,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.PERFORMANCE_INFO,
  })(async (_, args, { authPayload }) => {
    const {
      startDate,
      endDate,
      employeeId,
    } = args;

    const {
      id,
    } = authPayload;

    const startMonth = moment().startOf('month').format('YYYY-MM-DD');
    const endMonth = moment().endOf('month').format('YYYY-MM-DD');

    const parameter = {
      startDate: startDate || startMonth,
      endDate: endDate || endMonth,
      employeeId: employeeId || id,
    };

    const rawResult = await db.models.BehaviorRecord.scope(
      { method: ['searchForPerformance', parameter] },
      { method: ['performanceMemberAssociation', { employeeType: '包膜師' }] },
      { method: ['performancePNTableAssociation'] },
    ).findAll({
      attributes: ['id', 'startTime', 'endTime', 'price', 'BehaviorId', 'MemberId', 'PNTableId'],
      where: {
        BehaviorId: 1,
      },
    });

    const memberSchedule = await db.models.MemberSchedule.scope(
      { method: ['search', parameter] },
    ).findAll({
      attributes: ['id', 'date', 'status', 'MemberId'],
      where: {
        [Op.or]: [{
          status: '4',
        }, {
          status: '004',
        }],
      },
    });

    const performanceSettings = await db.models.PerformanceSetting.findAll({
      attributes: ['id', 'PNCategoryId', 'targetAmount', 'commission'],
    });

    const lastMonthStartTime = moment(startDate || startMonth).subtract(1, 'months').format('YYYY-MM-DD');
    const lastMonthEndTime = moment(endDate || endMonth).subtract(1, 'months').format('YYYY-MM-DD');

    const lastMonthArgs = {
      ...parameter,
      startDate: lastMonthStartTime,
      endDate: lastMonthEndTime,
    };


    const lastMonthRecord = await db.models.BehaviorRecord.scope(
      { method: ['searchForPerformance', lastMonthArgs] },
      { method: ['performanceMemberAssociation', { employeeType: '包膜師' }] },
      { method: ['performancePNTableAssociation'] },
    ).findAll({
      attributes: ['id', 'price', 'BehaviorId', 'MemberId', 'PNTableId'],
      where: {
        BehaviorId: 1,
      },
    });

    const totalDays = (
      (moment(endDate || endMonth) - moment(startDate || startMonth)) / 86400000) + 1;

    const groupByMember = lodash.groupBy(rawResult, 'MemberId');

    const calculatedResult = await Object.keys(groupByMember).map(memberId => async (results) => {
      const memberRecord = groupByMember[memberId];

      const lastMonthRecordByMember = lastMonthRecord
        .filter(x => x.MemberId === parseInt(memberId, 10));

      const workDays = memberSchedule.filter(x => x.MemberId === parseInt(memberId, 10)).length;

      const recordGroupByPNCategoryId = await getRecordGroupByPNCategoryId(
        memberRecord,
        performanceSettings,
        lastMonthRecordByMember,
        totalDays,
        workDays,
        [parseInt(memberId, 10)],
      );

      return [
        ...results,
        {
          workDays,
          member: memberRecord[0].Member,
          performance: recordGroupByPNCategoryId,
        },
      ];
    }).reduce((prev, next) => prev.then(next), Promise.resolve([]));

    if (!calculatedResult.length) return null;

    const materialSetting = performanceSettings.find(x => x.PNCategoryId === 1);
    const protectorSetting = performanceSettings.find(x => x.PNCategoryId === 2);

    const materialPerformance = calculatedResult[0].performance.find(x => x.pnCategory.id === 1);
    const protectorPerformance = calculatedResult[0].performance.find(x => x.pnCategory.id === 2);

    const clientCalculatedResult = getClientCalResult(
      materialPerformance,
      protectorPerformance,
      materialSetting,
      protectorSetting,
      totalDays,
    );

    return {
      id: calculatedResult[0].member.id,
      ...clientCalculatedResult,
    };
  }),
};

export default performance;
