import {
  GraphQLInt,
  GraphQLNonNull,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import {
  db,
} from '../../db';
import { actions } from '../../../shared/roleActions';
import {
  PermissionError,
} from '../../errors/General';
import {
  announcementType,
} from '../../types/Announcement';

export const announcement = {
  type: announcementType,
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.NOTIFICATION_INFO,
  })((_, args) => (
    db.models.Announcement.scope(
      { method: ['search', args] },
    ).findOne()
  )),
};

export default null;
