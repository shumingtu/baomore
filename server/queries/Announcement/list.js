import {
  GraphQLList,
  GraphQLInt,
  GraphQLString,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import {
  db,
} from '../../db';
import { actions } from '../../../shared/roleActions';
import {
  PermissionError,
} from '../../errors/General';
import {
  announcementType,
} from '../../types/Announcement';

export const announcements = {
  type: new GraphQLList(announcementType),
  args: {
    keyword: {
      type: GraphQLString,
    },
    startDate: {
      type: GraphQLString,
    },
    endDate: {
      type: GraphQLString,
    },
    limit: {
      type: GraphQLInt,
      defaultValue: 10,
    },
    offset: {
      type: GraphQLInt,
      defaultValue: 0,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.NOTIFICATION_INFO,
  })((_, args) => (
    db.models.Announcement.scope(
      { method: ['search', args] },
      { method: ['pages', args.limit, args.offset] },
      'order',
    ).findAll()
  )),
};

export default null;
