import {
  GraphQLList,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';

import {
  db,
} from '../../db';
import {
  roleType,
} from '../../types/Role';
import { PermissionError } from '../../errors/General';
import { actions } from '../../../shared/roleActions';

export const roleList = {
  type: new GraphQLList(roleType),
  args: {
    limit: {
      type: GraphQLInt,
    },
    offset: {
      type: GraphQLInt,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_MEMBER_MANAGE,
  })(async (_, args) => {
    const {
      limit,
      offset,
    } = args;

    const result = await db.models.Role.scope(
      'action',
      (limit && offset !== undefined) ? { method: ['pages', limit, offset] } : {},
    ).findAll();

    return result;
  }),
};

export default roleList;
