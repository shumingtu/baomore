// @flow

import {
  GraphQLList,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';
import { db } from '../../db';
import {
  PNTableType,
  PNTableCodeType,
  CustomPNTableType,
} from '../../types/PNTable';
import {
  PNTableItemNotFoundError,
} from '../../errors/PNTable';

export const listFilterBySubCategory = {
  type: new GraphQLList(PNTableType),
  args: {
    subCategoryId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: async (_, {
    subCategoryId,
  }) => db.models.PNTable.scope({ method: ['filterBySubCategory', subCategoryId] }).findAll({
    where: {
      isOnSale: true,
    },
  }),
};

export const pnTableItem = {
  type: PNTableType,
  args: {
    id: {
      type: GraphQLInt,
    },
  },
  resolve: async (_, { id }) => {
    const result = await db.models.PNTable.scope(
      { method: ['search', { id }] },
      { method: ['vendor'] },
      { method: ['categories'] },
      { method: ['brandAndModel'] },
    ).findOne();

    if (!result) throw new PNTableItemNotFoundError();

    const isHaveSmallCategory = result.PNSubCategory.PNSubCategory;

    return {
      ...result.dataValues,
      pnCategory: result.PNSubCategory.PNCategory,
      bigSubCategory: isHaveSmallCategory ? result.PNSubCategory.PNSubCategory : result.PNSubCategory,
      smallSubCategory: isHaveSmallCategory ? result.PNSubCategory : null,
      Vendor: result.Vendor,
      BrandModel: (result && result.BrandModel) || null,
      Brand: (result && result.BrandModel && result.BrandModel.Brand) || null,
    };
  },
};

export const customPNTable = {
  type: CustomPNTableType,
  resolve: async (_, { id }) => {
    const result = await db.models.PNTable.scope(
      { method: ['search', { isAllCustomized: true }] },
    ).findOne();

    if (!result) throw new PNTableItemNotFoundError();

    return result;
  },
};

export const listForPNTableCode = {
  type: new GraphQLList(PNTableCodeType),
  args: {
    code: {
      type: GraphQLString,
    },
  },
  resolve: async (_, args) => {
    const result = db.models.PNTable.scope({ method: ['findCode', args.code] }).findAll();
    return result;
  },
};

export const listFilterByAdmin = {
  type: new GraphQLList(PNTableType),
  args: {
    code: {
      type: GraphQLString,
    },
    pnCategoryId: {
      type: GraphQLInt,
    },
    bigSubCategoryId: {
      type: GraphQLInt,
    },
    smallSubCategoryId: {
      type: GraphQLInt,
    },
    keyword: {
      type: GraphQLString,
    },
    vendorId: {
      type: GraphQLInt,
    },
    brandId: {
      type: GraphQLInt,
    },
    modelId: {
      type: GraphQLInt,
    },
    limit: {
      type: GraphQLInt,
      defaultValue: 10,
    },
    offset: {
      type: GraphQLInt,
      defaultValue: 0,
    },
  },
  resolve: async (_, args) => {
    const {
      pnCategoryId,
      bigSubCategoryId,
      smallSubCategoryId,
      limit,
      offset,
      vendorId,
      brandId,
      modelId,
    } = args;

    const results = await db.models.PNTable.scope(
      { method: ['search', args] },
      { method: ['pages', limit, offset] },
      { method: ['vendor', vendorId] },
      { method: ['categories', pnCategoryId, bigSubCategoryId, smallSubCategoryId] },
      { method: ['brandAndModel', brandId, modelId] },
    ).findAll();

    const filteredResults = results.map((r) => {
      const isHaveSmallCategory = r.PNSubCategory.PNSubCategory;
      return {
        ...r.dataValues,
        pnCategory: r.PNSubCategory.PNCategory,
        bigSubCategory: isHaveSmallCategory ? r.PNSubCategory.PNSubCategory : r.PNSubCategory,
        smallSubCategory: isHaveSmallCategory ? r.PNSubCategory : null,
        Vendor: r.Vendor,
      };
    });

    return filteredResults;
  },
};
