// @flow

import {
  GraphQLList,
  GraphQLString,
  GraphQLInt,
  GraphQLBoolean,
} from 'graphql';
import { db } from '../../../db';
import {
  PNSubCategoryType,
} from '../../../types/PNTable';

export const listForSale = {
  type: new GraphQLList(PNSubCategoryType),
  resolve: async () => db.models.PNSubCategory.scope(
    'onSale',
    'orderBySeqNum',
    'bigSubCategory',
  ).findAll(),
};

export const listFilterByPNCategory = {
  type: new GraphQLList(PNSubCategoryType),
  description: '取得根據料好類型的大類列表, 帶PNCategoryId',
  args: {
    id: {
      type: GraphQLInt,
    },
    keyword: {
      type: GraphQLString,
    },
    isOnSale: {
      type: GraphQLBoolean,
      defaultValue: false,
    },
  },
  resolve: async (_, {
    id,
    keyword,
    isOnSale,
  }) => db.models.PNSubCategory.scope(
    { method: ['findByPNCategoryId', id] },
    { method: ['filterByKeyword', keyword] },
    isOnSale ? 'onSale' : {},
  ).findAll(),
};

export const listFilterByPNSubCategory = {
  type: new GraphQLList(PNSubCategoryType),
  args: {
    id: {
      type: GraphQLInt,
    },
    keyword: {
      type: GraphQLString,
    },
    isOnSale: {
      type: GraphQLBoolean,
      defaultValue: false,
    },
  },
  resolve: async (_, {
    id,
    keyword,
    isOnSale,
  }) => db.models.PNSubCategory.scope(
    { method: ['findBySubCategoryId', id] },
    { method: ['filterByKeyword', keyword] },
    isOnSale ? 'onSale' : {},
  ).findAll(),
};
