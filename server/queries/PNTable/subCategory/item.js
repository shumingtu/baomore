// @flow

import {
  GraphQLNonNull,
  GraphQLInt,
} from 'graphql';
import { db } from '../../../db';
import {
  PNSubCategoryType,
} from '../../../types/PNTable';

export const pnSubCategoryItem = {
  type: PNSubCategoryType,
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: async (_, { id }) => db.models.PNSubCategory.scope({ method: ['findById', id] }).findOne(),
};

export default null;
