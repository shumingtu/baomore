// @flow

import {
  GraphQLList,
  GraphQLString,
} from 'graphql';
import { db } from '../../../db';
import {
  PNCategoryType,
} from '../../../types/PNTable';

export const listForPNCategory = {
  type: new GraphQLList(PNCategoryType),
  resolve: async () => db.models.PNCategory.findAll(),
};

export const listPNCatgoryFilterByKeyword = {
  type: new GraphQLList(PNCategoryType),
  args: {
    keyword: {
      type: GraphQLString,
    },
  },
  resolve: async (_, {
    keyword,
  }) => db.models.PNCategory.scope({ method: ['filterByKeyword', keyword] }).findAll(),
};

export default listForPNCategory;
