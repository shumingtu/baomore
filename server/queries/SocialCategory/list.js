import fs from 'fs';
import {
  GraphQLList,
  GraphQLInt,
} from 'graphql';
import {
  listType,
} from '../../types/SocialCategory';

export const socialCategories = {
  type: new GraphQLList(listType),
  args: {
    id: {
      type: GraphQLInt,
    },
  },
  resolve: (_, { id }) => {
    const rawData = fs.readFileSync('shared/socialCategory.json');

    if (!rawData) return null;

    const result = JSON.parse(rawData).socialCategory;

    if (id) {
      const singleResult = result.find(x => x.id === id);

      return [singleResult];
    }

    return result;
  },
};

export default socialCategories;
