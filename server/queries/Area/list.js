import {
  GraphQLList,
} from 'graphql';
import { db } from '../../db';
import { AreaType } from '../../types/Area';

export const listArea = {
  type: new GraphQLList(AreaType),
  resolve: async () => db.models.Area.findAll(),
};

export default null;
