export default ({
  DATE,
  INTEGER,
  STRING,
  ENUM,
}) => ({
  id: {
    type: INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  weekDay: {
    type: ENUM,
    values: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
    defaultValue: 'MON',
  },
  time: {
    type: STRING,
    allowNull: false,
  },
  createdAt: {
    type: DATE,
    allowNull: false,
  },
  updatedAt: {
    type: DATE,
    allowNull: false,
  },
  deletedAt: {
    type: DATE,
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  PNTableId: {
    type: INTEGER,
    references: {
      model: 'PNTables',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
});
