export default ({
  INTEGER,
  STRING,
  DATE,
  TEXT,
  DOUBLE,
  ENUM,
  BOOLEAN,
}) => ({
  id: {
    type: INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  startTime: {
    type: DATE,
  },
  endTime: {
    type: DATE,
  },
  picture: {
    type: STRING,
  },
  price: {
    type: DOUBLE,
  },
  remark: {
    type: TEXT,
  },
  createdAt: {
    type: DATE,
    allowNull: false,
  },
  updatedAt: {
    type: DATE,
    allowNull: false,
  },
  deletedAt: {
    type: DATE,
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  isTransfering: {
    type: BOOLEAN,
    allowNull: false,
    defaultValue: 0,
  },
  BehaviorId: {
    type: INTEGER,
    references: {
      model: 'Behaviors',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
  MemberId: {
    type: INTEGER,
    references: {
      model: 'Members',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
  VendorId: {
    type: INTEGER,
    references: {
      model: 'Vendors',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
  StoreId: {
    type: INTEGER,
    references: {
      model: 'Stores',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
  PNTableId: {
    type: INTEGER,
    references: {
      model: 'PNTables',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
  rollbackType: {
    type: ENUM,
    values: ['NONE', 'DOA', 'PULLOF', 'SOCIAL', 'CUSTOMERCOMPLAINT'],
    defaultValue: 'NONE',
  },
  ApproverId: {
    type: INTEGER,
    references: {
      model: 'Members',
      as: 'Approver',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
  orderSN: {
    type: STRING,
  },
  newOrderSN: {
    type: STRING,
  },
  quantity: {
    type: INTEGER,
  },
  qrcode: {
    type: STRING,
  },
  isTransfering: {
    type: BOOLEAN,
    defaultValue: false,
  },
});
