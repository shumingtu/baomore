export default ({
  INTEGER,
  DATE,
  STRING,
}) => ({
  id: {
    type: INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  name: {
    type: STRING,
    allowNull: false,
    defaultValue: '白色',
  },
  phoneBg: {
    type: STRING,
    allowNull: false,
  },
  phoneMask: {
    type: STRING,
    allowNull: false,
  },
  phoneCover: {
    type: STRING,
    allowNull: false,
  },
  createdAt: {
    type: DATE,
    allowNull: false,
  },
  updatedAt: {
    type: DATE,
    allowNull: false,
  },
  deletedAt: {
    type: DATE,
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  BrandModelId: {
    type: INTEGER,
    references: {
      model: 'BrandModels',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
});
