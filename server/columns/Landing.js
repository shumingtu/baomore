export default ({
  INTEGER,
  STRING,
  DATE,
}) => ({
  id: {
    type: INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  desktopImg: {
    type: STRING,
    allowNull: false,
  },
  mobileImg: {
    type: STRING,
    allowNull: false,
  },
  link: {
    type: STRING,
    defaultValue: null,
  },
  createdAt: {
    type: DATE,
    allowNull: false,
  },
  updatedAt: {
    type: DATE,
    allowNull: false,
  },
  deletedAt: {
    type: DATE,
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
});
