export default ({
  INTEGER,
  STRING,
  DATE,
  ENUM,
}) => ({
  id: {
    type: INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  orderSN: { // 訂單編號 - For ERP - 規則
    type: STRING,
  },
  qrcode: {
    type: STRING,
  },
  hashQRCode: {
    type: STRING,
  },
  quantity: { // 數量
    type: INTEGER,
    allowNull: false,
  },
  picture: {
    type: STRING,
    allowNull: false,
  },
  status: {
    type: ENUM,
    values: ['ORDERING', 'UNDISPATCHED', 'DISPATCHED', 'DISPATCHEDANDCHECKED', 'SOLDED'], // 未出貨、已出貨未銷貨、已出貨且以銷貨、已賣
    defaultValue: 'ORDERING',
  },
  orderSource: {
    type: ENUM,
    values: ['EMPOLYEE', 'ONLINEORDER', 'TRANSFER'], // 包膜師訂貨、線上訂單或調貨
    defaultValue: 'EMPOLYEE',
  },
  createdAt: {
    type: DATE,
    allowNull: false,
  },
  updatedAt: {
    type: DATE,
    allowNull: false,
  },
  deletedAt: {
    type: DATE,
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  StoreId: {
    type: INTEGER,
    references: {
      model: 'Stores',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
  EmpolyedMemberId: {
    type: INTEGER,
    references: {
      model: 'Members',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
  PNTableId: {
    type: INTEGER,
    references: {
      model: 'PNTables',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
  OrderGroupShipmentId: {
    type: INTEGER,
    references: {
      model: 'OrderGroupShipments',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
  ProductType: {
    type: ENUM,
    values: ['PROTECTOR', 'MATERIAL', 'CUSTOMIZE'],
    defaultValue: 'MATERIAL',
  },
  invoiceNumber: {
    type: STRING,
  },
  invoiceDate: {
    type: DATE,
  },
});
