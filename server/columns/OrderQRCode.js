export default ({
  STRING,
}) => ({
  qrcode: {
    type: STRING,
    allowNull: false,
  },
});
