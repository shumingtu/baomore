export default ({
  DATE,
  BOOLEAN,
}) => ({
  sendTransferTime: {
    type: DATE,
    allowNull: false,
  },
  isNotified: {
    type: BOOLEAN,
    defaultValue: false,
  },
});
