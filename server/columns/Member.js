import bcrypt from 'bcrypt';

export default ({
  INTEGER,
  DATE,
  STRING,
  ENUM,
  TEXT,
  BOOLEAN,
}) => ({
  id: {
    type: INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  serialNumber: { // 工號
    type: STRING,
  },
  account: {
    type: STRING,
    unique: true,
  },
  password: {
    type: STRING(60),
    set(plaintext) {
      this.setDataValue('password', bcrypt.hashSync(plaintext, bcrypt.genSaltSync(10)));
    },
  },
  lineId: {
    type: STRING,
  },
  name: {
    type: STRING,
  },
  avatar: {
    type: STRING,
  },
  email: {
    type: STRING,
  },
  gender: {
    type: ENUM,
    values: ['male', 'female'],
    defaultValue: 'male',
  },
  birthday: {
    type: DATE,
  },
  phone: {
    type: STRING,
  },
  refreshToken: {
    type: STRING,
  },
  createdAt: {
    type: DATE,
    allowNull: false,
  },
  updatedAt: {
    type: DATE,
    allowNull: false,
  },
  deletedAt: {
    type: DATE,
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  storedDesignJSON: {
    type: TEXT,
  },
  AreaId: {
    type: INTEGER,
    references: {
      model: 'Areas',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
  managerName: {
    type: STRING,
  },
  lineAccount: {
    type: STRING,
  },
  archive: {
    type: BOOLEAN,
    allowNull: false,
    defaultValue: 0,
  },
});
