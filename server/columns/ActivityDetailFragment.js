export default ({
  INTEGER,
  STRING,
  TEXT,
  DATE,
  ENUM,
}) => ({
  id: {
    type: INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  type: {
    type: ENUM,
    values: ['TEXT'],
  },
  title: {
    type: STRING,
    defaultValue: null,
  },
  content: {
    type: TEXT,
    allowNull: false,
  },
  createdAt: {
    type: DATE,
    allowNull: false,
  },
  updatedAt: {
    type: DATE,
    allowNull: false,
  },
  deletedAt: {
    type: DATE,
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  ActivityId: {
    type: INTEGER,
    references: {
      model: 'Activities',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
  seqNum: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 1,
  },
});
