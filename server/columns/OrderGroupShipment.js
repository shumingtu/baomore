export default ({
  INTEGER,
  ENUM,
  DATE,
  STRING,
}) => ({
  id: {
    type: INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  SN: {
    type: STRING,
    allowNull: false,
  },
  status: {
    type: ENUM,
    values: ['UNSHIPMENTED', 'SHIPMENTED', 'CANCELEDSHIPMENT'], // 未出貨、已出貨、取消出貨
    default: 'UNSHIPMENTED',
  },
  startDate: { // 結單起始日
    type: DATE,
    allowNull: false,
  },
  endDate: { // 結單日
    type: DATE,
    allowNull: false,
  },
  createdAt: {
    type: DATE,
    allowNull: false,
  },
  updatedAt: {
    type: DATE,
    allowNull: false,
  },
  deletedAt: {
    type: DATE,
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  PNTableId: {
    type: INTEGER,
    references: {
      model: 'PNTables',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
});
