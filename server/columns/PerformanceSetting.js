export default ({
  INTEGER,
  DATE,
  FLOAT,
}) => ({
  id: {
    type: INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  targetAmount: {
    type: INTEGER,
    allowNull: false,
  },
  commission: {
    type: FLOAT,
    allowNull: false,
  },
  createdAt: {
    type: DATE,
    allowNull: false,
  },
  updatedAt: {
    type: DATE,
    allowNull: false,
  },
  deletedAt: {
    type: DATE,
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
});
