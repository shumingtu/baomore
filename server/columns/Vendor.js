export default ({
  INTEGER,
  STRING,
  DATE,
}) => ({
  id: {
    type: INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  name: {
    type: STRING,
  },
  contactPersonName: {
    type: STRING,
  },
  phone: {
    type: STRING,
  },
  address: {
    type: STRING,
  },
  createdAt: {
    type: DATE,
    allowNull: false,
  },
  updatedAt: {
    type: DATE,
    allowNull: false,
  },
  deletedAt: {
    type: DATE,
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
});
