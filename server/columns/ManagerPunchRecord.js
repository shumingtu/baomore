export default ({
  INTEGER,
  DATE,
  DOUBLE,
  STRING,
  TEXT,
}) => ({
  id: {
    type: INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  punchStartTime: {
    type: DATE,
    allowNull: false,
  },
  punchEndTime: {
    type: DATE,
    allowNull: false,
  },
  grade: {
    type: DOUBLE,
    allowNull: false,
  },
  remark: {
    type: TEXT,
  },
  createdAt: {
    type: DATE,
    allowNull: false,
  },
  updatedAt: {
    type: DATE,
    allowNull: false,
  },
  deletedAt: {
    type: DATE,
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  EmployeedMemberId: {
    type: INTEGER,
    references: {
      model: 'Members',
      as: 'EmployeedMember',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
  MemberId: {
    type: INTEGER,
    references: {
      model: 'Members',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
  StoreId: {
    type: INTEGER,
    references: {
      model: 'Stores',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
});
