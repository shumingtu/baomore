export default ({
  INTEGER,
  STRING,
  DATE,
}) => ({
  id: {
    type: INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  name: {
    type: STRING,
    allowNull: false,
  },
  createdAt: {
    type: DATE,
    allowNull: false,
  },
  updatedAt: {
    type: DATE,
    allowNull: false,
  },
  deletedAt: {
    type: DATE,
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  CityId: {
    type: INTEGER,
    references: {
      model: 'Cities',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
});
