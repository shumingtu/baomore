export default ({
  DATE,
  BOOLEAN,
}) => ({
  sendBookedInfoTime: {
    type: DATE,
    allowNull: false,
  },
  isNotified: {
    type: BOOLEAN,
    defaultValue: false,
  },
});
