// 料號資料表
export default ({
  DATE,
  INTEGER,
  STRING,
  ENUM,
  TEXT,
  BOOLEAN,
  DOUBLE,
}) => ({
  id: {
    type: INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  code: {
    type: STRING,
    allowNull: false,
  },
  name: {
    type: STRING,
    allowNull: false,
  },
  picture: {
    type: STRING,
  },
  suitableDevice: {
    type: ENUM,
    values: ['PHONE', 'PAD', 'DESKTOP'],
    defaultValue: 'PHONE',
  },
  description: {
    type: TEXT,
  },
  isOnSale: {
    type: BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  isAllCustomized: {
    type: BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  price: {
    type: DOUBLE,
  },
  onlinePrice: {
    type: DOUBLE,
  },
  createdAt: {
    type: DATE,
    allowNull: false,
  },
  updatedAt: {
    type: DATE,
    allowNull: false,
  },
  deletedAt: {
    type: DATE,
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  PNSubCategoryId: {
    type: INTEGER,
    references: {
      model: 'PNSubCategories',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
  BrandModelId: {
    type: INTEGER,
    references: {
      model: 'BrandModels',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
  VendorId: {
    type: INTEGER,
    references: {
      model: 'Vendors',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
});
