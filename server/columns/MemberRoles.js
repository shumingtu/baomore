export default ({
  INTEGER,
  DATE,
}) => ({
  createdAt: {
    type: DATE,
    allowNull: false,
  },
  updatedAt: {
    type: DATE,
    allowNull: false,
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  MemberId: {
    type: INTEGER,
    references: {
      model: 'Members',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
  RoleId: {
    type: INTEGER,
    references: {
      model: 'Roles',
      key: 'id',
    },
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
});
