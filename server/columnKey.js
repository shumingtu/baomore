export const warrantyCardColumn = [
  'lineUUID',
  '顧客',
  '消費項目',
  '消費門市',
  '手機型號',
  '統一發票',
  '建立時間',
  '包膜師',
  '審核時間',
  '審核結果',
];

export const directorColumn = [{
  id: 1,
  name: '區域',
}, {
  id: 2,
  name: '工號',
}, {
  id: 3,
  name: '姓名',
}, {
  id: 4,
  name: '電話',
}, {
  id: 5,
  name: '一階主管',
}, {
  id: 6,
  name: 'lineAccount',
}];

export const employeeColumn = [{
  id: 1,
  name: '區域',
}, {
  id: 2,
  name: '工號',
}, {
  id: 3,
  name: '姓名',
}, {
  id: 4,
  name: '電話',
}, {
  id: 5,
  name: '主任工號',
}, {
  id: 6,
  name: 'lineAccount',
}];

export const channelColumn = [{
  id: 1,
  name: '通路名稱',
}, {
  id: 2,
  name: '是否可以成為包膜師進貨寄送門市',
}];

export const storeColumn = [{
  id: 1,
  name: '門市資料',
}, {
  id: 2,
  name: '門市電話',
}, {
  id: 3,
  name: '門市縣市',
}, {
  id: 4,
  name: '門市所屬行政區',
}, {
  id: 5,
  name: '門市地址',
}, {
  id: 6,
  name: '區域',
}, {
  id: 7,
  name: '通路',
}, {
  id: 8,
  name: '主要包膜師工號',
}, {
  id: 9,
  name: '次要包膜師工號',
}];

export const salesColumn = [{
  id: 1,
  name: '包膜師工號',
}, {
  id: 2,
  name: '訂單編號',
}, {
  id: 3,
  name: '銷貨開始時間',
}, {
  id: 4,
  name: '銷貨結束時間',
}, {
  id: 5,
  name: '料號',
}, {
  id: 6,
  name: '門市名稱',
}, {
  id: 7,
  name: '價格',
}];

export const stockColumn = [{
  id: 1,
  name: '料號',
}, {
  id: 2,
  name: '包膜師工號',
}, {
  id: 3,
  name: '庫存數量',
}];

export default null;
