import debug from 'debug';
import cityData from './taiwanZones.json';
import { db } from '../db';

const debugCity = debug('Baomore:CityMocks');

export default () => (
  new Promise(async (resolve, reject) => {
    try {
      await Promise.all(cityData.map(item => new Promise(async (resolveCity, rejectCity) => {
        try {
          const city = await db.models.City.create({
            name: item.city,
            latitude: item.lat,
            longitude: item.lng,
          });

          resolveCity(Promise.all(item.zones.map(zone => (
            new Promise(async (resolveZone, rejectZone) => {
              try {
                const dist = await db.models.District.create(zone);
                resolveZone(dist.setCity(city));
              } catch (e) {
                rejectZone(e);
              }
            })
          ))));
        } catch (e) {
          rejectCity(e);
        }
      })));
      resolve();
    } catch (e) {
      debugCity(e);
      reject();
    }
  })
);
