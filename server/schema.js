// @flow
import {
  GraphQLSchema,
  GraphQLObjectType,
} from 'graphql';

// queries
import {
  listVendor,
} from './queries/Vendor/list';
import {
  brandList,
  adminBrandModelColorList,
} from './queries/Brand/list';
import brandModelList from './queries/Brand/modelList';
import {
  brandModelColor,
  brandModelColorList,
} from './queries/Brand/colorList';
import {
  listForSale,
  listFilterByPNSubCategory,
  listFilterByPNCategory,
} from './queries/PNTable/subCategory/list';
import {
  pnSubCategoryItem,
} from './queries/PNTable/subCategory/item';
import {
  listForPNCategory,
  listPNCatgoryFilterByKeyword,
} from './queries/PNTable/category/list';
import {
  pnTableItem,
  customPNTable,
  listFilterBySubCategory,
  listFilterByAdmin,
  listForPNTableCode,
} from './queries/PNTable/list';
import {
  cityDistrictList,
  districtList,
} from './queries/CityDistrict/list';
import {
  list as stores,
} from './queries/Store/list';
import {
  locationInfo,
} from './queries/Store/location';
import {
  availableBookedDates,
  availableBookedTimes,
  alreadyBookedDates,
  bookTimeSlotList,
} from './queries/BookTime/list';
import {
  emoji,
  emojiCategories,
} from './queries/Emoji/list';
import me from './queries/Member/me';
import {
  onlineOrderlist,
  onlineOrder,
} from './queries/OnlineOrder/list';
import {
  employeeMemberlist,
  punchlist,
  allMemberList,
  adminPunchList,
} from './queries/Member/list';
import {
  schedulelist,
} from './queries/OnlineOrder/schedule';
import {
  listClosedOrderSetting,
  itemClosedOrderSetting,
} from './queries/ClosedOrderSetting/list';
import {
  listArea,
} from './queries/Area/list';
import {
  listOrderGroupShipment,
} from './queries/OrderGroupShipment/list';
import checkInfo from './queries/Stock/checkInfo';
import inventories, { stockList } from './queries/Stock/list';
import meInventory from './queries/Stock/single';
import { announcements } from './queries/Announcement/list';
import { announcement } from './queries/Announcement/detail';
import {
  meScheduleDates,
  meScheduleDateAssignments,
} from './queries/Member/schedule';
import { performance } from './queries/Member/performance';
import { banners } from './queries/Banner/list';
import {
  landings,
} from './queries/Landing/list';
import {
  activityCategories,
  activities,
} from './queries/Activity/list';
import {
  activity,
  activityCategory,
} from './queries/Activity/detail';
import myOrderList from './queries/Member/order.js';
import behaviorRecordList, { recordList, behaviorList } from './queries/BehaviorRecord/list';
import { socialCategories } from './queries/SocialCategory/list.js';
import {
  employeePerformanceForAdmin,
  storePerformanceForAdmin,
} from './queries/BehaviorRecord/performance.js';
import {
  listChannel,
} from './queries/Channel/list.js';
import myWarranties from './queries/Member/warranties';
import {
  orderList,
  orderListByEmployee,
  orderQRCodeList,
} from './queries/Order/list.js';
import {
  roleList,
} from './queries/Role/list.js';
import {
  actionList,
} from './queries/Action/list';

// mutations
import createBrand from './mutations/Brand/create';
import createBrandModel from './mutations/Brand/createModel';
import createBrandModelColor from './mutations/Brand/createModelColor';
import editBrand from './mutations/Brand/edit';
import editBrandModel from './mutations/Brand/editModel';
import editBrandModelColor from './mutations/Brand/editModelColor';
import deleteBrand from './mutations/Brand/delete';
import deleteBrandModel from './mutations/Brand/deleteModel';
import deleteBrandModelColor from './mutations/Brand/deleteModelColor';
import payConfirm from './mutations/Pay/confirm';
import storeJSON from './mutations/Member/storeJSON';
import makeOnlineOrder from './mutations/Pay/makeOnlineOrder';
import deletePNTableItem from './mutations/PNTable/delete';
import createPNTableItem from './mutations/PNTable/create';
import editPNTableItem from './mutations/PNTable/edit';
import createPNCategory from './mutations/PNCategory/create';
import createPNSubCategory from './mutations/PNSubCategory/create';
import editPNCategory from './mutations/PNCategory/edit';
import editPNSubCategory from './mutations/PNSubCategory/edit';
import deletePNCategory from './mutations/PNCategory/delete';
import deletePNSubCategory from './mutations/PNSubCategory/delete';
import createClosedOrderSetting from './mutations/ClosedOrderSetting/create';
import editClosedOrderSetting from './mutations/ClosedOrderSetting/edit';
import deleteClosedOrderSetting from './mutations/ClosedOrderSetting/delete';
import updateGroupOrderStatus from './mutations/OrderGroupShipment/update';
import employeeLogin from './mutations/Member/lineBotLogin';
import checkInventory from './mutations/Stock/check';
import {
  saleStart,
  saleStop,
} from './mutations/Stock/sale';
import purchase from './mutations/Stock/purchase';
import transfer from './mutations/Stock/transfer';
import inventoryRollback, { rollBackForCustomer } from './mutations/Stock/rollback';
import punch from './mutations/Member/punch';
import createLanding from './mutations/Landing/create';
import editLanding from './mutations/Landing/edit';
import deleteLanding from './mutations/Landing/delete';
import createBanner from './mutations/Banner/create';
import editBanner from './mutations/Banner/edit';
import deleteBanner from './mutations/Banner/delete';
import createActivityCategory from './mutations/Activity/createCategory';
import editActivityCategory from './mutations/Activity/editCategory';
import deleteActivityCategory from './mutations/Activity/deleteCategory';
import createActivity from './mutations/Activity/create';
import editActivity, { toggleActivity } from './mutations/Activity/edit';
import deleteActivity from './mutations/Activity/delete';
import editSocialCategory from './mutations/SocialCategory/edit';
import createMember from './mutations/Member/create';
import deleteMember from './mutations/Member/delete';
import {
  editMemberForAdmin,
  editMe,
  editMemberForConsumer,
  toggleMemberArchive,
} from './mutations/Member/edit.js';
import createStore from './mutations/Store/create.js';
import editStore, { toggleStoreArchive } from './mutations/Store/edit.js';
import deleteStore from './mutations/Store/delete.js';
import createVendor from './mutations/Vendor/create.js';
import editVendor from './mutations/Vendor/edit.js';
import deleteVendor from './mutations/Vendor/delete.js';
import managerPunch from './mutations/Member/managerPunch';
import changeSchedule from './mutations/Member/changeSchedule';
import cancelSchedule from './mutations/Member/cancelSchedule';
import verifyBehaviorRecords from './mutations/BehaviorRecord/verify.js';
import createAnnouncement from './mutations/Announcement/create';
import editAnnouncement from './mutations/Announcement/edit';
import deleteAnnouncement from './mutations/Announcement/delete';
import addWarrantyRecord from './mutations/Member/addWarrantyRecord';
import {
  deleteOrderByLineBot,
  deleteOrderByAdmin,
} from './mutations/Order/delete';
import {
  editOrderByAdmin,
  editOrderByLineBot,
  assignOrderToEmployee,
} from './mutations/Order/edit';
import {
  createOrder,
} from './mutations/Order/create';
import editRole from './mutations/Role/edit.js';

// The GraphQL schema
export default new GraphQLSchema({
  description: 'root schema',
  query: new GraphQLObjectType({
    name: 'Query',
    fields: {
      brandList,
      brandModelList,
      brandModelColorList,
      pnCategories: listForPNCategory,
      pnCategoriesByKeyword: listPNCatgoryFilterByKeyword,
      subCategoriesByPNCategory: listFilterByPNCategory,
      subCategoriesForSale: listForSale,
      subCategoriesBySubCategory: listFilterByPNSubCategory,
      pntablelistBySubcategory: listFilterBySubCategory,
      pntableList: listFilterByAdmin,
      vendorList: listVendor,
      cityDistricts: cityDistrictList,
      districts: districtList,
      availableBookedDates,
      availableBookedTimes,
      alreadyBookedDates,
      emojiCategories,
      emoji,
      me,
      adminBrandModelColorList,
      brandModelColor,
      onlineOrderlist,
      employeeMemberlist,
      stores,
      bookTimeSlotList,
      schedulelist,
      pnTableItem,
      pnTableCodeList: listForPNTableCode,
      pnSubCategoryItem,
      closedOrderSettings: listClosedOrderSetting,
      singleClosedOrderSetting: itemClosedOrderSetting,
      areaList: listArea,
      orderGroupShipmentList: listOrderGroupShipment,
      checkInfo,
      inventories,
      meInventory,
      punchlist,
      announcements,
      announcement,
      meScheduleDates,
      meScheduleDateAssignments,
      performance,
      banners,
      landings,
      activityCategories,
      activityCategory,
      activities,
      activity,
      myOrderList,
      socialCategories,
      employeePerformanceForAdmin,
      storePerformanceForAdmin,
      onlineOrder,
      behaviorRecordList,
      myWarranties,
      channelList: listChannel,
      orderList,
      orderListByEmployee,
      stockList,
      recordList,
      behaviorList,
      roleList,
      actionList,
      allMemberList,
      orderQRCodeList,
      customPNTable,
      locationInfo,
      adminPunchList,
    },
  }),
  mutation: new GraphQLObjectType({
    name: 'Mutation',
    fields: {
      createBrand,
      createBrandModel,
      createBrandModelColor,
      editBrand,
      editBrandModel,
      editBrandModelColor,
      deleteBrand,
      deleteBrandModel,
      deleteBrandModelColor,
      payConfirm,
      storeJSON,
      makeOnlineOrder,
      deletePNTableItem,
      createPNTableItem,
      editPNTableItem,
      createPNCategory,
      createPNSubCategory,
      editPNCategory,
      editPNSubCategory,
      deletePNCategory,
      deletePNSubCategory,
      createClosedOrderSetting,
      editClosedOrderSetting,
      deleteClosedOrderSetting,
      updateGroupOrderStatus,
      employeeLogin,
      checkInventory,
      saleStart,
      saleStop,
      purchase,
      transfer,
      inventoryRollback,
      punch,
      editMe,
      createLanding,
      editLanding,
      deleteLanding,
      createBanner,
      editBanner,
      deleteBanner,
      createActivityCategory,
      editActivityCategory,
      deleteActivityCategory,
      createActivity,
      editActivity,
      deleteActivity,
      editSocialCategory,
      createMember,
      deleteMember,
      editMemberForAdmin,
      createStore,
      editStore,
      deleteStore,
      createVendor,
      editVendor,
      deleteVendor,
      managerPunch,
      changeSchedule,
      cancelSchedule,
      verifyBehaviorRecords,
      createAnnouncement,
      editAnnouncement,
      deleteAnnouncement,
      addWarrantyRecord,
      editMemberForConsumer,
      deleteOrderByLineBot,
      editOrderByAdmin,
      editOrderByLineBot,
      deleteOrderByAdmin,
      createOrder,
      editRole,
      assignOrderToEmployee,
      toggleActivity,
      rollBackForCustomer,
      toggleMemberArchive,
      toggleStoreArchive,
    },
  }),
});
