import KoaRouter from 'koa-router';
import { Op } from 'sequelize';
import fs from 'fs';
import path from 'path';
import multer from 'koa-multer';
import googleMap from '@google/maps';
import sumBy from 'lodash/sumBy';
import difference from 'lodash/difference';

import padSN from '../helpers/padSN';
import { isPermissionAllowed } from '../helpers/roles.js';
import getCsvJsonArray from '../helpers/getCsvJsonArray.js';
import {
  directorColumn,
  employeeColumn,
  channelColumn,
  storeColumn,
  warrantyCardColumn,
  salesColumn,
  stockColumn,
} from '../columnKey.js';
import { db } from '../db';
import {
  CsvImportError,
} from '../errors/Csv.js';
import { GOOGLE_MAP_API_KEY } from '../../shared/env';
import {
  PERMISSION_ERROR,
} from '../errors/Export.js';

const importRouter = new KoaRouter();

const storage = multer.diskStorage({
  destination: path.join(__dirname, '../uploads'),
  filename: (req, file, cb) => {
    if (file.mimetype === 'image/jpeg'
        || file.mimetype === 'image/jpg') {
      return cb(null, `${Date.now()}.jpg`);
    }
    if (file.mimetype === 'image/png') {
      return cb(null, `${Date.now()}.png`);
    }
    return cb(null, `${Date.now()}${path.extname(file.originalname)}`);
  },
});

const upload = multer({ storage });

const googleMapClient = googleMap.createClient({
  key: GOOGLE_MAP_API_KEY,
  Promise,
});

// importRouter.post('/stockHelper', upload.array('stocks'), async (ctx) => {
//   const stocksData = ctx.req.files;
//
//   const {
//     serialNumber,
//     pnCode,
//   } = ctx.request.query;
//
//   try {
//     const convertedJsonArray = await getCsvJsonArray(stocksData[0].path);
//     const target = convertedJsonArray.find(x => x['工號'] === serialNumber && x['料號'] === pnCode);
//     const convertedJsonArray2 = await getCsvJsonArray(stocksData[1].path);
//     const target2 = convertedJsonArray2.find(x => x['工號'] === serialNumber && x['料號'] === pnCode);
//
//     const initialQuantity = (parseInt((target && target['數量']), 10) || 0) + (parseInt((target2 && target2['數量']), 10) || 0);
//
//     const member = await db.models.Member.findOne({
//       where: {
//         serialNumber,
//       },
//     });
//
//     const pnTable = await db.models.PNTable.findOne({
//       where: {
//         code: pnCode,
//       },
//     });
//
//     const transferResult = await db.models.BehaviorRecord.findAll({
//       attributes: [
//         'id',
//         'remark',
//       ],
//       where: {
//         MemberId: member.id,
//         PNTableId: pnTable.id,
//         BehaviorId: 4, // 調貨
//         remark: {
//           [Op.notLike]: '%調貨要求',
//         },
//       },
//     });
//
//     const saleRecords = await db.models.BehaviorRecord.findAll({
//       attributes: ['id', 'quantity'],
//       where: {
//         MemberId: member.id,
//         PNTableId: pnTable.id,
//         BehaviorId: 1,
//       },
//     });
//     const salesCount = sumBy(saleRecords, r => r.quantity);
//
//     const checkRecords = await db.models.BehaviorRecord.findAll({
//       attributes: ['id', 'quantity'],
//       where: {
//         MemberId: member.id,
//         PNTableId: pnTable.id,
//         BehaviorId: 2,
//       },
//     });
//     const checkCount = sumBy(checkRecords, r => r.quantity);
//
//     const rollbackRecords = await db.models.BehaviorRecord.findAll({
//       attributes: ['id', 'quantity', 'ApproverId'],
//       where: {
//         MemberId: member.id,
//         PNTableId: pnTable.id,
//         BehaviorId: 5,
//         rollbackType: {
//           [Op.ne]: 'CUSTOMERCOMPLAINT',
//         },
//       },
//     });
//     const rollbackCount = sumBy(rollbackRecords, r => r.quantity);
//
//     const currentStock = await db.models.MemberStock.findOne({
//       where: {
//         MemberId: member.id,
//         PNTableId: pnTable.id,
//       },
//     });
//
//     const transferResultIds = transferResult.map(t => t.id);
//
//     const transferSuccessTo = await db.models.BehaviorRecord.findAll({
//       attributes: [
//         'id',
//         'remark',
//         'quantity',
//         'TransferRecordId',
//       ],
//       where: {
//         TransferRecordId: transferResultIds,
//       },
//     });
//
//     const transferSuccessToCount = sumBy(transferSuccessTo, t => t.quantity);
//
//     const transferFailedRecords = await db.models.BehaviorRecord.findAll({
//       attributes: [
//         'id',
//         'qrcode',
//       ],
//       where: {
//         id: difference(transferResultIds, transferSuccessTo.map(t => t.TransferRecordId)),
//       },
//     });
//
//
//     ctx.body = {
//       initialQuantity,
//       salesCount,
//       checkCount,
//       transferSuccessToCount,
//       rollbackCount,
//       expectedQuantity: initialQuantity - salesCount + checkCount - transferSuccessToCount - rollbackCount,
//       currentStock,
//       rollbackRecords,
//       transferFailedRecords,
//     };
//   } catch (e) {
//     ctx.body = {
//       message: `${e}`,
//     };
//   } finally {
//     stocksData.forEach((data) => {
//       fs.unlinkSync(data.path);
//     });
//   }
// });

importRouter.post('/import/director', upload.single('file'), async (ctx) => {
  const {
    path: pathname,
  } = ctx.req.file;

  const {
    token,
  } = ctx.request.query;

  if (!ctx.req.member || !isPermissionAllowed(['ADMIN_MEMBER_MANAGE'], token)) {
    PERMISSION_ERROR(ctx);
    fs.unlinkSync(pathname);
    return;
  }

  const convertedJsonArray = await getCsvJsonArray(pathname);

  if (!convertedJsonArray) {
    fs.unlinkSync(pathname);
    ctx.status = 400;
    throw new CsvImportError();
  }

  const createDataPromise = () => new Promise(async (resolve, reject) => {
    const data = await Promise.all(convertedJsonArray.map(async (x) => {
      const serialNumber = x[directorColumn[1].name];
      const name = x[directorColumn[2].name];
      const phone = x[directorColumn[3].name];
      const managerName = x[directorColumn[4].name];
      const lineAccount = x[directorColumn[5].name];

      if (!serialNumber || !name || !managerName) {
        reject();
      }

      const targetArea = await db.models.Area.findOne({
        attributes: ['id'],
        where: {
          name: {
            [Op.like]: `%${x[directorColumn[0].name]}%`,
          },
        },
      });

      if (!targetArea) return null;

      return {
        AreaId: targetArea && targetArea.id,
        serialNumber,
        name,
        phone,
        managerName,
        lineAccount: lineAccount || null,
      };
    }));

    resolve(data);
  });

  try {
    const data = await createDataPromise();
    const createBundle = data.filter(x => x !== null);

    if (!createBundle.length) {
      ctx.status = 400;
      ctx.body = {
        message: '發生錯誤',
      };
      return;
    }

    const role = await db.models.Role.findOne({
      where: {
        name: '主任包膜師',
      },
    });

    const memberCreateGroup = await db.models.Member.bulkCreate(createBundle);

    await role.addMembers(memberCreateGroup);

    ctx.status = 204;
  } catch (e) {
    ctx.status = 400;
    ctx.body = {
      message: '發生錯誤',
    };
  } finally {
    fs.unlinkSync(pathname);
  }
});

importRouter.post('/import/employee', upload.single('file'), async (ctx) => {
  const {
    path: pathname,
  } = ctx.req.file;

  const {
    token,
  } = ctx.request.query;

  if (!ctx.req.member || !isPermissionAllowed(['ADMIN_MEMBER_MANAGE'], token)) {
    PERMISSION_ERROR(ctx);
    fs.unlinkSync(pathname);
    return;
  }

  const convertedJsonArray = await getCsvJsonArray(pathname);

  if (!convertedJsonArray) {
    ctx.status = 400;
    fs.unlinkSync(pathname);
    throw new CsvImportError();
  }

  const createDataPromise = () => new Promise(async (resolve, reject) => {
    const data = await Promise.all(convertedJsonArray.map(async (x) => {
      const serialNumber = x[employeeColumn[1].name];
      const name = x[employeeColumn[2].name];
      const phone = x[employeeColumn[3].name];
      const directorSN = x[employeeColumn[4].name];
      const lineAccount = x[employeeColumn[5].name];

      if (!serialNumber || !name || !phone || !directorSN) {
        reject();
      }

      const memberDirector = await db.models.Member.findOne({
        attributes: ['id'],
        where: {
          serialNumber: directorSN,
        },
      });

      const targetArea = await db.models.Area.findOne({
        attributes: ['id'],
        where: {
          name: {
            [Op.like]: `%${x[employeeColumn[0].name]}%`,
          },
        },
      });

      if (!memberDirector || !targetArea) return null;

      return {
        AreaId: targetArea.id,
        serialNumber,
        name,
        phone,
        lineAccount: lineAccount || null,
        MemberId: memberDirector.id,
      };
    }));

    resolve(data);
  });

  try {
    const data = await createDataPromise();
    const createBundle = data.filter(x => x !== null);

    const memberCreateGroup = await db.models.Member.bulkCreate(createBundle);

    if (!memberCreateGroup) {
      ctx.status = 400;
      ctx.body = {
        message: '發生錯誤！',
      };
      return;
    }

    const role = await db.models.Role.findOne({
      where: {
        name: '包膜師',
      },
    });

    await role.addMembers(memberCreateGroup);

    ctx.status = 204;
  } catch (e) {
    ctx.status = 400;
    ctx.body = {
      message: '發生錯誤！',
    };
  } finally {
    fs.unlinkSync(pathname);
  }
});

importRouter.post('/import/channel', upload.single('file'), async (ctx) => {
  const {
    path: pathname,
  } = ctx.req.file;

  const {
    token,
  } = ctx.request.query;

  if (!ctx.req.member || !isPermissionAllowed(['ADMIN_MEMBER_MANAGE'], token)) {
    PERMISSION_ERROR(ctx);
    fs.unlinkSync(pathname);
    return;
  }

  const convertedJsonArray = await getCsvJsonArray(pathname);

  if (!convertedJsonArray) {
    ctx.status = 400;
    fs.unlinkSync(pathname);
    throw new CsvImportError();
  }

  const createDataPromise = () => new Promise(async (resolve, reject) => {
    const data = convertedJsonArray.map((x) => {
      const name = x[channelColumn[0].name];
      const isPurchaseChannel = x[channelColumn[1].name] === 'Y' || false;

      if (!name) {
        reject();
      }

      return {
        name,
        isPurchaseChannel,
      };
    });

    resolve(data);
  });

  try {
    const createBundle = await createDataPromise();

    if (!createBundle.length) {
      ctx.status = 400;
      ctx.body = {
        message: '發生錯誤',
      };
      return;
    }

    await db.models.Channel.bulkCreate(createBundle);
    ctx.status = 204;
  } catch (e) {
    ctx.status = 400;
    ctx.body = {
      message: '發生錯誤',
    };
  } finally {
    fs.unlinkSync(pathname);
  }
});

importRouter.post('/import/store', upload.single('file'), async (ctx) => {
  const {
    path: pathname,
  } = ctx.req.file;

  const {
    token,
  } = ctx.request.query;

  if (!ctx.req.member || !isPermissionAllowed(['ADMIN_MEMBER_MANAGE'], token)) {
    PERMISSION_ERROR(ctx);
    fs.unlinkSync(pathname);
    return;
  }

  let convertedJsonArray = await getCsvJsonArray(pathname);

  if (!convertedJsonArray) {
    ctx.status = 400;
    fs.unlinkSync(pathname);
    throw new CsvImportError();
  }

  convertedJsonArray = convertedJsonArray.filter(x => x['門市地址']);

  const storeIgnoreBundle = [];
  const storeCreateBundle = [];
  const memberStoreBundle = [];

  try {
    await convertedJsonArray.map(x => () => new Promise(async (resolve) => {
      const name = x[storeColumn[0].name];
      const phone = x[storeColumn[1].name];
      const address = x[storeColumn[4].name];
      const mainEmployeeSN = x[storeColumn[7].name];
      const backupEmployeeSN = x[storeColumn[8].name];
      const city = x[storeColumn[2].name];

      const existStore = await db.models.Store.findOne({
        where: {
          name,
        },
      });

      if (existStore) {
        storeIgnoreBundle.push({
          name,
        });
        return resolve();
      }

      const targetArea = await db.models.Area.findOne({
        attributes: ['id'],
        where: {
          name: {
            [Op.like]: `%${x[storeColumn[5].name]}%`,
          },
        },
      });

      const targetChannel = await db.models.Channel.findOne({
        attributes: ['id'],
        where: {
          name: {
            [Op.like]: `%${x[storeColumn[6].name]}%`,
          },
        },
      });

      const targetCity = await db.models.City.findOne({
        attributes: ['id'],
        where: {
          name: {
            [Op.like]: `%${city}%`,
          },
        },
      });

      const targetDistrict = await db.models.District.findOne({
        attributes: ['id'],
        where: {
          name: {
            [Op.like]: `%${x[storeColumn[3].name]}%`,
          },
          CityId: (targetCity && targetCity.id) || null,
        },
      });

      let mainEmployee = null;

      if (mainEmployeeSN) {
        mainEmployee = await db.models.Member.findOne({
          attributes: ['id'],
          where: {
            serialNumber: mainEmployeeSN,
          },
        });
      }

      let backupEmployee = null;

      if (backupEmployeeSN) {
        backupEmployee = await db.models.Member.findOne({
          attributes: ['id'],
          where: {
            serialNumber: backupEmployeeSN,
          },
        });
      }

      const googleResponse = await googleMapClient.geocode({ address }).asPromise();

      let locationResult;

      if (
        googleResponse
        && googleResponse.json
        && googleResponse.json.results
        && googleResponse.json.results[0]
        && googleResponse.json.results[0].geometry
      ) {
        locationResult = googleResponse.json.results[0].geometry.location;
      }

      const createdStore = await db.models.Store.create({
        name,
        phone,
        address,
        latitude: locationResult && locationResult.lat,
        longitude: locationResult && locationResult.lng,
        AreaId: targetArea && targetArea.id,
        ChannelId: targetChannel && targetChannel.id,
        DistrictId: targetDistrict && targetDistrict.id,
      });

      if (mainEmployee) {
        memberStoreBundle.push({
          type: 'MAIN',
          MemberId: mainEmployee.id,
          StoreId: createdStore.id,
        });
      }

      if (backupEmployee && mainEmployeeSN !== backupEmployeeSN) {
        memberStoreBundle.push({
          type: 'BACKUP',
          MemberId: backupEmployee.id,
          StoreId: createdStore.id,
        });
      }

      storeCreateBundle.push({
        name,
      });

      return resolve();
    })).reduce((prev, next) => prev.then(next), Promise.resolve());

    await db.models.MemberStore.bulkCreate(memberStoreBundle);

    ctx.status = 200;
    ctx.body = {
      storeCreateBundle,
      storeIgnoreBundle,
    };
  } catch (e) {
    ctx.status = 400;
    ctx.body = {
      message: `${e}`,
    };
  } finally {
    fs.unlinkSync(pathname);
  }
});

importRouter.post('/import/schedule', upload.single('file'), async (ctx) => {
  const {
    path: pathname,
  } = ctx.req.file;

  const {
    token,
  } = ctx.request.query;

  if (!ctx.req.member || !isPermissionAllowed(['ADMIN_MEMBER_MANAGE'], token)) {
    PERMISSION_ERROR(ctx);
    fs.unlinkSync(pathname);
    return;
  }

  const convertedJsonArray = await getCsvJsonArray(pathname);

  if (!convertedJsonArray) {
    ctx.status = 400;
    fs.unlinkSync(pathname);
    throw new CsvImportError();
  }

  const dates = Object.keys(convertedJsonArray[0]).filter(key => key !== '員工編號' && key !== '姓  名');

  if (!dates.length) throw new CsvImportError();

  const createData = [];
  const deleteDataId = [];

  function getCreateDeleteContent(targetMember, item) {
    return new Promise(async (resolve) => {
      if (targetMember) {
        await Promise.all(dates.map(async (date) => {
          const existMemberSchedule = await db.models.MemberSchedule.findOne({
            attributes: ['id'],
            where: {
              MemberId: targetMember.id,
              date,
            },
          });

          if (existMemberSchedule) {
            deleteDataId.push(existMemberSchedule.id);
          }

          const status = item[date];

          createData.push({
            date,
            MemberId: targetMember.id,
            status: (status === '4' || status === '004') ? '4' : status,
          });
        }));
      }
      resolve();
    });
  }

  try {
    await convertedJsonArray
      .map(x => () => new Promise(async (resolve) => {
        const serialNumber = x['員工編號'];
        const targetMember = await db.models.Member.findOne({
          attributes: ['id'],
          where: {
            serialNumber,
          },
        });

        if (!targetMember) return resolve(x);

        await getCreateDeleteContent(targetMember, x);

        return resolve(x);
      })).reduce((prev, next) => prev.then(next), Promise.resolve());
  } catch (e) {
    ctx.status = 400;
    fs.unlinkSync(pathname);
    throw new CsvImportError();
  }

  if (!createData.length) {
    ctx.status = 200;
    fs.unlinkSync(pathname);
    return;
  }

  const transaction = await db.transaction();

  try {
    if (deleteDataId.length) {
      await db.models.MemberSchedule.destroy({
        force: true,
        where: {
          id: {
            [Op.in]: deleteDataId,
          },
        },
      }, transaction);
    }

    await db.models.MemberSchedule.bulkCreate(createData, { transaction });

    fs.unlinkSync(pathname);

    await transaction.commit();

    ctx.status = 200;
  } catch (e) {
    ctx.status = 400;

    fs.unlinkSync(pathname);

    await transaction.rollback();

    throw new CsvImportError();
  }
});

importRouter.post('/import/warranty', upload.single('file'), async (ctx) => {
  const {
    path: pathname,
  } = ctx.req.file;

  const {
    token,
  } = ctx.request.query;

  if (!ctx.req.member || !isPermissionAllowed(['ADMIN_MEMBER_MANAGE'], token)) {
    PERMISSION_ERROR(ctx);
    fs.unlinkSync(pathname);
    return;
  }

  const convertedJsonArray = await getCsvJsonArray(pathname);

  if (!convertedJsonArray) {
    ctx.status = 400;
    fs.unlinkSync(pathname);
    throw new CsvImportError();
  }

  const transaction = await db.transaction();

  const rawBundle = [];

  try {
    await convertedJsonArray.map(x => () => new Promise(async (resolve) => {
      // Member data
      const lineId = x[warrantyCardColumn[0]];
      const name = x[warrantyCardColumn[1]];
      const employeeSN = padSN(x[warrantyCardColumn[7]]);
      // warranty data
      const service = x[warrantyCardColumn[2]];
      const store = x[warrantyCardColumn[3]];
      const phoneModel = x[warrantyCardColumn[4]];
      const VAT = x[warrantyCardColumn[5]];
      const createdAt = x[warrantyCardColumn[6]];
      const verifiedTime = x[warrantyCardColumn[8]];
      const isApproved = x[warrantyCardColumn[9]] === '認證';

      if (!lineId
        || !name
        || !employeeSN
        || !service
        || !store
        || !phoneModel
        || !VAT
        || !createdAt
        || !verifiedTime) {
        rawBundle.push({
          status: false,
          lineId,
          name,
        });
        return resolve();
      }

      const employee = await db.models.Member.findOne({
        where: {
          serialNumber: employeeSN,
        },
      });

      const consumer = await db.models.Member.findOrCreate({
        where: {
          lineId,
          name,
        },
        transaction,
      });

      if (consumer[1]) {
        const role = await db.models.Role.findOne({
          where: {
            name: '一般登入用戶',
          },
        });
        await role.addMember(consumer[0].id, { transaction });
      }

      rawBundle.push({
        status: true,
        service,
        phoneModel,
        store,
        VAT,
        createdAt,
        verifiedTime: verifiedTime === '-' ? null : verifiedTime,
        isApproved,
        IME: null,
        phone: null,
        MemberId: consumer[0].id,
        approverSN: employeeSN,
        ApproverId: (employee && employee.id) || null,
      });
      return resolve();
    })).reduce((prev, next) => prev.then(next), Promise.resolve());

    const ignoreBundle = rawBundle.filter(x => !x.status);

    if (ignoreBundle.length) {
      await transaction.rollback();
      ctx.status = 400;
      ctx.body = {
        ignoreBundle,
      };
      return;
    }

    const createBundle = rawBundle.filter(x => x.status);

    await db.models.MemberWarranty.bulkCreate(createBundle, { transaction });

    await transaction.commit();

    ctx.status = 200;
    ctx.body = {
      createBundle,
    };
  } catch (e) {
    await transaction.rollback();

    ctx.status = 400;
    ctx.body = {
      message: `${e}`,
    };
  } finally {
    fs.unlinkSync(pathname);
  }
});

importRouter.post('/import/salesData', upload.single('file'), async (ctx) => {
  const {
    path: pathname,
  } = ctx.req.file;

  const {
    token,
  } = ctx.request.query;

  if (!ctx.req.member || !isPermissionAllowed(['ADMIN_MEMBER_MANAGE'], token)) {
    PERMISSION_ERROR(ctx);
    fs.unlinkSync(pathname);
    return;
  }

  const convertedJsonArray = await getCsvJsonArray(pathname);

  if (!convertedJsonArray) {
    ctx.status = 400;
    fs.unlinkSync(pathname);
    throw new CsvImportError();
  }

  const behavior = await db.models.Behavior.findOne({
    where: {
      name: '銷貨',
    },
  });

  const generateCreateData = () => new Promise(async (resolve, reject) => {
    const data = await Promise.all(convertedJsonArray.map(async (x) => {
      const serialNumber = x[salesColumn[0].name];
      const orderSN = x[salesColumn[1].name];
      const startTime = x[salesColumn[2].name];
      const endTime = x[salesColumn[3].name];
      const pnTableCode = x[salesColumn[4].name];
      const storeName = x[salesColumn[5].name];
      const price = x[salesColumn[6].name];

      if (
        !serialNumber
        || !orderSN
        || !startTime
        || !endTime
        || !pnTableCode
        || !storeName
        || !price
      ) {
        reject();
      }

      const targetMember = await db.models.Member.findOne({
        where: {
          serialNumber,
        },
      });
      const targetPNTable = await db.models.PNTable.findOne({
        where: {
          code: pnTableCode,
        },
      });
      const targetStore = await db.models.Store.findOne({
        where: {
          name: {
            [Op.like]: `%${storeName}%`,
          },
        },
      });

      if (!targetMember || !targetPNTable || !targetStore || !behavior) {
        return {
          isNull: true,
          serialNumber,
          pnTableCode,
          storeName,
        };
      }

      return {
        startTime,
        endTime,
        MemberId: targetMember.id,
        orderSN,
        PNTableId: targetPNTable.id,
        StoreId: targetStore.id,
        price,
        BehaviorId: behavior.id,
      };
    }));

    resolve(data);
  });

  try {
    const data = await generateCreateData();

    const nullData = data.filter(x => x.isNull === true);
    const createBundle = data.filter(x => !x.isNull);

    await db.models.BehaviorRecord.bulkCreate(createBundle);

    ctx.status = 200;
    ctx.body = {
      nullData,
      createBundle,
    };
  } catch (e) {
    ctx.status = 400;
    ctx.body = {
      message: '發生錯誤',
    };
  } finally {
    fs.unlinkSync(pathname);
  }
});

importRouter.post('/import/stockData', upload.single('file'), async (ctx) => {
  const {
    path: pathname,
  } = ctx.req.file;

  const {
    token,
  } = ctx.request.query;

  if (!ctx.req.member || !isPermissionAllowed(['ADMIN_MEMBER_MANAGE'], token)) {
    PERMISSION_ERROR(ctx);
    fs.unlinkSync(pathname);
    return;
  }

  const convertedJsonArray = await getCsvJsonArray(pathname);

  if (!convertedJsonArray) {
    ctx.status = 400;
    fs.unlinkSync(pathname);
    throw new CsvImportError();
  }

  const generateCreateData = () => new Promise(async (resolve, reject) => {
    const data = await Promise.all(convertedJsonArray.map(async (x) => {
      const pnTableCode = x[stockColumn[0].name];
      const serialNumber = x[stockColumn[1].name];
      const storageCount = x[stockColumn[2].name];

      if (
        !serialNumber
        || !storageCount
        || !pnTableCode
      ) {
        reject();
      }

      const targetMember = await db.models.Member.findOne({
        where: {
          serialNumber,
        },
      });
      const targetPNTable = await db.models.PNTable.findOne({
        where: {
          code: pnTableCode,
        },
      });

      if (!targetMember || !targetPNTable) {
        return {
          isNull: true,
          pnTableCode,
          serialNumber,
        };
      }

      return {
        MemberId: targetMember.id,
        PNTableId: targetPNTable.id,
        storageCount,
      };
    }));

    resolve(data);
  });

  try {
    const data = await generateCreateData();

    const nullData = data.filter(x => x.isNull === true);
    const createBundle = data.filter(x => !x.isNull);

    await db.models.MemberStock.bulkCreate(createBundle);

    ctx.status = 200;
    ctx.body = {
      nullData,
    };
  } catch (e) {
    ctx.status = 400;
    ctx.body = {
      message: '發生錯誤',
    };
  } finally {
    fs.unlinkSync(pathname);
  }
});

export default importRouter;
