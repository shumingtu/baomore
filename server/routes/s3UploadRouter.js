import fs from 'fs';
import path from 'path';
import multer from 'koa-multer';
import AWS from 'aws-sdk';
import KoaRouter from 'koa-router';

import { getStaticDownloadURL } from '../helpers/url';

AWS.config.update({
  accessKeyId: process.env.S3ACCESSKEY,
  secretAccessKey: process.env.S3SECRETACCESSKEY,
});

const s3 = new AWS.S3();
const BUCKET_NAME = process.env.S3BUCKETNAME || 'baomore-test';

const s3UploadRouter = new KoaRouter();

const storage = multer.diskStorage({
  destination: path.join(__dirname, '../uploads'),
  filename: (req, file, cb) => {
    if (file.mimetype === 'image/jpeg'
        || file.mimetype === 'image/jpg') {
      return cb(null, `${Date.now()}.jpg`);
    }
    if (file.mimetype === 'image/png') {
      return cb(null, `${Date.now()}.png`);
    }
    return cb(null, `${Date.now()}${path.extname(file.originalname)}`);
  },
});

const upload = multer({ storage });

function s3upload(filepath, filename) {
  return new Promise((resolve, reject) => {
    const stream = fs.createReadStream(filepath);
    const params = {
      Bucket: BUCKET_NAME,
      Key: filename,
      Body: stream,
      ACL: 'public-read',
    };

    s3.upload(params, (err, data) => {
      if (err) return reject(err);

      fs.unlink(filepath, (fsError) => {
        if (fsError) {
          reject(fsError);
        }
      });

      return resolve(data.Location);
    });
  });
}

s3UploadRouter.post('/upload', upload.single('file'), async (ctx) => {
  const {
    filename,
  } = ctx.req.file;

  const s3path = await s3upload(path.resolve(__dirname, `../uploads/${filename}`), filename);
  ctx.body = {
    url: getStaticDownloadURL(s3path),
    filename,
  };
});


export default s3UploadRouter;
