// @flow
import Koa from 'koa';
import path from 'path';
import KoaRouter from 'koa-router';
import { Op } from 'sequelize';
import koaConvert from 'koa-convert';
import GraphQLAuthKeeper from 'graphql-auth-keeper';
import send from 'koa-send';
import { ApolloServer } from 'apollo-server-koa';
import debug from 'debug';
import kcors from 'kcors';
import serve from 'koa-static';
import bodyParser from 'koa-bodyparser';
import cron from 'node-cron';
import moment from 'moment';
import schema from './schema';
import initDB, { db } from './db';
import { JWT_TOKEN } from './helpers/jwt';
import errorMiddleware from './helpers/errorMiddleware';
import {
  memberLogin,
  adminLogin,
  employeeLogin,
  memberLoginFromCustomizePage,
} from './helpers/line/lineRouter';
import s3UploadRouter from './routes/s3UploadRouter.js';
import { reserve } from './helpers/line/linePayRouter';
import authorizationMiddleware from './helpers/authorization';
import linebot, { sendBookedInfoToDirector, sendLineMessage } from './helpers/line/linebot';
import linebotMiddleware from './helpers/line/linebotMiddleware';
import exportRouter from './routes/exportRouter.js';
import importRouter from './routes/importRouter.js';

const PORT = parseInt(process.env.PORT || '2113', 10);
const debugServer = debug('BAOMORE:Server');

const app = new Koa();
const router = new KoaRouter();

// oAuth login
router.get('/auth/lineLogin', memberLogin);
router.get('/auth/adminLineLogin', adminLogin);
router.get('/auth/customize/lineLogin', memberLoginFromCustomizePage);
router.get('/auth/lineBotLogin', employeeLogin);
// LINE Reserve
router.get('/line/pay/reserve', reserve);
// LINE Bot Webhook
router.post('/lineWebHooks', linebotMiddleware(linebot));

app.use(bodyParser());

const authKeeper = new GraphQLAuthKeeper({
  secret: JWT_TOKEN,
  syncFn: payload => db.models.Member.findOne({
    where: {
      id: payload.id,
    },
    include: [{
      model: db.models.Role,
      attributes: [
        'id',
        'name',
      ],
      include: [{
        model: db.models.Action,
        attributes: [
          'code',
        ],
      }],
    }],
  }),
});

app.use(koaConvert(kcors()));

const server = new ApolloServer(authKeeper.apolloServerKoaOptions({
  schema,
  context: {
    db,
  },
  introspection: true,
}));
server.applyMiddleware({ app });

if (process.env.NODE_ENV !== 'production') {
  app.use(kcors());
  app.use(serve(path.resolve(__dirname, 'static')));
  app.use(serve(path.join(__dirname, 'uploads')));
} else {
  app.use(serve(path.resolve(__dirname, 'static')));
  app.use(serve(path.join(__dirname, 'uploads')));
}

// error middleware
app.use(errorMiddleware);

app.use(authorizationMiddleware);

app.use(router.routes());
app.use(router.allowedMethods());

// AWS S3 Uploader
app.use(s3UploadRouter.routes());
app.use(s3UploadRouter.allowedMethods());

// export router
app.use(exportRouter.routes());
app.use(exportRouter.allowedMethods());

// import router
app.use(importRouter.routes());
app.use(importRouter.allowedMethods());

if (process.env.NODE_ENV === 'production') {
  app.use(async (ctx) => {
    await send(ctx, 'server/static/index.html');
  });
} else {
  app.use(async (ctx) => {
    await send(ctx, 'client/static/index.html');
  });
}

// LINE Bot Webhook
app.use(linebotMiddleware(linebot));

cron.schedule('* * * * *', async () => {
  const saleBehavior = await db.models.Behavior.findOne({
    where: {
      name: '銷貨',
    },
  });

  if (!saleBehavior) return;

  const unCompletedSaleRecord = await db.models.BehaviorRecord.findAll({
    where: {
      BehaviorId: saleBehavior.id,
      startTime: {
        [Op.lte]: moment().subtract(1, 'hours').toDate(), // 超過一小時
      },
      endTime: null,
    },
  });

  if (unCompletedSaleRecord.length) {
    const transaction = await db.transaction();

    try {
      await Promise.all(unCompletedSaleRecord.map(async (record) => {
        await record.update({
          endTime: new Date(),
        }, { transaction });

        await db.models.MemberStock.reduceStock(
          record.MemberId,
          record.PNTableId,
          record.quantity,
          transaction,
        );
      }));
      await transaction.commit();
    } catch (e) {
      await transaction.rollback();
    }
  }
});

cron.schedule('* * * * *', async () => {
  const confirmSchedules = await db.models.EmployeeConfirmSchedule.findAll({
    where: {
      isNotified: false,
      sendBookedInfoTime: {
        [Op.lte]: moment().subtract(10, 'minutes').toDate(),
      },
    },
    include: [{
      model: db.models.OnlineOrder.scope('bookedMember', 'member', 'store'),
      where: {
        payedStatus: 'PAIDNOTCONFIRMED',
      },
    }],
  });

  // remove 2019-08-05
  // const transferSchedules = await db.models.EmployeeTransferSchedule.findAll({
  //   where: {
  //     isNotified: false,
  //     sendTransferTime: {
  //       [Op.gte]: moment().subtract(10, 'minutes').toDate(),
  //     },
  //   },
  //   include: [{
  //     model: db.models.BehaviorRecord,
  //     where: {
  //       isTransfering: true,
  //     },
  //     include: [{
  //       model: db.models.Member,
  //     }],
  //   }],
  // });

  // if (transferSchedules.length) {
  //   const transferScheduleIds = transferSchedules.map(t => t.id);
  //
  //   await db.models.EmployeeTransferSchedule.update({
  //     isNotified: true,
  //   }, {
  //     where: {
  //       id: transferScheduleIds,
  //     },
  //   });
  //
  //   transferSchedules.forEach(async (transferRecord) => {
  //     const director = await transferRecord.BehaviorRecord.Member.getMember();
  //
  //     await sendLineMessage(null, `包膜師：${transferRecord.BehaviorRecord.Member.name}超過10分鐘未確認調貨請求！`, director.lineId);
  //   });
  // }

  if (confirmSchedules.length) {
    const confirmScheduleIds = confirmSchedules.map(c => c.id);

    await db.models.EmployeeConfirmSchedule.update({
      isNotified: true,
    }, {
      where: {
        id: confirmScheduleIds,
      },
    });

    confirmSchedules.forEach(async (order) => {
      const oriEmployee = order.OnlineOrder.BookedMember;
      const director = await order.OnlineOrder.BookedMember.getMember();

      await sendBookedInfoToDirector(
        director.lineId,
        `【包膜師尚未確認預約】\n線上訂單編號：${order.OnlineOrder.id}\n訂購人姓名：${order.OnlineOrder.Owner.name}\n訂購人電話：${order.OnlineOrder.Owner.phone}\n預約時間：${order.OnlineOrder.date} ${order.OnlineOrder.time}\n預約門市：${order.OnlineOrder.Store.Channel.name}${order.OnlineOrder.Store.name}\n包膜師：${oriEmployee.name}`,
        order.OnlineOrder.id,
        director.id,
      );
    });
  }
});

cron.schedule('*/5 * * * *', async () => {
  const dayOfWeek = moment().weekday();
  const hourMinute = moment().format('HH:mm');

  const closedOrderSetting = await db.models.ClosedOrderSetting.findAll();

  // 要結單的設定
  const needCloseSetting = closedOrderSetting.filter(
    x => x.closedDay === (dayOfWeek === 0 ? 7 : dayOfWeek)
    && x.closedTime <= hourMinute
    && x.isClosed === false
  );

  // 不結單的設定
  const excludeCloseSetting = closedOrderSetting.filter(
    x => x.closedDay !== (dayOfWeek === 0 ? 7 : dayOfWeek)
    || x.closedTime > hourMinute
    || x.isClosed === true // 已經結過單了
  );

  // 要結單的pnTableIds
  const pnTableIds = needCloseSetting.map(x => x.PNTableId);

  // 不結單的pnTableIds
  const excludePnTableIds = excludeCloseSetting.map(x => x.PNTableId);


  // 結單後isClosed設定為true 避免重複結單, 不等於禮拜三的單isClosed要設定為false (禮拜二原本是true, 禮拜三變成false)
  const needCloseSettingIds = needCloseSetting.map(x => x.id);
  await db.models.ClosedOrderSetting.update({
    isClosed: true,
  }, {
    where: {
      id: needCloseSettingIds,
    },
  });

  await db.models.ClosedOrderSetting.update({
    isClosed: false,
  }, {
    where: {
      closedDay: {
        [Op.ne]: dayOfWeek === 0 ? 7 : dayOfWeek,
      },
    },
  });

  if (pnTableIds.length) {
    const closingOrders = await db.models.Order.scope(
      pnTableIds.includes(null) ? { method: ['excludePnTableIds', excludePnTableIds] }
        : { method: ['InPnTableIds', pnTableIds] },
    ).findAll({
      where: {
        status: 'ORDERING',
        OrderGroupShipmentId: null,
      },
      include: [{
        model: db.models.PNTable,
        required: true,
      }],
    });

    if (closingOrders.length) {
      db.models.OrderGroupShipment.groupOrder(closingOrders);
    }
  }
});

initDB().then(() => {
  app.listen(PORT, () => debugServer(`🚀 Server Listen on Port: ${PORT}`));
});
