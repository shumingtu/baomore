// @flow
import Sequelize, { Model, DataTypes, Op } from 'sequelize';
import moment from 'moment';
import OnlineOrderColumns from '../columns/OnlineOrder.js';
import { db } from '../db';

export default class OnlineOrder extends Model<ModelDefaultAttributes, OnlineOrderAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(OnlineOrder.fields, {
      sequelize,
      defaultScope: OnlineOrder.defaultScope,
      scopes: OnlineOrder.scopes,
    });
  }

  static fields = OnlineOrderColumns(DataTypes);

  static associate() {
    OnlineOrder.belongsTo(db.models.Store);
    OnlineOrder.belongsTo(db.models.Member, { as: 'BookedMember' });
    OnlineOrder.belongsTo(db.models.Member, { as: 'Owner' });
    OnlineOrder.belongsTo(db.models.Order);
    OnlineOrder.belongsTo(db.models.PNTable);
  }

  static defaultScope = {
    order: [
      ['createdAt', 'DESC'],
    ],
  };

  static scopes = {
    search: (args) => {
      const {
        storeId,
        bookedDate,
        bookedEndDate,
        bookedMemberIds,
        date,
        orderId,
        startDate,
        endDate,
        payedStatus,
        bookedMemberId,
        keyword,
        shippingStatus,
      } = args;

      let whereClause = {};

      if (storeId) whereClause.StoreId = storeId;

      if (bookedDate || bookedEndDate) {
        const condition = [];

        if (bookedDate) {
          condition.push({
            [Op.gte]: moment(bookedDate),
          });
        }

        if (bookedEndDate) {
          condition.push({
            [Op.lte]: moment(bookedEndDate),
          });
        }

        whereClause.date = {
          [Op.and]: condition,
        };
      }
      if (bookedMemberIds && bookedMemberIds.length) {
        whereClause.BookedMemberId = {
          [Op.in]: bookedMemberIds,
        };
      }
      if (date) whereClause.date = moment(date);
      if (orderId) whereClause.id = orderId;
      if (startDate) {
        whereClause.createdAt = {
          [Op.and]: [{
            [Op.gte]: moment(startDate),
          }, {
            [Op.lte]: (endDate && moment(endDate)) || moment(),
          }],
        };
      }
      if (payedStatus) whereClause.payedStatus = payedStatus;
      if (shippingStatus) whereClause.shippingStatus = shippingStatus;
      if (bookedMemberId) whereClause.BookedMemberId = bookedMemberId;
      if (keyword) {
        whereClause = {
          ...whereClause,
          [Op.or]: [{
            name: {
              [Op.like]: `%${keyword}%`,
            },
          }, {
            phone: {
              [Op.like]: `%${keyword}%`,
            },
          }, {
            email: {
              [Op.like]: `%${keyword}%`,
            },
          }],
        };
      }

      return {
        where: whereClause,
      };
    },
    store: () => ({
      include: [{
        model: db.models.Store,
        include: [{
          model: db.models.Member,
        }, {
          model: db.models.Channel,
        }],
      }],
    }),
    pages: (limit, offset) => ({
      limit,
      offset,
    }),
    order: {
      order: [
        ['createdAt', 'DESC'],
      ],
    },
    bookedMember: memberId => ({
      include: [{
        model: db.models.Member,
        as: 'BookedMember',
        where: memberId ? {
          id: memberId,
        } : {},
        required: !!memberId,
      }],
    }),
    member: (archiveLimit = false) => {
      const whereClause = {
        lineId: {
          [Op.ne]: null,
        },
      };

      if (archiveLimit) whereClause.archive = false;

      return {
        include: [{
          model: db.models.Member,
          as: 'Owner',
          where: whereClause,
        }],
      };
    },
    payedAndConfirmed: {
      where: {
        payedStatus: 'PAIDANDCONFIRMED',
      },
    },
  }
}
