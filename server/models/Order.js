// @flow
import Sequelize, { Model, DataTypes, Op } from 'sequelize';
import moment from 'moment';
import OrderColumns from '../columns/Order.js';
import { db } from '../db';

export default class Order extends Model<ModelDefaultAttributes, OrderAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(Order.fields, { sequelize, scopes: Order.scopes });
  }

  static fields = OrderColumns(DataTypes);

  static associate() {
    Order.belongsTo(db.models.Store);
    Order.belongsTo(db.models.Member, { foreignKey: 'EmpolyedMemberId' });
    Order.belongsTo(db.models.OrderGroupShipment);
    Order.hasOne(db.models.OnlineOrder);
    Order.belongsTo(db.models.PNTable);
    Order.hasMany(db.models.OrderQRCode);
  }

  static scopes = {
    InPnTableIds: pnTableIds => ({
      where: {
        PNTableId: {
          [Op.in]: pnTableIds,
        },
      },
    }),
    excludePnTableIds: excludePnTableIds => ({
      where: {
        PNTableId: {
          [Op.notIn]: excludePnTableIds,
        },
      },
    }),
    findByOrderGroupShipmentId: id => ({
      where: {
        OrderGroupShipmentId: id,
      },
    }),
    memberArea: args => ({
      include: [{
        model: db.models.Member.scope({ method: ['search', args] }),
        include: [{
          model: db.models.Area,
        }],
      }],
    }),
    search: (args) => {
      const {
        memberId,
        status,
        orderSN,
      } = args;

      const whereClause = {};

      if (memberId) whereClause.EmpolyedMemberId = memberId;
      if (status) whereClause.status = status;
      if (orderSN) whereClause.orderSN = orderSN;

      return {
        where: whereClause,
      };
    },
    store: () => ({
      include: [{
        model: db.models.Store,
        include: [{
          model: db.models.Channel,
        }, {
          model: db.models.District,
        }],
      }],
    }),
    pages: (limit, offset) => ({
      limit,
      offset,
    }),
    orderGroupShipment: (args) => {
      const {
        closeDate,
      } = args;

      const whereClause = {};

      if (closeDate) {
        whereClause.createdAt = {
          [Op.and]: [{
            [Op.gte]: moment(closeDate),
          }, {
            [Op.lt]: moment(closeDate).add(1, 'd'),
          }],
        };
      }

      return {
        include: [{
          model: db.models.OrderGroupShipment,
          where: whereClause,
          required: !!closeDate,
        }],
      };
    },
    qrcodes: (args) => {
      const {
        qrcode,
      } = args;

      const whereClause = {};

      if (qrcode) {
        whereClause.qrcode = qrcode;
      }

      return {
        include: [{
          model: db.models.OrderQRCode,
          where: whereClause,
          required: !!qrcode,
        }],
      };
    },
  };
}
