// @flow
import Sequelize, { Model, DataTypes, Op } from 'sequelize';
import PNCategoryColumns from '../columns/PNCategory.js';
import { db } from '../db';
import {
  PNCategoryNotFoundError,
  PNCategoryCreateError,
} from '../errors/PNCategory';

export default class PNCategory extends Model<ModelDefaultAttributes, PNCategoryAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(PNCategory.fields, { sequelize, scopes: PNCategory.scopes });
  }

  static fields = PNCategoryColumns(DataTypes);

  static associate() {}

  static async deletePNCategory({ id }) {
    const pnCategory = await db.models.PNCategory.findOne({
      where: {
        id,
      },
    });

    if (!pnCategory) {
      throw new PNCategoryNotFoundError();
    }

    await pnCategory.destroy();

    return {
      id: pnCategory.id,
      status: true,
      message: '成功刪除料號',
    };
  }

  static async createPNCategory({ name }) {
    const pnCategory = await db.models.PNCategory.create({ name });

    if (!pnCategory) {
      throw new PNCategoryCreateError();
    }

    return pnCategory;
  }

  static async editPNCategory({
    id,
    name,
  }) {
    const pnCategory = await db.models.PNCategory.findOne({
      where: {
        id,
      },
    });

    if (!pnCategory) {
      throw new PNCategoryNotFoundError();
    }

    pnCategory.name = name;

    await pnCategory.save();

    return pnCategory;
  }

  static scopes = {
    search: (args) => {
      const {
        id,
        keyword,
      } = args;

      let whereClause = {};

      if (id) whereClause.id = id;
      if (keyword) {
        whereClause = {
          ...whereClause,
          [Op.or]: [{
            name: {
              [Op.like]: `%${keyword}%`,
            },
          }],
        };
      }

      return {
        where: whereClause,
      };
    },
    filterByKeyword: (keyword) => {
      let whereClause = {};

      if (keyword) {
        whereClause = {
          ...whereClause,
          [Op.or]: [{
            name: {
              [Op.like]: `%${keyword}%`,
            },
          }],
        };
      }

      return {
        where: whereClause,
      };
    },
  };
}
