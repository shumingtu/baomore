// @flow
import moment from 'moment';
import Sequelize, { Model, DataTypes, Op } from 'sequelize';
import MemberPunchRecordColumns from '../columns/MemberPunchRecord.js';
import { db } from '../db';

export default class MemberPunchRecord extends Model<ModelDefaultAttributes, MemberPunchRecordAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(MemberPunchRecord.fields, { sequelize, scopes: MemberPunchRecord.scopes });
  }

  static fields = MemberPunchRecordColumns(DataTypes);

  static associate() {
    MemberPunchRecord.belongsTo(db.models.Member);
    MemberPunchRecord.belongsTo(db.models.Store);
  }

  static scopes = {
    search: (args) => {
      const {
        memberId,
        storeId,
        startDate,
        endDate,
      } = args;

      const whereClause = {};

      if (memberId) whereClause.MemberId = memberId;
      if (storeId) whereClause.StoreId = storeId;
      if (startDate) {
        whereClause.punchTime = {
          [Op.and]: [{
            [Op.gte]: moment(startDate),
          }, {
            [Op.lte]: (endDate && moment(endDate).endOf('day')) || moment(),
          }],
        };
      }

      return {
        where: whereClause,
      };
    },
    store: () => ({
      include: [{
        model: db.models.Store,
      }],
    }),
    pages: (limit, offset) => ({
      limit,
      offset,
    }),
    member: () => ({
      include: [{
        model: db.models.Member,
      }],
    }),
  }
}
