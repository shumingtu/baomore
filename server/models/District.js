// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import DistrictColumns from '../columns/District.js';
import { db } from '../db';

export default class District extends Model<ModelDefaultAttributes, DistrictAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(District.fields, { sequelize, scopes: District.scopes });
  }

  static fields = DistrictColumns(DataTypes);

  static associate() {
    District.belongsTo(db.models.City);
  }

  static scopes = {
    findByCity: cityId => ({
      where: {
        CityId: cityId,
      },
    }),
  }
}
