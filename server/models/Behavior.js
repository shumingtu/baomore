// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import BehaviorColumns from '../columns/Behavior.js';
import { db } from '../db';

export default class Behavior extends Model<ModelDefaultAttributes, MemberAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(Behavior.fields, { sequelize });
  }

  static fields = BehaviorColumns(DataTypes)

  static associate() {

  }
}
