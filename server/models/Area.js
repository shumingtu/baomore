// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import AreaColumns from '../columns/Area.js';
import { db } from '../db';

export default class Area extends Model<ModelDefaultAttributes, AreaAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(Area.fields, { sequelize, scopes: Area.scopes });
  }

  static fields = AreaColumns(DataTypes);

  static associate() {
    Area.hasMany(db.models.Member);
  }

  static scopes = {
    search: (args) => {
      const {
        areaId,
      } = args;

      const whereClause = {};
      if (areaId) whereClause.id = areaId;

      return {
        where: whereClause,
      };
    },
    findById: areaId => ({
      where: {
        id: areaId,
      },
    }),
  };
}
