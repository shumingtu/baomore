// @flow
import Sequelize, { Model, DataTypes, Op } from 'sequelize';
import PNTableColumns from '../columns/PNTable.js';
import { db } from '../db';
import {
  PNTableItemNotFoundError,
  PNTableCreateError,
} from '../errors/PNTable';

export default class PNTable extends Model<ModelDefaultAttributes, PNTableAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(PNTable.fields, { sequelize, scopes: PNTable.scopes });
  }

  static fields = PNTableColumns(DataTypes);

  static associate() {
    PNTable.belongsTo(db.models.PNSubCategory);
    PNTable.belongsTo(db.models.BrandModel);
    PNTable.belongsTo(db.models.Vendor);
    PNTable.hasMany(db.models.MemberStock);
    PNTable.belongsToMany(db.models.Member, {
      through: db.models.MemberStock,
      foreignKey: 'PNTableId',
      otherKey: 'MemberId',
    });
    PNTable.hasMany(db.models.Order);
  }

  static async editPNTableItem({
    id,
    code,
    pnCategoryId,
    bigSubCategoryId,
    smallSubCategoryId,
    BrandModelId,
    VendorId,
    name,
    picture,
    price,
    device,
    description,
    isOnSale,
    onlinePrice,
  }) {
    const pnTableItem = await db.models.PNTable.findOne({
      where: {
        id,
      },
    });

    if (!pnTableItem) {
      throw new PNTableItemNotFoundError();
    }

    pnTableItem.code = code;
    pnTableItem.PNSubCategoryId = (!smallSubCategoryId || smallSubCategoryId === -1)
      ? bigSubCategoryId : smallSubCategoryId;
    pnTableItem.BrandModelId = BrandModelId || null;
    pnTableItem.VendorId = VendorId;
    pnTableItem.name = name;
    pnTableItem.picture = picture;
    pnTableItem.price = price;
    pnTableItem.suitableDevice = device;
    pnTableItem.description = description;
    pnTableItem.isOnSale = isOnSale;
    pnTableItem.onlinePrice = onlinePrice || null;

    await pnTableItem.save();

    const pnCategory = await db.models.PNCategory.scope({ method: ['search', pnCategoryId] }).findOne();
    const bigSubCategory = await db.models.PNSubCategory.scope({ method: ['findById', bigSubCategoryId] }).findOne();
    const smallSubCategory = await db.models.PNSubCategory.scope({ method: ['findById', smallSubCategoryId] }).findOne();
    const vendor = await db.models.Vendor.findByPk(VendorId);

    return {
      ...pnTableItem.dataValues,
      pnCategory,
      bigSubCategory,
      smallSubCategory,
      Vendor: vendor,
    };
  }

  static async createPNTableItem({
    code,
    pnCategoryId,
    bigSubCategoryId,
    smallSubCategoryId,
    BrandModelId,
    VendorId,
    name,
    picture,
    price,
    device,
    description,
    isOnSale,
    onlinePrice,
  }) {
    const pnTableItem = await db.models.PNTable.create({
      code,
      PNSubCategoryId:
      (!smallSubCategoryId || smallSubCategoryId === -1) ? bigSubCategoryId : smallSubCategoryId,
      BrandModelId: BrandModelId || null,
      VendorId,
      name,
      picture,
      price,
      suitableDevice: device,
      description,
      isOnSale,
      onlinePrice: onlinePrice || null,
    });

    if (!pnTableItem) {
      throw new PNTableCreateError();
    }

    const pnCategory = await db.models.PNCategory.scope({ method: ['search', pnCategoryId] }).findOne();
    const bigSubCategory = await db.models.PNSubCategory.scope({ method: ['findById', bigSubCategoryId] }).findOne();
    const smallSubCategory = await db.models.PNSubCategory.scope({ method: ['findById', smallSubCategoryId] }).findOne();
    const vendor = await db.models.Vendor.scope({ method: ['search', VendorId] }).findOne();

    return {
      ...pnTableItem.dataValues,
      pnCategory,
      bigSubCategory,
      smallSubCategory,
      vendor,
    };
  }

  static async deletePNTableById({
    id,
  }) {
    const pnTableItem = await db.models.PNTable.findOne({
      where: {
        id,
      },
    });

    if (!pnTableItem) {
      throw new PNTableItemNotFoundError();
    }

    await pnTableItem.destroy();

    return {
      id: pnTableItem.id,
      status: true,
      message: '成功刪除',
    };
  }

  static scopes = {
    findCode: (code) => {
      const whereClause = {
        [Op.or]: [{
          code: {
            [Op.like]: `%${code}%`,
          },
        }],
      };

      return {
        where: whereClause,
      };
    },
    search: (args) => {
      const {
        id,
        code,
        keyword,
        isAllCustomized,
      } = args;

      let whereClause = {};

      if (id) whereClause.id = id;
      if (code) whereClause.code = code;
      if (isAllCustomized) whereClause.isAllCustomized = isAllCustomized;
      if (keyword) {
        whereClause = {
          ...whereClause,
          [Op.or]: [{
            name: {
              [Op.like]: `%${keyword}%`,
            },
          }, {
            code: {
              [Op.like]: `%${keyword}%`,
            },
          }],
        };
      }

      return {
        where: whereClause,
      };
    },
    pages: (limit, offset) => ({
      limit,
      offset,
    }),
    filterBySubCategory: subCategoryId => ({
      include: [
        { model: db.models.PNSubCategory.scope({ method: ['findById', subCategoryId] }) },
      ],
    }),
    stocks: (id, requireFlag) => {
      const whereClause = {
        MemberId: id,
      };

      if (requireFlag) {
        whereClause.storageCount = {
          [Op.ne]: 0,
        };
      }

      return {
        include: [{
          model: db.models.MemberStock,
          where: whereClause,
          required: !!requireFlag,
        }],
      };
    },
    brandAndModel: (brandId, brandModelId) => ({
      include: [{
        model: db.models.BrandModel.scope({ method: ['search', { id: brandModelId, brandId }] }),
        include: [{
          model: db.models.Brand,
        }],
        required: !!brandId,
      }],
    }),
    categories: (pnCategoryId, bigSubCategoryId, smallSubCategoryId) => ({
      include: [{
        model: db.models.PNSubCategory.scope(
          { method: ['search', { id: smallSubCategoryId, pnSubCategoryId: bigSubCategoryId, pnCategoryId }] }
        ),
        include: [{
          model: db.models.PNCategory,
        }, {
          model: db.models.PNSubCategory,
        }],
      }],
    }),
    vendor: vendorId => ({
      include: [{
        model: db.models.Vendor.scope({ method: ['search', { id: vendorId }] }),
        include: [{
          model: db.models.VendorSetting,
        }],
      }],
    }),
  }
}
