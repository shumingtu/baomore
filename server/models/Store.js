// @flow
import Sequelize, { Model, DataTypes, Op } from 'sequelize';
import StoreColumns from '../columns/Store.js';
import { db } from '../db';

export default class Store extends Model<ModelDefaultAttributes, StoreAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(Store.fields, { sequelize, scopes: Store.scopes, hooks: Store.hooks });
  }

  static fields = StoreColumns(DataTypes);

  static associate() {
    Store.belongsTo(db.models.Area);
    Store.belongsTo(db.models.Channel);
    Store.belongsTo(db.models.District);
    Store.belongsToMany(db.models.Member, {
      through: db.models.MemberStore,
      foreignKey: 'StoreId',
      otherKey: 'MemberId',
    });
    Store.hasMany(db.models.OnlineOrder);
    Store.hasMany(db.models.Order);
  }

  static hooks = {
    beforeBulkCreate: (instances) => {
      instances.forEach((instance) => {
        console.log('Store beforeBulkCreate', instance);
      });
    },
  }

  static scopes = {
    search: (args) => {
      const {
        storeId,
        districtId,
        keyword,
        channelId,
        isForClient,
      } = args;

      let whereClause = {};
      if (storeId) whereClause.id = storeId;
      if (districtId) whereClause.DistrictId = districtId;
      if (channelId) whereClause.ChannelId = channelId;
      if (keyword) {
        whereClause = {
          ...whereClause,
          [Op.or]: [{
            name: {
              [Op.like]: `%${keyword}%`,
            },
          }, {
            phone: {
              [Op.like]: `%${keyword}%`,
            },
          }, {
            address: {
              [Op.like]: `%${keyword}%`,
            },
          }],
        };
      }
      if (isForClient) whereClause.archive = false;


      return {
        where: whereClause,
      };
    },
    pages: (limit, offset) => ({
      limit,
      offset,
    }),
    area: () => ({
      include: [{
        model: db.models.Area,
      }],
    }),
    channel: (isForCustomize) => {
      const whereClause = {};

      if (isForCustomize) whereClause.isCustomizeChannel = isForCustomize;
      return {
        include: [{
          model: db.models.Channel,
          where: whereClause,
          required: isForCustomize,
        }],
      };
    },
    district: (cityId) => {
      const cityWhereClause = {};

      if (cityId) cityWhereClause.id = cityId;

      return {
        include: [{
          model: db.models.District,
          required: !!cityId,
          include: [{
            model: db.models.City,
            where: cityWhereClause,
          }],
        }],
      };
    },
    members: (isForClient) => {
      const whereClause = {};

      if (isForClient) {
        whereClause.lineId = {
          [Op.ne]: null,
        };
        whereClause.archive = false;
      }

      return {
        include: [{
          model: db.models.Member,
          where: whereClause,
          required: isForClient,
        }],
      };
    },
  }
}
