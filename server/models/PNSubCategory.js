// @flow
import Sequelize, { Model, DataTypes, Op } from 'sequelize';
import PNSubCategoryColumns from '../columns/PNSubCategory.js';
import { db } from '../db';
import {
  PNSubCategoryNotFoundError,
  PNSubCategoryCreateError,
} from '../errors/PNSubCategory';

export default class PNSubCategory extends Model<ModelDefaultAttributes, PNSubCategoryAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(PNSubCategory.fields, { sequelize, scopes: PNSubCategory.scopes });
  }

  static fields = PNSubCategoryColumns(DataTypes);

  static associate() {
    PNSubCategory.belongsTo(db.models.PNCategory);
    PNSubCategory.belongsTo(db.models.PNSubCategory);
  }

  static async deletePNSubCategory({ id }) {
    const pnSubCategory = await db.models.PNSubCategory.findOne({
      where: {
        id,
      },
    });

    if (!pnSubCategory) {
      throw new PNSubCategoryNotFoundError();
    }

    await pnSubCategory.destroy();

    return {
      id: pnSubCategory.id,
      status: true,
      message: '成功刪除類別',
    };
  }

  static async editPNSubCategory({
    id,
    name,
    picture,
    isOnSale,
  }) {
    const pnSubCategory = await db.models.PNSubCategory.findOne({
      where: {
        id,
      },
    });

    if (!pnSubCategory) {
      throw new PNSubCategoryNotFoundError();
    }

    pnSubCategory.name = name;
    pnSubCategory.picture = picture || null;
    pnSubCategory.isOnSale = isOnSale;

    await pnSubCategory.save();

    return pnSubCategory;
  }

  static async createPNSubCategory({
    name,
    picture,
    isOnSale,
    PNCategoryId,
    PNSubCategoryId,
  }) {
    const pnSubCategory = await db.models.PNSubCategory.create({
      name,
      picture: picture || null,
      isOnSale,
      PNCategoryId,
      PNSubCategoryId: PNSubCategoryId || null,
    });

    if (!pnSubCategory) {
      throw new PNSubCategoryCreateError();
    }

    return pnSubCategory;
  }

  static scopes = {
    search: (args) => {
      const {
        id,
        pnSubCategoryId,
        pnCategoryId,
      } = args;

      const whereClause = {};
      if (id) whereClause.id = id;
      if (pnSubCategoryId) whereClause.PNSubCategoryId = pnSubCategoryId;
      if (pnCategoryId) whereClause.PNCategoryId = pnCategoryId;

      return {
        where: whereClause,
      };
    },
    onSale: {
      where: {
        isOnSale: true,
      },
    },
    bigSubCategory: {
      where: {
        PNSubCategoryId: null,
      },
    },
    orderBySeqNum: {
      order: [
        ['seqNum'],
      ],
    },
    filterByKeyword: (keyword) => {
      let whereClause = {};

      if (keyword) {
        whereClause = {
          ...whereClause,
          [Op.or]: [{
            name: {
              [Op.like]: `%${keyword}%`,
            },
          }],
        };
      }

      return {
        where: whereClause,
      };
    },
    findByPNCategoryId: id => ({
      where: {
        PNCategoryId: id,
        PNSubCategoryId: null,
      },
    }),
    findBySubCategoryId: id => ({
      where: {
        PNSubCategoryId: id,
      },
    }),
    findById: id => ({
      where: {
        id,
      },
    }),
  };
}
