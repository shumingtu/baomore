// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import CityColumns from '../columns/City.js';
import { db } from '../db';

export default class City extends Model<ModelDefaultAttributes, CityAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(City.fields, { sequelize, scopes: City.scopes });
  }

  static fields = CityColumns(DataTypes);

  static associate() {
    City.hasMany(db.models.District);
  }

  static scopes = {
    districts: () => ({
      include: [
        { model: db.models.District }
      ],
    }),
  }
}
