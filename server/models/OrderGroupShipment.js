// @flow
import Sequelize, { Model, DataTypes, Op } from 'sequelize';
import moment from 'moment';
import _ from 'lodash';
import OrderGroupShipmentColumns from '../columns/OrderGroupShipment.js';
import { db } from '../db';
import {
  OrderGroupShipmentCreateError,
} from '../errors/OrderGroupShipment';
import padDigits from '../helpers/padDigits.js';

export default class OrderGroupShipment extends Model<ModelDefaultAttributes, OrderGroupShipmentAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(OrderGroupShipment.fields, { sequelize, scopes: OrderGroupShipment.scopes });
  }

  static fields = OrderGroupShipmentColumns(DataTypes);

  static associate() {
    OrderGroupShipment.belongsTo(db.models.PNTable);
    OrderGroupShipment.hasMany(db.models.Order);
  }

  static scopes = {
    search: (args) => {
      const {
        date,
        startDate,
        endDate,
        shippingStatus,
      } = args;

      const whereClause = {};

      if (startDate) {
        whereClause.createdAt = {
          [Op.and]: [{
            [Op.gte]: moment(startDate),
          }, {
            [Op.lt]: (endDate && moment(endDate).add(1, 'd')) || moment().add(1, 'd'),
          }],
        };
      }

      if (endDate && !startDate) {
        whereClause.createdAt = {
          [Op.lt]: moment(endDate).add(1, 'd'),
        };
      }

      if (date) whereClause.SN = date;
      if (shippingStatus) whereClause.status = shippingStatus;

      return {
        where: whereClause,
      };
    },
    pnTableAllAssoicateAndOrder: (args) => {
      const {
        vendorId,
      } = args;

      const vendorWhereClause = {};
      if (vendorId) vendorWhereClause.id = vendorId;

      return {
        include: [{
          model: db.models.PNTable.scope('categories', { method: ['search', args] }),
          required: true,
          include: [{
            model: db.models.Vendor,
            where: vendorWhereClause,
          }],
        }, {
          model: db.models.Order.scope({ method: ['memberArea', args] }),
        }],
      };
    },
    pages: (limit, offset) => ({
      limit,
      offset,
    }),
    pnTable: args => ({
      include: [{
        model: db.models.PNTable.scope({ method: ['search', args] }),
      }],
    }),
    order: (args, required) => ({
      include: [{
        model: db.models.Order.scope({ method: ['search', args] }),
      }],
      required: required || true,
    }),
  }

  static async updateGroupOrderStatus({ id }) {
    const orderGroupShipment = await db.models.OrderGroupShipment.findOne({
      where: {
        id,
      },
    });

    if (!orderGroupShipment) throw new OrderGroupShipmentCreateError();

    orderGroupShipment.status = 'SHIPMENTED';

    const targetOrders = await db.models.Order.findAll({
      where: {
        OrderGroupShipmentId: orderGroupShipment.id,
      },
      include: [{
        model: db.models.OnlineOrder,
      }],
    });

    const orderIds = targetOrders.map(x => x.id);
    const onlineOrderIds = [];
    targetOrders.forEach((x) => {
      if (x.OnlineOrder) onlineOrderIds.push(x.OnlineOrder.id);
    });

    await db.models.Order.update({
      status: 'DISPATCHED',
    }, {
      where: {
        id: orderIds,
      },
    });

    if (onlineOrderIds.length) {
      await db.models.OnlineOrder.update({
        shippingStatus: 'DISPATCHED',
      }, {
        where: {
          id: onlineOrderIds,
        },
      });
    }


    await orderGroupShipment.save();

    return {
      id: orderGroupShipment.id,
      status: true,
      message: '更新成功',
    };
  }

  static getOrderSN(closingOrders) {
    const orderSNResults = [];

    let orderSN = 1;

    const orderGroupByMember = _.groupBy(closingOrders, 'EmpolyedMemberId');

    Object.keys(orderGroupByMember).forEach((memberId) => {
      const memberOrders = orderGroupByMember[memberId];

      const orderByProduct = _.groupBy(memberOrders, 'ProductType');

      Object.keys(orderByProduct).forEach((productType) => {
        const productOrders = orderByProduct[productType];

        const orderByVendor = _.groupBy(productOrders, 'PNTable.VendorId');

        Object.keys(orderByVendor).forEach((vendorId) => {
          const vendorOrders = orderByVendor[vendorId];

          const orderIds = vendorOrders.map(x => x.id);

          orderSNResults.push({
            orderIds,
            orderSN,
            memberId, // 包膜師
            productType, // 商品
            vendorId, // 廠商
          });

          orderSN += 1;
        });
      });
    });

    return orderSNResults;
  }

  static async createOrderGroupShipment(pnTables, closingOrders) {
    const minDateOrder = _.minBy(closingOrders, 'createdAt');

    const orderSNGroup = db.models.OrderGroupShipment.getOrderSN(closingOrders);

    const transaction = await db.transaction();

    try {
      await orderSNGroup.map(x => () => new Promise(async (resolve) => {
        let SN = padDigits(x.orderSN, 3);
        // 可能同一天兩種料號有設定結單 一個在早上一個在下午
        // 所以要先找今天結過的單 同包膜師 同商品 同廠商 有的話訂單編號該一樣
        // 沒有的話 可能早上有結過 為了避免重複要找原本結過的最大orderSN開始當作訂單編號

        // 是否有相同訂單編號的單
        const sameSNOrders = await db.models.Order.findAll({
          attributes: ['orderSN'],
          where: {
            orderSN: {
              [Op.like]: `${moment().format('YYYYMMDD')}%`,
            },
            EmpolyedMemberId: x.memberId,
            ProductType: x.productType,
            orderSource: 'EMPOLYEE',
          },
          include: [{
            model: db.models.PNTable,
            where: {
              VendorId: x.vendorId,
            },
          }],
        });

        if (sameSNOrders.length) SN = sameSNOrders[0].orderSN.slice(8);

        if (!sameSNOrders.length) {
          // 找今天原本結過的單, 有的話應該取orderSN最大的開始當作訂單編號
          const groupedOrder = await db.models.Order.findAll({
            attributes: ['orderSN'],
            where: {
              orderSN: {
                [Op.like]: `${moment().format('YYYYMMDD')}%`,
              },
              orderSource: 'EMPOLYEE',
            },
          }).map(g => `${g.orderSN.slice(8)}`); // get last 3 number;

          if (groupedOrder.length) {
            const biggestSN = Math.max(...groupedOrder);
            SN = padDigits(x.orderSN + biggestSN, 3);
          }
        }

        await db.models.Order.update({
          orderSN: `${moment().format('YYYYMMDD')}${SN}`,
        }, {
          where: {
            id: {
              [Op.in]: x.orderIds,
            },
          },
          transaction,
        });

        resolve();
      })).reduce((prev, next) => prev.then(next), Promise.resolve());

      await pnTables.map(pnTable => () => new Promise(async (resolve) => {
        const orderGroupShipment = await db.models.OrderGroupShipment.create({
          SN: moment().format('YYYYMMDD'),
          status: 'UNSHIPMENTED',
          startDate: minDateOrder.createdAt,
          endDate: new Date(),
          PNTableId: pnTable.id,
        }, { transaction });

        const pnTablesOrders = closingOrders.filter(order => order.PNTable.code === pnTable.code);

        await db.models.Order.update({
          OrderGroupShipmentId: orderGroupShipment.id,
          status: 'UNDISPATCHED',
        }, {
          where: {
            id: pnTablesOrders.map(pntableOrder => pntableOrder.id),
          },
          transaction,
        });

        let qrcodeSN = 0;

        const createdQRCode = await db.models.OrderQRCode.findAll({
          where: {
            qrcode: {
              [Op.like]: `${moment().format('YYYYMMDD')}${pnTable.code}%`,
            },
          },
        }).map(q => `${q.qrcode.slice(20)}`); // get last 5 number;

        if (createdQRCode.length) {
          const biggestCode = Math.max(...createdQRCode);
          qrcodeSN = biggestCode;
        }

        await pnTablesOrders.map(order => () => new Promise(async (innerResolve) => {
          const {
            quantity,
          } = order;

          const arr = Array.from(Array(quantity));

          const qrcodeArray = arr.map(() => {
            qrcodeSN += 1;

            return {
              qrcode: `${moment().format('YYYYMMDD')}${order.PNTable.code}${padDigits(qrcodeSN, 5)}`,
              OrderId: order.id,
            };
          });

          await db.models.OrderQRCode.bulkCreate(qrcodeArray, { transaction });
          innerResolve();
        })).reduce((prev, next) => prev.then(next), Promise.resolve());

        resolve();
      })).reduce((prev, next) => prev.then(next), Promise.resolve());

      await transaction.commit();
    } catch (e) {
      await transaction.rollback();
      throw e;
    }
  }

  static async groupOrder(closingOrders) {
    const closingOrdersPNTables = closingOrders.map(x => ({
      id: x.PNTable.id, code: x.PNTable.code,
    }));

    const filteredOrderPNTables = _.uniqBy(closingOrdersPNTables, 'id');

    OrderGroupShipment.createOrderGroupShipment(filteredOrderPNTables, closingOrders);
  }
}
