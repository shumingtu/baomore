// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import BannerColumns from '../columns/Banner.js';
import { db } from '../db';
import {
  ResourceNotFoundError,
} from '../errors/General';

export default class Banner extends Model<ModelDefaultAttributes, BannerAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(Banner.fields, { sequelize });
  }

  static fields = BannerColumns(DataTypes);

  static associate() {}

  static async createBanner({
    picture,
    link,
    mobileImg,
  }) {
    const banner = await db.models.Banner.create({
      picture,
      link: link || null,
      mobileImg: mobileImg || null,
    });

    if (!banner) {
      return {
        error: 'Banner create failed.',
      };
    }

    return banner;
  }

  static async editBanner({
    id,
    picture,
    link,
    mobileImg,
  }) {
    const banner = await db.models.Banner.findOne({
      where: {
        id,
      },
    });

    if (!banner) {
      throw new ResourceNotFoundError();
    }

    banner.picture = picture;
    if (link) banner.link = link;
    if (mobileImg) banner.mobileImg = mobileImg;
    await banner.save();

    return banner;
  }

  static async deleteBanner({
    id,
  }) {
    const banner = await db.models.Banner.findOne({
      where: {
        id,
      },
    });

    if (!banner) {
      throw new ResourceNotFoundError();
    }

    await banner.destroy();

    return {
      id: banner.id,
      status: true,
      message: '刪除成功',
    };
  }
}
