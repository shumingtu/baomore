// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import ColorColumns from '../columns/BrandModelColors.js';
import { db } from '../db';
import {
  BrandModelColorNotFoundError,
  BrandModelNotFoundError,
} from '../errors/Brand';

export default class BrandModelColor extends Model<ModelDefaultAttributes, ColorAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(BrandModelColor.fields, { sequelize, scopes: BrandModelColor.scopes });
  }

  static fields = ColorColumns(DataTypes);

  static associate() {
    BrandModelColor.belongsTo(db.models.BrandModel);
  }

  static async createBrandModelColor({
    name,
    modelId,
    phoneBg,
    phoneMask,
    phoneCover,
  }) {
    const brandModel = await db.models.BrandModel.findOne({
      where: {
        id: modelId,
      },
    });

    if (!brandModel) {
      throw new BrandModelNotFoundError();
    }

    const brandModelColor = await db.models.BrandModelColor.create({
      name,
      phoneBg,
      phoneMask,
      phoneCover,
      BrandModelId: modelId,
    });

    if (!brandModelColor) {
      return {
        error: 'BrandModelColor create failed.',
      };
    }

    return brandModelColor;
  }

  static async editBrandModelColor({
    id,
    name,
    phoneBg,
    phoneMask,
    phoneCover,
    brandModelId,
  }) {
    const brandModelColor = await db.models.BrandModelColor.scope('includeBrandAndModel').findOne({
      where: {
        id,
      },
    });
    const brandModel = await db.models.BrandModel.isExist(brandModelId);

    if (!brandModelColor || !brandModel) {
      throw new BrandModelColorNotFoundError();
    }

    brandModelColor.name = name;
    brandModelColor.phoneBg = phoneBg;
    brandModelColor.phoneMask = phoneMask;
    brandModelColor.phoneCover = phoneCover;
    brandModelColor.BrandModelId = brandModelId;
    await brandModelColor.save();

    return brandModelColor;
  }

  static async deleteBrandModelColor({
    id,
  }) {
    const brandModelColor = await db.models.BrandModelColor.findOne({
      where: {
        id,
      },
    });

    if (!brandModelColor) {
      throw new BrandModelColorNotFoundError();
    }

    await brandModelColor.destroy();

    return {
      id: brandModelColor.id,
      status: true,
      message: '成功刪除顏色',
    };
  }

  static scopes = {
    search: (args) => {
      const {
        brandId,
        colorId,
        modelId,
      } = args;

      let whereClause = {};
      let whereIncludeClause = [{
        model: db.models.BrandModel,
        include: [{
          model: db.models.Brand,
        }]
      }];

      if (colorId) whereClause.id = colorId;
      if (modelId) whereClause.BrandModelId = modelId;
      if (brandId) {
        whereIncludeClause = [{
          model: db.models.BrandModel,
          where: {
            BrandId: brandId,
          },
          include: [{
            model: db.models.Brand,
          }]
        }];
      }

      return {
        where: whereClause,
        include: whereIncludeClause,
      }
    },
    pages: (limit, offset) => ({
      limit,
      offset,
    }),
    includeBrandAndModel: () => ({
      include: [{
        model: db.models.BrandModel,
        include: [{
          model: db.models.Brand,
        }]
      }],
    }),
  }
}
