// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import EmojiCategoryColumns from '../columns/EmojiCategory.js';
import { db } from '../db';

export default class EmojiCategory extends Model<ModelDefaultAttributes, EmojiCategoryAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(EmojiCategory.fields, { sequelize });
  }

  static fields = EmojiCategoryColumns(DataTypes);

  static associate() {}
}
