// @flow
import Sequelize, { Model, DataTypes, Op } from 'sequelize';
import ClosedOrderSettingColumns from '../columns/ClosedOrderSetting.js';
import { db } from '../db';
import {
  ClosedOrderSettingNotFoundError,
  ClosedOrderSettingCreateError,
  ClosedOrderSettingIsAlreadyExist,
} from '../errors/ClosedOrderSetting';
import {
  PNTableItemNotFoundError,
} from '../errors/PNTable';

export default class ClosedOrderSetting extends Model<ModelDefaultAttributes, ClosedOrderSettingAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(
      ClosedOrderSetting.fields,
      { sequelize, scopes: ClosedOrderSetting.scopes },
    );
  }

  static fields = ClosedOrderSettingColumns(DataTypes);

  static associate() {
    ClosedOrderSetting.belongsTo(db.models.PNTable);
  }

  static async deleteClosedOrderSetting({ id }) {
    const closedOrderSetting = await db.models.ClosedOrderSetting.findOne({
      where: {
        id,
      },
    });

    if (!closedOrderSetting) {
      throw new ClosedOrderSettingNotFoundError();
    }

    await closedOrderSetting.destroy();

    return {
      id: closedOrderSetting.id,
      status: true,
      message: '成功刪除結單設定',
    };
  }

  static async createClosedOrderSetting({
    closedDay,
    closedTime,
    code,
  }) {
    let PNTable = null;
    if (code) {
      const findPNTable = await db.models.PNTable.findOne({
        where: {
          code,
        },
      });

      if (!findPNTable) {
        throw new PNTableItemNotFoundError();
      }

      const closedOrderSettingItem = await db.models.ClosedOrderSetting.findOne({
        where: {
          PNTableId: findPNTable.id,
        },
      });

      if (closedOrderSettingItem) {
        throw new ClosedOrderSettingIsAlreadyExist();
      }

      PNTable = findPNTable;
    }

    const closedOrderSetting = await db.models.ClosedOrderSetting.create({
      closedDay,
      closedTime,
      PNTableId: PNTable ? PNTable.id : null,
    });

    if (!closedOrderSetting) {
      throw new ClosedOrderSettingCreateError();
    }

    return {
      ...closedOrderSetting.dataValues,
      PNTable,
    };
  }

  static async editClosedOrderSetting({
    id,
    closedDay,
    closedTime,
    code,
  }) {
    const closedOrderSetting = await db.models.ClosedOrderSetting.findOne({
      where: {
        id,
      },
    });

    if (!closedOrderSetting) {
      throw new ClosedOrderSettingNotFoundError();
    }

    let PNTable = null;

    if (code) {
      const findPNTable = await db.models.PNTable.findOne({
        where: {
          code,
        },
      });

      if (!findPNTable) {
        throw new PNTableItemNotFoundError();
      }

      PNTable = findPNTable;
      closedOrderSetting.PNTableId = findPNTable.id;
    }

    closedOrderSetting.closedDay = closedDay;
    // 2019-08-07 結單更新後如果時間不同 isClosed => false
    if (closedOrderSetting.closedTime !== closedTime) {
      closedOrderSetting.isClosed = false;
    }
    closedOrderSetting.closedTime = closedTime;

    await closedOrderSetting.save();

    return {
      ...closedOrderSetting.dataValues,
      PNTable,
    };
  }

  static scopes = {
    joinPNTable: () => ({
      include: [{
        model: db.models.PNTable,
      }],
    }),
  };
}
