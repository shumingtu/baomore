// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import EmojiColumns from '../columns/Emoji.js';
import { db } from '../db';

export default class Emoji extends Model<ModelDefaultAttributes, EmojiAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(Emoji.fields, { sequelize, scopes: Emoji.scopes });
  }

  static fields = EmojiColumns(DataTypes);

  static associate() {
    Emoji.belongsTo(db.models.EmojiCategory);
  }

  static scopes = {
    search: (args) => {
      const {
        categoryId,
      } = args;

      let whereClause = {};
      if (categoryId) whereClause.EmojiCategoryId = categoryId;

      return {
        where: whereClause,
      }
    },
  }
}
