// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import BookTimeSlotGroupColumns from '../columns/BookTimeSlotGroup.js';
import { db } from '../db';

export default class BookTimeSlotGroup extends Model<ModelDefaultAttributes, BookTimeSlotGroupAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(BookTimeSlotGroup.fields, { sequelize });
  }

  static fields = BookTimeSlotGroupColumns(DataTypes);

  static associate() {
    BookTimeSlotGroup.hasMany(db.models.BookTimeSlot);
  }
}
