// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import OrderQRCodeColumns from '../columns/OrderQRCode.js';
import { db } from '../db';

export default class OrderQRCode extends Model<ModelDefaultAttributes, OrderAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(OrderQRCode.fields, { sequelize });
  }

  static fields = OrderQRCodeColumns(DataTypes);

  static associate() {
    OrderQRCode.belongsTo(db.models.Order);
  }
}
