// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import MemberStoreColumns from '../columns/MemberStore.js';
import { db } from '../db';

export default class MemberStore extends Model<ModelDefaultAttributes, MemberStoreAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(MemberStore.fields, { sequelize, indexes: MemberStore.indexes, paranoid: false });
  }

  static fields = MemberStoreColumns(DataTypes);

  static associate() {
    MemberStore.belongsTo(db.models.Member);
    MemberStore.belongsTo(db.models.Store);
  }

  static indexes = [{
    name: 'MemberId',
    fields: ['MemberId'],
  }, {
    name: 'StoreId',
    fields: ['StoreId'],
  }]
}
