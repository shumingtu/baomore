// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import ActionColumns from '../columns/Action.js';
import { db } from '../db';

export default class Action extends Model<ModelDefaultAttributes, MemberAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(Action.fields, { sequelize });
  }

  static fields = ActionColumns(DataTypes)

  static associate() {
    Action.belongsToMany(db.models.Role, {
      through: 'RoleActions',
      foreignKey: 'ActionCode',
      otherKey: 'RoleId',
    });
  }
}
