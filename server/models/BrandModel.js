// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import BrandModelColumns from '../columns/BrandModels.js';
import { db } from '../db';
import {
  BrandNotFoundError,
  BrandModelNotFoundError,
} from '../errors/Brand';

export default class BrandModel extends Model<ModelDefaultAttributes, BrandModelAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(BrandModel.fields, { sequelize, hooks: BrandModel.hooks, scopes: BrandModel.scopes });
  }

  static fields = BrandModelColumns(DataTypes);

  static associate() {
    BrandModel.belongsTo(db.models.Brand);
    BrandModel.hasMany(db.models.BrandModelColor, { as: 'colors' });
    BrandModel.hasMany(db.models.PNTable);
  }

  static async createBrandModel({
    name,
    brandId,
  }) {
    const brand = await db.models.Brand.findOne({
      where: {
        id: brandId,
      },
    });

    if (!brand) {
      throw new BrandNotFoundError();
    }

    const brandModel = await db.models.BrandModel.create({
      name,
      BrandId: brandId,
    });

    if (!brandModel) {
      return {
        error: 'BrandModel create failed.',
      };
    }

    return brandModel;
  }

  static async editBrandModel({
    id,
    name,
  }) {
    const brandModel = await db.models.BrandModel.findOne({
      where: {
        id,
      },
    });

    if (!brandModel) {
      throw new BrandModelNotFoundError();
    }

    brandModel.name = name;
    await brandModel.save();

    return brandModel;
  }

  static async deleteBrandModel({
    id,
  }) {
    const brandModel = await db.models.BrandModel.findOne({
      where: {
        id,
      },
    });

    if (!brandModel) {
      throw new BrandModelNotFoundError();
    }

    await brandModel.destroy();

    return {
      id: brandModel.id,
      status: true,
      message: '成功刪除型號',
    };
  }

  static async isExist(id) {
    const brandModel = await db.models.BrandModel.findOne({
      attributes: [
        'id',
      ],
      where: {
        id
      },
    });

    if (brandModel) return true;
    return false;
  }

  static hooks = {
    afterDestroy: async (brandModel) => {
      await db.models.BrandModelColor.destroy({
        where: {
          BrandModelId: brandModel.id,
        }
      });
    },
  }

  static scopes = {
    search: (args) => {
      const {
        id,
        brandId,
      } = args;

      const whereClause = {};

      if (id) whereClause.id = id;
      if (brandId) whereClause.BrandId = brandId;

      return {
        where: whereClause,
      };
    },
  }
}
