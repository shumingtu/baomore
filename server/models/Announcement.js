// @flow
import Sequelize, { Model, DataTypes, Op } from 'sequelize';
import moment from 'moment';
import AnnouncementColumns from '../columns/Announcement.js';
import { db } from '../db';
import { ResourceNotFoundError } from '../errors/General';

export default class Announcement extends Model<ModelDefaultAttributes, AnnouncementAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(Announcement.fields, {
      sequelize,
      defaultScope: Announcement.defaultScope,
      scopes: Announcement.scopes,
    });
  }

  static fields = AnnouncementColumns(DataTypes);

  static associate() {

  }

  static async createAnnouncement({
    title,
    content,
  }) {
    const announcement = await db.models.Announcement.create({
      title,
      content,
    });

    if (!announcement) {
      return {
        error: 'Announcement create failed.',
      };
    }

    return announcement;
  }

  static async editAnnouncement({
    id,
    title,
    content,
  }) {
    const announcement = await db.models.Announcement.findOne({
      where: {
        id,
      },
    });

    if (!announcement) {
      throw new ResourceNotFoundError();
    }

    announcement.title = title;
    announcement.content = content;
    await announcement.save();

    return announcement;
  }

  static async deleteAnnouncement({
    id,
  }) {
    const announcement = await db.models.Announcement.findOne({
      where: {
        id,
      },
    });

    if (!announcement) {
      throw new ResourceNotFoundError();
    }

    await announcement.destroy();

    return {
      id: announcement.id,
      status: true,
      message: '成功刪除公告',
    };
  }

  static defaultScope = {
    order: [
      ['createdAt', 'DESC'],
    ],
  };

  static scopes = {
    search: (args) => {
      const {
        id,
        keyword,
        startDate,
        endDate,
      } = args;

      let whereClause = {};

      if (id) whereClause.id = id;
      if (startDate || endDate) {
        whereClause.createdAt = {
          [Op.and]: [{
            [Op.gte]: (startDate && moment(startDate)) || moment(),
          }, {
            [Op.lte]: (endDate && moment(endDate)) || moment(),
          }],
        };
      }
      if (keyword) {
        whereClause = {
          ...whereClause,
          [Op.or]: [{
            title: {
              [Op.like]: `%${keyword}%`,
            },
          }, {
            content: {
              [Op.like]: `%${keyword}%`,
            },
          }],
        };
      }

      return {
        where: whereClause,
      };
    },
    order: {
      order: [
        ['createdAt', 'DESC'],
      ],
    },
    pages: (limit, offset) => ({
      limit,
      offset,
    }),
  }
}
