// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';

import EmployeeTransferScheduleColumns from '../columns/EmployeeTransferSchedule.js';
import { db } from '../db';

export default class EmployeeTransferSchedule extends Model<ModelDefaultAttributes, OnlineOrderAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(EmployeeTransferSchedule.fields, {
      sequelize,
    });
  }

  static fields = EmployeeTransferScheduleColumns(DataTypes);

  static associate() {
    EmployeeTransferSchedule.belongsTo(db.models.BehaviorRecord);
  }
}
