// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import ChannelColumns from '../columns/Channel.js';

export default class Channel extends Model<ModelDefaultAttributes, ChannelAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(Channel.fields, { sequelize, scopes: Channel.scopes });
  }

  static fields = ChannelColumns(DataTypes);

  static associate() {}

  static scopes = {
    search: (args) => {
      const {
        channelId,
      } = args;

      const whereClause = {};
      if (channelId) whereClause.id = channelId;

      return {
        where: whereClause,
      };
    },
  };
}
