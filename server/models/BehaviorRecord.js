// @flow
import Sequelize, { Model, DataTypes, Op } from 'sequelize';
import moment from 'moment';
import BehaviorRecordColumns from '../columns/BehaviorRecord.js';
import { db } from '../db';
import {
  ResourceNotFoundError,
  MemberNotFoundError,
} from '../errors/General';

export default class BehaviorRecord extends Model<ModelDefaultAttributes, MemberAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(BehaviorRecord.fields, {
      sequelize,
      defaultScope: BehaviorRecord.defaultScope,
      scopes: BehaviorRecord.scopes,
    });
  }

  static fields = BehaviorRecordColumns(DataTypes)

  static associate() {
    BehaviorRecord.belongsTo(db.models.BehaviorRecord, { as: 'TransferRecord' });
    BehaviorRecord.belongsTo(db.models.Member);
    BehaviorRecord.belongsTo(db.models.Member, { as: 'Approver' });
    BehaviorRecord.belongsTo(db.models.Vendor);
    BehaviorRecord.belongsTo(db.models.Store);
    BehaviorRecord.belongsTo(db.models.Behavior);
    BehaviorRecord.belongsTo(db.models.PNTable);
  }

  static defaultScope = {
    order: [
      ['createdAt', 'DESC'],
    ],
  };

  static async verifyBehaviorRecords({
    recordIds,
    approverId,
  }) {
    const approver = await db.models.Member.findOne({
      where: {
        id: approverId,
      },
    });

    if (!approver) throw new MemberNotFoundError();

    const records = await db.models.BehaviorRecord.scope(
      { method: ['searchByIds', { ids: recordIds }] },
      'pnTable',
      'member',
      'order',
      { method: ['vendor', false] },
      { method: ['store', false] },
    ).findAll({
      where: {
        ApproverId: null,
      },
    });

    if (!records) throw new ResourceNotFoundError();

    let errorRecords = [];

    await records.map(record => () => new Promise(async (resolve, reject) => {
      const transaction = await db.transaction();

      try {
        await record.update({
          ApproverId: approverId,
        }, { transaction });

        if (record.rollbackType !== 'CUSTOMERCOMPLAINT') {
          await db.models.MemberStock.reduceStock(
            record.MemberId,
            record.PNTableId,
            record.quantity,
            transaction,
          );
        }
        await transaction.commit();
        resolve();
      } catch (e) {
        await transaction.rollback();
        errorRecords.push(record);
        reject(e);
      }
    })).reduce((prev, next) => prev.then(next).catch(e => console.log(e)), Promise.resolve());

    const errorRecordIds = errorRecords.map(errorRecord => errorRecord.id);

    const resultRecords = records.filter(record => !errorRecordIds.includes(record.id));

    const newRecords = resultRecords.map(record => ({
      ...record.dataValues,
      remarks: (record.dataValues.remark && record.dataValues.remark.split('***')) || [],
      pnCategory: (record.PNTable
        && record.PNTable.PNSubCategory
        && record.PNTable.PNSubCategory.PNCategory) || null,
      Approver: approver,
    }));

    errorRecords = errorRecords.map(errorRecord => ({
      ...errorRecord.dataValues,
      remarks: (errorRecord.dataValues.remark && errorRecord.dataValues.remark.split('***')) || [],
      pnCategory: (errorRecord.PNTable
        && errorRecord.PNTable.PNSubCategory
        && errorRecord.PNTable.PNSubCategory.PNCategory) || null,
      Approver: null,
    }));

    return {
      behaviorRecords: newRecords,
      errorRecords,
    };
  }

  static scopes = {
    searchForPerformance: (args) => {
      const {
        startDate,
        endDate,
        storeId,
        employeeId,
        behaviorId,
        recordStartDate,
        recordEndDate,
      } = args;

      const whereClause = {};

      const recordDateCondition = [];

      if (recordStartDate) {
        recordDateCondition.push({
          [Op.gte]: moment(recordStartDate),
        });
      }

      if (recordEndDate) {
        recordDateCondition.push({
          [Op.lt]: moment(recordEndDate).add(1, 'd'),
        });
      }

      if (recordDateCondition.length) {
        whereClause.createdAt = {
          [Op.and]: recordDateCondition,
        };
      }

      if (startDate) {
        whereClause.startTime = {
          [Op.gte]: moment(startDate),
        };
      }

      if (endDate) {
        whereClause.endTime = {
          [Op.lt]: moment(endDate).add(1, 'd'),
        };
      }

      if (storeId) whereClause.StoreId = storeId;
      if (employeeId) whereClause.MemberId = employeeId;
      if (behaviorId) whereClause.BehaviorId = behaviorId;

      return {
        where: whereClause,
      };
    },
    search: (args) => {
      const {
        id,
        ids,
        rollbackType,
        startDate,
        endDate,
        memberId,
        errorType,
        target,
        storeId,
        vat,
        customerName,
        customerPhone,
      } = args;

      let whereClause = {};

      if (rollbackType && rollbackType !== 'NONE') {
        whereClause.BehaviorId = 5; // 退貨
      }

      whereClause.rollbackType = rollbackType || 'NONE';

      if (ids && Array.isArray(ids)) {
        whereClause.id = ids;
      }

      if (startDate || endDate) {
        whereClause.createdAt = {
          [Op.and]: [{
            [Op.gte]: (startDate && moment(startDate)) || moment(),
          }, {
            [Op.lte]: (endDate && moment(endDate)) || moment(),
          }],
        };
      }

      if (id) whereClause.id = id;
      if (memberId) whereClause.MemberId = memberId;
      if (storeId) whereClause.StoreId = storeId;

      if (errorType || vat || customerName || customerPhone || target) {
        const errorTypeSearch = errorType ? { remark: { [Op.like]: `%${errorType}%` } } : null;
        const vatSearch = vat ? { remark: { [Op.like]: `%${vat}%` } } : null;
        const customerNameSearch = customerName ? { remark: { [Op.like]: `%${customerName}%` } } : null;
        const customerPhoneSearch = customerPhone ? { remark: { [Op.like]: `%${customerPhone}%` } } : null;
        const targetSearch = target ? { remark: { [Op.like]: `%${target}%` } } : null;
        whereClause = {
          ...whereClause,
          [Op.and]: [
            errorTypeSearch,
            vatSearch,
            customerNameSearch,
            customerPhoneSearch,
            targetSearch,
          ].filter(s => s),
        };
      }

      return {
        where: whereClause,
      };
    },
    searchByIds: ({ ids }) => ({
      where: ids && Array.isArray(ids) ? {
        id: ids,
      } : {},
    }),
    pnTable: (code, pnCategoryId) => ({
      include: [{
        model: db.models.PNTable.scope(
          { method: ['search', { code }] },
          { method: ['categories', pnCategoryId, null, null] },
        ),
      }],
    }),
    member: memberId => ({
      include: [{
        model: db.models.Member,
        where: memberId ? {
          id: memberId,
        } : {},
      }],
    }),
    vendor: (required = true, vendorId) => ({
      include: [{
        model: db.models.Vendor,
        required,
        where: vendorId ? {
          id: vendorId,
        } : {},
      }],
    }),
    store: (required = true, storeId) => ({
      include: [{
        model: db.models.Store,
        required,
        where: storeId ? {
          id: storeId,
        } : {},
      }],
    }),
    approver: (required = true, memberId) => ({
      include: [{
        model: db.models.Member,
        required,
        as: 'Approver',
        where: memberId ? {
          id: memberId,
        } : {},
      }],
    }),
    pages: (limit, offset) => ({
      limit,
      offset,
    }),
    order: {
      order: [
        ['createdAt', 'DESC'],
      ],
    },
    performanceMemberAssociation: args => ({
      include: [{
        attributes: ['id', 'name', 'AreaId', 'MemberId'],
        model: db.models.Member.scope({ method: ['search', args] }),
        required: true,
        include: [{
          attributes: ['id', 'name'],
          model: db.models.Area,
          required: true,
        }, {
          model: db.models.Role,
          where: {
            name: '包膜師',
          },
          required: true,
        }],
      }],
    }),
    performancePNTableAssociation: () => ({
      include: [{
        model: db.models.PNTable,
        attributes: ['id', 'PNSubCategoryId'],
        required: true,
        include: [{
          attributes: ['id', 'PNCategoryId'],
          model: db.models.PNSubCategory,
          required: true,
          include: [{
            attributes: ['id', 'name'],
            model: db.models.PNCategory,
            where: {
              id: {
                [Op.in]: [1, 2],
              },
            },
          }],
        }],
      }],
    }),
    performanceStoreAssociation: args => ({
      include: [{
        attributes: ['id', 'name', 'ChannelId'],
        model: db.models.Store.scope({ method: ['search', args] }),
        required: true,
        include: [{
          attributes: ['id', 'name'],
          model: db.models.Channel,
          required: true,
        }],
      }],
    }),
    behavior: () => ({
      include: [{
        model: db.models.Behavior,
      }],
    }),
  };
}
