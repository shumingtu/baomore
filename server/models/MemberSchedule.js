// @flow
import Sequelize, { Model, DataTypes, Op } from 'sequelize';
import moment from 'moment';
import MemberScheduleColumns from '../columns/MemberSchedule.js';
import { db } from '../db';

export default class MemberSchedule extends Model<ModelDefaultAttributes, MemberScheduleAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(MemberSchedule.fields, { sequelize, scopes: MemberSchedule.scopes });
  }

  static fields = MemberScheduleColumns(DataTypes);

  static associate() {
    MemberSchedule.belongsTo(db.models.Member);
  }

  static scopes = {
    search: (args) => {
      const {
        employeeId,
        startDate,
        endDate,
      } = args;

      const whereClause = {};

      if (employeeId) whereClause.MemberId = employeeId;

      if (startDate && endDate) {
        whereClause.date = {
          [Op.and]: [{
            [Op.gte]: moment(startDate),
          }, {
            [Op.lt]: moment(endDate).add(1, 'd'),
          }],
        };
      }

      return {
        where: whereClause,
      };
    },
  }
}
