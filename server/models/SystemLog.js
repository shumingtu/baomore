// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import SystemLogColumns from '../columns/SystemLog.js';
import { db } from '../db';

export default class SystemLog extends Model<ModelDefaultAttributes, SystemLogAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(SystemLog.fields, { sequelize, scopes: SystemLog.scopes });
  }

  static fields = SystemLogColumns(DataTypes);

  static associate() {}

  static createLog(typeId, memberId, log) {
    SystemLog.create({
      SystemLogTypeId: typeId,
      MemberId: memberId,
      log,
    });
  }

  static scopes = {
    search: args => {
      const {
        id,
      } = args;

      const whereClause = {};

      if (id) whereClause.id = id;

      return {
        where: whereClause,
      };
    },
  };
}
