// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import BookTimeSlotColumns from '../columns/BookTimeSlot.js';
import { db } from '../db';

export default class BookTimeSlot extends Model<ModelDefaultAttributes, BookTimeSlotAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(BookTimeSlot.fields, { sequelize, scopes: BookTimeSlot.scopes });
  }

  static fields = BookTimeSlotColumns(DataTypes);

  static associate() {
    BookTimeSlot.belongsTo(db.models.BookTimeSlotGroup);
  }


  static scopes = {
    search: (args) => {
      const {
        id,
        time,
      } = args;

      let whereClause = {};

      if (id) whereClause.id = id;
      if (time) whereClause.time = time;

      return {
        where: whereClause,
      };
    },
  }
}
