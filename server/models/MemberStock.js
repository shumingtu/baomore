// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import MemberStockColumns from '../columns/MemberStock.js';
import {
  StockNotFoundError,
  StockOverflowError,
} from '../errors/Stock';
import { db } from '../db';

export default class MemberStock extends Model<ModelDefaultAttributes, MemberStockAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(MemberStock.fields, { sequelize, indexes: MemberStock.indexes, scopes: MemberStock.scopes });
  }

  static fields = MemberStockColumns(DataTypes);

  static associate() {
    MemberStock.belongsTo(db.models.Member);
    MemberStock.belongsTo(db.models.PNTable);
  }

  static indexes = [{
    name: 'MemberId',
    fields: ['MemberId'],
  }, {
    name: 'PNTableId',
    fields: ['PNTableId'],
  }]

  static scopes = {
    search: (args) => {
      const {
        pnTableId,
        memberId,
      } = args;

      let whereClause = {};

      if (pnTableId) whereClause.PNTableId = pnTableId;
      if (memberId) whereClause.MemberId = memberId;

      return {
        where: whereClause,
      };
    },
    member: () => ({
      include: [{
        model: db.models.Member,
      }],
    }),
    pnTable: args => ({
      include: [{
        model: db.models.PNTable.scope(
          'brandAndModel',
          'categories',
          'vendor',
          { method: ['search', args] },
        ),
      }],
    }),
  };

  static async addStock(memberId, pnTableId, amount, transaction) {
    const stock = await db.models.MemberStock.scope(
      { method: ['search', { memberId, pnTableId }] }
    ).findOne();

    if (stock) {
      await stock.increment('storageCount', {
        by: amount,
        transaction,
      });
      return;
    }

    await db.models.MemberStock.create({
      storageCount: amount,
      MemberId: memberId,
      PNTableId: pnTableId,
    }, { transaction });
  }

  static async reduceStock(memberId, pnTableId, amount, transaction) {
    const stock = await db.models.MemberStock.scope(
      { method: ['search', { memberId, pnTableId }] }
    ).findOne();

    if (!stock) throw new StockNotFoundError();
    if (stock.storageCount < amount) throw new StockOverflowError();

    await stock.decrement('storageCount', {
      by: amount,
      transaction,
    });
  }
}
