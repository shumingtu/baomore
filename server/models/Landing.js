// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import LandingColumns from '../columns/Landing.js';
import { db } from '../db';
import {
  ResourceNotFoundError,
} from '../errors/General';

export default class Landing extends Model<ModelDefaultAttributes, LandingAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(Landing.fields, { sequelize });
  }

  static fields = LandingColumns(DataTypes);

  static associate() {}

  static async createLanding({
    desktopImg,
    mobileImg,
    link,
  }) {
    const landing = await db.models.Landing.create({
      desktopImg,
      mobileImg,
      link: link || null,
    });

    if (!landing) {
      return {
        error: 'Landing create failed.',
      };
    }

    return landing;
  }

  static async editLanding({
    id,
    desktopImg,
    mobileImg,
    link,
  }) {
    const landing = await db.models.Landing.findOne({
      where: {
        id,
      },
    });

    if (!landing) {
      throw new ResourceNotFoundError();
    }

    if (link) landing.link = link;
    landing.desktopImg = desktopImg;
    landing.mobileImg = mobileImg;
    await landing.save();

    return landing;
  }

  static async deleteLanding({
    id,
  }) {
    const landing = await db.models.Landing.findOne({
      where: {
        id,
      },
    });

    if (!landing) {
      throw new ResourceNotFoundError();
    }

    await landing.destroy();

    return {
      id: landing.id,
      status: true,
      message: '刪除成功',
    };
  }
}
