// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import PerformanceSettingColumns from '../columns/PerformanceSetting.js';
import { db } from '../db';

export default class PerformanceSetting extends Model<ModelDefaultAttributes, PerformanceSettingAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(PerformanceSetting.fields, { sequelize, scopes: PerformanceSetting.scopes });
  }

  static fields = PerformanceSettingColumns(DataTypes);

  static associate() {
    PerformanceSetting.belongsTo(db.models.PNCategory);
  }

  static scopes = {
    search: (args) => {
      const {
        PNCategoryId,
      } = args;

      const whereClause = {};

      if (PNCategoryId) whereClause.PNCategoryId = PNCategoryId;

      return {
        where: whereClause,
      };
    },
  }
}
