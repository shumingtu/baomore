// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import ActivityDetailFragmentColumns from '../columns/ActivityDetailFragment.js';
import { db } from '../db';

export default class ActivityDetailFragment extends Model<ModelDefaultAttributes, ActivityDetailFragmentAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(ActivityDetailFragment.fields, { sequelize });
  }

  static fields = ActivityDetailFragmentColumns(DataTypes);

  static associate() {
    ActivityDetailFragment.belongsTo(db.models.Activity);
  }
}
