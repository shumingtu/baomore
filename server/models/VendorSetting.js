// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';

import VendorColumns from '../columns/VendorSetting.js';
import { db } from '../db';

export default class VendorSetting extends Model<ModelDefaultAttributes, VendorSettingAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(VendorSetting.fields, { sequelize, paranoid: false });
  }

  static fields = VendorColumns(DataTypes);

  static associate() {
    VendorSetting.belongsTo(db.models.Vendor);
    VendorSetting.belongsTo(db.models.Member);
  }
}
