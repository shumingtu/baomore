// @flow
import moment from 'moment';
import Sequelize, { Model, DataTypes, Op } from 'sequelize';
import ManagerPunchRecordColumns from '../columns/ManagerPunchRecord.js';
import { db } from '../db';

export default class ManagerPunchRecord extends Model<ModelDefaultAttributes, ManagerPunchRecordAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(ManagerPunchRecord.fields, { sequelize, scopes: ManagerPunchRecord.scopes });
  }

  static fields = ManagerPunchRecordColumns(DataTypes);

  static associate() {
    ManagerPunchRecord.belongsTo(db.models.Member, { as: 'EmployeedMember' });
    ManagerPunchRecord.belongsTo(db.models.Member);
    ManagerPunchRecord.belongsTo(db.models.Store);
  }

  static scopes = {
    search: (args) => {
      const {
        employeeId,
        storeId,
        startDate,
        endDate,
      } = args;

      const whereClause = {};

      if (employeeId) whereClause.EmployeedMemberId = employeeId;
      if (storeId) whereClause.StoreId = storeId;
      if (startDate) {
        whereClause.createdAt = {
          [Op.and]: [{
            [Op.gte]: moment(startDate),
          }, {
            [Op.lte]: endDate && moment(endDate) || moment(),
          }],
        }
      }

      return {
        where: whereClause,
      };
    },
    store: () => ({
      include: [{
        model: db.models.Store,
      }],
    }),
    pages: (limit, offset) => ({
      limit,
      offset,
    }),
  }
}
