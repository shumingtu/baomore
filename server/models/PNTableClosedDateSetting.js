// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import PNTableClosedDateSettingColumns from '../columns/PNTableClosedDateSetting.js';
import { db } from '../db';

export default class PNTableClosedDateSetting extends Model<ModelDefaultAttributes, PNTableClosedDateSettingAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(PNTableClosedDateSetting.fields, { sequelize });
  }

  static fields = PNTableClosedDateSettingColumns(DataTypes);

  static associate() {
    PNTableClosedDateSetting.belongsTo(db.models.PNTable);
  }
}
