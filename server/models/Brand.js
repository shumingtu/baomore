// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import BrandColumns from '../columns/Brand.js';
import { db } from '../db';
import {
  BrandNotFoundError,
} from '../errors/Brand';

export default class Brand extends Model<ModelDefaultAttributes, BrandAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(Brand.fields, { sequelize, hooks: Brand.hooks, scopes: Brand.scopes });
  }

  static fields = BrandColumns(DataTypes);

  static associate() {
    Brand.hasMany(db.models.BrandModel, { as: 'models' });
  }

  static async createBrand({
    name,
  }) {
    const brand = await db.models.Brand.create({ name });

    if (!brand) {
      return {
        error: 'Brand create failed.',
      };
    }

    return brand;
  }

  static async editBrand({
    id,
    name,
  }) {
    const brand = await db.models.Brand.findOne({
      where: {
        id,
      },
    });

    if (!brand) {
      throw new BrandNotFoundError();
    }

    brand.name = name;
    await brand.save();

    return brand;
  }

  static async deleteBrand({
    id,
  }) {
    const brand = await db.models.Brand.findOne({
      where: {
        id,
      },
    });

    if (!brand) {
      throw new BrandNotFoundError();
    }

    await brand.destroy();

    return {
      id: brand.id,
      status: true,
      message: '成功刪除品牌',
    };
  }

  static hooks = {
    afterDestroy: async (brand) => {
      const brandModels = await db.models.BrandModel.findAll({
        where: {
          BrandId: brand.id,
        },
      });

      brandModels.map(async (brandModel) => {
        await brandModel.destroy();
      });
    },
  }

  static scopes = {
    search: (args) => {
      const {
        id,
      } = args;

      const whereClause = {};

      if (id) whereClause.id = id;

      return {
        where: whereClause,
      };
    },
  }
}
