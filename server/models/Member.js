// @flow

import bcrypt from 'bcrypt';
import debug from 'debug';
import Sequelize, { Model, DataTypes, Op } from 'sequelize';
import {
  jwtSign,
  jwtSignRefreshToken,
} from '../helpers/jwt.js';
import { getMemberPermission } from '../helpers/permissions';
import {
  AccountNotFoundError,
  PasswordNotMatchedError,
} from '../errors/Member';
import {
  SystemError,
} from '../errors/General';
import MemberColumns from '../columns/Member';
import { db } from '../db';

const debugMemeberModel = debug('BAOMORE:Member:Model');

export default class Member extends Model<ModelDefaultAttributes, MemberAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(Member.fields, { sequelize, scopes: Member.scopes });
  }

  static fields = MemberColumns(DataTypes)

  // Class Method
  static associate() {
    Member.belongsToMany(db.models.Role, {
      through: 'MemberRoles',
      foreignKey: 'MemberId',
      otherKey: 'RoleId',
    });
    Member.belongsToMany(db.models.Store, {
      through: db.models.MemberStore,
      foreignKey: 'MemberId',
      otherKey: 'StoreId',
    });
    Member.hasMany(db.models.VendorSetting);
    Member.belongsTo(db.models.Member);
    Member.hasMany(db.models.Order, { foreignKey: 'EmpolyedMemberId' });
    Member.hasMany(db.models.MemberWarranty, { as: 'memberWarranties' });
    Member.hasMany(db.models.MemberWarranty, { foreignKey: 'ApproverId', as: 'Approver' });
    Member.hasMany(db.models.OnlineOrder, { foreignKey: 'BookedMemberId' });
    Member.hasMany(db.models.OnlineOrder, { foreignKey: 'OwnerId', as: 'onlineOrders' });
    Member.hasMany(db.models.MemberSchedule);
    Member.belongsTo(db.models.Area);
    Member.belongsToMany(db.models.PNTable, {
      through: db.models.MemberStock,
      foreignKey: 'MemberId',
      otherKey: 'PNTableId',
    });
  }

  static async login(account, password) {
    const member = await Member.findOne({
      where: {
        account,
      },
    });

    if (!member) throw new AccountNotFoundError();

    if (await member.validPassword(password)) {
      return {
        member,
        refreshToken: jwtSignRefreshToken({
          id: member.id,
          account: member.account,
        }),
      };
    }

    throw new PasswordNotMatchedError();
  }

  static async createoAuthMember(data) {
    const member = await Member.create(data);

    if (!member) {
      return {
        error: 'Member Create Failed',
      };
    }

    return member;
  }

  static async memberoAuthLogin(oAuthId) {
    try {
      const member = await Member.findOne({
        where: {
          lineId: oAuthId,
          archive: false,
        },
      });

      if (!member) {
        return AccountNotFoundError;
      }

      const token = await member.accessToken();

      return {
        member,
        accessToken: token,
      };
    } catch (ex) {
      debugMemeberModel(ex);

      return SystemError;
    }
  }

  static getMe(memberId) {
    return db.models.Member.findOne({ where: { id: memberId } });
  }

  static scopes = {
    search: (args) => {
      const {
        memberId,
        areaId,
        keyword,
        directorId,
        lineIdRequired,
        isForClient,
      } = args;

      let whereClause = {};
      if (lineIdRequired) {
        whereClause.lineId = {
          [Op.ne]: null,
        };
      }
      if (isForClient) {
        whereClause.archive = false;
      }
      if (memberId) whereClause.id = memberId;
      if (areaId) whereClause.AreaId = areaId;
      if (directorId) whereClause.MemberId = directorId;
      if (keyword) {
        whereClause = {
          ...whereClause,
          [Op.or]: [{
            serialNumber: {
              [Op.like]: `%${keyword}%`,
            },
          }, {
            name: {
              [Op.like]: `%${keyword}%`,
            },
          }, {
            phone: {
              [Op.like]: `%${keyword}%`,
            },
          }],
        };
      }

      return {
        where: whereClause,
      };
    },
    findById: memberId => ({
      where: {
        id: memberId,
      },
    }),
    pages: (limit, offset) => ({
      limit,
      offset,
    }),
    employee: ({
      storeId,
      employeeType,
    }) => ({
      include: [{
        model: db.models.Role,
        where: {
          name: employeeType || '包膜師',
        },
      }, {
        model: db.models.Store,
        where: {
          id: storeId,
        },
        required: !!storeId,
      }, {
        model: db.models.Area,
      }],
    }),
    warranty: () => ({
      include: [{
        model: db.models.MemberWarranty,
        as: 'memberWarranties',
      }],
    }),
    onlineOrderStore: () => ({
      include: [{
        model: db.models.OnlineOrder,
        as: 'onlineOrders',
        include: [{
          model: db.models.Store,
        }, {
          model: db.models.Member,
          as: 'BookedMember',
        }],
      }],
    }),
  }

  // Prototype
  validPassword(password) {
    return bcrypt.compare(password, this.password);
  }

  async permissions() {
    return getMemberPermission(this.id);
  }

  async accessToken() {
    return jwtSign({
      id: this.id,
      name: this.name,
      permissions: await this.permissions(),
    });
  }

  async payToken(orderId) {
    return jwtSign({
      id: this.id,
      orderId,
    });
  }
}
