// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import VendorColumns from '../columns/Vendor.js';
import { db } from '../db';

export default class Vendor extends Model<ModelDefaultAttributes, VendorAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(Vendor.fields, { sequelize, scopes: Vendor.scopes });
  }

  static fields = VendorColumns(DataTypes);

  static associate() {
    Vendor.hasMany(db.models.VendorSetting);
  }

  static scopes = {
    search: (args) => {
      const {
        id,
      } = args;

      const whereClause = {};

      if (id) whereClause.id = id;

      return {
        where: whereClause,
      };
    },
    pages: (limit, offset) => ({
      limit,
      offset,
    }),
  };
}
