// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import ActivityCategoryColumns from '../columns/ActivityCategory.js';
import { db } from '../db';
import {
  ResourceNotFoundError,
} from '../errors/General';

export default class ActivityCategory extends Model<ModelDefaultAttributes, ActivityCategoryAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(ActivityCategory.fields, { sequelize, hooks: ActivityCategory.hooks });
  }

  static fields = ActivityCategoryColumns(DataTypes);

  static associate() {
    ActivityCategory.hasMany(db.models.Activity);
  }

  static async createCategory({
    name,
    desktopImg,
    mobileImg,
  }) {
    const category = await db.models.ActivityCategory.create({
      name,
      desktopImg,
      mobileImg,
    });

    if (!category) {
      return {
        error: 'Category create failed.',
      };
    }

    return category;
  }

  static async editCategory({
    id,
    name,
    desktopImg,
    mobileImg,
  }) {
    const category = await db.models.ActivityCategory.findOne({
      where: {
        id,
      },
    });

    if (!category) {
      throw new ResourceNotFoundError();
    }

    category.name = name;
    category.desktopImg = desktopImg;
    category.mobileImg = mobileImg;
    await category.save();

    return category;
  }

  static async deleteCategory({
    id,
  }) {
    const category = await db.models.ActivityCategory.findOne({
      where: {
        id,
      },
    });

    if (!category) {
      throw new ResourceNotFoundError();
    }

    await category.destroy();

    return {
      id: category.id,
      status: true,
      message: '刪除成功',
    };
  }

  static hooks = {
    afterDestroy: async (activityCategory) => {
      await db.models.Activity.destroy({
        where: {
          ActivityCategoryId: activityCategory.id,
        },
      });
    },
  }
}
