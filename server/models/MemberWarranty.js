// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import MemberWarrantyColumns from '../columns/MemberWarranty.js';
import { db } from '../db';

export default class MemberWarranty extends Model<ModelDefaultAttributes, MemberWarrantyAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(MemberWarranty.fields, { sequelize, scopes: MemberWarranty.scopes });
  }

  static fields = MemberWarrantyColumns(DataTypes);

  static associate() {
    MemberWarranty.belongsTo(db.models.Member);
    MemberWarranty.belongsTo(db.models.Member, { as: 'Approver' });
  }

  static scopes = {
    employee: () => ({
      include: [{
        model: db.models.Member,
        as: 'Approver',
      }],
    }),
    pages: (limit, offset) => ({
      limit,
      offset,
    }),
  };
}
