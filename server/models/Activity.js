// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import ActivityColumns from '../columns/Activity.js';
import { db } from '../db';
import {
  ResourceNotFoundError,
} from '../errors/General';

export default class Activity extends Model<ModelDefaultAttributes, ActivityAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(Activity.fields, {
      sequelize,
      scopes: Activity.scopes,
      hooks: Activity.hooks,
    });
  }

  static fields = ActivityColumns(DataTypes);

  static associate() {
    Activity.hasMany(db.models.ActivityDetailFragment, {
      as: 'fragments',
    });
    Activity.belongsTo(db.models.ActivityCategory);
  }

  static async addOrUpdateFragments(fragments, activityId, transaction) {
    if (activityId) {
      await db.models.ActivityDetailFragment.destroy({
        where: {
          ActivityId: activityId,
        },
        force: true,
        transaction,
      });

      if (fragments.length) {
        const fragmentWithSeq = fragments.map((f, idx) => ({
          ...f,
          seqNum: idx + 1,
          ActivityId: activityId,
        }));

        await db.models.ActivityDetailFragment.bulkCreate(fragmentWithSeq, { transaction });
      }
    }
  }

  static async createActivity({
    name,
    desktopImg,
    mobileImg,
    description,
    categoryId,
    fragments,
  }) {
    const transaction = await db.transaction();
    try {
      const activityPayload = {
        name,
        desktopImg,
        mobileImg,
        description: description || null,
        ActivityCategoryId: categoryId,
      };

      const activity = await db.models.Activity.create(activityPayload, { transaction });

      if (!activity) {
        return {
          error: 'Activity create failed.',
        };
      }

      if (fragments) {
        await db.models.Activity.addOrUpdateFragments(fragments, activity.id, transaction);
      }

      await transaction.commit();

      return activity;
    } catch (ex) {
      await transaction.rollback();

      throw ex;
    }
  }

  static async editActivity({
    id,
    name,
    desktopImg,
    mobileImg,
    description,
    fragments,
  }) {
    const activity = await db.models.Activity.findOne({
      where: {
        id,
      },
    });

    if (!activity) {
      throw new ResourceNotFoundError();
    }

    const transaction = await db.transaction();
    try {
      await activity.update({
        name,
        desktopImg,
        mobileImg,
        description,
      }, { transaction });

      if (fragments) {
        await db.models.Activity.addOrUpdateFragments(fragments, activity.id, transaction);
      }

      await transaction.commit();

      return activity;
    } catch (ex) {
      await transaction.rollback();

      throw ex;
    }
  }

  static async deleteActivity({
    id,
  }) {
    const activity = await db.models.Activity.findOne({
      where: {
        id,
      },
    });

    if (!activity) {
      throw new ResourceNotFoundError();
    }

    await activity.destroy();

    return {
      id: activity.id,
      status: true,
      message: '刪除成功',
    };
  }

  static scopes = {
    includeDetailFragments: () => ({
      include: [{
        model: db.models.ActivityDetailFragment,
        as: 'fragments',
      }],
    }),
  };

  static hooks = {
    afterDestroy: async (activity) => {
      await db.models.ActivityDetailFragment.destroy({
        where: {
          ActivityId: activity.id,
        },
      });
    },
  }
}
