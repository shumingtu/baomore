// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import SystemLogTypeColumns from '../columns/SystemLogType.js';
import { db } from '../db';

export default class SystemLogType extends Model<ModelDefaultAttributes, SystemLogTypeAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(SystemLogType.fields, { sequelize, scopes: SystemLogType.scopes });
  }

  static fields = SystemLogTypeColumns(DataTypes);

  static associate() {}

  static scopes = {
    search: args => {
      const {
        id,
      } = args;

      const whereClause = {};

      if (id) whereClause.id = id;

      return {
        where: whereClause,
      };
    },
  };
}
