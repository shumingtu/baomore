// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';

import EmployeeConfirmScheduleColumns from '../columns/EmployeeConfirmSchedule.js';
import { db } from '../db';

export default class EmployeeConfirmSchedule extends Model<ModelDefaultAttributes, OnlineOrderAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(EmployeeConfirmSchedule.fields, {
      sequelize,
    });
  }

  static fields = EmployeeConfirmScheduleColumns(DataTypes);

  static associate() {
    EmployeeConfirmSchedule.belongsTo(db.models.OnlineOrder);
  }
}
