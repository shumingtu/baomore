// @flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import RoleColumns from '../columns/Role';
import { db } from '../db';

export default class Role extends Model<ModelDefaultAttributes, MemberAttributes> {
  static init(sequelize: Sequelize) {
    return super.init(Role.fields, { sequelize, scopes: Role.scopes });
  }

  static fields = RoleColumns(DataTypes)

  static associate() {
    Role.belongsToMany(db.models.Action, {
      through: 'RoleActions',
      foreignKey: 'RoleId',
      otherKey: 'ActionCode',
    });
    Role.belongsToMany(db.models.Member, {
      through: 'MemberRoles',
      foreignKey: 'RoleId',
      otherKey: 'MemberId',
    });
  }

  static scopes = {
    search: (args) => {
      const {
        roleId,
      } = args;

      const whereClause = {};

      if (roleId) whereClause.id = roleId;

      return {
        where: whereClause,
      };
    },
    pages: (limit, offset) => ({
      limit,
      offset,
    }),
    action: () => ({
      include: [{
        model: db.models.Action,
      }],
    }),
  }

  static get() {
    const cachedRoles = {};

    return (role) => {
      if (cachedRoles[role.id]) {
        return cachedRoles[role.id];
      }

      return Role.findOne({
        where: {
          id: role.id,
        },
      });
    };
  }
}
