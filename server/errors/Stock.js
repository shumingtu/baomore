// @flow

const ErrorClass = Error;

export class StockOrderGroupNotFoundError extends ErrorClass {
  code = 600

  message = this.message || '此商品已點入庫，請關閉後確認欲執行動作'

  human = '找不到此點貨編號的結單紀錄，請再次確認'
}

export class StockNotFoundError extends ErrorClass {
  code = 601

  message = this.message || 'Member Stock Record Not Found Error'

  human = '找不到此會員的庫存紀錄，請再次確認'
}

export class StockOverflowError extends ErrorClass {
  code = 602

  message = this.message || '庫存不足無法執行動作，請再次確認'

  human = '庫存不足無法執行動作，請再次確認'
}

export class SalesRecordNotFoundError extends ErrorClass {
  code = 603

  message = this.message || 'Sales Record Not Found Error'

  human = '查無銷貨紀錄，請重新銷貨'
}

export class QRCodeHasBeenRollBackedOrSoldError extends ErrorClass {
  code = 604

  message = this.message || '此商品已被退貨或銷貨調貨！'

  human = '此商品已被退貨或銷貨調貨！'
}


export class QRCodeHasBeenTransferedError extends ErrorClass {
  code = 605

  message = this.message || '此商品已被調貨或退貨銷貨！'

  human = '此商品已被調貨或退貨銷貨！'
}

export class PrductTypeError extends ErrorClass {
  code = 606

  message = this.message || '料號與產品類型不符！'

  human = '料號與產品類型不符！'
}

export default null;
