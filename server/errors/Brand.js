// @flow

const ErrorClass = Error;

export class BrandNotFoundError extends ErrorClass {
  code = 300

  message = this.message || 'Brand Not Found'

  human = '找不到該品牌編號'
}

export class BrandModelNotFoundError extends ErrorClass {
  code = 301

  message = this.message || 'BrandModel Not Found'

  human = '找不到該型號編號'
}

export class BrandModelColorNotFoundError extends ErrorClass {
  code = 302

  message = this.message || 'BrandModelColor Or Model Not Found'

  human = '找不到該顏色編號或品牌編號'
}
