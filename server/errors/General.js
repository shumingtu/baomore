// @flow

export class PermissionError extends Error {
  code = 100

  message = this.message || 'Permission Error';
}

export class InvalidAccessToken extends Error {
  code = 101

  message = this.message || 'Invalid Access Token';
}

export class InvalidRefreshToken extends Error {
  code = 102

  message = this.message || 'Invalid Refresh Token';
}

export class MemberNotFoundError extends Error {
  code = 103

  message = this.message || 'Member Not Found Error';
}

export class SystemError extends Error {
  code = 104

  message = this.message || 'System Error';
}

export class InvalidParameter extends Error {
  code = 105

  message = this.message || 'Invalid Parameter';
}

export class ResourceNotFoundError extends Error {
  code = 106

  message = this.message || 'Resource Not Found';
}

export class DuplicatedLineIdError extends Error {
  code = 107

  message = this.message || 'DuplicatedLineIdError';
}
