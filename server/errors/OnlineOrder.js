// @flow

const ErrorClass = Error;

export class OnlineOrderNotFoundError extends ErrorClass {
  code = 1000

  message = this.message || 'OnlineOrder Not Found'

  human = '找不到該線上訂單'
}

export class OnlineOrderConfirmedError extends ErrorClass {
  code = 1001

  message = this.message || '該線上訂單已被確認'

  human = '該線上訂單已被確認'
}

export default null;
