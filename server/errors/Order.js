export class OrderUpdateFailError extends Error {
  code = 2001

  message = this.message || '訂單更新失敗, 請確認訂單狀態！'

  human = '訂單更新失敗, 請確認訂單狀態！'
}

export class OrderCreateFailed extends Error {
  code = 2002

  message = this.message || '訂單新增失敗, 請再試一次！'

  human = '訂單新增失敗, 請再試一次！'
}


export default null;
