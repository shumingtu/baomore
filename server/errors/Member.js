// @flow

const ErrorClass = Error;

export class PermissionError extends ErrorClass {
  code = 200

  message = this.message || 'Permission Denied'

  human = '沒有權限，請再次確認'
}

export class AccountNotFoundError extends ErrorClass {
  code = 201

  message = this.message || 'Account Not Found'

  human = '帳號或密碼錯誤，請再次確認'
}

export class PasswordNotMatchedError extends ErrorClass {
  code = 202

  message = this.message || 'Password Not Matched'

  human = '帳號或密碼錯誤，請再次確認'
}

export class InvalidRefreshToken extends ErrorClass {
  code = 203

  message = this.message || 'Invalid Refresh Token Error'
}

export class DuplicateAccountError extends ErrorClass {
  code = 204

  message = this.message || 'Duplicate Account'

  human = '已有相同帳號存在，請重新註冊'
}

export class TokenExpiredError extends ErrorClass {
  code = 205

  message = this.message || 'Token Expired Error'
}

export class TokenInvalidError extends ErrorClass {
  code = 206

  message = this.message || 'Token Invalid Error'
}

export class MemberLINELoginError extends ErrorClass {
  code = 207

  message = this.message || 'LINE Login Error Or Code Expired'

  human = 'LINE Login 已失效請重新登入'
}

export class MemberNotConnectLINEError extends ErrorClass {
  code = 208

  message = this.message || 'Member LineId Is Not Found'

  human = '此會員尚未與 LINE 連結，請再次確認'
}

export class MemberCreateFaild extends ErrorClass {
  code = 209

  message = this.message || 'Member Create Failed'

  human = '會員新增失敗'
}
