// @flow

const ErrorClass = Error;

export class RichmenuFetchError extends ErrorClass {
  code = 700

  message = this.message || 'LINE Richmenus Fetch Error'

  human = '無法取得 LINE BOT Richmenu 清單'
}

export class RichmenuNotFoundError extends ErrorClass {
  code = 701

  message = this.message || 'Richmenu Not Found Error'

  human = '目前無可使用的 richmenus'
}

export class SetRichmenuError extends ErrorClass {
  code = 702

  message = this.message || 'Set Richmenu Error'

  human = '設定指定 richmenu 發生錯誤'
}

export default null;
