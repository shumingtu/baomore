// @flow

const ErrorClass = Error;

export class PayConfirmError extends ErrorClass {
  code = 500

  message = this.message || 'LINE Pay Confirm API Error'

  human = 'LINE Pay 確認付款失敗，請再嘗試一次'
}

export class CreateOnlineOrderError extends ErrorClass {
  code = 501

  message = this.message || 'Created Online Order Error'

  human = '訂單成立失敗，請再次確認'
}

export class OnlineOrderNotFoundError extends ErrorClass {
  code = 501

  message = this.message || 'Online Order Not Found'

  human = '查無此訂單'
}
