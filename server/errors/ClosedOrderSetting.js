// @flow

const ErrorClass = Error;

export class ClosedOrderSettingNotFoundError extends ErrorClass {
  code = 800

  message = this.message || 'ClosedOrderSetting Not Found'

  human = '找不到該結單設定'
}

export class ClosedOrderSettingCreateError extends ErrorClass {
  code = 801

  message = this.message || 'ClosedOrderSetting Create Failed'

  human = '結單設定新增失敗'
}

export class ClosedOrderSettingIsAlreadyExist extends ErrorClass {
  code = 802

  message = this.message || '此料號已有設定結單時間'

  human = 'ClosedOrderSetting Already Exist'
}

export default null;
