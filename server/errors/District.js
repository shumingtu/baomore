const ErrorClass = Error;

export class DistrictNotFoundError extends ErrorClass {
  code = 300

  message = this.message || 'District Not Found'

  human = '找不到區域'
}

export default null;
