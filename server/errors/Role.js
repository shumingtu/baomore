// @flow

const ErrorClass = Error;

export class RoleNotFoundError extends ErrorClass {
  code = 300

  message = this.message || 'Role Not Found'

  human = '找不到該角色'
}

export default null;
