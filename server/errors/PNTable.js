// @flow

const ErrorClass = Error;

export class PNTableItemNotFoundError extends ErrorClass {
  code = 600

  message = this.message || 'PNTable Item Not Found'

  human = '找不到該料號'
}

export class PNTableCreateError extends ErrorClass {
  code = 601

  message = this.message || 'PNTable Create Failed'

  human = '新增料號失敗'
}

export default null;
