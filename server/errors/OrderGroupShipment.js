// @flow

const ErrorClass = Error;

export class OrderGroupShipmentCreateError extends ErrorClass {
  code = 900

  message = this.message || 'OrderGroupShipment Created Failed'

  human = '結單新增失敗'
}

export class OrderUpdateGroupShipmentError extends ErrorClass {
  code = 901

  message = this.message || 'OrderGroupShipmentId Updated Failed'

  human = '訂單新增結單關聯失敗'
}

export default null;
