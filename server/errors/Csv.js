const ErrorClass = Error;

export class CsvImportError extends ErrorClass {
  code = 300

  message = this.message || 'Csv Import Failed'

  human = 'CSV匯入失敗'
}

export default null;
