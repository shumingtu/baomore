const ErrorClass = Error;

export class VendorCreateFailed extends ErrorClass {
  code = 300

  message = this.message || 'Vendor Create Failed'

  human = '廠商新增失敗'
}

export class VendorNotFound extends ErrorClass {
  code = 301

  message = this.message || 'Vendor Not Found'

  human = '找不到廠商'
}

export default null;
