// @flow

const ErrorClass = Error;

export class PNSubCategoryNotFoundError extends ErrorClass {
  code = 700

  message = this.message || 'PNSubCategory Not Found'

  human = '找不到該副類型'
}

export class PNSubCategoryCreateError extends ErrorClass {
  code = 701

  message = this.message || 'PNSubCategory Create Failed'

  human = '副類型新增失敗'
}

export default null;
