export const PERMISSION_ERROR = (ctx) => {
  ctx.status = 403;
  ctx.body = {
    message: '沒有權限',
  };
};

export const PARAMETER_ERROR = (ctx) => {
  ctx.status = 403;
  ctx.body = {
    message: '參數錯誤',
  };
};

export const SERVER_ERROR = (ctx) => {
  ctx.status = 500;
  ctx.body = {
    message: '伺服器錯誤',
  };
};
