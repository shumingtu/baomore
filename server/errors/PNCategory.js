// @flow

const ErrorClass = Error;

export class PNCategoryNotFoundError extends ErrorClass {
  code = 400

  message = this.message || 'PNCategory Not Found'

  human = '找不到該料號類型'
}

export class PNCategoryCreateError extends ErrorClass {
  code = 401

  message = this.message || 'PNCategory Create Failed'

  human = '類型新增失敗'
}

export default null;
