const ErrorClass = Error;

export class StoreCreateFailed extends ErrorClass {
  code = 300

  message = this.message || 'Store Create Failed'

  human = '門市新增失敗'
}

export class MemberStoreCreateFailed extends ErrorClass {
  code = 301

  message = this.message || 'MemberStore Create Failed'

  human = 'MemberStore Create Failed'
}

export class StoreNotFound extends ErrorClass {
  code = 302

  message = this.message || 'Store Not Found'

  human = '找不到該門市'
}

export class MemberStoreNotFound extends ErrorClass {
  code = 303

  message = this.message || 'MemberStore Not Found'

  human = 'MemberStore Not Found'
}

export class StoreAddressError extends ErrorClass {
  code = 304

  message = this.message || '門市地址錯誤'

  human = '門市地址錯誤'
}


export default null;
