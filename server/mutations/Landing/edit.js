import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import {
  listType,
} from '../../types/Landing';

export default {
  type: new GraphQLNonNull(listType),
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    desktopImg: {
      type: new GraphQLNonNull(GraphQLString),
    },
    mobileImg: {
      type: new GraphQLNonNull(GraphQLString),
    },
    link: {
      type: GraphQLString,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_WEBSITE_MANAGE,
  })(async (_, args) => db.models.Landing.editLanding(args)),
};
