import {
  GraphQLNonNull,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import {
  landingDeleteResponseType,
} from '../../types/Landing';

export default {
  type: new GraphQLNonNull(landingDeleteResponseType),
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_WEBSITE_MANAGE,
  })(async (_, args) => db.models.Landing.deleteLanding(args)),
};
