// @flow

import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';

import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { onlineOrderType } from '../../types/OnlineOrder';
import {
  PermissionError,
  ResourceNotFoundError,
} from '../../errors/General';
import {
  OnlineOrderNotFoundError,
} from '../../errors/OnlineOrder';
import { sendLineMessage, sendLineMessageToClient } from '../../helpers/line/linebot';

export default {
  type: new GraphQLNonNull(onlineOrderType),
  args: {
    onlineOrderId: {
      type: new GraphQLNonNull(GraphQLString),
    },
    date: {
      type: new GraphQLNonNull(GraphQLString),
    },
    time: {
      type: new GraphQLNonNull(GraphQLString),
    },
    employeeId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    storeId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ONLINE_ORDER_MANAGE,
  })(async (n: null, {
    onlineOrderId,
    date,
    time,
    employeeId,
    storeId,
  }, { authPayload }) => {
    const {
      id,
      name,
    } = authPayload;

    const onlineOrderScope = await db.models.OnlineOrder.scope(
      { method: ['search', { orderId: onlineOrderId }] },
      'store',
      'bookedMember',
      'member',
    );
    const onlineOrder = await onlineOrderScope.find();

    const store = await db.models.Store.scope({ method: ['search', { storeId }] }).findOne();
    const bookTimeSlot = await db.models.BookTimeSlot.scope({ method: ['search', { time }] }).findOne();

    if (!onlineOrder) throw new OnlineOrderNotFoundError();
    if (!bookTimeSlot || !store) throw new ResourceNotFoundError();

    await onlineOrderScope.update({
      date,
      time,
      BookTimeSlotGroupId: bookTimeSlot.BookTimeSlotGroupId,
      BookedMemberId: employeeId,
      StoreId: store.id,
    });

    const updatedOnlineOrder = await onlineOrderScope.find();

    const director = await updatedOnlineOrder.BookedMember.getMember();

    if (director && director.lineId) {
      await sendLineMessage(
        null,
        `【包膜預約行程變更通知】\n線上訂單編號：${updatedOnlineOrder.id}\n訂購人姓名：${updatedOnlineOrder.Owner.name}\n訂購人電話：${updatedOnlineOrder.Owner.phone}\n預約時間：${updatedOnlineOrder.date} ${updatedOnlineOrder.time}\n預約門市：${updatedOnlineOrder.Store.Channel.name}${updatedOnlineOrder.Store.name}`,
        director.lineId,
      );
    }

    if (updatedOnlineOrder.Owner.lineId) {
      if (
        updatedOnlineOrder.date !== onlineOrder.date
        || updatedOnlineOrder.time !== onlineOrder.time
        || updatedOnlineOrder.Store.id !== onlineOrder.Store.id
      ) {
        // 如果只更動包膜師不用通知客戶
        await sendLineMessageToClient(
          `【包膜預約行程變更通知】\n訂單編號：${updatedOnlineOrder.id}\n預約時間：${updatedOnlineOrder.date} ${updatedOnlineOrder.time}\n預約門市：${updatedOnlineOrder.Store.Channel.name}${updatedOnlineOrder.Store.name}\n訂單詳細資訊請至官網會員中心 > 訂單查詢\n訂單查詢傳送門：\nhttps://baomore.rytass.com/member/order\n如有任何問題請與 artmo 線上小幫手聯繫`,
          updatedOnlineOrder.Owner.lineId,
        );
      }
    }


    if (updatedOnlineOrder.BookedMember.lineId) {
      await sendLineMessage(
        null,
        `【包膜預約行程變更通知】\n線上訂單編號：${updatedOnlineOrder.id}\n訂購人姓名：${updatedOnlineOrder.Owner.name}\n訂購人電話：${updatedOnlineOrder.Owner.phone}\n預約時間：${updatedOnlineOrder.date} ${updatedOnlineOrder.time}\n預約門市：${updatedOnlineOrder.Store.Channel.name}${updatedOnlineOrder.Store.name}`,
        updatedOnlineOrder.BookedMember.lineId,
      );
    }

    // record
    db.models.SystemLog.createLog(1, id, `${name} 修改訂單的包膜師時間至: ${updatedOnlineOrder.date} ${updatedOnlineOrder.time}, 門市: ${updatedOnlineOrder.Store.name}, 包膜師: ${updatedOnlineOrder.BookedMember.name}`);

    // send notification

    return updatedOnlineOrder;
  }),
};
