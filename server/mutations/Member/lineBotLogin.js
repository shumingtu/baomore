// @flow

import {
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';
import debug from 'debug';
import { db } from '../../db';
import { employeeLoginType } from '../../types/Member.js';
import { employeeLogin } from '../../helpers/line/lineRouter';
import {
  MemberNotFoundError,
} from '../../errors/General';

const debugLineBotLogin = debug('BAOMORE:GrqphQLLineBotLogin');

export default {
  type: new GraphQLNonNull(employeeLoginType),
  args: {
    code: {
      type: new GraphQLNonNull(GraphQLString),
    },
    SN: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phone: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: async (n: null, {
    code,
    SN,
    phone,
  }) => {
    const employee = await db.models.Member.findOne({
      where: {
        serialNumber: SN,
        phone,
        archive: false,
      },
    });

    if (!employee) throw new MemberNotFoundError();

    try {
      const result = await employeeLogin(employee, code);
      return result;
    } catch (e) {
      debugLineBotLogin(e);
      return e;
    }
  },
};
