// @flow

import {
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { onlineOrderType } from '../../types/OnlineOrder';
import {
  PermissionError,
} from '../../errors/General';
import {
  OnlineOrderNotFoundError,
} from '../../errors/OnlineOrder';

export default {
  type: new GraphQLNonNull(onlineOrderType),
  args: {
    onlineOrderId: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ONLINE_ORDER_MANAGE,
  })(async (n: null, {
    onlineOrderId,
  }, { authPayload }) => {
    const {
      id,
      name,
    } = authPayload;

    const onlineOrderScope = await db.models.OnlineOrder.scope(
      { method: ['search', { orderId: onlineOrderId }] },
      'store',
      'bookedMember',
      'member',
    );
    const onlineOrder = await onlineOrderScope.find();

    if (!onlineOrder) throw new OnlineOrderNotFoundError();

    await onlineOrder.destroy();

    // record
    db.models.SystemLog.createLog(1, id, `${name} 刪除了訂單，訂單時間: ${onlineOrder.date} ${onlineOrder.time}, 門市: ${onlineOrder.Store.name}, 包膜師: ${onlineOrder.BookedMember.name}`);

    // send notification

    return onlineOrder;
  }),
};
