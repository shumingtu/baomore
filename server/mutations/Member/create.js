import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import {
  PermissionError,
  MemberCreateFaild,
} from '../../errors/Member';
import { DuplicatedLineIdError } from '../../errors/General.js';
import {
  memberType,
} from '../../types/Member';


export default {
  type: new GraphQLNonNull(memberType),
  args: {
    serialNumber: {
      type: GraphQLString,
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    email: {
      type: GraphQLString,
    },
    gender: {
      type: GraphQLString,
    },
    birthday: {
      type: GraphQLString,
    },
    phone: {
      type: GraphQLString,
    },
    areaId: {
      type: GraphQLInt,
    },
    directorId: {
      type: GraphQLInt,
    },
    lineAccount: {
      type: GraphQLString,
    },
    roleIds: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLInt)),
    },
    lineId: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_MEMBER_MANAGE,
  })(async (_, args) => {
    const {
      serialNumber,
      name,
      email,
      gender,
      birthday,
      phone,
      areaId,
      directorId,
      roleIds,
      lineId,
      lineAccount,
    } = args;

    const existMember = await db.models.Member.findOne({
      where: {
        lineId,
      },
    });

    if (existMember) throw new DuplicatedLineIdError();

    const createdMember = await db.models.Member.create({
      serialNumber: serialNumber || null,
      name,
      email: email || null,
      gender: gender || null,
      birthday: birthday || null,
      phone: phone || null,
      AreaId: areaId || null,
      MemberId: directorId || null,
      lineId,
      lineAccount: lineAccount || null,
    });

    if (!createdMember) throw new MemberCreateFaild();

    await createdMember.addRoles(roleIds);

    const memberRoles = await createdMember.getRoles();

    return {
      ...createdMember.dataValues,
      Roles: memberRoles,
    };
  }),
};
