// @flow

import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db, MemberRole } from '../../db';
import { actions } from '../../../shared/roleActions';
import { memberMeType, memberType, memberToggleArchiveResponseType } from '../../types/Member.js';
import {
  PermissionError,
  MemberNotFoundError,
  DuplicatedLineIdError,
} from '../../errors/General';

export const toggleMemberArchive = {
  type: memberToggleArchiveResponseType,
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_MEMBER_MANAGE,
  })(async (_, args) => {
    const {
      id,
    } = args;

    const target = await db.models.Member.findOne({
      where: {
        id,
      },
    });

    if (!target) throw new MemberNotFoundError();

    await target.update({
      archive: !target.archive,
    });

    return {
      id,
      message: '成功',
      status: true,
    };
  }),
};

export const editMe = {
  type: new GraphQLNonNull(memberMeType),
  args: {
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    email: {
      type: GraphQLString,
    },
    gender: {
      type: GraphQLString,
    },
    birthday: {
      type: GraphQLString,
    },
    phone: {
      type: GraphQLString,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.NORMAL_MEMBER,
  })(async (n: null, {
    name,
    email,
    gender,
    birthday,
    phone,
  }, { authPayload }) => {
    const {
      id,
    } = authPayload;

    const member = await db.models.Member.getMe(id);

    if (!member) throw new MemberNotFoundError();

    if (name) member.name = name;
    if (email) member.email = email;
    if (gender) member.gender = gender;
    if (birthday) member.birthday = birthday;
    if (phone) member.phone = phone;

    await member.save();

    return member;
  }),
};

export const editMemberForAdmin = {
  type: new GraphQLNonNull(memberType),
  args: {
    memberId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    serialNumber: {
      type: GraphQLString,
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    email: {
      type: GraphQLString,
    },
    gender: {
      type: GraphQLString,
    },
    birthday: {
      type: GraphQLString,
    },
    phone: {
      type: GraphQLString,
    },
    areaId: {
      type: GraphQLInt,
    },
    directorId: {
      type: GraphQLInt,
    },
    lineAccount: {
      type: GraphQLString,
    },
    roleIds: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLInt)),
    },
    lineId: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_MEMBER_MANAGE,
  })(async (n: null, {
    memberId,
    serialNumber,
    name,
    email,
    gender,
    birthday,
    phone,
    areaId,
    directorId,
    roleIds,
    lineId,
    lineAccount,
  }) => {
    const targetMember = await db.models.Member.getMe(memberId);

    if (!targetMember) throw new MemberNotFoundError();

    const existMember = await db.models.Member.findOne({
      where: {
        lineId,
      },
    });

    if (existMember && existMember.id !== targetMember.id) throw new DuplicatedLineIdError();

    await targetMember.update({
      serialNumber: serialNumber || null,
      name,
      email: email || null,
      gender: gender || null,
      birthday: birthday || null,
      phone: phone || null,
      AreaId: areaId || null,
      MemberId: directorId || null,
      lineId,
      lineAccount: lineAccount || null,
    });

    await MemberRole.destroy({
      where: {
        MemberId: targetMember.id,
      },
    });

    await targetMember.addRoles(roleIds);

    return {
      ...targetMember.dataValues,
      Roles: await targetMember.getRoles(),
      Area: await targetMember.getArea(),
      director: await targetMember.getMember(),
    };
  }),
};

export const editMemberForConsumer = {
  type: new GraphQLNonNull(memberType),
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    lineId: {
      type: new GraphQLNonNull(GraphQLString),
    },
    birthday: {
      type: new GraphQLNonNull(GraphQLString),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phone: {
      type: new GraphQLNonNull(GraphQLString),
    },
    email: {
      type: new GraphQLNonNull(GraphQLString),
    },
    gender: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_MEMBER_MANAGE,
  })(async (n: null, {
    id,
    lineId,
    birthday,
    name,
    phone,
    email,
    gender,
  }) => {
    const targetMember = await db.models.Member.scope(
      'warranty',
      'onlineOrderStore',
    ).findOne({
      where: {
        id,
      },
    });

    if (!targetMember) throw new MemberNotFoundError();

    await targetMember.update({
      lineId,
      birthday,
      name,
      phone,
      email,
      gender,
    });

    return targetMember;
  }),
};
