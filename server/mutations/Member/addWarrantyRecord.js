// @flow

import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import moment from 'moment';

import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { memberWarrantyType } from '../../types/Warranty.js';
import {
  PermissionError,
  InvalidParameter,
} from '../../errors/General';
import { sendWarrantyInfoToEmployee } from '../../helpers/line/linebot.js';

export default {
  type: new GraphQLNonNull(memberWarrantyType),
  args: {
    service: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phoneModel: {
      type: new GraphQLNonNull(GraphQLString),
    },
    store: {
      type: new GraphQLNonNull(GraphQLString),
    },
    VAT: {
      type: new GraphQLNonNull(GraphQLString),
    },
    IME: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phone: {
      type: new GraphQLNonNull(GraphQLString),
    },
    EmployeeMemberId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.NORMAL_MEMBER,
  })(async (n: null, {
    service,
    phoneModel,
    store,
    VAT,
    IME,
    phone,
    EmployeeMemberId,
  }, { authPayload }) => {
    const {
      id,
    } = authPayload;

    const employee = await db.models.Member.getMe(EmployeeMemberId);

    const customer = await db.models.Member.getMe(id);

    if (!employee || !customer) {
      throw new InvalidParameter('Invalid Parameter: employee id || authPayload id');
    }

    const record = await db.models.MemberWarranty.create({
      service,
      phoneModel,
      store,
      VAT,
      IME,
      phone,
      MemberId: id,
    });

    await sendWarrantyInfoToEmployee(
      employee,
      `【Artmo 產品售後服務卡】\n消費時間：${moment().format('YYYY-MM-DD HH:mm:ss')}\n顧客名稱：${customer.name}\n服務項目：${service}\n手機型號：${phoneModel}\nIMEI後五碼：${IME}\n統一發票：${VAT}\n消費門市：${store}`,
      `command=warrantyConfirm&recordId=${record.id}&employeeId=${employee.id}`,
      `command=warrantyCancel&recordId=${record.id}`,
    );

    return {
      ...record.toJSON(),
    };
  }),
};
