import {
  GraphQLNonNull,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import {
  memberDeleteResponseType,
} from '../../types/Member';
import {
  MemberNotFoundError,
} from '../../errors/General';


export default {
  type: new GraphQLNonNull(memberDeleteResponseType),
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_MEMBER_MANAGE,
  })(async (_, args) => {
    const targetMember = await db.models.Member.findOne({
      attributes: ['id'],
      where: {
        id: args.id,
      },
    });

    if (!targetMember) throw new MemberNotFoundError();

    await targetMember.destroy();

    return {
      id: targetMember.id,
      status: true,
      message: '刪除成功',
    };
  }),
};
