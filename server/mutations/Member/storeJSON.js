// @flow

import {
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { memberMeType } from '../../types/Member.js';
import {
  PermissionError,
  MemberNotFoundError,
} from '../../errors/General';

export default {
  type: new GraphQLNonNull(memberMeType),
  args: {
    json: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.NORMAL_MEMBER,
  })(async (n: null, { json }, { authPayload }) => {
    const {
      id,
    } = authPayload;

    const member = await db.models.Member.getMe(id);

    if (!member) throw new MemberNotFoundError();

    member.storedDesignJSON = json;
    await member.save();

    return member;
  }),
};
