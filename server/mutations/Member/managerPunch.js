// @flow

import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLFloat,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { managerPunchResponseType } from '../../types/Member.js';
import {
  PermissionError,
  ResourceNotFoundError,
} from '../../errors/General';

export default {
  type: new GraphQLNonNull(managerPunchResponseType),
  args: {
    punchStartTime: {
      type: new GraphQLNonNull(GraphQLString),
    },
    punchEndTime: {
      type: new GraphQLNonNull(GraphQLString),
    },
    grade: {
      type: new GraphQLNonNull(GraphQLFloat),
    },
    remark: {
      type: GraphQLString,
    },
    employeeId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    storeId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: [actions.CHECKIN, actions.MANAGE_EMPLOYEE_MANAGER],
  })(async (n: null, {
    punchStartTime,
    punchEndTime,
    grade,
    remark,
    employeeId,
    storeId,
  }, { authPayload }) => {
    const {
      id,
    } = authPayload;

    const store = await db.models.Store.scope({ method: ['search', { id: storeId }] }).findOne();

    if (!store) throw new ResourceNotFoundError('Store Not Found Error');

    // check relation between id and employeeId

    const record = await db.models.ManagerPunchRecord.create({
      punchStartTime,
      punchEndTime,
      grade,
      remark,
      EmployeedMemberId: employeeId,
      MemberId: id,
      StoreId: storeId,
    });

    return {
      id: record.id,
      status: true,
      message: '巡檢打卡完成！',
    };
  }),
};
