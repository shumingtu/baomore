// @flow

import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLFloat,
  GraphQLInt,
} from 'graphql';
import moment from 'moment';
import { Op } from 'sequelize';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { employeePunchResponseType } from '../../types/Member.js';
import {
  PermissionError,
  ResourceNotFoundError,
} from '../../errors/General';

export default {
  type: new GraphQLNonNull(employeePunchResponseType),
  args: {
    punchTime: {
      type: new GraphQLNonNull(GraphQLString),
    },
    latitude: {
      type: new GraphQLNonNull(GraphQLFloat),
    },
    longitude: {
      type: new GraphQLNonNull(GraphQLFloat),
    },
    storeId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.CHECKIN,
  })(async (n: null, {
    punchTime,
    latitude,
    longitude,
    storeId,
  }, { authPayload }) => {
    const {
      id,
    } = authPayload;

    const store = await db.models.Store.scope({ method: ['search', { id: storeId }] }).findOne();

    if (!store) throw new ResourceNotFoundError('Store Not Found Error');

    const startOfTime = moment(punchTime).startOf('day').toDate();
    const endOfTime = moment(punchTime).endOf('day').toDate();

    // 0 => 上班, 1 => 下班 2=> 上下班已打過卡
    const existRecordCount = await db.models.MemberPunchRecord.count({
      where: {
        punchTime: {
          [Op.gt]: startOfTime,
          [Op.lt]: endOfTime,
        },
        MemberId: id,
      },
    });

    if (existRecordCount === 2) throw new Error('已打過上下班卡！');

    const record = await db.models.MemberPunchRecord.create({
      punchTime,
      latitude,
      longitude,
      type: existRecordCount ? 'OFF' : 'ON',
      MemberId: id,
      StoreId: storeId,
    });

    return {
      id: record.id,
      status: true,
      message: '打卡完成！',
    };
  }),
};
