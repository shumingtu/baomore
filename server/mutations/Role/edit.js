import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';

import { db, RoleAction } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError, ResourceNotFoundError } from '../../errors/General';
import { roleType } from '../../types/Role.js';

export default {
  type: new GraphQLNonNull(roleType),
  args: {
    roleId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    actionCodes: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLInt)),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_MEMBER_MANAGE,
  })(async (_, args) => {
    const {
      roleId,
      name,
      actionCodes,
    } = args;

    const targetRole = await db.models.Role.findOne({
      where: {
        id: roleId,
      },
    });

    if (!targetRole) throw new ResourceNotFoundError();

    const transaction = await db.transaction();

    try {
      await targetRole.update({
        name,
      }, { transaction });

      await RoleAction.destroy({
        where: {
          RoleId: targetRole.id,
        },
        transaction,
      });

      if (actionCodes && actionCodes.length) await targetRole.addActions(actionCodes, { transaction });

      await transaction.commit();

      const newActions = await targetRole.getActions();

      return {
        ...targetRole.dataValues,
        Actions: newActions,
      };
    } catch (e) {
      await transaction.rollback();

      throw e;
    }
  }),
};
