import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLBoolean,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import {
  PNTableType,
} from '../../types/PNTable';

export default {
  type: new GraphQLNonNull(PNTableType),
  args: {
    code: {
      type: new GraphQLNonNull(GraphQLString),
    },
    pnCategoryId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    bigSubCategoryId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    smallSubCategoryId: {
      type: GraphQLInt,
    },
    BrandModelId: {
      type: GraphQLInt,
    },
    VendorId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    picture: {
      type: new GraphQLNonNull(GraphQLString),
    },
    price: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    device: {
      type: new GraphQLNonNull(GraphQLString),
    },
    description: {
      type: new GraphQLNonNull(GraphQLString),
    },
    isOnSale: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    onlinePrice: {
      type: GraphQLInt,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.PNTABLE_MANAGE,
  })(async (_, args) => db.models.PNTable.createPNTableItem(args)),
};
