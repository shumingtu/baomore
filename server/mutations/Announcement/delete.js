import {
  GraphQLNonNull,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import {
  announcementDeleteResponseType,
} from '../../types/Announcement';

export default {
  type: announcementDeleteResponseType,
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_NOTIFICATION_MANAGE,
  })(async (_, args) => db.models.Announcement.deleteAnnouncement(args)),
};
