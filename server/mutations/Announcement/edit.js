import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import {
  announcementType,
} from '../../types/Announcement';

export default {
  type: new GraphQLNonNull(announcementType),
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    title: {
      type: new GraphQLNonNull(GraphQLString),
    },
    content: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_NOTIFICATION_MANAGE,
  })(async (_, args) => db.models.Announcement.editAnnouncement(args)),
};
