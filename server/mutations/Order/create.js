import {
  GraphQLNonNull,
  GraphQLInt,
  GraphQLString,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import { ResourceNotFoundError } from '../../errors/General';
import { OrderCreateFailed } from '../../errors/Order';
import {
  orderType,
} from '../../types/Order';

export const createOrder = {
  type: new GraphQLNonNull(orderType),
  args: {
    employeeId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    code: {
      type: new GraphQLNonNull(GraphQLString),
    },
    quantity: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    productType: {
      type: new GraphQLNonNull(GraphQLString),
    },
    storeId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.INVOICING_MANAGE,
  })(async (_, args) => {
    const {
      employeeId,
      code,
      quantity,
      productType,
      storeId,
    } = args;

    const targetPNTable = await db.models.PNTable.findOne({
      where: {
        code,
      },
    });

    if (!targetPNTable) throw new ResourceNotFoundError();

    const createdOrder = await db.models.Order.create({
      EmpolyedMemberId: employeeId,
      quantity,
      ProductType: productType,
      StoreId: storeId,
      picture: targetPNTable.picture,
      PNTableId: targetPNTable.id,
      orderSource: 'EMPOLYEE',
    });

    if (!createdOrder) throw new OrderCreateFailed();

    const store = await createdOrder.getStore({
      include: [{
        model: db.models.Channel,
      }, {
        model: db.models.District,
      }],
    });

    const member = await createdOrder.getMember({
      include: [{
        model: db.models.Area,
      }],
    });

    const pnTable = await createdOrder.getPNTable({
      scope: ('brandAndModel', 'categories'),
    });

    return {
      ...createdOrder.dataValues,
      Store: store,
      Member: member,
      PNTable: pnTable,
    };
  }),
};


export default null;
