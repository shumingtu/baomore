import {
  GraphQLNonNull,
  GraphQLInt,
  GraphQLString,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import moment from 'moment';
import { Op } from 'sequelize';

import { db } from '../../db';
import onlineQRCodeGenerator from '../../helpers/onlineQRCodeGenerator.js';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import { ResourceNotFoundError } from '../../errors/General';
import { OrderUpdateFailError } from '../../errors/Order';
import { OnlineOrderNotFoundError, OnlineOrderConfirmedError } from '../../errors/OnlineOrder.js';
import padDigits from '../../helpers/padDigits.js';
import createInvoice from '../../helpers/invoice.js';
import {
  orderType,
} from '../../types/Order';
import {
  onlineOrderType,
} from '../../types/OnlineOrder';
import {
  sendLineMessageToClient,
  sendLineMessage,
} from '../../helpers/line/linebot.js';

export const assignOrderToEmployee = {
  type: new GraphQLNonNull(onlineOrderType),
  args: {
    onlineOrderId: {
      type: new GraphQLNonNull(GraphQLString),
    },
    storeId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    date: {
      type: new GraphQLNonNull(GraphQLString),
    },
    time: {
      type: new GraphQLNonNull(GraphQLString),
    },
    employeeId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.MANAGE_EMPLOYEE_MANAGER,
  })(async (_, args) => {
    const {
      onlineOrderId,
      storeId,
      date,
      time,
      employeeId,
    } = args;

    const targetOnlineOrderScope = db.models.OnlineOrder.scope(
      'store',
      'order',
      'bookedMember',
      'member',
    );

    const targetOnlineOrder = await targetOnlineOrderScope.findByPk(onlineOrderId, {
      include: [{
        model: db.models.PNTable,
      }],
    });

    const assignedMembr = await db.models.Member.findOne({
      where: {
        id: employeeId,
      },
    });

    if (!targetOnlineOrder || !assignedMembr) throw new OnlineOrderNotFoundError();

    if (targetOnlineOrder.payedStatus === 'PAIDANDCONFIRMED') {
      throw new OnlineOrderConfirmedError();
    }

    const transaction = await db.transaction();

    try {
      const orderGroupShipment = await db.models.OrderGroupShipment.create({
        SN: moment().format('YYYYMMDD'),
        status: 'UNSHIPMENTED',
        startDate: targetOnlineOrder.createdAt,
        endDate: new Date(),
        PNTableId: targetOnlineOrder.PNTableId,
      }, { transaction });

      const order = await db.models.Order.create({
        orderSN: `${moment().format('YYmmssMMDD')}8`,
        quantity: 1,
        picture: targetOnlineOrder.picture,
        status: 'UNDISPATCHED',
        orderSource: 'ONLINEORDER',
        StoreId: storeId,
        PNTableId: targetOnlineOrder.PNTableId,
        ProduceType: 'MATERIAL',
        EmpolyedMemberId: assignedMembr.id,
        OrderGroupShipmentId: orderGroupShipment.id,
      }, { transaction });

      let isTimeDifferent = false;

      if (
        targetOnlineOrder.date !== date
        || targetOnlineOrder.time !== time
      ) {
        isTimeDifferent = true;
      }

      await targetOnlineOrder.update({
        StoreId: storeId,
        date,
        time,
        BookedMemberId: assignedMembr.id,
        payedStatus: 'PAIDANDCONFIRMED',
        OrderId: order.id,
        isAssigned: true,
      }, { transaction });

      const qrcodePrefix = `${moment().format('YYYYMMDD')}${targetOnlineOrder.PNTable.code}`;

      const qrcode = await onlineQRCodeGenerator(qrcodePrefix);

      await db.models.OrderQRCode.create({
        OrderId: order.id,
        qrcode,
      }, { transaction });

      const result = await createInvoice(targetOnlineOrder);

      await order.update({
        invoiceNumber: result.InvoiceNumber,
        invoiceDate: result.InvoiceDate,
      }, { transaction });

      await transaction.commit();

      const updatedOnlineOrder = await targetOnlineOrderScope.findByPk(onlineOrderId, {
        include: [{
          model: db.models.PNTable,
        }],
      });

      const director = await updatedOnlineOrder.BookedMember.getMember();

      if (director && director.lineId) {
        await sendLineMessage(
          null,
          `【包膜預約行程變更通知】\n線上訂單編號：${updatedOnlineOrder.id}\n訂購人姓名：${updatedOnlineOrder.name}\n訂購人電話：${updatedOnlineOrder.phone}\n預約時間：${updatedOnlineOrder.date} ${updatedOnlineOrder.time}\n預約門市：${updatedOnlineOrder.Store.Channel.name}${updatedOnlineOrder.Store.name}`,
          director.lineId,
        );
      }

      if (updatedOnlineOrder.Owner.lineId) {
        if (
          isTimeDifferent
          || updatedOnlineOrder.Store.id !== targetOnlineOrder.Store.id
        ) {
          // 如果只更動包膜師不用通知客戶
          const HOST = process.env.HOST || 'http://localhost:8108';

          const orderURL = `${HOST}/member/order`;

          await sendLineMessageToClient(
            `【包膜預約行程變更通知】\n訂單編號：${updatedOnlineOrder.id}\n預約時間：${updatedOnlineOrder.date} ${updatedOnlineOrder.time}\n預約門市：${updatedOnlineOrder.Store.Channel.name}${updatedOnlineOrder.Store.name}\n訂單詳細資訊請至官網會員中心 > 訂單查詢\n訂單查詢傳送門：\n${orderURL}\n如有任何問題請與 artmo 線上小幫手聯繫`,
            updatedOnlineOrder.Owner.lineId,
          );
        }
      }

      if (updatedOnlineOrder.BookedMember.lineId) {
        await sendLineMessage(
          null,
          `【包膜預約行程變更通知】\n線上訂單編號：${updatedOnlineOrder.id}\n訂購人姓名：${updatedOnlineOrder.name}\n訂購人電話：${updatedOnlineOrder.phone}\n預約時間：${updatedOnlineOrder.date} ${updatedOnlineOrder.time}\n預約門市：${updatedOnlineOrder.Store.Channel.name}${updatedOnlineOrder.Store.name}`,
          updatedOnlineOrder.BookedMember.lineId,
        );
      }

      return targetOnlineOrder;
    } catch (e) {
      await transaction.rollback();
      throw e;
    }
  }),
};

export const editOrderByLineBot = {
  type: new GraphQLNonNull(orderType),
  args: {
    orderId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    quantity: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    storeId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.INVENTORY_MANAGE,
  })(async (_, args) => {
    const {
      orderId,
      quantity,
      storeId,
    } = args;

    const targetOrder = await db.models.Order.scope(
      { method: ['memberArea', args] },
    ).findByPk(orderId, {
      include: [{
        model: db.models.PNTable.scope(
          'brandAndModel',
          'categories',
        ),
      }],
    });

    if (!targetOrder) throw new ResourceNotFoundError();

    if (targetOrder.status !== 'ORDERING') {
      throw new OrderUpdateFailError();
    }

    await targetOrder.update({
      quantity,
      StoreId: storeId,
    });

    const newStore = await targetOrder.getStore({
      include: [{
        model: db.models.Channel,
      }, {
        model: db.models.District,
      }],
    });

    return {
      ...targetOrder.dataValues,
      Store: newStore,
    };
  }),
};

export const editOrderByAdmin = {
  type: new GraphQLNonNull(orderType),
  args: {
    orderId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    quantity: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    storeId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.INVOICING_MANAGE,
  })(async (_, args) => {
    const {
      orderId,
      quantity,
      storeId,
    } = args;

    const targetOrder = await db.models.Order.scope(
      { method: ['memberArea', args] },
    ).findByPk(orderId, {
      include: [{
        model: db.models.PNTable.scope(
          'brandAndModel',
          'categories',
          'vendor',
        ),
      }, {
        model: db.models.OrderGroupShipment,
      }],
    });

    if (!targetOrder) throw new ResourceNotFoundError();

    if (targetOrder.status === 'ORDERING') {
      await targetOrder.update({
        quantity,
        StoreId: storeId,
      });

      const newStore = await targetOrder.getStore({
        include: [{
          model: db.models.Channel,
        }, {
          model: db.models.District,
        }],
      });

      return {
        ...targetOrder.dataValues,
        Store: newStore,
      };
    }

    if (targetOrder.status === 'UNDISPATCHED') {
      const transaction = await db.transaction();

      try {
        await targetOrder.update({
          quantity,
          StoreId: storeId,
        }, { transaction });

        if (targetOrder.orderSource !== 'ONLINEORDER') {
          const sameGroupQRCodes = await db.models.OrderQRCode.findAll({
            where: {
              qrcode: {
                [Op.like]: `${targetOrder.OrderGroupShipment.SN}${targetOrder.PNTable.code}%`,
              },
            },
          }).map(x => x.qrcode.slice(20)); // get last 5 number

          const biggestCode = Math.max(...sameGroupQRCodes);

          await db.models.OrderQRCode.destroy({
            where: {
              OrderId: targetOrder.id,
            },
            transaction,
            force: true,
          });

          const arr = Array.from(Array(quantity));

          const qrcodeArray = arr.map((x, idx) => ({
            qrcode: `${targetOrder.OrderGroupShipment.SN}${targetOrder.PNTable.code}${padDigits(idx + 1 + biggestCode, 5)}`,
            OrderId: targetOrder.id,
          }));

          await db.models.OrderQRCode.bulkCreate(qrcodeArray, { transaction });
        }

        await transaction.commit();
      } catch (e) {
        await transaction.rollback();
        throw e;
      }

      const newStore = await targetOrder.getStore({
        include: [{
          model: db.models.Channel,
        }, {
          model: db.models.District,
        }],
      });

      return {
        ...targetOrder.dataValues,
        Store: newStore,
      };
    }

    throw new OrderUpdateFailError();
  }),
};

export default null;
