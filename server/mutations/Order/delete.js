import {
  GraphQLNonNull,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import { ResourceNotFoundError } from '../../errors/General';
import {
  orderDeleteResponseType,
} from '../../types/Order';

export const deleteOrderByAdmin = {
  type: new GraphQLNonNull(orderDeleteResponseType),
  args: {
    orderId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.INVOICING_MANAGE,
  })(async (_, args) => {
    const {
      orderId,
    } = args;

    const targetOrder = await db.models.Order.findByPk(orderId);

    if (!targetOrder) throw new ResourceNotFoundError();

    if (targetOrder.status === 'ORDERING') {
      await targetOrder.destroy();

      return {
        id: targetOrder.id,
        status: true,
        message: '成功刪除',
      };
    }

    if (targetOrder.status === 'UNDISPATCHED') {
      const transaction = await db.transaction();

      try {
        await targetOrder.destroy({ transaction });

        await db.models.OrderQRCode.destroy({
          where: {
            OrderId: targetOrder.id,
          },
          transaction,
          force: true,
        });

        await transaction.commit();
      } catch (e) {
        await transaction.rollback();
        throw e;
      }

      return {
        id: targetOrder.id,
        status: true,
        message: '成功刪除',
      };
    }
 
    return {
      id: targetOrder.id,
      status: false,
      message: '訂單刪除失敗, 請確定該訂單之狀態！',
    };
  }),
};

export const deleteOrderByLineBot = {
  type: new GraphQLNonNull(orderDeleteResponseType),
  args: {
    orderId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.INVENTORY_MANAGE,
  })(async (_, args) => {
    const {
      orderId,
    } = args;

    const targetOrder = await db.models.Order.findByPk(orderId);

    if (!targetOrder) throw new ResourceNotFoundError();

    if (targetOrder.status !== 'ORDERING') {
      return {
        id: targetOrder.id,
        status: false,
        message: '訂單刪除失敗, 請確定該訂單之狀態！',
      };
    }

    await targetOrder.destroy();

    return {
      id: targetOrder.id,
      status: true,
      message: '成功刪除',
    };
  }),
};

export default null;
