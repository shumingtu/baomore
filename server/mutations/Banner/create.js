import {
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import {
  listType,
} from '../../types/Banner';

export default {
  type: new GraphQLNonNull(listType),
  args: {
    picture: {
      type: new GraphQLNonNull(GraphQLString),
    },
    link: {
      type: GraphQLString,
    },
    mobileImg: {
      type: GraphQLString,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_WEBSITE_MANAGE,
  })(async (_, args) => db.models.Banner.createBanner(args)),
};
