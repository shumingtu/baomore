import {
  GraphQLList,
  GraphQLInt,
  GraphQLNonNull,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import {
  db,
} from '../../db';
import { actions } from '../../../shared/roleActions';
import {
  PermissionError,
} from '../../errors/General';
import {
  verifyRollbackBehaviorRecordType,
} from '../../types/BehaviorRecord';

export default {
  type: verifyRollbackBehaviorRecordType,
  args: {
    recordIds: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLInt)),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_DOA_MANAGE,
  })(async (_, {
    recordIds,
  }, { authPayload }) => db.models.BehaviorRecord.verifyBehaviorRecords({
    recordIds,
    approverId: authPayload.id,
  })),
};
