import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLFloat,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';

import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/General';
import { StoreCreateFailed, MemberStoreCreateFailed } from '../../errors/Store';
import { storeType } from '../../types/Store.js';

export default {
  type: new GraphQLNonNull(storeType),
  args: {
    areaId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    channelId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    districtId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phone: {
      type: new GraphQLNonNull(GraphQLString),
    },
    address: {
      type: new GraphQLNonNull(GraphQLString),
    },
    manager: {
      type: new GraphQLNonNull(GraphQLString),
    },
    latitude: {
      type: new GraphQLNonNull(GraphQLFloat),
    },
    longitude: {
      type: new GraphQLNonNull(GraphQLFloat),
    },
    mainEmployeeId: {
      type: GraphQLInt,
    },
    backupEmployeeId: {
      type: GraphQLInt,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_SYSTEMINFO_MANAGE,
  })(async (_, args) => {
    const {
      areaId,
      channelId,
      name,
      phone,
      address,
      districtId,
      mainEmployeeId,
      manager,
      backupEmployeeId,
      latitude,
      longitude,
    } = args;

    const createdStore = await db.models.Store.create({
      AreaId: areaId,
      ChannelId: channelId,
      name,
      phone,
      address,
      manager,
      DistrictId: districtId,
      latitude,
      longitude,
    });

    if (!createdStore) throw new StoreCreateFailed();

    const memberStoreBundle = [];

    if (mainEmployeeId) {
      memberStoreBundle.push({
        type: 'MAIN',
        MemberId: mainEmployeeId,
        StoreId: createdStore.id,
      });
    }

    if (backupEmployeeId) {
      memberStoreBundle.push({
        type: 'BACKUP',
        MemberId: backupEmployeeId,
        StoreId: createdStore.id,
      });
    }

    const memberStore = await db.models.MemberStore.bulkCreate(memberStoreBundle);

    if (!memberStore) throw new MemberStoreCreateFailed();

    const storeArea = await createdStore.getArea();
    const storeChannel = await createdStore.getChannel();
    const storeMembers = await createdStore.getMembers();
    const storeDistrict = await createdStore.getDistrict();

    return {
      ...createdStore.dataValues,
      Area: storeArea || null,
      Channel: storeChannel || null,
      Members: storeMembers || null,
      District: storeDistrict || null,
    };
  }),
};
