import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLFloat,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';

import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/General';
import { StoreNotFound, MemberStoreCreateFailed } from '../../errors/Store';
import { storeType, storeToggleArchiveResponseType } from '../../types/Store.js';

export const toggleStoreArchive = {
  type: storeToggleArchiveResponseType,
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_SYSTEMINFO_MANAGE,
  })(async (_, args) => {
    const {
      id,
    } = args;

    const targetStore = await db.models.Store.findOne({
      where: {
        id,
      },
    });

    if (!targetStore) throw new StoreNotFound();

    await targetStore.update({
      archive: !targetStore.archive,
    });

    return {
      id,
      message: '成功',
      status: true,
    };
  }),
};

export default {
  type: new GraphQLNonNull(storeType),
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    areaId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    channelId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    districtId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phone: {
      type: new GraphQLNonNull(GraphQLString),
    },
    address: {
      type: new GraphQLNonNull(GraphQLString),
    },
    manager: {
      type: new GraphQLNonNull(GraphQLString),
    },
    latitude: {
      type: new GraphQLNonNull(GraphQLFloat),
    },
    longitude: {
      type: new GraphQLNonNull(GraphQLFloat),
    },
    mainEmployeeId: {
      type: GraphQLInt,
    },
    backupEmployeeId: {
      type: GraphQLInt,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_SYSTEMINFO_MANAGE,
  })(async (_, args) => {
    const {
      id,
      areaId,
      channelId,
      name,
      phone,
      address,
      districtId,
      mainEmployeeId,
      backupEmployeeId,
      manager,
      latitude,
      longitude,
    } = args;

    const targetStore = await db.models.Store.findOne({
      where: {
        id,
      },
    });

    if (!targetStore) throw new StoreNotFound();

    await targetStore.update({
      AreaId: areaId,
      ChannelId: channelId,
      name,
      phone,
      address,
      DistrictId: districtId,
      manager,
      latitude,
      longitude,
    });

    await db.models.MemberStore.destroy({
      where: {
        StoreId: targetStore.id,
      },
    });

    const memberStoreBundle = [];

    if (mainEmployeeId) {
      memberStoreBundle.push({
        type: 'MAIN',
        MemberId: mainEmployeeId,
        StoreId: targetStore.id,
      });
    }

    if (backupEmployeeId) {
      memberStoreBundle.push({
        type: 'BACKUP',
        MemberId: backupEmployeeId,
        StoreId: targetStore.id,
      });
    }

    const memberStore = await db.models.MemberStore.bulkCreate(memberStoreBundle);

    if (!memberStore) throw new MemberStoreCreateFailed();

    const storeArea = await targetStore.getArea();
    const storeChannel = await targetStore.getChannel();
    const storeMembers = await targetStore.getMembers();
    const storeDistrict = await targetStore.getDistrict();

    return {
      ...targetStore.dataValues,
      Area: storeArea || null,
      Channel: storeChannel || null,
      Members: storeMembers || null,
      District: storeDistrict || null,
    };
  }),
};
