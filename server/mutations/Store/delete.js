import {
  GraphQLNonNull,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';

import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/General';
import { StoreNotFound } from '../../errors/Store';
import { storeDeleteResponseType } from '../../types/Store.js';

export default {
  type: new GraphQLNonNull(storeDeleteResponseType),
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_SYSTEMINFO_MANAGE,
  })(async (_, args) => {
    const targetStore = await db.models.Store.findOne({
      where: {
        id: args.id,
      },
    });

    if (!targetStore) throw new StoreNotFound();

    await db.models.MemberStore.destroy({
      where: {
        StoreId: targetStore.id,
      },
    });

    await targetStore.destroy();

    return {
      id: targetStore.id,
      status: true,
      message: '刪除成功',
    };
  }),
};
