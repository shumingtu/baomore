import {
  GraphQLNonNull,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import {
  ClosedOrderSettingDeleteResponseType,
} from '../../types/ClosedOrderSetting';

export default {
  type: new GraphQLNonNull(ClosedOrderSettingDeleteResponseType),
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.INVOICING_MANAGE,
  })(async (_, args) => db.models.ClosedOrderSetting.deleteClosedOrderSetting(args)),
};
