import {
  GraphQLNonNull,
  GraphQLInt,
  GraphQLString,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import {
  ClosedOrderSettingType,
} from '../../types/ClosedOrderSetting';

export default {
  type: new GraphQLNonNull(ClosedOrderSettingType),
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    closedDay: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    closedTime: {
      type: new GraphQLNonNull(GraphQLString),
    },
    code: {
      type: GraphQLString,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.INVOICING_MANAGE,
  })(async (_, args) => db.models.ClosedOrderSetting.editClosedOrderSetting(args)),
};
