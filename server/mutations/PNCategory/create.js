import {
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import {
  PNCategoryType,
} from '../../types/PNTable';

export default {
  type: new GraphQLNonNull(PNCategoryType),
  args: {
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.PNTABLE_MANAGE,
  })(async (_, args) => db.models.PNCategory.createPNCategory(args)),
};
