import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
} from 'graphql';
import debug from 'debug';
import { Op } from 'sequelize';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import {
  PermissionError,
  ResourceNotFoundError,
} from '../../errors/General';
import {
  QRCodeHasBeenRollBackedOrSoldError,
  PrductTypeError,
} from '../../errors/Stock.js';
import {
  StockResponseType,
} from '../../types/Stock';

const debugRollback = debug('BAOMORE:ROLLBACK');

export const rollBackForCustomer = {
  type: new GraphQLNonNull(StockResponseType),
  args: {
    quantity: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    pnTableId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    picture: {
      type: GraphQLString,
      description: '上傳照片',
    },
    storeId: {
      type: GraphQLInt,
      description: '門市',
    },
    remark: {
      type: GraphQLString,
      description: '其他資訊(EX: 異常類型、對象...等)',
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.INVENTORY_MANAGE,
  })(async (_, {
    quantity,
    picture,
    storeId,
    remark,
    pnTableId,
  }, { authPayload }) => {
    const {
      id,
    } = authPayload;

    // change stock with transcation
    const transaction = await db.transaction();
    try {
      const behavior = await db.models.Behavior.findOne({
        where: {
          name: '退貨',
        },
      });

      // record
      await db.models.BehaviorRecord.create({
        remark,
        picture,
        StoreId: storeId,
        BehaviorId: behavior.id,
        MemberId: id,
        PNTableId: pnTableId,
        rollbackType: 'CUSTOMERCOMPLAINT',
        quantity,
      }, { transaction });

      await transaction.commit();

      return {
        id: pnTableId,
        message: '退貨完成',
        status: true,
      };
    } catch (e) {
      debugRollback(e);
      await transaction.rollback();
      throw e;
    }
  }),
};

export default {
  type: new GraphQLNonNull(StockResponseType),
  args: {
    qrcode: {
      type: new GraphQLNonNull(GraphQLString),
    },
    picture: {
      type: GraphQLString,
      description: '上傳照片',
    },
    storeId: {
      type: GraphQLInt,
      description: '門市',
    },
    remark: {
      type: GraphQLString,
      description: '其他資訊(EX: 異常類型、對象...等)',
    },
    productType: {
      type: GraphQLString,
    },
    rollbackType: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'DOA,PULLOF,SOCIAL,CUSTOMERCOMPLAINT,NONE',
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.INVENTORY_MANAGE,
  })(async (_, {
    qrcode,
    picture,
    storeId,
    remark,
    rollbackType,
    productType,
  }, { authPayload }) => {
    const {
      id,
    } = authPayload;

    const targetQRCode = await db.models.OrderQRCode.findOne({
      where: {
        qrcode,
      },
      include: [{
        model: db.models.Order,
        where: {
          status: 'DISPATCHEDANDCHECKED',
          EmpolyedMemberId: id,
        },
        include: [{
          model: db.models.PNTable.scope('categories'),
        }],
      }],
    });

    if (!targetQRCode) {
      throw new ResourceNotFoundError('該商品序號不存在！或還沒點貨');
    }

    if (rollbackType === 'DOA') {
      const pnCategory = targetQRCode.Order.PNTable.PNSubCategory.PNCategory;

      if (pnCategory.symbol !== productType) {
        throw new PrductTypeError('商品類型不符！');
      }
    }

    const memberStock = await db.models.MemberStock.findOne({
      where: {
        PNTableId: targetQRCode.Order.PNTable.id,
        MemberId: id,
      },
    });

    if (!memberStock || !memberStock.storageCount) {
      throw new ResourceNotFoundError('已無庫存！無法退貨');
    }

    const behaviorIds = await db.models.Behavior.findAll({
      where: {
        [Op.or]: [{
          name: '銷貨',
        }, {
          name: '退貨',
        }],
      },
    }).map(x => x.id);

    const transferBehavior = await db.models.Behavior.findOne({
      where: {
        name: '調貨',
      },
    });

    const isRollbackOrSoldOrTransfer = await db.models.BehaviorRecord.findOne({
      where: {
        [Op.or]: [{
          BehaviorId: {
            [Op.in]: behaviorIds,
          },
        }, {
          isTransfering: true,
          BehaviorId: transferBehavior.id,
        }],
        qrcode,
      },
    });

    if (isRollbackOrSoldOrTransfer) {
      throw new QRCodeHasBeenRollBackedOrSoldError();
    }

    // change stock with transcation
    const transaction = await db.transaction();
    try {
      const behavior = await db.models.Behavior.findOne({
        where: {
          name: '退貨',
        },
      });

      // record
      await db.models.BehaviorRecord.create({
        remark,
        picture,
        StoreId: storeId,
        BehaviorId: behavior.id,
        MemberId: id,
        PNTableId: targetQRCode.Order.PNTable.id,
        rollbackType,
        quantity: 1,
        qrcode,
        orderSN: targetQRCode.Order.orderSN,
      }, { transaction });

      await transaction.commit();

      return {
        id: targetQRCode.Order.PNTable.id,
        message: '退貨完成',
        status: true,
      };
    } catch (e) {
      debugRollback(e);
      await transaction.rollback();
      throw e;
    }
  }),
};
