import {
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';
import debug from 'debug';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import {
  PermissionError,
} from '../../errors/General';
import { StockOrderGroupNotFoundError } from '../../errors/Stock';
import {
  StockResponseType,
} from '../../types/Stock';

const debugCheck = debug('BAOMORE:CHECKORDER');

export default {
  type: new GraphQLNonNull(StockResponseType),
  args: {
    checkCode: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.INVENTORY_MANAGE,
  })(async (_, {
    checkCode,
  }, { authPayload }) => {
    const {
      id,
    } = authPayload;
    // order 要拉 DISPATCHED的
    const targetQRCode = await db.models.OrderQRCode.findOne({
      where: {
        qrcode: checkCode,
      },
      include: [{
        model: db.models.Order,
        where: {
          status: 'DISPATCHED',
          EmpolyedMemberId: id,
        },
        include: [{
          model: db.models.PNTable,
        }],
      }],
    });

    if (!targetQRCode) {
      throw new StockOrderGroupNotFoundError('此商品已點入庫，請關閉後確認欲執行動作');
    }

    const amount = targetQRCode.Order.quantity;

    // change stock with transcation
    const transaction = await db.transaction();
    try {
      await targetQRCode.Order.update({
        status: 'DISPATCHEDANDCHECKED',
      }, { transaction });
      // add stock
      await db.models.MemberStock.addStock(id, targetQRCode.Order.PNTable.id, amount, transaction);
      // record
      const behavior = await db.models.Behavior.findOne({ where: { name: '點貨' } });
      await db.models.BehaviorRecord.create({
        BehaviorId: behavior.id,
        remark: `點貨數量: ${amount}`,
        MemberId: id,
        PNTableId: targetQRCode.Order.PNTable.id,
        quantity: amount,
        orderSN: targetQRCode.Order.orderSN,
      });

      await transaction.commit();
      return {
        id: targetQRCode.id,
        message: '點貨成功!',
        status: true,
      };
    } catch (e) {
      debugCheck(e);
      await transaction.rollback();
      return {
        id: targetQRCode.id,
        message: '系統發生錯誤！',
        status: false,
      };
    }
  }),
};
