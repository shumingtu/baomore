import {
  GraphQLNonNull,
  GraphQLInt,
} from 'graphql';
import debug from 'debug';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import {
  PermissionError,
  ResourceNotFoundError,
} from '../../errors/General';
import {
  StockResponseTypeForPurchase,
} from '../../types/Stock';

const debugPurchase = debug('BAOMORE:PURCHASE');

export default {
  type: new GraphQLNonNull(StockResponseTypeForPurchase),
  args: {
    pnTableId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    storeId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    quantity: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.INVENTORY_MANAGE,
  })(async (_, {
    pnTableId,
    storeId,
    quantity,
  }, { authPayload }) => {
    const {
      id,
    } = authPayload;

    const pnTable = await db.models.PNTable.scope('vendor', 'categories', { method: ['search', { id: pnTableId }] }).findOne();
    const store = await db.models.Store.scope({ method: ['search', { storeId }] }).findOne();

    if (!pnTable || !store) throw new ResourceNotFoundError('PNTable Or store not found');

    // 檢查目前包膜師是否已有訂貨中的訂單 有的話要先刪掉

    const existOrder = await db.models.Order.findOne({
      where: {
        EmpolyedMemberId: id,
        status: 'ORDERING',
        PNTableId: pnTableId,
      },
    });

    if (existOrder) {
      await existOrder.destroy();
    }

    // 同廠商 其他料號的訂單

    const sameVendorOrder = await db.models.Order.findAll({
      where: {
        EmpolyedMemberId: id,
        status: 'ORDERING',
      },
      include: [{
        model: db.models.PNTable,
        where: {
          VendorId: pnTable.Vendor.id,
        },
      }],
    });

    let totalValues = pnTable.price * quantity;
    let totalQuantity = quantity;
    const vendorSettings = pnTable.Vendor.VendorSettings;
    const memberVendorSettings = vendorSettings.filter(v => v.MemberId === id);
    const defaultVendorSettings = vendorSettings.filter(v => v.MemberId === null);

    if (sameVendorOrder.length) {
      sameVendorOrder.forEach((x) => {
        totalValues += (x.quantity * x.PNTable.price);
        totalQuantity += x.quantity;
      });
    }

    let message = '進貨成功！';

    if (memberVendorSettings.length) {
      const quantitySetting = memberVendorSettings.find(v => v.type === 'QUANTITY');
      const valueSetting = memberVendorSettings.find(v => v.type === 'MONEY');

      if (quantitySetting && totalQuantity < quantitySetting.value) {
        message += `\n${quantitySetting.message}`;
      }

      if (valueSetting && totalValues < valueSetting.value) {
        message += `\n${valueSetting.message}`;
      }
    }

    if (!memberVendorSettings.length && defaultVendorSettings.length) {
      const quantitySetting = defaultVendorSettings.find(v => v.type === 'QUANTITY');
      const valueSetting = defaultVendorSettings.find(v => v.type === 'MONEY');

      if (quantitySetting && totalQuantity < quantitySetting.value) {
        message += `\n${quantitySetting.message}`;
      }

      if (valueSetting && totalValues < valueSetting.value) {
        message += `\n${valueSetting.message}`;
      }
    }

    const transaction = await db.transaction();
    try {
      const isHaveSmallCategory = pnTable.PNSubCategory.PNSubCategory;
      const pnCategory = pnTable.PNSubCategory.PNCategory;
      const bigSubCategory = isHaveSmallCategory ? pnTable.PNSubCategory.PNSubCategory : pnTable.PNSubCategory;
      const smallSubCategory = isHaveSmallCategory ? pnTable.PNSubCategory : null;
      // create order
      const order = await db.models.Order.create({
        quantity,
        picture: pnTable.picture,
        orderSource: 'EMPOLYEE',
        StoreId: store.id,
        EmpolyedMemberId: id,
        PNTableId: pnTable.id,
        ProductType: pnCategory.symbol,
      }, { transaction });

      const remarkText = `進貨數量:${quantity}\n商品類型:${pnCategory.name}\n大類:${bigSubCategory.name}\n小類:${smallSubCategory && smallSubCategory.name}\n品名:${pnTable.name}\n料號:${pnTable.code}`;
      // record
      const behavior = await db.models.Behavior.findOne({ where: { name: '進貨' } });
      await db.models.BehaviorRecord.create({
        remark: remarkText,
        BehaviorId: behavior.id,
        VendorId: pnTable.VendorId,
        StoreId: store.id,
        MemberId: id,
        PNTableId: pnTable.id,
        quantity,
      }, { transaction });

      await transaction.commit();

      return {
        pnTableId,
        message,
        status: true,
        order,
      };
    } catch (e) {
      debugPurchase(e);
      await transaction.rollback();
      throw e;
    }
  }),
};
