import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
} from 'graphql';
import debug from 'debug';
import { authKeeper } from 'graphql-auth-keeper';
import { Op } from 'sequelize';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import {
  PermissionError,
  InvalidParameter,
  ResourceNotFoundError,
} from '../../errors/General';
import {
  SalesRecordNotFoundError,
  QRCodeHasBeenRollBackedOrSoldError,
  StockOverflowError,
} from '../../errors/Stock';
import {
  StockResponseType,
} from '../../types/Stock';

const debugSale = debug('BAOMORE:SALE');

export const saleStart = {
  type: new GraphQLNonNull(StockResponseType),
  args: {
    startTime: {
      type: new GraphQLNonNull(GraphQLString),
    },
    storeId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    vat: {
      type: GraphQLString,
    },
    saleCode: {
      type: GraphQLString,
    },
    productCode: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.INVENTORY_MANAGE,
  })(async (_, {
    startTime,
    storeId,
    vat,
    saleCode,
    productCode,
  }, { authPayload }) => {
    if (!vat && !saleCode) throw new InvalidParameter('Vat And SaleCode Can Not Both Empty');

    const {
      id,
    } = authPayload;

    const targetQRCode = await db.models.OrderQRCode.findOne({
      where: {
        qrcode: productCode,
      },
      include: [{
        model: db.models.Order,
        where: {
          status: 'DISPATCHEDANDCHECKED',
          EmpolyedMemberId: id,
        },
        include: [{
          model: db.models.PNTable,
        }],
      }],
    });

    const targetStore = await db.models.Store.scope(
      'channel',
      'district',
    ).findByPk(storeId);

    if (!targetQRCode || !targetStore) {
      throw new ResourceNotFoundError();
    }

    const pnTableId = targetQRCode.Order.PNTable.id;

    const stock = await db.models.MemberStock.scope(
      { method: ['search', { memberId: id, pnTableId }] },
    ).findOne({
      where: {
        storageCount: {
          [Op.ne]: 0,
        },
      },
    });

    if (!stock) throw new StockOverflowError();

    const rollbackBehavior = await db.models.Behavior.findOne({
      where: {
        name: '退貨',
      },
    });

    const behavior = await db.models.Behavior.findOne({
      where: {
        name: '銷貨',
      },
    });

    const isRollbackOrSold = await db.models.BehaviorRecord.findOne({
      where: {
        [Op.or]: [{
          BehaviorId: rollbackBehavior.id,
        }, {
          BehaviorId: behavior.id,
          startTime: {
            [Op.ne]: null,
          },
          endTime: {
            [Op.ne]: null,
          },
        }],
        qrcode: productCode,
      },
    });

    if (isRollbackOrSold) {
      throw new QRCodeHasBeenRollBackedOrSoldError();
    }


    // change stock with transcation
    const transaction = await db.transaction();
    try {
      // record
      const vatOrSaleCodeRemark = vat ? `發票號碼:${vat}` : `銷貨編號${saleCode}`;

      const existBehaviorRecord = await db.models.BehaviorRecord.findOne({
        where: {
          BehaviorId: behavior.id,
          qrcode: productCode,
        },
      });

      if (existBehaviorRecord) {
        await existBehaviorRecord.destroy({ transaction });
      }

      const record = await db.models.BehaviorRecord.create({
        startTime,
        price: targetQRCode.Order.PNTable.price,
        remark: `銷貨縣市:${targetStore.District.City.name} ${targetStore.District.name}\n門市:${targetStore.name}\n通路:${targetStore.Channel.name}\n${vatOrSaleCodeRemark}`,
        BehaviorId: behavior.id,
        StoreId: storeId,
        MemberId: id,
        PNTableId: targetQRCode.Order.PNTable.id,
        quantity: 1,
        qrcode: productCode,
        orderSN: targetQRCode.Order.orderSN,
      }, { transaction });

      await transaction.commit();
      return {
        id: record.id,
        message: '銷貨開始!',
        status: true,
      };
    } catch (e) {
      debugSale(e);
      await transaction.rollback();
      throw e;
    }
  }),
};

export const saleStop = {
  type: new GraphQLNonNull(StockResponseType),
  args: {
    endTime: {
      type: new GraphQLNonNull(GraphQLString),
    },
    recordId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.MANAGE_STOCK_SYSTEM,
  })(async (_, {
    endTime,
    recordId,
  }, { authPayload }) => {
    const record = await db.models.BehaviorRecord.scope({ method: ['search', { id: recordId }] }).findOne();

    if (!record) throw new SalesRecordNotFoundError();

    if (record.endTime) {
      return {
        id: record.id,
        message: '銷貨完成!',
        status: true,
      };
    }
    // change stock with transcation
    const transaction = await db.transaction();
    try {
      // reduce stock
      await db.models.MemberStock.reduceStock(authPayload.id, record.PNTableId, 1, transaction);
      // record
      record.endTime = endTime;
      await record.save({ transaction });

      await transaction.commit();
      return {
        id: record.id,
        message: '銷貨完成!',
        status: true,
      };
    } catch (e) {
      debugSale(e);
      await transaction.rollback();
      throw e;
    }
  }),
};
