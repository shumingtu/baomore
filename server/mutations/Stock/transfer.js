import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
} from 'graphql';
import debug from 'debug';
import { Op } from 'sequelize';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import {
  PermissionError,
  ResourceNotFoundError,
} from '../../errors/General';
import {
  QRCodeHasBeenTransferedError,
} from '../../errors/Stock.js';
import {
  StockResponseType,
} from '../../types/Stock';
import { sendTranferInfoToEmployee } from '../../helpers/line/linebot';

const debugTransfer = debug('BAOMORE:TRANSFER');

export default {
  type: new GraphQLNonNull(StockResponseType),
  args: {
    qrcode: {
      type: new GraphQLNonNull(GraphQLString),
    },
    employeeId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    remark: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.INVENTORY_MANAGE,
  })(async (_, {
    qrcode,
    employeeId,
    remark,
  }, { authPayload }) => {
    const {
      id,
      name,
    } = authPayload;

    const targetQRCode = await db.models.OrderQRCode.findOne({
      where: {
        qrcode,
      },
      include: [{
        model: db.models.Order,
        where: {
          status: 'DISPATCHEDANDCHECKED',
          EmpolyedMemberId: id,
        },
        include: [{
          model: db.models.PNTable.scope('categories'),
        }],
      }],
    });

    if (!targetQRCode) {
      throw new ResourceNotFoundError();
    }

    const behaviorIds = await db.models.Behavior.findAll({
      where: {
        [Op.or]: [{
          name: '銷貨',
        }, {
          name: '退貨',
        }],
      },
    }).map(x => x.id);

    const behavior = await db.models.Behavior.findOne({
      where: {
        name: '調貨',
      },
    });

    const isTransfered = await db.models.BehaviorRecord.findOne({
      where: {
        [Op.or]: [{
          BehaviorId: {
            [Op.in]: behaviorIds, // 銷貨or退貨
          },
        }, {
          isTransfering: true, // 調貨中的狀態
          BehaviorId: behavior.id,
        }],
        qrcode,
      },
    });

    if (isTransfered) {
      throw new QRCodeHasBeenTransferedError();
    }

    // change stock with transcation
    const transaction = await db.transaction();
    try {
      // record
      const member = await db.models.Member.findOne({
        where: {
          id: employeeId,
        },
      });

      const pnTable = targetQRCode.Order.PNTable;
      const isHaveSmallCategory = pnTable.PNSubCategory.PNSubCategory;
      const pnCategory = pnTable.PNSubCategory.PNCategory;
      const bigSubCategory = isHaveSmallCategory ? pnTable.PNSubCategory.PNSubCategory : pnTable.PNSubCategory;
      const smallSubCategory = isHaveSmallCategory ? pnTable.PNSubCategory : null;

      const remarkText = `${remark}\n商品類型:${pnCategory.name}\n大類:${bigSubCategory.name}\n小類:${smallSubCategory && smallSubCategory.name}\n品名:${pnTable.name}\n料號:${pnTable.code}`;

      const record = await db.models.BehaviorRecord.create({
        remark: remarkText,
        BehaviorId: behavior.id,
        MemberId: id,
        PNTableId: targetQRCode.Order.PNTable.id,
        quantity: 1,
        qrcode,
        isTransfering: true,
        orderSN: targetQRCode.Order.orderSN,
      }, { transaction });

      await sendTranferInfoToEmployee(
        member,
        `包膜師調貨確認通知\n調出者：${name}\n品名：${pnTable.name}\n料號：${pnTable.code}\n商品序號：${qrcode}`,
        `command=transfer&pnTableId=${pnTable.id}&transferFromId=${id}&transferToId=${employeeId}&quantity=${1}&orderSN=${targetQRCode.Order.orderSN}&qrcode=${qrcode}&recordId=${record.id}&oriOrderId=${targetQRCode.Order.id}`,
      );

      // remove 2019-08-05
      // await db.models.EmployeeTransferSchedule.create({
      //   sendTransferTime: new Date(),
      //   BehaviorRecordId: record.id,
      // }, { transaction });

      await transaction.commit();
      return {
        id: record.id,
        message: '調貨需求已送出',
        status: true,
      };
    } catch (e) {
      debugTransfer(e);
      await transaction.rollback();
      throw e;
    }
  }),
};
