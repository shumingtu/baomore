import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLList,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import {
  ResourceNotFoundError,
} from '../../errors/General';
import {
  activityListType,
  activityFragmentInputType,
  activityToggleResponseType,
} from '../../types/Activity';

export const toggleActivity = {
  type: new GraphQLNonNull(activityToggleResponseType),
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_WEBSITE_MANAGE,
  })(async (_, args) => {
    const targetActivity = await db.models.Activity.findByPk(args.id);

    if (!targetActivity) throw new ResourceNotFoundError();

    targetActivity.update({
      isActived: !targetActivity.isActived,
    });

    return {
      id: targetActivity.id,
      status: true,
      message: '成功！',
    };
  }),
};

export default {
  type: new GraphQLNonNull(activityListType),
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    desktopImg: {
      type: new GraphQLNonNull(GraphQLString),
    },
    mobileImg: {
      type: new GraphQLNonNull(GraphQLString),
    },
    description: {
      type: GraphQLString,
    },
    fragments: {
      type: new GraphQLList(activityFragmentInputType),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_WEBSITE_MANAGE,
  })(async (_, args) => db.models.Activity.editActivity(args)),
};
