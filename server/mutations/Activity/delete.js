import {
  GraphQLNonNull,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import {
  activityDeleteResponseType,
} from '../../types/Activity';

export default {
  type: new GraphQLNonNull(activityDeleteResponseType),
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_WEBSITE_MANAGE,
  })(async (_, args) => db.models.Activity.deleteActivity(args)),
};
