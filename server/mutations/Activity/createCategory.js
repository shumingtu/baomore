import {
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import {
  categoryType,
} from '../../types/Activity';

export default {
  type: new GraphQLNonNull(categoryType),
  args: {
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    desktopImg: {
      type: new GraphQLNonNull(GraphQLString),
    },
    mobileImg: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_WEBSITE_MANAGE,
  })(async (_, args) => db.models.ActivityCategory.createCategory(args)),
};
