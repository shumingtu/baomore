import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/General';
import { VendorNotFound } from '../../errors/Vendor';
import {
  VendorType,
} from '../../types/Vendor';
import {
  settingType,
} from '../../types/VendorSetting';


export default {
  type: new GraphQLNonNull(VendorType),
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    contactPersonName: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phone: {
      type: new GraphQLNonNull(GraphQLString),
    },
    address: {
      type: new GraphQLNonNull(GraphQLString),
    },
    settings: {
      type: new GraphQLList(settingType),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_SYSTEMINFO_MANAGE,
  })(async (_, args) => {
    const {
      id,
      name,
      contactPersonName,
      phone,
      address,
      settings,
    } = args;

    const targetVendor = await db.models.Vendor.findOne({
      where: {
        id,
      },
    });

    if (!targetVendor) throw new VendorNotFound();

    await db.models.VendorSetting.destroy({
      where: {
        VendorId: targetVendor.id,
      },
    });

    if (settings && settings.length) {
      const settingValue = settings.map(x => ({
        ...x,
        MemberId: x.MemberId || null,
        VendorId: id,
      }));

      await db.models.VendorSetting.bulkCreate(settingValue);
    }

    const vendorSettings = await targetVendor.getVendorSettings();

    await targetVendor.update({
      name,
      contactPersonName,
      phone,
      address,
    });

    return {
      ...targetVendor.dataValues,
      VendorSettings: vendorSettings,
    };
  }),
};
