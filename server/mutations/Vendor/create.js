import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLList,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/General';
import { VendorCreateFailed } from '../../errors/Vendor';
import {
  VendorType,
} from '../../types/Vendor';
import {
  settingType,
} from '../../types/VendorSetting';


export default {
  type: new GraphQLNonNull(VendorType),
  args: {
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    contactPersonName: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phone: {
      type: new GraphQLNonNull(GraphQLString),
    },
    address: {
      type: new GraphQLNonNull(GraphQLString),
    },
    settings: {
      type: new GraphQLList(settingType),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_SYSTEMINFO_MANAGE,
  })(async (_, args) => {
    const {
      name,
      contactPersonName,
      phone,
      address,
      settings,
    } = args;

    const createdVendor = await db.models.Vendor.create({
      name,
      contactPersonName,
      phone,
      address,
    });

    if (!createdVendor) throw new VendorCreateFailed();

    if (settings && settings.length) {
      const settingValue = settings.map(x => ({
        ...x,
        VendorId: createdVendor.id,
      }));

      await db.models.VendorSetting.bulkCreate(settingValue);
    }

    const vendorSettings = await createdVendor.getVendorSettings();

    return {
      ...createdVendor.dataValues,
      VendorSettings: vendorSettings,
    };
  }),
};
