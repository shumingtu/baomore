import {
  GraphQLNonNull,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/General';
import { VendorNotFound } from '../../errors/Vendor';
import {
  vendorDeleteResponseType,
} from '../../types/Vendor';


export default {
  type: new GraphQLNonNull(vendorDeleteResponseType),
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_SYSTEMINFO_MANAGE,
  })(async (_, args) => {
    const targetVendor = await db.models.Vendor.findOne({
      where: {
        id: args.id,
      },
    });

    if (!targetVendor) throw new VendorNotFound();

    await db.models.VendorSetting.destroy({
      where: {
        VendorId: targetVendor.id,
      },
    });

    await targetVendor.destroy();

    return {
      id: targetVendor.id,
      status: true,
      message: '刪除成功',
    };
  }),
};
