import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
} from 'graphql';
import fs from 'fs';
import { authKeeper } from 'graphql-auth-keeper';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import {
  listType,
} from '../../types/SocialCategory';

export default {
  type: new GraphQLNonNull(listType),
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    link: {
      type: GraphQLString,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.ADMIN_WEBSITE_MANAGE,
  })((_, args) => {
    const rawData = fs.readFileSync('shared/socialCategory.json');
    if (!rawData) return null;
    const result = JSON.parse(rawData);

    const otherData = result.socialCategory.filter(x => x.id !== args.id);

    const payload = {
      id: args.id,
      name: args.name,
      link: args.link || '',
    };

    const updatedData = {
      socialCategory: [
        ...otherData,
        payload,
      ],
    };

    fs.writeFileSync('shared/socialCategory.json', JSON.stringify(updatedData));

    return payload;
  }),
};
