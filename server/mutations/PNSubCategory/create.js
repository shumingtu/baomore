import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLBoolean,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import {
  PNSubCategoryType,
} from '../../types/PNTable';

export default {
  type: new GraphQLNonNull(PNSubCategoryType),
  args: {
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    picture: {
      type: GraphQLString,
    },
    isOnSale: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    PNCategoryId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    PNSubCategoryId: {
      type: GraphQLInt,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.PNTABLE_MANAGE,
  })(async (_, args) => db.models.PNSubCategory.createPNSubCategory(args)),
};
