import {
  GraphQLNonNull,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import {
  PNSubCategoryDeleteResponseType,
} from '../../types/PNTable';

export default {
  type: new GraphQLNonNull(PNSubCategoryDeleteResponseType),
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.PNTABLE_MANAGE,
  })(async (_, args) => db.models.PNSubCategory.deletePNSubCategory(args)),
};
