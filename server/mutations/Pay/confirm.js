import {
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { Op } from 'sequelize';

import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import { PayConfirmError } from '../../errors/Pay';
import {
  PayConfirmResponseType,
} from '../../types/Pay';
import { confirm as linePayConfirm } from '../../helpers/line/linePayRouter';
import { ResourceNotFoundError } from '../../errors/General';
import { sendBookedInfoToEmployee, sendLineMessageToClient } from '../../helpers/line/linebot';

export default {
  type: new GraphQLNonNull(PayConfirmResponseType),
  args: {
    orderId: {
      type: new GraphQLNonNull(GraphQLString),
    },
    transactionId: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.NORMAL_MEMBER,
  })(async (_, {
    orderId,
    transactionId,
  }) => {
    const storeDateSlotNum = 4;

    const order = await db.models.OnlineOrder.scope(
      { method: ['search', { orderId }] },
      { method: ['member', { archiveLimit: true }] },
      'store',
    ).findOne();

    const memberBookedOrders = await db.models.OnlineOrder.findAll({
      where: {
        BookedMemberId: {
          [Op.in]: order.Store.Members.map(x => x.id),
        },
        date: order.date,
      },
    });

    const rawFreeMembers = order.Store.Members.filter((x) => {
      const memberOrders = memberBookedOrders.filter(o => o.BookedMemberId === x.id);
      return memberOrders.length < storeDateSlotNum;
    });

    const freeMembers = rawFreeMembers.filter(x => x.lineId !== null);

    if (!order || !order.Owner || !order.Store.Members.length || !freeMembers.length) {
      throw new ResourceNotFoundError('Order Resource Not Found');
    }

    const mainMember = freeMembers.find(x => x.MemberStore && x.MemberStore.type === 'MAIN');

    const transaction = await db.transaction();

    try {
      await linePayConfirm(transactionId, order.price);

      await order.update({
        payedStatus: 'PAIDNOTCONFIRMED',
        BookedMemberId: (mainMember && mainMember.id) || freeMembers[0].id,
      }, { transaction });

      // notify client
      // notify employee


      await sendBookedInfoToEmployee(
        (mainMember && mainMember.lineId) || freeMembers[0].lineId,
        `【新預約確認通知】\n線上訂單編號：${orderId}\n訂購人姓名：${order.name}\n訂購人電話：${order.phone}\n預約時間：${order.date} ${order.time}\n預約門市：${order.Store.Channel.name}${order.Store.name}`,
        `command=bookConfirm&onlineOrderId=${order.id}`,
      );

      const HOST = process.env.HOST || 'http://localhost:8108';

      const orderURL = `${HOST}/member/order`;

      await sendLineMessageToClient(
        `【包膜預約成功（已付款）】\n訂單編號：${orderId}\n預約時間：${order.date} ${order.time}\n預約門市：${order.Store.Channel.name}${order.Store.name}\n訂單詳細資訊請至官網會員中心 > 訂單查詢\n訂單查詢傳送門：\n${orderURL}\n如有任何問題請與 artmo 線上小幫手聯繫`,
        order.Owner.lineId,
      );

      await db.models.EmployeeConfirmSchedule.create({
        sendBookedInfoTime: new Date(),
        OnlineOrderId: order.id,
      }, { transaction });

      await transaction.commit();

      return {
        status: true,
        message: 'Success Confirm Payment',
        human: 'LINE PAY 付款確認成功',
      };
    } catch (e) {
      await transaction.rollback();
      throw new PayConfirmError();
    }
  }),
};
