import {
  GraphQLNonNull,
  GraphQLInt,
  GraphQLString,
  GraphQLBoolean,
} from 'graphql';
import moment from 'moment';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import { CreateOnlineOrderError } from '../../errors/Pay';
import {
  PayOnlineOrderResponseType,
} from '../../types/Pay';
import {
  MemberNotFoundError,
} from '../../errors/General';

function getRandomNumber() {
  return Math.floor(Math.random() * (900000000000) + 100000000000); // 12 digits
}

export default {
  type: new GraphQLNonNull(PayOnlineOrderResponseType),
  args: {
    pnTableId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    picture: {
      type: new GraphQLNonNull(GraphQLString),
    },
    storeId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    date: {
      type: new GraphQLNonNull(GraphQLString),
    },
    time: {
      type: new GraphQLNonNull(GraphQLString),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    email: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phone: {
      type: new GraphQLNonNull(GraphQLString),
    },
    isShared: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    storedDesignJSON: {
      type: new GraphQLNonNull(GraphQLString),
    },
    isAllCustomized: {
      type: GraphQLBoolean,
      defaultValue: false,
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.NORMAL_MEMBER,
  })(async (_, {
    pnTableId,
    picture,
    storeId,
    date,
    time,
    name,
    email,
    phone,
    isShared,
    storedDesignJSON,
    isAllCustomized,
  }, { authPayload }) => {
    const {
      id,
    } = authPayload;

    try {
      const whereClause = {};

      if (isAllCustomized) {
        whereClause.isAllCustomized = isAllCustomized;
      } else {
        whereClause.id = pnTableId;
      }
      // find pntable
      const pnTable = await db.models.PNTable.findOne({
        where: whereClause,
      });
      const member = await db.models.Member.getMe(id);

      if (!member) {
        throw new MemberNotFoundError();
      }

      // create order
      const orderId = `${moment().format('YYYYHHMMDD')}${getRandomNumber()}`;
      await db.models.OnlineOrder.create({
        id: orderId,
        picture,
        name,
        phone,
        email,
        date,
        time,
        price: pnTable.onlinePrice,
        isShared: isShared || false,
        payedStatus: 'UNPAID',
        StoreId: storeId,
        // BookedMemberId 系統分配包膜師
        OwnerId: member.id,
        PNTableId: pnTable.id,
      });

      member.storedDesignJSON = storedDesignJSON;
      await member.save();

      return {
        payToken: await member.payToken(orderId),
        status: true,
        message: 'Success Created Order',
        human: '訂單已成立，每筆訂單將會保留 30 分鐘，請前往付款',
      };
    } catch (e) {
      throw new CreateOnlineOrderError();
    }
  }),
};
