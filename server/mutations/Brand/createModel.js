import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import {
  BrandModelResponseType,
} from '../../types/Brand';

export default {
  type: new GraphQLNonNull(BrandModelResponseType),
  args: {
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    brandId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.PHONE_MODEL_MANAGE,
  })(async (_, args) => db.models.BrandModel.createBrandModel(args)),
};
