import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import {
  BrandModelColorResponseType,
} from '../../types/Brand';

export default {
  type: new GraphQLNonNull(BrandModelColorResponseType),
  args: {
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    modelId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    phoneBg: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phoneMask: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phoneCover: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.PHONE_MODEL_MANAGE,
  })(async (_, args) => db.models.BrandModelColor.createBrandModelColor(args)),
};
