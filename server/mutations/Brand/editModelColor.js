import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import {
  BrandModelColorType,
} from '../../types/Brand';

export default {
  type: new GraphQLNonNull(BrandModelColorType),
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phoneBg: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phoneMask: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phoneCover: {
      type: new GraphQLNonNull(GraphQLString),
    },
    brandModelId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.PHONE_MODEL_MANAGE,
  })(async (_, args) => db.models.BrandModelColor.editBrandModelColor(args)),
};
