import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
} from 'graphql';
import { authKeeper } from 'graphql-auth-keeper';
import { db } from '../../db';
import { actions } from '../../../shared/roleActions';
import { PermissionError } from '../../errors/Member';
import {
  BrandResponseType,
} from '../../types/Brand';

export default {
  type: new GraphQLNonNull(BrandResponseType),
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: authKeeper({
    onFailed: new PermissionError(),
    actions: actions.PHONE_MODEL_MANAGE,
  })(async (_, args) => db.models.Brand.editBrand(args)),
};
