import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLFloat,
  GraphQLList,
  GraphQLBoolean,
} from 'graphql';

import { AreaType } from './Area';
import { ChannelType } from './Channel';
import { memberType } from './Member';
import { districtType } from './District';

export const locationType = new GraphQLObjectType({
  name: 'location',
  fields: {
    lat: {
      type: new GraphQLNonNull(GraphQLFloat),
    },
    lon: {
      type: new GraphQLNonNull(GraphQLFloat),
    },
  },
});

export const storeType = new GraphQLObjectType({
  name: 'Store',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phone: {
      type: new GraphQLNonNull(GraphQLString),
    },
    address: {
      type: new GraphQLNonNull(GraphQLString),
    },
    latitude: {
      type: new GraphQLNonNull(GraphQLFloat),
    },
    longitude: {
      type: new GraphQLNonNull(GraphQLFloat),
    },
    manager: {
      type: GraphQLString,
    },
    archive: {
      type: GraphQLBoolean,
    },
    area: {
      type: AreaType,
      resolve: store => store.Area,
    },
    channel: {
      type: ChannelType,
      resolve: store => store.Channel,
    },
    members: {
      type: GraphQLList(memberType),
      resolve: store => store.Members,
    },
    district: {
      type: districtType,
      resolve: store => store.District,
    },
  }),
});

export const storeDeleteResponseType = new GraphQLObjectType({
  name: 'StoreDeleteResponseType',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export const storeToggleArchiveResponseType = new GraphQLObjectType({
  name: 'StoreToggleArchiveResponseType',
  fields: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
});

export default null;
