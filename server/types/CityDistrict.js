import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLList,
  GraphQLInt,
  GraphQLFloat,
} from 'graphql';

import { districtType } from './District';

export const cityType = new GraphQLObjectType({
  name: 'City',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    latitude: {
      type: GraphQLFloat,
    },
    longitude: {
      type: GraphQLFloat,
    },
    disticts: {
      type: new GraphQLList(districtType),
      resolve: city => city.Districts,
    },
  }),
});

export default null;
