import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLBoolean,
} from 'graphql';
import { PNTableType } from './PNTable';
import { orderType } from './Order.js';
import { memberType } from './Member.js';

export const stockListType = new GraphQLObjectType({
  name: 'StockList',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    storageCount: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    member: {
      type: new GraphQLNonNull(memberType),
      resolve: stock => stock.Member,
    },
    pnTable: {
      type: new GraphQLNonNull(PNTableType),
      resolve: stock => stock.PNTable,
    },
  }),
});

export const StockCheckResponseType = new GraphQLObjectType({
  name: 'StockCheckResponse',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    pnTable: {
      type: new GraphQLNonNull(PNTableType),
    },
    amount: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  }),
});

export const StockResponseType = new GraphQLObjectType({
  name: 'StockResponse',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
  }),
});

export const StockResponseTypeForPurchase = new GraphQLObjectType({
  name: 'StockResponseTypeForPurchase',
  fields: () => ({
    pnTableId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    order: {
      type: orderType,
    },
  }),
});

export const StockType = new GraphQLObjectType({
  name: 'Inventory',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
      description: '料號ID',
    },
    pnTable: {
      type: new GraphQLNonNull(PNTableType),
    },
    amount: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    orderingOrder: {
      type: orderType,
    },
  }),
});
