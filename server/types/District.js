import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLInt,
} from 'graphql';

import { cityType } from './CityDistrict.js';

export const districtType = new GraphQLObjectType({
  name: 'District',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    city: {
      type: cityType,
      resolve: district => district.City,
    },
  }),
});

export default null;
