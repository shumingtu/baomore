import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLBoolean,
} from 'graphql';
import moment from 'moment';

export const announcementType = new GraphQLObjectType({
  name: 'Announcement',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    title: {
      type: new GraphQLNonNull(GraphQLString),
    },
    content: {
      type: new GraphQLNonNull(GraphQLString),
    },
    createdAt: {
      type: new GraphQLNonNull(GraphQLString),
      resolve: announcement => moment(announcement.createdAt).format('YYYY/MM/DD HH:mm:ss'),
    },
  }),
});

export const announcementDeleteResponseType = new GraphQLObjectType({
  name: 'AnnouncementDeleteResponse',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export default null;
