import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLList,
  GraphQLInt,
} from 'graphql';
import {
  onlineOrderType,
} from './OnlineOrder';

export const assignmentType = new GraphQLObjectType({
  name: 'Assignment',
  fields: () => ({
    time: {
      type: new GraphQLNonNull(GraphQLString),
    },
    denominator: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    onlineOrders: {
      type: new GraphQLNonNull(new GraphQLList(onlineOrderType)),
    },
  }),
});

export const scheduleType = new GraphQLObjectType({
  name: 'Schedule',
  fields: () => ({
    date: {
      type: new GraphQLNonNull(GraphQLString),
    },
    assignments: {
      type: new GraphQLList(assignmentType),
    },
  }),
});

export const memberAssignmentType = new GraphQLObjectType({
  name: 'memberAssignment',
  fields: () => ({
    time: {
      type: new GraphQLNonNull(GraphQLString),
    },
    order: {
      type: new GraphQLNonNull(onlineOrderType),
    },
  }),
});

export default scheduleType;
