import {
  GraphQLObjectType,
  GraphQLInputObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLInt,
} from 'graphql';

export const vendorSettingType = new GraphQLObjectType({
  name: 'VendorSetting',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    type: {
      type: new GraphQLNonNull(GraphQLString),
    },
    value: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    VendorId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    MemberId: {
      type: GraphQLInt,
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export const settingType = new GraphQLInputObjectType({
  name: 'SettingObject',
  fields: () => ({
    type: {
      type: new GraphQLNonNull(GraphQLString),
    },
    value: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    MemberId: {
      type: GraphQLInt,
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export default null;
