import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
} from 'graphql';

export const emojiType = new GraphQLObjectType({
  name: 'Emoji',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    picture: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export const emojiCategoryType = new GraphQLObjectType({
  name: 'EmojiCategorty',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});
