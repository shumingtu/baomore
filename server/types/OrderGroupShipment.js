import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLList,
  GraphQLInt,
  GraphQLBoolean,
} from 'graphql';
import moment from 'moment';

import { PNTableType } from './PNTable';
import { orderType } from './Order';

export const OrderGroupShipmentQueryType = new GraphQLObjectType({
  name: 'OrderGroupShipmentQuery',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    SN: {
      type: new GraphQLNonNull(GraphQLString),
    },
    status: {
      type: new GraphQLNonNull(GraphQLString),
    },
    startDate: {
      type: new GraphQLNonNull(GraphQLString),
      resolve: x => x.startDate && moment(x.startDate).format('YYYY/MM/DD'),
    },
    endDate: {
      type: new GraphQLNonNull(GraphQLString),
      resolve: x => x.endDate && moment(x.endDate).format('YYYY/MM/DD'),
    },
    createdAt: {
      type: new GraphQLNonNull(GraphQLString),
      resolve: x => x.createdAt && moment(x.createdAt).format('YYYY/MM/DD'),
    },
    PNTable: {
      type: PNTableType,
    },
    Orders: {
      type: new GraphQLList(orderType),
    },
    totalQuantity: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  }),
});

export const OrderGroupShipmentUpdateType = new GraphQLObjectType({
  name: 'OrderGroupShipment',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export default null;
