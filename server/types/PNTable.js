import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLBoolean,
} from 'graphql';

import { VendorType } from './Vendor.js';
import {
  BrandType,
  BrandModelType,
} from './Brand.js';

export const PNTableCodeType = new GraphQLObjectType({
  name: 'PNTableCodeType',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    code: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export const PNTableDeleteResponseType = new GraphQLObjectType({
  name: 'PNTableDeleteResponseType',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export const PNCategoryType = new GraphQLObjectType({
  name: 'PNCategory',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export const PNCategoryDeleteResponseType = new GraphQLObjectType({
  name: 'PNCategoryDeleteResponseType',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export const PNSubCategoryType = new GraphQLObjectType({
  name: 'SubPNCategory',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    picture: {
      type: GraphQLString,
    },
    isOnSale: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
  }),
});

export const PNSubCategoryDeleteResponseType = new GraphQLObjectType({
  name: 'PNSubCategoryDeleteResponseType',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export const CustomPNTableType = new GraphQLObjectType({
  name: 'CustomPNTableType',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    code: {
      type: new GraphQLNonNull(GraphQLString),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    description: {
      type: GraphQLString,
    },
    onlinePrice: {
      type: GraphQLString,
    },
  }),
});

export const PNTableType = new GraphQLObjectType({
  name: 'PNTable',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    code: {
      type: new GraphQLNonNull(GraphQLString),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    picture: {
      type: GraphQLString,
    },
    suitableDevice: {
      type: GraphQLString,
    },
    description: {
      type: GraphQLString,
    },
    isOnSale: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    price: {
      type: GraphQLString,
    },
    onlinePrice: {
      type: GraphQLString,
    },
    pnCategory: {
      type: PNCategoryType,
      resolve: pnTable => (pnTable.PNSubCategory && pnTable.PNSubCategory.PNCategory) || pnTable.pnCategory,
    },
    bigSubCategory: {
      type: PNSubCategoryType,
      resolve: pnTable => (pnTable.PNSubCategory && pnTable.PNSubCategory.PNSubCategory) || pnTable.bigSubCategory,
    },
    smallSubCategory: {
      type: PNSubCategoryType,
      resolve: pnTable => pnTable.PNSubCategory || pnTable.smallSubCategory,
    },
    vendor: {
      type: VendorType,
      resolve: pnTable => pnTable.Vendor,
    },
    BrandModel: {
      type: BrandModelType,
    },
    Brand: {
      type: BrandType,
      resolve: pnTable => pnTable.BrandModel && pnTable.BrandModel.Brand,
    },
  }),
});
