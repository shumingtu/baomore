import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLBoolean,
} from 'graphql';

export const listType = new GraphQLObjectType({
  name: 'Banners',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    picture: {
      type: new GraphQLNonNull(GraphQLString),
    },
    link: {
      type: GraphQLString,
    },
    mobileImg: {
      type: GraphQLString,
    },
  }),
});

export const bannerDeleteResponseType = new GraphQLObjectType({
  name: 'BannerDeleteResponseType',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export default listType;
