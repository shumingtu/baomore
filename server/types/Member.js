import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLFloat,
  GraphQLBoolean,
  GraphQLList,
} from 'graphql';
import moment from 'moment';
import { storeType } from './Store';
import { AreaType } from './Area';
import { memberStoreType } from './MemberStore';
import { memberWarrantyType } from './Warranty';
import { onlineOrderType } from './OnlineOrder.js';
import { roleType } from './Role.js';

export const memberType = new GraphQLObjectType({
  name: 'Member',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    serialNumber: {
      type: GraphQLString,
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    AreaId: {
      type: GraphQLInt,
    },
    lineId: {
      type: GraphQLString,
    },
    gender: {
      type: GraphQLString,
    },
    birthday: {
      type: GraphQLString,
      resolve: member => member.birthday && moment(member.birthday).format('YYYY-MM-DD'),
    },
    avatar: {
      type: GraphQLString,
    },
    phone: {
      type: GraphQLString,
    },
    email: {
      type: GraphQLString,
    },
    area: {
      type: AreaType,
      resolve: member => member.Area,
    },
    lineAccount: {
      type: GraphQLString,
    },
    archive: {
      type: GraphQLBoolean,
    },
    director: {
      type: memberType,
      resolve: (member) => {
        if (member.getMember) return member.getMember();
        return member.director;
      },
    },
    memberStore: {
      type: memberStoreType,
      resolve: member => member.MemberStore,
    },
    memberWarranties: {
      type: new GraphQLList(memberWarrantyType),
      resolve: member => member.memberWarranties,
    },
    onlineOrders: {
      type: new GraphQLList(onlineOrderType),
      resolve: member => member.onlineOrders,
    },
    roles: {
      type: new GraphQLList(roleType),
      resolve: member => member.Roles,
    },
  }),
});

export const memberMeType = new GraphQLObjectType({
  name: 'MemberMe',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    avatar: {
      type: GraphQLString,
    },
    phone: {
      type: GraphQLString,
    },
    email: {
      type: GraphQLString,
    },
    gender: {
      type: GraphQLString,
    },
    birthday: {
      type: GraphQLString,
      resolve: member => member.birthday && moment(member.birthday).format('YYYY-MM-DD'),
    },
    storedDesignJSON: {
      type: GraphQLString,
    },
    AreaId: {
      type: GraphQLInt,
    },
  }),
});

export const employeeLoginType = new GraphQLObjectType({
  name: 'EmployeeLogin',
  fields: () => ({
    member: {
      type: new GraphQLNonNull(memberType),
    },
    accessToken: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export const employeePunchResponseType = new GraphQLObjectType({
  name: 'EmployeePunchResponse',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
  }),
});

export const managerPunchResponseType = new GraphQLObjectType({
  name: 'ManagerPunchResponse',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
  }),
});

export const employeePunchType = new GraphQLObjectType({
  name: 'EmployeePunch',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    punchTime: {
      type: new GraphQLNonNull(GraphQLString),
      resolve: punch => moment(punch.punchTime).format('YYYY/MM/DD HH:mm:ss'),
    },
    store: {
      type: new GraphQLNonNull(storeType),
      resolve: punch => punch.Store,
    },
    type: {
      type: GraphQLString,
    },
    member: {
      type: memberType,
      resolve: punch => punch.Member,
    },
  }),
});

export const employeePerformanceType = new GraphQLObjectType({
  name: 'EmployeePerformance',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
      description: '包膜師ID',
    },
    targets: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLInt)),
    },
    amounts: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLInt)),
    },
    sales: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLInt)),
    },
    achieveRate: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
    asp: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
    mom: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
    workHours: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
    expectAchieves: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
    expectProfits: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
  }),
});

export const memberDeleteResponseType = new GraphQLObjectType({
  name: 'MemberDeleteResponseType',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export const memberToggleArchiveResponseType = new GraphQLObjectType({
  name: 'MemberToggleArchiveResponseType',
  fields: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
});
