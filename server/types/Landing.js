import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLBoolean,
} from 'graphql';

export const listType = new GraphQLObjectType({
  name: 'Landings',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    desktopImg: {
      type: new GraphQLNonNull(GraphQLString),
    },
    mobileImg: {
      type: new GraphQLNonNull(GraphQLString),
    },
    link: {
      type: GraphQLString,
    },
  }),
});

export const landingDeleteResponseType = new GraphQLObjectType({
  name: 'LandingDeleteResponseType',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export default listType;
