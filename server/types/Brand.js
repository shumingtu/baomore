import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLString,
  GraphQLList,
  GraphQLInputObjectType,
  GraphQLBoolean,
} from 'graphql';

export const BrandType = new GraphQLObjectType({
  name: 'Brand',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export const BrandModelType = new GraphQLObjectType({
  name: 'BrandModel',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export const BrandModelColorType = new GraphQLObjectType({
  name: 'BrandModelColor',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phoneBg: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phoneMask: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phoneCover: {
      type: new GraphQLNonNull(GraphQLString),
    },
    brand: {
      type: BrandType,
      resolve: color => color.BrandModel.Brand,
    },
    model: {
      type: BrandModelType,
      resolve: color => color.BrandModel,
    },
  }),
});

export const BrandResponseType = new GraphQLObjectType({
  name: 'BrandResponse',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    createdAt: {
      type: new GraphQLNonNull(GraphQLString),
    },
    updatedAt: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export const BrandModelResponseType = new GraphQLObjectType({
  name: 'BrandModelResponse',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    createdAt: {
      type: new GraphQLNonNull(GraphQLString),
    },
    updatedAt: {
      type: new GraphQLNonNull(GraphQLString),
    },
    BrandId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  }),
});

export const BrandModelColorResponseType = new GraphQLObjectType({
  name: 'BrandModelColorResponse',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    createdAt: {
      type: new GraphQLNonNull(GraphQLString),
    },
    updatedAt: {
      type: new GraphQLNonNull(GraphQLString),
    },
    BrandModelId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    phoneBg: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phoneMask: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phoneCover: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export const BrandModelInputType = new GraphQLInputObjectType({
  name: 'BrandModelInput',
  fields: () => ({
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    colors: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLString)),
    },
  }),
});

export const BrandDeleteResponseType = new GraphQLObjectType({
  name: 'BrandDeleteResponseType',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export default BrandType;
