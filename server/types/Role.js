import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLList,
} from 'graphql';

import { actionType } from './Action.js';

export const roleType = new GraphQLObjectType({
  name: 'Role',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    actions: {
      type: new GraphQLList(actionType),
      resolve: role => role.Actions,
    },
  }),
});

export default null;
