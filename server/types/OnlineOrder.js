import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLBoolean,
} from 'graphql';
import moment from 'moment';
import { storeType } from './Store';
import { memberType } from './Member';
import { orderType } from './Order';

export const onlineOrderType = new GraphQLObjectType({
  name: 'OnlineOrder',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLString),
    },
    picture: {
      type: new GraphQLNonNull(GraphQLString),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phone: {
      type: new GraphQLNonNull(GraphQLString),
    },
    email: {
      type: new GraphQLNonNull(GraphQLString),
    },
    date: {
      type: new GraphQLNonNull(GraphQLString),
      resolve: order => order.date && moment(order.date).format('YYYY/MM/DD'),
    },
    time: {
      type: new GraphQLNonNull(GraphQLString),
    },
    store: {
      type: storeType,
      resolve: order => order.Store,
    },
    price: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    employee: {
      type: memberType,
      resolve: order => order.BookedMember,
    },
    createdAt: {
      type: new GraphQLNonNull(GraphQLString),
      resolve: order => order.createdAt && moment(order.createdAt).format('YYYY/MM/DD HH:mm'),
    },
    payedStatus: {
      type: new GraphQLNonNull(GraphQLString),
    },
    shippingStatus: {
      type: new GraphQLNonNull(GraphQLString),
    },
    isShared: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    order: {
      type: orderType,
      resolve: x => x.Order,
    },
  }),
});

export default onlineOrderType;
