import {
  GraphQLObjectType,
  GraphQLInputObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
  GraphQLBoolean,
} from 'graphql';

export const categoryType = new GraphQLObjectType({
  name: 'ActivityCategory',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    desktopImg: {
      type: new GraphQLNonNull(GraphQLString),
    },
    mobileImg: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export const activityFragmentType = new GraphQLObjectType({
  name: 'ActivityFragment',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    type: {
      type: new GraphQLNonNull(GraphQLString),
    },
    title: {
      type: GraphQLString,
    },
    content: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export const activityListType = new GraphQLObjectType({
  name: 'Activities',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    desktopImg: {
      type: new GraphQLNonNull(GraphQLString),
    },
    mobileImg: {
      type: new GraphQLNonNull(GraphQLString),
    },
    description: {
      type: GraphQLString,
    },
    isActived: {
      type: GraphQLBoolean,
    },
  }),
});

export const activityType = new GraphQLObjectType({
  name: 'Activity',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    desktopImg: {
      type: new GraphQLNonNull(GraphQLString),
    },
    mobileImg: {
      type: new GraphQLNonNull(GraphQLString),
    },
    description: {
      type: GraphQLString,
    },
    fragments: {
      type: new GraphQLList(activityFragmentType),
    },
  }),
});

export const activityFragmentInputType = new GraphQLInputObjectType({
  name: 'ActivityFragmentInput',
  fields: () => ({
    type: {
      type: new GraphQLNonNull(GraphQLString),
      desciption: 'TEXT',
    },
    title: {
      type: GraphQLString,
    },
    content: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export const activityDeleteResponseType = new GraphQLObjectType({
  name: 'ActivityDeleteResponseType',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export const activityToggleResponseType = new GraphQLObjectType({
  name: 'ActivityToggleResponseType',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export default categoryType;
