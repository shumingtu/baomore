import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLInt,
} from 'graphql';

export const memberStoreType = new GraphQLObjectType({
  name: 'MemberStore',
  fields: () => ({
    type: {
      type: new GraphQLNonNull(GraphQLString),
    },
    MemberId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    StoreId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  }),
});

export default null;
