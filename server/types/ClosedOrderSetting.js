import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLBoolean,
} from 'graphql';

import { PNTableType } from './PNTable';

export const ClosedOrderSettingType = new GraphQLObjectType({
  name: 'ClosedOrderSetting',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    closedDay: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    closedTime: {
      type: new GraphQLNonNull(GraphQLString),
    },
    PNTable: {
      type: PNTableType,
    },
  }),
});

export const ClosedOrderSettingDeleteResponseType = new GraphQLObjectType({
  name: 'ClosedOrderSettingDeleteResponseType',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export default null;
