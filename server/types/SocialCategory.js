import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLString,
} from 'graphql';

export const listType = new GraphQLObjectType({
  name: 'SocialCategory',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    link: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export default null;
