import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';

export const dateType = new GraphQLObjectType({
  name: 'Date',
  fields: () => ({
    date: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export const timeType = new GraphQLObjectType({
  name: 'Time',
  fields: () => ({
    time: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});
