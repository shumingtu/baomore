import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLBoolean,
  GraphQLNonNull,
} from 'graphql';

export const PayOnlineOrderResponseType = new GraphQLObjectType({
  name: 'PayOnlineOrder',
  fields: () => ({
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    message: {
      type: GraphQLString,
    },
    human: {
      type: GraphQLString,
    },
    payToken: {
      type: GraphQLString,
    },
  }),
});

export const PayConfirmResponseType = new GraphQLObjectType({
  name: 'PayConfirm',
  fields: () => ({
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    message: {
      type: GraphQLString,
    },
    human: {
      type: GraphQLString,
    },
  }),
});

// export const PayConfirmResponseType = new GraphQLObjectType({
//   name: 'PayConfirm',
//   fields: () => ({
//     status: {
//       type: new GraphQLNonNull(GraphQLBoolean),
//     },
//     message: {
//       type: GraphQLString,
//     },
//     human: {
//       type: GraphQLString,
//     },
//   }),
// });
