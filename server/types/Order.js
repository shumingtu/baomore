import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLBoolean,
} from 'graphql';

import { memberType } from './Member';
import { PNTableType } from './PNTable';
import { storeType } from './Store';
import { OrderGroupShipmentQueryType } from './OrderGroupShipment';
import { onlineOrderType } from './OnlineOrder';

export const orderQRCodeType = new GraphQLObjectType({
  name: 'OrderQRCode',
  fields: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    qrcode: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
});

export const orderType = new GraphQLObjectType({
  name: 'Order',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    quantity: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    status: {
      type: new GraphQLNonNull(GraphQLString),
    },
    orderSN: {
      type: GraphQLString,
    },
    orderSource: {
      type: GraphQLString,
    },
    Member: {
      type: memberType,
      resolve: order => order.Member,
    },
    pnTable: {
      type: PNTableType,
      resolve: order => order.PNTable,
    },
    store: {
      type: storeType,
      resolve: order => order.Store,
    },
    orderGropShipment: {
      type: OrderGroupShipmentQueryType,
      resolve: order => order.OrderGroupShipment,
    },
    onlineOrder: {
      type: onlineOrderType,
      resolve: order => order.OnlineOrder,
    },
  }),
});

export const orderDeleteResponseType = new GraphQLObjectType({
  name: 'OrderDeleteResponse',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export default null;
