import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLList,
  GraphQLFloat,
} from 'graphql';
import moment from 'moment';
import { storeType } from './Store';
import { memberType } from './Member';
import { VendorType } from './Vendor.js';
import {
  PNTableType,
  PNCategoryType,
} from './PNTable.js';

import { AreaType } from './Area';
import { ChannelType } from './Channel';

export const behaviorType = new GraphQLObjectType({
  name: 'Behavior',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export const behaviorRecordType = new GraphQLObjectType({
  name: 'BehaviorRecord',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    rollbackType: {
      type: new GraphQLNonNull(GraphQLString),
    },
    startTime: {
      type: GraphQLString,
      resolve: record => record.startTime && moment(record.startTime).format('YYYY-MM-DD HH:mm:ss'),
    },
    endTime: {
      type: GraphQLString,
      resolve: record => record.endTime && moment(record.endTime).format('YYYY-MM-DD HH:mm:ss'),
    },
    picture: {
      type: GraphQLString,
    },
    price: {
      type: GraphQLInt,
    },
    remark: {
      type: GraphQLString,
    },
    remarks: {
      type: new GraphQLList(GraphQLString),
    },
    createdAt: {
      type: new GraphQLNonNull(GraphQLString),
      resolve: record => record.createdAt && moment(record.createdAt).format('YYYY-MM-DD HH:mm'),
    },
    member: {
      type: memberType,
      resolve: record => record.Member || null,
    },
    vendor: {
      type: VendorType,
      resolve: record => record.Vendor || null,
    },
    store: {
      type: storeType,
      resolve: record => record.Store || null,
    },
    pnTable: {
      type: PNTableType,
      resolve: record => record.PNTable || null,
    },
    pnCategory: {
      type: PNCategoryType,
    },
    approver: {
      type: memberType,
      resolve: record => record.Approver,
    },
    behavior: {
      type: behaviorType,
      resolve: record => record.Behavior,
    },
    qrcode: {
      type: GraphQLString,
    },
    orderSN: {
      type: GraphQLString,
    },
  }),
});

export const verifyRollbackBehaviorRecordType = new GraphQLObjectType({
  name: 'VerifyRollbackBehaviorRecord',
  fields: () => ({
    behaviorRecords: {
      type: new GraphQLList(behaviorRecordType),
    },
    errorRecords: {
      type: new GraphQLList(behaviorRecordType),
    },
  }),
});

export const employeePerformanceForAdminType = new GraphQLObjectType({
  name: 'EmployeePerformanceForAdmin',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
      description: '包膜師ID',
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
      description: '包膜師姓名',
    },
    area: {
      type: AreaType,
    },
    targets: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLInt)),
    },
    amounts: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLInt)),
    },
    sales: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLInt)),
    },
    achieveRate: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
    asp: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
    mom: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
    workHours: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
    expectAchieves: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
    expectProfits: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
    turnover: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
    commissions: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
  }),
});

export const storePerformanceForAdminType = new GraphQLObjectType({
  name: 'StorePerformanceForAdmin',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
      description: '門市ID',
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
      description: '門市名稱',
    },
    channel: {
      type: ChannelType,
    },
    targets: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLInt)),
    },
    amounts: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLInt)),
    },
    sales: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLInt)),
    },
    achieveRate: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
    asp: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
    mom: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
    workHours: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
    expectAchieves: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
    expectProfits: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
    turnover: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
    commissions: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLFloat)),
    },
  }),
});
