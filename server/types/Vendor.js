import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLBoolean,
  GraphQLList,
} from 'graphql';

import { vendorSettingType } from './VendorSetting.js';

export const VendorType = new GraphQLObjectType({
  name: 'Vendor',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    contactPersonName: {
      type: GraphQLString,
    },
    phone: {
      type: GraphQLString,
    },
    address: {
      type: GraphQLString,
    },
    VendorSettings: {
      type: new GraphQLList(vendorSettingType),
    },
  }),
});

export const vendorDeleteResponseType = new GraphQLObjectType({
  name: 'VendorDeleteResponseType',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    status: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});


export default null;
