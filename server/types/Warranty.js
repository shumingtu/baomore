import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLBoolean,
} from 'graphql';
import moment from 'moment';

export const memberWarrantyType = new GraphQLObjectType({
  name: 'MemberWarranty',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    service: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phoneModel: {
      type: new GraphQLNonNull(GraphQLString),
    },
    store: {
      type: new GraphQLNonNull(GraphQLString),
    },
    VAT: {
      type: new GraphQLNonNull(GraphQLString),
    },
    IME: {
      type: GraphQLString,
    },
    phoneLastSix: {
      type: GraphQLString,
      resolve: warranty => (warranty.phone && warranty.phone.slice(-6)) || null,
    },
    isApproved: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    employeeCode: {
      type: GraphQLString,
      resolve: warranty => warranty.approverSN,
    },
    createdAt: {
      type: new GraphQLNonNull(GraphQLString),
      resolve: warranty => warranty.createdAt && moment(warranty.createdAt).format('YYYY/MM/DD HH:mm'),
    },
  }),
});

export default null;
