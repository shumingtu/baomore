// @flow
/* eslint import/no-cycle: 0 */
import Sequelize from 'sequelize';
import type { Model as SqlModel } from 'sequelize';
import debug from 'debug';

import Member from './models/Member';
import Action from './models/Action';
import Role from './models/Role';
import Brand from './models/Brand';
import BrandModel from './models/BrandModel';
import BrandModelColor from './models/BrandModelColor';
import Area from './models/Area';
import Channel from './models/Channel';
import Vendor from './models/Vendor';
import Store from './models/Store';
import PNCategory from './models/PNCategory';
import PNSubCategory from './models/PNSubCategory';
import PNTable from './models/PNTable';
import City from './models/City';
import District from './models/District';
import BookTimeSlotGroup from './models/BookTimeSlotGroup';
import BookTimeSlot from './models/BookTimeSlot';
import OrderGroupShipment from './models/OrderGroupShipment';
import Order from './models/Order';
import OnlineOrder from './models/OnlineOrder';
import PNTableClosedDateSetting from './models/PNTableClosedDateSetting';
import Emoji from './models/Emoji';
import EmojiCategory from './models/EmojiCategory';
import MemberStore from './models/MemberStore';
import ClosedOrderSetting from './models/ClosedOrderSetting';
import MemberStock from './models/MemberStock';
import Behavior from './models/Behavior';
import BehaviorRecord from './models/BehaviorRecord';
import Announcement from './models/Announcement';
import MemberPunchRecord from './models/MemberPunchRecord';
import PerformanceSetting from './models/PerformanceSetting';
import Banner from './models/Banner';
import Landing from './models/Landing';
import ActivityCategory from './models/ActivityCategory';
import Activity from './models/Activity';
import ActivityDetailFragment from './models/ActivityDetailFragment.js';
import MemberSchedule from './models/MemberSchedule.js';
import VendorSetting from './models/VendorSetting.js';
import ManagerPunchRecord from './models/ManagerPunchRecord.js';
import SystemLog from './models/SystemLog.js';
import SystemLogType from './models/SystemLogType.js';
import MemberWarranty from './models/MemberWarranty.js';
import OrderQRCode from './models/OrderQRCode';
import EmployeeConfirmSchedule from './models/EmployeeConfirmSchedule.js';
import EmployeeTransferSchedule from './models/EmployeeTransferSchedule.js';

const DB_URL = process.env.DB_URL || 'mysql://USER:PASSWORD@localhost/baomore';
const debugDB = debug('BAOMORE:DB');

export const db = new Sequelize(DB_URL, {
  timezone: '+08:00',
  benchmark: process.env.NODE_ENV !== 'production',
  logging: log => debugDB(log),
  define: {
    charset: 'utf8mb4',
    dialectOptions: {
      collate: 'utf8mb4_general_ci',
    },
    paranoid: true,
  },
});

Member.init(db);
Action.init(db);
Role.init(db);
Brand.init(db);
BrandModel.init(db);
BrandModelColor.init(db);
Area.init(db);
Channel.init(db);
Vendor.init(db);
Store.init(db);
PNCategory.init(db);
PNSubCategory.init(db);
PNTable.init(db);
City.init(db);
District.init(db);
BookTimeSlotGroup.init(db);
BookTimeSlot.init(db);
OrderGroupShipment.init(db);
Order.init(db);
OnlineOrder.init(db);
PNTableClosedDateSetting.init(db);
Emoji.init(db);
EmojiCategory.init(db);
MemberStore.init(db);
ClosedOrderSetting.init(db);
MemberStock.init(db);
Behavior.init(db);
BehaviorRecord.init(db);
Announcement.init(db);
MemberPunchRecord.init(db);
PerformanceSetting.init(db);
Banner.init(db);
Landing.init(db);
ActivityCategory.init(db);
Activity.init(db);
ActivityDetailFragment.init(db);
MemberSchedule.init(db);
VendorSetting.init(db);
ManagerPunchRecord.init(db);
SystemLog.init(db);
SystemLogType.init(db);
MemberWarranty.init(db);
OrderQRCode.init(db);
EmployeeConfirmSchedule.init(db);
EmployeeTransferSchedule.init(db);

debugDB('db Model Loaded.');

Object.values(db.models)
  .forEach((model: SqlModel) => model.associate(db.models));

export const MemberRole = db.models.MemberRoles;

export const RoleAction = db.models.RoleActions;

async function loadModels() {
  await db.sync();

  debugDB('db Model Syncd.');
}

export default async () => {
  await loadModels();
};
