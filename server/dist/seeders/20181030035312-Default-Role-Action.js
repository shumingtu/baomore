"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _bluebird = _interopRequireDefault(require("bluebird"));

var _roleActions = require("../../shared/roleActions.js");

var _Member = _interopRequireDefault(require("../columns/Member"));

var _Role = _interopRequireDefault(require("../columns/Role"));

var _Action = _interopRequireDefault(require("../columns/Action"));

var _RoleActions = _interopRequireDefault(require("../columns/RoleActions"));

var _MemberRoles = _interopRequireDefault(require("../columns/MemberRoles"));

var _Area = _interopRequireDefault(require("../columns/Area"));

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Areas', (0, _Area.default)(Sequelize));
    await queryInterface.createTable('Members', (0, _Member.default)(Sequelize));
    await queryInterface.createTable('Roles', (0, _Role.default)(Sequelize));
    await queryInterface.createTable('Actions', (0, _Action.default)(Sequelize));
    await queryInterface.createTable('RoleActions', (0, _RoleActions.default)(Sequelize), {
      indexes: [{
        name: 'ActionCode',
        fields: ['ActionCode']
      }, {
        name: 'RoleId',
        fields: ['RoleId']
      }]
    });
    await queryInterface.createTable('MemberRoles', (0, _MemberRoles.default)(Sequelize), {
      indexes: [{
        name: 'MemberId',
        fields: ['MemberId']
      }, {
        name: 'RoleId',
        fields: ['RoleId']
      }]
    });
    await queryInterface.bulkInsert('Roles', Object.values(_roleActions.roles).map(role => ({
      id: role.id,
      name: role.name,
      createdAt: new Date(),
      updatedAt: new Date()
    })));
    await queryInterface.bulkInsert('Actions', Object.values(_roleActions.actions).map(action => ({ ...action,
      createdAt: new Date(),
      updatedAt: new Date()
    })));
    await Object.values(_roleActions.roles).map(role => () => queryInterface.bulkInsert('RoleActions', role.actions.map(action => ({
      ActionCode: action.code,
      RoleId: role.id,
      createdAt: new Date(),
      updatedAt: new Date()
    })))).reduce((prev, next) => prev.then(next), _bluebird.default.resolve());
  },
  down: async queryInterface => {
    await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 0');
    await queryInterface.dropTable('RoleActions');
    await queryInterface.dropTable('Actions');
    await queryInterface.dropTable('Roles');
    await queryInterface.dropTable('Areas');
    await queryInterface.dropTable('Members');
    await queryInterface.dropTable('MemberRoles');
    await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 1');
  }
};