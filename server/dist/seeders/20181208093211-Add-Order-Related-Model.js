"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _BookTimeSlotGroup = _interopRequireDefault(require("../columns/BookTimeSlotGroup"));

var _BookTimeSlot = _interopRequireDefault(require("../columns/BookTimeSlot"));

var _Order = _interopRequireDefault(require("../columns/Order"));

var _OnlineOrder = _interopRequireDefault(require("../columns/OnlineOrder"));

var _OrderGroupShipment = _interopRequireDefault(require("../columns/OrderGroupShipment"));

var _PNTableClosedDateSetting = _interopRequireDefault(require("../columns/PNTableClosedDateSetting"));

var _Emoji = _interopRequireDefault(require("../columns/Emoji"));

var _EmojiCategory = _interopRequireDefault(require("../columns/EmojiCategory"));

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('BookTimeSlotGroups', (0, _BookTimeSlotGroup.default)(Sequelize));
    await queryInterface.createTable('BookTimeSlots', (0, _BookTimeSlot.default)(Sequelize));
    await queryInterface.createTable('OrderGroupShipments', (0, _OrderGroupShipment.default)(Sequelize));
    await queryInterface.createTable('Orders', (0, _Order.default)(Sequelize));
    await queryInterface.createTable('OnlineOrders', (0, _OnlineOrder.default)(Sequelize));
    await queryInterface.createTable('PNTableClosedDateSettings', (0, _PNTableClosedDateSetting.default)(Sequelize));
    await queryInterface.createTable('EmojiCategories', (0, _EmojiCategory.default)(Sequelize));
    await queryInterface.createTable('Emojis', (0, _Emoji.default)(Sequelize)); // BookTimeSlotGroup Mocks

    await queryInterface.bulkInsert('BookTimeSlotGroups', [{
      name: '下午',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '晚上',
      createdAt: new Date(),
      updatedAt: new Date()
    }]); // BookTimeSlot Mocks

    await queryInterface.bulkInsert('BookTimeSlots', [{
      time: '13:00',
      BookTimeSlotGroupId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      time: '14:00',
      BookTimeSlotGroupId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      time: '15:00',
      BookTimeSlotGroupId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      time: '16:00',
      BookTimeSlotGroupId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      time: '17:00',
      BookTimeSlotGroupId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      time: '18:00',
      BookTimeSlotGroupId: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      time: '19:00',
      BookTimeSlotGroupId: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      time: '20:00',
      BookTimeSlotGroupId: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
    await queryInterface.bulkInsert('PNTableClosedDateSettings', [{
      weekDay: 'WED',
      time: '10:00',
      createdAt: new Date(),
      updatedAt: new Date()
    }]); // Emoji Categroies

    await queryInterface.bulkInsert('EmojiCategories', [{
      name: 'Facebook Emoji',
      createdAt: new Date(),
      updatedAt: new Date()
    }]); // Emoji Mocks

    await queryInterface.bulkInsert('Emojis', [{
      picture: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/emoji-1.png',
      EmojiCategoryId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      picture: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/emoji-2.png',
      EmojiCategoryId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      picture: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/emoji-3.png',
      EmojiCategoryId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  },
  down: async queryInterface => {
    await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 0');
    await queryInterface.dropTable('BookTimeSlotGroups');
    await queryInterface.dropTable('BookTimeSlots');
    await queryInterface.dropTable('OrderGroupShipments');
    await queryInterface.dropTable('Orders');
    await queryInterface.dropTable('OnlineOrders');
    await queryInterface.dropTable('PNTableClosedDateSettings');
    await queryInterface.dropTable('EmojiCategories');
    await queryInterface.dropTable('Emojis');
    await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 1');
  }
};