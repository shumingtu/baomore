"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _PNTable = _interopRequireDefault(require("../columns/PNTable"));

var _PNCategory = _interopRequireDefault(require("../columns/PNCategory"));

var _PNSubCategory = _interopRequireDefault(require("../columns/PNSubCategory"));

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('PNCategories', (0, _PNCategory.default)(Sequelize));
    await queryInterface.createTable('PNSubCategories', (0, _PNSubCategory.default)(Sequelize));
    await queryInterface.createTable('PNTables', (0, _PNTable.default)(Sequelize)); // Mocks PNCategory

    await queryInterface.bulkInsert('PNCategories', [{
      name: '膜料',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '保貼',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '工具類',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '其他類',
      createdAt: new Date(),
      updatedAt: new Date()
    }]); // Mocks PNSubCategory

    await queryInterface.bulkInsert('PNSubCategories', [{
      name: '基礎膜料',
      PNCategoryId: 1,
      isOnSale: false,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '設計款浮雕包膜',
      picture: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/SubCategory2.png',
      PNCategoryId: 1,
      seqNum: 2,
      createdAt: new Date(),
      updatedAt: new Date(),
      isOnSale: true
    }, {
      name: '授權款浮雕包膜',
      picture: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/SubCategory3.png',
      isOnSale: true,
      PNCategoryId: 1,
      seqNum: 3,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '授權款壓紋包膜',
      PNCategoryId: 1,
      isOnSale: false,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '客製化包膜',
      picture: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/SubCategory1.png',
      isOnSale: true,
      PNCategoryId: 1,
      seqNum: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '工具',
      PNCategoryId: 3,
      isOnSale: false,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '其他類',
      PNCategoryId: 4,
      isOnSale: false,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '會員作品共享包膜',
      picture: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/SubCategory4.png',
      PNCategoryId: 1,
      isOnSale: true,
      seqNum: 4,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: 'LINTEC霧膜',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '超透亮膜',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '壓紋膜木紋',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '壓紋膜五芒星',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '壓紋膜卡夢',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '壓紋膜小三角',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '壓紋膜立體方格',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '壓紋膜交叉髮絲',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '壓紋膜3D三角',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '壓紋膜楓葉',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '壓紋膜大羽毛',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '3D立體浮雕膜',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '5D立體浮雕膜',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '3D浮雕膜 MARVEL系列',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 3,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '5D浮雕膜 MARVEL系列',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 3,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '3D浮雕膜 迪士尼系列',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 3,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '5D浮雕膜 迪士尼系列',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 3,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '3D浮雕膜 三麗鷗系列',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 3,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '5D浮雕膜 三麗鷗系列',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 3,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: 'MARVEL系列',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 4,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '三麗鷗系列',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 4,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: 'LINTEC邊條-亮',
      PNCategoryId: 3,
      isOnSale: false,
      PNSubCategoryId: 6,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: 'LINTEC邊條-霧',
      PNCategoryId: 3,
      isOnSale: false,
      PNSubCategoryId: 6,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '超透亮邊條',
      PNCategoryId: 3,
      isOnSale: false,
      PNSubCategoryId: 6,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '熱風槍組',
      PNCategoryId: 3,
      isOnSale: false,
      PNSubCategoryId: 6,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '推版',
      PNCategoryId: 3,
      isOnSale: false,
      PNSubCategoryId: 6,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '美工刀',
      PNCategoryId: 3,
      isOnSale: false,
      PNSubCategoryId: 6,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '客製化次分類',
      PNCategoryId: 1,
      isOnSale: false,
      PNSubCategoryId: 5,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '品牌保貼',
      PNCategoryId: 2,
      isOnSale: false,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '通用保貼',
      PNCategoryId: 2,
      isOnSale: false,
      createdAt: new Date(),
      updatedAt: new Date()
    }]); // Mocks PNTable

    await queryInterface.bulkInsert('PNTables', [{
      code: '12345678',
      name: '客製化料號',
      description: '自由打造獨特個人風格客製化手機殼、最具意義的紀念品讓精彩感動、隨時在你身邊',
      isOnSale: true,
      price: 1800,
      onlinePrice: 1800,
      PNSubCategoryId: 35,
      createdAt: new Date(),
      updatedAt: new Date(),
      VendorId: 1
    }, {
      code: '890708010056',
      name: '授權5D立體浮雕膜小熊維尼系列WIN-01',
      description: '授權5D立體浮雕膜小熊維尼系列WIN-01',
      isOnSale: true,
      price: 120,
      onlinePrice: 120,
      PNSubCategoryId: 24,
      createdAt: new Date(),
      updatedAt: new Date(),
      VendorId: 1
    }, {
      code: '890708010052',
      name: '授權5D立體浮雕膜MELODY系列MLD-03',
      description: '授權5D立體浮雕膜MELODY系列MLD-03',
      isOnSale: true,
      price: 180,
      onlinePrice: 180,
      PNSubCategoryId: 26,
      createdAt: new Date(),
      updatedAt: new Date(),
      VendorId: 1
    }, {
      code: '890708010088',
      name: '授權3D浮雕膜 MARVEL 系列MRL-01',
      description: '授權3D浮雕膜 MARVEL 系列MRL-01',
      picture: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/Marvel1.png',
      isOnSale: true,
      price: 180,
      onlinePrice: 180,
      PNSubCategoryId: 21,
      createdAt: new Date(),
      updatedAt: new Date(),
      VendorId: 1
    }, {
      code: '890708010089',
      name: '授權3D浮雕膜 MARVEL 系列MRL-01',
      description: '授權3D浮雕膜 MARVEL 系列MRL-01',
      picture: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/Marvel2.png',
      isOnSale: true,
      price: 180,
      onlinePrice: 180,
      PNSubCategoryId: 21,
      createdAt: new Date(),
      updatedAt: new Date(),
      VendorId: 1
    }, {
      code: '890708010090',
      name: '授權3D浮雕膜 MARVEL 系列MRL-01',
      description: '授權3D浮雕膜 MARVEL 系列MRL-01',
      picture: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/Marvel3.png',
      isOnSale: true,
      price: 180,
      onlinePrice: 180,
      PNSubCategoryId: 21,
      createdAt: new Date(),
      updatedAt: new Date(),
      VendorId: 1
    }, {
      code: '690201010098',
      name: 'artmo 2.5D滿版玻璃貼 Apple iPhone XR-黑 (簡配)',
      description: 'artmo 2.5D滿版玻璃貼 Apple iPhone XR-黑 (簡配)',
      isOnSale: false,
      price: 2600,
      PNSubCategoryId: 36,
      BrandModelId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
      VendorId: 1
    }]);
  },
  down: async queryInterface => {
    await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 0');
    await queryInterface.dropTable('PNTables');
    await queryInterface.dropTable('PNCategories');
    await queryInterface.dropTable('PNSubCategories');
    await queryInterface.dropTable('PNImages');
    await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 1');
  }
};