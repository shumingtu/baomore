"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _Brand = _interopRequireDefault(require("../columns/Brand"));

var _BrandModels = _interopRequireDefault(require("../columns/BrandModels"));

var _BrandModelColors = _interopRequireDefault(require("../columns/BrandModelColors"));

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Brands', (0, _Brand.default)(Sequelize));
    await queryInterface.createTable('BrandModels', (0, _BrandModels.default)(Sequelize), {
      indexes: [{
        name: 'BrandId',
        fields: ['BrandId']
      }]
    });
    await queryInterface.createTable('BrandModelColors', (0, _BrandModelColors.default)(Sequelize), {
      indexes: [{
        name: 'BrandModelId',
        fields: ['BrandModelId']
      }]
    });
    await queryInterface.bulkInsert('Brands', [{
      id: 1,
      name: 'iphone',
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
    await queryInterface.bulkInsert('BrandModels', [{
      id: 1,
      name: 'iphone X',
      createdAt: new Date(),
      updatedAt: new Date(),
      BrandId: 1
    }, {
      id: 2,
      name: 'iphone XS',
      createdAt: new Date(),
      updatedAt: new Date(),
      BrandId: 1
    }]);
    await queryInterface.bulkInsert('BrandModelColors', [{
      id: 1,
      name: '銀白色',
      phoneBg: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/iphoneX.png',
      phoneMask: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/iphoneX_mask.png',
      phoneCover: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/iphoneX_cover.png',
      createdAt: new Date(),
      updatedAt: new Date(),
      BrandModelId: 1
    }, {
      id: 2,
      name: '太空灰',
      phoneBg: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/iphoneX.png',
      phoneMask: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/iphoneX_mask.png',
      phoneCover: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/iphoneX_cover.png',
      createdAt: new Date(),
      updatedAt: new Date(),
      BrandModelId: 1
    }, {
      id: 3,
      name: '金色',
      phoneBg: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/iphoneX.png',
      phoneMask: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/iphoneX_mask.png',
      phoneCover: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/iphoneX_cover.png',
      createdAt: new Date(),
      updatedAt: new Date(),
      BrandModelId: 2
    }, {
      id: 4,
      name: '銀色',
      phoneBg: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/iphoneX.png',
      phoneMask: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/iphoneX_mask.png',
      phoneCover: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/iphoneX_cover.png',
      createdAt: new Date(),
      updatedAt: new Date(),
      BrandModelId: 2
    }, {
      id: 5,
      name: '太空灰',
      phoneBg: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/iphoneX.png',
      phoneMask: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/iphoneX_mask.png',
      phoneCover: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/iphoneX_cover.png',
      createdAt: new Date(),
      updatedAt: new Date(),
      BrandModelId: 2
    }]);
  },
  down: async queryInterface => {
    await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 0');
    await queryInterface.dropTable('Brands');
    await queryInterface.dropTable('BrandModels');
    await queryInterface.dropTable('BrandModelColors');
    await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 1');
  }
};