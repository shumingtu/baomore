"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _maps = _interopRequireDefault(require("@google/maps"));

var _Channel = _interopRequireDefault(require("../columns/Channel"));

var _Store = _interopRequireDefault(require("../columns/Store"));

var _Vendor = _interopRequireDefault(require("../columns/Vendor"));

var _City = _interopRequireDefault(require("../columns/City"));

var _District = _interopRequireDefault(require("../columns/District"));

var _env = require("../../shared/env");

var _district = _interopRequireDefault(require("../mocks/district"));

const googleMapClient = _maps.default.createClient({
  key: _env.GOOGLE_MAP_API_KEY,
  Promise
});

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Vendors', (0, _Vendor.default)(Sequelize));
    await queryInterface.createTable('Channels', (0, _Channel.default)(Sequelize));
    await queryInterface.createTable('Cities', (0, _City.default)(Sequelize));
    await queryInterface.createTable('Districts', (0, _District.default)(Sequelize));
    await queryInterface.createTable('Stores', (0, _Store.default)(Sequelize)); // Mocks Areas

    await queryInterface.bulkInsert('Areas', [{
      name: '北區',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '中區',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '南區',
      createdAt: new Date(),
      updatedAt: new Date()
    }]); // Mocks Channels

    await queryInterface.bulkInsert('Channels', [{
      name: '神腦',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '台哥大',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '遠傳',
      createdAt: new Date(),
      updatedAt: new Date()
    }]); // Mocks Vendors

    await queryInterface.bulkInsert('Vendors', [{
      name: '彩漾',
      createdAt: new Date(),
      updatedAt: new Date()
    }]); // Mocks Cities & Districts

    await (0, _district.default)(); // Mocks Stores

    await queryInterface.bulkInsert('Stores', [{
      name: '台東更生二',
      phone: '0917740622',
      address: '台東市更生路115號',
      ChannelId: 1,
      AreaId: 1,
      DistrictId: 338,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '永和永和',
      phone: '0922688300',
      address: '新北市永和區永和路一段55號',
      ChannelId: 3,
      AreaId: 1,
      DistrictId: 33,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: '林口竹林',
      phone: '0981129330',
      address: '新北市林口區竹林路27號',
      ChannelId: 1,
      AreaId: 1,
      DistrictId: 42,
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
    const rawStores = await queryInterface.sequelize.query('SELECT * FROM Stores');
    const stores = rawStores[0];
    await Promise.all(stores.map(store => googleMapClient.geocode({
      address: store.address
    }).asPromise().then(async response => {
      const {
        location
      } = response.json.results[0].geometry;
      await queryInterface.sequelize.query(`UPDATE Stores SET latitude = ${location.lat},longitude = ${location.lng} WHERE id=${store.id};`);
    }).catch(err => {
      console.log(err);
    })));
  },
  down: async queryInterface => {
    await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 0');
    await queryInterface.dropTable('Stores');
    await queryInterface.dropTable('Cities');
    await queryInterface.dropTable('Districts');
    await queryInterface.dropTable('Vendors');
    await queryInterface.dropTable('Channels');
    await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 1');
  }
};