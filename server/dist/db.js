"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.RoleAction = exports.MemberRole = exports.db = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _debug = _interopRequireDefault(require("debug"));

var _Member = _interopRequireDefault(require("./models/Member"));

var _Action = _interopRequireDefault(require("./models/Action"));

var _Role = _interopRequireDefault(require("./models/Role"));

var _Brand = _interopRequireDefault(require("./models/Brand"));

var _BrandModel = _interopRequireDefault(require("./models/BrandModel"));

var _BrandModelColor = _interopRequireDefault(require("./models/BrandModelColor"));

var _Area = _interopRequireDefault(require("./models/Area"));

var _Channel = _interopRequireDefault(require("./models/Channel"));

var _Vendor = _interopRequireDefault(require("./models/Vendor"));

var _Store = _interopRequireDefault(require("./models/Store"));

var _PNCategory = _interopRequireDefault(require("./models/PNCategory"));

var _PNSubCategory = _interopRequireDefault(require("./models/PNSubCategory"));

var _PNTable = _interopRequireDefault(require("./models/PNTable"));

var _City = _interopRequireDefault(require("./models/City"));

var _District = _interopRequireDefault(require("./models/District"));

var _BookTimeSlotGroup = _interopRequireDefault(require("./models/BookTimeSlotGroup"));

var _BookTimeSlot = _interopRequireDefault(require("./models/BookTimeSlot"));

var _OrderGroupShipment = _interopRequireDefault(require("./models/OrderGroupShipment"));

var _Order = _interopRequireDefault(require("./models/Order"));

var _OnlineOrder = _interopRequireDefault(require("./models/OnlineOrder"));

var _PNTableClosedDateSetting = _interopRequireDefault(require("./models/PNTableClosedDateSetting"));

var _Emoji = _interopRequireDefault(require("./models/Emoji"));

var _EmojiCategory = _interopRequireDefault(require("./models/EmojiCategory"));

var _MemberStore = _interopRequireDefault(require("./models/MemberStore"));

var _ClosedOrderSetting = _interopRequireDefault(require("./models/ClosedOrderSetting"));

var _MemberStock = _interopRequireDefault(require("./models/MemberStock"));

var _Behavior = _interopRequireDefault(require("./models/Behavior"));

var _BehaviorRecord = _interopRequireDefault(require("./models/BehaviorRecord"));

var _Announcement = _interopRequireDefault(require("./models/Announcement"));

var _MemberPunchRecord = _interopRequireDefault(require("./models/MemberPunchRecord"));

var _PerformanceSetting = _interopRequireDefault(require("./models/PerformanceSetting"));

var _Banner = _interopRequireDefault(require("./models/Banner"));

var _Landing = _interopRequireDefault(require("./models/Landing"));

var _ActivityCategory = _interopRequireDefault(require("./models/ActivityCategory"));

var _Activity = _interopRequireDefault(require("./models/Activity"));

var _ActivityDetailFragment = _interopRequireDefault(require("./models/ActivityDetailFragment.js"));

var _MemberSchedule = _interopRequireDefault(require("./models/MemberSchedule.js"));

var _VendorSetting = _interopRequireDefault(require("./models/VendorSetting.js"));

var _ManagerPunchRecord = _interopRequireDefault(require("./models/ManagerPunchRecord.js"));

var _SystemLog = _interopRequireDefault(require("./models/SystemLog.js"));

var _SystemLogType = _interopRequireDefault(require("./models/SystemLogType.js"));

var _MemberWarranty = _interopRequireDefault(require("./models/MemberWarranty.js"));

var _OrderQRCode = _interopRequireDefault(require("./models/OrderQRCode"));

var _EmployeeConfirmSchedule = _interopRequireDefault(require("./models/EmployeeConfirmSchedule.js"));

var _EmployeeTransferSchedule = _interopRequireDefault(require("./models/EmployeeTransferSchedule.js"));

/* eslint import/no-cycle: 0 */
const DB_URL = process.env.DB_URL || 'mysql://rytass:rytass2O15@localhost/baomore';
const debugDB = (0, _debug.default)('BAOMORE:DB');
const db = new _sequelize.default(DB_URL, {
  timezone: '+08:00',
  benchmark: process.env.NODE_ENV !== 'production',
  logging: log => debugDB(log),
  define: {
    charset: 'utf8mb4',
    dialectOptions: {
      collate: 'utf8mb4_general_ci'
    },
    paranoid: true
  }
});
exports.db = db;

_Member.default.init(db);

_Action.default.init(db);

_Role.default.init(db);

_Brand.default.init(db);

_BrandModel.default.init(db);

_BrandModelColor.default.init(db);

_Area.default.init(db);

_Channel.default.init(db);

_Vendor.default.init(db);

_Store.default.init(db);

_PNCategory.default.init(db);

_PNSubCategory.default.init(db);

_PNTable.default.init(db);

_City.default.init(db);

_District.default.init(db);

_BookTimeSlotGroup.default.init(db);

_BookTimeSlot.default.init(db);

_OrderGroupShipment.default.init(db);

_Order.default.init(db);

_OnlineOrder.default.init(db);

_PNTableClosedDateSetting.default.init(db);

_Emoji.default.init(db);

_EmojiCategory.default.init(db);

_MemberStore.default.init(db);

_ClosedOrderSetting.default.init(db);

_MemberStock.default.init(db);

_Behavior.default.init(db);

_BehaviorRecord.default.init(db);

_Announcement.default.init(db);

_MemberPunchRecord.default.init(db);

_PerformanceSetting.default.init(db);

_Banner.default.init(db);

_Landing.default.init(db);

_ActivityCategory.default.init(db);

_Activity.default.init(db);

_ActivityDetailFragment.default.init(db);

_MemberSchedule.default.init(db);

_VendorSetting.default.init(db);

_ManagerPunchRecord.default.init(db);

_SystemLog.default.init(db);

_SystemLogType.default.init(db);

_MemberWarranty.default.init(db);

_OrderQRCode.default.init(db);

_EmployeeConfirmSchedule.default.init(db);

_EmployeeTransferSchedule.default.init(db);

debugDB('db Model Loaded.');
Object.values(db.models).forEach(model => model.associate(db.models));
const MemberRole = db.models.MemberRoles;
exports.MemberRole = MemberRole;
const RoleAction = db.models.RoleActions;
exports.RoleAction = RoleAction;

async function loadModels() {
  await db.sync();
  debugDB('db Model Syncd.');
}

var _default = async () => {
  await loadModels();
};

exports.default = _default;