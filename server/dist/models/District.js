"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _District = _interopRequireDefault(require("../columns/District.js"));

var _db = require("../db");

class District extends _sequelize.Model {
  static init(sequelize) {
    return super.init(District.fields, {
      sequelize,
      scopes: District.scopes
    });
  }

  static associate() {
    District.belongsTo(_db.db.models.City);
  }

}

exports.default = District;
(0, _defineProperty2.default)(District, "fields", (0, _District.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(District, "scopes", {
  findByCity: cityId => ({
    where: {
      CityId: cityId
    }
  })
});