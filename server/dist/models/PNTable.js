"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _PNTable = _interopRequireDefault(require("../columns/PNTable.js"));

var _db = require("../db");

var _PNTable2 = require("../errors/PNTable");

class PNTable extends _sequelize.Model {
  static init(sequelize) {
    return super.init(PNTable.fields, {
      sequelize,
      scopes: PNTable.scopes
    });
  }

  static associate() {
    PNTable.belongsTo(_db.db.models.PNSubCategory);
    PNTable.belongsTo(_db.db.models.BrandModel);
    PNTable.belongsTo(_db.db.models.Vendor);
    PNTable.hasMany(_db.db.models.MemberStock);
    PNTable.belongsToMany(_db.db.models.Member, {
      through: _db.db.models.MemberStock,
      foreignKey: 'PNTableId',
      otherKey: 'MemberId'
    });
    PNTable.hasMany(_db.db.models.Order);
  }

  static async editPNTableItem({
    id,
    code,
    pnCategoryId,
    bigSubCategoryId,
    smallSubCategoryId,
    BrandModelId,
    VendorId,
    name,
    picture,
    price,
    device,
    description,
    isOnSale,
    onlinePrice
  }) {
    const pnTableItem = await _db.db.models.PNTable.findOne({
      where: {
        id
      }
    });

    if (!pnTableItem) {
      throw new _PNTable2.PNTableItemNotFoundError();
    }

    pnTableItem.code = code;
    pnTableItem.PNSubCategoryId = !smallSubCategoryId || smallSubCategoryId === -1 ? bigSubCategoryId : smallSubCategoryId;
    pnTableItem.BrandModelId = BrandModelId || null;
    pnTableItem.VendorId = VendorId;
    pnTableItem.name = name;
    pnTableItem.picture = picture;
    pnTableItem.price = price;
    pnTableItem.suitableDevice = device;
    pnTableItem.description = description;
    pnTableItem.isOnSale = isOnSale;
    pnTableItem.onlinePrice = onlinePrice || null;
    await pnTableItem.save();
    const pnCategory = await _db.db.models.PNCategory.scope({
      method: ['search', pnCategoryId]
    }).findOne();
    const bigSubCategory = await _db.db.models.PNSubCategory.scope({
      method: ['findById', bigSubCategoryId]
    }).findOne();
    const smallSubCategory = await _db.db.models.PNSubCategory.scope({
      method: ['findById', smallSubCategoryId]
    }).findOne();
    const vendor = await _db.db.models.Vendor.findByPk(VendorId);
    return { ...pnTableItem.dataValues,
      pnCategory,
      bigSubCategory,
      smallSubCategory,
      Vendor: vendor
    };
  }

  static async createPNTableItem({
    code,
    pnCategoryId,
    bigSubCategoryId,
    smallSubCategoryId,
    BrandModelId,
    VendorId,
    name,
    picture,
    price,
    device,
    description,
    isOnSale,
    onlinePrice
  }) {
    const pnTableItem = await _db.db.models.PNTable.create({
      code,
      PNSubCategoryId: !smallSubCategoryId || smallSubCategoryId === -1 ? bigSubCategoryId : smallSubCategoryId,
      BrandModelId: BrandModelId || null,
      VendorId,
      name,
      picture,
      price,
      suitableDevice: device,
      description,
      isOnSale,
      onlinePrice: onlinePrice || null
    });

    if (!pnTableItem) {
      throw new _PNTable2.PNTableCreateError();
    }

    const pnCategory = await _db.db.models.PNCategory.scope({
      method: ['search', pnCategoryId]
    }).findOne();
    const bigSubCategory = await _db.db.models.PNSubCategory.scope({
      method: ['findById', bigSubCategoryId]
    }).findOne();
    const smallSubCategory = await _db.db.models.PNSubCategory.scope({
      method: ['findById', smallSubCategoryId]
    }).findOne();
    const vendor = await _db.db.models.Vendor.scope({
      method: ['search', VendorId]
    }).findOne();
    return { ...pnTableItem.dataValues,
      pnCategory,
      bigSubCategory,
      smallSubCategory,
      vendor
    };
  }

  static async deletePNTableById({
    id
  }) {
    const pnTableItem = await _db.db.models.PNTable.findOne({
      where: {
        id
      }
    });

    if (!pnTableItem) {
      throw new _PNTable2.PNTableItemNotFoundError();
    }

    await pnTableItem.destroy();
    return {
      id: pnTableItem.id,
      status: true,
      message: '成功刪除'
    };
  }

}

exports.default = PNTable;
(0, _defineProperty2.default)(PNTable, "fields", (0, _PNTable.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(PNTable, "scopes", {
  findCode: code => {
    const whereClause = {
      [_sequelize.Op.or]: [{
        code: {
          [_sequelize.Op.like]: `%${code}%`
        }
      }]
    };
    return {
      where: whereClause
    };
  },
  search: args => {
    const {
      id,
      code,
      keyword,
      isAllCustomized
    } = args;
    let whereClause = {};
    if (id) whereClause.id = id;
    if (code) whereClause.code = code;
    if (isAllCustomized) whereClause.isAllCustomized = isAllCustomized;

    if (keyword) {
      whereClause = { ...whereClause,
        [_sequelize.Op.or]: [{
          name: {
            [_sequelize.Op.like]: `%${keyword}%`
          }
        }, {
          code: {
            [_sequelize.Op.like]: `%${keyword}%`
          }
        }]
      };
    }

    return {
      where: whereClause
    };
  },
  pages: (limit, offset) => ({
    limit,
    offset
  }),
  filterBySubCategory: subCategoryId => ({
    include: [{
      model: _db.db.models.PNSubCategory.scope({
        method: ['findById', subCategoryId]
      })
    }]
  }),
  stocks: (id, requireFlag) => {
    const whereClause = {
      MemberId: id
    };

    if (requireFlag) {
      whereClause.storageCount = {
        [_sequelize.Op.ne]: 0
      };
    }

    return {
      include: [{
        model: _db.db.models.MemberStock,
        where: whereClause,
        required: !!requireFlag
      }]
    };
  },
  brandAndModel: (brandId, brandModelId) => ({
    include: [{
      model: _db.db.models.BrandModel.scope({
        method: ['search', {
          id: brandModelId,
          brandId
        }]
      }),
      include: [{
        model: _db.db.models.Brand
      }],
      required: !!brandId
    }]
  }),
  categories: (pnCategoryId, bigSubCategoryId, smallSubCategoryId) => ({
    include: [{
      model: _db.db.models.PNSubCategory.scope({
        method: ['search', {
          id: smallSubCategoryId,
          pnSubCategoryId: bigSubCategoryId,
          pnCategoryId
        }]
      }),
      include: [{
        model: _db.db.models.PNCategory
      }, {
        model: _db.db.models.PNSubCategory
      }]
    }]
  }),
  vendor: vendorId => ({
    include: [{
      model: _db.db.models.Vendor.scope({
        method: ['search', {
          id: vendorId
        }]
      }),
      include: [{
        model: _db.db.models.VendorSetting
      }]
    }]
  })
});