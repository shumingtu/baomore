"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _Area = _interopRequireDefault(require("../columns/Area.js"));

var _db = require("../db");

class Area extends _sequelize.Model {
  static init(sequelize) {
    return super.init(Area.fields, {
      sequelize,
      scopes: Area.scopes
    });
  }

  static associate() {
    Area.hasMany(_db.db.models.Member);
  }

}

exports.default = Area;
(0, _defineProperty2.default)(Area, "fields", (0, _Area.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(Area, "scopes", {
  search: args => {
    const {
      areaId
    } = args;
    const whereClause = {};
    if (areaId) whereClause.id = areaId;
    return {
      where: whereClause
    };
  },
  findById: areaId => ({
    where: {
      id: areaId
    }
  })
});