"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _MemberStore = _interopRequireDefault(require("../columns/MemberStore.js"));

var _db = require("../db");

class MemberStore extends _sequelize.Model {
  static init(sequelize) {
    return super.init(MemberStore.fields, {
      sequelize,
      indexes: MemberStore.indexes,
      paranoid: false
    });
  }

  static associate() {
    MemberStore.belongsTo(_db.db.models.Member);
    MemberStore.belongsTo(_db.db.models.Store);
  }

}

exports.default = MemberStore;
(0, _defineProperty2.default)(MemberStore, "fields", (0, _MemberStore.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(MemberStore, "indexes", [{
  name: 'MemberId',
  fields: ['MemberId']
}, {
  name: 'StoreId',
  fields: ['StoreId']
}]);