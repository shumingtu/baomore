"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _moment = _interopRequireDefault(require("moment"));

var _OnlineOrder = _interopRequireDefault(require("../columns/OnlineOrder.js"));

var _db = require("../db");

class OnlineOrder extends _sequelize.Model {
  static init(sequelize) {
    return super.init(OnlineOrder.fields, {
      sequelize,
      defaultScope: OnlineOrder.defaultScope,
      scopes: OnlineOrder.scopes
    });
  }

  static associate() {
    OnlineOrder.belongsTo(_db.db.models.Store);
    OnlineOrder.belongsTo(_db.db.models.Member, {
      as: 'BookedMember'
    });
    OnlineOrder.belongsTo(_db.db.models.Member, {
      as: 'Owner'
    });
    OnlineOrder.belongsTo(_db.db.models.Order);
    OnlineOrder.belongsTo(_db.db.models.PNTable);
  }

}

exports.default = OnlineOrder;
(0, _defineProperty2.default)(OnlineOrder, "fields", (0, _OnlineOrder.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(OnlineOrder, "defaultScope", {
  order: [['createdAt', 'DESC']]
});
(0, _defineProperty2.default)(OnlineOrder, "scopes", {
  search: args => {
    const {
      storeId,
      bookedDate,
      bookedEndDate,
      bookedMemberIds,
      date,
      orderId,
      startDate,
      endDate,
      payedStatus,
      bookedMemberId,
      keyword,
      shippingStatus
    } = args;
    let whereClause = {};
    if (storeId) whereClause.StoreId = storeId;

    if (bookedDate || bookedEndDate) {
      const condition = [];

      if (bookedDate) {
        condition.push({
          [_sequelize.Op.gte]: (0, _moment.default)(bookedDate)
        });
      }

      if (bookedEndDate) {
        condition.push({
          [_sequelize.Op.lte]: (0, _moment.default)(bookedEndDate)
        });
      }

      whereClause.date = {
        [_sequelize.Op.and]: condition
      };
    }

    if (bookedMemberIds && bookedMemberIds.length) {
      whereClause.BookedMemberId = {
        [_sequelize.Op.in]: bookedMemberIds
      };
    }

    if (date) whereClause.date = (0, _moment.default)(date);
    if (orderId) whereClause.id = orderId;

    if (startDate) {
      whereClause.createdAt = {
        [_sequelize.Op.and]: [{
          [_sequelize.Op.gte]: (0, _moment.default)(startDate)
        }, {
          [_sequelize.Op.lte]: endDate && (0, _moment.default)(endDate) || (0, _moment.default)()
        }]
      };
    }

    if (payedStatus) whereClause.payedStatus = payedStatus;
    if (shippingStatus) whereClause.shippingStatus = shippingStatus;
    if (bookedMemberId) whereClause.BookedMemberId = bookedMemberId;

    if (keyword) {
      whereClause = { ...whereClause,
        [_sequelize.Op.or]: [{
          name: {
            [_sequelize.Op.like]: `%${keyword}%`
          }
        }, {
          phone: {
            [_sequelize.Op.like]: `%${keyword}%`
          }
        }, {
          email: {
            [_sequelize.Op.like]: `%${keyword}%`
          }
        }]
      };
    }

    return {
      where: whereClause
    };
  },
  store: () => ({
    include: [{
      model: _db.db.models.Store,
      include: [{
        model: _db.db.models.Member
      }, {
        model: _db.db.models.Channel
      }]
    }]
  }),
  pages: (limit, offset) => ({
    limit,
    offset
  }),
  order: {
    order: [['createdAt', 'DESC']]
  },
  bookedMember: memberId => ({
    include: [{
      model: _db.db.models.Member,
      as: 'BookedMember',
      where: memberId ? {
        id: memberId
      } : {},
      required: !!memberId
    }]
  }),
  member: (archiveLimit = false) => {
    const whereClause = {
      lineId: {
        [_sequelize.Op.ne]: null
      }
    };
    if (archiveLimit) whereClause.archive = false;
    return {
      include: [{
        model: _db.db.models.Member,
        as: 'Owner',
        where: whereClause
      }]
    };
  },
  payedAndConfirmed: {
    where: {
      payedStatus: 'PAIDANDCONFIRMED'
    }
  }
});