"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _MemberWarranty = _interopRequireDefault(require("../columns/MemberWarranty.js"));

var _db = require("../db");

class MemberWarranty extends _sequelize.Model {
  static init(sequelize) {
    return super.init(MemberWarranty.fields, {
      sequelize,
      scopes: MemberWarranty.scopes
    });
  }

  static associate() {
    MemberWarranty.belongsTo(_db.db.models.Member);
    MemberWarranty.belongsTo(_db.db.models.Member, {
      as: 'Approver'
    });
  }

}

exports.default = MemberWarranty;
(0, _defineProperty2.default)(MemberWarranty, "fields", (0, _MemberWarranty.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(MemberWarranty, "scopes", {
  employee: () => ({
    include: [{
      model: _db.db.models.Member,
      as: 'Approver'
    }]
  }),
  pages: (limit, offset) => ({
    limit,
    offset
  })
});