"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _Banner = _interopRequireDefault(require("../columns/Banner.js"));

var _db = require("../db");

var _General = require("../errors/General");

class Banner extends _sequelize.Model {
  static init(sequelize) {
    return super.init(Banner.fields, {
      sequelize
    });
  }

  static associate() {}

  static async createBanner({
    picture,
    link,
    mobileImg
  }) {
    const banner = await _db.db.models.Banner.create({
      picture,
      link: link || null,
      mobileImg: mobileImg || null
    });

    if (!banner) {
      return {
        error: 'Banner create failed.'
      };
    }

    return banner;
  }

  static async editBanner({
    id,
    picture,
    link,
    mobileImg
  }) {
    const banner = await _db.db.models.Banner.findOne({
      where: {
        id
      }
    });

    if (!banner) {
      throw new _General.ResourceNotFoundError();
    }

    banner.picture = picture;
    if (link) banner.link = link;
    if (mobileImg) banner.mobileImg = mobileImg;
    await banner.save();
    return banner;
  }

  static async deleteBanner({
    id
  }) {
    const banner = await _db.db.models.Banner.findOne({
      where: {
        id
      }
    });

    if (!banner) {
      throw new _General.ResourceNotFoundError();
    }

    await banner.destroy();
    return {
      id: banner.id,
      status: true,
      message: '刪除成功'
    };
  }

}

exports.default = Banner;
(0, _defineProperty2.default)(Banner, "fields", (0, _Banner.default)(_sequelize.DataTypes));