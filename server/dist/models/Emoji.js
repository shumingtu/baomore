"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _Emoji = _interopRequireDefault(require("../columns/Emoji.js"));

var _db = require("../db");

class Emoji extends _sequelize.Model {
  static init(sequelize) {
    return super.init(Emoji.fields, {
      sequelize,
      scopes: Emoji.scopes
    });
  }

  static associate() {
    Emoji.belongsTo(_db.db.models.EmojiCategory);
  }

}

exports.default = Emoji;
(0, _defineProperty2.default)(Emoji, "fields", (0, _Emoji.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(Emoji, "scopes", {
  search: args => {
    const {
      categoryId
    } = args;
    let whereClause = {};
    if (categoryId) whereClause.EmojiCategoryId = categoryId;
    return {
      where: whereClause
    };
  }
});