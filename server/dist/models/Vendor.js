"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _Vendor = _interopRequireDefault(require("../columns/Vendor.js"));

var _db = require("../db");

class Vendor extends _sequelize.Model {
  static init(sequelize) {
    return super.init(Vendor.fields, {
      sequelize,
      scopes: Vendor.scopes
    });
  }

  static associate() {
    Vendor.hasMany(_db.db.models.VendorSetting);
  }

}

exports.default = Vendor;
(0, _defineProperty2.default)(Vendor, "fields", (0, _Vendor.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(Vendor, "scopes", {
  search: args => {
    const {
      id
    } = args;
    const whereClause = {};
    if (id) whereClause.id = id;
    return {
      where: whereClause
    };
  },
  pages: (limit, offset) => ({
    limit,
    offset
  })
});