"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _PNTableClosedDateSetting = _interopRequireDefault(require("../columns/PNTableClosedDateSetting.js"));

var _db = require("../db");

class PNTableClosedDateSetting extends _sequelize.Model {
  static init(sequelize) {
    return super.init(PNTableClosedDateSetting.fields, {
      sequelize
    });
  }

  static associate() {
    PNTableClosedDateSetting.belongsTo(_db.db.models.PNTable);
  }

}

exports.default = PNTableClosedDateSetting;
(0, _defineProperty2.default)(PNTableClosedDateSetting, "fields", (0, _PNTableClosedDateSetting.default)(_sequelize.DataTypes));