"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _BookTimeSlotGroup = _interopRequireDefault(require("../columns/BookTimeSlotGroup.js"));

var _db = require("../db");

class BookTimeSlotGroup extends _sequelize.Model {
  static init(sequelize) {
    return super.init(BookTimeSlotGroup.fields, {
      sequelize
    });
  }

  static associate() {
    BookTimeSlotGroup.hasMany(_db.db.models.BookTimeSlot);
  }

}

exports.default = BookTimeSlotGroup;
(0, _defineProperty2.default)(BookTimeSlotGroup, "fields", (0, _BookTimeSlotGroup.default)(_sequelize.DataTypes));