"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _ActivityCategory = _interopRequireDefault(require("../columns/ActivityCategory.js"));

var _db = require("../db");

var _General = require("../errors/General");

class ActivityCategory extends _sequelize.Model {
  static init(sequelize) {
    return super.init(ActivityCategory.fields, {
      sequelize,
      hooks: ActivityCategory.hooks
    });
  }

  static associate() {
    ActivityCategory.hasMany(_db.db.models.Activity);
  }

  static async createCategory({
    name,
    desktopImg,
    mobileImg
  }) {
    const category = await _db.db.models.ActivityCategory.create({
      name,
      desktopImg,
      mobileImg
    });

    if (!category) {
      return {
        error: 'Category create failed.'
      };
    }

    return category;
  }

  static async editCategory({
    id,
    name,
    desktopImg,
    mobileImg
  }) {
    const category = await _db.db.models.ActivityCategory.findOne({
      where: {
        id
      }
    });

    if (!category) {
      throw new _General.ResourceNotFoundError();
    }

    category.name = name;
    category.desktopImg = desktopImg;
    category.mobileImg = mobileImg;
    await category.save();
    return category;
  }

  static async deleteCategory({
    id
  }) {
    const category = await _db.db.models.ActivityCategory.findOne({
      where: {
        id
      }
    });

    if (!category) {
      throw new _General.ResourceNotFoundError();
    }

    await category.destroy();
    return {
      id: category.id,
      status: true,
      message: '刪除成功'
    };
  }

}

exports.default = ActivityCategory;
(0, _defineProperty2.default)(ActivityCategory, "fields", (0, _ActivityCategory.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(ActivityCategory, "hooks", {
  afterDestroy: async activityCategory => {
    await _db.db.models.Activity.destroy({
      where: {
        ActivityCategoryId: activityCategory.id
      }
    });
  }
});