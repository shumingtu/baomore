"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _SystemLogType = _interopRequireDefault(require("../columns/SystemLogType.js"));

var _db = require("../db");

class SystemLogType extends _sequelize.Model {
  static init(sequelize) {
    return super.init(SystemLogType.fields, {
      sequelize,
      scopes: SystemLogType.scopes
    });
  }

  static associate() {}

}

exports.default = SystemLogType;
(0, _defineProperty2.default)(SystemLogType, "fields", (0, _SystemLogType.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(SystemLogType, "scopes", {
  search: args => {
    const {
      id
    } = args;
    const whereClause = {};
    if (id) whereClause.id = id;
    return {
      where: whereClause
    };
  }
});