"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _ClosedOrderSetting = _interopRequireDefault(require("../columns/ClosedOrderSetting.js"));

var _db = require("../db");

var _ClosedOrderSetting2 = require("../errors/ClosedOrderSetting");

var _PNTable = require("../errors/PNTable");

class ClosedOrderSetting extends _sequelize.Model {
  static init(sequelize) {
    return super.init(ClosedOrderSetting.fields, {
      sequelize,
      scopes: ClosedOrderSetting.scopes
    });
  }

  static associate() {
    ClosedOrderSetting.belongsTo(_db.db.models.PNTable);
  }

  static async deleteClosedOrderSetting({
    id
  }) {
    const closedOrderSetting = await _db.db.models.ClosedOrderSetting.findOne({
      where: {
        id
      }
    });

    if (!closedOrderSetting) {
      throw new _ClosedOrderSetting2.ClosedOrderSettingNotFoundError();
    }

    await closedOrderSetting.destroy();
    return {
      id: closedOrderSetting.id,
      status: true,
      message: '成功刪除結單設定'
    };
  }

  static async createClosedOrderSetting({
    closedDay,
    closedTime,
    code
  }) {
    let PNTable = null;

    if (code) {
      const findPNTable = await _db.db.models.PNTable.findOne({
        where: {
          code
        }
      });

      if (!findPNTable) {
        throw new _PNTable.PNTableItemNotFoundError();
      }

      const closedOrderSettingItem = await _db.db.models.ClosedOrderSetting.findOne({
        where: {
          PNTableId: findPNTable.id
        }
      });

      if (closedOrderSettingItem) {
        throw new _ClosedOrderSetting2.ClosedOrderSettingIsAlreadyExist();
      }

      PNTable = findPNTable;
    }

    const closedOrderSetting = await _db.db.models.ClosedOrderSetting.create({
      closedDay,
      closedTime,
      PNTableId: PNTable ? PNTable.id : null
    });

    if (!closedOrderSetting) {
      throw new _ClosedOrderSetting2.ClosedOrderSettingCreateError();
    }

    return { ...closedOrderSetting.dataValues,
      PNTable
    };
  }

  static async editClosedOrderSetting({
    id,
    closedDay,
    closedTime,
    code
  }) {
    const closedOrderSetting = await _db.db.models.ClosedOrderSetting.findOne({
      where: {
        id
      }
    });

    if (!closedOrderSetting) {
      throw new _ClosedOrderSetting2.ClosedOrderSettingNotFoundError();
    }

    let PNTable = null;

    if (code) {
      const findPNTable = await _db.db.models.PNTable.findOne({
        where: {
          code
        }
      });

      if (!findPNTable) {
        throw new _PNTable.PNTableItemNotFoundError();
      }

      PNTable = findPNTable;
      closedOrderSetting.PNTableId = findPNTable.id;
    }

    closedOrderSetting.closedDay = closedDay; // 2019-08-07 結單更新後如果時間不同 isClosed => false

    if (closedOrderSetting.closedTime !== closedTime) {
      closedOrderSetting.isClosed = false;
    }

    closedOrderSetting.closedTime = closedTime;
    await closedOrderSetting.save();
    return { ...closedOrderSetting.dataValues,
      PNTable
    };
  }

}

exports.default = ClosedOrderSetting;
(0, _defineProperty2.default)(ClosedOrderSetting, "fields", (0, _ClosedOrderSetting.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(ClosedOrderSetting, "scopes", {
  joinPNTable: () => ({
    include: [{
      model: _db.db.models.PNTable
    }]
  })
});