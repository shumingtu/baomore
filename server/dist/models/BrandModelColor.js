"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _BrandModelColors = _interopRequireDefault(require("../columns/BrandModelColors.js"));

var _db = require("../db");

var _Brand = require("../errors/Brand");

class BrandModelColor extends _sequelize.Model {
  static init(sequelize) {
    return super.init(BrandModelColor.fields, {
      sequelize,
      scopes: BrandModelColor.scopes
    });
  }

  static associate() {
    BrandModelColor.belongsTo(_db.db.models.BrandModel);
  }

  static async createBrandModelColor({
    name,
    modelId,
    phoneBg,
    phoneMask,
    phoneCover
  }) {
    const brandModel = await _db.db.models.BrandModel.findOne({
      where: {
        id: modelId
      }
    });

    if (!brandModel) {
      throw new _Brand.BrandModelNotFoundError();
    }

    const brandModelColor = await _db.db.models.BrandModelColor.create({
      name,
      phoneBg,
      phoneMask,
      phoneCover,
      BrandModelId: modelId
    });

    if (!brandModelColor) {
      return {
        error: 'BrandModelColor create failed.'
      };
    }

    return brandModelColor;
  }

  static async editBrandModelColor({
    id,
    name,
    phoneBg,
    phoneMask,
    phoneCover,
    brandModelId
  }) {
    const brandModelColor = await _db.db.models.BrandModelColor.scope('includeBrandAndModel').findOne({
      where: {
        id
      }
    });
    const brandModel = await _db.db.models.BrandModel.isExist(brandModelId);

    if (!brandModelColor || !brandModel) {
      throw new _Brand.BrandModelColorNotFoundError();
    }

    brandModelColor.name = name;
    brandModelColor.phoneBg = phoneBg;
    brandModelColor.phoneMask = phoneMask;
    brandModelColor.phoneCover = phoneCover;
    brandModelColor.BrandModelId = brandModelId;
    await brandModelColor.save();
    return brandModelColor;
  }

  static async deleteBrandModelColor({
    id
  }) {
    const brandModelColor = await _db.db.models.BrandModelColor.findOne({
      where: {
        id
      }
    });

    if (!brandModelColor) {
      throw new _Brand.BrandModelColorNotFoundError();
    }

    await brandModelColor.destroy();
    return {
      id: brandModelColor.id,
      status: true,
      message: '成功刪除顏色'
    };
  }

}

exports.default = BrandModelColor;
(0, _defineProperty2.default)(BrandModelColor, "fields", (0, _BrandModelColors.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(BrandModelColor, "scopes", {
  search: args => {
    const {
      brandId,
      colorId,
      modelId
    } = args;
    let whereClause = {};
    let whereIncludeClause = [{
      model: _db.db.models.BrandModel,
      include: [{
        model: _db.db.models.Brand
      }]
    }];
    if (colorId) whereClause.id = colorId;
    if (modelId) whereClause.BrandModelId = modelId;

    if (brandId) {
      whereIncludeClause = [{
        model: _db.db.models.BrandModel,
        where: {
          BrandId: brandId
        },
        include: [{
          model: _db.db.models.Brand
        }]
      }];
    }

    return {
      where: whereClause,
      include: whereIncludeClause
    };
  },
  pages: (limit, offset) => ({
    limit,
    offset
  }),
  includeBrandAndModel: () => ({
    include: [{
      model: _db.db.models.BrandModel,
      include: [{
        model: _db.db.models.Brand
      }]
    }]
  })
});