"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _Role = _interopRequireDefault(require("../columns/Role"));

var _db = require("../db");

class Role extends _sequelize.Model {
  static init(sequelize) {
    return super.init(Role.fields, {
      sequelize,
      scopes: Role.scopes
    });
  }

  static associate() {
    Role.belongsToMany(_db.db.models.Action, {
      through: 'RoleActions',
      foreignKey: 'RoleId',
      otherKey: 'ActionCode'
    });
    Role.belongsToMany(_db.db.models.Member, {
      through: 'MemberRoles',
      foreignKey: 'RoleId',
      otherKey: 'MemberId'
    });
  }

  static get() {
    const cachedRoles = {};
    return role => {
      if (cachedRoles[role.id]) {
        return cachedRoles[role.id];
      }

      return Role.findOne({
        where: {
          id: role.id
        }
      });
    };
  }

}

exports.default = Role;
(0, _defineProperty2.default)(Role, "fields", (0, _Role.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(Role, "scopes", {
  search: args => {
    const {
      roleId
    } = args;
    const whereClause = {};
    if (roleId) whereClause.id = roleId;
    return {
      where: whereClause
    };
  },
  pages: (limit, offset) => ({
    limit,
    offset
  }),
  action: () => ({
    include: [{
      model: _db.db.models.Action
    }]
  })
});