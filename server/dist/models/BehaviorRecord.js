"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _moment = _interopRequireDefault(require("moment"));

var _BehaviorRecord = _interopRequireDefault(require("../columns/BehaviorRecord.js"));

var _db = require("../db");

var _General = require("../errors/General");

class BehaviorRecord extends _sequelize.Model {
  static init(sequelize) {
    return super.init(BehaviorRecord.fields, {
      sequelize,
      defaultScope: BehaviorRecord.defaultScope,
      scopes: BehaviorRecord.scopes
    });
  }

  static associate() {
    BehaviorRecord.belongsTo(_db.db.models.BehaviorRecord, {
      as: 'TransferRecord'
    });
    BehaviorRecord.belongsTo(_db.db.models.Member);
    BehaviorRecord.belongsTo(_db.db.models.Member, {
      as: 'Approver'
    });
    BehaviorRecord.belongsTo(_db.db.models.Vendor);
    BehaviorRecord.belongsTo(_db.db.models.Store);
    BehaviorRecord.belongsTo(_db.db.models.Behavior);
    BehaviorRecord.belongsTo(_db.db.models.PNTable);
  }

  static async verifyBehaviorRecords({
    recordIds,
    approverId
  }) {
    const approver = await _db.db.models.Member.findOne({
      where: {
        id: approverId
      }
    });
    if (!approver) throw new _General.MemberNotFoundError();
    const records = await _db.db.models.BehaviorRecord.scope({
      method: ['searchByIds', {
        ids: recordIds
      }]
    }, 'pnTable', 'member', 'order', {
      method: ['vendor', false]
    }, {
      method: ['store', false]
    }).findAll({
      where: {
        ApproverId: null
      }
    });
    if (!records) throw new _General.ResourceNotFoundError();
    let errorRecords = [];
    await records.map(record => () => new Promise(async (resolve, reject) => {
      const transaction = await _db.db.transaction();

      try {
        await record.update({
          ApproverId: approverId
        }, {
          transaction
        });

        if (record.rollbackType !== 'CUSTOMERCOMPLAINT') {
          await _db.db.models.MemberStock.reduceStock(record.MemberId, record.PNTableId, record.quantity, transaction);
        }

        await transaction.commit();
        resolve();
      } catch (e) {
        await transaction.rollback();
        errorRecords.push(record);
        reject(e);
      }
    })).reduce((prev, next) => prev.then(next).catch(e => console.log(e)), Promise.resolve());
    const errorRecordIds = errorRecords.map(errorRecord => errorRecord.id);
    const resultRecords = records.filter(record => !errorRecordIds.includes(record.id));
    const newRecords = resultRecords.map(record => ({ ...record.dataValues,
      remarks: record.dataValues.remark && record.dataValues.remark.split('***') || [],
      pnCategory: record.PNTable && record.PNTable.PNSubCategory && record.PNTable.PNSubCategory.PNCategory || null,
      Approver: approver
    }));
    errorRecords = errorRecords.map(errorRecord => ({ ...errorRecord.dataValues,
      remarks: errorRecord.dataValues.remark && errorRecord.dataValues.remark.split('***') || [],
      pnCategory: errorRecord.PNTable && errorRecord.PNTable.PNSubCategory && errorRecord.PNTable.PNSubCategory.PNCategory || null,
      Approver: null
    }));
    return {
      behaviorRecords: newRecords,
      errorRecords
    };
  }

}

exports.default = BehaviorRecord;
(0, _defineProperty2.default)(BehaviorRecord, "fields", (0, _BehaviorRecord.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(BehaviorRecord, "defaultScope", {
  order: [['createdAt', 'DESC']]
});
(0, _defineProperty2.default)(BehaviorRecord, "scopes", {
  searchForPerformance: args => {
    const {
      startDate,
      endDate,
      storeId,
      employeeId,
      behaviorId,
      recordStartDate,
      recordEndDate
    } = args;
    const whereClause = {};
    const recordDateCondition = [];

    if (recordStartDate) {
      recordDateCondition.push({
        [_sequelize.Op.gte]: (0, _moment.default)(recordStartDate)
      });
    }

    if (recordEndDate) {
      recordDateCondition.push({
        [_sequelize.Op.lt]: (0, _moment.default)(recordEndDate).add(1, 'd')
      });
    }

    if (recordDateCondition.length) {
      whereClause.createdAt = {
        [_sequelize.Op.and]: recordDateCondition
      };
    }

    if (startDate) {
      whereClause.startTime = {
        [_sequelize.Op.gte]: (0, _moment.default)(startDate)
      };
    }

    if (endDate) {
      whereClause.endTime = {
        [_sequelize.Op.lt]: (0, _moment.default)(endDate).add(1, 'd')
      };
    }

    if (storeId) whereClause.StoreId = storeId;
    if (employeeId) whereClause.MemberId = employeeId;
    if (behaviorId) whereClause.BehaviorId = behaviorId;
    return {
      where: whereClause
    };
  },
  search: args => {
    const {
      id,
      ids,
      rollbackType,
      startDate,
      endDate,
      memberId,
      errorType,
      target,
      storeId,
      vat,
      customerName,
      customerPhone
    } = args;
    let whereClause = {};

    if (rollbackType && rollbackType !== 'NONE') {
      whereClause.BehaviorId = 5; // 退貨
    }

    whereClause.rollbackType = rollbackType || 'NONE';

    if (ids && Array.isArray(ids)) {
      whereClause.id = ids;
    }

    if (startDate || endDate) {
      whereClause.createdAt = {
        [_sequelize.Op.and]: [{
          [_sequelize.Op.gte]: startDate && (0, _moment.default)(startDate) || (0, _moment.default)()
        }, {
          [_sequelize.Op.lte]: endDate && (0, _moment.default)(endDate) || (0, _moment.default)()
        }]
      };
    }

    if (id) whereClause.id = id;
    if (memberId) whereClause.MemberId = memberId;
    if (storeId) whereClause.StoreId = storeId;

    if (errorType || vat || customerName || customerPhone || target) {
      const errorTypeSearch = errorType ? {
        remark: {
          [_sequelize.Op.like]: `%${errorType}%`
        }
      } : null;
      const vatSearch = vat ? {
        remark: {
          [_sequelize.Op.like]: `%${vat}%`
        }
      } : null;
      const customerNameSearch = customerName ? {
        remark: {
          [_sequelize.Op.like]: `%${customerName}%`
        }
      } : null;
      const customerPhoneSearch = customerPhone ? {
        remark: {
          [_sequelize.Op.like]: `%${customerPhone}%`
        }
      } : null;
      const targetSearch = target ? {
        remark: {
          [_sequelize.Op.like]: `%${target}%`
        }
      } : null;
      whereClause = { ...whereClause,
        [_sequelize.Op.and]: [errorTypeSearch, vatSearch, customerNameSearch, customerPhoneSearch, targetSearch].filter(s => s)
      };
    }

    return {
      where: whereClause
    };
  },
  searchByIds: ({
    ids
  }) => ({
    where: ids && Array.isArray(ids) ? {
      id: ids
    } : {}
  }),
  pnTable: (code, pnCategoryId) => ({
    include: [{
      model: _db.db.models.PNTable.scope({
        method: ['search', {
          code
        }]
      }, {
        method: ['categories', pnCategoryId, null, null]
      })
    }]
  }),
  member: memberId => ({
    include: [{
      model: _db.db.models.Member,
      where: memberId ? {
        id: memberId
      } : {}
    }]
  }),
  vendor: (required = true, vendorId) => ({
    include: [{
      model: _db.db.models.Vendor,
      required,
      where: vendorId ? {
        id: vendorId
      } : {}
    }]
  }),
  store: (required = true, storeId) => ({
    include: [{
      model: _db.db.models.Store,
      required,
      where: storeId ? {
        id: storeId
      } : {}
    }]
  }),
  approver: (required = true, memberId) => ({
    include: [{
      model: _db.db.models.Member,
      required,
      as: 'Approver',
      where: memberId ? {
        id: memberId
      } : {}
    }]
  }),
  pages: (limit, offset) => ({
    limit,
    offset
  }),
  order: {
    order: [['createdAt', 'DESC']]
  },
  performanceMemberAssociation: args => ({
    include: [{
      attributes: ['id', 'name', 'AreaId', 'MemberId'],
      model: _db.db.models.Member.scope({
        method: ['search', args]
      }),
      required: true,
      include: [{
        attributes: ['id', 'name'],
        model: _db.db.models.Area,
        required: true
      }, {
        model: _db.db.models.Role,
        where: {
          name: '包膜師'
        },
        required: true
      }]
    }]
  }),
  performancePNTableAssociation: () => ({
    include: [{
      model: _db.db.models.PNTable,
      attributes: ['id', 'PNSubCategoryId'],
      required: true,
      include: [{
        attributes: ['id', 'PNCategoryId'],
        model: _db.db.models.PNSubCategory,
        required: true,
        include: [{
          attributes: ['id', 'name'],
          model: _db.db.models.PNCategory,
          where: {
            id: {
              [_sequelize.Op.in]: [1, 2]
            }
          }
        }]
      }]
    }]
  }),
  performanceStoreAssociation: args => ({
    include: [{
      attributes: ['id', 'name', 'ChannelId'],
      model: _db.db.models.Store.scope({
        method: ['search', args]
      }),
      required: true,
      include: [{
        attributes: ['id', 'name'],
        model: _db.db.models.Channel,
        required: true
      }]
    }]
  }),
  behavior: () => ({
    include: [{
      model: _db.db.models.Behavior
    }]
  })
});