"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _EmojiCategory = _interopRequireDefault(require("../columns/EmojiCategory.js"));

var _db = require("../db");

class EmojiCategory extends _sequelize.Model {
  static init(sequelize) {
    return super.init(EmojiCategory.fields, {
      sequelize
    });
  }

  static associate() {}

}

exports.default = EmojiCategory;
(0, _defineProperty2.default)(EmojiCategory, "fields", (0, _EmojiCategory.default)(_sequelize.DataTypes));