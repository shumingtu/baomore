"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _bcrypt = _interopRequireDefault(require("bcrypt"));

var _debug = _interopRequireDefault(require("debug"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _jwt = require("../helpers/jwt.js");

var _permissions = require("../helpers/permissions");

var _Member = require("../errors/Member");

var _General = require("../errors/General");

var _Member2 = _interopRequireDefault(require("../columns/Member"));

var _db = require("../db");

const debugMemeberModel = (0, _debug.default)('BAOMORE:Member:Model');

class Member extends _sequelize.Model {
  static init(sequelize) {
    return super.init(Member.fields, {
      sequelize,
      scopes: Member.scopes
    });
  }

  // Class Method
  static associate() {
    Member.belongsToMany(_db.db.models.Role, {
      through: 'MemberRoles',
      foreignKey: 'MemberId',
      otherKey: 'RoleId'
    });
    Member.belongsToMany(_db.db.models.Store, {
      through: _db.db.models.MemberStore,
      foreignKey: 'MemberId',
      otherKey: 'StoreId'
    });
    Member.hasMany(_db.db.models.VendorSetting);
    Member.belongsTo(_db.db.models.Member);
    Member.hasMany(_db.db.models.Order, {
      foreignKey: 'EmpolyedMemberId'
    });
    Member.hasMany(_db.db.models.MemberWarranty, {
      as: 'memberWarranties'
    });
    Member.hasMany(_db.db.models.MemberWarranty, {
      foreignKey: 'ApproverId',
      as: 'Approver'
    });
    Member.hasMany(_db.db.models.OnlineOrder, {
      foreignKey: 'BookedMemberId'
    });
    Member.hasMany(_db.db.models.OnlineOrder, {
      foreignKey: 'OwnerId',
      as: 'onlineOrders'
    });
    Member.hasMany(_db.db.models.MemberSchedule);
    Member.belongsTo(_db.db.models.Area);
    Member.belongsToMany(_db.db.models.PNTable, {
      through: _db.db.models.MemberStock,
      foreignKey: 'MemberId',
      otherKey: 'PNTableId'
    });
  }

  static async login(account, password) {
    const member = await Member.findOne({
      where: {
        account
      }
    });
    if (!member) throw new _Member.AccountNotFoundError();

    if (await member.validPassword(password)) {
      return {
        member,
        refreshToken: (0, _jwt.jwtSignRefreshToken)({
          id: member.id,
          account: member.account
        })
      };
    }

    throw new _Member.PasswordNotMatchedError();
  }

  static async createoAuthMember(data) {
    const member = await Member.create(data);

    if (!member) {
      return {
        error: 'Member Create Failed'
      };
    }

    return member;
  }

  static async memberoAuthLogin(oAuthId) {
    try {
      const member = await Member.findOne({
        where: {
          lineId: oAuthId,
          archive: false
        }
      });

      if (!member) {
        return _Member.AccountNotFoundError;
      }

      const token = await member.accessToken();
      return {
        member,
        accessToken: token
      };
    } catch (ex) {
      debugMemeberModel(ex);
      return _General.SystemError;
    }
  }

  static getMe(memberId) {
    return _db.db.models.Member.findOne({
      where: {
        id: memberId
      }
    });
  }

  validPassword(password) {
    return _bcrypt.default.compare(password, this.password);
  }

  async permissions() {
    return (0, _permissions.getMemberPermission)(this.id);
  }

  async accessToken() {
    return (0, _jwt.jwtSign)({
      id: this.id,
      name: this.name,
      permissions: await this.permissions()
    });
  }

  async payToken(orderId) {
    return (0, _jwt.jwtSign)({
      id: this.id,
      orderId
    });
  }

}

exports.default = Member;
(0, _defineProperty2.default)(Member, "fields", (0, _Member2.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(Member, "scopes", {
  search: args => {
    const {
      memberId,
      areaId,
      keyword,
      directorId,
      lineIdRequired,
      isForClient
    } = args;
    let whereClause = {};

    if (lineIdRequired) {
      whereClause.lineId = {
        [_sequelize.Op.ne]: null
      };
    }

    if (isForClient) {
      whereClause.archive = false;
    }

    if (memberId) whereClause.id = memberId;
    if (areaId) whereClause.AreaId = areaId;
    if (directorId) whereClause.MemberId = directorId;

    if (keyword) {
      whereClause = { ...whereClause,
        [_sequelize.Op.or]: [{
          serialNumber: {
            [_sequelize.Op.like]: `%${keyword}%`
          }
        }, {
          name: {
            [_sequelize.Op.like]: `%${keyword}%`
          }
        }, {
          phone: {
            [_sequelize.Op.like]: `%${keyword}%`
          }
        }]
      };
    }

    return {
      where: whereClause
    };
  },
  findById: memberId => ({
    where: {
      id: memberId
    }
  }),
  pages: (limit, offset) => ({
    limit,
    offset
  }),
  employee: ({
    storeId,
    employeeType
  }) => ({
    include: [{
      model: _db.db.models.Role,
      where: {
        name: employeeType || '包膜師'
      }
    }, {
      model: _db.db.models.Store,
      where: {
        id: storeId
      },
      required: !!storeId
    }, {
      model: _db.db.models.Area
    }]
  }),
  warranty: () => ({
    include: [{
      model: _db.db.models.MemberWarranty,
      as: 'memberWarranties'
    }]
  }),
  onlineOrderStore: () => ({
    include: [{
      model: _db.db.models.OnlineOrder,
      as: 'onlineOrders',
      include: [{
        model: _db.db.models.Store
      }, {
        model: _db.db.models.Member,
        as: 'BookedMember'
      }]
    }]
  }) // Prototype

});