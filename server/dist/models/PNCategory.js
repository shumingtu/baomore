"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _PNCategory = _interopRequireDefault(require("../columns/PNCategory.js"));

var _db = require("../db");

var _PNCategory2 = require("../errors/PNCategory");

class PNCategory extends _sequelize.Model {
  static init(sequelize) {
    return super.init(PNCategory.fields, {
      sequelize,
      scopes: PNCategory.scopes
    });
  }

  static associate() {}

  static async deletePNCategory({
    id
  }) {
    const pnCategory = await _db.db.models.PNCategory.findOne({
      where: {
        id
      }
    });

    if (!pnCategory) {
      throw new _PNCategory2.PNCategoryNotFoundError();
    }

    await pnCategory.destroy();
    return {
      id: pnCategory.id,
      status: true,
      message: '成功刪除料號'
    };
  }

  static async createPNCategory({
    name
  }) {
    const pnCategory = await _db.db.models.PNCategory.create({
      name
    });

    if (!pnCategory) {
      throw new _PNCategory2.PNCategoryCreateError();
    }

    return pnCategory;
  }

  static async editPNCategory({
    id,
    name
  }) {
    const pnCategory = await _db.db.models.PNCategory.findOne({
      where: {
        id
      }
    });

    if (!pnCategory) {
      throw new _PNCategory2.PNCategoryNotFoundError();
    }

    pnCategory.name = name;
    await pnCategory.save();
    return pnCategory;
  }

}

exports.default = PNCategory;
(0, _defineProperty2.default)(PNCategory, "fields", (0, _PNCategory.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(PNCategory, "scopes", {
  search: args => {
    const {
      id,
      keyword
    } = args;
    let whereClause = {};
    if (id) whereClause.id = id;

    if (keyword) {
      whereClause = { ...whereClause,
        [_sequelize.Op.or]: [{
          name: {
            [_sequelize.Op.like]: `%${keyword}%`
          }
        }]
      };
    }

    return {
      where: whereClause
    };
  },
  filterByKeyword: keyword => {
    let whereClause = {};

    if (keyword) {
      whereClause = { ...whereClause,
        [_sequelize.Op.or]: [{
          name: {
            [_sequelize.Op.like]: `%${keyword}%`
          }
        }]
      };
    }

    return {
      where: whereClause
    };
  }
});