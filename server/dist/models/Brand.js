"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _Brand = _interopRequireDefault(require("../columns/Brand.js"));

var _db = require("../db");

var _Brand2 = require("../errors/Brand");

class Brand extends _sequelize.Model {
  static init(sequelize) {
    return super.init(Brand.fields, {
      sequelize,
      hooks: Brand.hooks,
      scopes: Brand.scopes
    });
  }

  static associate() {
    Brand.hasMany(_db.db.models.BrandModel, {
      as: 'models'
    });
  }

  static async createBrand({
    name
  }) {
    const brand = await _db.db.models.Brand.create({
      name
    });

    if (!brand) {
      return {
        error: 'Brand create failed.'
      };
    }

    return brand;
  }

  static async editBrand({
    id,
    name
  }) {
    const brand = await _db.db.models.Brand.findOne({
      where: {
        id
      }
    });

    if (!brand) {
      throw new _Brand2.BrandNotFoundError();
    }

    brand.name = name;
    await brand.save();
    return brand;
  }

  static async deleteBrand({
    id
  }) {
    const brand = await _db.db.models.Brand.findOne({
      where: {
        id
      }
    });

    if (!brand) {
      throw new _Brand2.BrandNotFoundError();
    }

    await brand.destroy();
    return {
      id: brand.id,
      status: true,
      message: '成功刪除品牌'
    };
  }

}

exports.default = Brand;
(0, _defineProperty2.default)(Brand, "fields", (0, _Brand.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(Brand, "hooks", {
  afterDestroy: async brand => {
    const brandModels = await _db.db.models.BrandModel.findAll({
      where: {
        BrandId: brand.id
      }
    });
    brandModels.map(async brandModel => {
      await brandModel.destroy();
    });
  }
});
(0, _defineProperty2.default)(Brand, "scopes", {
  search: args => {
    const {
      id
    } = args;
    const whereClause = {};
    if (id) whereClause.id = id;
    return {
      where: whereClause
    };
  }
});