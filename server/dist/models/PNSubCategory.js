"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _PNSubCategory = _interopRequireDefault(require("../columns/PNSubCategory.js"));

var _db = require("../db");

var _PNSubCategory2 = require("../errors/PNSubCategory");

class PNSubCategory extends _sequelize.Model {
  static init(sequelize) {
    return super.init(PNSubCategory.fields, {
      sequelize,
      scopes: PNSubCategory.scopes
    });
  }

  static associate() {
    PNSubCategory.belongsTo(_db.db.models.PNCategory);
    PNSubCategory.belongsTo(_db.db.models.PNSubCategory);
  }

  static async deletePNSubCategory({
    id
  }) {
    const pnSubCategory = await _db.db.models.PNSubCategory.findOne({
      where: {
        id
      }
    });

    if (!pnSubCategory) {
      throw new _PNSubCategory2.PNSubCategoryNotFoundError();
    }

    await pnSubCategory.destroy();
    return {
      id: pnSubCategory.id,
      status: true,
      message: '成功刪除類別'
    };
  }

  static async editPNSubCategory({
    id,
    name,
    picture,
    isOnSale
  }) {
    const pnSubCategory = await _db.db.models.PNSubCategory.findOne({
      where: {
        id
      }
    });

    if (!pnSubCategory) {
      throw new _PNSubCategory2.PNSubCategoryNotFoundError();
    }

    pnSubCategory.name = name;
    pnSubCategory.picture = picture || null;
    pnSubCategory.isOnSale = isOnSale;
    await pnSubCategory.save();
    return pnSubCategory;
  }

  static async createPNSubCategory({
    name,
    picture,
    isOnSale,
    PNCategoryId,
    PNSubCategoryId
  }) {
    const pnSubCategory = await _db.db.models.PNSubCategory.create({
      name,
      picture: picture || null,
      isOnSale,
      PNCategoryId,
      PNSubCategoryId: PNSubCategoryId || null
    });

    if (!pnSubCategory) {
      throw new _PNSubCategory2.PNSubCategoryCreateError();
    }

    return pnSubCategory;
  }

}

exports.default = PNSubCategory;
(0, _defineProperty2.default)(PNSubCategory, "fields", (0, _PNSubCategory.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(PNSubCategory, "scopes", {
  search: args => {
    const {
      id,
      pnSubCategoryId,
      pnCategoryId
    } = args;
    const whereClause = {};
    if (id) whereClause.id = id;
    if (pnSubCategoryId) whereClause.PNSubCategoryId = pnSubCategoryId;
    if (pnCategoryId) whereClause.PNCategoryId = pnCategoryId;
    return {
      where: whereClause
    };
  },
  onSale: {
    where: {
      isOnSale: true
    }
  },
  bigSubCategory: {
    where: {
      PNSubCategoryId: null
    }
  },
  orderBySeqNum: {
    order: [['seqNum']]
  },
  filterByKeyword: keyword => {
    let whereClause = {};

    if (keyword) {
      whereClause = { ...whereClause,
        [_sequelize.Op.or]: [{
          name: {
            [_sequelize.Op.like]: `%${keyword}%`
          }
        }]
      };
    }

    return {
      where: whereClause
    };
  },
  findByPNCategoryId: id => ({
    where: {
      PNCategoryId: id,
      PNSubCategoryId: null
    }
  }),
  findBySubCategoryId: id => ({
    where: {
      PNSubCategoryId: id
    }
  }),
  findById: id => ({
    where: {
      id
    }
  })
});