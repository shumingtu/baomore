"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _Behavior = _interopRequireDefault(require("../columns/Behavior.js"));

var _db = require("../db");

class Behavior extends _sequelize.Model {
  static init(sequelize) {
    return super.init(Behavior.fields, {
      sequelize
    });
  }

  static associate() {}

}

exports.default = Behavior;
(0, _defineProperty2.default)(Behavior, "fields", (0, _Behavior.default)(_sequelize.DataTypes));