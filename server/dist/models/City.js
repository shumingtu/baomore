"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _City = _interopRequireDefault(require("../columns/City.js"));

var _db = require("../db");

class City extends _sequelize.Model {
  static init(sequelize) {
    return super.init(City.fields, {
      sequelize,
      scopes: City.scopes
    });
  }

  static associate() {
    City.hasMany(_db.db.models.District);
  }

}

exports.default = City;
(0, _defineProperty2.default)(City, "fields", (0, _City.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(City, "scopes", {
  districts: () => ({
    include: [{
      model: _db.db.models.District
    }]
  })
});