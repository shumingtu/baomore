"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _MemberStock = _interopRequireDefault(require("../columns/MemberStock.js"));

var _Stock = require("../errors/Stock");

var _db = require("../db");

class MemberStock extends _sequelize.Model {
  static init(sequelize) {
    return super.init(MemberStock.fields, {
      sequelize,
      indexes: MemberStock.indexes,
      scopes: MemberStock.scopes
    });
  }

  static associate() {
    MemberStock.belongsTo(_db.db.models.Member);
    MemberStock.belongsTo(_db.db.models.PNTable);
  }

  static async addStock(memberId, pnTableId, amount, transaction) {
    const stock = await _db.db.models.MemberStock.scope({
      method: ['search', {
        memberId,
        pnTableId
      }]
    }).findOne();

    if (stock) {
      await stock.increment('storageCount', {
        by: amount,
        transaction
      });
      return;
    }

    await _db.db.models.MemberStock.create({
      storageCount: amount,
      MemberId: memberId,
      PNTableId: pnTableId
    }, {
      transaction
    });
  }

  static async reduceStock(memberId, pnTableId, amount, transaction) {
    const stock = await _db.db.models.MemberStock.scope({
      method: ['search', {
        memberId,
        pnTableId
      }]
    }).findOne();
    if (!stock) throw new _Stock.StockNotFoundError();
    if (stock.storageCount < amount) throw new _Stock.StockOverflowError();
    await stock.decrement('storageCount', {
      by: amount,
      transaction
    });
  }

}

exports.default = MemberStock;
(0, _defineProperty2.default)(MemberStock, "fields", (0, _MemberStock.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(MemberStock, "indexes", [{
  name: 'MemberId',
  fields: ['MemberId']
}, {
  name: 'PNTableId',
  fields: ['PNTableId']
}]);
(0, _defineProperty2.default)(MemberStock, "scopes", {
  search: args => {
    const {
      pnTableId,
      memberId
    } = args;
    let whereClause = {};
    if (pnTableId) whereClause.PNTableId = pnTableId;
    if (memberId) whereClause.MemberId = memberId;
    return {
      where: whereClause
    };
  },
  member: () => ({
    include: [{
      model: _db.db.models.Member
    }]
  }),
  pnTable: args => ({
    include: [{
      model: _db.db.models.PNTable.scope('brandAndModel', 'categories', 'vendor', {
        method: ['search', args]
      })
    }]
  })
});