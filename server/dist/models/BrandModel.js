"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _BrandModels = _interopRequireDefault(require("../columns/BrandModels.js"));

var _db = require("../db");

var _Brand = require("../errors/Brand");

class BrandModel extends _sequelize.Model {
  static init(sequelize) {
    return super.init(BrandModel.fields, {
      sequelize,
      hooks: BrandModel.hooks,
      scopes: BrandModel.scopes
    });
  }

  static associate() {
    BrandModel.belongsTo(_db.db.models.Brand);
    BrandModel.hasMany(_db.db.models.BrandModelColor, {
      as: 'colors'
    });
    BrandModel.hasMany(_db.db.models.PNTable);
  }

  static async createBrandModel({
    name,
    brandId
  }) {
    const brand = await _db.db.models.Brand.findOne({
      where: {
        id: brandId
      }
    });

    if (!brand) {
      throw new _Brand.BrandNotFoundError();
    }

    const brandModel = await _db.db.models.BrandModel.create({
      name,
      BrandId: brandId
    });

    if (!brandModel) {
      return {
        error: 'BrandModel create failed.'
      };
    }

    return brandModel;
  }

  static async editBrandModel({
    id,
    name
  }) {
    const brandModel = await _db.db.models.BrandModel.findOne({
      where: {
        id
      }
    });

    if (!brandModel) {
      throw new _Brand.BrandModelNotFoundError();
    }

    brandModel.name = name;
    await brandModel.save();
    return brandModel;
  }

  static async deleteBrandModel({
    id
  }) {
    const brandModel = await _db.db.models.BrandModel.findOne({
      where: {
        id
      }
    });

    if (!brandModel) {
      throw new _Brand.BrandModelNotFoundError();
    }

    await brandModel.destroy();
    return {
      id: brandModel.id,
      status: true,
      message: '成功刪除型號'
    };
  }

  static async isExist(id) {
    const brandModel = await _db.db.models.BrandModel.findOne({
      attributes: ['id'],
      where: {
        id
      }
    });
    if (brandModel) return true;
    return false;
  }

}

exports.default = BrandModel;
(0, _defineProperty2.default)(BrandModel, "fields", (0, _BrandModels.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(BrandModel, "hooks", {
  afterDestroy: async brandModel => {
    await _db.db.models.BrandModelColor.destroy({
      where: {
        BrandModelId: brandModel.id
      }
    });
  }
});
(0, _defineProperty2.default)(BrandModel, "scopes", {
  search: args => {
    const {
      id,
      brandId
    } = args;
    const whereClause = {};
    if (id) whereClause.id = id;
    if (brandId) whereClause.BrandId = brandId;
    return {
      where: whereClause
    };
  }
});