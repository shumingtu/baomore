"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _EmployeeConfirmSchedule = _interopRequireDefault(require("../columns/EmployeeConfirmSchedule.js"));

var _db = require("../db");

class EmployeeConfirmSchedule extends _sequelize.Model {
  static init(sequelize) {
    return super.init(EmployeeConfirmSchedule.fields, {
      sequelize
    });
  }

  static associate() {
    EmployeeConfirmSchedule.belongsTo(_db.db.models.OnlineOrder);
  }

}

exports.default = EmployeeConfirmSchedule;
(0, _defineProperty2.default)(EmployeeConfirmSchedule, "fields", (0, _EmployeeConfirmSchedule.default)(_sequelize.DataTypes));