"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _PerformanceSetting = _interopRequireDefault(require("../columns/PerformanceSetting.js"));

var _db = require("../db");

class PerformanceSetting extends _sequelize.Model {
  static init(sequelize) {
    return super.init(PerformanceSetting.fields, {
      sequelize,
      scopes: PerformanceSetting.scopes
    });
  }

  static associate() {
    PerformanceSetting.belongsTo(_db.db.models.PNCategory);
  }

}

exports.default = PerformanceSetting;
(0, _defineProperty2.default)(PerformanceSetting, "fields", (0, _PerformanceSetting.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(PerformanceSetting, "scopes", {
  search: args => {
    const {
      PNCategoryId
    } = args;
    const whereClause = {};
    if (PNCategoryId) whereClause.PNCategoryId = PNCategoryId;
    return {
      where: whereClause
    };
  }
});