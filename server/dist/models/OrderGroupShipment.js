"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _moment = _interopRequireDefault(require("moment"));

var _lodash = _interopRequireDefault(require("lodash"));

var _OrderGroupShipment = _interopRequireDefault(require("../columns/OrderGroupShipment.js"));

var _db = require("../db");

var _OrderGroupShipment2 = require("../errors/OrderGroupShipment");

var _padDigits = _interopRequireDefault(require("../helpers/padDigits.js"));

class OrderGroupShipment extends _sequelize.Model {
  static init(sequelize) {
    return super.init(OrderGroupShipment.fields, {
      sequelize,
      scopes: OrderGroupShipment.scopes
    });
  }

  static associate() {
    OrderGroupShipment.belongsTo(_db.db.models.PNTable);
    OrderGroupShipment.hasMany(_db.db.models.Order);
  }

  static async updateGroupOrderStatus({
    id
  }) {
    const orderGroupShipment = await _db.db.models.OrderGroupShipment.findOne({
      where: {
        id
      }
    });
    if (!orderGroupShipment) throw new _OrderGroupShipment2.OrderGroupShipmentCreateError();
    orderGroupShipment.status = 'SHIPMENTED';
    const targetOrders = await _db.db.models.Order.findAll({
      where: {
        OrderGroupShipmentId: orderGroupShipment.id
      },
      include: [{
        model: _db.db.models.OnlineOrder
      }]
    });
    const orderIds = targetOrders.map(x => x.id);
    const onlineOrderIds = [];
    targetOrders.forEach(x => {
      if (x.OnlineOrder) onlineOrderIds.push(x.OnlineOrder.id);
    });
    await _db.db.models.Order.update({
      status: 'DISPATCHED'
    }, {
      where: {
        id: orderIds
      }
    });

    if (onlineOrderIds.length) {
      await _db.db.models.OnlineOrder.update({
        shippingStatus: 'DISPATCHED'
      }, {
        where: {
          id: onlineOrderIds
        }
      });
    }

    await orderGroupShipment.save();
    return {
      id: orderGroupShipment.id,
      status: true,
      message: '更新成功'
    };
  }

  static getOrderSN(closingOrders) {
    const orderSNResults = [];
    let orderSN = 1;

    const orderGroupByMember = _lodash.default.groupBy(closingOrders, 'EmpolyedMemberId');

    Object.keys(orderGroupByMember).forEach(memberId => {
      const memberOrders = orderGroupByMember[memberId];

      const orderByProduct = _lodash.default.groupBy(memberOrders, 'ProductType');

      Object.keys(orderByProduct).forEach(productType => {
        const productOrders = orderByProduct[productType];

        const orderByVendor = _lodash.default.groupBy(productOrders, 'PNTable.VendorId');

        Object.keys(orderByVendor).forEach(vendorId => {
          const vendorOrders = orderByVendor[vendorId];
          const orderIds = vendorOrders.map(x => x.id);
          orderSNResults.push({
            orderIds,
            orderSN,
            memberId,
            // 包膜師
            productType,
            // 商品
            vendorId // 廠商

          });
          orderSN += 1;
        });
      });
    });
    return orderSNResults;
  }

  static async createOrderGroupShipment(pnTables, closingOrders) {
    const minDateOrder = _lodash.default.minBy(closingOrders, 'createdAt');

    const orderSNGroup = _db.db.models.OrderGroupShipment.getOrderSN(closingOrders);

    const transaction = await _db.db.transaction();

    try {
      await orderSNGroup.map(x => () => new Promise(async resolve => {
        let SN = (0, _padDigits.default)(x.orderSN, 3); // 可能同一天兩種料號有設定結單 一個在早上一個在下午
        // 所以要先找今天結過的單 同包膜師 同商品 同廠商 有的話訂單編號該一樣
        // 沒有的話 可能早上有結過 為了避免重複要找原本結過的最大orderSN開始當作訂單編號
        // 是否有相同訂單編號的單

        const sameSNOrders = await _db.db.models.Order.findAll({
          attributes: ['orderSN'],
          where: {
            orderSN: {
              [_sequelize.Op.like]: `${(0, _moment.default)().format('YYYYMMDD')}%`
            },
            EmpolyedMemberId: x.memberId,
            ProductType: x.productType,
            orderSource: 'EMPOLYEE'
          },
          include: [{
            model: _db.db.models.PNTable,
            where: {
              VendorId: x.vendorId
            }
          }]
        });
        if (sameSNOrders.length) SN = sameSNOrders[0].orderSN.slice(8);

        if (!sameSNOrders.length) {
          // 找今天原本結過的單, 有的話應該取orderSN最大的開始當作訂單編號
          const groupedOrder = await _db.db.models.Order.findAll({
            attributes: ['orderSN'],
            where: {
              orderSN: {
                [_sequelize.Op.like]: `${(0, _moment.default)().format('YYYYMMDD')}%`
              },
              orderSource: 'EMPOLYEE'
            }
          }).map(g => `${g.orderSN.slice(8)}`); // get last 3 number;

          if (groupedOrder.length) {
            const biggestSN = Math.max(...groupedOrder);
            SN = (0, _padDigits.default)(x.orderSN + biggestSN, 3);
          }
        }

        await _db.db.models.Order.update({
          orderSN: `${(0, _moment.default)().format('YYYYMMDD')}${SN}`
        }, {
          where: {
            id: {
              [_sequelize.Op.in]: x.orderIds
            }
          },
          transaction
        });
        resolve();
      })).reduce((prev, next) => prev.then(next), Promise.resolve());
      await pnTables.map(pnTable => () => new Promise(async resolve => {
        const orderGroupShipment = await _db.db.models.OrderGroupShipment.create({
          SN: (0, _moment.default)().format('YYYYMMDD'),
          status: 'UNSHIPMENTED',
          startDate: minDateOrder.createdAt,
          endDate: new Date(),
          PNTableId: pnTable.id
        }, {
          transaction
        });
        const pnTablesOrders = closingOrders.filter(order => order.PNTable.code === pnTable.code);
        await _db.db.models.Order.update({
          OrderGroupShipmentId: orderGroupShipment.id,
          status: 'UNDISPATCHED'
        }, {
          where: {
            id: pnTablesOrders.map(pntableOrder => pntableOrder.id)
          },
          transaction
        });
        let qrcodeSN = 0;
        const createdQRCode = await _db.db.models.OrderQRCode.findAll({
          where: {
            qrcode: {
              [_sequelize.Op.like]: `${(0, _moment.default)().format('YYYYMMDD')}${pnTable.code}%`
            }
          }
        }).map(q => `${q.qrcode.slice(20)}`); // get last 5 number;

        if (createdQRCode.length) {
          const biggestCode = Math.max(...createdQRCode);
          qrcodeSN = biggestCode;
        }

        await pnTablesOrders.map(order => () => new Promise(async innerResolve => {
          const {
            quantity
          } = order;
          const arr = Array.from(Array(quantity));
          const qrcodeArray = arr.map(() => {
            qrcodeSN += 1;
            return {
              qrcode: `${(0, _moment.default)().format('YYYYMMDD')}${order.PNTable.code}${(0, _padDigits.default)(qrcodeSN, 5)}`,
              OrderId: order.id
            };
          });
          await _db.db.models.OrderQRCode.bulkCreate(qrcodeArray, {
            transaction
          });
          innerResolve();
        })).reduce((prev, next) => prev.then(next), Promise.resolve());
        resolve();
      })).reduce((prev, next) => prev.then(next), Promise.resolve());
      await transaction.commit();
    } catch (e) {
      await transaction.rollback();
      throw e;
    }
  }

  static async groupOrder(closingOrders) {
    const closingOrdersPNTables = closingOrders.map(x => ({
      id: x.PNTable.id,
      code: x.PNTable.code
    }));

    const filteredOrderPNTables = _lodash.default.uniqBy(closingOrdersPNTables, 'id');

    OrderGroupShipment.createOrderGroupShipment(filteredOrderPNTables, closingOrders);
  }

}

exports.default = OrderGroupShipment;
(0, _defineProperty2.default)(OrderGroupShipment, "fields", (0, _OrderGroupShipment.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(OrderGroupShipment, "scopes", {
  search: args => {
    const {
      date,
      startDate,
      endDate,
      shippingStatus
    } = args;
    const whereClause = {};

    if (startDate) {
      whereClause.createdAt = {
        [_sequelize.Op.and]: [{
          [_sequelize.Op.gte]: (0, _moment.default)(startDate)
        }, {
          [_sequelize.Op.lt]: endDate && (0, _moment.default)(endDate).add(1, 'd') || (0, _moment.default)().add(1, 'd')
        }]
      };
    }

    if (endDate && !startDate) {
      whereClause.createdAt = {
        [_sequelize.Op.lt]: (0, _moment.default)(endDate).add(1, 'd')
      };
    }

    if (date) whereClause.SN = date;
    if (shippingStatus) whereClause.status = shippingStatus;
    return {
      where: whereClause
    };
  },
  pnTableAllAssoicateAndOrder: args => {
    const {
      vendorId
    } = args;
    const vendorWhereClause = {};
    if (vendorId) vendorWhereClause.id = vendorId;
    return {
      include: [{
        model: _db.db.models.PNTable.scope('categories', {
          method: ['search', args]
        }),
        required: true,
        include: [{
          model: _db.db.models.Vendor,
          where: vendorWhereClause
        }]
      }, {
        model: _db.db.models.Order.scope({
          method: ['memberArea', args]
        })
      }]
    };
  },
  pages: (limit, offset) => ({
    limit,
    offset
  }),
  pnTable: args => ({
    include: [{
      model: _db.db.models.PNTable.scope({
        method: ['search', args]
      })
    }]
  }),
  order: (args, required) => ({
    include: [{
      model: _db.db.models.Order.scope({
        method: ['search', args]
      })
    }],
    required: required || true
  })
});