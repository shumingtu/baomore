"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _VendorSetting = _interopRequireDefault(require("../columns/VendorSetting.js"));

var _db = require("../db");

class VendorSetting extends _sequelize.Model {
  static init(sequelize) {
    return super.init(VendorSetting.fields, {
      sequelize,
      paranoid: false
    });
  }

  static associate() {
    VendorSetting.belongsTo(_db.db.models.Vendor);
    VendorSetting.belongsTo(_db.db.models.Member);
  }

}

exports.default = VendorSetting;
(0, _defineProperty2.default)(VendorSetting, "fields", (0, _VendorSetting.default)(_sequelize.DataTypes));