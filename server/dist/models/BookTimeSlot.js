"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _BookTimeSlot = _interopRequireDefault(require("../columns/BookTimeSlot.js"));

var _db = require("../db");

class BookTimeSlot extends _sequelize.Model {
  static init(sequelize) {
    return super.init(BookTimeSlot.fields, {
      sequelize,
      scopes: BookTimeSlot.scopes
    });
  }

  static associate() {
    BookTimeSlot.belongsTo(_db.db.models.BookTimeSlotGroup);
  }

}

exports.default = BookTimeSlot;
(0, _defineProperty2.default)(BookTimeSlot, "fields", (0, _BookTimeSlot.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(BookTimeSlot, "scopes", {
  search: args => {
    const {
      id,
      time
    } = args;
    let whereClause = {};
    if (id) whereClause.id = id;
    if (time) whereClause.time = time;
    return {
      where: whereClause
    };
  }
});