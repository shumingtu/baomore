"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _Landing = _interopRequireDefault(require("../columns/Landing.js"));

var _db = require("../db");

var _General = require("../errors/General");

class Landing extends _sequelize.Model {
  static init(sequelize) {
    return super.init(Landing.fields, {
      sequelize
    });
  }

  static associate() {}

  static async createLanding({
    desktopImg,
    mobileImg,
    link
  }) {
    const landing = await _db.db.models.Landing.create({
      desktopImg,
      mobileImg,
      link: link || null
    });

    if (!landing) {
      return {
        error: 'Landing create failed.'
      };
    }

    return landing;
  }

  static async editLanding({
    id,
    desktopImg,
    mobileImg,
    link
  }) {
    const landing = await _db.db.models.Landing.findOne({
      where: {
        id
      }
    });

    if (!landing) {
      throw new _General.ResourceNotFoundError();
    }

    if (link) landing.link = link;
    landing.desktopImg = desktopImg;
    landing.mobileImg = mobileImg;
    await landing.save();
    return landing;
  }

  static async deleteLanding({
    id
  }) {
    const landing = await _db.db.models.Landing.findOne({
      where: {
        id
      }
    });

    if (!landing) {
      throw new _General.ResourceNotFoundError();
    }

    await landing.destroy();
    return {
      id: landing.id,
      status: true,
      message: '刪除成功'
    };
  }

}

exports.default = Landing;
(0, _defineProperty2.default)(Landing, "fields", (0, _Landing.default)(_sequelize.DataTypes));