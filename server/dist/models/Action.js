"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _Action = _interopRequireDefault(require("../columns/Action.js"));

var _db = require("../db");

class Action extends _sequelize.Model {
  static init(sequelize) {
    return super.init(Action.fields, {
      sequelize
    });
  }

  static associate() {
    Action.belongsToMany(_db.db.models.Role, {
      through: 'RoleActions',
      foreignKey: 'ActionCode',
      otherKey: 'RoleId'
    });
  }

}

exports.default = Action;
(0, _defineProperty2.default)(Action, "fields", (0, _Action.default)(_sequelize.DataTypes));