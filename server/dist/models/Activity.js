"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _Activity = _interopRequireDefault(require("../columns/Activity.js"));

var _db = require("../db");

var _General = require("../errors/General");

class Activity extends _sequelize.Model {
  static init(sequelize) {
    return super.init(Activity.fields, {
      sequelize,
      scopes: Activity.scopes,
      hooks: Activity.hooks
    });
  }

  static associate() {
    Activity.hasMany(_db.db.models.ActivityDetailFragment, {
      as: 'fragments'
    });
    Activity.belongsTo(_db.db.models.ActivityCategory);
  }

  static async addOrUpdateFragments(fragments, activityId, transaction) {
    if (activityId) {
      await _db.db.models.ActivityDetailFragment.destroy({
        where: {
          ActivityId: activityId
        },
        force: true,
        transaction
      });

      if (fragments.length) {
        const fragmentWithSeq = fragments.map((f, idx) => ({ ...f,
          seqNum: idx + 1,
          ActivityId: activityId
        }));
        await _db.db.models.ActivityDetailFragment.bulkCreate(fragmentWithSeq, {
          transaction
        });
      }
    }
  }

  static async createActivity({
    name,
    desktopImg,
    mobileImg,
    description,
    categoryId,
    fragments
  }) {
    const transaction = await _db.db.transaction();

    try {
      const activityPayload = {
        name,
        desktopImg,
        mobileImg,
        description: description || null,
        ActivityCategoryId: categoryId
      };
      const activity = await _db.db.models.Activity.create(activityPayload, {
        transaction
      });

      if (!activity) {
        return {
          error: 'Activity create failed.'
        };
      }

      if (fragments) {
        await _db.db.models.Activity.addOrUpdateFragments(fragments, activity.id, transaction);
      }

      await transaction.commit();
      return activity;
    } catch (ex) {
      await transaction.rollback();
      throw ex;
    }
  }

  static async editActivity({
    id,
    name,
    desktopImg,
    mobileImg,
    description,
    fragments
  }) {
    const activity = await _db.db.models.Activity.findOne({
      where: {
        id
      }
    });

    if (!activity) {
      throw new _General.ResourceNotFoundError();
    }

    const transaction = await _db.db.transaction();

    try {
      await activity.update({
        name,
        desktopImg,
        mobileImg,
        description
      }, {
        transaction
      });

      if (fragments) {
        await _db.db.models.Activity.addOrUpdateFragments(fragments, activity.id, transaction);
      }

      await transaction.commit();
      return activity;
    } catch (ex) {
      await transaction.rollback();
      throw ex;
    }
  }

  static async deleteActivity({
    id
  }) {
    const activity = await _db.db.models.Activity.findOne({
      where: {
        id
      }
    });

    if (!activity) {
      throw new _General.ResourceNotFoundError();
    }

    await activity.destroy();
    return {
      id: activity.id,
      status: true,
      message: '刪除成功'
    };
  }

}

exports.default = Activity;
(0, _defineProperty2.default)(Activity, "fields", (0, _Activity.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(Activity, "scopes", {
  includeDetailFragments: () => ({
    include: [{
      model: _db.db.models.ActivityDetailFragment,
      as: 'fragments'
    }]
  })
});
(0, _defineProperty2.default)(Activity, "hooks", {
  afterDestroy: async activity => {
    await _db.db.models.ActivityDetailFragment.destroy({
      where: {
        ActivityId: activity.id
      }
    });
  }
});