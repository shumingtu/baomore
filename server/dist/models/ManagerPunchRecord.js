"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _moment = _interopRequireDefault(require("moment"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _ManagerPunchRecord = _interopRequireDefault(require("../columns/ManagerPunchRecord.js"));

var _db = require("../db");

class ManagerPunchRecord extends _sequelize.Model {
  static init(sequelize) {
    return super.init(ManagerPunchRecord.fields, {
      sequelize,
      scopes: ManagerPunchRecord.scopes
    });
  }

  static associate() {
    ManagerPunchRecord.belongsTo(_db.db.models.Member, {
      as: 'EmployeedMember'
    });
    ManagerPunchRecord.belongsTo(_db.db.models.Member);
    ManagerPunchRecord.belongsTo(_db.db.models.Store);
  }

}

exports.default = ManagerPunchRecord;
(0, _defineProperty2.default)(ManagerPunchRecord, "fields", (0, _ManagerPunchRecord.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(ManagerPunchRecord, "scopes", {
  search: args => {
    const {
      employeeId,
      storeId,
      startDate,
      endDate
    } = args;
    const whereClause = {};
    if (employeeId) whereClause.EmployeedMemberId = employeeId;
    if (storeId) whereClause.StoreId = storeId;

    if (startDate) {
      whereClause.createdAt = {
        [_sequelize.Op.and]: [{
          [_sequelize.Op.gte]: (0, _moment.default)(startDate)
        }, {
          [_sequelize.Op.lte]: endDate && (0, _moment.default)(endDate) || (0, _moment.default)()
        }]
      };
    }

    return {
      where: whereClause
    };
  },
  store: () => ({
    include: [{
      model: _db.db.models.Store
    }]
  }),
  pages: (limit, offset) => ({
    limit,
    offset
  })
});