"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _moment = _interopRequireDefault(require("moment"));

var _Order = _interopRequireDefault(require("../columns/Order.js"));

var _db = require("../db");

class Order extends _sequelize.Model {
  static init(sequelize) {
    return super.init(Order.fields, {
      sequelize,
      scopes: Order.scopes
    });
  }

  static associate() {
    Order.belongsTo(_db.db.models.Store);
    Order.belongsTo(_db.db.models.Member, {
      foreignKey: 'EmpolyedMemberId'
    });
    Order.belongsTo(_db.db.models.OrderGroupShipment);
    Order.hasOne(_db.db.models.OnlineOrder);
    Order.belongsTo(_db.db.models.PNTable);
    Order.hasMany(_db.db.models.OrderQRCode);
  }

}

exports.default = Order;
(0, _defineProperty2.default)(Order, "fields", (0, _Order.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(Order, "scopes", {
  InPnTableIds: pnTableIds => ({
    where: {
      PNTableId: {
        [_sequelize.Op.in]: pnTableIds
      }
    }
  }),
  excludePnTableIds: excludePnTableIds => ({
    where: {
      PNTableId: {
        [_sequelize.Op.notIn]: excludePnTableIds
      }
    }
  }),
  findByOrderGroupShipmentId: id => ({
    where: {
      OrderGroupShipmentId: id
    }
  }),
  memberArea: args => ({
    include: [{
      model: _db.db.models.Member.scope({
        method: ['search', args]
      }),
      include: [{
        model: _db.db.models.Area
      }]
    }]
  }),
  search: args => {
    const {
      memberId,
      status,
      orderSN
    } = args;
    const whereClause = {};
    if (memberId) whereClause.EmpolyedMemberId = memberId;
    if (status) whereClause.status = status;
    if (orderSN) whereClause.orderSN = orderSN;
    return {
      where: whereClause
    };
  },
  store: () => ({
    include: [{
      model: _db.db.models.Store,
      include: [{
        model: _db.db.models.Channel
      }, {
        model: _db.db.models.District
      }]
    }]
  }),
  pages: (limit, offset) => ({
    limit,
    offset
  }),
  orderGroupShipment: args => {
    const {
      closeDate
    } = args;
    const whereClause = {};

    if (closeDate) {
      whereClause.createdAt = {
        [_sequelize.Op.and]: [{
          [_sequelize.Op.gte]: (0, _moment.default)(closeDate)
        }, {
          [_sequelize.Op.lt]: (0, _moment.default)(closeDate).add(1, 'd')
        }]
      };
    }

    return {
      include: [{
        model: _db.db.models.OrderGroupShipment,
        where: whereClause,
        required: !!closeDate
      }]
    };
  },
  qrcodes: args => {
    const {
      qrcode
    } = args;
    const whereClause = {};

    if (qrcode) {
      whereClause.qrcode = qrcode;
    }

    return {
      include: [{
        model: _db.db.models.OrderQRCode,
        where: whereClause,
        required: !!qrcode
      }]
    };
  }
});