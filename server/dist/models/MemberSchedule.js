"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _moment = _interopRequireDefault(require("moment"));

var _MemberSchedule = _interopRequireDefault(require("../columns/MemberSchedule.js"));

var _db = require("../db");

class MemberSchedule extends _sequelize.Model {
  static init(sequelize) {
    return super.init(MemberSchedule.fields, {
      sequelize,
      scopes: MemberSchedule.scopes
    });
  }

  static associate() {
    MemberSchedule.belongsTo(_db.db.models.Member);
  }

}

exports.default = MemberSchedule;
(0, _defineProperty2.default)(MemberSchedule, "fields", (0, _MemberSchedule.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(MemberSchedule, "scopes", {
  search: args => {
    const {
      employeeId,
      startDate,
      endDate
    } = args;
    const whereClause = {};
    if (employeeId) whereClause.MemberId = employeeId;

    if (startDate && endDate) {
      whereClause.date = {
        [_sequelize.Op.and]: [{
          [_sequelize.Op.gte]: (0, _moment.default)(startDate)
        }, {
          [_sequelize.Op.lt]: (0, _moment.default)(endDate).add(1, 'd')
        }]
      };
    }

    return {
      where: whereClause
    };
  }
});