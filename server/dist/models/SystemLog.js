"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _SystemLog = _interopRequireDefault(require("../columns/SystemLog.js"));

var _db = require("../db");

class SystemLog extends _sequelize.Model {
  static init(sequelize) {
    return super.init(SystemLog.fields, {
      sequelize,
      scopes: SystemLog.scopes
    });
  }

  static associate() {}

  static createLog(typeId, memberId, log) {
    SystemLog.create({
      SystemLogTypeId: typeId,
      MemberId: memberId,
      log
    });
  }

}

exports.default = SystemLog;
(0, _defineProperty2.default)(SystemLog, "fields", (0, _SystemLog.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(SystemLog, "scopes", {
  search: args => {
    const {
      id
    } = args;
    const whereClause = {};
    if (id) whereClause.id = id;
    return {
      where: whereClause
    };
  }
});