"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _moment = _interopRequireDefault(require("moment"));

var _Announcement = _interopRequireDefault(require("../columns/Announcement.js"));

var _db = require("../db");

var _General = require("../errors/General");

class Announcement extends _sequelize.Model {
  static init(sequelize) {
    return super.init(Announcement.fields, {
      sequelize,
      defaultScope: Announcement.defaultScope,
      scopes: Announcement.scopes
    });
  }

  static associate() {}

  static async createAnnouncement({
    title,
    content
  }) {
    const announcement = await _db.db.models.Announcement.create({
      title,
      content
    });

    if (!announcement) {
      return {
        error: 'Announcement create failed.'
      };
    }

    return announcement;
  }

  static async editAnnouncement({
    id,
    title,
    content
  }) {
    const announcement = await _db.db.models.Announcement.findOne({
      where: {
        id
      }
    });

    if (!announcement) {
      throw new _General.ResourceNotFoundError();
    }

    announcement.title = title;
    announcement.content = content;
    await announcement.save();
    return announcement;
  }

  static async deleteAnnouncement({
    id
  }) {
    const announcement = await _db.db.models.Announcement.findOne({
      where: {
        id
      }
    });

    if (!announcement) {
      throw new _General.ResourceNotFoundError();
    }

    await announcement.destroy();
    return {
      id: announcement.id,
      status: true,
      message: '成功刪除公告'
    };
  }

}

exports.default = Announcement;
(0, _defineProperty2.default)(Announcement, "fields", (0, _Announcement.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(Announcement, "defaultScope", {
  order: [['createdAt', 'DESC']]
});
(0, _defineProperty2.default)(Announcement, "scopes", {
  search: args => {
    const {
      id,
      keyword,
      startDate,
      endDate
    } = args;
    let whereClause = {};
    if (id) whereClause.id = id;

    if (startDate || endDate) {
      whereClause.createdAt = {
        [_sequelize.Op.and]: [{
          [_sequelize.Op.gte]: startDate && (0, _moment.default)(startDate) || (0, _moment.default)()
        }, {
          [_sequelize.Op.lte]: endDate && (0, _moment.default)(endDate) || (0, _moment.default)()
        }]
      };
    }

    if (keyword) {
      whereClause = { ...whereClause,
        [_sequelize.Op.or]: [{
          title: {
            [_sequelize.Op.like]: `%${keyword}%`
          }
        }, {
          content: {
            [_sequelize.Op.like]: `%${keyword}%`
          }
        }]
      };
    }

    return {
      where: whereClause
    };
  },
  order: {
    order: [['createdAt', 'DESC']]
  },
  pages: (limit, offset) => ({
    limit,
    offset
  })
});