"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _moment = _interopRequireDefault(require("moment"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _MemberPunchRecord = _interopRequireDefault(require("../columns/MemberPunchRecord.js"));

var _db = require("../db");

class MemberPunchRecord extends _sequelize.Model {
  static init(sequelize) {
    return super.init(MemberPunchRecord.fields, {
      sequelize,
      scopes: MemberPunchRecord.scopes
    });
  }

  static associate() {
    MemberPunchRecord.belongsTo(_db.db.models.Member);
    MemberPunchRecord.belongsTo(_db.db.models.Store);
  }

}

exports.default = MemberPunchRecord;
(0, _defineProperty2.default)(MemberPunchRecord, "fields", (0, _MemberPunchRecord.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(MemberPunchRecord, "scopes", {
  search: args => {
    const {
      memberId,
      storeId,
      startDate,
      endDate
    } = args;
    const whereClause = {};
    if (memberId) whereClause.MemberId = memberId;
    if (storeId) whereClause.StoreId = storeId;

    if (startDate) {
      whereClause.punchTime = {
        [_sequelize.Op.and]: [{
          [_sequelize.Op.gte]: (0, _moment.default)(startDate)
        }, {
          [_sequelize.Op.lte]: endDate && (0, _moment.default)(endDate).endOf('day') || (0, _moment.default)()
        }]
      };
    }

    return {
      where: whereClause
    };
  },
  store: () => ({
    include: [{
      model: _db.db.models.Store
    }]
  }),
  pages: (limit, offset) => ({
    limit,
    offset
  }),
  member: () => ({
    include: [{
      model: _db.db.models.Member
    }]
  })
});