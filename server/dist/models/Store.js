"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _Store = _interopRequireDefault(require("../columns/Store.js"));

var _db = require("../db");

class Store extends _sequelize.Model {
  static init(sequelize) {
    return super.init(Store.fields, {
      sequelize,
      scopes: Store.scopes,
      hooks: Store.hooks
    });
  }

  static associate() {
    Store.belongsTo(_db.db.models.Area);
    Store.belongsTo(_db.db.models.Channel);
    Store.belongsTo(_db.db.models.District);
    Store.belongsToMany(_db.db.models.Member, {
      through: _db.db.models.MemberStore,
      foreignKey: 'StoreId',
      otherKey: 'MemberId'
    });
    Store.hasMany(_db.db.models.OnlineOrder);
    Store.hasMany(_db.db.models.Order);
  }

}

exports.default = Store;
(0, _defineProperty2.default)(Store, "fields", (0, _Store.default)(_sequelize.DataTypes));
(0, _defineProperty2.default)(Store, "hooks", {
  beforeBulkCreate: instances => {
    instances.forEach(instance => {
      console.log('Store beforeBulkCreate', instance);
    });
  }
});
(0, _defineProperty2.default)(Store, "scopes", {
  search: args => {
    const {
      storeId,
      districtId,
      keyword,
      channelId,
      isForClient
    } = args;
    let whereClause = {};
    if (storeId) whereClause.id = storeId;
    if (districtId) whereClause.DistrictId = districtId;
    if (channelId) whereClause.ChannelId = channelId;

    if (keyword) {
      whereClause = { ...whereClause,
        [_sequelize.Op.or]: [{
          name: {
            [_sequelize.Op.like]: `%${keyword}%`
          }
        }, {
          phone: {
            [_sequelize.Op.like]: `%${keyword}%`
          }
        }, {
          address: {
            [_sequelize.Op.like]: `%${keyword}%`
          }
        }]
      };
    }

    if (isForClient) whereClause.archive = false;
    return {
      where: whereClause
    };
  },
  pages: (limit, offset) => ({
    limit,
    offset
  }),
  area: () => ({
    include: [{
      model: _db.db.models.Area
    }]
  }),
  channel: isForCustomize => {
    const whereClause = {};
    if (isForCustomize) whereClause.isCustomizeChannel = isForCustomize;
    return {
      include: [{
        model: _db.db.models.Channel,
        where: whereClause,
        required: isForCustomize
      }]
    };
  },
  district: cityId => {
    const cityWhereClause = {};
    if (cityId) cityWhereClause.id = cityId;
    return {
      include: [{
        model: _db.db.models.District,
        required: !!cityId,
        include: [{
          model: _db.db.models.City,
          where: cityWhereClause
        }]
      }]
    };
  },
  members: isForClient => {
    const whereClause = {};

    if (isForClient) {
      whereClause.lineId = {
        [_sequelize.Op.ne]: null
      };
      whereClause.archive = false;
    }

    return {
      include: [{
        model: _db.db.models.Member,
        where: whereClause,
        required: isForClient
      }]
    };
  }
});