"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _OrderQRCode = _interopRequireDefault(require("../columns/OrderQRCode.js"));

var _db = require("../db");

class OrderQRCode extends _sequelize.Model {
  static init(sequelize) {
    return super.init(OrderQRCode.fields, {
      sequelize
    });
  }

  static associate() {
    OrderQRCode.belongsTo(_db.db.models.Order);
  }

}

exports.default = OrderQRCode;
(0, _defineProperty2.default)(OrderQRCode, "fields", (0, _OrderQRCode.default)(_sequelize.DataTypes));