"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = ({
  DATE,
  BOOLEAN
}) => ({
  sendBookedInfoTime: {
    type: DATE,
    allowNull: false
  },
  isNotified: {
    type: BOOLEAN,
    defaultValue: false
  }
});

exports.default = _default;