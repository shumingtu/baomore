"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = ({
  INTEGER,
  TIME,
  DATE,
  BOOLEAN
}) => ({
  id: {
    type: INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  closedDay: {
    type: INTEGER,
    allowNull: false
  },
  closedTime: {
    type: TIME,
    allowNull: false
  },
  PNTableId: {
    type: INTEGER,
    references: {
      model: 'PNTables',
      key: 'id'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  },
  isClosed: {
    type: BOOLEAN,
    defaultValue: false
  },
  createdAt: {
    type: DATE,
    allowNull: false
  },
  updatedAt: {
    type: DATE,
    allowNull: false
  },
  deletedAt: {
    type: DATE
  }
});

exports.default = _default;