"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = ({
  INTEGER,
  STRING,
  DATE,
  BOOLEAN
}) => ({
  id: {
    type: INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  service: {
    type: STRING,
    allowNull: false
  },
  phoneModel: {
    type: STRING,
    allowNull: false
  },
  store: {
    type: STRING,
    allowNull: false
  },
  VAT: {
    type: STRING,
    allowNull: false
  },
  IME: {
    type: STRING
  },
  phone: {
    type: STRING
  },
  isApproved: {
    type: BOOLEAN,
    defaultValue: false
  },
  approverSN: {
    type: STRING
  },
  verifiedTime: {
    type: DATE
  },
  createdAt: {
    type: DATE,
    allowNull: false
  },
  updatedAt: {
    type: DATE,
    allowNull: false
  },
  deletedAt: {
    type: DATE
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0
  },
  MemberId: {
    type: INTEGER,
    references: {
      model: 'Members',
      key: 'id'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  },
  ApproverId: {
    type: INTEGER,
    references: {
      model: 'Members',
      as: 'Approver',
      key: 'id'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  }
});

exports.default = _default;