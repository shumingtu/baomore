"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = ({
  INTEGER,
  DATE,
  STRING,
  BOOLEAN
}) => ({
  id: {
    type: INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  createdAt: {
    type: DATE,
    allowNull: false
  },
  updatedAt: {
    type: DATE,
    allowNull: false
  },
  deletedAt: {
    type: DATE
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0
  },
  name: {
    type: STRING,
    unique: true,
    allowNull: false
  },
  removable: {
    type: BOOLEAN,
    allowNull: false,
    defaultValue: true
  }
});

exports.default = _default;