"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = ({
  INTEGER,
  DATE,
  DOUBLE,
  ENUM
}) => ({
  id: {
    type: INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  type: {
    type: ENUM,
    values: ['ON', 'OFF']
  },
  latitude: {
    type: DOUBLE,
    allowNull: false
  },
  longitude: {
    type: DOUBLE,
    allowNull: false
  },
  punchTime: {
    type: DATE,
    allowNull: false
  },
  createdAt: {
    type: DATE,
    allowNull: false
  },
  updatedAt: {
    type: DATE,
    allowNull: false
  },
  deletedAt: {
    type: DATE
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0
  },
  MemberId: {
    type: INTEGER,
    references: {
      model: 'Members',
      key: 'id'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  },
  StoreId: {
    type: INTEGER,
    references: {
      model: 'Stores',
      key: 'id'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  }
});

exports.default = _default;