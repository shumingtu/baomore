"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = ({
  INTEGER,
  STRING,
  TEXT,
  DATE,
  BOOLEAN
}) => ({
  id: {
    type: INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  name: {
    type: STRING,
    allowNull: false
  },
  desktopImg: {
    type: STRING,
    allowNull: false
  },
  isActived: {
    type: BOOLEAN,
    allowNull: false,
    defaultValue: false
  },
  mobileImg: {
    type: STRING,
    allowNull: false
  },
  description: {
    type: TEXT,
    defaultValue: null
  },
  createdAt: {
    type: DATE,
    allowNull: false
  },
  updatedAt: {
    type: DATE,
    allowNull: false
  },
  deletedAt: {
    type: DATE
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0
  },
  ActivityCategoryId: {
    type: INTEGER,
    references: {
      model: 'ActivityCategories',
      key: 'id'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  }
});

exports.default = _default;