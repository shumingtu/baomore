"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = ({
  INTEGER,
  STRING,
  DATE,
  BOOLEAN
}) => ({
  id: {
    type: INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: STRING
  },
  isPurchaseChannel: {
    type: BOOLEAN,
    allowNull: false,
    defaultValue: 0
  },
  isCustomizeChannel: {
    type: BOOLEAN,
    allowNull: false,
    defaultValue: 0
  },
  createdAt: {
    type: DATE,
    allowNull: false
  },
  updatedAt: {
    type: DATE,
    allowNull: false
  },
  deletedAt: {
    type: DATE
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0
  }
});

exports.default = _default;