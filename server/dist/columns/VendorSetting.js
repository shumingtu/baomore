"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = ({
  INTEGER,
  ENUM,
  STRING
}) => ({
  id: {
    type: INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  type: {
    type: ENUM,
    values: ['QUANTITY', 'MONEY'],
    allowNull: false
  },
  value: {
    type: INTEGER,
    allowNull: false
  },
  message: {
    type: STRING,
    allowNull: false
  }
});

exports.default = _default;