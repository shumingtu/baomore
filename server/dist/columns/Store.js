"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = ({
  INTEGER,
  STRING,
  DATE,
  DOUBLE,
  BOOLEAN
}) => ({
  id: {
    type: INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: STRING,
    allowNull: false
  },
  phone: {
    type: STRING,
    allowNull: false
  },
  address: {
    type: STRING,
    allowNull: false
  },
  latitude: {
    type: DOUBLE
  },
  longitude: {
    type: DOUBLE
  },
  manager: {
    // 一階主管，單純紀錄
    type: STRING
  },
  createdAt: {
    type: DATE,
    allowNull: false
  },
  updatedAt: {
    type: DATE,
    allowNull: false
  },
  deletedAt: {
    type: DATE
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0
  },
  AreaId: {
    type: INTEGER,
    references: {
      model: 'Areas',
      key: 'id'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  },
  ChannelId: {
    type: INTEGER,
    references: {
      model: 'Channels',
      key: 'id'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  },
  DistrictId: {
    type: INTEGER,
    references: {
      model: 'Districts',
      key: 'id'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  },
  archive: {
    type: BOOLEAN,
    allowNull: false,
    defaultValue: 0
  }
});

exports.default = _default;