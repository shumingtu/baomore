"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = ({
  INTEGER,
  STRING,
  DATE
}) => ({
  code: {
    primaryKey: true,
    allowNull: false,
    unique: true,
    type: INTEGER.UNSIGNED,
    max: 4294967295,
    min: 0
  },
  createdAt: {
    type: DATE,
    allowNull: false
  },
  updatedAt: {
    type: DATE,
    allowNull: false
  },
  deletedAt: {
    type: DATE
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0
  },
  name: {
    type: STRING,
    unique: true
  },
  description: {
    type: STRING
  }
});

exports.default = _default;