"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = ({
  INTEGER,
  DATE
}) => ({
  createdAt: {
    type: DATE,
    allowNull: false
  },
  updatedAt: {
    type: DATE,
    allowNull: false
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0
  },
  RoleId: {
    type: INTEGER,
    references: {
      model: 'Roles',
      key: 'id'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  },
  ActionCode: {
    type: INTEGER.UNSIGNED,
    references: {
      model: 'Actions',
      key: 'code'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  }
});

exports.default = _default;