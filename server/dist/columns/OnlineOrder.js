"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = ({
  INTEGER,
  STRING,
  DATE,
  BOOLEAN,
  ENUM,
  DOUBLE,
  DATEONLY
}) => ({
  id: {
    type: STRING,
    allowNull: false,
    unique: true,
    primaryKey: true
  },
  picture: {
    type: STRING,
    allowNull: false
  },
  name: {
    type: STRING,
    allowNull: false
  },
  phone: {
    type: STRING,
    allowNull: false
  },
  email: {
    type: STRING,
    allowNull: false
  },
  date: {
    type: DATEONLY
  },
  time: {
    type: STRING
  },
  isShared: {
    type: BOOLEAN,
    defaultValue: 0
  },
  payedStatus: {
    type: ENUM,
    values: ['UNPAID', 'PAIDNOTCONFIRMED', 'PAIDANDCONFIRMED', 'CANCELED', 'ROLLBACKED'] // 未付款、已付款包膜師未確認、包膜師已確認、已取消、已退款

  },
  shippingStatus: {
    type: ENUM,
    values: ['UNDISPATCHED', 'DISPATCHED', 'DISPATCHEDANDCHECKED', 'SOLDED'],
    // 未出貨、已出貨未銷貨、已出貨且已銷貨、已賣出
    defaultValue: 'UNDISPATCHED'
  },
  price: {
    type: DOUBLE
  },
  createdAt: {
    type: DATE,
    allowNull: false
  },
  updatedAt: {
    type: DATE,
    allowNull: false
  },
  deletedAt: {
    type: DATE
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0
  },
  StoreId: {
    type: INTEGER,
    references: {
      model: 'Stores',
      key: 'id'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  },
  BookedMemberId: {
    type: INTEGER,
    references: {
      model: 'Members',
      as: 'BookedMember',
      key: 'id'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  },
  OwnerId: {
    type: INTEGER,
    references: {
      model: 'Members',
      as: 'Owner',
      key: 'id'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  },
  OrderId: {
    type: INTEGER,
    references: {
      model: 'Orders',
      key: 'id'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  },
  isAssigned: {
    type: BOOLEAN,
    defaultValue: 0
  },
  PNTableId: {
    type: INTEGER,
    references: {
      model: 'PNTables',
      key: 'id'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  },
  BookTimeSlotGroupId: {
    type: INTEGER,
    references: {
      model: 'BookTimeSlotGroups',
      key: 'id'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  }
});

exports.default = _default;