"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = ({
  STRING
}) => ({
  qrcode: {
    type: STRING,
    allowNull: false
  }
});

exports.default = _default;