"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = ({
  INTEGER,
  STRING,
  DATE
}) => ({
  id: {
    type: INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  picture: {
    type: STRING,
    allowNull: false
  },
  link: {
    type: STRING,
    defaultValue: null
  },
  mobileImg: {
    type: STRING
  },
  createdAt: {
    type: DATE,
    allowNull: false
  },
  updatedAt: {
    type: DATE,
    allowNull: false
  },
  deletedAt: {
    type: DATE
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0
  }
});

exports.default = _default;