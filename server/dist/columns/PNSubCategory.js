"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

// 料號大小分類
var _default = ({
  DATE,
  INTEGER,
  STRING,
  BOOLEAN
}) => ({
  id: {
    type: INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: STRING,
    allowNull: false
  },
  picture: {
    type: STRING
  },
  isOnSale: {
    type: BOOLEAN,
    allowNull: false,
    defaultValue: 0
  },
  seqNum: {
    type: INTEGER,
    defaultValue: 1
  },
  createdAt: {
    type: DATE,
    allowNull: false
  },
  updatedAt: {
    type: DATE,
    allowNull: false
  },
  deletedAt: {
    type: DATE
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0
  },
  PNCategoryId: {
    type: INTEGER,
    references: {
      model: 'PNCategories',
      key: 'id'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  },
  PNSubCategoryId: {
    type: INTEGER,
    references: {
      model: 'PNSubCategories',
      key: 'id'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  }
});

exports.default = _default;