"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = ({
  INTEGER,
  TEXT,
  DATE,
  DOUBLE
}) => ({
  id: {
    type: INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  log: {
    type: TEXT,
    allowNull: false
  },
  createdAt: {
    type: DATE,
    allowNull: false
  },
  updatedAt: {
    type: DATE,
    allowNull: false
  },
  deletedAt: {
    type: DATE
  },
  version: {
    type: INTEGER,
    allowNull: false,
    defaultValue: 0
  },
  MemberId: {
    type: INTEGER,
    references: {
      model: 'Members',
      key: 'id'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  },
  SystemLogTypeId: {
    type: INTEGER,
    references: {
      model: 'SystemLogTypes',
      key: 'id'
    },
    onUpdate: 'cascade',
    onDelete: 'cascade'
  }
});

exports.default = _default;