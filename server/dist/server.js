"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _koa = _interopRequireDefault(require("koa"));

var _path = _interopRequireDefault(require("path"));

var _koaRouter = _interopRequireDefault(require("koa-router"));

var _sequelize = require("sequelize");

var _koaConvert = _interopRequireDefault(require("koa-convert"));

var _graphqlAuthKeeper = _interopRequireDefault(require("graphql-auth-keeper"));

var _koaSend = _interopRequireDefault(require("koa-send"));

var _apolloServerKoa = require("apollo-server-koa");

var _debug = _interopRequireDefault(require("debug"));

var _kcors = _interopRequireDefault(require("kcors"));

var _koaStatic = _interopRequireDefault(require("koa-static"));

var _koaBodyparser = _interopRequireDefault(require("koa-bodyparser"));

var _nodeCron = _interopRequireDefault(require("node-cron"));

var _moment = _interopRequireDefault(require("moment"));

var _schema = _interopRequireDefault(require("./schema"));

var _db = _interopRequireWildcard(require("./db"));

var _jwt = require("./helpers/jwt");

var _errorMiddleware = _interopRequireDefault(require("./helpers/errorMiddleware"));

var _lineRouter = require("./helpers/line/lineRouter");

var _s3UploadRouter = _interopRequireDefault(require("./routes/s3UploadRouter.js"));

var _linePayRouter = require("./helpers/line/linePayRouter");

var _authorization = _interopRequireDefault(require("./helpers/authorization"));

var _linebot = _interopRequireWildcard(require("./helpers/line/linebot"));

var _linebotMiddleware = _interopRequireDefault(require("./helpers/line/linebotMiddleware"));

var _exportRouter = _interopRequireDefault(require("./routes/exportRouter.js"));

var _importRouter = _interopRequireDefault(require("./routes/importRouter.js"));

const PORT = parseInt(process.env.PORT || '2113', 10);
const debugServer = (0, _debug.default)('BAOMORE:Server');
const app = new _koa.default();
const router = new _koaRouter.default(); // oAuth login

router.get('/auth/lineLogin', _lineRouter.memberLogin);
router.get('/auth/adminLineLogin', _lineRouter.adminLogin);
router.get('/auth/customize/lineLogin', _lineRouter.memberLoginFromCustomizePage);
router.get('/auth/lineBotLogin', _lineRouter.employeeLogin); // LINE Reserve

router.get('/line/pay/reserve', _linePayRouter.reserve); // LINE Bot Webhook

router.post('/lineWebHooks', (0, _linebotMiddleware.default)(_linebot.default));
app.use((0, _koaBodyparser.default)());
const authKeeper = new _graphqlAuthKeeper.default({
  secret: _jwt.JWT_TOKEN,
  syncFn: payload => _db.db.models.Member.findOne({
    where: {
      id: payload.id
    },
    include: [{
      model: _db.db.models.Role,
      attributes: ['id', 'name'],
      include: [{
        model: _db.db.models.Action,
        attributes: ['code']
      }]
    }]
  })
});
app.use((0, _koaConvert.default)((0, _kcors.default)()));
const server = new _apolloServerKoa.ApolloServer(authKeeper.apolloServerKoaOptions({
  schema: _schema.default,
  context: {
    db: _db.db
  },
  introspection: true
}));
server.applyMiddleware({
  app
});

if (process.env.NODE_ENV !== 'production') {
  app.use((0, _kcors.default)());
  app.use((0, _koaStatic.default)(_path.default.resolve(__dirname, 'static')));
  app.use((0, _koaStatic.default)(_path.default.join(__dirname, 'uploads')));
} else {
  app.use((0, _koaStatic.default)(_path.default.resolve(__dirname, 'static')));
  app.use((0, _koaStatic.default)(_path.default.join(__dirname, 'uploads')));
} // error middleware


app.use(_errorMiddleware.default);
app.use(_authorization.default);
app.use(router.routes());
app.use(router.allowedMethods()); // AWS S3 Uploader

app.use(_s3UploadRouter.default.routes());
app.use(_s3UploadRouter.default.allowedMethods()); // export router

app.use(_exportRouter.default.routes());
app.use(_exportRouter.default.allowedMethods()); // import router

app.use(_importRouter.default.routes());
app.use(_importRouter.default.allowedMethods());

if (process.env.NODE_ENV === 'production') {
  app.use(async ctx => {
    await (0, _koaSend.default)(ctx, 'server/static/index.html');
  });
} else {
  app.use(async ctx => {
    await (0, _koaSend.default)(ctx, 'client/static/index.html');
  });
} // LINE Bot Webhook


app.use((0, _linebotMiddleware.default)(_linebot.default));

_nodeCron.default.schedule('* * * * *', async () => {
  const saleBehavior = await _db.db.models.Behavior.findOne({
    where: {
      name: '銷貨'
    }
  });
  if (!saleBehavior) return;
  const unCompletedSaleRecord = await _db.db.models.BehaviorRecord.findAll({
    where: {
      BehaviorId: saleBehavior.id,
      startTime: {
        [_sequelize.Op.lte]: (0, _moment.default)().subtract(1, 'hours').toDate() // 超過一小時

      },
      endTime: null
    }
  });

  if (unCompletedSaleRecord.length) {
    const transaction = await _db.db.transaction();

    try {
      await Promise.all(unCompletedSaleRecord.map(async record => {
        await record.update({
          endTime: new Date()
        }, {
          transaction
        });
        await _db.db.models.MemberStock.reduceStock(record.MemberId, record.PNTableId, record.quantity, transaction);
      }));
      await transaction.commit();
    } catch (e) {
      await transaction.rollback();
    }
  }
});

_nodeCron.default.schedule('* * * * *', async () => {
  const confirmSchedules = await _db.db.models.EmployeeConfirmSchedule.findAll({
    where: {
      isNotified: false,
      sendBookedInfoTime: {
        [_sequelize.Op.lte]: (0, _moment.default)().subtract(10, 'minutes').toDate()
      }
    },
    include: [{
      model: _db.db.models.OnlineOrder.scope('bookedMember', 'member', 'store'),
      where: {
        payedStatus: 'PAIDNOTCONFIRMED'
      }
    }]
  }); // remove 2019-08-05
  // const transferSchedules = await db.models.EmployeeTransferSchedule.findAll({
  //   where: {
  //     isNotified: false,
  //     sendTransferTime: {
  //       [Op.gte]: moment().subtract(10, 'minutes').toDate(),
  //     },
  //   },
  //   include: [{
  //     model: db.models.BehaviorRecord,
  //     where: {
  //       isTransfering: true,
  //     },
  //     include: [{
  //       model: db.models.Member,
  //     }],
  //   }],
  // });
  // if (transferSchedules.length) {
  //   const transferScheduleIds = transferSchedules.map(t => t.id);
  //
  //   await db.models.EmployeeTransferSchedule.update({
  //     isNotified: true,
  //   }, {
  //     where: {
  //       id: transferScheduleIds,
  //     },
  //   });
  //
  //   transferSchedules.forEach(async (transferRecord) => {
  //     const director = await transferRecord.BehaviorRecord.Member.getMember();
  //
  //     await sendLineMessage(null, `包膜師：${transferRecord.BehaviorRecord.Member.name}超過10分鐘未確認調貨請求！`, director.lineId);
  //   });
  // }

  if (confirmSchedules.length) {
    const confirmScheduleIds = confirmSchedules.map(c => c.id);
    await _db.db.models.EmployeeConfirmSchedule.update({
      isNotified: true
    }, {
      where: {
        id: confirmScheduleIds
      }
    });
    confirmSchedules.forEach(async order => {
      const oriEmployee = order.OnlineOrder.BookedMember;
      const director = await order.OnlineOrder.BookedMember.getMember();
      await (0, _linebot.sendBookedInfoToDirector)(director.lineId, `【包膜師尚未確認預約】\n線上訂單編號：${order.OnlineOrder.id}\n訂購人姓名：${order.OnlineOrder.Owner.name}\n訂購人電話：${order.OnlineOrder.Owner.phone}\n預約時間：${order.OnlineOrder.date} ${order.OnlineOrder.time}\n預約門市：${order.OnlineOrder.Store.Channel.name}${order.OnlineOrder.Store.name}\n包膜師：${oriEmployee.name}`, order.OnlineOrder.id, director.id);
    });
  }
});

_nodeCron.default.schedule('*/5 * * * *', async () => {
  const dayOfWeek = (0, _moment.default)().weekday();
  const hourMinute = (0, _moment.default)().format('HH:mm');
  const closedOrderSetting = await _db.db.models.ClosedOrderSetting.findAll(); // 要結單的設定

  const needCloseSetting = closedOrderSetting.filter(x => x.closedDay === (dayOfWeek === 0 ? 7 : dayOfWeek) && x.closedTime <= hourMinute && x.isClosed === false); // 不結單的設定

  const excludeCloseSetting = closedOrderSetting.filter(x => x.closedDay !== (dayOfWeek === 0 ? 7 : dayOfWeek) || x.closedTime > hourMinute || x.isClosed === true // 已經結過單了
  ); // 要結單的pnTableIds

  const pnTableIds = needCloseSetting.map(x => x.PNTableId); // 不結單的pnTableIds

  const excludePnTableIds = excludeCloseSetting.map(x => x.PNTableId); // 結單後isClosed設定為true 避免重複結單, 不等於禮拜三的單isClosed要設定為false (禮拜二原本是true, 禮拜三變成false)

  const needCloseSettingIds = needCloseSetting.map(x => x.id);
  await _db.db.models.ClosedOrderSetting.update({
    isClosed: true
  }, {
    where: {
      id: needCloseSettingIds
    }
  });
  await _db.db.models.ClosedOrderSetting.update({
    isClosed: false
  }, {
    where: {
      closedDay: {
        [_sequelize.Op.ne]: dayOfWeek === 0 ? 7 : dayOfWeek
      }
    }
  });

  if (pnTableIds.length) {
    const closingOrders = await _db.db.models.Order.scope(pnTableIds.includes(null) ? {
      method: ['excludePnTableIds', excludePnTableIds]
    } : {
      method: ['InPnTableIds', pnTableIds]
    }).findAll({
      where: {
        status: 'ORDERING',
        OrderGroupShipmentId: null
      },
      include: [{
        model: _db.db.models.PNTable,
        required: true
      }]
    });

    if (closingOrders.length) {
      _db.db.models.OrderGroupShipment.groupOrder(closingOrders);
    }
  }
});

(0, _db.default)().then(() => {
  app.listen(PORT, () => debugServer(`🚀 Server Listen on Port: ${PORT}`));
});