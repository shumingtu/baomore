"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.ClosedOrderSettingIsAlreadyExist = exports.ClosedOrderSettingCreateError = exports.ClosedOrderSettingNotFoundError = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

const ErrorClass = Error;

class ClosedOrderSettingNotFoundError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 800);
    (0, _defineProperty2.default)(this, "message", this.message || 'ClosedOrderSetting Not Found');
    (0, _defineProperty2.default)(this, "human", '找不到該結單設定');
  }

}

exports.ClosedOrderSettingNotFoundError = ClosedOrderSettingNotFoundError;

class ClosedOrderSettingCreateError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 801);
    (0, _defineProperty2.default)(this, "message", this.message || 'ClosedOrderSetting Create Failed');
    (0, _defineProperty2.default)(this, "human", '結單設定新增失敗');
  }

}

exports.ClosedOrderSettingCreateError = ClosedOrderSettingCreateError;

class ClosedOrderSettingIsAlreadyExist extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 802);
    (0, _defineProperty2.default)(this, "message", this.message || '此料號已有設定結單時間');
    (0, _defineProperty2.default)(this, "human", 'ClosedOrderSetting Already Exist');
  }

}

exports.ClosedOrderSettingIsAlreadyExist = ClosedOrderSettingIsAlreadyExist;
var _default = null;
exports.default = _default;