"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.OnlineOrderNotFoundError = exports.CreateOnlineOrderError = exports.PayConfirmError = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

const ErrorClass = Error;

class PayConfirmError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 500);
    (0, _defineProperty2.default)(this, "message", this.message || 'LINE Pay Confirm API Error');
    (0, _defineProperty2.default)(this, "human", 'LINE Pay 確認付款失敗，請再嘗試一次');
  }

}

exports.PayConfirmError = PayConfirmError;

class CreateOnlineOrderError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 501);
    (0, _defineProperty2.default)(this, "message", this.message || 'Created Online Order Error');
    (0, _defineProperty2.default)(this, "human", '訂單成立失敗，請再次確認');
  }

}

exports.CreateOnlineOrderError = CreateOnlineOrderError;

class OnlineOrderNotFoundError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 501);
    (0, _defineProperty2.default)(this, "message", this.message || 'Online Order Not Found');
    (0, _defineProperty2.default)(this, "human", '查無此訂單');
  }

}

exports.OnlineOrderNotFoundError = OnlineOrderNotFoundError;