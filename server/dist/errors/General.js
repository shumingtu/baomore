"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DuplicatedLineIdError = exports.ResourceNotFoundError = exports.InvalidParameter = exports.SystemError = exports.MemberNotFoundError = exports.InvalidRefreshToken = exports.InvalidAccessToken = exports.PermissionError = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

class PermissionError extends Error {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 100);
    (0, _defineProperty2.default)(this, "message", this.message || 'Permission Error');
  }

}

exports.PermissionError = PermissionError;

class InvalidAccessToken extends Error {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 101);
    (0, _defineProperty2.default)(this, "message", this.message || 'Invalid Access Token');
  }

}

exports.InvalidAccessToken = InvalidAccessToken;

class InvalidRefreshToken extends Error {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 102);
    (0, _defineProperty2.default)(this, "message", this.message || 'Invalid Refresh Token');
  }

}

exports.InvalidRefreshToken = InvalidRefreshToken;

class MemberNotFoundError extends Error {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 103);
    (0, _defineProperty2.default)(this, "message", this.message || 'Member Not Found Error');
  }

}

exports.MemberNotFoundError = MemberNotFoundError;

class SystemError extends Error {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 104);
    (0, _defineProperty2.default)(this, "message", this.message || 'System Error');
  }

}

exports.SystemError = SystemError;

class InvalidParameter extends Error {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 105);
    (0, _defineProperty2.default)(this, "message", this.message || 'Invalid Parameter');
  }

}

exports.InvalidParameter = InvalidParameter;

class ResourceNotFoundError extends Error {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 106);
    (0, _defineProperty2.default)(this, "message", this.message || 'Resource Not Found');
  }

}

exports.ResourceNotFoundError = ResourceNotFoundError;

class DuplicatedLineIdError extends Error {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 107);
    (0, _defineProperty2.default)(this, "message", this.message || 'DuplicatedLineIdError');
  }

}

exports.DuplicatedLineIdError = DuplicatedLineIdError;