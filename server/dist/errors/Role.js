"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.RoleNotFoundError = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

const ErrorClass = Error;

class RoleNotFoundError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 300);
    (0, _defineProperty2.default)(this, "message", this.message || 'Role Not Found');
    (0, _defineProperty2.default)(this, "human", '找不到該角色');
  }

}

exports.RoleNotFoundError = RoleNotFoundError;
var _default = null;
exports.default = _default;