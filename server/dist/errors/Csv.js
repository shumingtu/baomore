"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.CsvImportError = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

const ErrorClass = Error;

class CsvImportError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 300);
    (0, _defineProperty2.default)(this, "message", this.message || 'Csv Import Failed');
    (0, _defineProperty2.default)(this, "human", 'CSV匯入失敗');
  }

}

exports.CsvImportError = CsvImportError;
var _default = null;
exports.default = _default;