"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.PrductTypeError = exports.QRCodeHasBeenTransferedError = exports.QRCodeHasBeenRollBackedOrSoldError = exports.SalesRecordNotFoundError = exports.StockOverflowError = exports.StockNotFoundError = exports.StockOrderGroupNotFoundError = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

const ErrorClass = Error;

class StockOrderGroupNotFoundError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 600);
    (0, _defineProperty2.default)(this, "message", this.message || '此商品已點入庫，請關閉後確認欲執行動作');
    (0, _defineProperty2.default)(this, "human", '找不到此點貨編號的結單紀錄，請再次確認');
  }

}

exports.StockOrderGroupNotFoundError = StockOrderGroupNotFoundError;

class StockNotFoundError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 601);
    (0, _defineProperty2.default)(this, "message", this.message || 'Member Stock Record Not Found Error');
    (0, _defineProperty2.default)(this, "human", '找不到此會員的庫存紀錄，請再次確認');
  }

}

exports.StockNotFoundError = StockNotFoundError;

class StockOverflowError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 602);
    (0, _defineProperty2.default)(this, "message", this.message || '庫存不足無法執行動作，請再次確認');
    (0, _defineProperty2.default)(this, "human", '庫存不足無法執行動作，請再次確認');
  }

}

exports.StockOverflowError = StockOverflowError;

class SalesRecordNotFoundError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 603);
    (0, _defineProperty2.default)(this, "message", this.message || 'Sales Record Not Found Error');
    (0, _defineProperty2.default)(this, "human", '查無銷貨紀錄，請重新銷貨');
  }

}

exports.SalesRecordNotFoundError = SalesRecordNotFoundError;

class QRCodeHasBeenRollBackedOrSoldError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 604);
    (0, _defineProperty2.default)(this, "message", this.message || '此商品已被退貨或銷貨調貨！');
    (0, _defineProperty2.default)(this, "human", '此商品已被退貨或銷貨調貨！');
  }

}

exports.QRCodeHasBeenRollBackedOrSoldError = QRCodeHasBeenRollBackedOrSoldError;

class QRCodeHasBeenTransferedError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 605);
    (0, _defineProperty2.default)(this, "message", this.message || '此商品已被調貨或退貨銷貨！');
    (0, _defineProperty2.default)(this, "human", '此商品已被調貨或退貨銷貨！');
  }

}

exports.QRCodeHasBeenTransferedError = QRCodeHasBeenTransferedError;

class PrductTypeError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 606);
    (0, _defineProperty2.default)(this, "message", this.message || '料號與產品類型不符！');
    (0, _defineProperty2.default)(this, "human", '料號與產品類型不符！');
  }

}

exports.PrductTypeError = PrductTypeError;
var _default = null;
exports.default = _default;