"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SERVER_ERROR = exports.PARAMETER_ERROR = exports.PERMISSION_ERROR = void 0;

const PERMISSION_ERROR = ctx => {
  ctx.status = 403;
  ctx.body = {
    message: '沒有權限'
  };
};

exports.PERMISSION_ERROR = PERMISSION_ERROR;

const PARAMETER_ERROR = ctx => {
  ctx.status = 403;
  ctx.body = {
    message: '參數錯誤'
  };
};

exports.PARAMETER_ERROR = PARAMETER_ERROR;

const SERVER_ERROR = ctx => {
  ctx.status = 500;
  ctx.body = {
    message: '伺服器錯誤'
  };
};

exports.SERVER_ERROR = SERVER_ERROR;