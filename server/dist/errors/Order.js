"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.OrderCreateFailed = exports.OrderUpdateFailError = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

class OrderUpdateFailError extends Error {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 2001);
    (0, _defineProperty2.default)(this, "message", this.message || '訂單更新失敗, 請確認訂單狀態！');
    (0, _defineProperty2.default)(this, "human", '訂單更新失敗, 請確認訂單狀態！');
  }

}

exports.OrderUpdateFailError = OrderUpdateFailError;

class OrderCreateFailed extends Error {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 2002);
    (0, _defineProperty2.default)(this, "message", this.message || '訂單新增失敗, 請再試一次！');
    (0, _defineProperty2.default)(this, "human", '訂單新增失敗, 請再試一次！');
  }

}

exports.OrderCreateFailed = OrderCreateFailed;
var _default = null;
exports.default = _default;