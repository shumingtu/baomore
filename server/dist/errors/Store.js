"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.StoreAddressError = exports.MemberStoreNotFound = exports.StoreNotFound = exports.MemberStoreCreateFailed = exports.StoreCreateFailed = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

const ErrorClass = Error;

class StoreCreateFailed extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 300);
    (0, _defineProperty2.default)(this, "message", this.message || 'Store Create Failed');
    (0, _defineProperty2.default)(this, "human", '門市新增失敗');
  }

}

exports.StoreCreateFailed = StoreCreateFailed;

class MemberStoreCreateFailed extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 301);
    (0, _defineProperty2.default)(this, "message", this.message || 'MemberStore Create Failed');
    (0, _defineProperty2.default)(this, "human", 'MemberStore Create Failed');
  }

}

exports.MemberStoreCreateFailed = MemberStoreCreateFailed;

class StoreNotFound extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 302);
    (0, _defineProperty2.default)(this, "message", this.message || 'Store Not Found');
    (0, _defineProperty2.default)(this, "human", '找不到該門市');
  }

}

exports.StoreNotFound = StoreNotFound;

class MemberStoreNotFound extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 303);
    (0, _defineProperty2.default)(this, "message", this.message || 'MemberStore Not Found');
    (0, _defineProperty2.default)(this, "human", 'MemberStore Not Found');
  }

}

exports.MemberStoreNotFound = MemberStoreNotFound;

class StoreAddressError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 304);
    (0, _defineProperty2.default)(this, "message", this.message || '門市地址錯誤');
    (0, _defineProperty2.default)(this, "human", '門市地址錯誤');
  }

}

exports.StoreAddressError = StoreAddressError;
var _default = null;
exports.default = _default;