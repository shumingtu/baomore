"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.OrderUpdateGroupShipmentError = exports.OrderGroupShipmentCreateError = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

const ErrorClass = Error;

class OrderGroupShipmentCreateError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 900);
    (0, _defineProperty2.default)(this, "message", this.message || 'OrderGroupShipment Created Failed');
    (0, _defineProperty2.default)(this, "human", '結單新增失敗');
  }

}

exports.OrderGroupShipmentCreateError = OrderGroupShipmentCreateError;

class OrderUpdateGroupShipmentError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 901);
    (0, _defineProperty2.default)(this, "message", this.message || 'OrderGroupShipmentId Updated Failed');
    (0, _defineProperty2.default)(this, "human", '訂單新增結單關聯失敗');
  }

}

exports.OrderUpdateGroupShipmentError = OrderUpdateGroupShipmentError;
var _default = null;
exports.default = _default;