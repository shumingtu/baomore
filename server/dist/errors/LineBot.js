"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.SetRichmenuError = exports.RichmenuNotFoundError = exports.RichmenuFetchError = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

const ErrorClass = Error;

class RichmenuFetchError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 700);
    (0, _defineProperty2.default)(this, "message", this.message || 'LINE Richmenus Fetch Error');
    (0, _defineProperty2.default)(this, "human", '無法取得 LINE BOT Richmenu 清單');
  }

}

exports.RichmenuFetchError = RichmenuFetchError;

class RichmenuNotFoundError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 701);
    (0, _defineProperty2.default)(this, "message", this.message || 'Richmenu Not Found Error');
    (0, _defineProperty2.default)(this, "human", '目前無可使用的 richmenus');
  }

}

exports.RichmenuNotFoundError = RichmenuNotFoundError;

class SetRichmenuError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 702);
    (0, _defineProperty2.default)(this, "message", this.message || 'Set Richmenu Error');
    (0, _defineProperty2.default)(this, "human", '設定指定 richmenu 發生錯誤');
  }

}

exports.SetRichmenuError = SetRichmenuError;
var _default = null;
exports.default = _default;