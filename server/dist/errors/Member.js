"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MemberCreateFaild = exports.MemberNotConnectLINEError = exports.MemberLINELoginError = exports.TokenInvalidError = exports.TokenExpiredError = exports.DuplicateAccountError = exports.InvalidRefreshToken = exports.PasswordNotMatchedError = exports.AccountNotFoundError = exports.PermissionError = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

const ErrorClass = Error;

class PermissionError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 200);
    (0, _defineProperty2.default)(this, "message", this.message || 'Permission Denied');
    (0, _defineProperty2.default)(this, "human", '沒有權限，請再次確認');
  }

}

exports.PermissionError = PermissionError;

class AccountNotFoundError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 201);
    (0, _defineProperty2.default)(this, "message", this.message || 'Account Not Found');
    (0, _defineProperty2.default)(this, "human", '帳號或密碼錯誤，請再次確認');
  }

}

exports.AccountNotFoundError = AccountNotFoundError;

class PasswordNotMatchedError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 202);
    (0, _defineProperty2.default)(this, "message", this.message || 'Password Not Matched');
    (0, _defineProperty2.default)(this, "human", '帳號或密碼錯誤，請再次確認');
  }

}

exports.PasswordNotMatchedError = PasswordNotMatchedError;

class InvalidRefreshToken extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 203);
    (0, _defineProperty2.default)(this, "message", this.message || 'Invalid Refresh Token Error');
  }

}

exports.InvalidRefreshToken = InvalidRefreshToken;

class DuplicateAccountError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 204);
    (0, _defineProperty2.default)(this, "message", this.message || 'Duplicate Account');
    (0, _defineProperty2.default)(this, "human", '已有相同帳號存在，請重新註冊');
  }

}

exports.DuplicateAccountError = DuplicateAccountError;

class TokenExpiredError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 205);
    (0, _defineProperty2.default)(this, "message", this.message || 'Token Expired Error');
  }

}

exports.TokenExpiredError = TokenExpiredError;

class TokenInvalidError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 206);
    (0, _defineProperty2.default)(this, "message", this.message || 'Token Invalid Error');
  }

}

exports.TokenInvalidError = TokenInvalidError;

class MemberLINELoginError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 207);
    (0, _defineProperty2.default)(this, "message", this.message || 'LINE Login Error Or Code Expired');
    (0, _defineProperty2.default)(this, "human", 'LINE Login 已失效請重新登入');
  }

}

exports.MemberLINELoginError = MemberLINELoginError;

class MemberNotConnectLINEError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 208);
    (0, _defineProperty2.default)(this, "message", this.message || 'Member LineId Is Not Found');
    (0, _defineProperty2.default)(this, "human", '此會員尚未與 LINE 連結，請再次確認');
  }

}

exports.MemberNotConnectLINEError = MemberNotConnectLINEError;

class MemberCreateFaild extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 209);
    (0, _defineProperty2.default)(this, "message", this.message || 'Member Create Failed');
    (0, _defineProperty2.default)(this, "human", '會員新增失敗');
  }

}

exports.MemberCreateFaild = MemberCreateFaild;