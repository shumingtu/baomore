"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.OnlineOrderConfirmedError = exports.OnlineOrderNotFoundError = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

const ErrorClass = Error;

class OnlineOrderNotFoundError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 1000);
    (0, _defineProperty2.default)(this, "message", this.message || 'OnlineOrder Not Found');
    (0, _defineProperty2.default)(this, "human", '找不到該線上訂單');
  }

}

exports.OnlineOrderNotFoundError = OnlineOrderNotFoundError;

class OnlineOrderConfirmedError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 1001);
    (0, _defineProperty2.default)(this, "message", this.message || '該線上訂單已被確認');
    (0, _defineProperty2.default)(this, "human", '該線上訂單已被確認');
  }

}

exports.OnlineOrderConfirmedError = OnlineOrderConfirmedError;
var _default = null;
exports.default = _default;