"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.PNSubCategoryCreateError = exports.PNSubCategoryNotFoundError = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

const ErrorClass = Error;

class PNSubCategoryNotFoundError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 700);
    (0, _defineProperty2.default)(this, "message", this.message || 'PNSubCategory Not Found');
    (0, _defineProperty2.default)(this, "human", '找不到該副類型');
  }

}

exports.PNSubCategoryNotFoundError = PNSubCategoryNotFoundError;

class PNSubCategoryCreateError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 701);
    (0, _defineProperty2.default)(this, "message", this.message || 'PNSubCategory Create Failed');
    (0, _defineProperty2.default)(this, "human", '副類型新增失敗');
  }

}

exports.PNSubCategoryCreateError = PNSubCategoryCreateError;
var _default = null;
exports.default = _default;