"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.VendorNotFound = exports.VendorCreateFailed = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

const ErrorClass = Error;

class VendorCreateFailed extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 300);
    (0, _defineProperty2.default)(this, "message", this.message || 'Vendor Create Failed');
    (0, _defineProperty2.default)(this, "human", '廠商新增失敗');
  }

}

exports.VendorCreateFailed = VendorCreateFailed;

class VendorNotFound extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 301);
    (0, _defineProperty2.default)(this, "message", this.message || 'Vendor Not Found');
    (0, _defineProperty2.default)(this, "human", '找不到廠商');
  }

}

exports.VendorNotFound = VendorNotFound;
var _default = null;
exports.default = _default;