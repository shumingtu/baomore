"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BrandModelColorNotFoundError = exports.BrandModelNotFoundError = exports.BrandNotFoundError = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

const ErrorClass = Error;

class BrandNotFoundError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 300);
    (0, _defineProperty2.default)(this, "message", this.message || 'Brand Not Found');
    (0, _defineProperty2.default)(this, "human", '找不到該品牌編號');
  }

}

exports.BrandNotFoundError = BrandNotFoundError;

class BrandModelNotFoundError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 301);
    (0, _defineProperty2.default)(this, "message", this.message || 'BrandModel Not Found');
    (0, _defineProperty2.default)(this, "human", '找不到該型號編號');
  }

}

exports.BrandModelNotFoundError = BrandModelNotFoundError;

class BrandModelColorNotFoundError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 302);
    (0, _defineProperty2.default)(this, "message", this.message || 'BrandModelColor Or Model Not Found');
    (0, _defineProperty2.default)(this, "human", '找不到該顏色編號或品牌編號');
  }

}

exports.BrandModelColorNotFoundError = BrandModelColorNotFoundError;