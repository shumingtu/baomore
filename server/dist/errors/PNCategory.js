"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.PNCategoryCreateError = exports.PNCategoryNotFoundError = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

const ErrorClass = Error;

class PNCategoryNotFoundError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 400);
    (0, _defineProperty2.default)(this, "message", this.message || 'PNCategory Not Found');
    (0, _defineProperty2.default)(this, "human", '找不到該料號類型');
  }

}

exports.PNCategoryNotFoundError = PNCategoryNotFoundError;

class PNCategoryCreateError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 401);
    (0, _defineProperty2.default)(this, "message", this.message || 'PNCategory Create Failed');
    (0, _defineProperty2.default)(this, "human", '類型新增失敗');
  }

}

exports.PNCategoryCreateError = PNCategoryCreateError;
var _default = null;
exports.default = _default;