"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.PNTableCreateError = exports.PNTableItemNotFoundError = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

const ErrorClass = Error;

class PNTableItemNotFoundError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 600);
    (0, _defineProperty2.default)(this, "message", this.message || 'PNTable Item Not Found');
    (0, _defineProperty2.default)(this, "human", '找不到該料號');
  }

}

exports.PNTableItemNotFoundError = PNTableItemNotFoundError;

class PNTableCreateError extends ErrorClass {
  constructor(...args) {
    super(...args);
    (0, _defineProperty2.default)(this, "code", 601);
    (0, _defineProperty2.default)(this, "message", this.message || 'PNTable Create Failed');
    (0, _defineProperty2.default)(this, "human", '新增料號失敗');
  }

}

exports.PNTableCreateError = PNTableCreateError;
var _default = null;
exports.default = _default;