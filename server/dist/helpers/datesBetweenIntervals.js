"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _moment = _interopRequireDefault(require("moment"));

var _default = (start, end, interval) => {
  let startDate = start;
  const intervals = [{
    date: startDate
  }];
  const diffUnitOfTime = `${interval}s`;

  while ((0, _moment.default)(end).diff(startDate, diffUnitOfTime) > 0) {
    const currentEnd = (0, _moment.default)((0, _moment.default)(startDate).add(1, diffUnitOfTime)).format('YYYY-MM-DD');
    intervals.push({
      date: currentEnd
    });
    startDate = currentEnd;
  }

  return intervals;
};

exports.default = _default;