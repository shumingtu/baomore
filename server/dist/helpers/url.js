"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getStaticDownloadURL = getStaticDownloadURL;
exports.getStaticFilename = getStaticFilename;
const PORT = process.env.PORT || 2113;
const API_HOST = process.env.API_HOST || `http://localhost:${PORT}`;

function getStaticDownloadURL(filename) {
  const pattern = new RegExp(/^((http|https):\/\/)/);
  if (pattern.test(filename)) return filename;
  return `${API_HOST}/${filename}`;
}

function getStaticFilename(url) {
  const re = new RegExp(`^${API_HOST}/`);
  return url.replace(re, '');
}