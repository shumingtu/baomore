"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = async (ctx, next) => {
  try {
    await next();
  } catch (e) {
    const status = e.status || 500;
    const code = e.code || null;
    const message = e.message || '伺服器錯誤';
    const human = e.human || null;

    if (e.code === 'LIMIT_FILE_SIZE') {
      ctx.body = {
        status,
        message,
        code: 100,
        human: '檔案過大'
      };
      return;
    }

    ctx.status = status;

    switch (ctx.status) {
      default:
        ctx.body = {
          status,
          message,
          code,
          human
        };
    }
  }
};

exports.default = _default;