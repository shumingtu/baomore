"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _querystring = _interopRequireDefault(require("querystring"));

var _requestPromise = _interopRequireDefault(require("request-promise"));

var _debug = _interopRequireDefault(require("debug"));

var _transformURLEncoder = _interopRequireDefault(require("./transformURLEncoder.js"));

var _getCheckMacValue = _interopRequireDefault(require("./getCheckMacValue.js"));

var _global = require("../../shared/global.js");

const debugInvoice = (0, _debug.default)('BAOMORE:INVOICE');

const encodeData = data => (0, _transformURLEncoder.default)(encodeURIComponent(data)).toLowerCase();

var _default = async onlineOrder => new Promise(async (resolve, reject) => {
  const payload = {
    Donation: '0',
    InvType: '07',
    ItemAmount: `${onlineOrder.price}`,
    ItemCount: '1',
    ItemPrice: `${onlineOrder.price}`,
    MerchantID: _global.invoiceSetting.MerchantID,
    Print: '0',
    RelateNumber: onlineOrder.id,
    SalesAmount: onlineOrder.price,
    TaxType: '1',
    TimeStamp: Math.floor(Date.now() / 1000),
    CustomerName: onlineOrder.name,
    // 客戶名稱，長度為20字元
    CustomerEmail: onlineOrder.email
  };
  const origin = { ...payload
  };
  payload.CustomerName = encodeData(payload.CustomerName);
  payload.CustomerEmail = encodeData(payload.CustomerEmail);
  const checkMacValue = (0, _getCheckMacValue.default)(payload, _global.invoiceSetting.HashKey, _global.invoiceSetting.HashIV);
  origin.ItemName = onlineOrder.PNTable.name;
  origin.ItemWord = '張';
  origin.CheckMacValue = checkMacValue;
  const options = {
    url: _global.INVOICING_API_HOST,
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: _querystring.default.stringify(origin)
  };

  try {
    const invoicingResult = await (0, _requestPromise.default)(options);

    const {
      RtnCode,
      RtnMsg
    } = _querystring.default.parse(invoicingResult);

    if (RtnCode !== '1') {
      reject(new Error(RtnMsg));
      return;
    }

    const {
      InvoiceDate,
      InvoiceNumber
    } = _querystring.default.parse(invoicingResult);

    resolve({
      InvoiceDate,
      InvoiceNumber
    });
  } catch (e) {
    debugInvoice(e);
    reject(e);
  }
});

exports.default = _default;