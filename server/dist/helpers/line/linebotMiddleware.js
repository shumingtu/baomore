"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = bot => ctx => {
  console.log('bot');

  if (bot.options.verify && ctx.request.rawBody && !bot.verify(ctx.request.rawBody, ctx.headers['x-line-signature'])) {
    ctx.status = 400;
    return;
  }

  console.log('receive');
  bot.parse(ctx.request.body);
  ctx.body = {};
};

exports.default = _default;