"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.reserve = reserve;
exports.confirm = confirm;

var _debug = _interopRequireDefault(require("debug"));

var _moment = _interopRequireDefault(require("moment"));

var _requestPromise = _interopRequireDefault(require("request-promise"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _db = require("../../db");

var _General = require("../../errors/General");

var _Pay = require("../../errors/Pay");

const debugPay = (0, _debug.default)('BAOMORE:LinePay');
const debugConfirm = (0, _debug.default)('BAOMORE:LinePayConfirm');
const LINE_PAY_OAUTH_HOST = process.env.LINE_PAY_OAUTH_HOST || 'https://api-pay.line.me/v2/payments/request';
const LINE_PAY_CLIENT_ID = process.env.LINE_PAY_CLIENT_ID || '1649578350';
const LINE_PAY_CLIENT_SECRET = process.env.LINE_PAY_CLIENT_SECRET || '7de5dd24b59d1b1395e33ae5a19068d3';
const LINE_PAY_CONFIRM_URL = process.env.LINE_PAY_CONFIRM_URL || 'https://api-pay.line.me/v2/payments';
const HOST = process.env.HOST || 'http://localhost:8108';

async function reserve(ctx, next) {
  const confirmURL = `${HOST}/payment/`;
  const {
    token
  } = ctx.request.query;

  if (!token) {
    debugPay('Invalid Parameter');
    throw new _General.InvalidParameter(); // Invalid Parameter
  }

  if (!ctx.req.member) {
    debugPay('Permission Denied');
    throw new _General.PermissionError();
  }

  const onlineOrder = await _db.db.models.OnlineOrder.findOne({
    where: {
      id: ctx.req.member.orderId,
      OwnerId: ctx.req.member.id
    }
  });

  if (!onlineOrder) {
    throw new _Pay.OnlineOrderNotFoundError();
  }

  console.log('confirmURL', confirmURL);

  try {
    const postData = {
      productName: '客製化包膜',
      productImageUrl: onlineOrder.picture,
      amount: onlineOrder.price,
      currency: 'TWD',
      confirmUrl: `${confirmURL}success?d=${onlineOrder.id}`,
      orderId: `${onlineOrder.id}`
    };
    const options = {
      url: LINE_PAY_OAUTH_HOST,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-LINE-ChannelId': LINE_PAY_CLIENT_ID,
        'X-LINE-ChannelSecret': LINE_PAY_CLIENT_SECRET
      },
      json: true,
      body: postData
    };
    const result = await (0, _requestPromise.default)(options);

    if (!result || result.returnCode !== '0000') {
      debugPay(result);
      ctx.redirect(`${confirmURL}failed?errorCode=${result.returnCode || ''}`);
      return;
    }

    ctx.redirect(result.info.paymentUrl.web);
  } catch (e) {
    debugPay(e);
    ctx.redirect(`${confirmURL}failed?errorCode=999`);
  }
}

async function confirm(transactionId, amount) {
  return new Promise(async (resolve, reject) => {
    const postData = {
      amount,
      currency: 'TWD'
    };
    const options = {
      url: `${LINE_PAY_CONFIRM_URL}/${transactionId}/confirm`,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-LINE-ChannelId': LINE_PAY_CLIENT_ID,
        'X-LINE-ChannelSecret': LINE_PAY_CLIENT_SECRET
      },
      json: true,
      body: postData
    };
    const result = await (0, _requestPromise.default)(options);

    if (!result || result.returnCode !== '0000') {
      debugConfirm(result);
      reject();
    }

    resolve();
  });
}