"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sendWarrantyInfoToEmployee = exports.sendTranferInfoToEmployee = exports.setEmployeeRichMenu = exports.sendBookedInfoToEmployee = exports.sendBookedInfoToDirector = exports.sendAssignInfoToEmployee = exports.default = exports.sendLineMessage = exports.sendLineMessageToClient = void 0;

var _linebot = _interopRequireDefault(require("linebot"));

var _debug = _interopRequireDefault(require("debug"));

var _queryString = _interopRequireDefault(require("query-string"));

var _requestPromise = _interopRequireDefault(require("request-promise"));

var _moment = _interopRequireDefault(require("moment"));

var _db = require("../../db");

var _onlineQRCodeGenerator = _interopRequireDefault(require("../onlineQRCodeGenerator.js"));

var _LineBot = require("../../errors/LineBot");

var _Member = require("../../errors/Member");

var _invoice = _interopRequireDefault(require("../invoice"));

/* eslint-disable max-len */
const debugLineBot = (0, _debug.default)('BAOMORE:LINEBOT');
const LINEBOT_CHANNEL_ID = process.env.LINEBOT_CHANNEL_ID || '1599854403';
const LINEBOT_CHANNEL_SECRET = process.env.LINEBOT_CHANNEL_SECRET || 'f7370139712c5d5752227678d3475e39';
const LINEBOT_CHANNEL_ACCESS_TOKEN = process.env.LINEBOT_CHANNEL_ACCESS_TOKEN || 'Bearer S2FdPByjri6rfv1xjxjx8c9wxNAAO+s4vPrlN8PKcAFcuDcAKrEDe/EOa3oUDT1xp58CwqYtaNQrSsVz+2qZRVK7LET7Hzrm89rmRY9Ym9Yp7/OtA7pB0vrTTp3kVcbZ9+Gydh1lXUxufezZLgHr3gdB04t89/1O/w1cDnyilFU=';
const LINEBOT_PUSH_MESSAGE_PATH = process.env.LINEBOT_PUSH_MESSAGE_PATH || 'https://api.line.me/v2/bot/message/push';
const LINEBOT_REPLY_MESSAGE_PATH = process.env.LINEBOT_PUSH_MESSAGE_PATH || 'https://api.line.me/v2/bot/message/reply';
const LINEBOT_FETCH_RICHMENUS = process.env.LINEBOT_FETCH_RICHMENUS || 'https://api.line.me/v2/bot/richmenu/list';
const LINEBOT_SET_RICHMENU_TO_MEMBER = process.env.LINEBOT_SET_RICHMENU_TO_MEMBER || 'https://api.line.me/v2/bot/user';
const ADMIN_HOST = process.env.ADMIN_HOST || 'http://192.168.1.56:8109';
const LINEBOT_CLIENT_ACCESS_TOKEN = process.env.LINEBOT_CLIENT_ACCESS_TOKEN || 'Bearer OD1iy06RE6m9Gd4t9j9sGGxk7SSPidV6matoPUAPSeIgUFwtI4qcFuTIFGC1rSXDNkC3RKGc6lCIdRH8zTklYEeXzTp0t3oKM0xYnBv06uqtCROE5t2ygKeYzKynTKrKOkxYXKUAmtcWx/n4VeZ1JQdB04t89/1O/w1cDnyilFU=';
const bot = (0, _linebot.default)({
  channelId: LINEBOT_CHANNEL_ID,
  channelSecret: LINEBOT_CHANNEL_SECRET,
  channelAccessToken: LINEBOT_CHANNEL_ACCESS_TOKEN
});
const ROLE_EMPOLYEE_NAME = '包膜師';
const ROLE_EMPOLYEE_MANAGER_NAME = '主任包膜師';

const sendLineMessageToClient = (text, lineId) => new Promise(async (resolve, reject) => {
  const postData = {
    messages: [{
      type: 'text',
      text
    }],
    to: lineId
  };
  const options = {
    url: LINEBOT_PUSH_MESSAGE_PATH,
    method: 'POST',
    headers: {
      Authorization: LINEBOT_CLIENT_ACCESS_TOKEN
    },
    json: true,
    body: postData
  };

  try {
    await (0, _requestPromise.default)(options);
    resolve();
  } catch (e) {
    debugLineBot(e);
    reject();
  }
});

exports.sendLineMessageToClient = sendLineMessageToClient;

const sendLineMessage = (replyToken, text, lineId) => new Promise(async (resolve, reject) => {
  const postData = {
    messages: [{
      type: 'text',
      text
    }]
  };

  if (lineId) {
    postData.to = lineId;
  } else {
    postData.replyToken = replyToken;
  }

  const options = {
    url: lineId ? LINEBOT_PUSH_MESSAGE_PATH : LINEBOT_REPLY_MESSAGE_PATH,
    method: 'POST',
    headers: {
      Authorization: LINEBOT_CHANNEL_ACCESS_TOKEN
    },
    json: true,
    body: postData
  };

  try {
    await (0, _requestPromise.default)(options);
    resolve();
  } catch (e) {
    debugLineBot(e);
    reject();
  }
});

exports.sendLineMessage = sendLineMessage;
bot.on('postback', async event => {
  const parseData = _queryString.default.parse(event.postback.data);

  debugLineBot(parseData);

  switch (parseData.command) {
    case 'warrantyConfirm':
      {
        const {
          recordId,
          employeeId
        } = parseData;

        if (!recordId || !employeeId) {
          await sendLineMessage(event.replyToken, '失敗！請聯絡系統管理員');
          return;
        }

        const targetWarranty = await _db.db.models.MemberWarranty.findByPk(parseInt(recordId, 10), {
          include: [{
            model: _db.db.models.Member
          }]
        });
        const employee = await _db.db.models.Member.findByPk(parseInt(employeeId, 10));

        if (!targetWarranty || !employee) {
          await sendLineMessage(event.replyToken, '失敗！請聯絡系統管理員');
          return;
        }

        if (targetWarranty.isApproved) {
          await sendLineMessage(event.replyToken, '此保卡已被認證！');
          return;
        }

        try {
          await targetWarranty.update({
            isApproved: true,
            ApproverId: employeeId,
            approverSN: employee.serialNumber
          });
          const HOST = process.env.HOST || 'http://localhost:8108';
          const serivceURL = `${HOST}/member/service`;
          await sendLineMessage(event.replyToken, '認證成功');
          await sendLineMessageToClient(`【Artmo 產品售後服務卡】\n消費時間：${(0, _moment.default)(targetWarranty.createdAt).format('YYYY-MM-DD HH:mm:ss')}\n顧客名稱：${targetWarranty.Member.name}\n服務項目：${targetWarranty.service}\n手機型號：${targetWarranty.phoneModel}\nIMEI後五碼：${targetWarranty.IME}\n統一發票：${targetWarranty.VAT}\n消費門市：${targetWarranty.store}\n如有售後服務需求,請至會員中心>售後服務,或至售後服務傳送門：\n${serivceURL}\n查看您的售後服務卡,並示出證明`, targetWarranty.Member.lineId);
        } catch (e) {
          await sendLineMessage(event.replyToken, '認證失敗！請聯絡系統管理員');
        }

        break;
      }

    case 'warrantyCancel':
      {
        const {
          recordId
        } = parseData;

        if (!recordId) {
          await sendLineMessage(event.replyToken, '失敗！請聯絡系統管理員');
          return;
        }

        const targetWarranty = await _db.db.models.MemberWarranty.findByPk(parseInt(recordId, 10), {
          include: [{
            model: _db.db.models.Member
          }]
        });

        if (!targetWarranty) {
          await sendLineMessage(event.replyToken, '失敗！請聯絡系統管理員');
          return;
        }

        if (targetWarranty.isApproved) {
          await sendLineMessage(event.replyToken, '此保卡已被認證，無法取消！');
          return;
        }

        try {
          await targetWarranty.destroy();
          await sendLineMessage(event.replyToken, '已成功刪除保卡！');
          await sendLineMessageToClient('售後服務卡未完成認證,請洽客服人員', targetWarranty.Member.lineId);
        } catch (e) {
          await sendLineMessage(event.replyToken, '失敗！請聯絡系統管理員');
        }

        break;
      }

    case 'transfer':
      {
        // 被調貨確認的動作
        const {
          transferToId,
          transferFromId,
          pnTableId,
          quantity,
          orderSN,
          qrcode,
          recordId,
          oriOrderId
        } = parseData;

        if (!transferFromId || !transferToId || !pnTableId || !quantity || !orderSN || !qrcode || !recordId || !oriOrderId) {
          await sendLineMessage(event.replyToken, '調貨失敗！請再試一次\nmessage:缺少欄位');
          return;
        }

        const transaction = await _db.db.transaction();

        try {
          const targetQRCode = await _db.db.models.OrderQRCode.findOne({
            where: {
              qrcode
            },
            include: [{
              model: _db.db.models.Order
            }],
            transaction
          });

          if (!targetQRCode) {
            await sendLineMessage(event.replyToken, 'QRCODE不存在！');
            await transaction.commit();
            return;
          }

          const transferedBehaviorRecord = await _db.db.models.BehaviorRecord.findOne({
            where: {
              TransferRecordId: recordId,
              qrcode
            },
            transaction
          });

          if (transferedBehaviorRecord) {
            await sendLineMessage(event.replyToken, `訂單已確認\n調貨訂單編號：${targetQRCode.Order.orderSN}`);
            await transaction.commit();
            return;
          }

          const saleBehavior = await _db.db.models.Behavior.findOne({
            where: {
              name: '銷貨'
            }
          });

          if (!saleBehavior) {
            await sendLineMessage(event.replyToken, '系統錯誤！');
            await transaction.commit();
            return;
          }

          const saleBehaviorRecord = await _db.db.models.BehaviorRecord.findOne({
            where: {
              BehaviorId: saleBehavior.id,
              qrcode
            },
            transaction
          });

          if (saleBehaviorRecord) {
            await sendLineMessage(event.replyToken, '此調貨單已取消');
            await transaction.commit();
            return;
          }

          try {
            const pnTable = await _db.db.models.PNTable.scope({
              method: ['search', {
                id: pnTableId
              }]
            }).findOne();
            const transferFromMember = await _db.db.models.Member.findOne({
              where: {
                id: parseInt(transferFromId, 10)
              }
            });
            const transferToMember = await _db.db.models.Member.findOne({
              where: {
                id: parseInt(transferToId, 10)
              }
            });
            const orderGroupShipment = await _db.db.models.OrderGroupShipment.create({
              SN: (0, _moment.default)().format('YYYYMMDD'),
              status: 'SHIPMENTED',
              startDate: new Date(),
              endDate: new Date(),
              PNTableId: pnTable.id
            }, {
              transaction
            });
            const newOrder = await _db.db.models.Order.create({
              status: 'DISPATCHED',
              quantity,
              picture: pnTable.picture,
              orderSource: 'TRANSFER',
              EmpolyedMemberId: parseInt(transferToId, 10),
              PNTableId: pnTable.id,
              OrderGroupShipmentId: orderGroupShipment.id,
              orderSN: `${(0, _moment.default)().format('YYmmssMMDD')}7`
            }, {
              transaction
            });
            await targetQRCode.update({
              OrderId: newOrder.id
            }, {
              transaction
            });
            const oriOrder = await _db.db.models.Order.findOne({
              where: {
                id: parseInt(oriOrderId, 10)
              }
            });
            await oriOrder.update({
              quantity: oriOrder.quantity - 1
            }, {
              transaction
            });
            await _db.db.models.MemberStock.reduceStock(transferFromMember.id, pnTable.id, quantity, transaction); // record

            const behavior = await _db.db.models.Behavior.findOne({
              where: {
                name: '調貨'
              }
            });
            await _db.db.models.BehaviorRecord.create({
              remark: `${transferToMember.name}包膜師接受${transferFromMember.name}包膜師的調貨要求`,
              BehaviorId: behavior.id,
              MemberId: transferToMember.id,
              PNTableId: pnTable.id,
              TransferRecordId: recordId,
              orderSN,
              newOrderSN: newOrder.orderSN,
              quantity,
              qrcode
            }, {
              transaction
            });
            await _db.db.models.BehaviorRecord.update({
              isTransfering: false
            }, {
              where: {
                id: recordId
              },
              transaction
            });
            await transaction.commit();
            await sendLineMessage(event.replyToken, `調貨確認完成\n調貨訂單編號：${newOrder.orderSN}`);
            await sendLineMessage(event.replyToken, `【包膜師調貨通知：已接受調貨】\n收貨者：${transferToMember.name}\n品名：${pnTable.name}\n料號：${pnTable.code}\n商品序號：${qrcode}`, transferFromMember.lineId);
          } catch (e) {
            await transaction.rollback();
            await sendLineMessage(event.replyToken, `調貨失敗！請再試一次\nmessage:${e}`);
            debugLineBot(e);
          }
        } catch (e) {
          debugLineBot(e);
          await transaction.rollback();
          await sendLineMessage(event.replyToken, `調貨失敗！請再試一次\nmessage:${e}`);
        }

        break;
      }

    case 'bookConfirm':
      {
        const {
          onlineOrderId
        } = parseData;
        const onlineOrder = await _db.db.models.OnlineOrder.scope({
          method: ['search', {
            orderId: onlineOrderId
          }]
        }).findOne({
          include: [{
            model: _db.db.models.PNTable
          }, {
            model: _db.db.models.Order
          }]
        });

        if (!onlineOrder) {
          await sendLineMessage(event.replyToken, '找不到此線上訂單！');
          return;
        }

        if (onlineOrder.payedStatus === 'PAIDANDCONFIRMED') {
          const titleMessage = onlineOrder.isAssigned ? '訂單已轉派' : '訂單已確認';
          await sendLineMessage(event.replyToken, `${titleMessage}\n訂單編號：${onlineOrder.Order.orderSN}`);
          return;
        }

        const transaction = await _db.db.transaction();

        try {
          const orderGroupShipment = await _db.db.models.OrderGroupShipment.create({
            SN: (0, _moment.default)().format('YYYYMMDD'),
            status: 'UNSHIPMENTED',
            startDate: onlineOrder.createdAt,
            endDate: new Date(),
            PNTableId: onlineOrder.PNTableId
          }, {
            transaction
          });
          const order = await _db.db.models.Order.create({
            orderSN: `${(0, _moment.default)().format('YYmmssMMDD')}8`,
            quantity: 1,
            picture: onlineOrder.picture,
            status: 'UNDISPATCHED',
            orderSource: 'ONLINEORDER',
            StoreId: onlineOrder.StoreId,
            PNTableId: onlineOrder.PNTableId,
            ProduceType: 'MATERIAL',
            EmpolyedMemberId: onlineOrder.BookedMemberId,
            OrderGroupShipmentId: orderGroupShipment.id
          }, {
            transaction
          });
          await onlineOrder.update({
            payedStatus: 'PAIDANDCONFIRMED',
            OrderId: order.id
          }, {
            transaction
          });
          const qrcodePrefix = `${(0, _moment.default)().format('YYYYMMDD')}${onlineOrder.PNTable.code}`;
          const qrcode = await (0, _onlineQRCodeGenerator.default)(qrcodePrefix);
          await _db.db.models.OrderQRCode.create({
            OrderId: order.id,
            qrcode
          }, {
            transaction
          });
          const result = await (0, _invoice.default)(onlineOrder);
          await order.update({
            invoiceNumber: result.InvoiceNumber,
            invoiceDate: result.InvoiceDate
          }, {
            transaction
          });
          await sendLineMessage(event.replyToken, `確認完成\n訂單編號：${order.orderSN}`);
          await transaction.commit();
        } catch (e) {
          await transaction.rollback();
          await sendLineMessage(event.replyToken, `確認失敗！或發票開立失敗${e}`);
          debugLineBot(e);
        }

        break;
      }

    default:
  }
});
var _default = bot; // HTTP Message Functions

exports.default = _default;

const sendAssignInfoToEmployee = (lineId, text) => new Promise(async (resolve, reject) => {
  if (!lineId) return reject();
  const postData = {
    to: lineId,
    messages: [{
      type: 'text',
      altText: '被轉派通知',
      label: 'hi',
      text
    }]
  };
  const options = {
    url: LINEBOT_PUSH_MESSAGE_PATH,
    method: 'POST',
    headers: {
      Authorization: LINEBOT_CHANNEL_ACCESS_TOKEN
    },
    json: true,
    body: postData
  };

  try {
    const result = await (0, _requestPromise.default)(options);
    return resolve(result);
  } catch (e) {
    debugLineBot(e);
    return reject();
  }
});

exports.sendAssignInfoToEmployee = sendAssignInfoToEmployee;

const sendBookedInfoToDirector = (lineId, text, orderId, directorId) => new Promise(async (resolve, reject) => {
  if (!lineId) return reject();
  const postData = {
    to: lineId,
    messages: [{
      type: 'template',
      altText: '轉派通知',
      template: {
        type: 'buttons',
        text,
        actions: [{
          type: 'uri',
          label: '修改訂單資訊',
          uri: `${ADMIN_HOST}/linebot/assignOrder/${orderId}/${directorId}`
        }]
      }
    }]
  };
  const options = {
    url: LINEBOT_PUSH_MESSAGE_PATH,
    method: 'POST',
    headers: {
      Authorization: LINEBOT_CHANNEL_ACCESS_TOKEN
    },
    json: true,
    body: postData
  };

  try {
    const result = await (0, _requestPromise.default)(options);
    return resolve(result);
  } catch (e) {
    debugLineBot(e);
    return reject();
  }
});

exports.sendBookedInfoToDirector = sendBookedInfoToDirector;

const sendBookedInfoToEmployee = (lineId, text, postbackData) => new Promise(async (resolve, reject) => {
  if (!lineId) return reject();
  const postData = {
    to: lineId,
    messages: [{
      type: 'template',
      altText: '新預約確認通知',
      template: {
        type: 'buttons',
        text,
        actions: [{
          type: 'postback',
          label: '確認此預約',
          data: postbackData
        }]
      }
    }]
  };
  const options = {
    url: LINEBOT_PUSH_MESSAGE_PATH,
    method: 'POST',
    headers: {
      Authorization: LINEBOT_CHANNEL_ACCESS_TOKEN
    },
    json: true,
    body: postData
  };

  try {
    const result = await (0, _requestPromise.default)(options);
    return resolve(result);
  } catch (e) {
    debugLineBot(e);
    return reject();
  }
});

exports.sendBookedInfoToEmployee = sendBookedInfoToEmployee;

const setEmployeeRichMenu = member => new Promise(async (resolve, reject) => {
  if (!member || !member.lineId) return reject(new _Member.MemberNotConnectLINEError()); // fetch all richmenu

  const fetchRichMenusOptions = {
    url: LINEBOT_FETCH_RICHMENUS,
    method: 'GET',
    headers: {
      Authorization: LINEBOT_CHANNEL_ACCESS_TOKEN
    }
  };

  try {
    const roles = await member.getRoles();
    debugLineBot(roles);
    const fetchRichmenuResult = await (0, _requestPromise.default)(fetchRichMenusOptions);
    if (!fetchRichmenuResult) throw new _LineBot.RichmenuFetchError();
    const {
      richmenus
    } = JSON.parse(fetchRichmenuResult);
    if (!richmenus || !richmenus.length) throw new _LineBot.RichmenuNotFoundError();
    const empolyee = roles.find(role => role.name === ROLE_EMPOLYEE_NAME);
    const manager = roles.find(role => role.name === ROLE_EMPOLYEE_MANAGER_NAME);
    let selectedRichmenu = null;
    if (empolyee) selectedRichmenu = richmenus.find(richmenu => richmenu.name === empolyee.name);
    if (manager) selectedRichmenu = richmenus.find(richmenu => richmenu.name === manager.name);
    if (!selectedRichmenu) throw new _LineBot.RichmenuNotFoundError();
    const setRichMenuOptions = {
      url: `${LINEBOT_SET_RICHMENU_TO_MEMBER}/${member.lineId}/richmenu/${selectedRichmenu.richMenuId}`,
      method: 'POST',
      headers: {
        Authorization: LINEBOT_CHANNEL_ACCESS_TOKEN
      }
    };
    await (0, _requestPromise.default)(setRichMenuOptions);
    return resolve();
  } catch (e) {
    debugLineBot(e);
    return reject(e.code ? e : new _LineBot.SetRichmenuError());
  }
});

exports.setEmployeeRichMenu = setEmployeeRichMenu;

const sendTranferInfoToEmployee = (member, text, postbackData) => new Promise(async (resolve, reject) => {
  if (!member || !member.lineId) return reject();
  const postData = {
    to: member.lineId,
    messages: [{
      type: 'template',
      altText: '調貨通知',
      template: {
        type: 'buttons',
        text,
        actions: [{
          type: 'postback',
          label: '接受調貨',
          data: postbackData
        }]
      }
    }]
  };
  const options = {
    url: LINEBOT_PUSH_MESSAGE_PATH,
    method: 'POST',
    headers: {
      Authorization: LINEBOT_CHANNEL_ACCESS_TOKEN
    },
    json: true,
    body: postData
  };

  try {
    const result = await (0, _requestPromise.default)(options);
    return resolve(result);
  } catch (e) {
    debugLineBot(e);
    return reject(e);
  }
});

exports.sendTranferInfoToEmployee = sendTranferInfoToEmployee;

const sendWarrantyInfoToEmployee = (member, text, confirmData, cancelData) => new Promise(async (resolve, reject) => {
  if (!member || !member.lineId) return reject();
  const postData = {
    to: member.lineId,
    messages: [{
      type: 'template',
      altText: '售後服務卡通知',
      template: {
        type: 'buttons',
        text,
        actions: [{
          type: 'postback',
          label: '認證',
          data: confirmData
        }, {
          type: 'postback',
          label: '取消',
          data: cancelData
        }]
      }
    }]
  };
  const options = {
    url: LINEBOT_PUSH_MESSAGE_PATH,
    method: 'POST',
    headers: {
      Authorization: LINEBOT_CHANNEL_ACCESS_TOKEN
    },
    json: true,
    body: postData
  };

  try {
    const result = await (0, _requestPromise.default)(options);
    return resolve(result);
  } catch (e) {
    debugLineBot(e);
    return reject(e);
  }
});

exports.sendWarrantyInfoToEmployee = sendWarrantyInfoToEmployee;