"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.employeeLogin = employeeLogin;
exports.adminLogin = adminLogin;
exports.memberLogin = memberLogin;
exports.memberLoginFromCustomizePage = memberLoginFromCustomizePage;

var _debug = _interopRequireDefault(require("debug"));

var _requestPromise = _interopRequireDefault(require("request-promise"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _db = require("../../db");

var _linebot = require("./linebot");

var _Member = require("../../errors/Member");

const debugAuth = (0, _debug.default)('BAOMORE:LineAuth');
const LINE_OAUTH_HOST = process.env.LINE_OAUTH_HOST || 'https://api.line.me/oauth2/v2.1/token';
const LINE_CLIENT_ID = process.env.LINE_CLIENT_ID || '1599855723';
const LINE_CLIENT_SECRET = process.env.LINE_CLIENT_SECRET || '6d97c25d2001669f87d7dd55ab4a49e1';
const API_HOST = process.env.API_HOST || 'http://localhost:2113';
const HOST = process.env.HOST || 'http://localhost:8108';
const ADMIN_HOST = process.env.ADMIN_HOST || 'http://localhost:8109';

async function employeeLogin(member, code) {
  return new Promise(async (resolve, reject) => {
    try {
      // get line id_token
      const options = {
        uri: LINE_OAUTH_HOST,
        method: 'POST',
        form: {
          code,
          redirect_uri: `${ADMIN_HOST}/linebot/register`,
          client_id: LINE_CLIENT_ID,
          client_secret: LINE_CLIENT_SECRET,
          grant_type: 'authorization_code'
        }
      };

      try {
        const stringData = await (0, _requestPromise.default)(options);
        const data = JSON.parse(stringData); // get user info by this token

        const decoded = _jsonwebtoken.default.decode(data.id_token); // update Member lineId binding & picture & refreshToken


        const {
          sub,
          picture
        } = decoded;
        member.lineId = sub;
        member.avatar = picture;
        member.refreshToken = data.refreshToken;
        await member.save();
      } catch (e) {
        debugAuth(e);
        throw new _Member.MemberLINELoginError();
      }

      const accessToken = await member.accessToken(); // check role to set rich menu

      await (0, _linebot.setEmployeeRichMenu)(member);
      resolve({
        member,
        accessToken
      });
    } catch (e) {
      debugAuth(e);
      reject(e);
    }
  });
}

async function adminLogin(ctx) {
  const redirectUrl = `${ADMIN_HOST}/oAuth?token=`;

  try {
    // line code
    const {
      code
    } = ctx.request.query; // get line id_token

    const options = {
      uri: LINE_OAUTH_HOST,
      method: 'POST',
      form: {
        code,
        redirect_uri: `${API_HOST}/auth/adminLineLogin`,
        client_id: LINE_CLIENT_ID,
        client_secret: LINE_CLIENT_SECRET,
        grant_type: 'authorization_code'
      }
    };
    const stringData = await (0, _requestPromise.default)(options);
    const data = JSON.parse(stringData); // get user info by this token

    const decoded = _jsonwebtoken.default.decode(data.id_token); // check user is exist or not


    const result = await _db.db.models.Member.memberoAuthLogin(decoded.sub); // exist

    if (result.member) {
      return ctx.redirect(`${redirectUrl}${result.accessToken}`);
    } // create Member


    const {
      sub,
      name,
      picture
    } = decoded;
    const memberData = {
      lineId: sub,
      name: name || 'artmo 會員',
      avatar: picture,
      refreshToken: data.refreshToken
    };
    const member = await _db.db.models.Member.createoAuthMember(memberData); // add default roles

    const memberRole = await _db.db.models.Role.findOne({
      where: {
        name: '一般登入用戶'
      }
    });
    await member.addRole(memberRole);
    const accessToken = await member.accessToken();
    return ctx.redirect(`${redirectUrl}${accessToken}`);
  } catch (e) {
    debugAuth(e);
    return ctx.redirect(`${redirectUrl}&error=授權失敗`);
  }
}

async function memberLogin(ctx) {
  const redirectUrl = `${HOST}/oAuth?token=`;

  try {
    // line code
    const {
      code
    } = ctx.request.query; // get line id_token

    const options = {
      uri: LINE_OAUTH_HOST,
      method: 'POST',
      form: {
        code,
        redirect_uri: `${API_HOST}/auth/lineLogin`,
        client_id: LINE_CLIENT_ID,
        client_secret: LINE_CLIENT_SECRET,
        grant_type: 'authorization_code'
      }
    };
    const stringData = await (0, _requestPromise.default)(options);
    const data = JSON.parse(stringData); // get user info by this token

    const decoded = _jsonwebtoken.default.decode(data.id_token); // check user is exist or not


    const result = await _db.db.models.Member.memberoAuthLogin(decoded.sub); // exist

    if (result.member) {
      return ctx.redirect(`${redirectUrl}${result.accessToken}&state=1`);
    } // create Member


    const {
      sub,
      name,
      picture
    } = decoded;
    const memberData = {
      lineId: sub,
      name: name || 'artmo 會員',
      avatar: picture,
      refreshToken: data.refreshToken
    };
    const member = await _db.db.models.Member.createoAuthMember(memberData); // add default roles

    const memberRole = await _db.db.models.Role.findOne({
      where: {
        name: '一般登入用戶'
      }
    });
    await member.addRole(memberRole);
    const accessToken = await member.accessToken();
    return ctx.redirect(`${redirectUrl}${accessToken}&state=0`);
  } catch (e) {
    debugAuth(e);
    return ctx.redirect(`${redirectUrl}&error=授權失敗`);
  }
}

async function memberLoginFromCustomizePage(ctx) {
  const redirectUrl = `${HOST}/customize/oAuth?token=`;

  try {
    // line code
    const {
      code
    } = ctx.request.query; // get line id_token

    const options = {
      uri: LINE_OAUTH_HOST,
      method: 'POST',
      form: {
        code,
        redirect_uri: `${API_HOST}/auth/customize/lineLogin`,
        client_id: LINE_CLIENT_ID,
        client_secret: LINE_CLIENT_SECRET,
        grant_type: 'authorization_code'
      }
    };
    const stringData = await (0, _requestPromise.default)(options);
    const data = JSON.parse(stringData); // get user info by this token

    const decoded = _jsonwebtoken.default.decode(data.id_token); // check user is exist or not


    const result = await _db.db.models.Member.memberoAuthLogin(decoded.sub); // exist

    if (result.member) {
      return ctx.redirect(`${redirectUrl}${result.accessToken}`);
    } // create Member


    const {
      sub,
      name,
      picture
    } = decoded;
    const memberData = {
      lineId: sub,
      name: name || 'artmo 會員',
      avatar: picture,
      refreshToken: data.refreshToken
    };
    const member = await _db.db.models.Member.createoAuthMember(memberData); // add default roles

    const memberRole = await _db.db.models.Role.findOne({
      where: {
        name: '一般登入用戶'
      }
    });
    await member.addRole(memberRole);
    const accessToken = await member.accessToken();
    return ctx.redirect(`${redirectUrl}${accessToken}`);
  } catch (e) {
    debugAuth(e);
    return ctx.redirect(`${redirectUrl}&error=授權失敗`);
  }
}