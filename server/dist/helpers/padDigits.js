"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = padDigits;

function padDigits(number, digits) {
  return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number;
}