"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getCsvJsonArray;

var _csvtojson = _interopRequireDefault(require("csvtojson"));

var _iconvLite = _interopRequireDefault(require("iconv-lite"));

var _fs = _interopRequireDefault(require("fs"));

function getCsvJsonArray(pathname) {
  return new Promise(async (resolve, reject) => {
    try {
      const rawCsvString = _fs.default.readFileSync(pathname).toString(); // 保卡正常 其他不正常


      if (rawCsvString.indexOf('�') !== -1) {
        const csvFile = _fs.default.readFileSync(pathname);

        const csvString = _iconvLite.default.decode(csvFile, 'big5');

        const convertedJsonArray = await (0, _csvtojson.default)().fromString(csvString);
        resolve(convertedJsonArray);
        return;
      }

      const convertedJsonArray = await (0, _csvtojson.default)().fromString(rawCsvString);
      resolve(convertedJsonArray);
    } catch (e) {
      reject();
    }
  });
}