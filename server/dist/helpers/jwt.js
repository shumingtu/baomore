"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.jwtSignRefreshToken = jwtSignRefreshToken;
exports.jwtSign = jwtSign;
exports.verifyRefreshToken = verifyRefreshToken;
exports.parseJWT = parseJWT;
exports.TOKEN_EXPIRED_SECONDS = exports.JWT_REFRESH_TOKEN = exports.JWT_TOKEN = void 0;

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _bluebird = _interopRequireDefault(require("bluebird"));

var _debug = _interopRequireDefault(require("debug"));

var _Member = require("../errors/Member");

const NODE_ENV = process.env.NODE_ENV || 'development';
const debugJWT = (0, _debug.default)('PHOTO_GENERATOR:JWT');
const JWT_TOKEN = process.env.JWT_TOKEN || '^Baomore_Rytass:Baomore-2018$';
exports.JWT_TOKEN = JWT_TOKEN;
const JWT_REFRESH_TOKEN = process.env.JWT_REFRESH_TOKEN || `${JWT_TOKEN}:RefreshToken2OI8`;
exports.JWT_REFRESH_TOKEN = JWT_REFRESH_TOKEN;
const TOKEN_EXPIRED_SECONDS = parseInt(process.env.TOKEN_EXPIRED_SECONDS || `${60 * 15}`, 10); // 15 minutes

exports.TOKEN_EXPIRED_SECONDS = TOKEN_EXPIRED_SECONDS;

function jwtSignRefreshToken(payload) {
  return new _bluebird.default(resolve => {
    _jsonwebtoken.default.sign(payload, JWT_REFRESH_TOKEN, (error, token) => {
      if (error) {
        debugJWT('JWT Sign Refresh Token Error', {
          error,
          payload
        });
        resolve();
      } else {
        debugJWT('Signed JWT Refresh Token');
        resolve(token);
      }
    });
  });
}

function jwtSign(payload) {
  return new _bluebird.default(resolve => {
    _jsonwebtoken.default.sign(payload, JWT_TOKEN, {
      expiresIn: NODE_ENV === 'production' ? TOKEN_EXPIRED_SECONDS : 9999999999
    }, (error, token) => {
      if (error) {
        debugJWT('JWT Sign Error', {
          error,
          payload
        });
        resolve();
      } else {
        debugJWT('Signed JWT token', {
          token
        });
        resolve(token);
      }
    });
  });
}

function verifyRefreshToken(token) {
  if (!token) throw new _Member.InvalidRefreshToken();
  return new _bluebird.default(resolve => {
    _jsonwebtoken.default.verify(token, JWT_REFRESH_TOKEN, (err, member) => {
      if (err) {
        debugJWT('JWT Parse Error', err);
        throw new _Member.InvalidRefreshToken();
      }

      resolve(member);
    });
  });
}

function parseJWT(token) {
  if (!token) return null;
  return new _bluebird.default(resolve => {
    _jsonwebtoken.default.verify(token.replace(/^Bearer\s/, ''), JWT_TOKEN, (err, member) => {
      if (err) {
        debugJWT('JWT Parse Error', err);
        resolve(null);
      } else {
        resolve(member);
      }
    });
  });
}