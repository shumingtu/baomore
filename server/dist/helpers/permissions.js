"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.checkShouldHavePermission = checkShouldHavePermission;
exports.getMemberPermission = getMemberPermission;
exports.getMemberPermissionFromId = getMemberPermissionFromId;

var _flatten = _interopRequireDefault(require("lodash/flatten"));

var _uniq = _interopRequireDefault(require("lodash/uniq"));

var _db = require("../db.js");

/* eslint no-bitwise: 0 */
function checkShouldHavePermission(permissionCode, actionCodes = [], orMode = false) {
  let shouldHaveActionCodes = actionCodes;

  if (!Array.isArray(actionCodes)) {
    shouldHaveActionCodes = [actionCodes];
  }

  if (orMode) {
    return shouldHaveActionCodes.some(code => (permissionCode & code) === code);
  }

  const intersectionPermission = shouldHaveActionCodes.reduce((last, next) => last | next, 0);
  return (permissionCode & intersectionPermission) === intersectionPermission;
}

async function getMemberPermission(memberId) {
  const {
    Member,
    Role,
    Action
  } = _db.db.models;
  const member = await Member.findOne({
    where: {
      id: memberId
    },
    include: [{
      model: Role,
      attributes: ['id'],
      include: [{
        model: Action,
        attributes: ['code']
      }]
    }]
  });
  return (0, _uniq.default)((0, _flatten.default)(member.Roles.map(role => role.Actions.map(action => action.code)))).reduce((last, next) => last | next, 0);
}

async function getMemberPermissionFromId(memberId) {
  if (!memberId) return 0;
  const member = await _db.db.models.Member.findOne({
    where: {
      id: memberId
    }
  });
  if (!member) return 0;
  return member.getPermission();
}