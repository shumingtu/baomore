"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _debug = _interopRequireDefault(require("debug"));

var _db = require("../db.js");

var _jwt = require("./jwt");

const debugAuth = (0, _debug.default)('Baomore:Authentication');

var _default = async (ctx, next) => {
  const {
    token
  } = ctx.request.query;

  if (!token) {
    ctx.req.member = null;
    return next();
  }

  return new Promise(resolve => {
    _jsonwebtoken.default.verify(token.replace(/^Bearer\s/, ''), _jwt.JWT_TOKEN, (err, payload) => {
      if (err) {
        debugAuth(err);
        ctx.req.member = null;
        next();
        return;
      }

      ctx.req.member = payload;

      if (ctx.req.member) {
        ctx.req.member.getInstance = function getInstance() {
          return _db.db.models.Member.findOne({
            where: {
              id: payload.id
            }
          });
        };
      }

      resolve(next());
    });
  });
};

exports.default = _default;