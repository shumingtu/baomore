"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _crypto = _interopRequireDefault(require("crypto"));

var _lodash = _interopRequireDefault(require("lodash"));

var _transformURLEncoder = _interopRequireDefault(require("./transformURLEncoder.js"));

const getCheckMacValue = (payload, hashKey, hashIV) => {
  const keys = Object.keys(payload);

  const sortedKeys = _lodash.default.sortBy(keys, key => key);

  const uri = _lodash.default.map(sortedKeys, key => `${key}=${payload[key]}`).join('&');

  const raw = (0, _transformURLEncoder.default)(encodeURIComponent(`HashKey=${hashKey}&${uri}&HashIV=${hashIV}`)).toLowerCase();

  const checkMacValue = _crypto.default.createHash('md5').update(raw).digest('hex').toUpperCase();

  return checkMacValue;
};

var _default = getCheckMacValue;
exports.default = _default;