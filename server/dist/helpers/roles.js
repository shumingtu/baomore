"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getMyRoles = getMyRoles;
exports.isPermissionAllowed = isPermissionAllowed;

var _jwtDecode = _interopRequireDefault(require("jwt-decode"));

var _roleActions = require("../../shared/roleActions.js");

function getMyRoles(token) {
  if (token) {
    const {
      permissions
    } = (0, _jwtDecode.default)(token);
    const actionKeys = Object.keys(_roleActions.actions).map(actionKey => ({
      key: actionKey,
      code: _roleActions.actions[actionKey].code
    })).filter(c => permissions & c.code).map(action => action.key);
    return actionKeys;
  }

  return null;
}

function isPermissionAllowed(targetRoles = [], token) {
  if (!targetRoles || !Array.isArray(targetRoles)) {
    return false;
  }

  const myRolePermission = getMyRoles(token);

  if (myRolePermission) {
    return myRolePermission.some(r => ~targetRoles.indexOf(r));
  }

  return false;
}