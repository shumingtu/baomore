"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.Actions = exports.AUTH_METHOD_PASSWORD = exports.AUTH_METHOD_TOKEN = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _debug = _interopRequireDefault(require("debug"));

var _jwt = require("./jwt");

const debugAuth = (0, _debug.default)('Baomore:Auth');
const AUTH_METHOD_TOKEN = 'AUTH_METHOD/TOKEN';
exports.AUTH_METHOD_TOKEN = AUTH_METHOD_TOKEN;
const AUTH_METHOD_PASSWORD = 'AUTH_METHOD/PASSWORD';
exports.AUTH_METHOD_PASSWORD = AUTH_METHOD_PASSWORD;
const Actions = {
  ADMIN_LOGIN: 1
};
exports.Actions = Actions;

class Auth {
  constructor(token, db) {
    (0, _defineProperty2.default)(this, "db", void 0);
    (0, _defineProperty2.default)(this, "token", void 0);
    (0, _defineProperty2.default)(this, "payload", void 0);
    (0, _defineProperty2.default)(this, "method", void 0);
    this.token = token;
    this.db = db;
  }

  getMember() {
    if (!this.token || !this.db) return null;
    return this.db.models.Administrator.findOne({
      where: {
        id: this.payload.id
      }
    });
  }

  setMethod(newMethod) {
    this.method = newMethod;
  }

  async parseToken() {
    if (this.token) {
      try {
        this.payload = await (0, _jwt.parseAccessToken)(this.token.replace(/^Bearer\s/, ''));
        this.setMethod(AUTH_METHOD_TOKEN);
      } catch (ex) {
        debugAuth('Invalid Access Token');
      }
    }
  }

}

exports.default = Auth;
(0, _defineProperty2.default)(Auth, "INVALID_METHOD_TYPE", new Error('Invalid AuthMethod Type'));