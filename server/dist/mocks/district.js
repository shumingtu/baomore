"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _debug = _interopRequireDefault(require("debug"));

var _taiwanZones = _interopRequireDefault(require("./taiwanZones.json"));

var _db = require("../db");

const debugCity = (0, _debug.default)('Baomore:CityMocks');

var _default = () => new Promise(async (resolve, reject) => {
  try {
    await Promise.all(_taiwanZones.default.map(item => new Promise(async (resolveCity, rejectCity) => {
      try {
        const city = await _db.db.models.City.create({
          name: item.city,
          latitude: item.lat,
          longitude: item.lng
        });
        resolveCity(Promise.all(item.zones.map(zone => new Promise(async (resolveZone, rejectZone) => {
          try {
            const dist = await _db.db.models.District.create(zone);
            resolveZone(dist.setCity(city));
          } catch (e) {
            rejectZone(e);
          }
        }))));
      } catch (e) {
        rejectCity(e);
      }
    })));
    resolve();
  } catch (e) {
    debugCity(e);
    reject();
  }
});

exports.default = _default;