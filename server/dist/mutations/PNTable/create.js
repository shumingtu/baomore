"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _Member = require("../../errors/Member");

var _PNTable = require("../../types/PNTable");

var _default = {
  type: new _graphql.GraphQLNonNull(_PNTable.PNTableType),
  args: {
    code: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    pnCategoryId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    bigSubCategoryId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    smallSubCategoryId: {
      type: _graphql.GraphQLInt
    },
    BrandModelId: {
      type: _graphql.GraphQLInt
    },
    VendorId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    picture: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    price: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    device: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    description: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    isOnSale: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    onlinePrice: {
      type: _graphql.GraphQLInt
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member.PermissionError(),
    actions: _roleActions.actions.PNTABLE_MANAGE
  })(async (_, args) => _db.db.models.PNTable.createPNTableItem(args))
};
exports.default = _default;