"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

var _Store = require("../../errors/Store");

var _Store2 = require("../../types/Store.js");

var _default = {
  type: new _graphql.GraphQLNonNull(_Store2.storeType),
  args: {
    areaId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    channelId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    districtId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    phone: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    address: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    manager: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    latitude: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLFloat)
    },
    longitude: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLFloat)
    },
    mainEmployeeId: {
      type: _graphql.GraphQLInt
    },
    backupEmployeeId: {
      type: _graphql.GraphQLInt
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.ADMIN_SYSTEMINFO_MANAGE
  })(async (_, args) => {
    const {
      areaId,
      channelId,
      name,
      phone,
      address,
      districtId,
      mainEmployeeId,
      manager,
      backupEmployeeId,
      latitude,
      longitude
    } = args;
    const createdStore = await _db.db.models.Store.create({
      AreaId: areaId,
      ChannelId: channelId,
      name,
      phone,
      address,
      manager,
      DistrictId: districtId,
      latitude,
      longitude
    });
    if (!createdStore) throw new _Store.StoreCreateFailed();
    const memberStoreBundle = [];

    if (mainEmployeeId) {
      memberStoreBundle.push({
        type: 'MAIN',
        MemberId: mainEmployeeId,
        StoreId: createdStore.id
      });
    }

    if (backupEmployeeId) {
      memberStoreBundle.push({
        type: 'BACKUP',
        MemberId: backupEmployeeId,
        StoreId: createdStore.id
      });
    }

    const memberStore = await _db.db.models.MemberStore.bulkCreate(memberStoreBundle);
    if (!memberStore) throw new _Store.MemberStoreCreateFailed();
    const storeArea = await createdStore.getArea();
    const storeChannel = await createdStore.getChannel();
    const storeMembers = await createdStore.getMembers();
    const storeDistrict = await createdStore.getDistrict();
    return { ...createdStore.dataValues,
      Area: storeArea || null,
      Channel: storeChannel || null,
      Members: storeMembers || null,
      District: storeDistrict || null
    };
  })
};
exports.default = _default;