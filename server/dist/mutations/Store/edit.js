"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.toggleStoreArchive = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

var _Store = require("../../errors/Store");

var _Store2 = require("../../types/Store.js");

const toggleStoreArchive = {
  type: _Store2.storeToggleArchiveResponseType,
  args: {
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.ADMIN_SYSTEMINFO_MANAGE
  })(async (_, args) => {
    const {
      id
    } = args;
    const targetStore = await _db.db.models.Store.findOne({
      where: {
        id
      }
    });
    if (!targetStore) throw new _Store.StoreNotFound();
    await targetStore.update({
      archive: !targetStore.archive
    });
    return {
      id,
      message: '成功',
      status: true
    };
  })
};
exports.toggleStoreArchive = toggleStoreArchive;
var _default = {
  type: new _graphql.GraphQLNonNull(_Store2.storeType),
  args: {
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    areaId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    channelId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    districtId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    phone: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    address: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    manager: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    latitude: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLFloat)
    },
    longitude: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLFloat)
    },
    mainEmployeeId: {
      type: _graphql.GraphQLInt
    },
    backupEmployeeId: {
      type: _graphql.GraphQLInt
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.ADMIN_SYSTEMINFO_MANAGE
  })(async (_, args) => {
    const {
      id,
      areaId,
      channelId,
      name,
      phone,
      address,
      districtId,
      mainEmployeeId,
      backupEmployeeId,
      manager,
      latitude,
      longitude
    } = args;
    const targetStore = await _db.db.models.Store.findOne({
      where: {
        id
      }
    });
    if (!targetStore) throw new _Store.StoreNotFound();
    await targetStore.update({
      AreaId: areaId,
      ChannelId: channelId,
      name,
      phone,
      address,
      DistrictId: districtId,
      manager,
      latitude,
      longitude
    });
    await _db.db.models.MemberStore.destroy({
      where: {
        StoreId: targetStore.id
      }
    });
    const memberStoreBundle = [];

    if (mainEmployeeId) {
      memberStoreBundle.push({
        type: 'MAIN',
        MemberId: mainEmployeeId,
        StoreId: targetStore.id
      });
    }

    if (backupEmployeeId) {
      memberStoreBundle.push({
        type: 'BACKUP',
        MemberId: backupEmployeeId,
        StoreId: targetStore.id
      });
    }

    const memberStore = await _db.db.models.MemberStore.bulkCreate(memberStoreBundle);
    if (!memberStore) throw new _Store.MemberStoreCreateFailed();
    const storeArea = await targetStore.getArea();
    const storeChannel = await targetStore.getChannel();
    const storeMembers = await targetStore.getMembers();
    const storeDistrict = await targetStore.getDistrict();
    return { ...targetStore.dataValues,
      Area: storeArea || null,
      Channel: storeChannel || null,
      Members: storeMembers || null,
      District: storeDistrict || null
    };
  })
};
exports.default = _default;