"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

var _Store = require("../../errors/Store");

var _Store2 = require("../../types/Store.js");

var _default = {
  type: new _graphql.GraphQLNonNull(_Store2.storeDeleteResponseType),
  args: {
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.ADMIN_SYSTEMINFO_MANAGE
  })(async (_, args) => {
    const targetStore = await _db.db.models.Store.findOne({
      where: {
        id: args.id
      }
    });
    if (!targetStore) throw new _Store.StoreNotFound();
    await _db.db.models.MemberStore.destroy({
      where: {
        StoreId: targetStore.id
      }
    });
    await targetStore.destroy();
    return {
      id: targetStore.id,
      status: true,
      message: '刪除成功'
    };
  })
};
exports.default = _default;