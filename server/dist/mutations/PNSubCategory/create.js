"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _Member = require("../../errors/Member");

var _PNTable = require("../../types/PNTable");

var _default = {
  type: new _graphql.GraphQLNonNull(_PNTable.PNSubCategoryType),
  args: {
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    picture: {
      type: _graphql.GraphQLString
    },
    isOnSale: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    PNCategoryId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    PNSubCategoryId: {
      type: _graphql.GraphQLInt
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member.PermissionError(),
    actions: _roleActions.actions.PNTABLE_MANAGE
  })(async (_, args) => _db.db.models.PNSubCategory.createPNSubCategory(args))
};
exports.default = _default;