"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _Member = require("../../errors/Member");

var _ClosedOrderSetting = require("../../types/ClosedOrderSetting");

var _default = {
  type: new _graphql.GraphQLNonNull(_ClosedOrderSetting.ClosedOrderSettingType),
  args: {
    closedDay: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    closedTime: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    code: {
      type: _graphql.GraphQLString
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member.PermissionError(),
    actions: _roleActions.actions.INVOICING_MANAGE
  })(async (_, args) => _db.db.models.ClosedOrderSetting.createClosedOrderSetting(args))
};
exports.default = _default;