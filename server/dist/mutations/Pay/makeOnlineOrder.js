"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _moment = _interopRequireDefault(require("moment"));

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _Member = require("../../errors/Member");

var _Pay = require("../../errors/Pay");

var _Pay2 = require("../../types/Pay");

var _General = require("../../errors/General");

function getRandomNumber() {
  return Math.floor(Math.random() * 900000000000 + 100000000000); // 12 digits
}

var _default = {
  type: new _graphql.GraphQLNonNull(_Pay2.PayOnlineOrderResponseType),
  args: {
    pnTableId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    picture: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    storeId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    date: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    time: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    email: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    phone: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    isShared: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    storedDesignJSON: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    isAllCustomized: {
      type: _graphql.GraphQLBoolean,
      defaultValue: false
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member.PermissionError(),
    actions: _roleActions.actions.NORMAL_MEMBER
  })(async (_, {
    pnTableId,
    picture,
    storeId,
    date,
    time,
    name,
    email,
    phone,
    isShared,
    storedDesignJSON,
    isAllCustomized
  }, {
    authPayload
  }) => {
    const {
      id
    } = authPayload;

    try {
      const whereClause = {};

      if (isAllCustomized) {
        whereClause.isAllCustomized = isAllCustomized;
      } else {
        whereClause.id = pnTableId;
      } // find pntable


      const pnTable = await _db.db.models.PNTable.findOne({
        where: whereClause
      });
      const member = await _db.db.models.Member.getMe(id);

      if (!member) {
        throw new _General.MemberNotFoundError();
      } // create order


      const orderId = `${(0, _moment.default)().format('YYYYHHMMDD')}${getRandomNumber()}`;
      await _db.db.models.OnlineOrder.create({
        id: orderId,
        picture,
        name,
        phone,
        email,
        date,
        time,
        price: pnTable.onlinePrice,
        isShared: isShared || false,
        payedStatus: 'UNPAID',
        StoreId: storeId,
        // BookedMemberId 系統分配包膜師
        OwnerId: member.id,
        PNTableId: pnTable.id
      });
      member.storedDesignJSON = storedDesignJSON;
      await member.save();
      return {
        payToken: await member.payToken(orderId),
        status: true,
        message: 'Success Created Order',
        human: '訂單已成立，每筆訂單將會保留 30 分鐘，請前往付款'
      };
    } catch (e) {
      throw new _Pay.CreateOnlineOrderError();
    }
  })
};
exports.default = _default;