"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _sequelize = require("sequelize");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _Member = require("../../errors/Member");

var _Pay = require("../../errors/Pay");

var _Pay2 = require("../../types/Pay");

var _linePayRouter = require("../../helpers/line/linePayRouter");

var _General = require("../../errors/General");

var _linebot = require("../../helpers/line/linebot");

var _default = {
  type: new _graphql.GraphQLNonNull(_Pay2.PayConfirmResponseType),
  args: {
    orderId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    transactionId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member.PermissionError(),
    actions: _roleActions.actions.NORMAL_MEMBER
  })(async (_, {
    orderId,
    transactionId
  }) => {
    const storeDateSlotNum = 4;
    const order = await _db.db.models.OnlineOrder.scope({
      method: ['search', {
        orderId
      }]
    }, {
      method: ['member', {
        archiveLimit: true
      }]
    }, 'store').findOne();
    const memberBookedOrders = await _db.db.models.OnlineOrder.findAll({
      where: {
        BookedMemberId: {
          [_sequelize.Op.in]: order.Store.Members.map(x => x.id)
        },
        date: order.date
      }
    });
    const rawFreeMembers = order.Store.Members.filter(x => {
      const memberOrders = memberBookedOrders.filter(o => o.BookedMemberId === x.id);
      return memberOrders.length < storeDateSlotNum;
    });
    const freeMembers = rawFreeMembers.filter(x => x.lineId !== null);

    if (!order || !order.Owner || !order.Store.Members.length || !freeMembers.length) {
      throw new _General.ResourceNotFoundError('Order Resource Not Found');
    }

    const mainMember = freeMembers.find(x => x.MemberStore && x.MemberStore.type === 'MAIN');
    const transaction = await _db.db.transaction();

    try {
      await (0, _linePayRouter.confirm)(transactionId, order.price);
      await order.update({
        payedStatus: 'PAIDNOTCONFIRMED',
        BookedMemberId: mainMember && mainMember.id || freeMembers[0].id
      }, {
        transaction
      }); // notify client
      // notify employee

      await (0, _linebot.sendBookedInfoToEmployee)(mainMember && mainMember.lineId || freeMembers[0].lineId, `【新預約確認通知】\n線上訂單編號：${orderId}\n訂購人姓名：${order.name}\n訂購人電話：${order.phone}\n預約時間：${order.date} ${order.time}\n預約門市：${order.Store.Channel.name}${order.Store.name}`, `command=bookConfirm&onlineOrderId=${order.id}`);
      const HOST = process.env.HOST || 'http://localhost:8108';
      const orderURL = `${HOST}/member/order`;
      await (0, _linebot.sendLineMessageToClient)(`【包膜預約成功（已付款）】\n訂單編號：${orderId}\n預約時間：${order.date} ${order.time}\n預約門市：${order.Store.Channel.name}${order.Store.name}\n訂單詳細資訊請至官網會員中心 > 訂單查詢\n訂單查詢傳送門：\n${orderURL}\n如有任何問題請與 artmo 線上小幫手聯繫`, order.Owner.lineId);
      await _db.db.models.EmployeeConfirmSchedule.create({
        sendBookedInfoTime: new Date(),
        OnlineOrderId: order.id
      }, {
        transaction
      });
      await transaction.commit();
      return {
        status: true,
        message: 'Success Confirm Payment',
        human: 'LINE PAY 付款確認成功'
      };
    } catch (e) {
      await transaction.rollback();
      throw new _Pay.PayConfirmError();
    }
  })
};
exports.default = _default;