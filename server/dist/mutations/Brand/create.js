"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _Member = require("../../errors/Member");

var _Brand = require("../../types/Brand");

var _default = {
  type: new _graphql.GraphQLNonNull(_Brand.BrandResponseType),
  args: {
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member.PermissionError(),
    actions: _roleActions.actions.PHONE_MODEL_MANAGE
  })(async (_, args) => _db.db.models.Brand.createBrand(args))
};
exports.default = _default;