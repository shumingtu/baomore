"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _debug = _interopRequireDefault(require("debug"));

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

var _Stock = require("../../errors/Stock");

var _Stock2 = require("../../types/Stock");

const debugCheck = (0, _debug.default)('BAOMORE:CHECKORDER');
var _default = {
  type: new _graphql.GraphQLNonNull(_Stock2.StockResponseType),
  args: {
    checkCode: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.INVENTORY_MANAGE
  })(async (_, {
    checkCode
  }, {
    authPayload
  }) => {
    const {
      id
    } = authPayload; // order 要拉 DISPATCHED的

    const targetQRCode = await _db.db.models.OrderQRCode.findOne({
      where: {
        qrcode: checkCode
      },
      include: [{
        model: _db.db.models.Order,
        where: {
          status: 'DISPATCHED',
          EmpolyedMemberId: id
        },
        include: [{
          model: _db.db.models.PNTable
        }]
      }]
    });

    if (!targetQRCode) {
      throw new _Stock.StockOrderGroupNotFoundError('此商品已點入庫，請關閉後確認欲執行動作');
    }

    const amount = targetQRCode.Order.quantity; // change stock with transcation

    const transaction = await _db.db.transaction();

    try {
      await targetQRCode.Order.update({
        status: 'DISPATCHEDANDCHECKED'
      }, {
        transaction
      }); // add stock

      await _db.db.models.MemberStock.addStock(id, targetQRCode.Order.PNTable.id, amount, transaction); // record

      const behavior = await _db.db.models.Behavior.findOne({
        where: {
          name: '點貨'
        }
      });
      await _db.db.models.BehaviorRecord.create({
        BehaviorId: behavior.id,
        remark: `點貨數量: ${amount}`,
        MemberId: id,
        PNTableId: targetQRCode.Order.PNTable.id,
        quantity: amount,
        orderSN: targetQRCode.Order.orderSN
      });
      await transaction.commit();
      return {
        id: targetQRCode.id,
        message: '點貨成功!',
        status: true
      };
    } catch (e) {
      debugCheck(e);
      await transaction.rollback();
      return {
        id: targetQRCode.id,
        message: '系統發生錯誤！',
        status: false
      };
    }
  })
};
exports.default = _default;