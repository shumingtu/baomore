"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _debug = _interopRequireDefault(require("debug"));

var _sequelize = require("sequelize");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

var _Stock = require("../../errors/Stock.js");

var _Stock2 = require("../../types/Stock");

var _linebot = require("../../helpers/line/linebot");

const debugTransfer = (0, _debug.default)('BAOMORE:TRANSFER');
var _default = {
  type: new _graphql.GraphQLNonNull(_Stock2.StockResponseType),
  args: {
    qrcode: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    employeeId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    remark: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.INVENTORY_MANAGE
  })(async (_, {
    qrcode,
    employeeId,
    remark
  }, {
    authPayload
  }) => {
    const {
      id,
      name
    } = authPayload;
    const targetQRCode = await _db.db.models.OrderQRCode.findOne({
      where: {
        qrcode
      },
      include: [{
        model: _db.db.models.Order,
        where: {
          status: 'DISPATCHEDANDCHECKED',
          EmpolyedMemberId: id
        },
        include: [{
          model: _db.db.models.PNTable.scope('categories')
        }]
      }]
    });

    if (!targetQRCode) {
      throw new _General.ResourceNotFoundError();
    }

    const behaviorIds = await _db.db.models.Behavior.findAll({
      where: {
        [_sequelize.Op.or]: [{
          name: '銷貨'
        }, {
          name: '退貨'
        }]
      }
    }).map(x => x.id);
    const behavior = await _db.db.models.Behavior.findOne({
      where: {
        name: '調貨'
      }
    });
    const isTransfered = await _db.db.models.BehaviorRecord.findOne({
      where: {
        [_sequelize.Op.or]: [{
          BehaviorId: {
            [_sequelize.Op.in]: behaviorIds // 銷貨or退貨

          }
        }, {
          isTransfering: true,
          // 調貨中的狀態
          BehaviorId: behavior.id
        }],
        qrcode
      }
    });

    if (isTransfered) {
      throw new _Stock.QRCodeHasBeenTransferedError();
    } // change stock with transcation


    const transaction = await _db.db.transaction();

    try {
      // record
      const member = await _db.db.models.Member.findOne({
        where: {
          id: employeeId
        }
      });
      const pnTable = targetQRCode.Order.PNTable;
      const isHaveSmallCategory = pnTable.PNSubCategory.PNSubCategory;
      const pnCategory = pnTable.PNSubCategory.PNCategory;
      const bigSubCategory = isHaveSmallCategory ? pnTable.PNSubCategory.PNSubCategory : pnTable.PNSubCategory;
      const smallSubCategory = isHaveSmallCategory ? pnTable.PNSubCategory : null;
      const remarkText = `${remark}\n商品類型:${pnCategory.name}\n大類:${bigSubCategory.name}\n小類:${smallSubCategory && smallSubCategory.name}\n品名:${pnTable.name}\n料號:${pnTable.code}`;
      const record = await _db.db.models.BehaviorRecord.create({
        remark: remarkText,
        BehaviorId: behavior.id,
        MemberId: id,
        PNTableId: targetQRCode.Order.PNTable.id,
        quantity: 1,
        qrcode,
        isTransfering: true,
        orderSN: targetQRCode.Order.orderSN
      }, {
        transaction
      });
      await (0, _linebot.sendTranferInfoToEmployee)(member, `包膜師調貨確認通知\n調出者：${name}\n品名：${pnTable.name}\n料號：${pnTable.code}\n商品序號：${qrcode}`, `command=transfer&pnTableId=${pnTable.id}&transferFromId=${id}&transferToId=${employeeId}&quantity=${1}&orderSN=${targetQRCode.Order.orderSN}&qrcode=${qrcode}&recordId=${record.id}&oriOrderId=${targetQRCode.Order.id}`); // remove 2019-08-05
      // await db.models.EmployeeTransferSchedule.create({
      //   sendTransferTime: new Date(),
      //   BehaviorRecordId: record.id,
      // }, { transaction });

      await transaction.commit();
      return {
        id: record.id,
        message: '調貨需求已送出',
        status: true
      };
    } catch (e) {
      debugTransfer(e);
      await transaction.rollback();
      throw e;
    }
  })
};
exports.default = _default;