"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.rollBackForCustomer = void 0;

var _graphql = require("graphql");

var _debug = _interopRequireDefault(require("debug"));

var _sequelize = require("sequelize");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

var _Stock = require("../../errors/Stock.js");

var _Stock2 = require("../../types/Stock");

const debugRollback = (0, _debug.default)('BAOMORE:ROLLBACK');
const rollBackForCustomer = {
  type: new _graphql.GraphQLNonNull(_Stock2.StockResponseType),
  args: {
    quantity: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    pnTableId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    picture: {
      type: _graphql.GraphQLString,
      description: '上傳照片'
    },
    storeId: {
      type: _graphql.GraphQLInt,
      description: '門市'
    },
    remark: {
      type: _graphql.GraphQLString,
      description: '其他資訊(EX: 異常類型、對象...等)'
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.INVENTORY_MANAGE
  })(async (_, {
    quantity,
    picture,
    storeId,
    remark,
    pnTableId
  }, {
    authPayload
  }) => {
    const {
      id
    } = authPayload; // change stock with transcation

    const transaction = await _db.db.transaction();

    try {
      const behavior = await _db.db.models.Behavior.findOne({
        where: {
          name: '退貨'
        }
      }); // record

      await _db.db.models.BehaviorRecord.create({
        remark,
        picture,
        StoreId: storeId,
        BehaviorId: behavior.id,
        MemberId: id,
        PNTableId: pnTableId,
        rollbackType: 'CUSTOMERCOMPLAINT',
        quantity
      }, {
        transaction
      });
      await transaction.commit();
      return {
        id: pnTableId,
        message: '退貨完成',
        status: true
      };
    } catch (e) {
      debugRollback(e);
      await transaction.rollback();
      throw e;
    }
  })
};
exports.rollBackForCustomer = rollBackForCustomer;
var _default = {
  type: new _graphql.GraphQLNonNull(_Stock2.StockResponseType),
  args: {
    qrcode: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    picture: {
      type: _graphql.GraphQLString,
      description: '上傳照片'
    },
    storeId: {
      type: _graphql.GraphQLInt,
      description: '門市'
    },
    remark: {
      type: _graphql.GraphQLString,
      description: '其他資訊(EX: 異常類型、對象...等)'
    },
    productType: {
      type: _graphql.GraphQLString
    },
    rollbackType: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString),
      description: 'DOA,PULLOF,SOCIAL,CUSTOMERCOMPLAINT,NONE'
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.INVENTORY_MANAGE
  })(async (_, {
    qrcode,
    picture,
    storeId,
    remark,
    rollbackType,
    productType
  }, {
    authPayload
  }) => {
    const {
      id
    } = authPayload;
    const targetQRCode = await _db.db.models.OrderQRCode.findOne({
      where: {
        qrcode
      },
      include: [{
        model: _db.db.models.Order,
        where: {
          status: 'DISPATCHEDANDCHECKED',
          EmpolyedMemberId: id
        },
        include: [{
          model: _db.db.models.PNTable.scope('categories')
        }]
      }]
    });

    if (!targetQRCode) {
      throw new _General.ResourceNotFoundError('該商品序號不存在！或還沒點貨');
    }

    if (rollbackType === 'DOA') {
      const pnCategory = targetQRCode.Order.PNTable.PNSubCategory.PNCategory;

      if (pnCategory.symbol !== productType) {
        throw new _Stock.PrductTypeError('商品類型不符！');
      }
    }

    const memberStock = await _db.db.models.MemberStock.findOne({
      where: {
        PNTableId: targetQRCode.Order.PNTable.id,
        MemberId: id
      }
    });

    if (!memberStock || !memberStock.storageCount) {
      throw new _General.ResourceNotFoundError('已無庫存！無法退貨');
    }

    const behaviorIds = await _db.db.models.Behavior.findAll({
      where: {
        [_sequelize.Op.or]: [{
          name: '銷貨'
        }, {
          name: '退貨'
        }]
      }
    }).map(x => x.id);
    const transferBehavior = await _db.db.models.Behavior.findOne({
      where: {
        name: '調貨'
      }
    });
    const isRollbackOrSoldOrTransfer = await _db.db.models.BehaviorRecord.findOne({
      where: {
        [_sequelize.Op.or]: [{
          BehaviorId: {
            [_sequelize.Op.in]: behaviorIds
          }
        }, {
          isTransfering: true,
          BehaviorId: transferBehavior.id
        }],
        qrcode
      }
    });

    if (isRollbackOrSoldOrTransfer) {
      throw new _Stock.QRCodeHasBeenRollBackedOrSoldError();
    } // change stock with transcation


    const transaction = await _db.db.transaction();

    try {
      const behavior = await _db.db.models.Behavior.findOne({
        where: {
          name: '退貨'
        }
      }); // record

      await _db.db.models.BehaviorRecord.create({
        remark,
        picture,
        StoreId: storeId,
        BehaviorId: behavior.id,
        MemberId: id,
        PNTableId: targetQRCode.Order.PNTable.id,
        rollbackType,
        quantity: 1,
        qrcode,
        orderSN: targetQRCode.Order.orderSN
      }, {
        transaction
      });
      await transaction.commit();
      return {
        id: targetQRCode.Order.PNTable.id,
        message: '退貨完成',
        status: true
      };
    } catch (e) {
      debugRollback(e);
      await transaction.rollback();
      throw e;
    }
  })
};
exports.default = _default;