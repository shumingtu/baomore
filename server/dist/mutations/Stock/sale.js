"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.saleStop = exports.saleStart = void 0;

var _graphql = require("graphql");

var _debug = _interopRequireDefault(require("debug"));

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _sequelize = require("sequelize");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

var _Stock = require("../../errors/Stock");

var _Stock2 = require("../../types/Stock");

const debugSale = (0, _debug.default)('BAOMORE:SALE');
const saleStart = {
  type: new _graphql.GraphQLNonNull(_Stock2.StockResponseType),
  args: {
    startTime: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    storeId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    vat: {
      type: _graphql.GraphQLString
    },
    saleCode: {
      type: _graphql.GraphQLString
    },
    productCode: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.INVENTORY_MANAGE
  })(async (_, {
    startTime,
    storeId,
    vat,
    saleCode,
    productCode
  }, {
    authPayload
  }) => {
    if (!vat && !saleCode) throw new _General.InvalidParameter('Vat And SaleCode Can Not Both Empty');
    const {
      id
    } = authPayload;
    const targetQRCode = await _db.db.models.OrderQRCode.findOne({
      where: {
        qrcode: productCode
      },
      include: [{
        model: _db.db.models.Order,
        where: {
          status: 'DISPATCHEDANDCHECKED',
          EmpolyedMemberId: id
        },
        include: [{
          model: _db.db.models.PNTable
        }]
      }]
    });
    const targetStore = await _db.db.models.Store.scope('channel', 'district').findByPk(storeId);

    if (!targetQRCode || !targetStore) {
      throw new _General.ResourceNotFoundError();
    }

    const pnTableId = targetQRCode.Order.PNTable.id;
    const stock = await _db.db.models.MemberStock.scope({
      method: ['search', {
        memberId: id,
        pnTableId
      }]
    }).findOne({
      where: {
        storageCount: {
          [_sequelize.Op.ne]: 0
        }
      }
    });
    if (!stock) throw new _Stock.StockOverflowError();
    const rollbackBehavior = await _db.db.models.Behavior.findOne({
      where: {
        name: '退貨'
      }
    });
    const behavior = await _db.db.models.Behavior.findOne({
      where: {
        name: '銷貨'
      }
    });
    const isRollbackOrSold = await _db.db.models.BehaviorRecord.findOne({
      where: {
        [_sequelize.Op.or]: [{
          BehaviorId: rollbackBehavior.id
        }, {
          BehaviorId: behavior.id,
          startTime: {
            [_sequelize.Op.ne]: null
          },
          endTime: {
            [_sequelize.Op.ne]: null
          }
        }],
        qrcode: productCode
      }
    });

    if (isRollbackOrSold) {
      throw new _Stock.QRCodeHasBeenRollBackedOrSoldError();
    } // change stock with transcation


    const transaction = await _db.db.transaction();

    try {
      // record
      const vatOrSaleCodeRemark = vat ? `發票號碼:${vat}` : `銷貨編號${saleCode}`;
      const existBehaviorRecord = await _db.db.models.BehaviorRecord.findOne({
        where: {
          BehaviorId: behavior.id,
          qrcode: productCode
        }
      });

      if (existBehaviorRecord) {
        await existBehaviorRecord.destroy({
          transaction
        });
      }

      const record = await _db.db.models.BehaviorRecord.create({
        startTime,
        price: targetQRCode.Order.PNTable.price,
        remark: `銷貨縣市:${targetStore.District.City.name} ${targetStore.District.name}\n門市:${targetStore.name}\n通路:${targetStore.Channel.name}\n${vatOrSaleCodeRemark}`,
        BehaviorId: behavior.id,
        StoreId: storeId,
        MemberId: id,
        PNTableId: targetQRCode.Order.PNTable.id,
        quantity: 1,
        qrcode: productCode,
        orderSN: targetQRCode.Order.orderSN
      }, {
        transaction
      });
      await transaction.commit();
      return {
        id: record.id,
        message: '銷貨開始!',
        status: true
      };
    } catch (e) {
      debugSale(e);
      await transaction.rollback();
      throw e;
    }
  })
};
exports.saleStart = saleStart;
const saleStop = {
  type: new _graphql.GraphQLNonNull(_Stock2.StockResponseType),
  args: {
    endTime: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    recordId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.MANAGE_STOCK_SYSTEM
  })(async (_, {
    endTime,
    recordId
  }, {
    authPayload
  }) => {
    const record = await _db.db.models.BehaviorRecord.scope({
      method: ['search', {
        id: recordId
      }]
    }).findOne();
    if (!record) throw new _Stock.SalesRecordNotFoundError();

    if (record.endTime) {
      return {
        id: record.id,
        message: '銷貨完成!',
        status: true
      };
    } // change stock with transcation


    const transaction = await _db.db.transaction();

    try {
      // reduce stock
      await _db.db.models.MemberStock.reduceStock(authPayload.id, record.PNTableId, 1, transaction); // record

      record.endTime = endTime;
      await record.save({
        transaction
      });
      await transaction.commit();
      return {
        id: record.id,
        message: '銷貨完成!',
        status: true
      };
    } catch (e) {
      debugSale(e);
      await transaction.rollback();
      throw e;
    }
  })
};
exports.saleStop = saleStop;