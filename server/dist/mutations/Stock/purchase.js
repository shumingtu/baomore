"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _debug = _interopRequireDefault(require("debug"));

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

var _Stock = require("../../types/Stock");

const debugPurchase = (0, _debug.default)('BAOMORE:PURCHASE');
var _default = {
  type: new _graphql.GraphQLNonNull(_Stock.StockResponseTypeForPurchase),
  args: {
    pnTableId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    storeId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    quantity: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.INVENTORY_MANAGE
  })(async (_, {
    pnTableId,
    storeId,
    quantity
  }, {
    authPayload
  }) => {
    const {
      id
    } = authPayload;
    const pnTable = await _db.db.models.PNTable.scope('vendor', 'categories', {
      method: ['search', {
        id: pnTableId
      }]
    }).findOne();
    const store = await _db.db.models.Store.scope({
      method: ['search', {
        storeId
      }]
    }).findOne();
    if (!pnTable || !store) throw new _General.ResourceNotFoundError('PNTable Or store not found'); // 檢查目前包膜師是否已有訂貨中的訂單 有的話要先刪掉

    const existOrder = await _db.db.models.Order.findOne({
      where: {
        EmpolyedMemberId: id,
        status: 'ORDERING',
        PNTableId: pnTableId
      }
    });

    if (existOrder) {
      await existOrder.destroy();
    } // 同廠商 其他料號的訂單


    const sameVendorOrder = await _db.db.models.Order.findAll({
      where: {
        EmpolyedMemberId: id,
        status: 'ORDERING'
      },
      include: [{
        model: _db.db.models.PNTable,
        where: {
          VendorId: pnTable.Vendor.id
        }
      }]
    });
    let totalValues = pnTable.price * quantity;
    let totalQuantity = quantity;
    const vendorSettings = pnTable.Vendor.VendorSettings;
    const memberVendorSettings = vendorSettings.filter(v => v.MemberId === id);
    const defaultVendorSettings = vendorSettings.filter(v => v.MemberId === null);

    if (sameVendorOrder.length) {
      sameVendorOrder.forEach(x => {
        totalValues += x.quantity * x.PNTable.price;
        totalQuantity += x.quantity;
      });
    }

    let message = '進貨成功！';

    if (memberVendorSettings.length) {
      const quantitySetting = memberVendorSettings.find(v => v.type === 'QUANTITY');
      const valueSetting = memberVendorSettings.find(v => v.type === 'MONEY');

      if (quantitySetting && totalQuantity < quantitySetting.value) {
        message += `\n${quantitySetting.message}`;
      }

      if (valueSetting && totalValues < valueSetting.value) {
        message += `\n${valueSetting.message}`;
      }
    }

    if (!memberVendorSettings.length && defaultVendorSettings.length) {
      const quantitySetting = defaultVendorSettings.find(v => v.type === 'QUANTITY');
      const valueSetting = defaultVendorSettings.find(v => v.type === 'MONEY');

      if (quantitySetting && totalQuantity < quantitySetting.value) {
        message += `\n${quantitySetting.message}`;
      }

      if (valueSetting && totalValues < valueSetting.value) {
        message += `\n${valueSetting.message}`;
      }
    }

    const transaction = await _db.db.transaction();

    try {
      const isHaveSmallCategory = pnTable.PNSubCategory.PNSubCategory;
      const pnCategory = pnTable.PNSubCategory.PNCategory;
      const bigSubCategory = isHaveSmallCategory ? pnTable.PNSubCategory.PNSubCategory : pnTable.PNSubCategory;
      const smallSubCategory = isHaveSmallCategory ? pnTable.PNSubCategory : null; // create order

      const order = await _db.db.models.Order.create({
        quantity,
        picture: pnTable.picture,
        orderSource: 'EMPOLYEE',
        StoreId: store.id,
        EmpolyedMemberId: id,
        PNTableId: pnTable.id,
        ProductType: pnCategory.symbol
      }, {
        transaction
      });
      const remarkText = `進貨數量:${quantity}\n商品類型:${pnCategory.name}\n大類:${bigSubCategory.name}\n小類:${smallSubCategory && smallSubCategory.name}\n品名:${pnTable.name}\n料號:${pnTable.code}`; // record

      const behavior = await _db.db.models.Behavior.findOne({
        where: {
          name: '進貨'
        }
      });
      await _db.db.models.BehaviorRecord.create({
        remark: remarkText,
        BehaviorId: behavior.id,
        VendorId: pnTable.VendorId,
        StoreId: store.id,
        MemberId: id,
        PNTableId: pnTable.id,
        quantity
      }, {
        transaction
      });
      await transaction.commit();
      return {
        pnTableId,
        message,
        status: true,
        order
      };
    } catch (e) {
      debugPurchase(e);
      await transaction.rollback();
      throw e;
    }
  })
};
exports.default = _default;