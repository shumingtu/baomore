"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _Member = require("../../types/Member.js");

var _General = require("../../errors/General");

var _default = {
  type: new _graphql.GraphQLNonNull(_Member.memberMeType),
  args: {
    json: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.NORMAL_MEMBER
  })(async (n, {
    json
  }, {
    authPayload
  }) => {
    const {
      id
    } = authPayload;
    const member = await _db.db.models.Member.getMe(id);
    if (!member) throw new _General.MemberNotFoundError();
    member.storedDesignJSON = json;
    await member.save();
    return member;
  })
};
exports.default = _default;