"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _moment = _interopRequireDefault(require("moment"));

var _sequelize = require("sequelize");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _Member = require("../../types/Member.js");

var _General = require("../../errors/General");

var _default = {
  type: new _graphql.GraphQLNonNull(_Member.employeePunchResponseType),
  args: {
    punchTime: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    latitude: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLFloat)
    },
    longitude: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLFloat)
    },
    storeId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.CHECKIN
  })(async (n, {
    punchTime,
    latitude,
    longitude,
    storeId
  }, {
    authPayload
  }) => {
    const {
      id
    } = authPayload;
    const store = await _db.db.models.Store.scope({
      method: ['search', {
        id: storeId
      }]
    }).findOne();
    if (!store) throw new _General.ResourceNotFoundError('Store Not Found Error');
    const startOfTime = (0, _moment.default)(punchTime).startOf('day').toDate();
    const endOfTime = (0, _moment.default)(punchTime).endOf('day').toDate(); // 0 => 上班, 1 => 下班 2=> 上下班已打過卡

    const existRecordCount = await _db.db.models.MemberPunchRecord.count({
      where: {
        punchTime: {
          [_sequelize.Op.gt]: startOfTime,
          [_sequelize.Op.lt]: endOfTime
        },
        MemberId: id
      }
    });
    if (existRecordCount === 2) throw new Error('已打過上下班卡！');
    const record = await _db.db.models.MemberPunchRecord.create({
      punchTime,
      latitude,
      longitude,
      type: existRecordCount ? 'OFF' : 'ON',
      MemberId: id,
      StoreId: storeId
    });
    return {
      id: record.id,
      status: true,
      message: '打卡完成！'
    };
  })
};
exports.default = _default;