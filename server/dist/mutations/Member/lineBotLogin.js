"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _debug = _interopRequireDefault(require("debug"));

var _db = require("../../db");

var _Member = require("../../types/Member.js");

var _lineRouter = require("../../helpers/line/lineRouter");

var _General = require("../../errors/General");

const debugLineBotLogin = (0, _debug.default)('BAOMORE:GrqphQLLineBotLogin');
var _default = {
  type: new _graphql.GraphQLNonNull(_Member.employeeLoginType),
  args: {
    code: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    SN: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    phone: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  },
  resolve: async (n, {
    code,
    SN,
    phone
  }) => {
    const employee = await _db.db.models.Member.findOne({
      where: {
        serialNumber: SN,
        phone,
        archive: false
      }
    });
    if (!employee) throw new _General.MemberNotFoundError();

    try {
      const result = await (0, _lineRouter.employeeLogin)(employee, code);
      return result;
    } catch (e) {
      debugLineBotLogin(e);
      return e;
    }
  }
};
exports.default = _default;