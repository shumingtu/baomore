"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.editMemberForConsumer = exports.editMemberForAdmin = exports.editMe = exports.toggleMemberArchive = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _Member = require("../../types/Member.js");

var _General = require("../../errors/General");

const toggleMemberArchive = {
  type: _Member.memberToggleArchiveResponseType,
  args: {
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.ADMIN_MEMBER_MANAGE
  })(async (_, args) => {
    const {
      id
    } = args;
    const target = await _db.db.models.Member.findOne({
      where: {
        id
      }
    });
    if (!target) throw new _General.MemberNotFoundError();
    await target.update({
      archive: !target.archive
    });
    return {
      id,
      message: '成功',
      status: true
    };
  })
};
exports.toggleMemberArchive = toggleMemberArchive;
const editMe = {
  type: new _graphql.GraphQLNonNull(_Member.memberMeType),
  args: {
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    email: {
      type: _graphql.GraphQLString
    },
    gender: {
      type: _graphql.GraphQLString
    },
    birthday: {
      type: _graphql.GraphQLString
    },
    phone: {
      type: _graphql.GraphQLString
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.NORMAL_MEMBER
  })(async (n, {
    name,
    email,
    gender,
    birthday,
    phone
  }, {
    authPayload
  }) => {
    const {
      id
    } = authPayload;
    const member = await _db.db.models.Member.getMe(id);
    if (!member) throw new _General.MemberNotFoundError();
    if (name) member.name = name;
    if (email) member.email = email;
    if (gender) member.gender = gender;
    if (birthday) member.birthday = birthday;
    if (phone) member.phone = phone;
    await member.save();
    return member;
  })
};
exports.editMe = editMe;
const editMemberForAdmin = {
  type: new _graphql.GraphQLNonNull(_Member.memberType),
  args: {
    memberId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    serialNumber: {
      type: _graphql.GraphQLString
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    email: {
      type: _graphql.GraphQLString
    },
    gender: {
      type: _graphql.GraphQLString
    },
    birthday: {
      type: _graphql.GraphQLString
    },
    phone: {
      type: _graphql.GraphQLString
    },
    areaId: {
      type: _graphql.GraphQLInt
    },
    directorId: {
      type: _graphql.GraphQLInt
    },
    lineAccount: {
      type: _graphql.GraphQLString
    },
    roleIds: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLInt))
    },
    lineId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.ADMIN_MEMBER_MANAGE
  })(async (n, {
    memberId,
    serialNumber,
    name,
    email,
    gender,
    birthday,
    phone,
    areaId,
    directorId,
    roleIds,
    lineId,
    lineAccount
  }) => {
    const targetMember = await _db.db.models.Member.getMe(memberId);
    if (!targetMember) throw new _General.MemberNotFoundError();
    const existMember = await _db.db.models.Member.findOne({
      where: {
        lineId
      }
    });
    if (existMember && existMember.id !== targetMember.id) throw new _General.DuplicatedLineIdError();
    await targetMember.update({
      serialNumber: serialNumber || null,
      name,
      email: email || null,
      gender: gender || null,
      birthday: birthday || null,
      phone: phone || null,
      AreaId: areaId || null,
      MemberId: directorId || null,
      lineId,
      lineAccount: lineAccount || null
    });
    await _db.MemberRole.destroy({
      where: {
        MemberId: targetMember.id
      }
    });
    await targetMember.addRoles(roleIds);
    return { ...targetMember.dataValues,
      Roles: await targetMember.getRoles(),
      Area: await targetMember.getArea(),
      director: await targetMember.getMember()
    };
  })
};
exports.editMemberForAdmin = editMemberForAdmin;
const editMemberForConsumer = {
  type: new _graphql.GraphQLNonNull(_Member.memberType),
  args: {
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    lineId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    birthday: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    phone: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    email: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    gender: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.ADMIN_MEMBER_MANAGE
  })(async (n, {
    id,
    lineId,
    birthday,
    name,
    phone,
    email,
    gender
  }) => {
    const targetMember = await _db.db.models.Member.scope('warranty', 'onlineOrderStore').findOne({
      where: {
        id
      }
    });
    if (!targetMember) throw new _General.MemberNotFoundError();
    await targetMember.update({
      lineId,
      birthday,
      name,
      phone,
      email,
      gender
    });
    return targetMember;
  })
};
exports.editMemberForConsumer = editMemberForConsumer;