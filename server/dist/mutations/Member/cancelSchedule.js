"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _OnlineOrder = require("../../types/OnlineOrder");

var _General = require("../../errors/General");

var _OnlineOrder2 = require("../../errors/OnlineOrder");

var _default = {
  type: new _graphql.GraphQLNonNull(_OnlineOrder.onlineOrderType),
  args: {
    onlineOrderId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.ONLINE_ORDER_MANAGE
  })(async (n, {
    onlineOrderId
  }, {
    authPayload
  }) => {
    const {
      id,
      name
    } = authPayload;
    const onlineOrderScope = await _db.db.models.OnlineOrder.scope({
      method: ['search', {
        orderId: onlineOrderId
      }]
    }, 'store', 'bookedMember', 'member');
    const onlineOrder = await onlineOrderScope.find();
    if (!onlineOrder) throw new _OnlineOrder2.OnlineOrderNotFoundError();
    await onlineOrder.destroy(); // record

    _db.db.models.SystemLog.createLog(1, id, `${name} 刪除了訂單，訂單時間: ${onlineOrder.date} ${onlineOrder.time}, 門市: ${onlineOrder.Store.name}, 包膜師: ${onlineOrder.BookedMember.name}`); // send notification


    return onlineOrder;
  })
};
exports.default = _default;