"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _Member = require("../../types/Member.js");

var _General = require("../../errors/General");

var _default = {
  type: new _graphql.GraphQLNonNull(_Member.managerPunchResponseType),
  args: {
    punchStartTime: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    punchEndTime: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    grade: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLFloat)
    },
    remark: {
      type: _graphql.GraphQLString
    },
    employeeId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    storeId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: [_roleActions.actions.CHECKIN, _roleActions.actions.MANAGE_EMPLOYEE_MANAGER]
  })(async (n, {
    punchStartTime,
    punchEndTime,
    grade,
    remark,
    employeeId,
    storeId
  }, {
    authPayload
  }) => {
    const {
      id
    } = authPayload;
    const store = await _db.db.models.Store.scope({
      method: ['search', {
        id: storeId
      }]
    }).findOne();
    if (!store) throw new _General.ResourceNotFoundError('Store Not Found Error'); // check relation between id and employeeId

    const record = await _db.db.models.ManagerPunchRecord.create({
      punchStartTime,
      punchEndTime,
      grade,
      remark,
      EmployeedMemberId: employeeId,
      MemberId: id,
      StoreId: storeId
    });
    return {
      id: record.id,
      status: true,
      message: '巡檢打卡完成！'
    };
  })
};
exports.default = _default;