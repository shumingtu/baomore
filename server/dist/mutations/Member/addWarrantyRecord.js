"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _moment = _interopRequireDefault(require("moment"));

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _Warranty = require("../../types/Warranty.js");

var _General = require("../../errors/General");

var _linebot = require("../../helpers/line/linebot.js");

var _default = {
  type: new _graphql.GraphQLNonNull(_Warranty.memberWarrantyType),
  args: {
    service: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    phoneModel: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    store: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    VAT: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    IME: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    phone: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    EmployeeMemberId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.NORMAL_MEMBER
  })(async (n, {
    service,
    phoneModel,
    store,
    VAT,
    IME,
    phone,
    EmployeeMemberId
  }, {
    authPayload
  }) => {
    const {
      id
    } = authPayload;
    const employee = await _db.db.models.Member.getMe(EmployeeMemberId);
    const customer = await _db.db.models.Member.getMe(id);

    if (!employee || !customer) {
      throw new _General.InvalidParameter('Invalid Parameter: employee id || authPayload id');
    }

    const record = await _db.db.models.MemberWarranty.create({
      service,
      phoneModel,
      store,
      VAT,
      IME,
      phone,
      MemberId: id
    });
    await (0, _linebot.sendWarrantyInfoToEmployee)(employee, `【Artmo 產品售後服務卡】\n消費時間：${(0, _moment.default)().format('YYYY-MM-DD HH:mm:ss')}\n顧客名稱：${customer.name}\n服務項目：${service}\n手機型號：${phoneModel}\nIMEI後五碼：${IME}\n統一發票：${VAT}\n消費門市：${store}`, `command=warrantyConfirm&recordId=${record.id}&employeeId=${employee.id}`, `command=warrantyCancel&recordId=${record.id}`);
    return { ...record.toJSON()
    };
  })
};
exports.default = _default;