"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _OnlineOrder = require("../../types/OnlineOrder");

var _General = require("../../errors/General");

var _OnlineOrder2 = require("../../errors/OnlineOrder");

var _linebot = require("../../helpers/line/linebot");

var _default = {
  type: new _graphql.GraphQLNonNull(_OnlineOrder.onlineOrderType),
  args: {
    onlineOrderId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    date: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    time: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    employeeId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    storeId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.ONLINE_ORDER_MANAGE
  })(async (n, {
    onlineOrderId,
    date,
    time,
    employeeId,
    storeId
  }, {
    authPayload
  }) => {
    const {
      id,
      name
    } = authPayload;
    const onlineOrderScope = await _db.db.models.OnlineOrder.scope({
      method: ['search', {
        orderId: onlineOrderId
      }]
    }, 'store', 'bookedMember', 'member');
    const onlineOrder = await onlineOrderScope.find();
    const store = await _db.db.models.Store.scope({
      method: ['search', {
        storeId
      }]
    }).findOne();
    const bookTimeSlot = await _db.db.models.BookTimeSlot.scope({
      method: ['search', {
        time
      }]
    }).findOne();
    if (!onlineOrder) throw new _OnlineOrder2.OnlineOrderNotFoundError();
    if (!bookTimeSlot || !store) throw new _General.ResourceNotFoundError();
    await onlineOrderScope.update({
      date,
      time,
      BookTimeSlotGroupId: bookTimeSlot.BookTimeSlotGroupId,
      BookedMemberId: employeeId,
      StoreId: store.id
    });
    const updatedOnlineOrder = await onlineOrderScope.find();
    const director = await updatedOnlineOrder.BookedMember.getMember();

    if (director && director.lineId) {
      await (0, _linebot.sendLineMessage)(null, `【包膜預約行程變更通知】\n線上訂單編號：${updatedOnlineOrder.id}\n訂購人姓名：${updatedOnlineOrder.Owner.name}\n訂購人電話：${updatedOnlineOrder.Owner.phone}\n預約時間：${updatedOnlineOrder.date} ${updatedOnlineOrder.time}\n預約門市：${updatedOnlineOrder.Store.Channel.name}${updatedOnlineOrder.Store.name}`, director.lineId);
    }

    if (updatedOnlineOrder.Owner.lineId) {
      if (updatedOnlineOrder.date !== onlineOrder.date || updatedOnlineOrder.time !== onlineOrder.time || updatedOnlineOrder.Store.id !== onlineOrder.Store.id) {
        // 如果只更動包膜師不用通知客戶
        await (0, _linebot.sendLineMessageToClient)(`【包膜預約行程變更通知】\n訂單編號：${updatedOnlineOrder.id}\n預約時間：${updatedOnlineOrder.date} ${updatedOnlineOrder.time}\n預約門市：${updatedOnlineOrder.Store.Channel.name}${updatedOnlineOrder.Store.name}\n訂單詳細資訊請至官網會員中心 > 訂單查詢\n訂單查詢傳送門：\nhttps://baomore.rytass.com/member/order\n如有任何問題請與 artmo 線上小幫手聯繫`, updatedOnlineOrder.Owner.lineId);
      }
    }

    if (updatedOnlineOrder.BookedMember.lineId) {
      await (0, _linebot.sendLineMessage)(null, `【包膜預約行程變更通知】\n線上訂單編號：${updatedOnlineOrder.id}\n訂購人姓名：${updatedOnlineOrder.Owner.name}\n訂購人電話：${updatedOnlineOrder.Owner.phone}\n預約時間：${updatedOnlineOrder.date} ${updatedOnlineOrder.time}\n預約門市：${updatedOnlineOrder.Store.Channel.name}${updatedOnlineOrder.Store.name}`, updatedOnlineOrder.BookedMember.lineId);
    } // record


    _db.db.models.SystemLog.createLog(1, id, `${name} 修改訂單的包膜師時間至: ${updatedOnlineOrder.date} ${updatedOnlineOrder.time}, 門市: ${updatedOnlineOrder.Store.name}, 包膜師: ${updatedOnlineOrder.BookedMember.name}`); // send notification


    return updatedOnlineOrder;
  })
};
exports.default = _default;