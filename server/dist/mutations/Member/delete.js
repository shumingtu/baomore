"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _Member = require("../../errors/Member");

var _Member2 = require("../../types/Member");

var _General = require("../../errors/General");

var _default = {
  type: new _graphql.GraphQLNonNull(_Member2.memberDeleteResponseType),
  args: {
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member.PermissionError(),
    actions: _roleActions.actions.ADMIN_MEMBER_MANAGE
  })(async (_, args) => {
    const targetMember = await _db.db.models.Member.findOne({
      attributes: ['id'],
      where: {
        id: args.id
      }
    });
    if (!targetMember) throw new _General.MemberNotFoundError();
    await targetMember.destroy();
    return {
      id: targetMember.id,
      status: true,
      message: '刪除成功'
    };
  })
};
exports.default = _default;