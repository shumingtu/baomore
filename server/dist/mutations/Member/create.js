"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _Member = require("../../errors/Member");

var _General = require("../../errors/General.js");

var _Member2 = require("../../types/Member");

var _default = {
  type: new _graphql.GraphQLNonNull(_Member2.memberType),
  args: {
    serialNumber: {
      type: _graphql.GraphQLString
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    email: {
      type: _graphql.GraphQLString
    },
    gender: {
      type: _graphql.GraphQLString
    },
    birthday: {
      type: _graphql.GraphQLString
    },
    phone: {
      type: _graphql.GraphQLString
    },
    areaId: {
      type: _graphql.GraphQLInt
    },
    directorId: {
      type: _graphql.GraphQLInt
    },
    lineAccount: {
      type: _graphql.GraphQLString
    },
    roleIds: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLInt))
    },
    lineId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member.PermissionError(),
    actions: _roleActions.actions.ADMIN_MEMBER_MANAGE
  })(async (_, args) => {
    const {
      serialNumber,
      name,
      email,
      gender,
      birthday,
      phone,
      areaId,
      directorId,
      roleIds,
      lineId,
      lineAccount
    } = args;
    const existMember = await _db.db.models.Member.findOne({
      where: {
        lineId
      }
    });
    if (existMember) throw new _General.DuplicatedLineIdError();
    const createdMember = await _db.db.models.Member.create({
      serialNumber: serialNumber || null,
      name,
      email: email || null,
      gender: gender || null,
      birthday: birthday || null,
      phone: phone || null,
      AreaId: areaId || null,
      MemberId: directorId || null,
      lineId,
      lineAccount: lineAccount || null
    });
    if (!createdMember) throw new _Member.MemberCreateFaild();
    await createdMember.addRoles(roleIds);
    const memberRoles = await createdMember.getRoles();
    return { ...createdMember.dataValues,
      Roles: memberRoles
    };
  })
};
exports.default = _default;