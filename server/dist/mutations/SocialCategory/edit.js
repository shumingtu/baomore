"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _fs = _interopRequireDefault(require("fs"));

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _roleActions = require("../../../shared/roleActions");

var _Member = require("../../errors/Member");

var _SocialCategory = require("../../types/SocialCategory");

var _default = {
  type: new _graphql.GraphQLNonNull(_SocialCategory.listType),
  args: {
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    link: {
      type: _graphql.GraphQLString
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member.PermissionError(),
    actions: _roleActions.actions.ADMIN_WEBSITE_MANAGE
  })((_, args) => {
    const rawData = _fs.default.readFileSync('shared/socialCategory.json');

    if (!rawData) return null;
    const result = JSON.parse(rawData);
    const otherData = result.socialCategory.filter(x => x.id !== args.id);
    const payload = {
      id: args.id,
      name: args.name,
      link: args.link || ''
    };
    const updatedData = {
      socialCategory: [...otherData, payload]
    };

    _fs.default.writeFileSync('shared/socialCategory.json', JSON.stringify(updatedData));

    return payload;
  })
};
exports.default = _default;