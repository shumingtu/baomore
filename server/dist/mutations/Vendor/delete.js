"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

var _Vendor = require("../../errors/Vendor");

var _Vendor2 = require("../../types/Vendor");

var _default = {
  type: new _graphql.GraphQLNonNull(_Vendor2.vendorDeleteResponseType),
  args: {
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.ADMIN_SYSTEMINFO_MANAGE
  })(async (_, args) => {
    const targetVendor = await _db.db.models.Vendor.findOne({
      where: {
        id: args.id
      }
    });
    if (!targetVendor) throw new _Vendor.VendorNotFound();
    await _db.db.models.VendorSetting.destroy({
      where: {
        VendorId: targetVendor.id
      }
    });
    await targetVendor.destroy();
    return {
      id: targetVendor.id,
      status: true,
      message: '刪除成功'
    };
  })
};
exports.default = _default;