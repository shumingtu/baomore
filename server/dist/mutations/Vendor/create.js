"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

var _Vendor = require("../../errors/Vendor");

var _Vendor2 = require("../../types/Vendor");

var _VendorSetting = require("../../types/VendorSetting");

var _default = {
  type: new _graphql.GraphQLNonNull(_Vendor2.VendorType),
  args: {
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    contactPersonName: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    phone: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    address: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    settings: {
      type: new _graphql.GraphQLList(_VendorSetting.settingType)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.ADMIN_SYSTEMINFO_MANAGE
  })(async (_, args) => {
    const {
      name,
      contactPersonName,
      phone,
      address,
      settings
    } = args;
    const createdVendor = await _db.db.models.Vendor.create({
      name,
      contactPersonName,
      phone,
      address
    });
    if (!createdVendor) throw new _Vendor.VendorCreateFailed();

    if (settings && settings.length) {
      const settingValue = settings.map(x => ({ ...x,
        VendorId: createdVendor.id
      }));
      await _db.db.models.VendorSetting.bulkCreate(settingValue);
    }

    const vendorSettings = await createdVendor.getVendorSettings();
    return { ...createdVendor.dataValues,
      VendorSettings: vendorSettings
    };
  })
};
exports.default = _default;