"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _Member = require("../../errors/Member");

var _Activity = require("../../types/Activity");

var _default = {
  type: new _graphql.GraphQLNonNull(_Activity.activityListType),
  args: {
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    desktopImg: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    mobileImg: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    description: {
      type: _graphql.GraphQLString
    },
    fragments: {
      type: new _graphql.GraphQLList(_Activity.activityFragmentInputType)
    },
    categoryId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member.PermissionError(),
    actions: _roleActions.actions.ADMIN_WEBSITE_MANAGE
  })(async (_, args) => _db.db.models.Activity.createActivity(args))
};
exports.default = _default;