"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.toggleActivity = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _Member = require("../../errors/Member");

var _General = require("../../errors/General");

var _Activity = require("../../types/Activity");

const toggleActivity = {
  type: new _graphql.GraphQLNonNull(_Activity.activityToggleResponseType),
  args: {
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member.PermissionError(),
    actions: _roleActions.actions.ADMIN_WEBSITE_MANAGE
  })(async (_, args) => {
    const targetActivity = await _db.db.models.Activity.findByPk(args.id);
    if (!targetActivity) throw new _General.ResourceNotFoundError();
    targetActivity.update({
      isActived: !targetActivity.isActived
    });
    return {
      id: targetActivity.id,
      status: true,
      message: '成功！'
    };
  })
};
exports.toggleActivity = toggleActivity;
var _default = {
  type: new _graphql.GraphQLNonNull(_Activity.activityListType),
  args: {
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    desktopImg: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    mobileImg: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    description: {
      type: _graphql.GraphQLString
    },
    fragments: {
      type: new _graphql.GraphQLList(_Activity.activityFragmentInputType)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member.PermissionError(),
    actions: _roleActions.actions.ADMIN_WEBSITE_MANAGE
  })(async (_, args) => _db.db.models.Activity.editActivity(args))
};
exports.default = _default;