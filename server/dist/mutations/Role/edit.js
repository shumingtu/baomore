"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

var _Role = require("../../types/Role.js");

var _default = {
  type: new _graphql.GraphQLNonNull(_Role.roleType),
  args: {
    roleId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    actionCodes: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLInt))
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.ADMIN_MEMBER_MANAGE
  })(async (_, args) => {
    const {
      roleId,
      name,
      actionCodes
    } = args;
    const targetRole = await _db.db.models.Role.findOne({
      where: {
        id: roleId
      }
    });
    if (!targetRole) throw new _General.ResourceNotFoundError();
    const transaction = await _db.db.transaction();

    try {
      await targetRole.update({
        name
      }, {
        transaction
      });
      await _db.RoleAction.destroy({
        where: {
          RoleId: targetRole.id
        },
        transaction
      });
      if (actionCodes && actionCodes.length) await targetRole.addActions(actionCodes, {
        transaction
      });
      await transaction.commit();
      const newActions = await targetRole.getActions();
      return { ...targetRole.dataValues,
        Actions: newActions
      };
    } catch (e) {
      await transaction.rollback();
      throw e;
    }
  })
};
exports.default = _default;