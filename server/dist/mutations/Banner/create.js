"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _Member = require("../../errors/Member");

var _Banner = require("../../types/Banner");

var _default = {
  type: new _graphql.GraphQLNonNull(_Banner.listType),
  args: {
    picture: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    link: {
      type: _graphql.GraphQLString
    },
    mobileImg: {
      type: _graphql.GraphQLString
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member.PermissionError(),
    actions: _roleActions.actions.ADMIN_WEBSITE_MANAGE
  })(async (_, args) => _db.db.models.Banner.createBanner(args))
};
exports.default = _default;