"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

var _BehaviorRecord = require("../../types/BehaviorRecord");

var _default = {
  type: _BehaviorRecord.verifyRollbackBehaviorRecordType,
  args: {
    recordIds: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLInt))
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.ADMIN_DOA_MANAGE
  })(async (_, {
    recordIds
  }, {
    authPayload
  }) => _db.db.models.BehaviorRecord.verifyBehaviorRecords({
    recordIds,
    approverId: authPayload.id
  }))
};
exports.default = _default;