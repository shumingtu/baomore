"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.deleteOrderByLineBot = exports.deleteOrderByAdmin = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _Member = require("../../errors/Member");

var _General = require("../../errors/General");

var _Order = require("../../types/Order");

const deleteOrderByAdmin = {
  type: new _graphql.GraphQLNonNull(_Order.orderDeleteResponseType),
  args: {
    orderId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member.PermissionError(),
    actions: _roleActions.actions.INVOICING_MANAGE
  })(async (_, args) => {
    const {
      orderId
    } = args;
    const targetOrder = await _db.db.models.Order.findByPk(orderId);
    if (!targetOrder) throw new _General.ResourceNotFoundError();

    if (targetOrder.status === 'ORDERING') {
      await targetOrder.destroy();
      return {
        id: targetOrder.id,
        status: true,
        message: '成功刪除'
      };
    }

    if (targetOrder.status === 'UNDISPATCHED') {
      const transaction = await _db.db.transaction();

      try {
        await targetOrder.destroy({
          transaction
        });
        await _db.db.models.OrderQRCode.destroy({
          where: {
            OrderId: targetOrder.id
          },
          transaction,
          force: true
        });
        await transaction.commit();
      } catch (e) {
        await transaction.rollback();
        throw e;
      }

      return {
        id: targetOrder.id,
        status: true,
        message: '成功刪除'
      };
    }

    return {
      id: targetOrder.id,
      status: false,
      message: '訂單刪除失敗, 請確定該訂單之狀態！'
    };
  })
};
exports.deleteOrderByAdmin = deleteOrderByAdmin;
const deleteOrderByLineBot = {
  type: new _graphql.GraphQLNonNull(_Order.orderDeleteResponseType),
  args: {
    orderId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member.PermissionError(),
    actions: _roleActions.actions.INVENTORY_MANAGE
  })(async (_, args) => {
    const {
      orderId
    } = args;
    const targetOrder = await _db.db.models.Order.findByPk(orderId);
    if (!targetOrder) throw new _General.ResourceNotFoundError();

    if (targetOrder.status !== 'ORDERING') {
      return {
        id: targetOrder.id,
        status: false,
        message: '訂單刪除失敗, 請確定該訂單之狀態！'
      };
    }

    await targetOrder.destroy();
    return {
      id: targetOrder.id,
      status: true,
      message: '成功刪除'
    };
  })
};
exports.deleteOrderByLineBot = deleteOrderByLineBot;
var _default = null;
exports.default = _default;