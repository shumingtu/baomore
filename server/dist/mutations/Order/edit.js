"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.editOrderByAdmin = exports.editOrderByLineBot = exports.assignOrderToEmployee = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _moment = _interopRequireDefault(require("moment"));

var _sequelize = require("sequelize");

var _db = require("../../db");

var _onlineQRCodeGenerator = _interopRequireDefault(require("../../helpers/onlineQRCodeGenerator.js"));

var _roleActions = require("../../../shared/roleActions");

var _Member = require("../../errors/Member");

var _General = require("../../errors/General");

var _Order = require("../../errors/Order");

var _OnlineOrder = require("../../errors/OnlineOrder.js");

var _padDigits = _interopRequireDefault(require("../../helpers/padDigits.js"));

var _invoice = _interopRequireDefault(require("../../helpers/invoice.js"));

var _Order2 = require("../../types/Order");

var _OnlineOrder2 = require("../../types/OnlineOrder");

var _linebot = require("../../helpers/line/linebot.js");

const assignOrderToEmployee = {
  type: new _graphql.GraphQLNonNull(_OnlineOrder2.onlineOrderType),
  args: {
    onlineOrderId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    storeId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    date: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    time: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    employeeId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member.PermissionError(),
    actions: _roleActions.actions.MANAGE_EMPLOYEE_MANAGER
  })(async (_, args) => {
    const {
      onlineOrderId,
      storeId,
      date,
      time,
      employeeId
    } = args;

    const targetOnlineOrderScope = _db.db.models.OnlineOrder.scope('store', 'order', 'bookedMember', 'member');

    const targetOnlineOrder = await targetOnlineOrderScope.findByPk(onlineOrderId, {
      include: [{
        model: _db.db.models.PNTable
      }]
    });
    const assignedMembr = await _db.db.models.Member.findOne({
      where: {
        id: employeeId
      }
    });
    if (!targetOnlineOrder || !assignedMembr) throw new _OnlineOrder.OnlineOrderNotFoundError();

    if (targetOnlineOrder.payedStatus === 'PAIDANDCONFIRMED') {
      throw new _OnlineOrder.OnlineOrderConfirmedError();
    }

    const transaction = await _db.db.transaction();

    try {
      const orderGroupShipment = await _db.db.models.OrderGroupShipment.create({
        SN: (0, _moment.default)().format('YYYYMMDD'),
        status: 'UNSHIPMENTED',
        startDate: targetOnlineOrder.createdAt,
        endDate: new Date(),
        PNTableId: targetOnlineOrder.PNTableId
      }, {
        transaction
      });
      const order = await _db.db.models.Order.create({
        orderSN: `${(0, _moment.default)().format('YYmmssMMDD')}8`,
        quantity: 1,
        picture: targetOnlineOrder.picture,
        status: 'UNDISPATCHED',
        orderSource: 'ONLINEORDER',
        StoreId: storeId,
        PNTableId: targetOnlineOrder.PNTableId,
        ProduceType: 'MATERIAL',
        EmpolyedMemberId: assignedMembr.id,
        OrderGroupShipmentId: orderGroupShipment.id
      }, {
        transaction
      });
      let isTimeDifferent = false;

      if (targetOnlineOrder.date !== date || targetOnlineOrder.time !== time) {
        isTimeDifferent = true;
      }

      await targetOnlineOrder.update({
        StoreId: storeId,
        date,
        time,
        BookedMemberId: assignedMembr.id,
        payedStatus: 'PAIDANDCONFIRMED',
        OrderId: order.id,
        isAssigned: true
      }, {
        transaction
      });
      const qrcodePrefix = `${(0, _moment.default)().format('YYYYMMDD')}${targetOnlineOrder.PNTable.code}`;
      const qrcode = await (0, _onlineQRCodeGenerator.default)(qrcodePrefix);
      await _db.db.models.OrderQRCode.create({
        OrderId: order.id,
        qrcode
      }, {
        transaction
      });
      const result = await (0, _invoice.default)(targetOnlineOrder);
      await order.update({
        invoiceNumber: result.InvoiceNumber,
        invoiceDate: result.InvoiceDate
      }, {
        transaction
      });
      await transaction.commit();
      const updatedOnlineOrder = await targetOnlineOrderScope.findByPk(onlineOrderId, {
        include: [{
          model: _db.db.models.PNTable
        }]
      });
      const director = await updatedOnlineOrder.BookedMember.getMember();

      if (director && director.lineId) {
        await (0, _linebot.sendLineMessage)(null, `【包膜預約行程變更通知】\n線上訂單編號：${updatedOnlineOrder.id}\n訂購人姓名：${updatedOnlineOrder.name}\n訂購人電話：${updatedOnlineOrder.phone}\n預約時間：${updatedOnlineOrder.date} ${updatedOnlineOrder.time}\n預約門市：${updatedOnlineOrder.Store.Channel.name}${updatedOnlineOrder.Store.name}`, director.lineId);
      }

      if (updatedOnlineOrder.Owner.lineId) {
        if (isTimeDifferent || updatedOnlineOrder.Store.id !== targetOnlineOrder.Store.id) {
          // 如果只更動包膜師不用通知客戶
          const HOST = process.env.HOST || 'http://localhost:8108';
          const orderURL = `${HOST}/member/order`;
          await (0, _linebot.sendLineMessageToClient)(`【包膜預約行程變更通知】\n訂單編號：${updatedOnlineOrder.id}\n預約時間：${updatedOnlineOrder.date} ${updatedOnlineOrder.time}\n預約門市：${updatedOnlineOrder.Store.Channel.name}${updatedOnlineOrder.Store.name}\n訂單詳細資訊請至官網會員中心 > 訂單查詢\n訂單查詢傳送門：\n${orderURL}\n如有任何問題請與 artmo 線上小幫手聯繫`, updatedOnlineOrder.Owner.lineId);
        }
      }

      if (updatedOnlineOrder.BookedMember.lineId) {
        await (0, _linebot.sendLineMessage)(null, `【包膜預約行程變更通知】\n線上訂單編號：${updatedOnlineOrder.id}\n訂購人姓名：${updatedOnlineOrder.name}\n訂購人電話：${updatedOnlineOrder.phone}\n預約時間：${updatedOnlineOrder.date} ${updatedOnlineOrder.time}\n預約門市：${updatedOnlineOrder.Store.Channel.name}${updatedOnlineOrder.Store.name}`, updatedOnlineOrder.BookedMember.lineId);
      }

      return targetOnlineOrder;
    } catch (e) {
      await transaction.rollback();
      throw e;
    }
  })
};
exports.assignOrderToEmployee = assignOrderToEmployee;
const editOrderByLineBot = {
  type: new _graphql.GraphQLNonNull(_Order2.orderType),
  args: {
    orderId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    quantity: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    storeId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member.PermissionError(),
    actions: _roleActions.actions.INVENTORY_MANAGE
  })(async (_, args) => {
    const {
      orderId,
      quantity,
      storeId
    } = args;
    const targetOrder = await _db.db.models.Order.scope({
      method: ['memberArea', args]
    }).findByPk(orderId, {
      include: [{
        model: _db.db.models.PNTable.scope('brandAndModel', 'categories')
      }]
    });
    if (!targetOrder) throw new _General.ResourceNotFoundError();

    if (targetOrder.status !== 'ORDERING') {
      throw new _Order.OrderUpdateFailError();
    }

    await targetOrder.update({
      quantity,
      StoreId: storeId
    });
    const newStore = await targetOrder.getStore({
      include: [{
        model: _db.db.models.Channel
      }, {
        model: _db.db.models.District
      }]
    });
    return { ...targetOrder.dataValues,
      Store: newStore
    };
  })
};
exports.editOrderByLineBot = editOrderByLineBot;
const editOrderByAdmin = {
  type: new _graphql.GraphQLNonNull(_Order2.orderType),
  args: {
    orderId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    quantity: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    storeId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member.PermissionError(),
    actions: _roleActions.actions.INVOICING_MANAGE
  })(async (_, args) => {
    const {
      orderId,
      quantity,
      storeId
    } = args;
    const targetOrder = await _db.db.models.Order.scope({
      method: ['memberArea', args]
    }).findByPk(orderId, {
      include: [{
        model: _db.db.models.PNTable.scope('brandAndModel', 'categories', 'vendor')
      }, {
        model: _db.db.models.OrderGroupShipment
      }]
    });
    if (!targetOrder) throw new _General.ResourceNotFoundError();

    if (targetOrder.status === 'ORDERING') {
      await targetOrder.update({
        quantity,
        StoreId: storeId
      });
      const newStore = await targetOrder.getStore({
        include: [{
          model: _db.db.models.Channel
        }, {
          model: _db.db.models.District
        }]
      });
      return { ...targetOrder.dataValues,
        Store: newStore
      };
    }

    if (targetOrder.status === 'UNDISPATCHED') {
      const transaction = await _db.db.transaction();

      try {
        await targetOrder.update({
          quantity,
          StoreId: storeId
        }, {
          transaction
        });

        if (targetOrder.orderSource !== 'ONLINEORDER') {
          const sameGroupQRCodes = await _db.db.models.OrderQRCode.findAll({
            where: {
              qrcode: {
                [_sequelize.Op.like]: `${targetOrder.OrderGroupShipment.SN}${targetOrder.PNTable.code}%`
              }
            }
          }).map(x => x.qrcode.slice(20)); // get last 5 number

          const biggestCode = Math.max(...sameGroupQRCodes);
          await _db.db.models.OrderQRCode.destroy({
            where: {
              OrderId: targetOrder.id
            },
            transaction,
            force: true
          });
          const arr = Array.from(Array(quantity));
          const qrcodeArray = arr.map((x, idx) => ({
            qrcode: `${targetOrder.OrderGroupShipment.SN}${targetOrder.PNTable.code}${(0, _padDigits.default)(idx + 1 + biggestCode, 5)}`,
            OrderId: targetOrder.id
          }));
          await _db.db.models.OrderQRCode.bulkCreate(qrcodeArray, {
            transaction
          });
        }

        await transaction.commit();
      } catch (e) {
        await transaction.rollback();
        throw e;
      }

      const newStore = await targetOrder.getStore({
        include: [{
          model: _db.db.models.Channel
        }, {
          model: _db.db.models.District
        }]
      });
      return { ...targetOrder.dataValues,
        Store: newStore
      };
    }

    throw new _Order.OrderUpdateFailError();
  })
};
exports.editOrderByAdmin = editOrderByAdmin;
var _default = null;
exports.default = _default;