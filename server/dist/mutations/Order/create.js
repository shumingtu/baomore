"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.createOrder = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _Member = require("../../errors/Member");

var _General = require("../../errors/General");

var _Order = require("../../errors/Order");

var _Order2 = require("../../types/Order");

const createOrder = {
  type: new _graphql.GraphQLNonNull(_Order2.orderType),
  args: {
    employeeId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    code: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    quantity: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    productType: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    storeId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member.PermissionError(),
    actions: _roleActions.actions.INVOICING_MANAGE
  })(async (_, args) => {
    const {
      employeeId,
      code,
      quantity,
      productType,
      storeId
    } = args;
    const targetPNTable = await _db.db.models.PNTable.findOne({
      where: {
        code
      }
    });
    if (!targetPNTable) throw new _General.ResourceNotFoundError();
    const createdOrder = await _db.db.models.Order.create({
      EmpolyedMemberId: employeeId,
      quantity,
      ProductType: productType,
      StoreId: storeId,
      picture: targetPNTable.picture,
      PNTableId: targetPNTable.id,
      orderSource: 'EMPOLYEE'
    });
    if (!createdOrder) throw new _Order.OrderCreateFailed();
    const store = await createdOrder.getStore({
      include: [{
        model: _db.db.models.Channel
      }, {
        model: _db.db.models.District
      }]
    });
    const member = await createdOrder.getMember({
      include: [{
        model: _db.db.models.Area
      }]
    });
    const pnTable = await createdOrder.getPNTable({
      scope: ('brandAndModel', 'categories')
    });
    return { ...createdOrder.dataValues,
      Store: store,
      Member: member,
      PNTable: pnTable
    };
  })
};
exports.createOrder = createOrder;
var _default = null;
exports.default = _default;