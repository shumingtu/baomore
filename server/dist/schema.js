"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _list = require("./queries/Vendor/list");

var _list2 = require("./queries/Brand/list");

var _modelList = _interopRequireDefault(require("./queries/Brand/modelList"));

var _colorList = require("./queries/Brand/colorList");

var _list3 = require("./queries/PNTable/subCategory/list");

var _item = require("./queries/PNTable/subCategory/item");

var _list4 = require("./queries/PNTable/category/list");

var _list5 = require("./queries/PNTable/list");

var _list6 = require("./queries/CityDistrict/list");

var _list7 = require("./queries/Store/list");

var _location = require("./queries/Store/location");

var _list8 = require("./queries/BookTime/list");

var _list9 = require("./queries/Emoji/list");

var _me = _interopRequireDefault(require("./queries/Member/me"));

var _list10 = require("./queries/OnlineOrder/list");

var _list11 = require("./queries/Member/list");

var _schedule = require("./queries/OnlineOrder/schedule");

var _list12 = require("./queries/ClosedOrderSetting/list");

var _list13 = require("./queries/Area/list");

var _list14 = require("./queries/OrderGroupShipment/list");

var _checkInfo = _interopRequireDefault(require("./queries/Stock/checkInfo"));

var _list15 = _interopRequireWildcard(require("./queries/Stock/list"));

var _single = _interopRequireDefault(require("./queries/Stock/single"));

var _list16 = require("./queries/Announcement/list");

var _detail = require("./queries/Announcement/detail");

var _schedule2 = require("./queries/Member/schedule");

var _performance = require("./queries/Member/performance");

var _list17 = require("./queries/Banner/list");

var _list18 = require("./queries/Landing/list");

var _list19 = require("./queries/Activity/list");

var _detail2 = require("./queries/Activity/detail");

var _order = _interopRequireDefault(require("./queries/Member/order.js"));

var _list20 = _interopRequireWildcard(require("./queries/BehaviorRecord/list"));

var _list21 = require("./queries/SocialCategory/list.js");

var _performance2 = require("./queries/BehaviorRecord/performance.js");

var _list22 = require("./queries/Channel/list.js");

var _warranties = _interopRequireDefault(require("./queries/Member/warranties"));

var _list23 = require("./queries/Order/list.js");

var _list24 = require("./queries/Role/list.js");

var _list25 = require("./queries/Action/list");

var _create = _interopRequireDefault(require("./mutations/Brand/create"));

var _createModel = _interopRequireDefault(require("./mutations/Brand/createModel"));

var _createModelColor = _interopRequireDefault(require("./mutations/Brand/createModelColor"));

var _edit = _interopRequireDefault(require("./mutations/Brand/edit"));

var _editModel = _interopRequireDefault(require("./mutations/Brand/editModel"));

var _editModelColor = _interopRequireDefault(require("./mutations/Brand/editModelColor"));

var _delete = _interopRequireDefault(require("./mutations/Brand/delete"));

var _deleteModel = _interopRequireDefault(require("./mutations/Brand/deleteModel"));

var _deleteModelColor = _interopRequireDefault(require("./mutations/Brand/deleteModelColor"));

var _confirm = _interopRequireDefault(require("./mutations/Pay/confirm"));

var _storeJSON = _interopRequireDefault(require("./mutations/Member/storeJSON"));

var _makeOnlineOrder = _interopRequireDefault(require("./mutations/Pay/makeOnlineOrder"));

var _delete2 = _interopRequireDefault(require("./mutations/PNTable/delete"));

var _create2 = _interopRequireDefault(require("./mutations/PNTable/create"));

var _edit2 = _interopRequireDefault(require("./mutations/PNTable/edit"));

var _create3 = _interopRequireDefault(require("./mutations/PNCategory/create"));

var _create4 = _interopRequireDefault(require("./mutations/PNSubCategory/create"));

var _edit3 = _interopRequireDefault(require("./mutations/PNCategory/edit"));

var _edit4 = _interopRequireDefault(require("./mutations/PNSubCategory/edit"));

var _delete3 = _interopRequireDefault(require("./mutations/PNCategory/delete"));

var _delete4 = _interopRequireDefault(require("./mutations/PNSubCategory/delete"));

var _create5 = _interopRequireDefault(require("./mutations/ClosedOrderSetting/create"));

var _edit5 = _interopRequireDefault(require("./mutations/ClosedOrderSetting/edit"));

var _delete5 = _interopRequireDefault(require("./mutations/ClosedOrderSetting/delete"));

var _update = _interopRequireDefault(require("./mutations/OrderGroupShipment/update"));

var _lineBotLogin = _interopRequireDefault(require("./mutations/Member/lineBotLogin"));

var _check = _interopRequireDefault(require("./mutations/Stock/check"));

var _sale = require("./mutations/Stock/sale");

var _purchase = _interopRequireDefault(require("./mutations/Stock/purchase"));

var _transfer = _interopRequireDefault(require("./mutations/Stock/transfer"));

var _rollback = _interopRequireWildcard(require("./mutations/Stock/rollback"));

var _punch = _interopRequireDefault(require("./mutations/Member/punch"));

var _create6 = _interopRequireDefault(require("./mutations/Landing/create"));

var _edit6 = _interopRequireDefault(require("./mutations/Landing/edit"));

var _delete6 = _interopRequireDefault(require("./mutations/Landing/delete"));

var _create7 = _interopRequireDefault(require("./mutations/Banner/create"));

var _edit7 = _interopRequireDefault(require("./mutations/Banner/edit"));

var _delete7 = _interopRequireDefault(require("./mutations/Banner/delete"));

var _createCategory = _interopRequireDefault(require("./mutations/Activity/createCategory"));

var _editCategory = _interopRequireDefault(require("./mutations/Activity/editCategory"));

var _deleteCategory = _interopRequireDefault(require("./mutations/Activity/deleteCategory"));

var _create8 = _interopRequireDefault(require("./mutations/Activity/create"));

var _edit8 = _interopRequireWildcard(require("./mutations/Activity/edit"));

var _delete8 = _interopRequireDefault(require("./mutations/Activity/delete"));

var _edit9 = _interopRequireDefault(require("./mutations/SocialCategory/edit"));

var _create9 = _interopRequireDefault(require("./mutations/Member/create"));

var _delete9 = _interopRequireDefault(require("./mutations/Member/delete"));

var _edit10 = require("./mutations/Member/edit.js");

var _create10 = _interopRequireDefault(require("./mutations/Store/create.js"));

var _edit11 = _interopRequireWildcard(require("./mutations/Store/edit.js"));

var _delete10 = _interopRequireDefault(require("./mutations/Store/delete.js"));

var _create11 = _interopRequireDefault(require("./mutations/Vendor/create.js"));

var _edit12 = _interopRequireDefault(require("./mutations/Vendor/edit.js"));

var _delete11 = _interopRequireDefault(require("./mutations/Vendor/delete.js"));

var _managerPunch = _interopRequireDefault(require("./mutations/Member/managerPunch"));

var _changeSchedule = _interopRequireDefault(require("./mutations/Member/changeSchedule"));

var _cancelSchedule = _interopRequireDefault(require("./mutations/Member/cancelSchedule"));

var _verify = _interopRequireDefault(require("./mutations/BehaviorRecord/verify.js"));

var _create12 = _interopRequireDefault(require("./mutations/Announcement/create"));

var _edit13 = _interopRequireDefault(require("./mutations/Announcement/edit"));

var _delete12 = _interopRequireDefault(require("./mutations/Announcement/delete"));

var _addWarrantyRecord = _interopRequireDefault(require("./mutations/Member/addWarrantyRecord"));

var _delete13 = require("./mutations/Order/delete");

var _edit14 = require("./mutations/Order/edit");

var _create13 = require("./mutations/Order/create");

var _edit15 = _interopRequireDefault(require("./mutations/Role/edit.js"));

// queries
// mutations
// The GraphQL schema
var _default = new _graphql.GraphQLSchema({
  description: 'root schema',
  query: new _graphql.GraphQLObjectType({
    name: 'Query',
    fields: {
      brandList: _list2.brandList,
      brandModelList: _modelList.default,
      brandModelColorList: _colorList.brandModelColorList,
      pnCategories: _list4.listForPNCategory,
      pnCategoriesByKeyword: _list4.listPNCatgoryFilterByKeyword,
      subCategoriesByPNCategory: _list3.listFilterByPNCategory,
      subCategoriesForSale: _list3.listForSale,
      subCategoriesBySubCategory: _list3.listFilterByPNSubCategory,
      pntablelistBySubcategory: _list5.listFilterBySubCategory,
      pntableList: _list5.listFilterByAdmin,
      vendorList: _list.listVendor,
      cityDistricts: _list6.cityDistrictList,
      districts: _list6.districtList,
      availableBookedDates: _list8.availableBookedDates,
      availableBookedTimes: _list8.availableBookedTimes,
      alreadyBookedDates: _list8.alreadyBookedDates,
      emojiCategories: _list9.emojiCategories,
      emoji: _list9.emoji,
      me: _me.default,
      adminBrandModelColorList: _list2.adminBrandModelColorList,
      brandModelColor: _colorList.brandModelColor,
      onlineOrderlist: _list10.onlineOrderlist,
      employeeMemberlist: _list11.employeeMemberlist,
      stores: _list7.list,
      bookTimeSlotList: _list8.bookTimeSlotList,
      schedulelist: _schedule.schedulelist,
      pnTableItem: _list5.pnTableItem,
      pnTableCodeList: _list5.listForPNTableCode,
      pnSubCategoryItem: _item.pnSubCategoryItem,
      closedOrderSettings: _list12.listClosedOrderSetting,
      singleClosedOrderSetting: _list12.itemClosedOrderSetting,
      areaList: _list13.listArea,
      orderGroupShipmentList: _list14.listOrderGroupShipment,
      checkInfo: _checkInfo.default,
      inventories: _list15.default,
      meInventory: _single.default,
      punchlist: _list11.punchlist,
      announcements: _list16.announcements,
      announcement: _detail.announcement,
      meScheduleDates: _schedule2.meScheduleDates,
      meScheduleDateAssignments: _schedule2.meScheduleDateAssignments,
      performance: _performance.performance,
      banners: _list17.banners,
      landings: _list18.landings,
      activityCategories: _list19.activityCategories,
      activityCategory: _detail2.activityCategory,
      activities: _list19.activities,
      activity: _detail2.activity,
      myOrderList: _order.default,
      socialCategories: _list21.socialCategories,
      employeePerformanceForAdmin: _performance2.employeePerformanceForAdmin,
      storePerformanceForAdmin: _performance2.storePerformanceForAdmin,
      onlineOrder: _list10.onlineOrder,
      behaviorRecordList: _list20.default,
      myWarranties: _warranties.default,
      channelList: _list22.listChannel,
      orderList: _list23.orderList,
      orderListByEmployee: _list23.orderListByEmployee,
      stockList: _list15.stockList,
      recordList: _list20.recordList,
      behaviorList: _list20.behaviorList,
      roleList: _list24.roleList,
      actionList: _list25.actionList,
      allMemberList: _list11.allMemberList,
      orderQRCodeList: _list23.orderQRCodeList,
      customPNTable: _list5.customPNTable,
      locationInfo: _location.locationInfo,
      adminPunchList: _list11.adminPunchList
    }
  }),
  mutation: new _graphql.GraphQLObjectType({
    name: 'Mutation',
    fields: {
      createBrand: _create.default,
      createBrandModel: _createModel.default,
      createBrandModelColor: _createModelColor.default,
      editBrand: _edit.default,
      editBrandModel: _editModel.default,
      editBrandModelColor: _editModelColor.default,
      deleteBrand: _delete.default,
      deleteBrandModel: _deleteModel.default,
      deleteBrandModelColor: _deleteModelColor.default,
      payConfirm: _confirm.default,
      storeJSON: _storeJSON.default,
      makeOnlineOrder: _makeOnlineOrder.default,
      deletePNTableItem: _delete2.default,
      createPNTableItem: _create2.default,
      editPNTableItem: _edit2.default,
      createPNCategory: _create3.default,
      createPNSubCategory: _create4.default,
      editPNCategory: _edit3.default,
      editPNSubCategory: _edit4.default,
      deletePNCategory: _delete3.default,
      deletePNSubCategory: _delete4.default,
      createClosedOrderSetting: _create5.default,
      editClosedOrderSetting: _edit5.default,
      deleteClosedOrderSetting: _delete5.default,
      updateGroupOrderStatus: _update.default,
      employeeLogin: _lineBotLogin.default,
      checkInventory: _check.default,
      saleStart: _sale.saleStart,
      saleStop: _sale.saleStop,
      purchase: _purchase.default,
      transfer: _transfer.default,
      inventoryRollback: _rollback.default,
      punch: _punch.default,
      editMe: _edit10.editMe,
      createLanding: _create6.default,
      editLanding: _edit6.default,
      deleteLanding: _delete6.default,
      createBanner: _create7.default,
      editBanner: _edit7.default,
      deleteBanner: _delete7.default,
      createActivityCategory: _createCategory.default,
      editActivityCategory: _editCategory.default,
      deleteActivityCategory: _deleteCategory.default,
      createActivity: _create8.default,
      editActivity: _edit8.default,
      deleteActivity: _delete8.default,
      editSocialCategory: _edit9.default,
      createMember: _create9.default,
      deleteMember: _delete9.default,
      editMemberForAdmin: _edit10.editMemberForAdmin,
      createStore: _create10.default,
      editStore: _edit11.default,
      deleteStore: _delete10.default,
      createVendor: _create11.default,
      editVendor: _edit12.default,
      deleteVendor: _delete11.default,
      managerPunch: _managerPunch.default,
      changeSchedule: _changeSchedule.default,
      cancelSchedule: _cancelSchedule.default,
      verifyBehaviorRecords: _verify.default,
      createAnnouncement: _create12.default,
      editAnnouncement: _edit13.default,
      deleteAnnouncement: _delete12.default,
      addWarrantyRecord: _addWarrantyRecord.default,
      editMemberForConsumer: _edit10.editMemberForConsumer,
      deleteOrderByLineBot: _delete13.deleteOrderByLineBot,
      editOrderByAdmin: _edit14.editOrderByAdmin,
      editOrderByLineBot: _edit14.editOrderByLineBot,
      deleteOrderByAdmin: _delete13.deleteOrderByAdmin,
      createOrder: _create13.createOrder,
      editRole: _edit15.default,
      assignOrderToEmployee: _edit14.assignOrderToEmployee,
      toggleActivity: _edit8.toggleActivity,
      rollBackForCustomer: _rollback.rollBackForCustomer,
      toggleMemberArchive: _edit10.toggleMemberArchive,
      toggleStoreArchive: _edit11.toggleStoreArchive
    }
  })
});

exports.default = _default;