"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _koaRouter = _interopRequireDefault(require("koa-router"));

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _moment = _interopRequireDefault(require("moment"));

var _lodash = _interopRequireDefault(require("lodash"));

var _db = require("../db");

var _Export = require("../errors/Export.js");

/* eslint-disable max-len */
const exportRouter = new _koaRouter.default({
  prefix: '/export'
});
const ADMIN_HOST = process.env.ADMIN_HOST || 'http://localhost:2113'; // exportRouter.get('/getDuplicatedTransfer', async (ctx) => {
//   const records = await db.models.BehaviorRecord.findAll({
//     attributes: ['id', 'qrcode', 'TransferRecordId'],
//     where: {
//       TransferRecordId: {
//         [Op.ne]: null,
//       },
//     },
//   }).map(x => x.TransferRecordId);
//
//   const duplicateArray = [];
//
//   records.reduce((prev, curr) => {
//     if (prev.includes(curr)) {
//       duplicateArray.push(curr);
//     }
//     return [...prev, curr];
//   }, []);
//
//   ctx.body = {
//     duplicateArray,
//   };
// });
//
// exportRouter.get('/getRollbackFailed', async (ctx) => {
//   const records = await db.models.BehaviorRecord.findAll({
//     attributes: ['id', 'MemberId', 'createdAt', 'quantity', 'PNTableId'],
//     where: {
//       BehaviorId: 5,
//       ApproverId: null,
//       rollbackType: {
//         [Op.ne]: 'CUSTOMERCOMPLAINT',
//       },
//     },
//     include: [{
//       model: db.models.Member,
//       attributes: ['id', 'name', 'serialNumber'],
//     }, {
//       model: db.models.PNTable,
//       attributes: ['id', 'code'],
//     }],
//   });
//
//   const result = await Promise.all(_(records).groupBy(x => x.MemberId)
//     .map(async (value, memberId) => {
//       const groupByPNTableArray = _(value).groupBy(v => v.PNTableId);
//       const rollbackData = await Promise.all(groupByPNTableArray.map(async (pnValue, pnTableId) => {
//         const remainStocks = await db.models.MemberStock.findOne({
//           attributes: ['storageCount'],
//           where: {
//             PNTableId: pnTableId,
//             MemberId: memberId,
//           },
//         });
//         return {
//           pnTableId,
//           code: pnValue[0].PNTable.code,
//           rollbackQuantitySum: _.sumBy(pnValue, p => p.quantity),
//           remainStocks: remainStocks.storageCount,
//         };
//       }));
//       return {
//         memberId,
//         serialNumber: value[0].Member.serialNumber,
//         rollback: rollbackData.filter(z => z.rollbackQuantitySum > z.remainStocks),
//       };
//     }).value());
//
//   ctx.status = 200;
//   ctx.body = {
//     result: result.filter(r => r.rollback.length),
//   };
// });

exportRouter.get('/e01', async ctx => {
  if (!ctx.req.member) {
    (0, _Export.PERMISSION_ERROR)(ctx);
    return;
  }

  const {
    startDate,
    endDate,
    orderSN,
    vendorId,
    memberId,
    areaId,
    shippingStatus,
    code
  } = ctx.query;
  const args = {
    startDate: startDate || null,
    endDate: endDate || null,
    orderSN: orderSN || null,
    shippingStatus: shippingStatus || null,
    vendorId: parseInt(vendorId, 10) || null,
    memberId: parseInt(memberId, 10) || null,
    areaId: parseInt(areaId, 10) || null,
    code: code || null
  };
  const orderSQL = args.orderSN ? `AND Orders.orderSN = ${args.orderSN}` : '';
  const whereClause = [_sequelize.default.literal(`EXISTS (SELECT tmp.id FROM OrderGroupShipments AS tmp INNER JOIN Orders ON Orders.OrderGroupShipmentId = tmp.id WHERE tmp.id = OrderGroupShipment.id AND Orders.orderSource != 'TRANSFER' ${orderSQL})`)];

  if (args.shippingStatus) {
    whereClause.push({
      status: args.shippingStatus
    });
  }

  if (args.startDate) {
    whereClause.push({
      createdAt: {
        [_sequelize.Op.and]: [{
          [_sequelize.Op.gte]: (0, _moment.default)(args.startDate)
        }, {
          [_sequelize.Op.lt]: args.endDate && (0, _moment.default)(args.endDate).add(1, 'd') || (0, _moment.default)().add(1, 'd')
        }]
      }
    });
  }

  if (args.endDate && !args.startDate) {
    whereClause.push({
      createdAt: {
        [_sequelize.Op.lt]: (0, _moment.default)(args.endDate).add(1, 'd')
      }
    });
  }

  const orderGroupShipments = await _db.db.models.OrderGroupShipment.scope({
    method: ['pnTableAllAssoicateAndOrder', args]
  }).findAll({
    where: {
      [_sequelize.Op.and]: whereClause
    },
    order: [['createdAt', 'DESC']]
  });

  if (!orderGroupShipments) {
    (0, _Export.SERVER_ERROR)(ctx);
    return;
  }

  const rawCsvData = orderGroupShipments.map(x => x.Orders.map(order => [order.Member.serialNumber, order.orderSN, (0, _moment.default)(order.createdAt).format('YYYY-MM-DD'), x.PNTable && x.PNTable.code, x.PNTable && x.PNTable.name, order.quantity, order.invoiceNumber, (0, _moment.default)(order.invoiceDate).format('YYYY-MM-DD')].join(',')));

  const csvData = _lodash.default.flatten(rawCsvData).join('\n').replace(/^/, '包膜師工號,訂單編號,下單日期,料號,品名,訂購數量,發票號碼,發票日期\n'); // fixed MS Excel BOM Bug


  const msExcelBuffer = Buffer.concat([Buffer.from('\xEF\xBB\xBF', 'binary'), Buffer.from(csvData)]);
  ctx.set('Content-Description', 'File Transfer');
  ctx.set('Content-Type', 'text/csv');
  ctx.set('Content-Disposition', `attachment; filename="erpe01${(0, _moment.default)().format('YYYYMMDD')}.csv"`);
  ctx.body = msExcelBuffer;
});
exportRouter.get('/e02', async ctx => {
  if (!ctx.req.member) {
    (0, _Export.PERMISSION_ERROR)(ctx);
    return;
  }

  const {
    startDate,
    endDate,
    orderSN,
    memberId
  } = ctx.query;
  const args = {
    startDate: startDate || null,
    endDate: endDate || null,
    orderSN: orderSN || null,
    memberId: parseInt(memberId, 10) || null
  };
  const whereClause = {
    BehaviorId: 2
  };

  if (args.startDate) {
    whereClause.createdAt = {
      [_sequelize.Op.and]: [{
        [_sequelize.Op.gte]: (0, _moment.default)(args.startDate)
      }, {
        [_sequelize.Op.lt]: args.endDate && (0, _moment.default)(args.endDate).add(1, 'd') || (0, _moment.default)().add(1, 'd')
      }]
    };
  }

  if (args.endDate && !args.startDate) {
    whereClause.createdAt = {
      [_sequelize.Op.lt]: (0, _moment.default)(args.endDate).add(1, 'd')
    };
  }

  if (args.orderSN) whereClause.orderSN = args.orderSN;
  if (args.memberId) whereClause.MemberId = args.memberId;
  const checkInventoryRecord = await _db.db.models.BehaviorRecord.findAll({
    attributes: ['id', 'createdAt', 'remark', 'PNTableId', 'MemberId', 'orderSN', 'quantity'],
    where: whereClause,
    include: [{
      attributes: ['id', 'code', 'name'],
      model: _db.db.models.PNTable,
      required: true
    }, {
      attributes: ['id', 'serialNumber'],
      model: _db.db.models.Member,
      required: true
    }]
  });

  if (!checkInventoryRecord) {
    (0, _Export.SERVER_ERROR)(ctx);
    return;
  }

  const csvData = checkInventoryRecord.map(record => [(0, _moment.default)(record.createdAt).format('YYYY-MM-DD'), record.Member.serialNumber, record.PNTable.code, record.PNTable.name, record.quantity, record.orderSN].join(',')).join('\n').replace(/^/, '點貨日期,異動對象,料號,品名,數量,訂單編號\n'); // fixed MS Excel BOM Bug

  const msExcelBuffer = Buffer.concat([Buffer.from('\xEF\xBB\xBF', 'binary'), Buffer.from(csvData)]);
  ctx.set('Content-Description', 'File Transfer');
  ctx.set('Content-Type', 'text/csv');
  ctx.set('Content-Disposition', `attachment; filename="erpe02${(0, _moment.default)().format('YYYYMMDD')}.csv"`);
  ctx.body = msExcelBuffer;
});
exportRouter.get('/e03', async ctx => {
  if (!ctx.req.member) {
    (0, _Export.PERMISSION_ERROR)(ctx);
    return;
  }

  const {
    startDate,
    endDate,
    orderSN,
    memberId
  } = ctx.query;
  const args = {
    startDate: startDate || null,
    endDate: endDate || null,
    orderSN: orderSN || null,
    memberId: parseInt(memberId, 10) || null
  };
  const whereClause = {
    BehaviorId: 1
  };

  if (args.startDate) {
    whereClause.createdAt = {
      [_sequelize.Op.and]: [{
        [_sequelize.Op.gte]: (0, _moment.default)(args.startDate)
      }, {
        [_sequelize.Op.lt]: args.endDate && (0, _moment.default)(args.endDate).add(1, 'd') || (0, _moment.default)().add(1, 'd')
      }]
    };
  }

  if (args.endDate && !args.startDate) {
    whereClause.createdAt = {
      [_sequelize.Op.lt]: (0, _moment.default)(args.endDate).add(1, 'd')
    };
  }

  if (args.orderSN) whereClause.orderSN = args.orderSN;
  if (args.memberId) whereClause.MemberId = args.memberId;
  const checkInventoryRecord = await _db.db.models.BehaviorRecord.findAll({
    attributes: ['id', 'endTime', 'remark', 'PNTableId', 'MemberId', 'orderSN'],
    where: whereClause,
    include: [{
      attributes: ['id', 'code', 'name'],
      model: _db.db.models.PNTable,
      required: true
    }, {
      attributes: ['id', 'serialNumber'],
      model: _db.db.models.Member,
      required: true
    }]
  });

  if (!checkInventoryRecord) {
    (0, _Export.SERVER_ERROR)(ctx);
    return;
  }

  const csvData = checkInventoryRecord.map(record => [record.Member.serialNumber, record.orderSN, (0, _moment.default)(record.endTime).format('YYYY-MM-DD'), record.PNTable.code, record.PNTable.name, 1].join(',')).join('\n').replace(/^/, '包膜師工號,訂單編號,銷貨日期,料號,品名,數量\n'); // fixed MS Excel BOM Bug

  const msExcelBuffer = Buffer.concat([Buffer.from('\xEF\xBB\xBF', 'binary'), Buffer.from(csvData)]);
  ctx.set('Content-Description', 'File Transfer');
  ctx.set('Content-Type', 'text/csv');
  ctx.set('Content-Disposition', `attachment; filename="erpe03${(0, _moment.default)().format('YYYYMMDD')}.csv"`);
  ctx.body = msExcelBuffer;
});
exportRouter.get('/e04', async ctx => {
  if (!ctx.req.member) {
    (0, _Export.PERMISSION_ERROR)(ctx);
    return;
  }

  const {
    startDate,
    endDate,
    orderSN,
    memberId
  } = ctx.query;
  const args = {
    startDate: startDate || null,
    endDate: endDate || null,
    orderSN: orderSN || null,
    memberId: parseInt(memberId, 10) || null
  };
  const whereClause = {
    [_sequelize.Op.or]: [{
      BehaviorId: 4
    }, {
      BehaviorId: 5,
      ApproverId: {
        [_sequelize.Op.ne]: null
      }
    }]
  };

  if (args.startDate) {
    whereClause.createdAt = {
      [_sequelize.Op.and]: [{
        [_sequelize.Op.gte]: (0, _moment.default)(args.startDate)
      }, {
        [_sequelize.Op.lt]: args.endDate && (0, _moment.default)(args.endDate).add(1, 'd') || (0, _moment.default)().add(1, 'd')
      }]
    };
  }

  if (args.endDate && !args.startDate) {
    whereClause.createdAt = {
      [_sequelize.Op.lt]: (0, _moment.default)(args.endDate).add(1, 'd')
    };
  }

  if (args.orderSN) whereClause.orderSN = args.orderSN;
  if (args.memberId) whereClause.MemberId = args.memberId;
  const checkInventoryRecord = await _db.db.models.BehaviorRecord.findAll({
    attributes: ['id', 'createdAt', 'remark', 'PNTableId', 'MemberId', 'TransferRecordId', 'BehaviorId', 'orderSN', 'newOrderSN', 'quantity', 'rollbackType'],
    where: whereClause,
    include: [{
      attributes: ['id', 'code', 'name'],
      model: _db.db.models.PNTable,
      required: true
    }, {
      attributes: ['id', 'serialNumber'],
      model: _db.db.models.Member,
      required: true
    }, {
      model: _db.db.models.BehaviorRecord,
      as: 'TransferRecord',
      include: [{
        attributes: ['id', 'serialNumber'],
        model: _db.db.models.Member,
        required: true
      }]
    }]
  });

  if (!checkInventoryRecord) {
    (0, _Export.SERVER_ERROR)(ctx);
    return;
  }

  const filterResult = checkInventoryRecord.filter(record => record.BehaviorId === 5 || record.BehaviorId === 4 && record.TransferRecord);
  const csvData = filterResult.map(record => {
    const isTransfer = record.BehaviorId === 4;
    const newLineReg = /\n/g;
    return [(0, _moment.default)(record.createdAt).format('YYYY-MM-DD'), isTransfer ? record.TransferRecord.Member.serialNumber : record.Member.serialNumber, isTransfer ? record.Member.serialNumber : '-', '', record.PNTable.code || '', record.PNTable.name || '', record.quantity || '', `${record.remark}`.replace(newLineReg, '') || '', record.orderSN || '', record.newOrderSN || '', record.rollbackType || '', ''].join(',');
  }).join('\n').replace(/^/, '單據日期,對象代碼(原包膜師客代),調撥對象代碼,調撥部門代號,料號,品名,數量,備註,訂單編號,調貨訂單編號,退回類型,退回廠商\n'); // fixed MS Excel BOM Bug

  const msExcelBuffer = Buffer.concat([Buffer.from('\xEF\xBB\xBF', 'binary'), Buffer.from(csvData)]);
  ctx.set('Content-Description', 'File Transfer');
  ctx.set('Content-Type', 'text/csv');
  ctx.set('Content-Disposition', `attachment; filename="erpe04${(0, _moment.default)().format('YYYYMMDD')}.csv"`);
  ctx.body = msExcelBuffer;
});
exportRouter.get('/qrcodes', async ctx => {
  if (!ctx.req.member) {
    (0, _Export.PERMISSION_ERROR)(ctx);
    return;
  }

  const {
    startDate,
    endDate,
    orderSN,
    vendorId,
    memberId,
    areaId,
    shippingStatus,
    code
  } = ctx.query;
  const args = {
    startDate: startDate || null,
    endDate: endDate || null,
    orderSN: orderSN || null,
    shippingStatus: shippingStatus || null,
    vendorId: parseInt(vendorId, 10) || null,
    memberId: parseInt(memberId, 10) || null,
    areaId: parseInt(areaId, 10) || null,
    code: code || null
  };
  const vendorWhereClause = {};
  const orderWhereClause = {};
  if (args.vendorId) vendorWhereClause.id = args.vendorId;
  if (args.orderSN) orderWhereClause.orderSN = args.orderSN;
  const orderGroupShipments = await _db.db.models.OrderGroupShipment.scope({
    method: ['search', args]
  }).findAll({
    include: [{
      model: _db.db.models.Order,
      include: [{
        model: _db.db.models.OrderQRCode,
        include: [{
          model: _db.db.models.Order,
          where: orderWhereClause,
          required: true,
          include: [{
            model: _db.db.models.PNTable.scope({
              method: ['search', args]
            }),
            required: true,
            include: [{
              model: _db.db.models.Vendor,
              where: vendorWhereClause
            }]
          }, {
            model: _db.db.models.Member.scope({
              method: ['search', args]
            }),
            required: true
          }]
        }]
      }]
    }]
  });

  if (!orderGroupShipments) {
    (0, _Export.SERVER_ERROR)(ctx);
    return;
  }

  const allOrderQRCodes = _lodash.default.flattenDeep(orderGroupShipments.map(x => x.Orders.map(y => y.OrderQRCodes)));

  const csvData = allOrderQRCodes.map(qrcode => [qrcode.Order.Member.serialNumber, qrcode.Order.Member.name, qrcode.Order.orderSN, qrcode.Order.PNTable.code, qrcode.Order.PNTable.name, qrcode.qrcode, `${ADMIN_HOST}/linebot/qrcode?code=${qrcode.qrcode}`].join(',')).join('\n').replace(/^/, '包膜師工號,包膜師姓名,訂單編號,料號,品名,商品序號,QRCode內容\n'); // fixed MS Excel BOM Bug

  const msExcelBuffer = Buffer.concat([Buffer.from('\xEF\xBB\xBF', 'binary'), Buffer.from(csvData)]);
  ctx.set('Content-Description', 'File Transfer');
  ctx.set('Content-Type', 'text/csv');
  ctx.set('Content-Disposition', `attachment; filename="qrcode${(0, _moment.default)().format('YYYYMMDD')}.csv"`);
  ctx.body = msExcelBuffer;
});
exportRouter.get('/sendingInfo', async ctx => {
  if (!ctx.req.member) {
    (0, _Export.PERMISSION_ERROR)(ctx);
    return;
  }

  const {
    startDate,
    endDate,
    orderSN,
    vendorId,
    memberId,
    areaId,
    shippingStatus,
    code
  } = ctx.query;

  if (!vendorId) {
    (0, _Export.SERVER_ERROR)(ctx);
    return;
  }

  const args = {
    startDate: startDate || null,
    endDate: endDate || null,
    orderSN: orderSN || null,
    shippingStatus: shippingStatus || null,
    vendorId: parseInt(vendorId, 10) || null,
    memberId: parseInt(memberId, 10) || null,
    areaId: parseInt(areaId, 10) || null,
    code: code || null
  };
  const vendorWhereClause = {};
  const orderWhereClause = {
    orderSource: {
      [_sequelize.Op.ne]: 'TRANSFER'
    }
  };
  if (args.vendorId) vendorWhereClause.id = args.vendorId;
  if (args.orderSN) orderWhereClause.orderSN = args.orderSN;
  const orderGroupShipments = await _db.db.models.OrderGroupShipment.scope({
    method: ['search', args]
  }).findAll({
    include: [{
      attributes: ['id', 'createdAt', 'StoreId', 'EmpolyedMemberId'],
      model: _db.db.models.Order,
      where: orderWhereClause,
      include: [{
        attributes: ['id', 'name', 'address', 'AreaId', 'ChannelId'],
        model: _db.db.models.Store.scope('area', 'channel'),
        required: true
      }, {
        attributes: ['id', 'name', 'phone'],
        model: _db.db.models.Member.scope({
          method: ['search', args]
        }),
        required: true
      }, {
        model: _db.db.models.PNTable.scope({
          method: ['search', args]
        }),
        required: true,
        include: [{
          model: _db.db.models.Vendor,
          where: vendorWhereClause
        }]
      }]
    }]
  });

  if (!orderGroupShipments) {
    (0, _Export.SERVER_ERROR)(ctx);
    return;
  }

  const results = _lodash.default.flattenDeep(orderGroupShipments.map(x => x.Orders));

  const csvResult = results.map(order => [(0, _moment.default)(order.createdAt).format('MM/DD/YYYY HH:mm:ss'), order.Store.Area.name, order.Store.name, order.Store.Channel.name, order.Member.name, order.Member.phone, order.Store.address].join(',')).join('\n').replace(/^/, '時間戳記,區域,門市名稱,通路,收件人/包膜師,聯絡電話,寄件地址\n'); // fixed MS Excel BOM Bug

  const msExcelBuffer = Buffer.concat([Buffer.from('\xEF\xBB\xBF', 'binary'), Buffer.from(csvResult)]);
  ctx.set('Content-Description', 'File Transfer');
  ctx.set('Content-Type', 'text/csv');
  ctx.set('Content-Disposition', `attachment; filename="vendoraddr${(0, _moment.default)().format('YYYYMMDD')}.csv"`);
  ctx.body = msExcelBuffer;
});
exportRouter.get('/orderDetail', async ctx => {
  if (!ctx.req.member) {
    (0, _Export.PERMISSION_ERROR)(ctx);
    return;
  }

  const {
    startDate,
    endDate,
    orderSN,
    vendorId,
    memberId,
    areaId,
    shippingStatus,
    code
  } = ctx.query;

  if (!vendorId) {
    (0, _Export.SERVER_ERROR)(ctx);
    return;
  }

  const args = {
    startDate: startDate || null,
    endDate: endDate || null,
    orderSN: orderSN || null,
    shippingStatus: shippingStatus || null,
    vendorId: parseInt(vendorId, 10) || null,
    memberId: parseInt(memberId, 10) || null,
    areaId: parseInt(areaId, 10) || null,
    code: code || null
  };
  const vendorWhereClause = {};
  const orderWhereClause = {
    orderSource: {
      [_sequelize.Op.ne]: 'TRANSFER'
    }
  };
  if (args.vendorId) vendorWhereClause.id = args.vendorId;
  if (args.vendorId) vendorWhereClause.id = args.vendorId;
  if (args.orderSN) orderWhereClause.orderSN = args.orderSN;
  const orderGroupShipments = await _db.db.models.OrderGroupShipment.scope({
    method: ['search', args]
  }).findAll({
    include: [{
      attributes: ['id', 'EmpolyedMemberId', 'PNTableId', 'quantity'],
      model: _db.db.models.Order.scope({
        method: ['memberArea', args]
      }),
      where: orderWhereClause,
      include: [{
        model: _db.db.models.PNTable.scope({
          method: ['search', args]
        }),
        required: true,
        include: [{
          model: _db.db.models.Vendor,
          where: vendorWhereClause
        }]
      }]
    }]
  });

  if (!orderGroupShipments) {
    (0, _Export.SERVER_ERROR)(ctx);
    return;
  }

  const results = _lodash.default.flattenDeep(orderGroupShipments.map(x => x.Orders));

  const allPNTables = _lodash.default.uniqBy(results.map(r => r.PNTable), 'id');

  const memberPNTables = (0, _lodash.default)(results).groupBy(m => m.Member.id).map((memberValues, memberKey) => {
    const groupByPNTable = (0, _lodash.default)(memberValues).groupBy(p => p.PNTableId).map((pValues, pKey) => ({
      id: parseInt(pKey, 10),
      name: pValues[0].PNTable.name,
      code: pValues[0].PNTable.code,
      count: _lodash.default.sumBy(pValues, 'quantity')
    })).value();
    return {
      id: parseInt(memberKey, 10),
      name: memberValues[0].Member.name,
      area: memberValues[0].Member.Area.name,
      pnTables: groupByPNTable
    };
  }).value();
  let firstRowStr = '料號,PO單品名';
  let secRowStr = '"",""';
  memberPNTables.forEach(x => {
    firstRowStr += `,${x.area}`;
    secRowStr += `,${x.name}`;
  });
  const csvResult = allPNTables.map(p => {
    const memberCounts = memberPNTables.map(m => {
      const {
        pnTables
      } = m;
      const target = pnTables.find(r => r.id === p.id);
      return target && target.count || 0;
    });
    return [p.code, p.name, ...memberCounts].join(',');
  }).join('\n').replace(/^/, `${firstRowStr}\n${secRowStr}\n`); // fixed MS Excel BOM Bug

  const msExcelBuffer = Buffer.concat([Buffer.from('\xEF\xBB\xBF', 'binary'), Buffer.from(csvResult)]);
  ctx.set('Content-Description', 'File Transfer');
  ctx.set('Content-Type', 'text/csv');
  ctx.set('Content-Disposition', `attachment; filename="vendororder${(0, _moment.default)().format('YYYYMMDD')}.csv"`);
  ctx.body = msExcelBuffer;
});
exportRouter.get('/memberStock', async ctx => {
  if (!ctx.req.member) {
    (0, _Export.PERMISSION_ERROR)(ctx);
    return;
  }

  const {
    memberId,
    keyword
  } = ctx.query;
  const args = {
    memberId: parseInt(memberId, 10) || null,
    keyword: keyword || null
  };
  const result = await _db.db.models.MemberStock.scope({
    method: ['search', args]
  }, {
    method: ['pnTable', args]
  }, 'member').findAll({
    order: [['MemberId', 'DESC']],
    where: {
      storageCount: {
        [_sequelize.Op.ne]: 0
      }
    }
  });
  const csvResult = result.map(stock => [stock.Member && stock.Member.name || '', stock.PNTable && stock.PNTable.name || '', stock.PNTable && stock.PNTable.code || '', stock.storageCount].join(',')).join('\n').replace(/^/, '包膜師,品名,料號,庫存數量\n'); // fixed MS Excel BOM Bug

  const msExcelBuffer = Buffer.concat([Buffer.from('\xEF\xBB\xBF', 'binary'), Buffer.from(csvResult)]);
  ctx.set('Content-Description', 'File Transfer');
  ctx.set('Content-Type', 'text/csv');
  ctx.set('Content-Disposition', `attachment; filename="memberStock${(0, _moment.default)().format('YYYYMMDD')}.csv"`);
  ctx.body = msExcelBuffer;
});
exportRouter.get('/behaviorRecord', async ctx => {
  if (!ctx.req.member) {
    (0, _Export.PERMISSION_ERROR)(ctx);
    return;
  }

  const {
    behaviorId,
    employeeId,
    startDate,
    endDate,
    recordStartDate,
    recordEndDate
  } = ctx.query;
  const args = {
    behaviorId: parseInt(behaviorId, 10) || null,
    employeeId: parseInt(employeeId, 10) || null,
    startDate: startDate || null,
    endDate: endDate || null,
    recordStartDate: recordStartDate || null,
    recordEndDate: recordEndDate || null
  };

  if (!args.recordStartDate || !args.recordEndDate || !args.behaviorId) {
    ctx.status = 400;
    ctx.body = {
      message: '開始時間, 結束時間, 行為類型必填'
    };
    return;
  }

  const result = await _db.db.models.BehaviorRecord.scope({
    method: ['searchForPerformance', args]
  }, 'member', 'order', 'behavior').findAll();
  const csvResult = result.map(record => [record.Behavior && record.Behavior.name || '', record.Member && record.Member.name || '', `${record.remark}`.replace(/\n/g, '') || '', record.qrcode && `』${record.qrcode}` || '', record.orderSN || '', record.startTime && (0, _moment.default)(record.startTime).format('YYYY-MM-DD HH:mm') || '', record.endTime && (0, _moment.default)(record.endTime).format('YYYY-MM-DD HH:mm') || '', record.price || '', (0, _moment.default)(record.createdAt).format('YYYY-MM-DD HH:mm')].join(',')).join('\n').replace(/^/, '行為類型,包膜師,備註,商品序號,訂單編號,銷貨開始時間,銷貨結束時間,價格,紀錄時間\n'); // fixed MS Excel BOM Bug

  const msExcelBuffer = Buffer.concat([Buffer.from('\xEF\xBB\xBF', 'binary'), Buffer.from(csvResult)]);
  ctx.set('Content-Description', 'File Transfer');
  ctx.set('Content-Type', 'text/csv');
  ctx.set('Content-Disposition', `attachment; filename="behaviorRecord${(0, _moment.default)().format('YYYYMMDD')}.csv"`);
  ctx.body = msExcelBuffer;
});
exportRouter.get('/pnTable', async ctx => {
  if (!ctx.req.member) {
    (0, _Export.PERMISSION_ERROR)(ctx);
    return;
  }

  const {
    code,
    pnCategoryId,
    bigSubCategoryId,
    smallSubCategoryId,
    modelId,
    brandId,
    vendorId,
    keyword
  } = ctx.query;
  const args = {
    code: code ? `${code}` : null,
    pnCategoryId: pnCategoryId === '-1' ? null : parseInt(pnCategoryId, 10),
    bigSubCategoryId: bigSubCategoryId === '-1' ? null : parseInt(bigSubCategoryId, 10),
    smallSubCategoryId: smallSubCategoryId === '-1' ? null : parseInt(smallSubCategoryId, 10),
    brandId: brandId === '-1' ? null : parseInt(brandId, 10),
    modelId: modelId === '-1' ? null : parseInt(modelId, 10),
    keyword: keyword ? `${keyword}` : null,
    vendorId: vendorId === '-1' ? null : parseInt(vendorId, 10)
  };
  const result = await _db.db.models.PNTable.scope({
    method: ['search', args]
  }, {
    method: ['vendor', args.vendorId]
  }, {
    method: ['categories', args.pnCategoryId, args.bigSubCategoryId, args.smallSubCategoryId]
  }, {
    method: ['brandAndModel', args.brandId, args.modelId]
  }).findAll();
  const csvResult = result.map(record => {
    const isHaveSmallCategory = record.PNSubCategory.PNSubCategory;
    return [record.code || '', record.PNSubCategory && record.PNSubCategory.PNCategory && record.PNSubCategory.PNCategory.name || '', isHaveSmallCategory ? record.PNSubCategory.PNSubCategory.name : record.PNSubCategory.name, isHaveSmallCategory ? record.PNSubCategory.name : '', record.name, record.price || '', record.onlinePrice || '無線上價格', record.Vendor.name].join(',');
  }).join('\n').replace(/^/, '料號編號,料號類型,大類,小類,品名,價格,線上價格,廠商\n'); // fixed MS Excel BOM Bug

  const msExcelBuffer = Buffer.concat([Buffer.from('\xEF\xBB\xBF', 'binary'), Buffer.from(csvResult)]);
  ctx.set('Content-Description', 'File Transfer');
  ctx.set('Content-Type', 'text/csv');
  ctx.set('Content-Disposition', `attachment; filename="pnTable${(0, _moment.default)().format('YYYYMMDD')}.csv"`);
  ctx.body = msExcelBuffer;
}); // copy from admin

function remarksGetter(remarks) {
  const remarkCache = {};

  if (remarks && Array.isArray(remarks)) {
    remarks.forEach(remark => {
      if (remark.startsWith('發票號碼')) {
        remarkCache.vat = remark.replace(/發票號碼：/g, '');
      }

      if (remark.startsWith('客戶姓名')) {
        remarkCache.customerName = remark.replace(/客戶姓名：/g, '');
      }

      if (remark.startsWith('手機號碼')) {
        remarkCache.customerPhone = remark.replace(/手機號碼：/g, '');
      }

      if (remark.startsWith('異常類型')) {
        remarkCache.errorType = remark.replace(/異常類型：/g, '');
      }

      if (remark.startsWith('異常說明')) {
        remarkCache.errorDesc = remark.replace(/異常說明：/g, '');
      }

      if (remark.startsWith('對象')) {
        remarkCache.target = remark.replace(/對象：/g, '');
      }

      if (remark.startsWith('說明')) {
        remarkCache.socialDesc = remark.replace(/說明：/g, '');
      }
    });
  }

  return remarkCache;
}

function rollbackTableDataFormatter(recordList) {
  if (!recordList) return [];
  return recordList.map(r => {
    const remarks = r.remark && r.remark.split('***') || [];
    const remarkCache = remarksGetter(remarks);
    return {
      id: r.id,
      rollbackType: r.rollbackType || '',
      picture: r.picture || '',
      createdAt: (0, _moment.default)(r.createdAt).format('YYYY-MM-DD HH:mm') || '',
      memberName: r.Member && r.Member.name || '',
      vendorName: r.Vendor && r.Vendor.name || '',
      storeName: r.Store && r.Store.name || '',
      pnTableCode: r.PNTable && r.PNTable.code || '',
      pnTableName: r.PNTable && r.PNTable.name || '',
      pnCategoryName: r.PNTable && r.PNTable.PNSubCategory && r.PNTable.PNSubCategory.PNCategory.name || '',
      approverName: r.Approver && r.Approver.name || '',
      errorType: remarkCache.errorType || '',
      vat: remarkCache.vat || '',
      customerName: remarkCache.customerName || '',
      customerPhone: remarkCache.customerPhone || '',
      target: remarkCache.target || '',
      socialDesc: remarkCache.socialDesc || ''
    };
  });
}

exportRouter.get('/rollback/:type', async ctx => {
  if (!ctx.req.member) {
    (0, _Export.PERMISSION_ERROR)(ctx);
    return;
  }

  const {
    type
  } = ctx.params;
  const {
    code,
    pnCategoryId,
    startDate,
    endDate,
    memberId,
    errorType,
    target,
    storeId,
    vat,
    customerName,
    customerPhone,
    rollbackType
  } = ctx.query;
  const args = {
    rollbackType,
    pnCategoryId: pnCategoryId && pnCategoryId !== '-1' ? parseInt(pnCategoryId, 10) : null,
    startDate: startDate || null,
    endDate: endDate || null,
    code: code || null,
    memberId: memberId && memberId !== '-1' ? parseInt(memberId, 10) : null,
    errorType: errorType && errorType !== '-1' ? errorType : null,
    target: target || null,
    storeId: storeId && storeId !== '-1' ? parseInt(storeId, 10) : null,
    vat: vat || null,
    customerName: customerName || null,
    customerPhone: customerPhone || null
  };

  if (!args.startDate || !args.endDate) {
    ctx.status = 400;
    ctx.body = {
      message: '開始時間, 結束時間必填'
    };
    return;
  }

  const rawResults = await _db.db.models.BehaviorRecord.scope({
    method: ['search', args]
  }, {
    method: ['pnTable', args.code, args.pnCategoryId]
  }, 'member', 'order', {
    method: ['vendor', false]
  }, {
    method: ['store', false]
  }, {
    method: ['approver', false]
  }).findAll();
  const results = rollbackTableDataFormatter(rawResults);
  let csvResult = [];

  switch (rollbackType) {
    case 'DOA':
      csvResult = results.map(record => [record.approverName, record.pnCategoryName, record.pnTableCode, record.errorType, record.picture, record.memberName, (0, _moment.default)(record.createdAt).format('YYYY-MM-DD HH:mm')].join(',')).join('\n').replace(/^/, '審核,商品類型,料號,異常類型,照片,包膜師,回報時間\n');
      break;

    case 'PULLOF':
      csvResult = results.map(record => [record.approverName, record.pnCategoryName, record.pnTableCode, record.memberName, (0, _moment.default)(record.createdAt).format('YYYY-MM-DD HH:mm')].join(',')).join('\n').replace(/^/, '審核,商品類型,料號,包膜師,回報時間\n');
      break;

    case 'SOCIAL':
      csvResult = results.map(record => [record.approverName, record.pnCategoryName, record.pnTableCode, record.target, record.socialDesc, record.picture, record.memberName, (0, _moment.default)(record.createdAt).format('YYYY-MM-DD HH:mm')].join(',')).join('\n').replace(/^/, '審核,商品類型,料號,對象,說明,照片,包膜師,回報時間\n');
      break;

    case 'CUSTOMERCOMPLAINT':
      csvResult = results.map(record => [record.approverName, record.pnCategoryName, record.pnTableCode, record.vat, record.customerName, record.customerPhone, record.errorType, record.picture, record.storeName, record.memberName, (0, _moment.default)(record.createdAt).format('YYYY-MM-DD HH:mm')].join(',')).join('\n').replace(/^/, '審核,商品類型,料號,發票號碼,客戶姓名,客戶手機號碼,異常類型,照片,所在門市,包膜師,回報時間\n');
      break;

    default:
      csvResult = [];
      break;
  } // fixed MS Excel BOM Bug


  const msExcelBuffer = Buffer.concat([Buffer.from('\xEF\xBB\xBF', 'binary'), Buffer.from(csvResult)]);
  ctx.set('Content-Description', 'File Transfer');
  ctx.set('Content-Type', 'text/csv');
  ctx.set('Content-Disposition', `attachment; filename="${type}${(0, _moment.default)().format('YYYYMMDD')}.csv"`);
  ctx.body = msExcelBuffer;
});
exportRouter.get('/punch', async ctx => {
  if (!ctx.req.member) {
    (0, _Export.PERMISSION_ERROR)(ctx);
    return;
  }

  const {
    startDate,
    endDate,
    employeeId
  } = ctx.query;
  const args = {
    startDate: startDate || null,
    endDate: endDate || null,
    memberId: employeeId || null
  };
  const results = await _db.db.models.MemberPunchRecord.scope({
    method: ['search', args]
  }, 'store', 'member').findAll({
    order: [['punchTime', 'DESC'], ['MemberId', 'DESC']]
  });

  if (!results) {
    (0, _Export.SERVER_ERROR)(ctx);
    return;
  }

  const punchText = type => {
    switch (type) {
      case 'ON':
        return '0';

      case 'OFF':
        return '1';

      default:
        return '';
    }
  };

  const csvResult = results.map(record => [record.Member && record.Member.serialNumber || '', 'APP', (0, _moment.default)(record.punchTime).format('YYYY-MM-DD HH:mm'), punchText(record.type)].join(',')).join('\n').replace(/^/, '工號,卡鐘,時間,刷卡別\n'); // fixed MS Excel BOM Bug

  const msExcelBuffer = Buffer.concat([Buffer.from('\xEF\xBB\xBF', 'binary'), Buffer.from(csvResult)]);
  ctx.set('Content-Description', 'File Transfer');
  ctx.set('Content-Type', 'text/csv');
  ctx.set('Content-Disposition', `attachment; filename="punch${(0, _moment.default)().format('YYYYMMDD')}.csv"`);
  ctx.body = msExcelBuffer;
});
var _default = exportRouter;
exports.default = _default;