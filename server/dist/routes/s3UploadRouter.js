"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

var _koaMulter = _interopRequireDefault(require("koa-multer"));

var _awsSdk = _interopRequireDefault(require("aws-sdk"));

var _koaRouter = _interopRequireDefault(require("koa-router"));

var _url = require("../helpers/url");

_awsSdk.default.config.update({
  accessKeyId: process.env.S3ACCESSKEY,
  secretAccessKey: process.env.S3SECRETACCESSKEY
});

const s3 = new _awsSdk.default.S3();
const BUCKET_NAME = process.env.S3BUCKETNAME || 'baomore-test';
const s3UploadRouter = new _koaRouter.default();

const storage = _koaMulter.default.diskStorage({
  destination: _path.default.join(__dirname, '../uploads'),
  filename: (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg') {
      return cb(null, `${Date.now()}.jpg`);
    }

    if (file.mimetype === 'image/png') {
      return cb(null, `${Date.now()}.png`);
    }

    return cb(null, `${Date.now()}${_path.default.extname(file.originalname)}`);
  }
});

const upload = (0, _koaMulter.default)({
  storage
});

function s3upload(filepath, filename) {
  return new Promise((resolve, reject) => {
    const stream = _fs.default.createReadStream(filepath);

    const params = {
      Bucket: BUCKET_NAME,
      Key: filename,
      Body: stream,
      ACL: 'public-read'
    };
    s3.upload(params, (err, data) => {
      if (err) return reject(err);

      _fs.default.unlink(filepath, fsError => {
        if (fsError) {
          reject(fsError);
        }
      });

      return resolve(data.Location);
    });
  });
}

s3UploadRouter.post('/upload', upload.single('file'), async ctx => {
  const {
    filename
  } = ctx.req.file;
  const s3path = await s3upload(_path.default.resolve(__dirname, `../uploads/${filename}`), filename);
  ctx.body = {
    url: (0, _url.getStaticDownloadURL)(s3path),
    filename
  };
});
var _default = s3UploadRouter;
exports.default = _default;