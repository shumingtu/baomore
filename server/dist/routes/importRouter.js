"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _koaRouter = _interopRequireDefault(require("koa-router"));

var _sequelize = require("sequelize");

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

var _koaMulter = _interopRequireDefault(require("koa-multer"));

var _maps = _interopRequireDefault(require("@google/maps"));

var _sumBy = _interopRequireDefault(require("lodash/sumBy"));

var _difference = _interopRequireDefault(require("lodash/difference"));

var _padSN = _interopRequireDefault(require("../helpers/padSN"));

var _roles = require("../helpers/roles.js");

var _getCsvJsonArray = _interopRequireDefault(require("../helpers/getCsvJsonArray.js"));

var _columnKey = require("../columnKey.js");

var _db = require("../db");

var _Csv = require("../errors/Csv.js");

var _env = require("../../shared/env");

var _Export = require("../errors/Export.js");

const importRouter = new _koaRouter.default();

const storage = _koaMulter.default.diskStorage({
  destination: _path.default.join(__dirname, '../uploads'),
  filename: (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg') {
      return cb(null, `${Date.now()}.jpg`);
    }

    if (file.mimetype === 'image/png') {
      return cb(null, `${Date.now()}.png`);
    }

    return cb(null, `${Date.now()}${_path.default.extname(file.originalname)}`);
  }
});

const upload = (0, _koaMulter.default)({
  storage
});

const googleMapClient = _maps.default.createClient({
  key: _env.GOOGLE_MAP_API_KEY,
  Promise
}); // importRouter.post('/stockHelper', upload.array('stocks'), async (ctx) => {
//   const stocksData = ctx.req.files;
//
//   const {
//     serialNumber,
//     pnCode,
//   } = ctx.request.query;
//
//   try {
//     const convertedJsonArray = await getCsvJsonArray(stocksData[0].path);
//     const target = convertedJsonArray.find(x => x['工號'] === serialNumber && x['料號'] === pnCode);
//     const convertedJsonArray2 = await getCsvJsonArray(stocksData[1].path);
//     const target2 = convertedJsonArray2.find(x => x['工號'] === serialNumber && x['料號'] === pnCode);
//
//     const initialQuantity = (parseInt((target && target['數量']), 10) || 0) + (parseInt((target2 && target2['數量']), 10) || 0);
//
//     const member = await db.models.Member.findOne({
//       where: {
//         serialNumber,
//       },
//     });
//
//     const pnTable = await db.models.PNTable.findOne({
//       where: {
//         code: pnCode,
//       },
//     });
//
//     const transferResult = await db.models.BehaviorRecord.findAll({
//       attributes: [
//         'id',
//         'remark',
//       ],
//       where: {
//         MemberId: member.id,
//         PNTableId: pnTable.id,
//         BehaviorId: 4, // 調貨
//         remark: {
//           [Op.notLike]: '%調貨要求',
//         },
//       },
//     });
//
//     const saleRecords = await db.models.BehaviorRecord.findAll({
//       attributes: ['id', 'quantity'],
//       where: {
//         MemberId: member.id,
//         PNTableId: pnTable.id,
//         BehaviorId: 1,
//       },
//     });
//     const salesCount = sumBy(saleRecords, r => r.quantity);
//
//     const checkRecords = await db.models.BehaviorRecord.findAll({
//       attributes: ['id', 'quantity'],
//       where: {
//         MemberId: member.id,
//         PNTableId: pnTable.id,
//         BehaviorId: 2,
//       },
//     });
//     const checkCount = sumBy(checkRecords, r => r.quantity);
//
//     const rollbackRecords = await db.models.BehaviorRecord.findAll({
//       attributes: ['id', 'quantity', 'ApproverId'],
//       where: {
//         MemberId: member.id,
//         PNTableId: pnTable.id,
//         BehaviorId: 5,
//         rollbackType: {
//           [Op.ne]: 'CUSTOMERCOMPLAINT',
//         },
//       },
//     });
//     const rollbackCount = sumBy(rollbackRecords, r => r.quantity);
//
//     const currentStock = await db.models.MemberStock.findOne({
//       where: {
//         MemberId: member.id,
//         PNTableId: pnTable.id,
//       },
//     });
//
//     const transferResultIds = transferResult.map(t => t.id);
//
//     const transferSuccessTo = await db.models.BehaviorRecord.findAll({
//       attributes: [
//         'id',
//         'remark',
//         'quantity',
//         'TransferRecordId',
//       ],
//       where: {
//         TransferRecordId: transferResultIds,
//       },
//     });
//
//     const transferSuccessToCount = sumBy(transferSuccessTo, t => t.quantity);
//
//     const transferFailedRecords = await db.models.BehaviorRecord.findAll({
//       attributes: [
//         'id',
//         'qrcode',
//       ],
//       where: {
//         id: difference(transferResultIds, transferSuccessTo.map(t => t.TransferRecordId)),
//       },
//     });
//
//
//     ctx.body = {
//       initialQuantity,
//       salesCount,
//       checkCount,
//       transferSuccessToCount,
//       rollbackCount,
//       expectedQuantity: initialQuantity - salesCount + checkCount - transferSuccessToCount - rollbackCount,
//       currentStock,
//       rollbackRecords,
//       transferFailedRecords,
//     };
//   } catch (e) {
//     ctx.body = {
//       message: `${e}`,
//     };
//   } finally {
//     stocksData.forEach((data) => {
//       fs.unlinkSync(data.path);
//     });
//   }
// });


importRouter.post('/import/director', upload.single('file'), async ctx => {
  const {
    path: pathname
  } = ctx.req.file;
  const {
    token
  } = ctx.request.query;

  if (!ctx.req.member || !(0, _roles.isPermissionAllowed)(['ADMIN_MEMBER_MANAGE'], token)) {
    (0, _Export.PERMISSION_ERROR)(ctx);

    _fs.default.unlinkSync(pathname);

    return;
  }

  const convertedJsonArray = await (0, _getCsvJsonArray.default)(pathname);

  if (!convertedJsonArray) {
    _fs.default.unlinkSync(pathname);

    ctx.status = 400;
    throw new _Csv.CsvImportError();
  }

  const createDataPromise = () => new Promise(async (resolve, reject) => {
    const data = await Promise.all(convertedJsonArray.map(async x => {
      const serialNumber = x[_columnKey.directorColumn[1].name];
      const name = x[_columnKey.directorColumn[2].name];
      const phone = x[_columnKey.directorColumn[3].name];
      const managerName = x[_columnKey.directorColumn[4].name];
      const lineAccount = x[_columnKey.directorColumn[5].name];

      if (!serialNumber || !name || !managerName) {
        reject();
      }

      const targetArea = await _db.db.models.Area.findOne({
        attributes: ['id'],
        where: {
          name: {
            [_sequelize.Op.like]: `%${x[_columnKey.directorColumn[0].name]}%`
          }
        }
      });
      if (!targetArea) return null;
      return {
        AreaId: targetArea && targetArea.id,
        serialNumber,
        name,
        phone,
        managerName,
        lineAccount: lineAccount || null
      };
    }));
    resolve(data);
  });

  try {
    const data = await createDataPromise();
    const createBundle = data.filter(x => x !== null);

    if (!createBundle.length) {
      ctx.status = 400;
      ctx.body = {
        message: '發生錯誤'
      };
      return;
    }

    const role = await _db.db.models.Role.findOne({
      where: {
        name: '主任包膜師'
      }
    });
    const memberCreateGroup = await _db.db.models.Member.bulkCreate(createBundle);
    await role.addMembers(memberCreateGroup);
    ctx.status = 204;
  } catch (e) {
    ctx.status = 400;
    ctx.body = {
      message: '發生錯誤'
    };
  } finally {
    _fs.default.unlinkSync(pathname);
  }
});
importRouter.post('/import/employee', upload.single('file'), async ctx => {
  const {
    path: pathname
  } = ctx.req.file;
  const {
    token
  } = ctx.request.query;

  if (!ctx.req.member || !(0, _roles.isPermissionAllowed)(['ADMIN_MEMBER_MANAGE'], token)) {
    (0, _Export.PERMISSION_ERROR)(ctx);

    _fs.default.unlinkSync(pathname);

    return;
  }

  const convertedJsonArray = await (0, _getCsvJsonArray.default)(pathname);

  if (!convertedJsonArray) {
    ctx.status = 400;

    _fs.default.unlinkSync(pathname);

    throw new _Csv.CsvImportError();
  }

  const createDataPromise = () => new Promise(async (resolve, reject) => {
    const data = await Promise.all(convertedJsonArray.map(async x => {
      const serialNumber = x[_columnKey.employeeColumn[1].name];
      const name = x[_columnKey.employeeColumn[2].name];
      const phone = x[_columnKey.employeeColumn[3].name];
      const directorSN = x[_columnKey.employeeColumn[4].name];
      const lineAccount = x[_columnKey.employeeColumn[5].name];

      if (!serialNumber || !name || !phone || !directorSN) {
        reject();
      }

      const memberDirector = await _db.db.models.Member.findOne({
        attributes: ['id'],
        where: {
          serialNumber: directorSN
        }
      });
      const targetArea = await _db.db.models.Area.findOne({
        attributes: ['id'],
        where: {
          name: {
            [_sequelize.Op.like]: `%${x[_columnKey.employeeColumn[0].name]}%`
          }
        }
      });
      if (!memberDirector || !targetArea) return null;
      return {
        AreaId: targetArea.id,
        serialNumber,
        name,
        phone,
        lineAccount: lineAccount || null,
        MemberId: memberDirector.id
      };
    }));
    resolve(data);
  });

  try {
    const data = await createDataPromise();
    const createBundle = data.filter(x => x !== null);
    const memberCreateGroup = await _db.db.models.Member.bulkCreate(createBundle);

    if (!memberCreateGroup) {
      ctx.status = 400;
      ctx.body = {
        message: '發生錯誤！'
      };
      return;
    }

    const role = await _db.db.models.Role.findOne({
      where: {
        name: '包膜師'
      }
    });
    await role.addMembers(memberCreateGroup);
    ctx.status = 204;
  } catch (e) {
    ctx.status = 400;
    ctx.body = {
      message: '發生錯誤！'
    };
  } finally {
    _fs.default.unlinkSync(pathname);
  }
});
importRouter.post('/import/channel', upload.single('file'), async ctx => {
  const {
    path: pathname
  } = ctx.req.file;
  const {
    token
  } = ctx.request.query;

  if (!ctx.req.member || !(0, _roles.isPermissionAllowed)(['ADMIN_MEMBER_MANAGE'], token)) {
    (0, _Export.PERMISSION_ERROR)(ctx);

    _fs.default.unlinkSync(pathname);

    return;
  }

  const convertedJsonArray = await (0, _getCsvJsonArray.default)(pathname);

  if (!convertedJsonArray) {
    ctx.status = 400;

    _fs.default.unlinkSync(pathname);

    throw new _Csv.CsvImportError();
  }

  const createDataPromise = () => new Promise(async (resolve, reject) => {
    const data = convertedJsonArray.map(x => {
      const name = x[_columnKey.channelColumn[0].name];
      const isPurchaseChannel = x[_columnKey.channelColumn[1].name] === 'Y' || false;

      if (!name) {
        reject();
      }

      return {
        name,
        isPurchaseChannel
      };
    });
    resolve(data);
  });

  try {
    const createBundle = await createDataPromise();

    if (!createBundle.length) {
      ctx.status = 400;
      ctx.body = {
        message: '發生錯誤'
      };
      return;
    }

    await _db.db.models.Channel.bulkCreate(createBundle);
    ctx.status = 204;
  } catch (e) {
    ctx.status = 400;
    ctx.body = {
      message: '發生錯誤'
    };
  } finally {
    _fs.default.unlinkSync(pathname);
  }
});
importRouter.post('/import/store', upload.single('file'), async ctx => {
  const {
    path: pathname
  } = ctx.req.file;
  const {
    token
  } = ctx.request.query;

  if (!ctx.req.member || !(0, _roles.isPermissionAllowed)(['ADMIN_MEMBER_MANAGE'], token)) {
    (0, _Export.PERMISSION_ERROR)(ctx);

    _fs.default.unlinkSync(pathname);

    return;
  }

  let convertedJsonArray = await (0, _getCsvJsonArray.default)(pathname);

  if (!convertedJsonArray) {
    ctx.status = 400;

    _fs.default.unlinkSync(pathname);

    throw new _Csv.CsvImportError();
  }

  convertedJsonArray = convertedJsonArray.filter(x => x['門市地址']);
  const storeIgnoreBundle = [];
  const storeCreateBundle = [];
  const memberStoreBundle = [];

  try {
    await convertedJsonArray.map(x => () => new Promise(async resolve => {
      const name = x[_columnKey.storeColumn[0].name];
      const phone = x[_columnKey.storeColumn[1].name];
      const address = x[_columnKey.storeColumn[4].name];
      const mainEmployeeSN = x[_columnKey.storeColumn[7].name];
      const backupEmployeeSN = x[_columnKey.storeColumn[8].name];
      const city = x[_columnKey.storeColumn[2].name];
      const existStore = await _db.db.models.Store.findOne({
        where: {
          name
        }
      });

      if (existStore) {
        storeIgnoreBundle.push({
          name
        });
        return resolve();
      }

      const targetArea = await _db.db.models.Area.findOne({
        attributes: ['id'],
        where: {
          name: {
            [_sequelize.Op.like]: `%${x[_columnKey.storeColumn[5].name]}%`
          }
        }
      });
      const targetChannel = await _db.db.models.Channel.findOne({
        attributes: ['id'],
        where: {
          name: {
            [_sequelize.Op.like]: `%${x[_columnKey.storeColumn[6].name]}%`
          }
        }
      });
      const targetCity = await _db.db.models.City.findOne({
        attributes: ['id'],
        where: {
          name: {
            [_sequelize.Op.like]: `%${city}%`
          }
        }
      });
      const targetDistrict = await _db.db.models.District.findOne({
        attributes: ['id'],
        where: {
          name: {
            [_sequelize.Op.like]: `%${x[_columnKey.storeColumn[3].name]}%`
          },
          CityId: targetCity && targetCity.id || null
        }
      });
      let mainEmployee = null;

      if (mainEmployeeSN) {
        mainEmployee = await _db.db.models.Member.findOne({
          attributes: ['id'],
          where: {
            serialNumber: mainEmployeeSN
          }
        });
      }

      let backupEmployee = null;

      if (backupEmployeeSN) {
        backupEmployee = await _db.db.models.Member.findOne({
          attributes: ['id'],
          where: {
            serialNumber: backupEmployeeSN
          }
        });
      }

      const googleResponse = await googleMapClient.geocode({
        address
      }).asPromise();
      let locationResult;

      if (googleResponse && googleResponse.json && googleResponse.json.results && googleResponse.json.results[0] && googleResponse.json.results[0].geometry) {
        locationResult = googleResponse.json.results[0].geometry.location;
      }

      const createdStore = await _db.db.models.Store.create({
        name,
        phone,
        address,
        latitude: locationResult && locationResult.lat,
        longitude: locationResult && locationResult.lng,
        AreaId: targetArea && targetArea.id,
        ChannelId: targetChannel && targetChannel.id,
        DistrictId: targetDistrict && targetDistrict.id
      });

      if (mainEmployee) {
        memberStoreBundle.push({
          type: 'MAIN',
          MemberId: mainEmployee.id,
          StoreId: createdStore.id
        });
      }

      if (backupEmployee && mainEmployeeSN !== backupEmployeeSN) {
        memberStoreBundle.push({
          type: 'BACKUP',
          MemberId: backupEmployee.id,
          StoreId: createdStore.id
        });
      }

      storeCreateBundle.push({
        name
      });
      return resolve();
    })).reduce((prev, next) => prev.then(next), Promise.resolve());
    await _db.db.models.MemberStore.bulkCreate(memberStoreBundle);
    ctx.status = 200;
    ctx.body = {
      storeCreateBundle,
      storeIgnoreBundle
    };
  } catch (e) {
    ctx.status = 400;
    ctx.body = {
      message: `${e}`
    };
  } finally {
    _fs.default.unlinkSync(pathname);
  }
});
importRouter.post('/import/schedule', upload.single('file'), async ctx => {
  const {
    path: pathname
  } = ctx.req.file;
  const {
    token
  } = ctx.request.query;

  if (!ctx.req.member || !(0, _roles.isPermissionAllowed)(['ADMIN_MEMBER_MANAGE'], token)) {
    (0, _Export.PERMISSION_ERROR)(ctx);

    _fs.default.unlinkSync(pathname);

    return;
  }

  const convertedJsonArray = await (0, _getCsvJsonArray.default)(pathname);

  if (!convertedJsonArray) {
    ctx.status = 400;

    _fs.default.unlinkSync(pathname);

    throw new _Csv.CsvImportError();
  }

  const dates = Object.keys(convertedJsonArray[0]).filter(key => key !== '員工編號' && key !== '姓  名');
  if (!dates.length) throw new _Csv.CsvImportError();
  const createData = [];
  const deleteDataId = [];

  function getCreateDeleteContent(targetMember, item) {
    return new Promise(async resolve => {
      if (targetMember) {
        await Promise.all(dates.map(async date => {
          const existMemberSchedule = await _db.db.models.MemberSchedule.findOne({
            attributes: ['id'],
            where: {
              MemberId: targetMember.id,
              date
            }
          });

          if (existMemberSchedule) {
            deleteDataId.push(existMemberSchedule.id);
          }

          const status = item[date];
          createData.push({
            date,
            MemberId: targetMember.id,
            status: status === '4' || status === '004' ? '4' : status
          });
        }));
      }

      resolve();
    });
  }

  try {
    await convertedJsonArray.map(x => () => new Promise(async resolve => {
      const serialNumber = x['員工編號'];
      const targetMember = await _db.db.models.Member.findOne({
        attributes: ['id'],
        where: {
          serialNumber
        }
      });
      if (!targetMember) return resolve(x);
      await getCreateDeleteContent(targetMember, x);
      return resolve(x);
    })).reduce((prev, next) => prev.then(next), Promise.resolve());
  } catch (e) {
    ctx.status = 400;

    _fs.default.unlinkSync(pathname);

    throw new _Csv.CsvImportError();
  }

  if (!createData.length) {
    ctx.status = 200;

    _fs.default.unlinkSync(pathname);

    return;
  }

  const transaction = await _db.db.transaction();

  try {
    if (deleteDataId.length) {
      await _db.db.models.MemberSchedule.destroy({
        force: true,
        where: {
          id: {
            [_sequelize.Op.in]: deleteDataId
          }
        }
      }, transaction);
    }

    await _db.db.models.MemberSchedule.bulkCreate(createData, {
      transaction
    });

    _fs.default.unlinkSync(pathname);

    await transaction.commit();
    ctx.status = 200;
  } catch (e) {
    ctx.status = 400;

    _fs.default.unlinkSync(pathname);

    await transaction.rollback();
    throw new _Csv.CsvImportError();
  }
});
importRouter.post('/import/warranty', upload.single('file'), async ctx => {
  const {
    path: pathname
  } = ctx.req.file;
  const {
    token
  } = ctx.request.query;

  if (!ctx.req.member || !(0, _roles.isPermissionAllowed)(['ADMIN_MEMBER_MANAGE'], token)) {
    (0, _Export.PERMISSION_ERROR)(ctx);

    _fs.default.unlinkSync(pathname);

    return;
  }

  const convertedJsonArray = await (0, _getCsvJsonArray.default)(pathname);

  if (!convertedJsonArray) {
    ctx.status = 400;

    _fs.default.unlinkSync(pathname);

    throw new _Csv.CsvImportError();
  }

  const transaction = await _db.db.transaction();
  const rawBundle = [];

  try {
    await convertedJsonArray.map(x => () => new Promise(async resolve => {
      // Member data
      const lineId = x[_columnKey.warrantyCardColumn[0]];
      const name = x[_columnKey.warrantyCardColumn[1]];
      const employeeSN = (0, _padSN.default)(x[_columnKey.warrantyCardColumn[7]]); // warranty data

      const service = x[_columnKey.warrantyCardColumn[2]];
      const store = x[_columnKey.warrantyCardColumn[3]];
      const phoneModel = x[_columnKey.warrantyCardColumn[4]];
      const VAT = x[_columnKey.warrantyCardColumn[5]];
      const createdAt = x[_columnKey.warrantyCardColumn[6]];
      const verifiedTime = x[_columnKey.warrantyCardColumn[8]];
      const isApproved = x[_columnKey.warrantyCardColumn[9]] === '認證';

      if (!lineId || !name || !employeeSN || !service || !store || !phoneModel || !VAT || !createdAt || !verifiedTime) {
        rawBundle.push({
          status: false,
          lineId,
          name
        });
        return resolve();
      }

      const employee = await _db.db.models.Member.findOne({
        where: {
          serialNumber: employeeSN
        }
      });
      const consumer = await _db.db.models.Member.findOrCreate({
        where: {
          lineId,
          name
        },
        transaction
      });

      if (consumer[1]) {
        const role = await _db.db.models.Role.findOne({
          where: {
            name: '一般登入用戶'
          }
        });
        await role.addMember(consumer[0].id, {
          transaction
        });
      }

      rawBundle.push({
        status: true,
        service,
        phoneModel,
        store,
        VAT,
        createdAt,
        verifiedTime: verifiedTime === '-' ? null : verifiedTime,
        isApproved,
        IME: null,
        phone: null,
        MemberId: consumer[0].id,
        approverSN: employeeSN,
        ApproverId: employee && employee.id || null
      });
      return resolve();
    })).reduce((prev, next) => prev.then(next), Promise.resolve());
    const ignoreBundle = rawBundle.filter(x => !x.status);

    if (ignoreBundle.length) {
      await transaction.rollback();
      ctx.status = 400;
      ctx.body = {
        ignoreBundle
      };
      return;
    }

    const createBundle = rawBundle.filter(x => x.status);
    await _db.db.models.MemberWarranty.bulkCreate(createBundle, {
      transaction
    });
    await transaction.commit();
    ctx.status = 200;
    ctx.body = {
      createBundle
    };
  } catch (e) {
    await transaction.rollback();
    ctx.status = 400;
    ctx.body = {
      message: `${e}`
    };
  } finally {
    _fs.default.unlinkSync(pathname);
  }
});
importRouter.post('/import/salesData', upload.single('file'), async ctx => {
  const {
    path: pathname
  } = ctx.req.file;
  const {
    token
  } = ctx.request.query;

  if (!ctx.req.member || !(0, _roles.isPermissionAllowed)(['ADMIN_MEMBER_MANAGE'], token)) {
    (0, _Export.PERMISSION_ERROR)(ctx);

    _fs.default.unlinkSync(pathname);

    return;
  }

  const convertedJsonArray = await (0, _getCsvJsonArray.default)(pathname);

  if (!convertedJsonArray) {
    ctx.status = 400;

    _fs.default.unlinkSync(pathname);

    throw new _Csv.CsvImportError();
  }

  const behavior = await _db.db.models.Behavior.findOne({
    where: {
      name: '銷貨'
    }
  });

  const generateCreateData = () => new Promise(async (resolve, reject) => {
    const data = await Promise.all(convertedJsonArray.map(async x => {
      const serialNumber = x[_columnKey.salesColumn[0].name];
      const orderSN = x[_columnKey.salesColumn[1].name];
      const startTime = x[_columnKey.salesColumn[2].name];
      const endTime = x[_columnKey.salesColumn[3].name];
      const pnTableCode = x[_columnKey.salesColumn[4].name];
      const storeName = x[_columnKey.salesColumn[5].name];
      const price = x[_columnKey.salesColumn[6].name];

      if (!serialNumber || !orderSN || !startTime || !endTime || !pnTableCode || !storeName || !price) {
        reject();
      }

      const targetMember = await _db.db.models.Member.findOne({
        where: {
          serialNumber
        }
      });
      const targetPNTable = await _db.db.models.PNTable.findOne({
        where: {
          code: pnTableCode
        }
      });
      const targetStore = await _db.db.models.Store.findOne({
        where: {
          name: {
            [_sequelize.Op.like]: `%${storeName}%`
          }
        }
      });

      if (!targetMember || !targetPNTable || !targetStore || !behavior) {
        return {
          isNull: true,
          serialNumber,
          pnTableCode,
          storeName
        };
      }

      return {
        startTime,
        endTime,
        MemberId: targetMember.id,
        orderSN,
        PNTableId: targetPNTable.id,
        StoreId: targetStore.id,
        price,
        BehaviorId: behavior.id
      };
    }));
    resolve(data);
  });

  try {
    const data = await generateCreateData();
    const nullData = data.filter(x => x.isNull === true);
    const createBundle = data.filter(x => !x.isNull);
    await _db.db.models.BehaviorRecord.bulkCreate(createBundle);
    ctx.status = 200;
    ctx.body = {
      nullData,
      createBundle
    };
  } catch (e) {
    ctx.status = 400;
    ctx.body = {
      message: '發生錯誤'
    };
  } finally {
    _fs.default.unlinkSync(pathname);
  }
});
importRouter.post('/import/stockData', upload.single('file'), async ctx => {
  const {
    path: pathname
  } = ctx.req.file;
  const {
    token
  } = ctx.request.query;

  if (!ctx.req.member || !(0, _roles.isPermissionAllowed)(['ADMIN_MEMBER_MANAGE'], token)) {
    (0, _Export.PERMISSION_ERROR)(ctx);

    _fs.default.unlinkSync(pathname);

    return;
  }

  const convertedJsonArray = await (0, _getCsvJsonArray.default)(pathname);

  if (!convertedJsonArray) {
    ctx.status = 400;

    _fs.default.unlinkSync(pathname);

    throw new _Csv.CsvImportError();
  }

  const generateCreateData = () => new Promise(async (resolve, reject) => {
    const data = await Promise.all(convertedJsonArray.map(async x => {
      const pnTableCode = x[_columnKey.stockColumn[0].name];
      const serialNumber = x[_columnKey.stockColumn[1].name];
      const storageCount = x[_columnKey.stockColumn[2].name];

      if (!serialNumber || !storageCount || !pnTableCode) {
        reject();
      }

      const targetMember = await _db.db.models.Member.findOne({
        where: {
          serialNumber
        }
      });
      const targetPNTable = await _db.db.models.PNTable.findOne({
        where: {
          code: pnTableCode
        }
      });

      if (!targetMember || !targetPNTable) {
        return {
          isNull: true,
          pnTableCode,
          serialNumber
        };
      }

      return {
        MemberId: targetMember.id,
        PNTableId: targetPNTable.id,
        storageCount
      };
    }));
    resolve(data);
  });

  try {
    const data = await generateCreateData();
    const nullData = data.filter(x => x.isNull === true);
    const createBundle = data.filter(x => !x.isNull);
    await _db.db.models.MemberStock.bulkCreate(createBundle);
    ctx.status = 200;
    ctx.body = {
      nullData
    };
  } catch (e) {
    ctx.status = 400;
    ctx.body = {
      message: '發生錯誤'
    };
  } finally {
    _fs.default.unlinkSync(pathname);
  }
});
var _default = importRouter;
exports.default = _default;