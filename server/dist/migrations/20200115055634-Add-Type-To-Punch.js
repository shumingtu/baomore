"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('MemberPunchRecords', 'type', {
      type: Sequelize.ENUM,
      values: ['ON', 'OFF']
    });
  },
  down: async queryInterface => {
    await queryInterface.removeColumn('MemberPunchRecords', 'type');
  }
};