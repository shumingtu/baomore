"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('Stores', 'archive', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: 0
    });
    await queryInterface.addColumn('Members', 'archive', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: 0
    });
  },
  down: async queryInterface => {
    await queryInterface.removeColumn('Stores', 'archive');
    await queryInterface.removeColumn('Members', 'archive');
  }
};