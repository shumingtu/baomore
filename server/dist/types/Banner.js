"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.bannerDeleteResponseType = exports.listType = void 0;

var _graphql = require("graphql");

const listType = new _graphql.GraphQLObjectType({
  name: 'Banners',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    picture: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    link: {
      type: _graphql.GraphQLString
    },
    mobileImg: {
      type: _graphql.GraphQLString
    }
  })
});
exports.listType = listType;
const bannerDeleteResponseType = new _graphql.GraphQLObjectType({
  name: 'BannerDeleteResponseType',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.bannerDeleteResponseType = bannerDeleteResponseType;
var _default = listType;
exports.default = _default;