"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.storePerformanceForAdminType = exports.employeePerformanceForAdminType = exports.verifyRollbackBehaviorRecordType = exports.behaviorRecordType = exports.behaviorType = void 0;

var _graphql = require("graphql");

var _moment = _interopRequireDefault(require("moment"));

var _Store = require("./Store");

var _Member = require("./Member");

var _Vendor = require("./Vendor.js");

var _PNTable = require("./PNTable.js");

var _Area = require("./Area");

var _Channel = require("./Channel");

const behaviorType = new _graphql.GraphQLObjectType({
  name: 'Behavior',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.behaviorType = behaviorType;
const behaviorRecordType = new _graphql.GraphQLObjectType({
  name: 'BehaviorRecord',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    rollbackType: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    startTime: {
      type: _graphql.GraphQLString,
      resolve: record => record.startTime && (0, _moment.default)(record.startTime).format('YYYY-MM-DD HH:mm:ss')
    },
    endTime: {
      type: _graphql.GraphQLString,
      resolve: record => record.endTime && (0, _moment.default)(record.endTime).format('YYYY-MM-DD HH:mm:ss')
    },
    picture: {
      type: _graphql.GraphQLString
    },
    price: {
      type: _graphql.GraphQLInt
    },
    remark: {
      type: _graphql.GraphQLString
    },
    remarks: {
      type: new _graphql.GraphQLList(_graphql.GraphQLString)
    },
    createdAt: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString),
      resolve: record => record.createdAt && (0, _moment.default)(record.createdAt).format('YYYY-MM-DD HH:mm')
    },
    member: {
      type: _Member.memberType,
      resolve: record => record.Member || null
    },
    vendor: {
      type: _Vendor.VendorType,
      resolve: record => record.Vendor || null
    },
    store: {
      type: _Store.storeType,
      resolve: record => record.Store || null
    },
    pnTable: {
      type: _PNTable.PNTableType,
      resolve: record => record.PNTable || null
    },
    pnCategory: {
      type: _PNTable.PNCategoryType
    },
    approver: {
      type: _Member.memberType,
      resolve: record => record.Approver
    },
    behavior: {
      type: behaviorType,
      resolve: record => record.Behavior
    },
    qrcode: {
      type: _graphql.GraphQLString
    },
    orderSN: {
      type: _graphql.GraphQLString
    }
  })
});
exports.behaviorRecordType = behaviorRecordType;
const verifyRollbackBehaviorRecordType = new _graphql.GraphQLObjectType({
  name: 'VerifyRollbackBehaviorRecord',
  fields: () => ({
    behaviorRecords: {
      type: new _graphql.GraphQLList(behaviorRecordType)
    },
    errorRecords: {
      type: new _graphql.GraphQLList(behaviorRecordType)
    }
  })
});
exports.verifyRollbackBehaviorRecordType = verifyRollbackBehaviorRecordType;
const employeePerformanceForAdminType = new _graphql.GraphQLObjectType({
  name: 'EmployeePerformanceForAdmin',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt),
      description: '包膜師ID'
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString),
      description: '包膜師姓名'
    },
    area: {
      type: _Area.AreaType
    },
    targets: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLInt))
    },
    amounts: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLInt))
    },
    sales: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLInt))
    },
    achieveRate: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    },
    asp: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    },
    mom: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    },
    workHours: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    },
    expectAchieves: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    },
    expectProfits: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    },
    turnover: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    },
    commissions: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    }
  })
});
exports.employeePerformanceForAdminType = employeePerformanceForAdminType;
const storePerformanceForAdminType = new _graphql.GraphQLObjectType({
  name: 'StorePerformanceForAdmin',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt),
      description: '門市ID'
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString),
      description: '門市名稱'
    },
    channel: {
      type: _Channel.ChannelType
    },
    targets: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLInt))
    },
    amounts: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLInt))
    },
    sales: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLInt))
    },
    achieveRate: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    },
    asp: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    },
    mom: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    },
    workHours: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    },
    expectAchieves: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    },
    expectProfits: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    },
    turnover: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    },
    commissions: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    }
  })
});
exports.storePerformanceForAdminType = storePerformanceForAdminType;