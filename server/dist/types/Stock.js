"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StockType = exports.StockResponseTypeForPurchase = exports.StockResponseType = exports.StockCheckResponseType = exports.stockListType = void 0;

var _graphql = require("graphql");

var _PNTable = require("./PNTable");

var _Order = require("./Order.js");

var _Member = require("./Member.js");

const stockListType = new _graphql.GraphQLObjectType({
  name: 'StockList',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    storageCount: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    member: {
      type: new _graphql.GraphQLNonNull(_Member.memberType),
      resolve: stock => stock.Member
    },
    pnTable: {
      type: new _graphql.GraphQLNonNull(_PNTable.PNTableType),
      resolve: stock => stock.PNTable
    }
  })
});
exports.stockListType = stockListType;
const StockCheckResponseType = new _graphql.GraphQLObjectType({
  name: 'StockCheckResponse',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    pnTable: {
      type: new _graphql.GraphQLNonNull(_PNTable.PNTableType)
    },
    amount: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  })
});
exports.StockCheckResponseType = StockCheckResponseType;
const StockResponseType = new _graphql.GraphQLObjectType({
  name: 'StockResponse',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    }
  })
});
exports.StockResponseType = StockResponseType;
const StockResponseTypeForPurchase = new _graphql.GraphQLObjectType({
  name: 'StockResponseTypeForPurchase',
  fields: () => ({
    pnTableId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    order: {
      type: _Order.orderType
    }
  })
});
exports.StockResponseTypeForPurchase = StockResponseTypeForPurchase;
const StockType = new _graphql.GraphQLObjectType({
  name: 'Inventory',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt),
      description: '料號ID'
    },
    pnTable: {
      type: new _graphql.GraphQLNonNull(_PNTable.PNTableType)
    },
    amount: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    orderingOrder: {
      type: _Order.orderType
    }
  })
});
exports.StockType = StockType;