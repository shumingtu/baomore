"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.districtType = void 0;

var _graphql = require("graphql");

var _CityDistrict = require("./CityDistrict.js");

const districtType = new _graphql.GraphQLObjectType({
  name: 'District',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    city: {
      type: _CityDistrict.cityType,
      resolve: district => district.City
    }
  })
});
exports.districtType = districtType;
var _default = null;
exports.default = _default;