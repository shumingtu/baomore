"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.vendorDeleteResponseType = exports.VendorType = void 0;

var _graphql = require("graphql");

var _VendorSetting = require("./VendorSetting.js");

const VendorType = new _graphql.GraphQLObjectType({
  name: 'Vendor',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    contactPersonName: {
      type: _graphql.GraphQLString
    },
    phone: {
      type: _graphql.GraphQLString
    },
    address: {
      type: _graphql.GraphQLString
    },
    VendorSettings: {
      type: new _graphql.GraphQLList(_VendorSetting.vendorSettingType)
    }
  })
});
exports.VendorType = VendorType;
const vendorDeleteResponseType = new _graphql.GraphQLObjectType({
  name: 'VendorDeleteResponseType',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.vendorDeleteResponseType = vendorDeleteResponseType;
var _default = null;
exports.default = _default;