"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.AreaType = void 0;

var _graphql = require("graphql");

const AreaType = new _graphql.GraphQLObjectType({
  name: 'Area',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.AreaType = AreaType;
var _default = null;
exports.default = _default;