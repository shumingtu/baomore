"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PNTableType = exports.CustomPNTableType = exports.PNSubCategoryDeleteResponseType = exports.PNSubCategoryType = exports.PNCategoryDeleteResponseType = exports.PNCategoryType = exports.PNTableDeleteResponseType = exports.PNTableCodeType = void 0;

var _graphql = require("graphql");

var _Vendor = require("./Vendor.js");

var _Brand = require("./Brand.js");

const PNTableCodeType = new _graphql.GraphQLObjectType({
  name: 'PNTableCodeType',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    code: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.PNTableCodeType = PNTableCodeType;
const PNTableDeleteResponseType = new _graphql.GraphQLObjectType({
  name: 'PNTableDeleteResponseType',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.PNTableDeleteResponseType = PNTableDeleteResponseType;
const PNCategoryType = new _graphql.GraphQLObjectType({
  name: 'PNCategory',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.PNCategoryType = PNCategoryType;
const PNCategoryDeleteResponseType = new _graphql.GraphQLObjectType({
  name: 'PNCategoryDeleteResponseType',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.PNCategoryDeleteResponseType = PNCategoryDeleteResponseType;
const PNSubCategoryType = new _graphql.GraphQLObjectType({
  name: 'SubPNCategory',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    picture: {
      type: _graphql.GraphQLString
    },
    isOnSale: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    }
  })
});
exports.PNSubCategoryType = PNSubCategoryType;
const PNSubCategoryDeleteResponseType = new _graphql.GraphQLObjectType({
  name: 'PNSubCategoryDeleteResponseType',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.PNSubCategoryDeleteResponseType = PNSubCategoryDeleteResponseType;
const CustomPNTableType = new _graphql.GraphQLObjectType({
  name: 'CustomPNTableType',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    code: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    description: {
      type: _graphql.GraphQLString
    },
    onlinePrice: {
      type: _graphql.GraphQLString
    }
  })
});
exports.CustomPNTableType = CustomPNTableType;
const PNTableType = new _graphql.GraphQLObjectType({
  name: 'PNTable',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    code: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    picture: {
      type: _graphql.GraphQLString
    },
    suitableDevice: {
      type: _graphql.GraphQLString
    },
    description: {
      type: _graphql.GraphQLString
    },
    isOnSale: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    price: {
      type: _graphql.GraphQLString
    },
    onlinePrice: {
      type: _graphql.GraphQLString
    },
    pnCategory: {
      type: PNCategoryType,
      resolve: pnTable => pnTable.PNSubCategory && pnTable.PNSubCategory.PNCategory || pnTable.pnCategory
    },
    bigSubCategory: {
      type: PNSubCategoryType,
      resolve: pnTable => pnTable.PNSubCategory && pnTable.PNSubCategory.PNSubCategory || pnTable.bigSubCategory
    },
    smallSubCategory: {
      type: PNSubCategoryType,
      resolve: pnTable => pnTable.PNSubCategory || pnTable.smallSubCategory
    },
    vendor: {
      type: _Vendor.VendorType,
      resolve: pnTable => pnTable.Vendor
    },
    BrandModel: {
      type: _Brand.BrandModelType
    },
    Brand: {
      type: _Brand.BrandType,
      resolve: pnTable => pnTable.BrandModel && pnTable.BrandModel.Brand
    }
  })
});
exports.PNTableType = PNTableType;