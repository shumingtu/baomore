"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PayConfirmResponseType = exports.PayOnlineOrderResponseType = void 0;

var _graphql = require("graphql");

const PayOnlineOrderResponseType = new _graphql.GraphQLObjectType({
  name: 'PayOnlineOrder',
  fields: () => ({
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    message: {
      type: _graphql.GraphQLString
    },
    human: {
      type: _graphql.GraphQLString
    },
    payToken: {
      type: _graphql.GraphQLString
    }
  })
});
exports.PayOnlineOrderResponseType = PayOnlineOrderResponseType;
const PayConfirmResponseType = new _graphql.GraphQLObjectType({
  name: 'PayConfirm',
  fields: () => ({
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    message: {
      type: _graphql.GraphQLString
    },
    human: {
      type: _graphql.GraphQLString
    }
  })
}); // export const PayConfirmResponseType = new GraphQLObjectType({
//   name: 'PayConfirm',
//   fields: () => ({
//     status: {
//       type: new GraphQLNonNull(GraphQLBoolean),
//     },
//     message: {
//       type: GraphQLString,
//     },
//     human: {
//       type: GraphQLString,
//     },
//   }),
// });

exports.PayConfirmResponseType = PayConfirmResponseType;