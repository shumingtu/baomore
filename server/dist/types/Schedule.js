"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.memberAssignmentType = exports.scheduleType = exports.assignmentType = void 0;

var _graphql = require("graphql");

var _OnlineOrder = require("./OnlineOrder");

const assignmentType = new _graphql.GraphQLObjectType({
  name: 'Assignment',
  fields: () => ({
    time: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    denominator: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    onlineOrders: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_OnlineOrder.onlineOrderType))
    }
  })
});
exports.assignmentType = assignmentType;
const scheduleType = new _graphql.GraphQLObjectType({
  name: 'Schedule',
  fields: () => ({
    date: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    assignments: {
      type: new _graphql.GraphQLList(assignmentType)
    }
  })
});
exports.scheduleType = scheduleType;
const memberAssignmentType = new _graphql.GraphQLObjectType({
  name: 'memberAssignment',
  fields: () => ({
    time: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    order: {
      type: new _graphql.GraphQLNonNull(_OnlineOrder.onlineOrderType)
    }
  })
});
exports.memberAssignmentType = memberAssignmentType;
var _default = scheduleType;
exports.default = _default;