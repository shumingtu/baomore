"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.timeType = exports.dateType = void 0;

var _graphql = require("graphql");

const dateType = new _graphql.GraphQLObjectType({
  name: 'Date',
  fields: () => ({
    date: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.dateType = dateType;
const timeType = new _graphql.GraphQLObjectType({
  name: 'Time',
  fields: () => ({
    time: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.timeType = timeType;