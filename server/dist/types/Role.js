"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.roleType = void 0;

var _graphql = require("graphql");

var _Action = require("./Action.js");

const roleType = new _graphql.GraphQLObjectType({
  name: 'Role',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    actions: {
      type: new _graphql.GraphQLList(_Action.actionType),
      resolve: role => role.Actions
    }
  })
});
exports.roleType = roleType;
var _default = null;
exports.default = _default;