"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.cityType = void 0;

var _graphql = require("graphql");

var _District = require("./District");

const cityType = new _graphql.GraphQLObjectType({
  name: 'City',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    latitude: {
      type: _graphql.GraphQLFloat
    },
    longitude: {
      type: _graphql.GraphQLFloat
    },
    disticts: {
      type: new _graphql.GraphQLList(_District.districtType),
      resolve: city => city.Districts
    }
  })
});
exports.cityType = cityType;
var _default = null;
exports.default = _default;