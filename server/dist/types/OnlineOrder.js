"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.onlineOrderType = void 0;

var _graphql = require("graphql");

var _moment = _interopRequireDefault(require("moment"));

var _Store = require("./Store");

var _Member = require("./Member");

var _Order = require("./Order");

const onlineOrderType = new _graphql.GraphQLObjectType({
  name: 'OnlineOrder',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    picture: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    phone: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    email: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    date: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString),
      resolve: order => order.date && (0, _moment.default)(order.date).format('YYYY/MM/DD')
    },
    time: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    store: {
      type: _Store.storeType,
      resolve: order => order.Store
    },
    price: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    employee: {
      type: _Member.memberType,
      resolve: order => order.BookedMember
    },
    createdAt: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString),
      resolve: order => order.createdAt && (0, _moment.default)(order.createdAt).format('YYYY/MM/DD HH:mm')
    },
    payedStatus: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    shippingStatus: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    isShared: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    order: {
      type: _Order.orderType,
      resolve: x => x.Order
    }
  })
});
exports.onlineOrderType = onlineOrderType;
var _default = onlineOrderType;
exports.default = _default;