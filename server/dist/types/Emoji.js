"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.emojiCategoryType = exports.emojiType = void 0;

var _graphql = require("graphql");

const emojiType = new _graphql.GraphQLObjectType({
  name: 'Emoji',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    picture: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.emojiType = emojiType;
const emojiCategoryType = new _graphql.GraphQLObjectType({
  name: 'EmojiCategorty',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.emojiCategoryType = emojiCategoryType;