"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.activityToggleResponseType = exports.activityDeleteResponseType = exports.activityFragmentInputType = exports.activityType = exports.activityListType = exports.activityFragmentType = exports.categoryType = void 0;

var _graphql = require("graphql");

const categoryType = new _graphql.GraphQLObjectType({
  name: 'ActivityCategory',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    desktopImg: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    mobileImg: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.categoryType = categoryType;
const activityFragmentType = new _graphql.GraphQLObjectType({
  name: 'ActivityFragment',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    type: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    title: {
      type: _graphql.GraphQLString
    },
    content: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.activityFragmentType = activityFragmentType;
const activityListType = new _graphql.GraphQLObjectType({
  name: 'Activities',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    desktopImg: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    mobileImg: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    description: {
      type: _graphql.GraphQLString
    },
    isActived: {
      type: _graphql.GraphQLBoolean
    }
  })
});
exports.activityListType = activityListType;
const activityType = new _graphql.GraphQLObjectType({
  name: 'Activity',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    desktopImg: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    mobileImg: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    description: {
      type: _graphql.GraphQLString
    },
    fragments: {
      type: new _graphql.GraphQLList(activityFragmentType)
    }
  })
});
exports.activityType = activityType;
const activityFragmentInputType = new _graphql.GraphQLInputObjectType({
  name: 'ActivityFragmentInput',
  fields: () => ({
    type: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString),
      desciption: 'TEXT'
    },
    title: {
      type: _graphql.GraphQLString
    },
    content: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.activityFragmentInputType = activityFragmentInputType;
const activityDeleteResponseType = new _graphql.GraphQLObjectType({
  name: 'ActivityDeleteResponseType',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.activityDeleteResponseType = activityDeleteResponseType;
const activityToggleResponseType = new _graphql.GraphQLObjectType({
  name: 'ActivityToggleResponseType',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.activityToggleResponseType = activityToggleResponseType;
var _default = categoryType;
exports.default = _default;