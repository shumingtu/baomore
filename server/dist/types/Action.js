"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.actionType = void 0;

var _graphql = require("graphql");

const actionType = new _graphql.GraphQLObjectType({
  name: 'Action',
  fields: () => ({
    code: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    description: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.actionType = actionType;
var _default = null;
exports.default = _default;