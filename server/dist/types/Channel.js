"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.ChannelType = void 0;

var _graphql = require("graphql");

const ChannelType = new _graphql.GraphQLObjectType({
  name: 'Channel',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.ChannelType = ChannelType;
var _default = null;
exports.default = _default;