"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.announcementDeleteResponseType = exports.announcementType = void 0;

var _graphql = require("graphql");

var _moment = _interopRequireDefault(require("moment"));

const announcementType = new _graphql.GraphQLObjectType({
  name: 'Announcement',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    title: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    content: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    createdAt: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString),
      resolve: announcement => (0, _moment.default)(announcement.createdAt).format('YYYY/MM/DD HH:mm:ss')
    }
  })
});
exports.announcementType = announcementType;
const announcementDeleteResponseType = new _graphql.GraphQLObjectType({
  name: 'AnnouncementDeleteResponse',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.announcementDeleteResponseType = announcementDeleteResponseType;
var _default = null;
exports.default = _default;