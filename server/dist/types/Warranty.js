"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.memberWarrantyType = void 0;

var _graphql = require("graphql");

var _moment = _interopRequireDefault(require("moment"));

const memberWarrantyType = new _graphql.GraphQLObjectType({
  name: 'MemberWarranty',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    service: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    phoneModel: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    store: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    VAT: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    IME: {
      type: _graphql.GraphQLString
    },
    phoneLastSix: {
      type: _graphql.GraphQLString,
      resolve: warranty => warranty.phone && warranty.phone.slice(-6) || null
    },
    isApproved: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    employeeCode: {
      type: _graphql.GraphQLString,
      resolve: warranty => warranty.approverSN
    },
    createdAt: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString),
      resolve: warranty => warranty.createdAt && (0, _moment.default)(warranty.createdAt).format('YYYY/MM/DD HH:mm')
    }
  })
});
exports.memberWarrantyType = memberWarrantyType;
var _default = null;
exports.default = _default;