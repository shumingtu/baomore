"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.settingType = exports.vendorSettingType = void 0;

var _graphql = require("graphql");

const vendorSettingType = new _graphql.GraphQLObjectType({
  name: 'VendorSetting',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    type: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    value: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    VendorId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    MemberId: {
      type: _graphql.GraphQLInt
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.vendorSettingType = vendorSettingType;
const settingType = new _graphql.GraphQLInputObjectType({
  name: 'SettingObject',
  fields: () => ({
    type: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    value: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    MemberId: {
      type: _graphql.GraphQLInt
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.settingType = settingType;
var _default = null;
exports.default = _default;