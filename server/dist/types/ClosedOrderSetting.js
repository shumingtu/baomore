"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.ClosedOrderSettingDeleteResponseType = exports.ClosedOrderSettingType = void 0;

var _graphql = require("graphql");

var _PNTable = require("./PNTable");

const ClosedOrderSettingType = new _graphql.GraphQLObjectType({
  name: 'ClosedOrderSetting',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    closedDay: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    closedTime: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    PNTable: {
      type: _PNTable.PNTableType
    }
  })
});
exports.ClosedOrderSettingType = ClosedOrderSettingType;
const ClosedOrderSettingDeleteResponseType = new _graphql.GraphQLObjectType({
  name: 'ClosedOrderSettingDeleteResponseType',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.ClosedOrderSettingDeleteResponseType = ClosedOrderSettingDeleteResponseType;
var _default = null;
exports.default = _default;