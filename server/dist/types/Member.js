"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.memberToggleArchiveResponseType = exports.memberDeleteResponseType = exports.employeePerformanceType = exports.employeePunchType = exports.managerPunchResponseType = exports.employeePunchResponseType = exports.employeeLoginType = exports.memberMeType = exports.memberType = void 0;

var _graphql = require("graphql");

var _moment = _interopRequireDefault(require("moment"));

var _Store = require("./Store");

var _Area = require("./Area");

var _MemberStore = require("./MemberStore");

var _Warranty = require("./Warranty");

var _OnlineOrder = require("./OnlineOrder.js");

var _Role = require("./Role.js");

const memberType = new _graphql.GraphQLObjectType({
  name: 'Member',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    serialNumber: {
      type: _graphql.GraphQLString
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    AreaId: {
      type: _graphql.GraphQLInt
    },
    lineId: {
      type: _graphql.GraphQLString
    },
    gender: {
      type: _graphql.GraphQLString
    },
    birthday: {
      type: _graphql.GraphQLString,
      resolve: member => member.birthday && (0, _moment.default)(member.birthday).format('YYYY-MM-DD')
    },
    avatar: {
      type: _graphql.GraphQLString
    },
    phone: {
      type: _graphql.GraphQLString
    },
    email: {
      type: _graphql.GraphQLString
    },
    area: {
      type: _Area.AreaType,
      resolve: member => member.Area
    },
    lineAccount: {
      type: _graphql.GraphQLString
    },
    archive: {
      type: _graphql.GraphQLBoolean
    },
    director: {
      type: memberType,
      resolve: member => {
        if (member.getMember) return member.getMember();
        return member.director;
      }
    },
    memberStore: {
      type: _MemberStore.memberStoreType,
      resolve: member => member.MemberStore
    },
    memberWarranties: {
      type: new _graphql.GraphQLList(_Warranty.memberWarrantyType),
      resolve: member => member.memberWarranties
    },
    onlineOrders: {
      type: new _graphql.GraphQLList(_OnlineOrder.onlineOrderType),
      resolve: member => member.onlineOrders
    },
    roles: {
      type: new _graphql.GraphQLList(_Role.roleType),
      resolve: member => member.Roles
    }
  })
});
exports.memberType = memberType;
const memberMeType = new _graphql.GraphQLObjectType({
  name: 'MemberMe',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    avatar: {
      type: _graphql.GraphQLString
    },
    phone: {
      type: _graphql.GraphQLString
    },
    email: {
      type: _graphql.GraphQLString
    },
    gender: {
      type: _graphql.GraphQLString
    },
    birthday: {
      type: _graphql.GraphQLString,
      resolve: member => member.birthday && (0, _moment.default)(member.birthday).format('YYYY-MM-DD')
    },
    storedDesignJSON: {
      type: _graphql.GraphQLString
    },
    AreaId: {
      type: _graphql.GraphQLInt
    }
  })
});
exports.memberMeType = memberMeType;
const employeeLoginType = new _graphql.GraphQLObjectType({
  name: 'EmployeeLogin',
  fields: () => ({
    member: {
      type: new _graphql.GraphQLNonNull(memberType)
    },
    accessToken: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.employeeLoginType = employeeLoginType;
const employeePunchResponseType = new _graphql.GraphQLObjectType({
  name: 'EmployeePunchResponse',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    }
  })
});
exports.employeePunchResponseType = employeePunchResponseType;
const managerPunchResponseType = new _graphql.GraphQLObjectType({
  name: 'ManagerPunchResponse',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    }
  })
});
exports.managerPunchResponseType = managerPunchResponseType;
const employeePunchType = new _graphql.GraphQLObjectType({
  name: 'EmployeePunch',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    punchTime: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString),
      resolve: punch => (0, _moment.default)(punch.punchTime).format('YYYY/MM/DD HH:mm:ss')
    },
    store: {
      type: new _graphql.GraphQLNonNull(_Store.storeType),
      resolve: punch => punch.Store
    },
    type: {
      type: _graphql.GraphQLString
    },
    member: {
      type: memberType,
      resolve: punch => punch.Member
    }
  })
});
exports.employeePunchType = employeePunchType;
const employeePerformanceType = new _graphql.GraphQLObjectType({
  name: 'EmployeePerformance',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt),
      description: '包膜師ID'
    },
    targets: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLInt))
    },
    amounts: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLInt))
    },
    sales: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLInt))
    },
    achieveRate: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    },
    asp: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    },
    mom: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    },
    workHours: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    },
    expectAchieves: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    },
    expectProfits: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLFloat))
    }
  })
});
exports.employeePerformanceType = employeePerformanceType;
const memberDeleteResponseType = new _graphql.GraphQLObjectType({
  name: 'MemberDeleteResponseType',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.memberDeleteResponseType = memberDeleteResponseType;
const memberToggleArchiveResponseType = new _graphql.GraphQLObjectType({
  name: 'MemberToggleArchiveResponseType',
  fields: {
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  }
});
exports.memberToggleArchiveResponseType = memberToggleArchiveResponseType;