"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.storeToggleArchiveResponseType = exports.storeDeleteResponseType = exports.storeType = exports.locationType = void 0;

var _graphql = require("graphql");

var _Area = require("./Area");

var _Channel = require("./Channel");

var _Member = require("./Member");

var _District = require("./District");

const locationType = new _graphql.GraphQLObjectType({
  name: 'location',
  fields: {
    lat: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLFloat)
    },
    lon: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLFloat)
    }
  }
});
exports.locationType = locationType;
const storeType = new _graphql.GraphQLObjectType({
  name: 'Store',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    phone: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    address: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    latitude: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLFloat)
    },
    longitude: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLFloat)
    },
    manager: {
      type: _graphql.GraphQLString
    },
    archive: {
      type: _graphql.GraphQLBoolean
    },
    area: {
      type: _Area.AreaType,
      resolve: store => store.Area
    },
    channel: {
      type: _Channel.ChannelType,
      resolve: store => store.Channel
    },
    members: {
      type: (0, _graphql.GraphQLList)(_Member.memberType),
      resolve: store => store.Members
    },
    district: {
      type: _District.districtType,
      resolve: store => store.District
    }
  })
});
exports.storeType = storeType;
const storeDeleteResponseType = new _graphql.GraphQLObjectType({
  name: 'StoreDeleteResponseType',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.storeDeleteResponseType = storeDeleteResponseType;
const storeToggleArchiveResponseType = new _graphql.GraphQLObjectType({
  name: 'StoreToggleArchiveResponseType',
  fields: {
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  }
});
exports.storeToggleArchiveResponseType = storeToggleArchiveResponseType;
var _default = null;
exports.default = _default;