"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.landingDeleteResponseType = exports.listType = void 0;

var _graphql = require("graphql");

const listType = new _graphql.GraphQLObjectType({
  name: 'Landings',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    desktopImg: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    mobileImg: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    link: {
      type: _graphql.GraphQLString
    }
  })
});
exports.listType = listType;
const landingDeleteResponseType = new _graphql.GraphQLObjectType({
  name: 'LandingDeleteResponseType',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.landingDeleteResponseType = landingDeleteResponseType;
var _default = listType;
exports.default = _default;