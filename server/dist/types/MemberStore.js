"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.memberStoreType = void 0;

var _graphql = require("graphql");

const memberStoreType = new _graphql.GraphQLObjectType({
  name: 'MemberStore',
  fields: () => ({
    type: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    MemberId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    StoreId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  })
});
exports.memberStoreType = memberStoreType;
var _default = null;
exports.default = _default;