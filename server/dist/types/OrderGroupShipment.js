"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.OrderGroupShipmentUpdateType = exports.OrderGroupShipmentQueryType = void 0;

var _graphql = require("graphql");

var _moment = _interopRequireDefault(require("moment"));

var _PNTable = require("./PNTable");

var _Order = require("./Order");

const OrderGroupShipmentQueryType = new _graphql.GraphQLObjectType({
  name: 'OrderGroupShipmentQuery',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    SN: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    startDate: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString),
      resolve: x => x.startDate && (0, _moment.default)(x.startDate).format('YYYY/MM/DD')
    },
    endDate: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString),
      resolve: x => x.endDate && (0, _moment.default)(x.endDate).format('YYYY/MM/DD')
    },
    createdAt: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString),
      resolve: x => x.createdAt && (0, _moment.default)(x.createdAt).format('YYYY/MM/DD')
    },
    PNTable: {
      type: _PNTable.PNTableType
    },
    Orders: {
      type: new _graphql.GraphQLList(_Order.orderType)
    },
    totalQuantity: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  })
});
exports.OrderGroupShipmentQueryType = OrderGroupShipmentQueryType;
const OrderGroupShipmentUpdateType = new _graphql.GraphQLObjectType({
  name: 'OrderGroupShipment',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.OrderGroupShipmentUpdateType = OrderGroupShipmentUpdateType;
var _default = null;
exports.default = _default;