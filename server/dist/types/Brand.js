"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.BrandDeleteResponseType = exports.BrandModelInputType = exports.BrandModelColorResponseType = exports.BrandModelResponseType = exports.BrandResponseType = exports.BrandModelColorType = exports.BrandModelType = exports.BrandType = void 0;

var _graphql = require("graphql");

const BrandType = new _graphql.GraphQLObjectType({
  name: 'Brand',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.BrandType = BrandType;
const BrandModelType = new _graphql.GraphQLObjectType({
  name: 'BrandModel',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.BrandModelType = BrandModelType;
const BrandModelColorType = new _graphql.GraphQLObjectType({
  name: 'BrandModelColor',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    phoneBg: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    phoneMask: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    phoneCover: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    brand: {
      type: BrandType,
      resolve: color => color.BrandModel.Brand
    },
    model: {
      type: BrandModelType,
      resolve: color => color.BrandModel
    }
  })
});
exports.BrandModelColorType = BrandModelColorType;
const BrandResponseType = new _graphql.GraphQLObjectType({
  name: 'BrandResponse',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    createdAt: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    updatedAt: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.BrandResponseType = BrandResponseType;
const BrandModelResponseType = new _graphql.GraphQLObjectType({
  name: 'BrandModelResponse',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    createdAt: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    updatedAt: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    BrandId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  })
});
exports.BrandModelResponseType = BrandModelResponseType;
const BrandModelColorResponseType = new _graphql.GraphQLObjectType({
  name: 'BrandModelColorResponse',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    createdAt: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    updatedAt: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    BrandModelId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    phoneBg: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    phoneMask: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    phoneCover: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.BrandModelColorResponseType = BrandModelColorResponseType;
const BrandModelInputType = new _graphql.GraphQLInputObjectType({
  name: 'BrandModelInput',
  fields: () => ({
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    colors: {
      type: new _graphql.GraphQLNonNull(new _graphql.GraphQLList(_graphql.GraphQLString))
    }
  })
});
exports.BrandModelInputType = BrandModelInputType;
const BrandDeleteResponseType = new _graphql.GraphQLObjectType({
  name: 'BrandDeleteResponseType',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.BrandDeleteResponseType = BrandDeleteResponseType;
var _default = BrandType;
exports.default = _default;