"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.listType = void 0;

var _graphql = require("graphql");

const listType = new _graphql.GraphQLObjectType({
  name: 'SocialCategory',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    name: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    link: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.listType = listType;
var _default = null;
exports.default = _default;