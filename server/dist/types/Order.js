"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.orderDeleteResponseType = exports.orderType = exports.orderQRCodeType = void 0;

var _graphql = require("graphql");

var _Member = require("./Member");

var _PNTable = require("./PNTable");

var _Store = require("./Store");

var _OrderGroupShipment = require("./OrderGroupShipment");

var _OnlineOrder = require("./OnlineOrder");

const orderQRCodeType = new _graphql.GraphQLObjectType({
  name: 'OrderQRCode',
  fields: {
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    qrcode: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  }
});
exports.orderQRCodeType = orderQRCodeType;
const orderType = new _graphql.GraphQLObjectType({
  name: 'Order',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    quantity: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    orderSN: {
      type: _graphql.GraphQLString
    },
    orderSource: {
      type: _graphql.GraphQLString
    },
    Member: {
      type: _Member.memberType,
      resolve: order => order.Member
    },
    pnTable: {
      type: _PNTable.PNTableType,
      resolve: order => order.PNTable
    },
    store: {
      type: _Store.storeType,
      resolve: order => order.Store
    },
    orderGropShipment: {
      type: _OrderGroupShipment.OrderGroupShipmentQueryType,
      resolve: order => order.OrderGroupShipment
    },
    onlineOrder: {
      type: _OnlineOrder.onlineOrderType,
      resolve: order => order.OnlineOrder
    }
  })
});
exports.orderType = orderType;
const orderDeleteResponseType = new _graphql.GraphQLObjectType({
  name: 'OrderDeleteResponse',
  fields: () => ({
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    status: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean)
    },
    message: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  })
});
exports.orderDeleteResponseType = orderDeleteResponseType;
var _default = null;
exports.default = _default;