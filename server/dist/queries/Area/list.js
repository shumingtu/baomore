"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.listArea = void 0;

var _graphql = require("graphql");

var _db = require("../../db");

var _Area = require("../../types/Area");

const listArea = {
  type: new _graphql.GraphQLList(_Area.AreaType),
  resolve: async () => _db.db.models.Area.findAll()
};
exports.listArea = listArea;
var _default = null;
exports.default = _default;