"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.announcement = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

var _Announcement = require("../../types/Announcement");

const announcement = {
  type: _Announcement.announcementType,
  args: {
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.NOTIFICATION_INFO
  })((_, args) => _db.db.models.Announcement.scope({
    method: ['search', args]
  }).findOne())
};
exports.announcement = announcement;
var _default = null;
exports.default = _default;