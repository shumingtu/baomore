"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.announcements = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

var _Announcement = require("../../types/Announcement");

const announcements = {
  type: new _graphql.GraphQLList(_Announcement.announcementType),
  args: {
    keyword: {
      type: _graphql.GraphQLString
    },
    startDate: {
      type: _graphql.GraphQLString
    },
    endDate: {
      type: _graphql.GraphQLString
    },
    limit: {
      type: _graphql.GraphQLInt,
      defaultValue: 10
    },
    offset: {
      type: _graphql.GraphQLInt,
      defaultValue: 0
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.NOTIFICATION_INFO
  })((_, args) => _db.db.models.Announcement.scope({
    method: ['search', args]
  }, {
    method: ['pages', args.limit, args.offset]
  }, 'order').findAll())
};
exports.announcements = announcements;
var _default = null;
exports.default = _default;