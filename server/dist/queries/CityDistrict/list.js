"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.districtList = exports.cityDistrictList = void 0;

var _graphql = require("graphql");

var _db = require("../../db");

var _CityDistrict = require("../../types/CityDistrict");

var _District = require("../../types/District");

const cityDistrictList = {
  type: new _graphql.GraphQLList(_CityDistrict.cityType),
  resolve: () => _db.db.models.City.scope('districts').findAll()
};
exports.cityDistrictList = cityDistrictList;
const districtList = {
  type: new _graphql.GraphQLList(_District.districtType),
  args: {
    cityId: {
      type: _graphql.GraphQLInt
    }
  },
  resolve: async (_, args) => {
    const {
      cityId
    } = args;
    const whereClause = {};
    if (cityId) whereClause.CityId = cityId;
    return _db.db.models.District.findAll({
      where: whereClause
    });
  }
};
exports.districtList = districtList;