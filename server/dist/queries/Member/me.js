"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _db = require("../../db");

var _Member = require("../../types/Member.js");

var _General = require("../../errors/General");

var _default = {
  type: new _graphql.GraphQLNonNull(_Member.memberMeType),
  resolve: async (n, args, {
    authPayload
  }) => {
    if (!authPayload) {
      throw new _General.PermissionError();
    }

    const {
      id
    } = authPayload;
    const memberMe = await _db.db.models.Member.getMe(id);
    if (!memberMe) throw new _General.MemberNotFoundError();
    return memberMe;
  }
};
exports.default = _default;