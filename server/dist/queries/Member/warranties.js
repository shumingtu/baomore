"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _Warranty = require("../../types/Warranty.js");

var _General = require("../../errors/General");

var _default = {
  type: new _graphql.GraphQLList(_Warranty.memberWarrantyType),
  args: {
    limit: {
      type: _graphql.GraphQLInt,
      defaultValue: 10
    },
    offset: {
      type: _graphql.GraphQLInt,
      defaultValue: 0
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.NORMAL_MEMBER
  })(async (n, args, {
    authPayload
  }) => {
    const {
      id
    } = authPayload;
    const records = await _db.db.models.MemberWarranty.scope({
      method: ['pages', args.limit, args.offset]
    }).findAll({
      where: {
        MemberId: id
      },
      order: [['createdAt', 'DESC']]
    });
    return records;
  })
};
exports.default = _default;