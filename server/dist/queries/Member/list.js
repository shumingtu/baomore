"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.adminPunchList = exports.punchlist = exports.employeeMemberlist = exports.allMemberList = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _Member = require("../../types/Member");

var _roleActions = require("../../../shared/roleActions");

var _Member2 = require("../../errors/Member");

const allMemberList = {
  type: new _graphql.GraphQLList(_Member.memberType),
  args: {
    memberId: {
      type: _graphql.GraphQLInt
    },
    keyword: {
      type: _graphql.GraphQLString
    },
    limit: {
      type: _graphql.GraphQLInt
    },
    offset: {
      type: _graphql.GraphQLInt
    },
    roleId: {
      type: _graphql.GraphQLInt
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member2.PermissionError(),
    actions: _roleActions.actions.ADMIN_MEMBER_MANAGE
  })(async (_, args) => {
    const members = await _db.db.models.Member.scope({
      method: ['search', args]
    }, args.limit && args.offset !== undefined ? {
      method: ['pages', args.limit, args.offset]
    } : {}).findAll({
      include: [{
        model: _db.db.models.Role.scope({
          method: ['search', args]
        })
      }, {
        model: _db.db.models.Area
      }]
    });
    return members;
  })
};
exports.allMemberList = allMemberList;
const employeeMemberlist = {
  type: new _graphql.GraphQLList(_Member.memberType),
  args: {
    memberId: {
      type: _graphql.GraphQLInt
    },
    storeId: {
      type: _graphql.GraphQLInt
    },
    areaId: {
      type: _graphql.GraphQLInt
    },
    employeeType: {
      type: _graphql.GraphQLString
    },
    limit: {
      type: _graphql.GraphQLInt
    },
    offset: {
      type: _graphql.GraphQLInt
    },
    keyword: {
      type: _graphql.GraphQLString
    },
    directorId: {
      type: _graphql.GraphQLInt
    },
    lineIdRequired: {
      type: _graphql.GraphQLBoolean,
      defaultValue: false
    },
    isForClient: {
      type: _graphql.GraphQLBoolean,
      defaultValue: false
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member2.PermissionError(),
    actions: _roleActions.actions.EMPLOYEE_INFO
  })(async (_, args) => _db.db.models.Member.scope({
    method: ['employee', args]
  }, {
    method: ['search', args]
  }, args.limit && args.offset !== undefined ? {
    method: ['pages', args.limit, args.offset]
  } : {}, 'warranty', 'onlineOrderStore').findAll({
    order: ['id']
  }))
};
exports.employeeMemberlist = employeeMemberlist;
const punchlist = {
  type: new _graphql.GraphQLList(_Member.employeePunchType),
  args: {
    startDate: {
      type: _graphql.GraphQLString
    },
    endDate: {
      type: _graphql.GraphQLString
    },
    employeeId: {
      type: _graphql.GraphQLInt
    },
    storeId: {
      type: _graphql.GraphQLInt
    },
    limit: {
      type: _graphql.GraphQLInt,
      defaultValue: 10
    },
    offset: {
      type: _graphql.GraphQLInt,
      defaultValue: 0
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member2.PermissionError(),
    actions: _roleActions.actions.CHECKIN
  })(async (_, {
    startDate,
    endDate,
    storeId,
    employeeId,
    limit,
    offset
  }, {
    authPayload
  }) => _db.db.models.MemberPunchRecord.scope({
    method: ['search', {
      storeId,
      memberId: employeeId || authPayload.id,
      startDate,
      endDate
    }]
  }, {
    method: ['pages', limit, offset]
  }, 'store').findAll({
    order: [['punchTime', 'DESC']]
  }))
};
exports.punchlist = punchlist;
const adminPunchList = {
  type: new _graphql.GraphQLList(_Member.employeePunchType),
  args: {
    startDate: {
      type: _graphql.GraphQLString
    },
    endDate: {
      type: _graphql.GraphQLString
    },
    employeeId: {
      type: _graphql.GraphQLInt
    },
    limit: {
      type: _graphql.GraphQLInt,
      defaultValue: 10
    },
    offset: {
      type: _graphql.GraphQLInt,
      defaultValue: 0
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _Member2.PermissionError(),
    actions: _roleActions.actions.ADMIN_MEMBER_MANAGE
  })(async (_, {
    startDate,
    endDate,
    employeeId,
    limit,
    offset
  }) => _db.db.models.MemberPunchRecord.scope({
    method: ['search', {
      startDate,
      endDate,
      memberId: employeeId || null
    }]
  }, {
    method: ['pages', limit, offset]
  }, 'store', 'member').findAll({
    order: [['punchTime', 'DESC'], ['MemberId', 'DESC']]
  }))
};
exports.adminPunchList = adminPunchList;
var _default = employeeMemberlist;
exports.default = _default;