"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.performance = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _lodash = _interopRequireDefault(require("lodash"));

var _moment = _interopRequireDefault(require("moment"));

var _sequelize = require("sequelize");

var _db = require("../../db");

var _Member = require("../../types/Member");

var _General = require("../../errors/General");

var _roleActions = require("../../../shared/roleActions");

var _performanceCalculator = require("../../helpers/performanceCalculator.js");

const performance = {
  type: _Member.employeePerformanceType,
  args: {
    startDate: {
      type: _graphql.GraphQLString
    },
    endDate: {
      type: _graphql.GraphQLString
    },
    employeeId: {
      type: _graphql.GraphQLInt
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.PERFORMANCE_INFO
  })(async (_, args, {
    authPayload
  }) => {
    const {
      startDate,
      endDate,
      employeeId
    } = args;
    const {
      id
    } = authPayload;
    const startMonth = (0, _moment.default)().startOf('month').format('YYYY-MM-DD');
    const endMonth = (0, _moment.default)().endOf('month').format('YYYY-MM-DD');
    const parameter = {
      startDate: startDate || startMonth,
      endDate: endDate || endMonth,
      employeeId: employeeId || id
    };
    const rawResult = await _db.db.models.BehaviorRecord.scope({
      method: ['searchForPerformance', parameter]
    }, {
      method: ['performanceMemberAssociation', {
        employeeType: '包膜師'
      }]
    }, {
      method: ['performancePNTableAssociation']
    }).findAll({
      attributes: ['id', 'startTime', 'endTime', 'price', 'BehaviorId', 'MemberId', 'PNTableId'],
      where: {
        BehaviorId: 1
      }
    });
    const memberSchedule = await _db.db.models.MemberSchedule.scope({
      method: ['search', parameter]
    }).findAll({
      attributes: ['id', 'date', 'status', 'MemberId'],
      where: {
        [_sequelize.Op.or]: [{
          status: '4'
        }, {
          status: '004'
        }]
      }
    });
    const performanceSettings = await _db.db.models.PerformanceSetting.findAll({
      attributes: ['id', 'PNCategoryId', 'targetAmount', 'commission']
    });
    const lastMonthStartTime = (0, _moment.default)(startDate || startMonth).subtract(1, 'months').format('YYYY-MM-DD');
    const lastMonthEndTime = (0, _moment.default)(endDate || endMonth).subtract(1, 'months').format('YYYY-MM-DD');
    const lastMonthArgs = { ...parameter,
      startDate: lastMonthStartTime,
      endDate: lastMonthEndTime
    };
    const lastMonthRecord = await _db.db.models.BehaviorRecord.scope({
      method: ['searchForPerformance', lastMonthArgs]
    }, {
      method: ['performanceMemberAssociation', {
        employeeType: '包膜師'
      }]
    }, {
      method: ['performancePNTableAssociation']
    }).findAll({
      attributes: ['id', 'price', 'BehaviorId', 'MemberId', 'PNTableId'],
      where: {
        BehaviorId: 1
      }
    });
    const totalDays = ((0, _moment.default)(endDate || endMonth) - (0, _moment.default)(startDate || startMonth)) / 86400000 + 1;

    const groupByMember = _lodash.default.groupBy(rawResult, 'MemberId');

    const calculatedResult = await Object.keys(groupByMember).map(memberId => async results => {
      const memberRecord = groupByMember[memberId];
      const lastMonthRecordByMember = lastMonthRecord.filter(x => x.MemberId === parseInt(memberId, 10));
      const workDays = memberSchedule.filter(x => x.MemberId === parseInt(memberId, 10)).length;
      const recordGroupByPNCategoryId = await (0, _performanceCalculator.getRecordGroupByPNCategoryId)(memberRecord, performanceSettings, lastMonthRecordByMember, totalDays, workDays, [parseInt(memberId, 10)]);
      return [...results, {
        workDays,
        member: memberRecord[0].Member,
        performance: recordGroupByPNCategoryId
      }];
    }).reduce((prev, next) => prev.then(next), Promise.resolve([]));
    if (!calculatedResult.length) return null;
    const materialSetting = performanceSettings.find(x => x.PNCategoryId === 1);
    const protectorSetting = performanceSettings.find(x => x.PNCategoryId === 2);
    const materialPerformance = calculatedResult[0].performance.find(x => x.pnCategory.id === 1);
    const protectorPerformance = calculatedResult[0].performance.find(x => x.pnCategory.id === 2);
    const clientCalculatedResult = (0, _performanceCalculator.getClientCalResult)(materialPerformance, protectorPerformance, materialSetting, protectorSetting, totalDays);
    return {
      id: calculatedResult[0].member.id,
      ...clientCalculatedResult
    };
  })
};
exports.performance = performance;
var _default = performance;
exports.default = _default;