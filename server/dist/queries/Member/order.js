"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _OnlineOrder = require("../../types/OnlineOrder.js");

var _General = require("../../errors/General");

var _default = {
  type: new _graphql.GraphQLList(_OnlineOrder.onlineOrderType),
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.NORMAL_MEMBER
  })(async (n, args, {
    authPayload
  }) => {
    if (!authPayload) {
      throw new _General.PermissionError();
    }

    const memberMe = await _db.db.models.Member.getMe(authPayload.id);
    if (!memberMe) throw new _General.MemberNotFoundError();
    const {
      id
    } = memberMe;
    const orders = await _db.db.models.OnlineOrder.scope('store', 'order').findAll({
      where: {
        OwnerId: id
      },
      order: [['createdAt', 'DESC']]
    });
    return orders;
  })
};
exports.default = _default;