"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.meScheduleDateAssignments = exports.meScheduleDates = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _Schedule = require("../../types/Schedule");

var _General = require("../../errors/General");

var _roleActions = require("../../../shared/roleActions");

const meScheduleDates = {
  type: new _graphql.GraphQLList(_graphql.GraphQLString),
  args: {
    startDate: {
      type: _graphql.GraphQLString
    },
    endDate: {
      type: _graphql.GraphQLString
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.STROKE_INFO
  })(async (_, args, {
    authPayload
  }) => {
    const results = await _db.db.models.OnlineOrder.scope({
      method: ['search', args]
    }, {
      method: ['bookedMember', authPayload.id]
    }, 'payedAndConfirmed').findAll();
    return results.map(result => result.date);
  })
};
exports.meScheduleDates = meScheduleDates;
const meScheduleDateAssignments = {
  type: new _graphql.GraphQLList(_Schedule.memberAssignmentType),
  args: {
    date: {
      type: _graphql.GraphQLString
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.STROKE_INFO
  })(async (_, args, {
    authPayload
  }) => {
    const results = await _db.db.models.OnlineOrder.scope({
      method: ['search', args]
    }, 'store', 'order', {
      method: ['bookedMember', authPayload.id]
    }, 'payedAndConfirmed').findAll();
    return results.map(result => ({
      time: result.time,
      order: result
    }));
  })
};
exports.meScheduleDateAssignments = meScheduleDateAssignments;
var _default = meScheduleDates;
exports.default = _default;