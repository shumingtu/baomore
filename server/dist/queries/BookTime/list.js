"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.bookTimeSlotList = exports.alreadyBookedDates = exports.availableBookedTimes = exports.availableBookedDates = void 0;

var _graphql = require("graphql");

var _lodash = _interopRequireDefault(require("lodash"));

var _moment = _interopRequireDefault(require("moment"));

var _db = require("../../db");

var _BookTime = require("../../types/BookTime");

var _General = require("../../errors/General");

var _datesBetweenIntervals = _interopRequireDefault(require("../../helpers/datesBetweenIntervals"));

const availableBookedDates = {
  type: new _graphql.GraphQLList(_BookTime.dateType),
  args: {
    storeId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: () => {
    const dates = (0, _datesBetweenIntervals.default)('2018-12-01', '2018-12-30', 'day');
    return dates;
  }
};
exports.availableBookedDates = availableBookedDates;
const availableBookedTimes = {
  type: new _graphql.GraphQLList(_BookTime.timeType),
  args: {
    storeId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    date: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    employeeId: {
      type: _graphql.GraphQLInt
    }
  },
  resolve: async (_, {
    storeId,
    date,
    employeeId
  }) => {
    const store = await _db.db.models.Store.scope({
      method: ['search', {
        storeId
      }]
    }).findOne();

    if (!store) {
      throw new _General.ResourceNotFoundError('Store Resource Not Found');
    }

    const dbTimes = await _db.db.models.BookTimeSlot.findAll();
    const times = dbTimes.map(dbTime => dbTime.time);

    const rawTimeBudgets = _lodash.default.flatten([times, times]);

    const timeBudgets = _lodash.default.uniq(rawTimeBudgets);

    const storeMemberWhereClause = {};
    if (employeeId) storeMemberWhereClause.id = employeeId;
    const storeMembers = await store.getMembers({
      where: storeMemberWhereClause
    });
    const orders = await _db.db.models.OnlineOrder.scope({
      method: ['search', {
        date,
        bookedMemberIds: storeMembers.map(m => m.id)
      }]
    }).findAll();
    const orderTimes = orders.map(order => order.time);
    const matchTimes = orderTimes.reduce((acc, curr) => {
      const index = acc.findIndex(i => i === curr);

      if (index > -1) {
        return [...acc.slice(0, index), ...acc.slice(index + 1)];
      }

      return acc;
    }, timeBudgets);
    return matchTimes.map(time => ({
      time
    }));
  }
};
exports.availableBookedTimes = availableBookedTimes;
const alreadyBookedDates = {
  type: new _graphql.GraphQLList(_BookTime.dateType),
  args: {
    storeId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    employeeId: {
      type: _graphql.GraphQLInt
    },
    categoryName: {
      type: _graphql.GraphQLString
    }
  },
  resolve: async (_, args) => {
    // two months booked dates
    const dates = (0, _datesBetweenIntervals.default)((0, _moment.default)(), (0, _moment.default)().add(2, 'months'), 'day');
    const store = await _db.db.models.Store.scope({
      method: ['search', args]
    }).findOne();

    if (!store) {
      throw new _General.ResourceNotFoundError('Store Resource Not Found');
    }

    const storeMemberWhereClause = {};
    if (args.employeeId) storeMemberWhereClause.id = args.employeeId;
    const storeMembers = await store.getMembers({
      where: storeMemberWhereClause
    });
    const storeOrders = await _db.db.models.OnlineOrder.scope({
      method: ['search', {
        bookedMemberIds: storeMembers.map(m => m.id),
        bookedDate: dates[0].date,
        bookedEndDate: dates[dates.length - 1].date
      }]
    }).findAll();
    const dayLimit = 9; // if (args.categoryName) {
    //   switch (args.categoryName) {
    //     case '客製化包膜':
    //     case '會員作品共享包膜':
    //       dayLimit = 7;
    //       break;
    //
    //     default:
    //       dayLimit = 3;
    //       break;
    //   }
    // }

    const now = (0, _moment.default)(Date.now()).format('YYYY-MM-DD');
    const limitDayArray = [now];
    Array.from(Array(dayLimit)).forEach((x, idx) => {
      limitDayArray.push((0, _moment.default)(now).add(idx + 1, 'days').format('YYYY-MM-DD'));
    });

    const groupByDates = _lodash.default.groupBy(storeOrders, 'date');

    const storeDateSlotNum = 4;
    const bookedDates = Object.keys(groupByDates).filter(date => groupByDates[date].length >= storeDateSlotNum);
    limitDayArray.forEach(date => {
      const isInclude = bookedDates.includes(date);
      if (!isInclude) bookedDates.push(date);
    });
    return bookedDates.map(date => ({
      date
    }));
  }
};
exports.alreadyBookedDates = alreadyBookedDates;
const bookTimeSlotList = {
  type: new _graphql.GraphQLList(_BookTime.timeType),
  resolve: () => _db.db.models.BookTimeSlot.findAll()
};
exports.bookTimeSlotList = bookTimeSlotList;