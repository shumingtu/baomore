"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.schedulelist = void 0;

var _graphql = require("graphql");

var _lodash = _interopRequireDefault(require("lodash"));

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _Schedule = require("../../types/Schedule");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

const schedulelist = {
  type: new _graphql.GraphQLList(_Schedule.scheduleType),
  args: {
    startDate: {
      type: _graphql.GraphQLString
    },
    endDate: {
      type: _graphql.GraphQLString
    },
    storeId: {
      type: _graphql.GraphQLInt
    },
    bookedMemberId: {
      type: _graphql.GraphQLInt
    },
    limit: {
      type: _graphql.GraphQLInt,
      defaultValue: 10
    },
    offset: {
      type: _graphql.GraphQLInt,
      defaultValue: 0
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.ONLINE_ORDER_MANAGE
  })(async (_, args) => {
    const filterArgs = {
      bookedDate: args.startDate || null,
      bookedEndDate: args.endDate || null,
      storeId: args.storeId || null,
      bookedMemberId: args.bookedMemberId || null
    };
    const results = await _db.db.models.OnlineOrder.scope({
      method: ['search', filterArgs]
    }, 'store', 'bookedMember', 'payedAndConfirmed').findAll({
      order: [['date', 'DESC']]
    });

    const dateAggregates = _lodash.default.groupBy(results, 'date');

    return Object.keys(dateAggregates).map(date => {
      const timeAggregates = _lodash.default.groupBy(dateAggregates[date], 'time');

      return {
        date,
        assignments: Object.keys(timeAggregates).map(time => ({
          time,
          denominator: 4,
          onlineOrders: timeAggregates[time]
        }))
      };
    });
  })
};
exports.schedulelist = schedulelist;
var _default = schedulelist;
exports.default = _default;