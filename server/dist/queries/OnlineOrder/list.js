"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.onlineOrder = exports.onlineOrderlist = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _OnlineOrder = require("../../types/OnlineOrder");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

const onlineOrderlist = {
  type: new _graphql.GraphQLList(_OnlineOrder.onlineOrderType),
  args: {
    keyword: {
      type: _graphql.GraphQLString
    },
    startDate: {
      type: _graphql.GraphQLString
    },
    endDate: {
      type: _graphql.GraphQLString
    },
    payedStatus: {
      type: _graphql.GraphQLString
    },
    shippingStatus: {
      type: _graphql.GraphQLString
    },
    storeId: {
      type: _graphql.GraphQLInt
    },
    bookedMemberId: {
      type: _graphql.GraphQLInt
    },
    limit: {
      type: _graphql.GraphQLInt,
      defaultValue: 10
    },
    offset: {
      type: _graphql.GraphQLInt,
      defaultValue: 0
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.ONLINE_ORDER_MANAGE
  })((_, args) => _db.db.models.OnlineOrder.scope({
    method: ['search', args]
  }, {
    method: ['pages', args.limit, args.offset]
  }, 'store', 'order', 'bookedMember').findAll({
    include: [{
      model: _db.db.models.Order
    }]
  }))
};
exports.onlineOrderlist = onlineOrderlist;
const onlineOrder = {
  type: _OnlineOrder.onlineOrderType,
  args: {
    onlineOrderId: {
      type: _graphql.GraphQLString
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.ONLINE_ORDER_MANAGE
  })((_, args) => _db.db.models.OnlineOrder.scope({
    method: ['search', {
      orderId: args.onlineOrderId
    }]
  }, 'store', 'order', 'bookedMember').findOne())
};
exports.onlineOrder = onlineOrder;
var _default = onlineOrderlist;
exports.default = _default;