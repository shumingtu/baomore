"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.listVendor = void 0;

var _graphql = require("graphql");

var _db = require("../../db");

var _Vendor = require("../../types/Vendor");

const listVendor = {
  type: new _graphql.GraphQLList(_Vendor.VendorType),
  args: {
    id: {
      type: _graphql.GraphQLInt
    },
    limit: {
      type: _graphql.GraphQLInt
    },
    offset: {
      type: _graphql.GraphQLInt
    }
  },
  resolve: async (_, args) => {
    const {
      limit,
      offset
    } = args;
    const vendorList = await _db.db.models.Vendor.scope({
      method: ['search', args]
    }, limit && offset !== undefined ? {
      method: ['pages', limit, offset]
    } : {}).findAll({
      include: [{
        model: _db.db.models.VendorSetting
      }]
    });
    return vendorList;
  }
};
exports.listVendor = listVendor;
var _default = null;
exports.default = _default;