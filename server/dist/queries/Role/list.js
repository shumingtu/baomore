"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.roleList = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _Role = require("../../types/Role");

var _General = require("../../errors/General");

var _roleActions = require("../../../shared/roleActions");

const roleList = {
  type: new _graphql.GraphQLList(_Role.roleType),
  args: {
    limit: {
      type: _graphql.GraphQLInt
    },
    offset: {
      type: _graphql.GraphQLInt
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.ADMIN_MEMBER_MANAGE
  })(async (_, args) => {
    const {
      limit,
      offset
    } = args;
    const result = await _db.db.models.Role.scope('action', limit && offset !== undefined ? {
      method: ['pages', limit, offset]
    } : {}).findAll();
    return result;
  })
};
exports.roleList = roleList;
var _default = roleList;
exports.default = _default;