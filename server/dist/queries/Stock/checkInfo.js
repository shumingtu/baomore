"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _moment = _interopRequireDefault(require("moment"));

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

var _Stock = require("../../errors/Stock");

var _Stock2 = require("../../types/Stock");

var _default = {
  type: new _graphql.GraphQLNonNull(_Stock2.StockCheckResponseType),
  args: {
    checkCode: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.STOCK_INFO
  })(async (_, {
    checkCode
  }, {
    authPayload
  }) => {
    const {
      id
    } = authPayload;
    const targetQRCode = await _db.db.models.OrderQRCode.findOne({
      where: {
        qrcode: checkCode
      },
      include: [{
        model: _db.db.models.Order,
        where: {
          status: 'DISPATCHED',
          EmpolyedMemberId: id
        },
        include: [{
          model: _db.db.models.PNTable.scope('categories')
        }]
      }]
    });

    if (!targetQRCode) {
      throw new _Stock.StockOrderGroupNotFoundError();
    }

    return {
      id: targetQRCode.id,
      pnTable: targetQRCode.Order.PNTable,
      amount: targetQRCode.Order.quantity
    };
  })
};
exports.default = _default;