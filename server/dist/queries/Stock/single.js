"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

var _Stock = require("../../types/Stock");

var _PNTable = require("../../errors/PNTable");

var _default = {
  type: _Stock.StockType,
  args: {
    pnTableId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    employeeId: {
      type: _graphql.GraphQLInt
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.STOCK_INFO
  })(async (_, args, {
    authPayload
  }) => {
    const {
      employeeId
    } = args;
    const pnTable = await _db.db.models.PNTable.scope({
      method: ['search', {
        id: args.pnTableId
      }]
    }, {
      method: ['stocks', employeeId || authPayload.id]
    }, 'categories', 'vendor', 'brandAndModel').findOne();
    if (!pnTable) throw new _PNTable.PNTableItemNotFoundError(); // 看是否有ORDERING狀態的訂單

    const orderingOrder = await _db.db.models.Order.scope('store').findOne({
      where: {
        EmpolyedMemberId: authPayload.id,
        status: 'ORDERING',
        PNTableId: pnTable.id
      }
    });
    return {
      id: pnTable.id,
      pnTable,
      amount: pnTable.MemberStocks[0] && pnTable.MemberStocks[0].storageCount || 0,
      orderingOrder: orderingOrder || null
    };
  })
};
exports.default = _default;