"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.stockList = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

var _Stock = require("../../types/Stock");

const stockList = {
  type: new _graphql.GraphQLList(_Stock.stockListType),
  args: {
    memberId: {
      type: _graphql.GraphQLInt
    },
    keyword: {
      type: _graphql.GraphQLString
    },
    limit: {
      type: _graphql.GraphQLInt,
      defaultValue: 10
    },
    offset: {
      type: _graphql.GraphQLInt,
      defaultValue: 0
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.INVOICING_MANAGE
  })(async (_, args) => {
    const result = await _db.db.models.MemberStock.scope({
      method: ['search', args]
    }, {
      method: ['pnTable', args]
    }, 'member').findAll({
      limit: args.limit,
      offset: args.offset
    });
    return result.map((r, idx) => ({
      id: idx + 1,
      ...r.dataValues
    }));
  })
};
exports.stockList = stockList;
var _default = {
  type: new _graphql.GraphQLList(_Stock.StockType),
  args: {
    code: {
      type: _graphql.GraphQLString
    },
    pnCategoryId: {
      type: _graphql.GraphQLInt
    },
    bigSubCategoryId: {
      type: _graphql.GraphQLInt
    },
    smallSubCategoryId: {
      type: _graphql.GraphQLInt
    },
    keyword: {
      type: _graphql.GraphQLString
    },
    employeeId: {
      type: _graphql.GraphQLInt
    },
    brandId: {
      type: _graphql.GraphQLInt
    },
    brandModelId: {
      type: _graphql.GraphQLInt
    },
    isForPurchase: {
      type: _graphql.GraphQLBoolean,
      defaultValue: false
    },
    limit: {
      type: _graphql.GraphQLInt,
      defaultValue: 10
    },
    offset: {
      type: _graphql.GraphQLInt,
      defaultValue: 0
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.STOCK_INFO
  })(async (_, args, {
    authPayload
  }) => {
    const {
      employeeId,
      isForPurchase
    } = args;
    const pnTables = await _db.db.models.PNTable.scope({
      method: ['search', args]
    }, {
      method: ['brandAndModel', args.brandId, args.brandModelId]
    }, {
      method: ['categories', args.pnCategoryId, args.bigSubCategoryId, args.smallSubCategoryId]
    }, {
      method: ['stocks', employeeId || authPayload.id, !isForPurchase]
    }, {
      method: ['pages', args.limit, args.offset]
    }).findAll({
      include: [{
        model: _db.db.models.Order.scope('store'),
        where: {
          EmpolyedMemberId: authPayload.id,
          status: 'ORDERING'
        },
        required: false
      }]
    });
    const result = pnTables.map(pn => ({
      id: pn.id,
      pnTable: pn,
      amount: pn.MemberStocks[0] && pn.MemberStocks[0].storageCount || 0,
      orderingOrder: pn.Orders[0] || null
    }));
    return result;
  })
};
exports.default = _default;