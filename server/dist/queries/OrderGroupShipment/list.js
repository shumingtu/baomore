"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.listOrderGroupShipment = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _lodash = _interopRequireDefault(require("lodash"));

var _moment = _interopRequireDefault(require("moment"));

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

var _OrderGroupShipment = require("../../types/OrderGroupShipment");

const listOrderGroupShipment = {
  type: new _graphql.GraphQLList(_OrderGroupShipment.OrderGroupShipmentQueryType),
  args: {
    startDate: {
      type: _graphql.GraphQLString
    },
    endDate: {
      type: _graphql.GraphQLString
    },
    shippingStatus: {
      type: _graphql.GraphQLString
    },
    vendorId: {
      type: _graphql.GraphQLInt
    },
    memberId: {
      type: _graphql.GraphQLInt
    },
    areaId: {
      type: _graphql.GraphQLInt
    },
    code: {
      type: _graphql.GraphQLString
    },
    orderSN: {
      type: _graphql.GraphQLString
    },
    limit: {
      type: _graphql.GraphQLInt,
      defaultValue: 10
    },
    offset: {
      type: _graphql.GraphQLInt,
      defaultValue: 0
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.INVOICING_MANAGE
  })(async (_, args) => {
    const {
      limit,
      offset,
      orderSN,
      startDate,
      endDate,
      shippingStatus
    } = args;
    const orderSQL = orderSN ? `AND Orders.orderSN = ${orderSN}` : '';
    const whereClause = [_sequelize.default.literal(`EXISTS (SELECT tmp.id FROM OrderGroupShipments AS tmp INNER JOIN Orders ON Orders.OrderGroupShipmentId = tmp.id WHERE tmp.id = OrderGroupShipment.id AND Orders.orderSource != 'TRANSFER' ${orderSQL})`)];

    if (shippingStatus) {
      whereClause.push({
        status: shippingStatus
      });
    }

    if (startDate) {
      whereClause.push({
        createdAt: {
          [_sequelize.Op.and]: [{
            [_sequelize.Op.gte]: (0, _moment.default)(startDate)
          }, {
            [_sequelize.Op.lt]: endDate && (0, _moment.default)(endDate).add(1, 'd') || (0, _moment.default)().add(1, 'd')
          }]
        }
      });
    }

    if (endDate && !startDate) {
      whereClause.push({
        createdAt: {
          [_sequelize.Op.lt]: (0, _moment.default)(endDate).add(1, 'd')
        }
      });
    }

    const rawResults = await _db.db.models.OrderGroupShipment.scope({
      method: ['pages', limit, offset]
    }, {
      method: ['pnTableAllAssoicateAndOrder', args]
    }).findAll({
      where: {
        [_sequelize.Op.and]: whereClause
      },
      order: [['createdAt', 'DESC']]
    });
    const results = rawResults.map(r => {
      const Orders = (0, _lodash.default)(r.Orders).groupBy(x => x.Member.id).map(value => ({
        id: value[0].id,
        quantity: _lodash.default.sumBy(value, v => v.quantity),
        status: value[0].status,
        Member: value[0].Member,
        orderSN: value[0].orderSN,
        orderSource: value[0].orderSource
      })).value();

      const totalQuantity = _lodash.default.sumBy(Orders, o => o.quantity);

      const isHaveSmallCategory = r.PNTable.PNSubCategory.PNSubCategory;
      return { ...r.dataValues,
        totalQuantity,
        PNTable: { ...r.PNTable.dataValues,
          pnCategory: r.PNTable.PNSubCategory.PNCategory,
          bigSubCategory: isHaveSmallCategory ? r.PNTable.PNSubCategory.PNSubCategory : r.PNTable.PNSubCategory,
          smallSubCategory: isHaveSmallCategory ? r.PNTable.PNSubCategory : null,
          vendor: r.PNTable.Vendor
        },
        Orders
      };
    });
    return results;
  })
};
exports.listOrderGroupShipment = listOrderGroupShipment;
var _default = null;
exports.default = _default;