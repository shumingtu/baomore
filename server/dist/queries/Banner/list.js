"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.banners = void 0;

var _graphql = require("graphql");

var _db = require("../../db");

var _Banner = require("../../types/Banner");

const banners = {
  type: new _graphql.GraphQLList(_Banner.listType),
  args: {
    id: {
      type: _graphql.GraphQLInt
    }
  },
  resolve: (_, {
    id
  }) => {
    if (id) {
      return _db.db.models.Banner.findAll({
        where: {
          id
        }
      });
    }

    return _db.db.models.Banner.findAll();
  }
};
exports.banners = banners;
var _default = banners;
exports.default = _default;