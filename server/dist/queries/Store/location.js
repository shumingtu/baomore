"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.locationInfo = void 0;

var _graphql = require("graphql");

var _maps = _interopRequireDefault(require("@google/maps"));

var _Store = require("../../types/Store");

var _Store2 = require("../../errors/Store");

var _env = require("../../../shared/env");

const googleMapClient = _maps.default.createClient({
  key: _env.GOOGLE_MAP_API_KEY,
  Promise
});

const locationInfo = {
  type: _Store.locationType,
  args: {
    address: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    }
  },
  resolve: async (_, args) => {
    const {
      address
    } = args;
    const googleResponse = await googleMapClient.geocode({
      address
    }).asPromise();

    if (!googleResponse || !googleResponse.json || !googleResponse.json.results || !googleResponse.json.results[0] || !googleResponse.json.results[0].geometry) {
      throw new _Store2.StoreAddressError();
    }

    const {
      location
    } = googleResponse.json.results[0].geometry;
    return {
      lat: location.lat,
      lon: location.lng
    };
  }
};
exports.locationInfo = locationInfo;
var _default = locationInfo;
exports.default = _default;