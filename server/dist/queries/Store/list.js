"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.list = void 0;

var _graphql = require("graphql");

var _sequelize = _interopRequireWildcard(require("sequelize"));

var _db = require("../../db");

var _Store = require("../../types/Store");

const list = {
  type: new _graphql.GraphQLList(_Store.storeType),
  args: {
    cityId: {
      type: _graphql.GraphQLInt
    },
    storeId: {
      type: _graphql.GraphQLInt
    },
    channelId: {
      type: _graphql.GraphQLInt
    },
    districtId: {
      type: _graphql.GraphQLInt
    },
    limit: {
      type: _graphql.GraphQLInt
    },
    offset: {
      type: _graphql.GraphQLInt
    },
    keyword: {
      type: _graphql.GraphQLString
    },
    isForClient: {
      type: _graphql.GraphQLBoolean,
      defaultValue: false
    },
    isForCustomize: {
      type: _graphql.GraphQLBoolean,
      defaultValue: false
    },
    lat: {
      type: _graphql.GraphQLFloat
    },
    lon: {
      type: _graphql.GraphQLFloat
    }
  },
  resolve: async (_, args) => {
    const {
      limit,
      offset,
      isForClient,
      lat,
      lon,
      isForCustomize
    } = args;
    const attributes = Object.keys(_db.db.models.Store.attributes);
    let whereClause = {};

    if (lat && lon) {
      const userPointLocation = _sequelize.default.fn('POINT', lon, lat);

      const storePointLocation = _sequelize.default.fn('POINT', _sequelize.default.col('Store.longitude'), _sequelize.default.col('Store.latitude'));

      const distance = _sequelize.default.fn('ST_DISTANCE_SPHERE', storePointLocation, userPointLocation);

      attributes.push([distance, 'distance']);
      whereClause = _sequelize.default.where(distance, {
        [_sequelize.Op.lte]: 1000 // 1km

      });
    }

    const storeList = await _db.db.models.Store.scope({
      method: ['search', args]
    }, limit && offset !== undefined ? {
      method: ['pages', limit, offset]
    } : {}, 'area', {
      method: ['channel', isForCustomize]
    }, {
      method: ['members', isForClient]
    }, {
      method: ['district', args.cityId]
    }).findAll({
      attributes,
      where: whereClause
    });
    return storeList;
  }
};
exports.list = list;
var _default = list;
exports.default = _default;