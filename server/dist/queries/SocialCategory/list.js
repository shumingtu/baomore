"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.socialCategories = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _graphql = require("graphql");

var _SocialCategory = require("../../types/SocialCategory");

const socialCategories = {
  type: new _graphql.GraphQLList(_SocialCategory.listType),
  args: {
    id: {
      type: _graphql.GraphQLInt
    }
  },
  resolve: (_, {
    id
  }) => {
    const rawData = _fs.default.readFileSync('shared/socialCategory.json');

    if (!rawData) return null;
    const result = JSON.parse(rawData).socialCategory;

    if (id) {
      const singleResult = result.find(x => x.id === id);
      return [singleResult];
    }

    return result;
  }
};
exports.socialCategories = socialCategories;
var _default = socialCategories;
exports.default = _default;