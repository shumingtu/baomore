"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.brandModelColorList = exports.brandModelColor = void 0;

var _graphql = require("graphql");

var _sequelize = require("sequelize");

var _Brand = require("../../types/Brand");

var _db = require("../../db");

const brandModelColor = {
  type: _Brand.BrandModelColorType,
  args: {
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: async (_, {
    id
  }) => _db.db.models.BrandModelColor.scope({
    method: ['search', {
      colorId: id
    }]
  }).findOne()
};
exports.brandModelColor = brandModelColor;
const brandModelColorList = {
  type: new _graphql.GraphQLList(_Brand.BrandModelColorType),
  args: {
    modelId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    keyword: {
      type: _graphql.GraphQLString
    }
  },
  resolve: async (_, {
    modelId,
    keyword
  }) => {
    const whereClause = {
      BrandModelId: modelId
    };

    if (keyword) {
      whereClause.name = {
        [_sequelize.Op.like]: `%${keyword}%`
      };
    }

    const brandColors = await _db.db.models.BrandModelColor.findAll({
      where: whereClause
    });
    return brandColors;
  }
};
exports.brandModelColorList = brandModelColorList;