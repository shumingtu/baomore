"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.brandList = exports.adminBrandModelColorList = void 0;

var _graphql = require("graphql");

var _sequelize = require("sequelize");

var _Brand = require("../../types/Brand");

var _db = require("../../db");

const adminBrandModelColorList = {
  type: new _graphql.GraphQLList(_Brand.BrandModelColorType),
  args: {
    brandId: {
      type: _graphql.GraphQLInt
    },
    modelId: {
      type: _graphql.GraphQLInt
    },
    colorId: {
      type: _graphql.GraphQLInt
    },
    limit: {
      type: _graphql.GraphQLInt,
      defaultValue: 10
    },
    offset: {
      type: _graphql.GraphQLInt,
      defaultValue: 0
    }
  },
  resolve: async (_, args) => _db.db.models.BrandModelColor.scope({
    method: ['pages', args.limit, args.offset]
  }, {
    method: ['search', args]
  }).findAll({
    order: [['BrandModel', 'Brand', 'name', 'ASC']]
  })
};
exports.adminBrandModelColorList = adminBrandModelColorList;
const brandList = {
  type: new _graphql.GraphQLList(_Brand.BrandType),
  args: {
    keyword: {
      type: _graphql.GraphQLString
    }
  },
  resolve: async (_, args) => {
    const {
      keyword
    } = args;
    const whereClause = {};

    if (keyword) {
      whereClause.name = {
        [_sequelize.Op.like]: `%${keyword}%`
      };
    }

    const result = await _db.db.models.Brand.findAll({
      where: whereClause,
      order: [['name', 'ASC']]
    });
    return result;
  }
};
exports.brandList = brandList;