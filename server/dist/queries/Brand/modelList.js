"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _sequelize = require("sequelize");

var _Brand = require("../../types/Brand");

var _db = require("../../db");

var _default = {
  type: new _graphql.GraphQLList(_Brand.BrandModelType),
  args: {
    brandId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    keyword: {
      type: _graphql.GraphQLString
    }
  },
  resolve: async (_, {
    brandId,
    keyword
  }) => {
    const whereClause = {
      BrandId: brandId
    };

    if (keyword) {
      whereClause.name = {
        [_sequelize.Op.like]: `%${keyword}%`
      };
    }

    const models = await _db.db.models.BrandModel.findAll({
      where: whereClause,
      include: [{
        model: _db.db.models.BrandModelColor,
        as: 'colors'
      }],
      order: [['name', 'ASC']]
    });
    return models;
  }
};
exports.default = _default;