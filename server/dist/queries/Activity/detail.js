"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.activity = exports.activityCategory = void 0;

var _graphql = require("graphql");

var _db = require("../../db");

var _Activity = require("../../types/Activity");

const activityCategory = {
  type: _Activity.categoryType,
  args: {
    categoryId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (_, {
    categoryId
  }) => _db.db.models.ActivityCategory.findOne({
    where: {
      id: categoryId
    }
  })
};
exports.activityCategory = activityCategory;
const activity = {
  type: _Activity.activityType,
  args: {
    activityId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: (_, {
    activityId
  }) => _db.db.models.Activity.scope('includeDetailFragments').findOne({
    where: {
      id: activityId
    }
  })
};
exports.activity = activity;
var _default = activity;
exports.default = _default;