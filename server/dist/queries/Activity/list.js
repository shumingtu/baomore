"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.activities = exports.activityCategories = void 0;

var _graphql = require("graphql");

var _db = require("../../db");

var _Activity = require("../../types/Activity");

const activityCategories = {
  type: new _graphql.GraphQLList(_Activity.categoryType),
  resolve: () => _db.db.models.ActivityCategory.findAll()
};
exports.activityCategories = activityCategories;
const activities = {
  type: new _graphql.GraphQLList(_Activity.activityListType),
  args: {
    categoryId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    },
    status: {
      type: _graphql.GraphQLBoolean
    }
  },
  resolve: async (_, {
    categoryId,
    status
  }) => {
    const whereClause = {
      ActivityCategoryId: categoryId
    };
    if (status !== undefined) whereClause.isActived = status;
    const result = await _db.db.models.Activity.findAll({
      where: whereClause
    });
    return result;
  }
};
exports.activities = activities;
var _default = null;
exports.default = _default;