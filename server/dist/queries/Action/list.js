"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.actionList = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _Action = require("../../types/Action");

var _General = require("../../errors/General");

var _roleActions = require("../../../shared/roleActions");

const actionList = {
  type: new _graphql.GraphQLList(_Action.actionType),
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.ADMIN_MEMBER_MANAGE
  })(() => _db.db.models.Action.findAll())
};
exports.actionList = actionList;
var _default = null;
exports.default = _default;