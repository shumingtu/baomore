"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.emoji = exports.emojiCategories = void 0;

var _graphql = require("graphql");

var _db = require("../../db");

var _Emoji = require("../../types/Emoji");

const emojiCategories = {
  type: new _graphql.GraphQLList(_Emoji.emojiCategoryType),
  resolve: () => _db.db.models.EmojiCategory.findAll()
};
exports.emojiCategories = emojiCategories;
const emoji = {
  type: new _graphql.GraphQLList(_Emoji.emojiType),
  args: {
    categoryId: {
      type: _graphql.GraphQLInt
    }
  },
  resolve: (_, args) => _db.db.models.Emoji.scope({
    method: ['search', args]
  }).findAll()
};
exports.emoji = emoji;