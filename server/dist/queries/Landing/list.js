"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.landings = void 0;

var _graphql = require("graphql");

var _db = require("../../db");

var _Landing = require("../../types/Landing");

const landings = {
  type: new _graphql.GraphQLList(_Landing.listType),
  args: {
    id: {
      type: _graphql.GraphQLInt
    }
  },
  resolve: (_, {
    id
  }) => {
    if (id) {
      return _db.db.models.Landing.findAll({
        where: {
          id
        }
      });
    }

    return _db.db.models.Landing.findAll();
  }
};
exports.landings = landings;
var _default = landings;
exports.default = _default;