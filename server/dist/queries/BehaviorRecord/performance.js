"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.employeePerformanceForAdmin = exports.storePerformanceForAdmin = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _lodash = _interopRequireDefault(require("lodash"));

var _moment = _interopRequireDefault(require("moment"));

var _sequelize = require("sequelize");

var _db = require("../../db");

var _BehaviorRecord = require("../../types/BehaviorRecord");

var _General = require("../../errors/General");

var _roleActions = require("../../../shared/roleActions");

var _performanceCalculator = require("../../helpers/performanceCalculator.js");

const storePerformanceForAdmin = {
  type: new _graphql.GraphQLList(_BehaviorRecord.storePerformanceForAdminType),
  args: {
    startDate: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    endDate: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    storeId: {
      type: _graphql.GraphQLInt
    },
    channelId: {
      type: _graphql.GraphQLInt
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.ADMIN_PERFORMANCE_MANAGE
  })(async (_, args) => {
    const searchForPerformanceArgs = { ...args,
      behaviorId: 1
    };
    const {
      startDate,
      endDate
    } = args;
    const lastMonthStartTime = (0, _moment.default)(startDate).subtract(1, 'months');
    const lastMonthEndTime = (0, _moment.default)(endDate).subtract(1, 'months');
    const lastMonthArgs = { ...searchForPerformanceArgs,
      startDate: lastMonthStartTime,
      endDate: lastMonthEndTime
    };
    const [rawResult, memberSchedule, performanceSettings, lastMonthRecord] = await Promise.all([_db.db.models.BehaviorRecord.scope({
      method: ['searchForPerformance', searchForPerformanceArgs]
    }, {
      method: ['performancePNTableAssociation']
    }, {
      method: ['performanceStoreAssociation', args]
    }).findAll({
      attributes: ['id', 'startTime', 'endTime', 'price', 'BehaviorId', 'MemberId', 'PNTableId', 'StoreId']
    }), _db.db.models.MemberSchedule.scope({
      method: ['search', args]
    }).findAll({
      attributes: ['id', 'date', 'status', 'MemberId'],
      where: {
        [_sequelize.Op.or]: [{
          status: '4'
        }, {
          status: '004'
        }]
      }
    }), _db.db.models.PerformanceSetting.findAll({
      attributes: ['id', 'PNCategoryId', 'targetAmount', 'commission']
    }), _db.db.models.BehaviorRecord.scope({
      method: ['searchForPerformance', lastMonthArgs]
    }, {
      method: ['performancePNTableAssociation']
    }, {
      method: ['performanceStoreAssociation', args]
    }).findAll({
      attributes: ['id', 'price', 'BehaviorId', 'MemberId', 'PNTableId', 'StoreId']
    })]);
    const totalDays = ((0, _moment.default)(endDate) - (0, _moment.default)(startDate)) / 86400000 + 1;

    const groupByStore = _lodash.default.groupBy(rawResult, 'StoreId');

    const calculatedResult = await Promise.all(Object.keys(groupByStore).map(async storeId => {
      const storeRecord = groupByStore[storeId];
      const storeMemberIds = storeRecord.map(x => x.MemberId).filter((element, index, arr) => arr.indexOf(element) === index);
      const lastMonthRecordByStore = lastMonthRecord.filter(x => x.StoreId === parseInt(storeId, 10));
      const workDays = memberSchedule.filter(x => storeMemberIds.indexOf(x.MemberId) >= 0).length;
      const recordGroupByPNCategoryId = await (0, _performanceCalculator.getRecordGroupByPNCategoryId)(storeRecord, performanceSettings, lastMonthRecordByStore, totalDays, workDays, storeMemberIds);
      return {
        workDays,
        store: storeRecord[0].Store,
        performance: recordGroupByPNCategoryId
      };
    }));
    const materialSetting = performanceSettings.find(x => x.PNCategoryId === 1);
    const protectorSetting = performanceSettings.find(x => x.PNCategoryId === 2);
    const result = [];
    calculatedResult.forEach(x => {
      const materialPerformance = x.performance.find(p => p.pnCategory.id === 1);
      const protectorPerformance = x.performance.find(p => p.pnCategory.id === 2);
      const clientCalculatedResult = (0, _performanceCalculator.getClientCalResult)(materialPerformance, protectorPerformance, materialSetting, protectorSetting, totalDays);
      result.push({
        id: x.store.id,
        name: x.store.name,
        channel: x.store.Channel,
        ...clientCalculatedResult
      });
    });
    return result;
  })
};
exports.storePerformanceForAdmin = storePerformanceForAdmin;
const employeePerformanceForAdmin = {
  type: new _graphql.GraphQLList(_BehaviorRecord.employeePerformanceForAdminType),
  args: {
    startDate: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    endDate: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    directorId: {
      type: _graphql.GraphQLInt
    },
    employeeId: {
      type: _graphql.GraphQLInt
    },
    areaId: {
      type: _graphql.GraphQLInt
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.ADMIN_PERFORMANCE_MANAGE
  })(async (_, args) => {
    const {
      directorId,
      areaId,
      startDate,
      endDate
    } = args;
    let targetMemberIds = [];
    const searchForPerformanceArgs = { ...args,
      behaviorId: 1
    };
    const memberScheduleArgs = { ...args
    }; // 主任底下的包膜師

    if (directorId) {
      targetMemberIds = await _db.db.models.Member.findAll({
        where: {
          MemberId: directorId
        }
      }).map(member => member.id);
      searchForPerformanceArgs.employeeId = targetMemberIds;
      memberScheduleArgs.employeeId = targetMemberIds;
    }

    const lastMonthStartTime = (0, _moment.default)(startDate).subtract(1, 'months');
    const lastMonthEndTime = (0, _moment.default)(endDate).subtract(1, 'months');
    const lastMonthArgs = { ...searchForPerformanceArgs,
      startDate: lastMonthStartTime,
      endDate: lastMonthEndTime
    };
    const [rawResult, memberSchedule, performanceSettings, lastMonthRecord] = await Promise.all([_db.db.models.BehaviorRecord.scope({
      method: ['searchForPerformance', searchForPerformanceArgs]
    }, {
      method: ['performanceMemberAssociation', {
        areaId: areaId || null
      }]
    }, {
      method: ['performancePNTableAssociation']
    } // memberStock另外拉
    ).findAll({
      attributes: ['id', 'startTime', 'endTime', 'price', 'BehaviorId', 'MemberId', 'PNTableId']
    }), _db.db.models.MemberSchedule.scope({
      method: ['search', memberScheduleArgs]
    }).findAll({
      attributes: ['id', 'date', 'status', 'MemberId'],
      where: {
        [_sequelize.Op.or]: [{
          status: '4'
        }, {
          status: '004'
        }]
      }
    }), _db.db.models.PerformanceSetting.findAll({
      attributes: ['id', 'PNCategoryId', 'targetAmount', 'commission']
    }), _db.db.models.BehaviorRecord.scope({
      method: ['searchForPerformance', lastMonthArgs]
    }, {
      method: ['performanceMemberAssociation', {
        areaId: areaId || null
      }]
    }, {
      method: ['performancePNTableAssociation']
    }).findAll({
      attributes: ['id', 'price', 'BehaviorId', 'MemberId', 'PNTableId']
    })]);
    const totalDays = ((0, _moment.default)(endDate) - (0, _moment.default)(startDate)) / 86400000 + 1;

    const groupByMember = _lodash.default.groupBy(rawResult, 'MemberId');

    const calculatedResult = await Promise.all(Object.keys(groupByMember).map(async memberId => {
      const memberRecord = groupByMember[memberId];
      const lastMonthRecordByMember = lastMonthRecord.filter(x => x.MemberId === parseInt(memberId, 10));
      const workDays = memberSchedule.filter(x => x.MemberId === parseInt(memberId, 10)).length;
      const recordGroupByPNCategoryId = await (0, _performanceCalculator.getRecordGroupByPNCategoryId)(memberRecord, performanceSettings, lastMonthRecordByMember, totalDays, workDays, [parseInt(memberId, 10)]);
      return {
        workDays,
        member: memberRecord[0].Member,
        performance: recordGroupByPNCategoryId
      };
    }));
    const materialSetting = performanceSettings.find(x => x.PNCategoryId === 1);
    const protectorSetting = performanceSettings.find(x => x.PNCategoryId === 2);
    const result = [];
    calculatedResult.forEach(x => {
      const materialPerformance = x.performance.find(p => p.pnCategory.id === 1);
      const protectorPerformance = x.performance.find(p => p.pnCategory.id === 2);
      const clientCalculatedResult = (0, _performanceCalculator.getClientCalResult)(materialPerformance, protectorPerformance, materialSetting, protectorSetting, totalDays);
      result.push({
        id: x.member.id,
        name: x.member.name,
        area: x.member.Area,
        ...clientCalculatedResult
      });
    });
    return result;
  })
};
exports.employeePerformanceForAdmin = employeePerformanceForAdmin;
var _default = null;
exports.default = _default;