"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.recordList = exports.behaviorList = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _roleActions = require("../../../shared/roleActions");

var _General = require("../../errors/General");

var _BehaviorRecord = require("../../types/BehaviorRecord");

const behaviorList = {
  type: new _graphql.GraphQLList(_BehaviorRecord.behaviorType),
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: [_roleActions.actions.ADMIN_DOA_MANAGE, _roleActions.actions.INVOICING_MANAGE],
    orMode: true
  })(() => _db.db.models.Behavior.findAll())
};
exports.behaviorList = behaviorList;
const recordList = {
  type: new _graphql.GraphQLList(_BehaviorRecord.behaviorRecordType),
  args: {
    behaviorId: {
      type: _graphql.GraphQLInt
    },
    employeeId: {
      type: _graphql.GraphQLInt
    },
    startDate: {
      type: _graphql.GraphQLString
    },
    endDate: {
      type: _graphql.GraphQLString
    },
    recordStartDate: {
      type: _graphql.GraphQLString
    },
    recordEndDate: {
      type: _graphql.GraphQLString
    },
    limit: {
      type: _graphql.GraphQLInt,
      defaultValue: 10
    },
    offset: {
      type: _graphql.GraphQLInt,
      defaultValue: 0
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: [_roleActions.actions.ADMIN_DOA_MANAGE, _roleActions.actions.INVOICING_MANAGE],
    orMode: true
  })((_, args) => _db.db.models.BehaviorRecord.scope({
    method: ['searchForPerformance', args]
  }, {
    method: ['pages', args.limit, args.offset]
  }, 'member', 'order', 'behavior').findAll())
};
exports.recordList = recordList;
var _default = {
  type: new _graphql.GraphQLList(_BehaviorRecord.behaviorRecordType),
  args: {
    rollbackType: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLString)
    },
    pnCategoryId: {
      type: _graphql.GraphQLInt
    },
    startDate: {
      type: _graphql.GraphQLString
    },
    endDate: {
      type: _graphql.GraphQLString
    },
    code: {
      type: _graphql.GraphQLString
    },
    memberId: {
      type: _graphql.GraphQLInt
    },
    errorType: {
      type: _graphql.GraphQLString
    },
    target: {
      type: _graphql.GraphQLString
    },
    storeId: {
      type: _graphql.GraphQLInt
    },
    vat: {
      type: _graphql.GraphQLString
    },
    customerName: {
      type: _graphql.GraphQLString
    },
    customerPhone: {
      type: _graphql.GraphQLString
    },
    limit: {
      type: _graphql.GraphQLInt,
      defaultValue: 10
    },
    offset: {
      type: _graphql.GraphQLInt,
      defaultValue: 0
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: [_roleActions.actions.ADMIN_DOA_MANAGE, _roleActions.actions.INVOICING_MANAGE],
    orMode: true
  })(async (_, args) => {
    const behaviorRecords = await _db.db.models.BehaviorRecord.scope({
      method: ['search', args]
    }, {
      method: ['pnTable', args.code, args.pnCategoryId]
    }, {
      method: ['pages', args.limit, args.offset]
    }, 'member', 'order', {
      method: ['vendor', false]
    }, {
      method: ['store', false]
    }, {
      method: ['approver', false]
    }).findAll();
    if (!behaviorRecords) throw new _General.ResourceNotFoundError();
    return behaviorRecords.map(record => ({ ...record.dataValues,
      remarks: record.dataValues.remark && record.dataValues.remark.split('***') || [],
      pnCategory: record.PNTable && record.PNTable.PNSubCategory && record.PNTable.PNSubCategory.PNCategory || null
    }));
  })
};
exports.default = _default;