"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.listClosedOrderSetting = exports.itemClosedOrderSetting = void 0;

var _graphql = require("graphql");

var _db = require("../../db");

var _ClosedOrderSetting = require("../../types/ClosedOrderSetting");

const itemClosedOrderSetting = {
  type: _ClosedOrderSetting.ClosedOrderSettingType,
  args: {
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: async (_, {
    id
  }) => {
    const result = await _db.db.models.ClosedOrderSetting.findOne({
      where: {
        id
      }
    });
    let PNTable = null;

    if (result && result.PNTableId) {
      PNTable = await _db.db.models.PNTable.findOne({
        where: {
          id: result.PNTableId
        }
      });
    }

    return { ...result.dataValues,
      PNTable
    };
  }
};
exports.itemClosedOrderSetting = itemClosedOrderSetting;
const listClosedOrderSetting = {
  type: new _graphql.GraphQLList(_ClosedOrderSetting.ClosedOrderSettingType),
  resolve: async () => {
    const result = await _db.db.models.ClosedOrderSetting.scope('joinPNTable').findAll();
    return result;
  }
};
exports.listClosedOrderSetting = listClosedOrderSetting;
var _default = null;
exports.default = _default;