"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.listPNCatgoryFilterByKeyword = exports.listForPNCategory = void 0;

var _graphql = require("graphql");

var _db = require("../../../db");

var _PNTable = require("../../../types/PNTable");

const listForPNCategory = {
  type: new _graphql.GraphQLList(_PNTable.PNCategoryType),
  resolve: async () => _db.db.models.PNCategory.findAll()
};
exports.listForPNCategory = listForPNCategory;
const listPNCatgoryFilterByKeyword = {
  type: new _graphql.GraphQLList(_PNTable.PNCategoryType),
  args: {
    keyword: {
      type: _graphql.GraphQLString
    }
  },
  resolve: async (_, {
    keyword
  }) => _db.db.models.PNCategory.scope({
    method: ['filterByKeyword', keyword]
  }).findAll()
};
exports.listPNCatgoryFilterByKeyword = listPNCatgoryFilterByKeyword;
var _default = listForPNCategory;
exports.default = _default;