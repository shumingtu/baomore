"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.pnSubCategoryItem = void 0;

var _graphql = require("graphql");

var _db = require("../../../db");

var _PNTable = require("../../../types/PNTable");

const pnSubCategoryItem = {
  type: _PNTable.PNSubCategoryType,
  args: {
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: async (_, {
    id
  }) => _db.db.models.PNSubCategory.scope({
    method: ['findById', id]
  }).findOne()
};
exports.pnSubCategoryItem = pnSubCategoryItem;
var _default = null;
exports.default = _default;