"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.listFilterByPNSubCategory = exports.listFilterByPNCategory = exports.listForSale = void 0;

var _graphql = require("graphql");

var _db = require("../../../db");

var _PNTable = require("../../../types/PNTable");

const listForSale = {
  type: new _graphql.GraphQLList(_PNTable.PNSubCategoryType),
  resolve: async () => _db.db.models.PNSubCategory.scope('onSale', 'orderBySeqNum', 'bigSubCategory').findAll()
};
exports.listForSale = listForSale;
const listFilterByPNCategory = {
  type: new _graphql.GraphQLList(_PNTable.PNSubCategoryType),
  description: '取得根據料好類型的大類列表, 帶PNCategoryId',
  args: {
    id: {
      type: _graphql.GraphQLInt
    },
    keyword: {
      type: _graphql.GraphQLString
    },
    isOnSale: {
      type: _graphql.GraphQLBoolean,
      defaultValue: false
    }
  },
  resolve: async (_, {
    id,
    keyword,
    isOnSale
  }) => _db.db.models.PNSubCategory.scope({
    method: ['findByPNCategoryId', id]
  }, {
    method: ['filterByKeyword', keyword]
  }, isOnSale ? 'onSale' : {}).findAll()
};
exports.listFilterByPNCategory = listFilterByPNCategory;
const listFilterByPNSubCategory = {
  type: new _graphql.GraphQLList(_PNTable.PNSubCategoryType),
  args: {
    id: {
      type: _graphql.GraphQLInt
    },
    keyword: {
      type: _graphql.GraphQLString
    },
    isOnSale: {
      type: _graphql.GraphQLBoolean,
      defaultValue: false
    }
  },
  resolve: async (_, {
    id,
    keyword,
    isOnSale
  }) => _db.db.models.PNSubCategory.scope({
    method: ['findBySubCategoryId', id]
  }, {
    method: ['filterByKeyword', keyword]
  }, isOnSale ? 'onSale' : {}).findAll()
};
exports.listFilterByPNSubCategory = listFilterByPNSubCategory;