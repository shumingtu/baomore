"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.listFilterByAdmin = exports.listForPNTableCode = exports.customPNTable = exports.pnTableItem = exports.listFilterBySubCategory = void 0;

var _graphql = require("graphql");

var _db = require("../../db");

var _PNTable = require("../../types/PNTable");

var _PNTable2 = require("../../errors/PNTable");

const listFilterBySubCategory = {
  type: new _graphql.GraphQLList(_PNTable.PNTableType),
  args: {
    subCategoryId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: async (_, {
    subCategoryId
  }) => _db.db.models.PNTable.scope({
    method: ['filterBySubCategory', subCategoryId]
  }).findAll({
    where: {
      isOnSale: true
    }
  })
};
exports.listFilterBySubCategory = listFilterBySubCategory;
const pnTableItem = {
  type: _PNTable.PNTableType,
  args: {
    id: {
      type: _graphql.GraphQLInt
    }
  },
  resolve: async (_, {
    id
  }) => {
    const result = await _db.db.models.PNTable.scope({
      method: ['search', {
        id
      }]
    }, {
      method: ['vendor']
    }, {
      method: ['categories']
    }, {
      method: ['brandAndModel']
    }).findOne();
    if (!result) throw new _PNTable2.PNTableItemNotFoundError();
    const isHaveSmallCategory = result.PNSubCategory.PNSubCategory;
    return { ...result.dataValues,
      pnCategory: result.PNSubCategory.PNCategory,
      bigSubCategory: isHaveSmallCategory ? result.PNSubCategory.PNSubCategory : result.PNSubCategory,
      smallSubCategory: isHaveSmallCategory ? result.PNSubCategory : null,
      Vendor: result.Vendor,
      BrandModel: result && result.BrandModel || null,
      Brand: result && result.BrandModel && result.BrandModel.Brand || null
    };
  }
};
exports.pnTableItem = pnTableItem;
const customPNTable = {
  type: _PNTable.CustomPNTableType,
  resolve: async (_, {
    id
  }) => {
    const result = await _db.db.models.PNTable.scope({
      method: ['search', {
        isAllCustomized: true
      }]
    }).findOne();
    if (!result) throw new _PNTable2.PNTableItemNotFoundError();
    return result;
  }
};
exports.customPNTable = customPNTable;
const listForPNTableCode = {
  type: new _graphql.GraphQLList(_PNTable.PNTableCodeType),
  args: {
    code: {
      type: _graphql.GraphQLString
    }
  },
  resolve: async (_, args) => {
    const result = _db.db.models.PNTable.scope({
      method: ['findCode', args.code]
    }).findAll();

    return result;
  }
};
exports.listForPNTableCode = listForPNTableCode;
const listFilterByAdmin = {
  type: new _graphql.GraphQLList(_PNTable.PNTableType),
  args: {
    code: {
      type: _graphql.GraphQLString
    },
    pnCategoryId: {
      type: _graphql.GraphQLInt
    },
    bigSubCategoryId: {
      type: _graphql.GraphQLInt
    },
    smallSubCategoryId: {
      type: _graphql.GraphQLInt
    },
    keyword: {
      type: _graphql.GraphQLString
    },
    vendorId: {
      type: _graphql.GraphQLInt
    },
    brandId: {
      type: _graphql.GraphQLInt
    },
    modelId: {
      type: _graphql.GraphQLInt
    },
    limit: {
      type: _graphql.GraphQLInt,
      defaultValue: 10
    },
    offset: {
      type: _graphql.GraphQLInt,
      defaultValue: 0
    }
  },
  resolve: async (_, args) => {
    const {
      pnCategoryId,
      bigSubCategoryId,
      smallSubCategoryId,
      limit,
      offset,
      vendorId,
      brandId,
      modelId
    } = args;
    const results = await _db.db.models.PNTable.scope({
      method: ['search', args]
    }, {
      method: ['pages', limit, offset]
    }, {
      method: ['vendor', vendorId]
    }, {
      method: ['categories', pnCategoryId, bigSubCategoryId, smallSubCategoryId]
    }, {
      method: ['brandAndModel', brandId, modelId]
    }).findAll();
    const filteredResults = results.map(r => {
      const isHaveSmallCategory = r.PNSubCategory.PNSubCategory;
      return { ...r.dataValues,
        pnCategory: r.PNSubCategory.PNCategory,
        bigSubCategory: isHaveSmallCategory ? r.PNSubCategory.PNSubCategory : r.PNSubCategory,
        smallSubCategory: isHaveSmallCategory ? r.PNSubCategory : null,
        Vendor: r.Vendor
      };
    });
    return filteredResults;
  }
};
exports.listFilterByAdmin = listFilterByAdmin;