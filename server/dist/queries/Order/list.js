"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.orderList = exports.orderListByEmployee = exports.orderQRCodeList = void 0;

var _graphql = require("graphql");

var _graphqlAuthKeeper = require("graphql-auth-keeper");

var _db = require("../../db");

var _General = require("../../errors/General");

var _roleActions = require("../../../shared/roleActions");

var _Order = require("../../types/Order");

const orderQRCodeList = {
  type: new _graphql.GraphQLList(_Order.orderQRCodeType),
  args: {
    orderId: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt)
    }
  },
  resolve: async (_, {
    orderId
  }) => {
    const qrcodes = await _db.db.models.OrderQRCode.findAll({
      where: {
        OrderId: orderId
      }
    });
    return qrcodes;
  }
};
exports.orderQRCodeList = orderQRCodeList;
const orderListByEmployee = {
  type: new _graphql.GraphQLList(_Order.orderType),
  args: {
    status: {
      type: _graphql.GraphQLString
    },
    keyword: {
      type: _graphql.GraphQLString
    },
    orderSN: {
      type: _graphql.GraphQLString
    },
    closeDate: {
      type: _graphql.GraphQLString
    },
    qrcode: {
      type: _graphql.GraphQLString
    },
    limit: {
      type: _graphql.GraphQLInt,
      defaultValue: 10
    },
    offset: {
      type: _graphql.GraphQLInt,
      defaultValue: 0
    }
  },
  resolve: async (_, args, {
    authPayload
  }) => {
    if (!authPayload) {
      throw new _General.PermissionError();
    }

    const {
      id
    } = authPayload;
    const {
      limit,
      offset
    } = args;
    const result = await _db.db.models.Order.scope({
      method: ['search', args]
    }, 'store', {
      method: ['pages', limit, offset]
    }, {
      method: ['orderGroupShipment', args]
    }, {
      method: ['qrcodes', args]
    }).findAll({
      where: {
        EmpolyedMemberId: id
      },
      include: [{
        model: _db.db.models.PNTable.scope('brandAndModel', 'categories', {
          method: ['search', args]
        })
      }],
      order: [['createdAt', 'DESC']]
    });
    return result;
  }
};
exports.orderListByEmployee = orderListByEmployee;
const orderList = {
  type: new _graphql.GraphQLList(_Order.orderType),
  args: {
    memberId: {
      type: _graphql.GraphQLInt
    },
    status: {
      type: _graphql.GraphQLString
    },
    keyword: {
      type: _graphql.GraphQLString
    },
    orderSN: {
      type: _graphql.GraphQLString
    },
    closeDate: {
      type: _graphql.GraphQLString
    },
    qrcode: {
      type: _graphql.GraphQLString
    },
    limit: {
      type: _graphql.GraphQLInt,
      defaultValue: 10
    },
    offset: {
      type: _graphql.GraphQLInt,
      defaultValue: 0
    }
  },
  resolve: (0, _graphqlAuthKeeper.authKeeper)({
    onFailed: new _General.PermissionError(),
    actions: _roleActions.actions.INVOICING_MANAGE
  })(async (_, args) => {
    const {
      limit,
      offset,
      memberId
    } = args;
    const memberArgs = {
      memberId: memberId || null
    };
    const result = await _db.db.models.Order.scope({
      method: ['memberArea', memberArgs]
    }, {
      method: ['search', args]
    }, 'store', {
      method: ['pages', limit, offset]
    }, {
      method: ['orderGroupShipment', args]
    }, {
      method: ['qrcodes', args]
    }).findAll({
      include: [{
        model: _db.db.models.PNTable.scope('brandAndModel', 'categories', 'vendor', {
          method: ['search', args]
        })
      }, {
        model: _db.db.models.OnlineOrder
      }],
      order: [['createdAt', 'DESC']]
    });
    return result;
  })
};
exports.orderList = orderList;
var _default = null;
exports.default = _default;