"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.listChannel = void 0;

var _graphql = require("graphql");

var _db = require("../../db");

var _Channel = require("../../types/Channel");

const listChannel = {
  type: new _graphql.GraphQLList(_Channel.ChannelType),
  args: {
    isPurchaseChannel: {
      type: _graphql.GraphQLBoolean
    }
  },
  resolve: async (_, {
    isPurchaseChannel
  }) => {
    const whereClause = {};
    if (isPurchaseChannel !== undefined) whereClause.isPurchaseChannel = isPurchaseChannel;
    const channelList = await _db.db.models.Channel.findAll({
      where: whereClause
    });
    return channelList;
  }
};
exports.listChannel = listChannel;
var _default = null;
exports.default = _default;