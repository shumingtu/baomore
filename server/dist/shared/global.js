"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.invoiceSetting = exports.INVALID_INVOICING_API_HOST = exports.INVOICING_API_HOST = void 0;
var NODE_ENV = process.env.NODE_ENV || 'development';
var INVOICING_API_HOST = NODE_ENV === 'production' ? 'https://einvoice.ecpay.com.tw/Invoice/Issue' : 'https://einvoice-stage.ecpay.com.tw/Invoice/Issue';
exports.INVOICING_API_HOST = INVOICING_API_HOST;
var INVALID_INVOICING_API_HOST = NODE_ENV === 'production' ? 'https://einvoice.ecpay.com.tw/Invoice/IssueInvalid' : 'https://einvoice-stage.ecpay.com.tw/Invoice/IssueInvalid';
exports.INVALID_INVOICING_API_HOST = INVALID_INVOICING_API_HOST;
var invoiceSetting = {
  MerchantID: NODE_ENV === 'production' ? '3109636' : '2000132',
  HashKey: NODE_ENV === 'production' ? 'REuwh7lEQgJg5Whk' : 'ejCk326UnaZWKisg',
  HashIV: NODE_ENV === 'production' ? 'VpXVbfKCbWsCpAmS' : 'q9jcZX8Ib9LM8wYk'
};
exports.invoiceSetting = invoiceSetting;