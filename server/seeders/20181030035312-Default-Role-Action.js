import Promise from 'bluebird';
import {
  actions,
  roles,
} from '../../shared/roleActions.js';
import Member from '../columns/Member';
import Role from '../columns/Role';
import Action from '../columns/Action';
import RoleActions from '../columns/RoleActions';
import MemberRoles from '../columns/MemberRoles';
import Areas from '../columns/Area';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Areas', Areas(Sequelize));
    await queryInterface.createTable('Members', Member(Sequelize));
    await queryInterface.createTable('Roles', Role(Sequelize));
    await queryInterface.createTable('Actions', Action(Sequelize));
    await queryInterface.createTable('RoleActions', RoleActions(Sequelize), {
      indexes: [{
        name: 'ActionCode',
        fields: ['ActionCode'],
      }, {
        name: 'RoleId',
        fields: ['RoleId'],
      }],
    });
    await queryInterface.createTable('MemberRoles', MemberRoles(Sequelize), {
      indexes: [{
        name: 'MemberId',
        fields: ['MemberId'],
      }, {
        name: 'RoleId',
        fields: ['RoleId'],
      }],
    });

    await queryInterface.bulkInsert('Roles', Object.values(roles).map(role => ({
      id: role.id,
      name: role.name,
      createdAt: new Date(),
      updatedAt: new Date(),
    })));

    await queryInterface.bulkInsert('Actions', Object.values(actions).map(action => ({
      ...action,
      createdAt: new Date(),
      updatedAt: new Date(),
    })));

    await Object.values(roles).map(role => () => queryInterface.bulkInsert('RoleActions', role.actions.map(action => ({
      ActionCode: action.code,
      RoleId: role.id,
      createdAt: new Date(),
      updatedAt: new Date(),
    })))).reduce((prev, next) => prev.then(next), Promise.resolve());
  },

  down: async (queryInterface) => {
    await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 0');
    await queryInterface.dropTable('RoleActions');
    await queryInterface.dropTable('Actions');
    await queryInterface.dropTable('Roles');
    await queryInterface.dropTable('Areas');
    await queryInterface.dropTable('Members');
    await queryInterface.dropTable('MemberRoles');
    await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 1');
  },
};
