import googleMap from '@google/maps';
import Channels from '../columns/Channel';
import Stores from '../columns/Store';
import Vendors from '../columns/Vendor';
import City from '../columns/City';
import District from '../columns/District';
import { GOOGLE_MAP_API_KEY } from '../../shared/env';
import DistrictInit from '../mocks/district';

const googleMapClient = googleMap.createClient({
  key: GOOGLE_MAP_API_KEY,
  Promise,
});

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Vendors', Vendors(Sequelize));
    await queryInterface.createTable('Channels', Channels(Sequelize));
    await queryInterface.createTable('Cities', City(Sequelize));
    await queryInterface.createTable('Districts', District(Sequelize));
    await queryInterface.createTable('Stores', Stores(Sequelize));

    // Mocks Areas
    await queryInterface.bulkInsert('Areas', [{
      name: '北區',
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: '中區',
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: '南區',
      createdAt: new Date(),
      updatedAt: new Date(),
    }]);

    // Mocks Channels
    await queryInterface.bulkInsert('Channels', [{
      name: '神腦',
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: '台哥大',
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: '遠傳',
      createdAt: new Date(),
      updatedAt: new Date(),
    }]);

    // Mocks Vendors
    await queryInterface.bulkInsert('Vendors', [{
      name: '彩漾',
      createdAt: new Date(),
      updatedAt: new Date(),
    }]);

    // Mocks Cities & Districts
    await DistrictInit();

    // Mocks Stores
    await queryInterface.bulkInsert('Stores', [{
      name: '台東更生二',
      phone: '0917740622',
      address: '台東市更生路115號',
      ChannelId: 1,
      AreaId: 1,
      DistrictId: 338,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: '永和永和',
      phone: '0922688300',
      address: '新北市永和區永和路一段55號',
      ChannelId: 3,
      AreaId: 1,
      DistrictId: 33,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: '林口竹林',
      phone: '0981129330',
      address: '新北市林口區竹林路27號',
      ChannelId: 1,
      AreaId: 1,
      DistrictId: 42,
      createdAt: new Date(),
      updatedAt: new Date(),
    }]);

    const rawStores = await queryInterface.sequelize.query('SELECT * FROM Stores');
    const stores = rawStores[0];
    await Promise.all(stores.map(store => googleMapClient.geocode({ address: store.address })
      .asPromise()
      .then(async (response) => {
        const {
          location,
        } = response.json.results[0].geometry;
        await queryInterface.sequelize.query(`UPDATE Stores SET latitude = ${location.lat},longitude = ${location.lng} WHERE id=${store.id};`);
      })
      .catch((err) => {
        console.log(err);
      })
    ));
  },

  down: async (queryInterface) => {
    await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 0');
    await queryInterface.dropTable('Stores');
    await queryInterface.dropTable('Cities');
    await queryInterface.dropTable('Districts');
    await queryInterface.dropTable('Vendors');
    await queryInterface.dropTable('Channels');
    await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 1');
  },
};
