import BookTimeSlotGroup from '../columns/BookTimeSlotGroup';
import BookTimeSlot from '../columns/BookTimeSlot';
import Order from '../columns/Order';
import OnlineOrder from '../columns/OnlineOrder';
import OrderGroupShipment from '../columns/OrderGroupShipment';
import PNTableClosedDateSetting from '../columns/PNTableClosedDateSetting';
import Emoji from '../columns/Emoji';
import EmojiCategory from '../columns/EmojiCategory';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('BookTimeSlotGroups', BookTimeSlotGroup(Sequelize));
    await queryInterface.createTable('BookTimeSlots', BookTimeSlot(Sequelize));
    await queryInterface.createTable('OrderGroupShipments', OrderGroupShipment(Sequelize));
    await queryInterface.createTable('Orders', Order(Sequelize));
    await queryInterface.createTable('OnlineOrders', OnlineOrder(Sequelize));
    await queryInterface.createTable('PNTableClosedDateSettings', PNTableClosedDateSetting(Sequelize));
    await queryInterface.createTable('EmojiCategories', EmojiCategory(Sequelize));
    await queryInterface.createTable('Emojis', Emoji(Sequelize));

    // BookTimeSlotGroup Mocks
    await queryInterface.bulkInsert('BookTimeSlotGroups', [{
      name: '下午',
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: '晚上',
      createdAt: new Date(),
      updatedAt: new Date(),
    }]);

    // BookTimeSlot Mocks
    await queryInterface.bulkInsert('BookTimeSlots', [{
      time: '13:00',
      BookTimeSlotGroupId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      time: '14:00',
      BookTimeSlotGroupId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      time: '15:00',
      BookTimeSlotGroupId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      time: '16:00',
      BookTimeSlotGroupId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      time: '17:00',
      BookTimeSlotGroupId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      time: '18:00',
      BookTimeSlotGroupId: 2,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      time: '19:00',
      BookTimeSlotGroupId: 2,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      time: '20:00',
      BookTimeSlotGroupId: 2,
      createdAt: new Date(),
      updatedAt: new Date(),
    }]);

    await queryInterface.bulkInsert('PNTableClosedDateSettings', [{
      weekDay: 'WED',
      time: '10:00',
      createdAt: new Date(),
      updatedAt: new Date(),
    }]);

    // Emoji Categroies
    await queryInterface.bulkInsert('EmojiCategories', [{
      name: 'Facebook Emoji',
      createdAt: new Date(),
      updatedAt: new Date(),
    }]);

    // Emoji Mocks
    await queryInterface.bulkInsert('Emojis', [{
      picture: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/emoji-1.png',
      EmojiCategoryId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      picture: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/emoji-2.png',
      EmojiCategoryId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      picture: 'https://s3-ap-northeast-1.amazonaws.com/baomore-test/emoji-3.png',
      EmojiCategoryId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    }]);
  },

  down: async (queryInterface) => {
    await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 0');
    await queryInterface.dropTable('BookTimeSlotGroups');
    await queryInterface.dropTable('BookTimeSlots');
    await queryInterface.dropTable('OrderGroupShipments');
    await queryInterface.dropTable('Orders');
    await queryInterface.dropTable('OnlineOrders');
    await queryInterface.dropTable('PNTableClosedDateSettings');
    await queryInterface.dropTable('EmojiCategories');
    await queryInterface.dropTable('Emojis');
    await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 1');
  },
};
