const NODE_ENV = process.env.NODE_ENV || 'development';

export const INVOICING_API_HOST = NODE_ENV === 'production' ? 'https://einvoice.ecpay.com.tw/Invoice/Issue' : 'https://einvoice-stage.ecpay.com.tw/Invoice/Issue';
export const INVALID_INVOICING_API_HOST = NODE_ENV === 'production' ? 'https://einvoice.ecpay.com.tw/Invoice/IssueInvalid' : 'https://einvoice-stage.ecpay.com.tw/Invoice/IssueInvalid';

export const invoiceSetting = {
  MerchantID: NODE_ENV === 'production' ? '3109636' : '2000132',
  HashKey: NODE_ENV === 'production' ? 'REuwh7lEQgJg5Whk' : 'ejCk326UnaZWKisg',
  HashIV: NODE_ENV === 'production' ? 'VpXVbfKCbWsCpAmS' : 'q9jcZX8Ib9LM8wYk',
};
