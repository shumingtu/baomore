/* Action keys reference: /shared/roleActions.js */

/* linebot */
// 巡檢打卡 - 可查看巡檢打卡頁面(含所有操作功能)
export const LINEBOT_PUNCH_CHECK_PAGE_KEYS = [
  'MANAGE_EMPLOYEE_MANAGER',
];
// 包膜師資訊 - 可選擇區域 + 關鍵字搜尋
export const LINEBOT_EMPLOYEE_INFO_AREA_SELECTOR_KEYS = [
  'MANAGE_EMPLOYEE_MANAGER',
];
// 門市店點 - 可查看門市店點頁面(含所有操作功能)
export const LINEBOT_STORE_PLACES_PAGE_KEYS = [
  'MANAGE_EMPLOYEE_MANAGER',
];
// 業績查詢 - 可選擇包膜師
export const LINEBOT_PERFORMANCE_EMPLOYEE_SELECTOR_KEYS = [
  'MANAGE_EMPLOYEE_MANAGER',
];
// 庫存 - 可選擇包膜師
export const LINEBOT_INVENTORY_EMPLOYEE_SELECTOR_KEYS = [
  'MANAGE_EMPLOYEE_MANAGER',
];
